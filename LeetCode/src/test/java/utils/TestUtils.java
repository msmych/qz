package utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static java.util.Arrays.sort;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.*;

public class TestUtils {

    @SafeVarargs
    public static <T> void assertEqualsOneOf(T actual, T... options) {
        for (T option : options) {
            if (actual.equals(option))
                return;
        }
        fail("No match");
    }

    public static boolean arrayEquals(int[][] expected, int[][] actual) {
        if (expected.length == 0) return actual.length == 0;
        if (expected.length != actual.length || expected[0].length != actual[0].length) return false;
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[0].length; j++) {
                if (expected[i][j] != actual[i][j]) return false;
            }
        }
        return true;
    }

    public static void assertArrayContainsAll(int[] arr, int... vals) {
        assertEquals(vals.length, arr.length);
        List<Integer> list = stream(arr).boxed().collect(toList());
        for (int val : vals)
            assertTrue(list.contains(val));
    }

    @SafeVarargs
    public static <T> void assertCollectionContainsAll(Collection<T> collection, T... vals) {
        assertEquals(vals.length, collection.size());
        for (T val : vals)
            assertTrue(collection.contains(val));
    }

    public static <T> void assertListCollectionContainsArrays(T[][] arrs, Collection<List<T>> lists) {
        assertEquals(arrs.length, lists.size());
        for (T[] arr : arrs) {
            sort(arr);
            assertTrue(lists.stream()
                    .anyMatch(list -> Arrays.equals(
                            list.stream()
                                    .sorted()
                                    .collect(toList())
                                    .toArray(),
                            arr)));
        }
    }

    public static <T> void assertListCollectionEquivalentToArrays(T[][] arrs, Collection<List<T>> lists) {
        assertEquals(arrs.length, lists.size());
        Iterator<List<T>> iterator = lists.iterator();
        for (T[] arr : arrs)
            assertArrayEquals(arr, iterator.next().toArray());
    }
}
