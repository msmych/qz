package leetcode.trie;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ReplaceWordsTest {

    private ReplaceWords replaceWords = new ReplaceWords.Solution();

    @Test void cat_bat_rat$the_cattle_was_rattled_by_the_battery() {
        assertEquals("the cat was rat by the bat",
                replaceWords.replaceWords(Arrays.asList("cat", "bat", "rat"),
                        "the cattle was rattled by the battery"));
    }

    @Test void a_aa_aaa_aaaa$a_aa_a_aaaa_aaa_aaa_aaa_aaaaaa_bbb_baba_ababa() {
        assertEquals("a a a a a a a a bbb baba a",
                replaceWords.replaceWords(Arrays.asList("a", "aa", "aaa", "aaaa"),
        "a aa a aaaa aaa aaa aaa aaaaaa bbb baba ababa"));
    }
}