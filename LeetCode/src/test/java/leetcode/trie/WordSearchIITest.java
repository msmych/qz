package leetcode.trie;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static utils.TestUtils.assertCollectionContainsAll;

class WordSearchIITest {

  private WordSearchII wordSearchII = new WordSearchII.Solution();

  @Test
  void oath_pea_eat_rain() {
    assertCollectionContainsAll(wordSearchII.findWords(new char[][]{
        {'o', 'a', 'a', 'n'},
        {'e', 't', 'a', 'e'},
        {'i', 'h', 'k', 'r'},
        {'i', 'f', 'l', 'v'}
      }, new String[]{"oath", "pea", "eat", "rain"}),
      "eat", "oath");
  }

  @Test
  void a_a() {
    assertArrayEquals(new String[]{"a"},
      wordSearchII.findWords(new char[][]{{'a', 'a'}}, new String[]{"a"}).toArray(new String[]{}));
  }

  @Test
  void a_b_a_a() {
    assertCollectionContainsAll(wordSearchII.findWords(new char[][]{{'a', 'b'}, {'a', 'a'}},
      new String[]{"aba", "baa", "bab", "aaab", "aaa", "aaaa", "aaba"}),
      "aaa", "aaab", "aaba", "aba", "baa");
  }
}