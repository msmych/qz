package leetcode.trie;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MapSumTest {

    @Test void apple() {
        AbstractMapSum mapSum = new MapSum();
        mapSum.insert("apple", 3);
        assertEquals(3, mapSum.sum("ap"));
        mapSum.insert("ap", 2);
        assertEquals(5, mapSum.sum("ap"));
    }
}