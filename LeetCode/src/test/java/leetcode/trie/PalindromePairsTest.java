package leetcode.trie;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PalindromePairsTest {

    private PalindromePairs palindromePairs = new PalindromePairs.Solution();

    @Test void abcd_dcba_lls_s_sssll() {
        List<List<Integer>> result = palindromePairs.palindromePairs(new String[]{"abcd", "dcba", "lls", "s", "sssll"});
        assertEquals(4, result.size());
        assertContainsPair(result, 0, 1);
        assertContainsPair(result, 1, 0);
        assertContainsPair(result, 3, 2);
        assertContainsPair(result, 2, 4);
    }

    private void assertContainsPair(List<List<Integer>> pairs, int i, int j) {
        assertTrue(pairs.stream().anyMatch(pair -> pair.get(0) == i && pair.get(1) == j));
    }

    @Test void bat_tab_cat() {
        List<List<Integer>> result = palindromePairs.palindromePairs(new String[]{"bat", "tab", "cat"});
        assertEquals(2, result.size());
        assertContainsPair(result, 0, 1);
        assertContainsPair(result, 1, 0);
    }

    @Test void a_() {
        List<List<Integer>> result = palindromePairs.palindromePairs(new String[]{"a", ""});
        assertEquals(2, result.size());
        assertContainsPair(result, 0, 1);
        assertContainsPair(result, 1, 0);
    }

    @Test void a_abc_aba() {
        List<List<Integer>> result = palindromePairs.palindromePairs(new String[]{"a", "abc", "aba", ""});
        assertEquals(4, result.size());
        assertContainsPair(result, 0, 3);
        assertContainsPair(result, 3, 0);
        assertContainsPair(result, 2, 3);
        assertContainsPair(result, 3, 2);
    }
}