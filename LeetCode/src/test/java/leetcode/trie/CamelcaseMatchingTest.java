package leetcode.trie;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CamelcaseMatchingTest {

    private CamelcaseMatching camelcaseMatching = new CamelcaseMatching.Solution();

    @Test void FB() {
        assertArrayEquals(new Boolean[]{true,false,true,true,false},
                camelcaseMatching.camelMatch(
                        new String[]{"FooBar","FooBarTest","FootBall","FrameBuffer","ForceFeedBack"}, "FB")
                .toArray(new Boolean[]{}));
    }

    @Test void FoBa() {
        assertArrayEquals(new Boolean[]{true,false,true,false,false},
                camelcaseMatching.camelMatch(
                        new String[]{"FooBar","FooBarTest","FootBall","FrameBuffer","ForceFeedBack"}, "FoBa")
                        .toArray(new Boolean[]{}));
    }

    @Test void FoBaT() {
        assertArrayEquals(new Boolean[]{false,true,false,false,false},
                camelcaseMatching.camelMatch(
                        new String[]{"FooBar","FooBarTest","FootBall","FrameBuffer","ForceFeedBack"}, "FoBaT")
                        .toArray(new Boolean[]{}));
    }

    @Test void CooP() {
        assertArrayEquals(new Boolean[]{false, false, true},
                camelcaseMatching.camelMatch(
                        new String[]{"CompetitiveProgramming", "CounterPick", "ControlPanel"}, "CooP")
                        .toArray(new Boolean[]{}));
    }

    @Test void ksvjLiknTzqn() {
        assertArrayEquals(new Boolean[]{true,true,true,true,true,true},
                camelcaseMatching.camelMatch(
                        new String[]{"aksvbjLiknuTzqon","ksvjLimflkpnTzqn","mmkasvjLiknTxzqn","ksvjLiurknTzzqbn","ksvsjLctikgnTzqn","knzsvzjLiknTszqn"},
                "ksvjLiknTzqn")
                        .toArray(new Boolean[]{}));
    }

    @Test void AqlzahalcezsLfj() {
        assertArrayEquals(new Boolean[]{true,true,true,true,true,true,true,true},
                camelcaseMatching.camelMatch(
                        new String[]{"uAxaqlzahfialcezsLfj","cAqlzyahaslccezssLfj","AqlezahjarflcezshLfj","AqlzofahaplcejuzsLfj","tAqlzahavslcezsLwzfj",
                                "AqlzahalcerrzsLpfonj","AqlzahalceaczdsosLfj","eAqlzbxahalcezelsLfj"},
                "AqlzahalcezsLfj")
                .toArray(new Boolean[]{}));
    }
}