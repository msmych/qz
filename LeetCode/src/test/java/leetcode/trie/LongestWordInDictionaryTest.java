package leetcode.trie;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestWordInDictionaryTest {

    private LongestWordInDictionary longestWordInDictionary = new LongestWordInDictionary.Solution();

    @Test void world() {
        assertEquals("world",
                longestWordInDictionary.longestWord(new String[]{"w","wo","wor","worl", "world"}));
    }

    @Test void apple() {
        assertEquals("apple",
                longestWordInDictionary.longestWord(new String[]{"a", "banana", "app", "appl", "ap", "apply", "apple"}));
    }
}