package leetcode.trie;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaximumXorOfTwoNumbersInAnArrayTest {

    private MaximumXorOfTwoNumbersInAnArray maximumXorOfTwoNumbersInAnArray =
            new MaximumXorOfTwoNumbersInAnArray.Solution();

    @Test void input3_10_5_25_2_8output28() {
        assertEquals(28, maximumXorOfTwoNumbersInAnArray.findMaximumXOR(new int[]{3, 10, 5, 25, 2, 8}));
    }

    @Test void input1output0() {
        assertEquals(0, maximumXorOfTwoNumbersInAnArray.findMaximumXOR(new int[]{1}));
    }

    @Test void input2_4output6() {
        assertEquals(6, maximumXorOfTwoNumbersInAnArray.findMaximumXOR(new int[]{2, 4}));
    }

    @Test void input8_10_2output10() {
        assertEquals(10, maximumXorOfTwoNumbersInAnArray.findMaximumXOR(new int[]{8, 10, 2}));
    }

    @Test void input14_15_9_3_2output13() {
        assertEquals(13, maximumXorOfTwoNumbersInAnArray.findMaximumXOR(new int[]{14, 15, 9, 3, 2}));
    }

    @Test void input2147483647_2147483646() {
        maximumXorOfTwoNumbersInAnArray.findMaximumXOR(new int[]{2147483647,2147483646});
    }
}