package leetcode.trie;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IndexPairsOfAStringTest {

    private IndexPairsOfAString indexPairsOfAString = new IndexPairsOfAString.Solution();

    @Test void thestoryofleetcodeandme() {
        assertArrayEquals(new int[][]{{3,7},{9,13},{10,17}},
                indexPairsOfAString.indexPairs("thestoryofleetcodeandme", new String[]{"story","fleet","leetcode"}));
    }

    @Test void ababa() {
        assertArrayEquals(new int[][]{{0,1},{0,2},{2,3},{2,4}},
                indexPairsOfAString.indexPairs("ababa", new String[]{"aba","ab"}));
    }
}