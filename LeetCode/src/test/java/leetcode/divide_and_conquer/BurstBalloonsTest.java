package leetcode.divide_and_conquer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BurstBalloonsTest {

  private BurstBalloons burstBalloons = new BurstBalloons.Solution();

  @Test
  void balloons3158() {
    assertEquals(167, burstBalloons.maxCoins(new int[]{3, 1, 5, 8}));
  }

  @Test
  void balloons826() {
    System.out.println(burstBalloons.maxCoins(new int[]{8,2,6,8,9,8,1,4,1,5,3,0,7,7,0,4,2,2}));
  }
}