package leetcode.divide_and_conquer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KthLargestElementInAnArrayTest {

  private KthLargestElementInAnArray kthLargestElementInAnArray = new KthLargestElementInAnArray.Solution();

  @Test
  void k2() {
    assertEquals(5, kthLargestElementInAnArray.findKthLargest(new int[]{3, 2, 1, 5, 6, 4}, 2));
  }

  @Test
  void k4() {
    assertEquals(4, kthLargestElementInAnArray.findKthLargest(new int[]{4, 3, 4, 1, 2, 4, 5, 5, 6}, 4));
  }
}
