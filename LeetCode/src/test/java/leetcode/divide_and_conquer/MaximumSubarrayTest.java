package leetcode.divide_and_conquer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaximumSubarrayTest {

    private MaximumSubarray maximumSubarray = new MaximumSubarray.Solution();

    @Test void inputN21N34N121N54output6() {
        assertEquals(6, maximumSubarray.maxSubArray(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}));
    }

    @Test void input1output1() {
        assertEquals(1, maximumSubarray.maxSubArray(new int[]{1}));
    }

    @Test void inputN1outputN1() {
        assertEquals(-1, maximumSubarray.maxSubArray(new int[]{-1}));
    }
}