package leetcode.divide_and_conquer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SearchA2DMatrixIITest {

    private int[][] matrix = new int[][]{
            {1, 4, 7, 11, 15},
            {2, 5, 8, 12, 19},
            {3, 6, 9, 16, 22},
            {10, 13, 14, 17, 24},
            {18, 21, 23, 26, 30}
    };

    private SearchA2DMatrixII searchA2DMatrixII = new SearchA2DMatrixII.Solution();

    @Test void target5true() {
        assertTrue(searchA2DMatrixII.searchMatrix(matrix, 5));
    }

    @Test void target20false() {
        assertFalse(searchA2DMatrixII.searchMatrix(matrix, 20));
    }

    @Test void N1N1targetN2false() {
        assertFalse(searchA2DMatrixII.searchMatrix(new int[][]{{-1}, {-1}}, -2));
    }

    @Test void matrix135target0false() {
        assertFalse(searchA2DMatrixII.searchMatrix(new int[][]{{1}, {3}, {5}}, 0));
    }

    @Test void matrix1425target2true() {
        assertTrue(searchA2DMatrixII.searchMatrix(new int[][]{{1, 4}, {2, 5}}, 2));
    }

    @Test void matrix1425target4true() {
        assertTrue(searchA2DMatrixII.searchMatrix(new int[][]{{1, 4}, {2, 5}}, 4));
    }

    @Test void target15true() {
        assertTrue(searchA2DMatrixII.searchMatrix(new int[][]{
                {1,2,3,4,5},
                {6,7,8,9,10},
                {11,12,13,14,15},
                {16,17,18,19,20},
                {21,22,23,24,25}
        }, 15));
    }

    @Test void target14false() {
        assertFalse(searchA2DMatrixII.searchMatrix(new int[][]{{4,6,9,10,15},{9,12,13,15,16}}, 14));
    }

    @Test void target32true() {
        assertTrue(searchA2DMatrixII.searchMatrix(new int[][]{
                {3,5,9,9,9,11},
                {5,8,13,13,16,17},
                {10,10,14,14,16,19},
                {15,18,20,24,26,26},
                {20,24,29,32,37,41}
                }, 32));
    }

  @Test
  void matrix1425() {
    assertTrue(searchA2DMatrixII.searchMatrix(new int[][]{{1, 4}, {2, 5}}, 2));
  }
}
