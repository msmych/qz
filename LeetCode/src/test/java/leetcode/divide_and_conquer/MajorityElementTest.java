package leetcode.divide_and_conquer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MajorityElementTest {

    private MajorityElement majorityElement = new MajorityElement.Solution();

    @Test void input323output3() {
        assertEquals(3, majorityElement.majorityElement(new int[]{3, 2, 3}));
    }

    @Test void input2211122output2() {
        assertEquals(2, majorityElement.majorityElement(new int[]{2, 2, 1, 1, 1, 2, 2}));
    }
}