package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RemoveOutermostParenthesesTest {

    private RemoveOutermostParentheses removeOutermostParentheses = new RemoveOutermostParentheses.Solution();

    @Test void ooo() {
        assertEquals("()()()", removeOutermostParentheses.removeOuterParentheses("(()())(())"));
    }

    @Test void ooooo() {
        assertEquals("()()()()(())", removeOutermostParentheses.removeOuterParentheses("(()())(())(()(()))"));
    }

    @Test void oo() {
        assertEquals("", removeOutermostParentheses.removeOuterParentheses("()()"));
    }
}