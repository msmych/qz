package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RemoveAllAdjacentDuplicatesStringIITest {

    private RemoveAllAdjacentDuplicatesStringII removeAllAdjacentDuplicatesStringII = new RemoveAllAdjacentDuplicatesStringII.Solution();

    @Test
    void abcd() {
        assertEquals("abcd", removeAllAdjacentDuplicatesStringII.removeDuplicates("abcd", 2));
    }

    @Test
    void deeedbbcccbdaa() {
        assertEquals("aa", removeAllAdjacentDuplicatesStringII.removeDuplicates("deeedbbcccbdaa", 3));
    }

    @Test
    void pbbcggttciiippooaais() {
        assertEquals("ps", removeAllAdjacentDuplicatesStringII.removeDuplicates("pbbcggttciiippooaais", 2));
    }
}