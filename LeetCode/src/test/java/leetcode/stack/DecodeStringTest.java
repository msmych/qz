package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DecodeStringTest {

    private DecodeString decodeString = new DecodeString.Solution();

    @Test void s3a2bc_aaabcbc() {
        assertEquals("aaabcbc", decodeString.decodeString("3[a]2[bc]"));
    }

    @Test void s3a2c_accaccacc() {
        assertEquals("accaccacc", decodeString.decodeString("3[a2[c]]"));
    }

    @Test void s2abc3cdef_abcabccdcdcdef() {
        assertEquals("abcabccdcdcdef", decodeString.decodeString("2[abc]3[cd]ef"));
    }
}