package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EvaluateReversePolishNotationTest {

    private EvaluateReversePolishNotation evaluateReversePolishNotation = new EvaluateReversePolishNotation.Solution();

    @Test void input21plus3star() {
        assertEquals(9, evaluateReversePolishNotation.evalRPN(new String[]{"2", "1", "+", "3", "*"}));
    }

    @Test void input4_13_5slash_plus() {
        assertEquals(6, evaluateReversePolishNotation.evalRPN(new String[]{"4", "13", "5", "/", "+"}));
    }

    @Test void input10_6_9_3plusN11star_slash_star17plus5plus() {
        assertEquals(22, evaluateReversePolishNotation.evalRPN(
                new String[]{"10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"}));
    }
}