package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimplifyPathTest {

    private SimplifyPath simplifyPath = new SimplifyPath.Solution();

    @Test void home() {
        assertEquals("/home", simplifyPath.simplifyPath("/home/"));
    }

    @Test void pp() {
        assertEquals("/", simplifyPath.simplifyPath("/../"));
    }

    @Test void home_foo() {
        assertEquals("/home/foo", simplifyPath.simplifyPath("/home//foo/"));
    }

    @Test void ab__c() {
        assertEquals("/c", simplifyPath.simplifyPath("/a/./b/../../c/"));
    }

    @Test void a__b_c() {
        assertEquals("/c", simplifyPath.simplifyPath("/a/../../b/../c//.//"));
    }

    @Test void abcd() {
        assertEquals("/a/b/c", simplifyPath.simplifyPath("/a//b////c/d//././/.."));
    }
}