package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BackspaceStringCompareTest {

    private BackspaceStringCompare backspaceStringCompare = new BackspaceStringCompare.Solution();

    @Test void ab$c_ad$c_true() {
        assertTrue(backspaceStringCompare.backspaceCompare("ab#c", "ad#c"));
    }

    @Test void ab$$_c$d$_true() {
        assertTrue(backspaceStringCompare.backspaceCompare("ab##", "c#d#"));
    }

    @Test void a$$c_$a$c_true() {
        assertTrue(backspaceStringCompare.backspaceCompare("a##c", "#a#c"));
    }

    @Test void a$c_b_false() {
        assertFalse(backspaceStringCompare.backspaceCompare("a#c", "b"));
    }
}