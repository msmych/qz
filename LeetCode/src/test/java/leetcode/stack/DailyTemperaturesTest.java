package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DailyTemperaturesTest {

    private DailyTemperatures dailyTemperatures = new DailyTemperatures.Solution();

    @Test void t73_74_75_71_69_72_76_73output11421100() {
        assertArrayEquals(new int[]{1, 1, 4, 2, 1, 1, 0, 0},
                dailyTemperatures.dailyTemperatures(new int[]{73, 74, 75, 71, 69, 72, 76, 73}));
    }
}