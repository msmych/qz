package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReverseSubstringsBetweenEachPairOfParenthesesTest {

  private ReverseSubstringsBetweenEachPairOfParentheses reverseSubstringsBetweenEachPairOfParentheses =
    new ReverseSubstringsBetweenEachPairOfParentheses.Solution();

  @Test void dcba() {
    assertEquals("dcba", reverseSubstringsBetweenEachPairOfParentheses.reverseParentheses("(abcd)"));
  }

  @Test void iloveu() {
    assertEquals("iloveu", reverseSubstringsBetweenEachPairOfParentheses.reverseParentheses("(u(love)i)"));
  }

  @Test void leetcode() {
    assertEquals("leetcode", reverseSubstringsBetweenEachPairOfParentheses.reverseParentheses("(ed(et(oc))el)"));
  }

  @Test void apmnolkjihgfedcbq() {
    assertEquals("apmnolkjihgfedcbq", reverseSubstringsBetweenEachPairOfParentheses.reverseParentheses("a(bcdefghijkl(mno)p)q"));
  }

  @Test void ta() {
    assertEquals("tauswa", reverseSubstringsBetweenEachPairOfParentheses.reverseParentheses("ta()usw((((a))))"));
  }

  @Test void n() {
    assertEquals("ndyfvefltvecb", reverseSubstringsBetweenEachPairOfParentheses.reverseParentheses("n(ev(t)((()lfevf))yd)cb()"));
  }
}
