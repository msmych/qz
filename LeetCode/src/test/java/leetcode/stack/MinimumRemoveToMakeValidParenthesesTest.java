package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinimumRemoveToMakeValidParenthesesTest {

  private MinimumRemoveToMakeValidParentheses minimumRemoveToMakeValidParentheses = new MinimumRemoveToMakeValidParentheses.Solution();

  @Test
  void leetcode() {
    assertEquals("lee(t(c)o)de", minimumRemoveToMakeValidParentheses.minRemoveToMakeValid("lee(t(c)o)de)"));
  }

  @Test
  void abcd() {
    assertEquals("ab(c)d", minimumRemoveToMakeValidParentheses.minRemoveToMakeValid("a)b(c)d"));
  }

  @Test
  void __() {
    assertEquals("", minimumRemoveToMakeValidParentheses.minRemoveToMakeValid("))(("));
  }

  @Test
  void abcd4() {
    assertEquals("a(b(c)d)", minimumRemoveToMakeValidParentheses.minRemoveToMakeValid("(a(b(c)d)"));
  }

  @Test
  void v() {
    assertEquals("v(())", minimumRemoveToMakeValidParentheses.minRemoveToMakeValid("v((((((())"));
  }

  @Test
  void OO() {
    assertEquals("()()", minimumRemoveToMakeValidParentheses.minRemoveToMakeValid("())()((("));
  }
}