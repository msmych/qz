package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyStackTest {

    @Test void push1push2top_pop_empty() {
        MyStack stack = new MyStack();
        stack.push(1);
        stack.push(2);
        assertEquals(2, stack.top());
        assertEquals(2, stack.pop());
        assertFalse(stack.empty());
    }

    @Test void push1pop_empty() {
        MyStack stack = new MyStack();
        stack.push(1);
        assertEquals(1, stack.pop());
        assertTrue(stack.empty());
    }
}