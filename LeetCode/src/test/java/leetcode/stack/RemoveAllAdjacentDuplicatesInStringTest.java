package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RemoveAllAdjacentDuplicatesInStringTest {

    private RemoveAllAdjacentDuplicatesInString removeAllAdjacentDuplicatesInString =
            new RemoveAllAdjacentDuplicatesInString.Solution();

    @Test void abbaca_ca() {
        assertEquals("ca", removeAllAdjacentDuplicatesInString.removeDuplicates("abbaca"));
    }
}