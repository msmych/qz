package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TrappingRainWaterTest {

    private TrappingRainWater trappingRainWater = new TrappingRainWater.Solution();

    @Test void input010210132121output6() {
        assertEquals(6, trappingRainWater.trap(new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}));
    }

    @Test void input202output2() {
        assertEquals(2, trappingRainWater.trap(new int[]{2, 0, 2}));
    }

    @Test void input2102output3() {
        assertEquals(3, trappingRainWater.trap(new int[]{2, 1, 0, 2}));
    }

    @Test void input521215output14() {
        assertEquals(14, trappingRainWater.trap(new int[]{5, 2, 1, 2, 1, 5}));
    }

    @Test void input423output1() {
        assertEquals(1, trappingRainWater.trap(new int[]{4, 2, 3}));
    }

    @Test void input5412output1() {
        assertEquals(1, trappingRainWater.trap(new int[]{5, 4, 1, 2}));
    }

    @Test void input420325output9() {
        assertEquals(9, trappingRainWater.trap(new int[]{4, 2, 0, 3, 2, 5}));
    }
}