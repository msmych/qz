package leetcode.stack;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class NextGreaterElementITest {

    private NextGreaterElementI nextGreaterElementI = new NextGreaterElementI.Solution();

    @Test void nums412nums1342() {
        assertArrayEquals(new int[]{-1,3,-1}, nextGreaterElementI.nextGreaterElement(new int[]{4,1,2}, new int[]{1,3,4,2}));
    }

    @Test void nums24nums1234() {
        assertArrayEquals(new int[]{3,-1}, nextGreaterElementI.nextGreaterElement(new int[]{2,4}, new int[]{1,2,3,4}));
    }
}