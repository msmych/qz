package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BasicCalculatorTest {

  private BasicCalculator basicCalculator = new BasicCalculator.Solution();

  @Test
  void calc1plus1() {
    assertEquals(2, basicCalculator.calculate("1 + 1"));
  }

  @Test
  void calc2minus1plus2() {
    assertEquals(3, basicCalculator.calculate(" 2-1 + 2 "));
  }

  @Test
  void calc1p4p5p2m3p6p8() {
    assertEquals(23, basicCalculator.calculate("(1+(4+5+2)-3)+(6+8)"));
  }
}