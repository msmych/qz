package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LargestRectangleInHistogramTest {

  private LargestRectangleInHistogram largestRectangleInHistogram = new LargestRectangleInHistogram.Solution();

  @Test
  void histogram215623output10() {
    assertEquals(10, largestRectangleInHistogram.largestRectangleArea(new int[]{2, 1, 5, 6, 2, 3}));
  }

  @Test
  void histogram1output1() {
    assertEquals(1, largestRectangleInHistogram.largestRectangleArea(new int[]{1}));
  }

  @Test
  void histogram11output2() {
    assertEquals(2, largestRectangleInHistogram.largestRectangleArea(new int[]{1, 1}));
  }
}