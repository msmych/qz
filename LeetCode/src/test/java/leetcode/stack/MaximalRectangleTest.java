package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaximalRectangleTest {

  private MaximalRectangle maximalRectangle = new MaximalRectangle.Solution();

  @Test
  void matrix10100() {
    assertEquals(6, maximalRectangle.maximalRectangle(new char[][]{
      {'1', '0', '1', '0', '1'},
      {'1', '0', '1', '1', '1'},
      {'1', '1', '1', '1', '1'},
      {'1', '0', '0', '1', '0'}
    }));
  }

  @Test
  void matrix1() {
    assertEquals(1, maximalRectangle.maximalRectangle(new char[][]{{'1'}}));
  }
}