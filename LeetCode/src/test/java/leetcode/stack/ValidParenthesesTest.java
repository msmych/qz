package leetcode.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ValidParenthesesTest {

    private ValidParentheses validParentheses = new ValidParentheses.Solution();

    @Test void normal() {
        assertTrue(validParentheses.isValid("()"));
    }

    @Test void normal_square_curly() {
        assertTrue(validParentheses.isValid("()[]{}"));
    }

    @Test void house() {
        assertFalse(validParentheses.isValid("(]"));
    }

    @Test void intersection() {
        assertFalse(validParentheses.isValid("([)]"));
    }

    @Test
    void nested() {
        assertTrue(validParentheses.isValid("{[]}"));
    }
}