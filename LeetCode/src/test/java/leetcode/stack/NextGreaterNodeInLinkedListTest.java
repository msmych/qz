package leetcode.stack;

import org.junit.jupiter.api.Test;

import static leetcode.linked_list.ListNode.fromVals;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class NextGreaterNodeInLinkedListTest {

    private NextGreaterNodeInLinkedList nextGreaterNodeInLinkedList = new NextGreaterNodeInLinkedList.Solution();

    @Test void input215output550() {
        assertArrayEquals(new int[]{5, 5, 0}, nextGreaterNodeInLinkedList.nextLargerNodes(fromVals(2, 1, 5)));
    }

    @Test void input27435output70550() {
        assertArrayEquals(new int[]{7, 0, 5, 5, 0}, nextGreaterNodeInLinkedList.nextLargerNodes(fromVals(2, 7, 4, 3, 5)));
    }

    @Test void input17519251output79990500() {
        assertArrayEquals(new int[]{7,9,9,9,0,5,0,0}, nextGreaterNodeInLinkedList.nextLargerNodes(fromVals(1,7,5,1,9,2,5,1)));
    }
}