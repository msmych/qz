package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GreatestSumDivisibleByThreeTest {

  private GreatestSumDivisibleByThree greatestSumDivisibleByThree = new GreatestSumDivisibleByThree.Solution();

  @Test
  void nums36518() {
    assertEquals(18, greatestSumDivisibleByThree.maxSumDivThree(new int[]{3, 6, 5, 1, 8}));
  }

  @Test
  void nums4() {
    assertEquals(0, greatestSumDivisibleByThree.maxSumDivThree(new int[]{4}));
  }

  @Test
  void nums12344() {
    assertEquals(12, greatestSumDivisibleByThree.maxSumDivThree(new int[]{1, 2, 3, 4, 4}));
  }
}