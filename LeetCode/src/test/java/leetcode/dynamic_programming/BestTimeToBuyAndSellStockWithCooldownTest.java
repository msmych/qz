package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BestTimeToBuyAndSellStockWithCooldownTest {

  private BestTimeToBuyAndSellStockWithCooldown bestTimeToBuyAndSellStockWithCooldown =
    new BestTimeToBuyAndSellStockWithCooldown.Solution();

  @Test
  void prices12302() {
    assertEquals(3, bestTimeToBuyAndSellStockWithCooldown.maxProfit(new int[]{1, 2, 3, 0, 2}));
  }

  @Test
  void prices124() {
    assertEquals(3, bestTimeToBuyAndSellStockWithCooldown.maxProfit(new int[]{1, 2, 4}));
  }
}