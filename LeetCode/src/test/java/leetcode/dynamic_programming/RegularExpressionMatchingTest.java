package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RegularExpressionMatchingTest {

  private RegularExpressionMatching regularExpressionMatching = new RegularExpressionMatching.Solution();

  @Test
  void s_aa_p_a_false() {
    assertFalse(regularExpressionMatching.isMatch("aa", "a"));
  }

  @Test
  void s_aa_p_aS_true() {
    assertTrue(regularExpressionMatching.isMatch("aa", "a*"));
  }

  @Test
  void s_ab_p_DS_true() {
    assertTrue(regularExpressionMatching.isMatch("ab", ".*"));
  }

  @Test
  void s_aab_p_cSaSb_true() {
    assertTrue(regularExpressionMatching.isMatch("aab", "c*a*b"));
  }

  @Test
  void s_mississippi_p_misSisSpSD_false() {
    assertFalse(regularExpressionMatching.isMatch("mississippi", "mis*is*p*."));
  }
}