package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InterleavingStringTest {

    private InterleavingString interleavingString = new InterleavingString.Solution();

    @Test void aabcc_dbbca_aadbbcbcac_true() {
        assertTrue(interleavingString.isInterleave("aabcc", "dbbca", "aadbbcbcac"));
    }

    @Test void aabcc_dbbca_aadbbbaccc_false() {
        assertFalse(interleavingString.isInterleave("aabcc", "dbbca", "aadbbbaccc"));
    }

    @Test void a_c_false() {
        assertFalse(interleavingString.isInterleave("a", "", "c"));
    }

    @Test void aacaac_aacaaeaac_aacaaeaaeaacaac_false() {
        assertFalse(interleavingString.isInterleave("aacaac", "aacaaeaac", "aacaaeaaeaacaac"));
    }

    @Test void abab() {
        assertFalse(interleavingString.isInterleave(
                "baababbabbababbaaababbbbbbbbbbbaabaabaaaabaaabbaaabaaaababaabaaabaabbbbaabbaabaabbbbabbbababbaaaabab",
                "aababaaabbbababababaabbbababaababbababbbbabbbbbababbbabaaaaabaaabbabbaaabbababbaaaababaababbbbabbbbb",
                "babbabbabbababbaaababbbbaababbaabbbbabbbbbaaabbabaababaabaaabaabbbaaaabbabbaaaaabbabbaabaaaabbbbababbbababbabaabababbababaaaaaabbababaaabbaabbbbaaaaabbbaaabbbabbbbaaabaababbaabababbbbababbaaabbbabbbab"));
    }
}