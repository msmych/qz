package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HouseRobberTest {

    private HouseRobber houseRobber = new HouseRobber.Solution();

    @Test void input1231output4() {
        assertEquals(4, houseRobber.rob(new int[]{1, 2, 3, 1}));
    }

    @Test void input27931output12() {
        assertEquals(12, houseRobber.rob(new int[]{2, 7, 9, 3, 1}));
    }
}