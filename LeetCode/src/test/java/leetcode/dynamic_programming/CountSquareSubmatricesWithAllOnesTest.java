package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountSquareSubmatricesWithAllOnesTest {

  private CountSquareSubmatricesWithAllOnes countSquareSubmatricesWithAllOnes = new CountSquareSubmatricesWithAllOnes.Solution();

  @Test
  void matrix43() {
    assertEquals(15, countSquareSubmatricesWithAllOnes.countSquares(new int[][]{
      {0, 1, 1, 1},
      {1, 1, 1, 1},
      {0, 1, 1, 1}
    }));
  }

  @Test
  void matrix33() {
    assertEquals(7, countSquareSubmatricesWithAllOnes.countSquares(new int[][]{
      {1, 0, 1},
      {1, 1, 0},
      {1, 1, 0}
    }));
  }
}