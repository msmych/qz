package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LongestPalindromicSubstringTest {

    private LongestPalindromicSubstring longestPalindromicSubstring = new LongestPalindromicSubstring.Solution();

    @Test void babad_bab() {
        assertEquals("bab", longestPalindromicSubstring.longestPalindrome("babad"));
    }

    @Test void cbbd_bb() {
        assertEquals("bb", longestPalindromicSubstring.longestPalindrome("cbbd"));
    }
}