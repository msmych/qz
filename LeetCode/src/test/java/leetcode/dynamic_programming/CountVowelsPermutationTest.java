package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountVowelsPermutationTest {

  private CountVowelsPermutation countVowelsPermutation = new CountVowelsPermutation.Solution();

  @Test
  void n1() {
    assertEquals(5, countVowelsPermutation.countVowelPermutation(1));
  }

  @Test
  void n2() {
    assertEquals(10, countVowelsPermutation.countVowelPermutation(2));
  }

  @Test
  void n5() {
    assertEquals(68, countVowelsPermutation.countVowelPermutation(5));
  }

  @Test
  void n10000() {
    assertEquals(76428576, countVowelsPermutation.countVowelPermutation(10000));
  }
}
