package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UniquePathsTest {

  private UniquePaths uniquePaths = new UniquePaths.Solution();

  @Test
  void m3n2output3() {
    assertEquals(3, uniquePaths.uniquePaths(3, 2));
  }

  @Test
  void m7n3output28() {
    assertEquals(28, uniquePaths.uniquePaths(7, 3));
  }

  @Test
  void m1n1output1() {
    assertEquals(1, uniquePaths.uniquePaths(1, 1));
  }
}
