package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaximumProductSubarrayTest {

    private MaximumProductSubarray maximumProductSubarray = new MaximumProductSubarray.Solution();

    @Test void nums23N24output6() {
        assertEquals(6, maximumProductSubarray.maxProduct(new int[]{2, 3, -2, 4}));
    }

    @Test void numsN20N1output0() {
        assertEquals(0, maximumProductSubarray.maxProduct(new int[]{-2, 0, -1}));
    }

    @Test void numsN301N2output1() {
        assertEquals(1, maximumProductSubarray.maxProduct(new int[]{-3, 0, 1, -2}));
    }

    @Test void nums0N2N3output6() {
        assertEquals(6, maximumProductSubarray.maxProduct(new int[]{0, -2, -3}));
    }

    @Test void numsN2313output9() {
        assertEquals(9, maximumProductSubarray.maxProduct(new int[]{-2, 3, 1, 3}));
    }

    @Test void nums1000output1() {
        assertEquals(1, maximumProductSubarray.maxProduct(new int[]{1, 0, 0, 0}));
    }

    @Test void numsN1N110output1() {
        assertEquals(1, maximumProductSubarray.maxProduct(new int[]{-1, -1, 1, 0}));
    }

    @Test void nums0N1N2output2() {
        assertEquals(2, maximumProductSubarray.maxProduct(new int[]{0, -1, -2}));
    }

    @Test void nums0N22N110output4() {
        assertEquals(4, maximumProductSubarray.maxProduct(new int[]{0, -2, 2, -1, 1, 0}));
    }

    @Test void nums0N20output0() {
        assertEquals(0, maximumProductSubarray.maxProduct(new int[]{0, -2, 0}));
    }
}