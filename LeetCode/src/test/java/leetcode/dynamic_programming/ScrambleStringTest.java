package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ScrambleStringTest {

  private ScrambleString scrambleString = new ScrambleString.Solution();

  @Test
  void great_rgeat_true() {
    assertTrue(scrambleString.isScramble("great", "rgeat"));
  }

  @Test
  void abcde_caebd_false() {
    assertFalse(scrambleString.isScramble("abcde", "caebd"));
  }

  @Test
  void abcdefghijklmn_efghijklmncadb() {
    System.out.println(scrambleString.isScramble("abcdefghijklmn", "efghijklmncadb"));
  }
}