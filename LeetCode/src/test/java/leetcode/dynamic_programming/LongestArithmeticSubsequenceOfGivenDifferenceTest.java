package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestArithmeticSubsequenceOfGivenDifferenceTest {

  private LongestArithmeticSubsequenceOfGivenDifference longestArithmeticSubsequenceOfGivenDifference =
    new LongestArithmeticSubsequenceOfGivenDifference.Solution();

  @Test
  void arr1234() {
    assertEquals(4, longestArithmeticSubsequenceOfGivenDifference.longestSubsequence(new int[]{1, 2, 3, 4}, 1));
  }

  @Test
  void arr1357() {
    assertEquals(1, longestArithmeticSubsequenceOfGivenDifference.longestSubsequence(new int[]{1, 3, 5, 7}, 1));
  }

  @Test
  void arr157853421() {
    assertEquals(4, longestArithmeticSubsequenceOfGivenDifference.longestSubsequence(new int[]{1, 5, 7, 8, 5, 3, 4, 2, 1}, -2));
  }

  @Test
  void arr4() {
    assertEquals(2, longestArithmeticSubsequenceOfGivenDifference.longestSubsequence(new int[]{4, 12, 10, 0, -2, 7, -8, 9, -9, -12, -12, 8, 8}, 0));
  }

  @Test
  void arr34N3N2N4() {
    assertEquals(2, longestArithmeticSubsequenceOfGivenDifference.longestSubsequence(new int[]{3, 4, -3, -2, -4}, -5));
  }
}
