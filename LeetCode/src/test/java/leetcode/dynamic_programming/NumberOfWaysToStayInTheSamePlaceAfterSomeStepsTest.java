package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfWaysToStayInTheSamePlaceAfterSomeStepsTest {

  private NumberOfWaysToStayInTheSamePlaceAfterSomeSteps numberOfWaysToStayInTheSamePlaceAfterSomeSteps =
    new NumberOfWaysToStayInTheSamePlaceAfterSomeSteps.Solution();

  @Test
  void steps3len2() {
    assertEquals(4, numberOfWaysToStayInTheSamePlaceAfterSomeSteps.numWays(3, 2));
  }

  @Test
  void steps2len4() {
    assertEquals(2, numberOfWaysToStayInTheSamePlaceAfterSomeSteps.numWays(2, 4));
  }

  @Test
  void steps4len2() {
    assertEquals(8, numberOfWaysToStayInTheSamePlaceAfterSomeSteps.numWays(4, 2));
  }

  @Test
  void steps27len7() {
    assertEquals(127784505, numberOfWaysToStayInTheSamePlaceAfterSomeSteps.numWays(27, 7));
  }
}