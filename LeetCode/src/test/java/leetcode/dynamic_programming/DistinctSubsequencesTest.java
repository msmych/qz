package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DistinctSubsequencesTest {

    private DistinctSubsequences distinctSubsequences = new DistinctSubsequences.Solution();

    @Test void rabbbit_rabbit3() {
        assertEquals(3, distinctSubsequences.numDistinct("rabbbit", "rabbit"));
    }

    @Test void babgbag_bag5() {
        assertEquals(5, distinctSubsequences.numDistinct("babgbag", "bag"));
    }
}