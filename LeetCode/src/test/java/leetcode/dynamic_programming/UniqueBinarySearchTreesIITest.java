package leetcode.dynamic_programming;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import java.util.List;

import static leetcode.tree.TreeNode.*;
import static org.junit.jupiter.api.Assertions.*;

class UniqueBinarySearchTreesIITest {

    private UniqueBinarySearchTreesII uniqueBinarySearchTreesII = new UniqueBinarySearchTreesII.Solution();

    @Test void input3() {
        List<TreeNode> trees = uniqueBinarySearchTreesII.generateTrees(3);
        assertContainsAll(trees,
                fromValAndLeftAndRight(1, null, fromValAndLeft(3, new TreeNode(2))),
                fromValAndLeft(3, fromValAndLeft(2, new TreeNode(1))),
                fromValAndLeft(3, fromValAndLeftAndRight(1, null, new TreeNode(2))),
                fromValAndLeftAndRight(2, new TreeNode(1), new TreeNode(3)),
                fromValAndLeftAndRight(1, null, fromValAndLeftAndRight(2, null, new TreeNode(3))));
    }

    private void assertContainsAll(List<TreeNode> treeList, TreeNode... trees) {
        for (TreeNode tree : trees) {
            assertTrue(treeList.stream()
                    .anyMatch(t -> serialize(t).equals(serialize(tree))));
        }
    }
}