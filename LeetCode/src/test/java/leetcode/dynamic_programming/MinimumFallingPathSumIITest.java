package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinimumFallingPathSumIITest {

  private MinimumFallingPathSumII minimumFallingPathSumII = new MinimumFallingPathSumII.Solution();

  @Test
  void arr123456789() {
    assertEquals(13, minimumFallingPathSumII.minFallingPathSum(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}));
  }

  @Test
  void arr2212222122221222212222122() {
    assertEquals(7, minimumFallingPathSumII.minFallingPathSum(new int[][]{
      {2, 2, 1, 2, 2},
      {2, 2, 1, 2, 2},
      {2, 2, 1, 2, 2},
      {2, 2, 1, 2, 2},
      {2, 2, 1, 2, 2}
    }));
  }

  @Test
  void arrN73() {
    assertEquals(-192, minimumFallingPathSumII.minFallingPathSum(new int[][]{
      {-73, 61, 43, -48, -36},
      {3, 30, 27, 57, 10},
      {96, -76, 84, 59, -15},
      {5, -49, 76, 31, -7},
      {97, 91, 61, -46, 67}
    }));
  }
}