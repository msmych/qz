package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TossStrangeCoinsTest {

  private TossStrangeCoins tossStrangeCoins = new TossStrangeCoins.Solution();

  @Test
  void prob4() {
    assertEquals(0.4, tossStrangeCoins.probabilityOfHeads(new double[]{0.4}, 1));
  }

  @Test
  void prob5() {
    assertEquals(0.03125, tossStrangeCoins.probabilityOfHeads(new double[]{0.5, 0.5, 0.5, 0.5, 0.5}, 0));
  }

  @Test
  void prob1111111111() {
    assertEquals(0.0, tossStrangeCoins.probabilityOfHeads(new double[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 9));
  }

  @Test
  void prob28035() {
    assertEquals(0.182, tossStrangeCoins.probabilityOfHeads(new double[]{0.2, 0.8, 0, 0.3, 0.5}, 3));
  }
}
