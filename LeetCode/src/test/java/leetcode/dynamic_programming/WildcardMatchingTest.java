package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WildcardMatchingTest {

  private WildcardMatching wildcardMatching = new WildcardMatching.Solution();

  @Test
  void s_aa_p_a_false() {
    assertFalse(wildcardMatching.isMatch("aa", "a"));
  }

  @Test
  void s_aa_p_S_true() {
    assertTrue(wildcardMatching.isMatch("aa", "*"));
  }

  @Test
  void s_cb_p_$a_false() {
    assertFalse(wildcardMatching.isMatch("cb", "?a"));
  }

  @Test
  void s_adceb_p_SaSb_true() {
    assertTrue(wildcardMatching.isMatch("adceb", "*a*b"));
  }

  @Test
  void s_acdcb_p_aSc$b_false() {
    assertFalse(wildcardMatching.isMatch("acdcb", "a*c?b"));
  }

  @Test
  void mississippi_m$$SssS$iSpi() {
    assertFalse(wildcardMatching.isMatch("mississippi", "m??*ss*?i*pi"));
  }

  @Test
  void aaabbbaabaaaaababaabaaabbabbbbbbbbaabababbabbbaaaaba_aSSSSSSSb_false() {
    assertFalse(wildcardMatching.isMatch("aaabbbaabaaaaababaabaaabbabbbbbbbbaabababbabbbaaaaba",
      "a*******b"));
  }

  @Test
  void bbbaaabaababbabbbaabababbbabababaabbaababbbabbbabb_SbSSbSSSbabaSSSaaaSbSSS() {
    assertFalse(wildcardMatching.isMatch("bbbaaabaababbabbbaabababbbabababaabbaababbbabbbabb",
      "*b**b***baba***aaa*b***"));
  }

  @Test
  void abefcdgiescdfimde_abScd$iSde_true() {
    assertTrue(wildcardMatching.isMatch("abefcdgiescdfimde", "ab*cd?i*de"));
  }

  @Test
  void bbaaaabaaaaabbabbabbabbababaabababaabbabaaabbaababababbabaabbabbbbbbaaaaaabaabbbbbabbbbabbabababaaaaa() {
    assertFalse(wildcardMatching.isMatch(
      "bbaaaabaaaaabbabbabbabbababaabababaabbabaaabbaababababbabaabbabbbbbbaaaaaabaabbbbbabbbbabbabababaaaaa",
      "******aa*bbb*aa*a*bb*ab***bbba*a*babaab*b*aa*a****"));
  }

  @Test
  void _$_false() {
    assertFalse(wildcardMatching.isMatch("", "?"));
  }

  @Test
  void aaaabaabaabbbabaabaabbbbaabaaabaaabbabbbaaabbbbbbabababbaabbabbbbaababaaabbbababbbaabbbaabbaaabbbaabb() {
    assertFalse(wildcardMatching.isMatch(
      "aaaabaabaabbbabaabaabbbbaabaaabaaabbabbbaaabbbbbbabababbaabbabbbbaababaaabbbababbbaabbbaabbaaabbbaabbbbbaaaabaaabaabbabbbaabababbaabbbabababbaabaaababbbbbabaababbbabbabaaaaaababbbbaabbbbaaababbbbaabbbbb",
      "**a*b*b**b*b****bb******b***babaab*ba*a*aaa***baa****b***bbbb*bbaa*a***a*a*****a*b*a*a**ba***aa*a**a*"));
  }

  @Test
  void ba_S$b$$_false() {
    assertFalse(wildcardMatching.isMatch("ba", "*?b??"));
  }
}