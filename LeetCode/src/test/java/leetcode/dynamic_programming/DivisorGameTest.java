package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DivisorGameTest {

    private DivisorGame divisorGame = new DivisorGame.Solution();

    @Test void input2true() {
        assertTrue(divisorGame.divisorGame(2));
    }

    @Test void input3false() {
        assertFalse(divisorGame.divisorGame(3));
    }

    @Test void input9false() {
        assertFalse(divisorGame.divisorGame(9));
    }

    @Test void input4true() {
        assertTrue(divisorGame.divisorGame(4));
    }
}