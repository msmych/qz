package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShortestCommonSupersequenceTest {

    private ShortestCommonSupersequence shortestCommonSupersequence = new ShortestCommonSupersequence.Solution();

    @Test void abac_cab_cabac() {
        assertEquals("cabac", shortestCommonSupersequence.shortestCommonSupersequence("abac", "cab"));
    }
}