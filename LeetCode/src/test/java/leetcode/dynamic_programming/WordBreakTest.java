package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WordBreakTest {

  private WordBreak wordBreak = new WordBreak.Solution();

  @Test
  void leetcode_true() {
    assertTrue(wordBreak.wordBreak("leetcode", asList("leet", "code")));
  }

  @Test
  void applepenapple_true() {
    assertTrue(wordBreak.wordBreak("applepenapple", asList("apple", "pen")));
  }

  @Test
  void catsandog_false() {
    assertFalse(wordBreak.wordBreak("catsandog", asList("cats", "dog", "sand", "and", "cat")));
  }
}