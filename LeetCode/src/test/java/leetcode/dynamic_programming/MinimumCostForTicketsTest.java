package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinimumCostForTicketsTest {

    private MinimumCostForTickets minimumCostForTickets = new MinimumCostForTickets.Solution();

    @Test void days1_4_6_7_8_20costs2_7_15output11() {
        assertEquals(11,
                minimumCostForTickets.mincostTickets(new int[]{1, 4, 6, 7, 8, 20}, new int[]{2, 7, 15}));
    }

    @Test void days1_2_3_4_5_6_7_8_9_10_30_31costs2_7_15output17() {
        assertEquals(17, minimumCostForTickets.mincostTickets(
                new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 30, 31},
                new int[]{2, 7, 15}));
    }

    @Test void days2to99() {
        assertEquals(246, minimumCostForTickets.mincostTickets(
                new int[]{2,3,5,6,7,8,9,10,11,17,18,19,23,26,27,29,31,32,33,34,35,36,38,39,40,41,42,43,44,45,47,51,54,
                        55,57,58,64,65,67,68,72,73,74,75,77,78,81,86,87,88,89,91,93,94,95,96,98,99},
                new int[]{5,24,85}));
    }
}