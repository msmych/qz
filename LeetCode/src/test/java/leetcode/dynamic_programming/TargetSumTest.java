package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TargetSumTest {

    private TargetSum targetSum = new TargetSum.Solution();

    @Test void nums11111s3output5() {
        assertEquals(5, targetSum.findTargetSumWays(new int[]{1, 1, 1, 1, 1}, 3));
    }

    @Test void nums1s2output0() {
        assertEquals(0, targetSum.findTargetSumWays(new int[]{1}, 2));
    }
}