package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClimbingStairsTest {

    private ClimbingStairs climbingStairs = new ClimbingStairs.Solution(),
            fibonacci = new ClimbingStairs.FibonacciSolution();

    @Test void input2output2() {
        assertEquals(2, climbingStairs.climbStairs(2));
        assertEquals(2, fibonacci.climbStairs(2));
    }

    @Test void input3output3() {
        assertEquals(3, climbingStairs.climbStairs(3));
        assertEquals(3, fibonacci.climbStairs(3));
    }
}