package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinCostClimbingStairsTest {

    private MinCostClimbingStairs minCostClimbingStairs = new MinCostClimbingStairs.Solution();

    @Test void cost10_15_20output15() {
        assertEquals(15, minCostClimbingStairs.minCostClimbingStairs(new int[]{10, 15, 20}));
    }

    @Test void cost1_100_1_1_1_100_1_1_100_1output6() {
        assertEquals(6,
                minCostClimbingStairs.minCostClimbingStairs(new int[]{1, 100, 1, 1, 1, 100, 1, 1, 100, 1}));
    }
}