package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TriangleTest {

    private Triangle triangle = new Triangle.Solution();

    @Test void triangle11() {
        assertEquals(11, triangle.minimumTotal(asList(
                singletonList(2),
                asList(3, 4),
                asList(6, 5, 7),
                asList(4, 1, 8, 3))));
    }

    @Test void triangleN10() {
        assertEquals(-10, triangle.minimumTotal(singletonList(singletonList(-10))));
    }

    @Test void triangleN1() {
        assertEquals(-1, triangle.minimumTotal(asList(
                singletonList(-1),
                asList(2, 3),
                asList(1, -1, -3))));
    }
}