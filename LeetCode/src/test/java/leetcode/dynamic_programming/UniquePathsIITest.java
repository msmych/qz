package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UniquePathsIITest {

    private UniquePathsII uniquePathsII = new UniquePathsII.Solution();

    @Test void grid000_010_000output2() {
        assertEquals(2, uniquePathsII.uniquePathsWithObstacles(new int[][]{
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}
        }));
    }

    @Test void grid00output1() {
        assertEquals(1, uniquePathsII.uniquePathsWithObstacles(new int[][]{{0, 0}}));
    }

    @Test void grid01output0() {
        assertEquals(0, uniquePathsII.uniquePathsWithObstacles(new int[][]{{0, 1}}));
    }
}