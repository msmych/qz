package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinimumPathSumTest {

    private MinimumPathSum minimumPathSum = new MinimumPathSum.Solution();

    @Test void grid131_151_421output7() {
        assertEquals(7, minimumPathSum.minPathSum(new int[][]{
                {1, 3, 1},
                {1, 5, 1},
                {4, 2, 1}
        }));
    }
}