package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfPathsWithMaxScoreTest {

  private NumberOfPathsWithMaxScore numberOfPathsWithMaxScore = new NumberOfPathsWithMaxScore.Solution();

  @Test
  void E232X212S() {
    assertArrayEquals(new int[]{7, 1}, numberOfPathsWithMaxScore.pathsWithMaxScore(Arrays.asList("E23", "2X2", "12S")));
  }

  @Test
  void E121X121S() {
    assertArrayEquals(new int[]{4, 2}, numberOfPathsWithMaxScore.pathsWithMaxScore(Arrays.asList("E12", "1X1", "21S")));
  }

  @Test
  void E11XXX11S() {
    assertArrayEquals(new int[]{0, 0}, numberOfPathsWithMaxScore.pathsWithMaxScore(Arrays.asList("E11", "XXX", "11S")));
  }
}