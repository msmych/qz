package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DungeonGameTest {

    private DungeonGame dungeonGame = new DungeonGame.Solution();

    @Test void kN2pN5output7() {
        assertEquals(7, dungeonGame.calculateMinimumHP(new int[][]{
                {-2, -3, 3},
                {-5, -10, 1},
                {10, 30, -5}
        }));
    }
}