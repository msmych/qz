package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoinChangeTest {

  private CoinChange coinChange = new CoinChange.Solution();

  @Test
  void coins125amount11() {
    assertEquals(3, coinChange.coinChange(new int[]{1, 2, 5}, 11));
  }

  @Test
  void coins2amount3() {
    assertEquals(-1, coinChange.coinChange(new int[]{2}, 3));
  }

  @Test
  void coins186() {
    assertEquals(20, coinChange.coinChange(new int[]{186, 419, 83, 408}, 6249));
  }

  @Test
  void coins27() {
    assertEquals(15, coinChange.coinChange(new int[]{27, 398, 90, 323, 454, 413, 70, 315}, 6131));
  }

  @Test
  void coins55() {
    assertEquals(14, coinChange.coinChange(new int[]{55, 319, 101, 494, 162, 58, 295, 253}, 5780));
  }

  @Test
  void coins492() {
    assertEquals(12, coinChange.coinChange(new int[]{492, 364, 366, 144, 492, 316, 221, 326, 16, 166, 353}, 5253));
  }

  @Test
  void coins333() {
    assertEquals(15, coinChange.coinChange(new int[]{333, 364, 408, 118, 63, 270, 69, 111, 218, 371, 305}, 5615));
  }
}
