package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HouseRobberIITest {

    private HouseRobber houseRobber = new HouseRobber.SolutionII();

    @Test void input232output3() {
        assertEquals(3, houseRobber.rob(new int[]{2, 3, 2}));
    }

    @Test void input1231output4() {
        assertEquals(4, houseRobber.rob(new int[]{1, 2, 3, 1}));
    }

    @Test void input1output1() {
        assertEquals(1, houseRobber.rob(new int[]{1}));
    }
}