package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DiceRollSimulationTest {

  private DiceRollSimulation diceRollSimulation = new DiceRollSimulation.Solution();

  @Test
  void n2roll112223() {
    assertEquals(34, diceRollSimulation.dieSimulator(2, new int[]{1, 1, 2, 2, 2, 3}));
  }

  @Test
  void n2roll111111() {
    assertEquals(30, diceRollSimulation.dieSimulator(2, new int[]{1, 1, 1, 1, 1, 1}));
  }

  @Test
  void n3roll111223() {
    assertEquals(181, diceRollSimulation.dieSimulator(3, new int[]{1, 1, 1, 2, 2, 3}));
  }
}
