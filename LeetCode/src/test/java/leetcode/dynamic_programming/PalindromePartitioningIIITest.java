package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PalindromePartitioningIIITest {

  private PalindromePartitioningIII palindromePartitioningIII = new PalindromePartitioningIII.Solution();

  @Test
  void abc2() {
    assertEquals(1, palindromePartitioningIII.palindromePartition("abc", 2));
  }

  @Test
  void aabbc3() {
    assertEquals(0, palindromePartitioningIII.palindromePartition("aabbc", 3));
  }

  @Test
  void leetcode8() {
    assertEquals(0, palindromePartitioningIII.palindromePartition("leetcode", 8));
  }

  @Test
  void oiwwhqjkb() {
    assertEquals(4, palindromePartitioningIII.palindromePartition("oiwwhqjkb",1));
  }
}