package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DecodeWaysTest {

  private DecodeWays decodeWays = new DecodeWays.Solution();

  @Test
  void input12output2() {
    assertEquals(2, decodeWays.numDecodings("12"));
  }

  @Test
  void input226output3() {
    assertEquals(3, decodeWays.numDecodings("226"));
  }

  @Test
  void input10output1() {
    assertEquals(1, decodeWays.numDecodings("10"));
  }

  @Test
  void input27output1() {
    assertEquals(1, decodeWays.numDecodings("27"));
  }

  @Test
  void input00output0() {
    assertEquals(0, decodeWays.numDecodings("00"));
  }

  @Test
  void input01output0() {
    assertEquals(0, decodeWays.numDecodings("01"));
  }

  @Test
  void input230output0() {
    assertEquals(0, decodeWays.numDecodings("230"));
  }

  @Test
  void input301output0() {
    assertEquals(0, decodeWays.numDecodings("301"));
  }

  @Test
  void input611output2() {
    assertEquals(2, decodeWays.numDecodings("611"));
  }
}