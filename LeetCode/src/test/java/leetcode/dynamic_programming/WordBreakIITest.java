package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static utils.TestUtils.assertCollectionContainsAll;

class WordBreakIITest {

  private WordBreakII wordBreakII = new WordBreakII.Solution();

  @Test
  void catsanddog() {
    assertCollectionContainsAll(
      wordBreakII.wordBreak("catsanddog", asList("cat", "cats", "and", "sand", "dog")),
      "cats and dog", "cat sand dog");
  }

  @Test
  void pineapplepenapple() {
    assertCollectionContainsAll(
      wordBreakII.wordBreak("pineapplepenapple", asList("apple", "pen", "applepen", "pine", "pineapple")),
      "pine apple pen apple", "pineapple pen apple", "pine applepen apple");
  }

  @Test
  void catsandog() {
    assertTrue(wordBreakII.wordBreak("catsandog", asList("cats", "dog", "sand", "and", "cat")).isEmpty());
  }
}