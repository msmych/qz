package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VideoStitchingTest {

    private VideoStitching videoStitching = new VideoStitching.Solution();

    @Test void t10output3() {
        assertEquals(3, videoStitching.videoStitching(new int[][]{{0,2},{4,6},{8,10},{1,9},{1,5},{5,9}}, 10));
    }

    @Test void t5outputN1() {
        assertEquals(-1, videoStitching.videoStitching(new int[][]{{0,1},{1,2}}, 5));
    }

    @Test void t9output3() {
        assertEquals(3, videoStitching.videoStitching(
                new int[][]{{0,1},{6,8},{0,2},{5,6},{0,4},{0,3},{6,7},{1,3},{4,7},{1,4},{2,5},{2,6},{3,4},{4,5},{5,7},{6,9}}, 9));
    }

    @Test void t5output2() {
        assertEquals(2, videoStitching.videoStitching(new int[][]{{0,4},{2,8}}, 5));
    }
}