package leetcode.dynamic_programming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UniqueBinarySearchTreesTest {

    private UniqueBinarySearchTrees uniqueBinarySearchTrees = new UniqueBinarySearchTrees.Solution();

    @Test void input3output5() {
        assertEquals(5, uniqueBinarySearchTrees.numTrees(3));
    }

    @Test void input19output1767263190() {
        assertEquals(1767263190, uniqueBinarySearchTrees.numTrees(19));
    }
}