package leetcode.sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidAnagramTest {

    private ValidAnagram validAnagram = new ValidAnagram.Solution();

    @Test void anagram_nagaram_true() {
        assertTrue(validAnagram.isAnagram("anagram", "nagaram"));
    }

    @Test void rat_car_false() {
        assertFalse(validAnagram.isAnagram("rat", "car"));
    }
}