package leetcode.sort;

import leetcode.Interval;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.singletonList;
import static leetcode.Interval.intervalEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InsertIntervalTest {

    private InsertInterval insertInterval = new InsertInterval.Solution();

    @Test void intervals13_69new25output15_69() {
        List<Interval> merged = insertInterval.insert(
                Arrays.asList(new Interval(1, 3), new Interval(6, 9)),
                new Interval(2, 5));
        assertEquals(2, merged.size());
        assertTrue(intervalEquals(1, 5, merged.get(0)));
        assertTrue(intervalEquals(6, 9, merged.get(1)));
    }

    @Test void intervals1$2_3$5_6$7_8$10_12$16new4$8output1$2_3$10_12$16() {
        List<Interval> merged = insertInterval.insert(
                Arrays.asList(new Interval(1, 2), new Interval(3, 5), new Interval(6, 7),
                        new Interval(8, 10), new Interval(12, 16)),
                new Interval(4, 8));
        assertEquals(3, merged.size());
        assertTrue(intervalEquals(1, 2, merged.get(0)));
        assertTrue(intervalEquals(3, 10, merged.get(1)));
        assertTrue(intervalEquals(12, 16, merged.get(2)));
    }

    @Test void intervals15new27output17() {
        List<Interval> merged = insertInterval.insert(singletonList(new Interval(1, 5)), new Interval(2, 7));
        assertEquals(1, merged.size());
        assertTrue(intervalEquals(1, 7, merged.get(0)));
    }

    @Test void intervals15new03output05() {
        List<Interval> merged = insertInterval.insert(singletonList(new Interval(1, 5)), new Interval(0, 3));
        assertEquals(1, merged.size());
        assertTrue(intervalEquals(0, 5, merged.get(0)));
    }

    @Test void intervals3$5_12$15new6$6output3$5_6$6_12$15() {
        List<Interval> merged = insertInterval.insert(
                Arrays.asList(new Interval(3, 5), new Interval(12, 15)), new Interval(6, 6));
        assertEquals(3, merged.size());
        assertTrue(intervalEquals(3, 5, merged.get(0)));
        assertTrue(intervalEquals(6, 6, merged.get(1)));
        assertTrue(intervalEquals(12, 15, merged.get(2)));
    }

    @Test void intervals2$7_8$8_10$10_12$13_16$19new9$17output2$7_8$8_9$19() {
        List<Interval> merged = insertInterval.insert(
                Arrays.asList(new Interval(2, 7), new Interval(8, 8), new Interval(10, 10),
                        new Interval(12, 13), new Interval(16, 19)),
                new Interval(9, 17));
        assertEquals(3, merged.size());
        assertTrue(intervalEquals(2, 7, merged.get(0)));
        assertTrue(intervalEquals(8, 8, merged.get(1)));
        assertTrue(intervalEquals(9, 19, merged.get(2)));
    }
}