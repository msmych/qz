package leetcode.sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static utils.TestUtils.arrayEquals;

class MatrixCellsInDistanceOrderTest {

    private MatrixCellsInDistanceOrder matrixCellsInDistanceOrder = new MatrixCellsInDistanceOrder.Solution();

    @Test void r1c2r0c0() {
        assertArrayEquals(new int[][]{{0, 0}, {0, 1}}, matrixCellsInDistanceOrder.allCellsDistOrder(1, 2, 0, 0));
    }

    @Test
    void r2c2r0c1() {
        int[][] matrix = matrixCellsInDistanceOrder.allCellsDistOrder(2, 2, 0, 1);
        assertTrue(arrayEquals(new int[][]{{0, 1}, {0, 0}, {1, 1}, {1, 0}}, matrix) ||
                arrayEquals(new int[][]{{0, 1}, {1, 1}, {0, 0}, {1, 0}}, matrix));
    }

    @Test void r2c3r1c2() {
        int[][] matrix = matrixCellsInDistanceOrder.allCellsDistOrder(2, 3, 1, 2);
        assertTrue((arrayEquals(new int[][]{{1, 2}, {0, 2}, {1, 1}, {0, 1}, {1, 0}, {0, 0}}, matrix) ||
                arrayEquals(new int[][]{{1, 2}, {1, 1}, {0, 2}, {0, 1}, {1, 0}, {0, 0}}, matrix)));
    }
}