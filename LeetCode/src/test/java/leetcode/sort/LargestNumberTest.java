package leetcode.sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LargestNumberTest {

    private LargestNumber largestNumber = new LargestNumber.Solution();

    @Test void input10_2output210() {
        assertEquals("210", largestNumber.largestNumber(new int[]{10, 2}));
    }

    @Test void input3_30_34_5_9output9534330() {
        assertEquals("9534330", largestNumber.largestNumber(new int[]{3, 30, 34, 5, 9}));
    }

    @Test void input10_2_3_3_30_300_91output913330300210() {
        assertEquals("913330300210", largestNumber.largestNumber(new int[]{10,2,3,3,30,300,91}));
    }

    @Test void input121_12output12121() {
        assertEquals("12121", largestNumber.largestNumber(new int[]{121, 12}));
    }
}