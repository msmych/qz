package leetcode.sort;

import org.junit.jupiter.api.Test;

import static leetcode.linked_list.ListNode.equalVals;
import static leetcode.linked_list.ListNode.fromVals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InsertionSortListTest {

    private InsertionSortList insertionSortList = new InsertionSortList.Solution();

    @Test void input4213() {
        assertTrue(equalVals(insertionSortList.insertionSortList(fromVals(4, 2, 1, 3)), 1, 2, 3, 4));
    }

    @Test void inputN15340() {
        assertTrue(equalVals(insertionSortList.insertionSortList(fromVals(-1, 5, 3, 4, 0)), -1, 0, 3, 4, 5));
    }
}