package leetcode.sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaximumGapTest {

    private MaximumGap maximumGap = new MaximumGap.Solution();

    @Test void input3691output3() {
        assertEquals(3, maximumGap.maximumGap(new int[]{3, 6, 9, 1}));
    }

    @Test void input10output0() {
        assertEquals(0, maximumGap.maximumGap(new int[]{10}));
    }

    @Test void input1_10000000output9999999() {
        assertEquals(9999999, maximumGap.maximumGap(new int[]{1, 10_000_000}));
    }
}