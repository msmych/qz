package leetcode.sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RelativeRanksTest {

    private RelativeRanks relativeRanks = new RelativeRanks.Solution();

    @Test void input54321() {
        assertArrayEquals(new String[]{"Gold Medal", "Silver Medal", "Bronze Medal", "4", "5"},
                relativeRanks.findRelativeRanks(new int[]{5, 4, 3, 2, 1}));
    }
}