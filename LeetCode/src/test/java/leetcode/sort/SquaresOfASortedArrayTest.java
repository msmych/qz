package leetcode.sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquaresOfASortedArrayTest {

    private SquaresOfASortedArray squaresOfASortedArray = new SquaresOfASortedArray.Solution();

    @Test void inputN4_N1_0_3_10output0_1_9_16_100() {
        assertArrayEquals(new int[]{0, 1, 9, 16, 100},
                squaresOfASortedArray.sortedSquares(new int[]{-4, -1, 0, 3, 10}));
    }

    @Test void inputN7_N3_2_3_11output4_9_9_49_121() {
        assertArrayEquals(new int[]{4, 9, 9, 49, 121},
                squaresOfASortedArray.sortedSquares(new int[]{-7, -3, 2, 3, 11}));
    }
}