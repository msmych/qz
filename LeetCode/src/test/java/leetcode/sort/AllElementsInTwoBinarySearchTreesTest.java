package leetcode.sort;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class AllElementsInTwoBinarySearchTreesTest {

  private AllElementsInTwoBinarySearchTrees allElementsInTwoBinarySearchTrees = new AllElementsInTwoBinarySearchTrees.Solution();

  @Test
  void root214root103() {
    assertArrayEquals(new Integer[]{0, 1, 1, 2, 3, 4}, allElementsInTwoBinarySearchTrees.getAllElements(
      fromValAndLeftAndRight(2, new TreeNode(1), new TreeNode(4)),
      fromValAndLeftAndRight(1, new TreeNode(0), new TreeNode(3)))
      .toArray(new Integer[]{}));
  }

  @Test
  void root0N1010root51702() {
    assertArrayEquals(new Integer[]{-10, 0, 0, 1, 2, 5, 7, 10}, allElementsInTwoBinarySearchTrees.getAllElements(
      fromValAndLeftAndRight(0, new TreeNode(-10), new TreeNode(10)),
      fromValAndLeftAndRight(5, fromValAndLeftAndRight(1, new TreeNode(0), new TreeNode(2)), new TreeNode(7)))
      .toArray(new Integer[]{}));
  }

  @Test
  void root_root51702() {
    assertArrayEquals(new Integer[]{0, 1, 2, 5, 7}, allElementsInTwoBinarySearchTrees.getAllElements(null,
      fromValAndLeftAndRight(0, fromValAndLeftAndRight(1, new TreeNode(5), new TreeNode(7)), new TreeNode(2))
    ).toArray(new Integer[]{}));
  }

  @Test
  void root0N1010root_() {
    assertArrayEquals(new Integer[]{-10, 0, 10}, allElementsInTwoBinarySearchTrees.getAllElements(
      fromValAndLeftAndRight(0, new TreeNode(-10), new TreeNode(10)),
      null
    ).toArray(new Integer[]{}));
  }

  @Test
  void root1_8root81() {
    assertArrayEquals(new Integer[]{1, 1, 8, 8}, allElementsInTwoBinarySearchTrees.getAllElements(
      fromValAndLeftAndRight(1, null, new TreeNode(8)),
      fromValAndLeft(8, new TreeNode(1))).toArray(new Integer[]{}));
  }
}