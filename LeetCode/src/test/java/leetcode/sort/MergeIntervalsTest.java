package leetcode.sort;

import leetcode.Interval;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static leetcode.Interval.intervalEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MergeIntervalsTest {

    private MergeIntervals mergeIntervals = new MergeIntervals.Solution();

    @Test
    void input1$3_2$6_8$10_15$18_output1$6_8$10_15$18() {
        List<Interval> merged = mergeIntervals.merge(Arrays.asList(
                new Interval(1, 3),
                new Interval(2, 6),
                new Interval(8, 10),
                new Interval(15, 18)));
        assertEquals(3, merged.size());
        assertTrue(intervalEquals(1, 6, merged.get(0)));
        assertTrue(intervalEquals(8, 10, merged.get(1)));
        assertTrue(intervalEquals(15, 18, merged.get(2)));
    }

    @Test void input14_45output15() {
        List<Interval> merged = mergeIntervals.merge(Arrays.asList(new Interval(1, 4), new Interval(4, 5)));
        assertEquals(1, merged.size());
        assertTrue(intervalEquals(1, 5, merged.get(0)));
    }

    @Test void input14_02_35output05() {
        List<Interval> merged = mergeIntervals.merge(
                Arrays.asList(new Interval(1, 4), new Interval(0, 2), new Interval(3, 5)));
        assertEquals(1, merged.size());
        assertTrue(intervalEquals(0, 5, merged.get(0)));
    }

    @Test void input45_14_01output05() {
        List<Interval> merged = mergeIntervals.merge(
                Arrays.asList(new Interval(4, 5), new Interval(1, 4), new Interval(0, 1)));
        assertEquals(1, merged.size());
        assertTrue(intervalEquals(0, 5, merged.get(0)));
    }
}