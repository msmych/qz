package leetcode.sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WiggleSortIITest {

  private WiggleSortII wiggleSortII = new WiggleSortII.Solution();

  @Test
  void nums151164() {
    int[] nums = new int[]{1, 5, 1, 1, 6, 4};
    wiggleSortII.wiggleSort(nums);
    assertArrayEquals(new int[]{1, 5, 1, 6, 1, 4}, nums);
  }

  @Test
  void nums132231() {
    int[] nums = new int[]{1, 3, 2, 2, 3, 1};
    wiggleSortII.wiggleSort(nums);
    assertArrayEquals(new int[]{2, 3, 1, 3, 1, 2}, nums);
  }

  @Test
  void nums1121221() {
    int[] nums = new int[]{1, 1, 2, 1, 2, 2, 1};
    wiggleSortII.wiggleSort(nums);
    assertArrayEquals(new int[]{1, 2, 1, 2, 1, 2, 1}, nums);
  }

  @Test
  void nums6541111() {
    int[] nums = new int[]{6, 5, 4, 1, 1, 1, 1};
    wiggleSortII.wiggleSort(nums);
    assertArrayEquals(new int[]{1, 5, 1, 6, 1, 4, 1}, nums);
  }

  @Test
  void nums6541234() {
    int[] nums = new int[]{6, 5, 4, 1, 2, 3, 4};
    wiggleSortII.wiggleSort(nums);
    assertArrayEquals(new int[]{4, 5, 2, 6, 3, 4, 1}, nums);
  }

  @Test
  void nums1() {
    int[] nums = new int[]{1};
    wiggleSortII.wiggleSort(nums);
    assertArrayEquals(new int[]{1}, nums);
  }

  @Test
  void nums121211221() {
    int[] nums = new int[]{1, 2, 1, 2, 1, 1, 2, 2, 1};
    wiggleSortII.wiggleSort(nums);
    assertArrayEquals(new int[]{1, 2, 1, 2, 1, 2, 1, 2, 1}, nums);
  }

  @Test
  void nums12212111221212112() {
    int[] nums = new int[]{1, 2, 2, 1, 2, 1, 1, 1, 2, 2, 1, 2, 1, 2, 1, 1, 2};
    wiggleSortII.wiggleSort(nums);
    assertArrayEquals(new int[]{1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1}, nums);
  }

  @Test
  void nums4556() {
    int[] nums = new int[]{4, 5, 5, 6};
    wiggleSortII.wiggleSort(nums);
    assertArrayEquals(new int[]{5, 6, 4, 5}, nums);
  }

  @Test
  void nums45555666() {
    int[] nums = new int[]{4, 5, 5, 5, 5, 6, 6, 6};
    wiggleSortII.wiggleSort(nums);
    assertArrayEquals(new int[]{5, 6, 5, 6, 5, 6, 4, 5}, nums);
  }
}