package leetcode.sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KClosestPointsToOriginTest {

    private KClosestPointsToOrigin kClosestPointsToOrigin = new KClosestPointsToOrigin.Solution();

    @Test void points13_N22k1outputN22() {
        assertArrayEquals(new int[][]{{-2, 2}}, kClosestPointsToOrigin.kClosest(new int[][]{{1, 3}, {-2, 2}}, 1));
    }

    @Test void points33_5N1_N24k2output33_N24() {
        assertArrayEquals(new int[][]{{3, 3}, {-2, 4}},
                kClosestPointsToOrigin.kClosest(new int[][]{{3, 3}, {5, -1}, {-2, 4}}, 2));
    }
}