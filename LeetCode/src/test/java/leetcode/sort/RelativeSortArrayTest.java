package leetcode.sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RelativeSortArrayTest {

    private RelativeSortArray relativeSortArray = new RelativeSortArray.Solution();

    @Test void arr231arr214() {
        assertArrayEquals(new int[]{2,2,2,1,4,3,3,9,6,7,19},
                relativeSortArray.relativeSortArray(new int[]{2,3,1,3,2,4,6,7,9,2,19}, new int[]{2,1,4,3,9,6}));
    }
}