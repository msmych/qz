package leetcode.sort;

import leetcode.linked_list.ListNode;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortListTest {

  private SortList sortList = new SortList.Solution();

  @Test
  void list4213() {
    assertTrue(ListNode.equalVals(sortList.sortList(ListNode.fromVals(4, 2, 3, 1)), 1, 2, 3, 4));
  }

  @Test
  void listN15340() {
    assertTrue(ListNode.equalVals(sortList.sortList(ListNode.fromVals(-1, 5, 3, 4, 0)), -1, 0, 3, 4, 5));
  }
}
