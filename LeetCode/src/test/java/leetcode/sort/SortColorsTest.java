package leetcode.sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortColorsTest {

  private SortColors sortColors = new SortColors.Solution();

  @Test
  void input202110output001122() {
    int[] input = new int[]{2, 0, 2, 1, 1, 0};
    sortColors.sortColors(input);
    assertArrayEquals(new int[]{0, 0, 1, 1, 2, 2}, input);
  }
}
