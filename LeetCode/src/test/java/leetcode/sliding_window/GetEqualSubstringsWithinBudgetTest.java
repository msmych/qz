package leetcode.sliding_window;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GetEqualSubstringsWithinBudgetTest {

    private GetEqualSubstringsWithinBudget getEqualSubstringsWithinBudget = new GetEqualSubstringsWithinBudget.Solution();

    @Test
    void abcd_bcdf_3() {
        assertEquals(3, getEqualSubstringsWithinBudget.equalSubstring("abcd", "bcdf", 3));
    }

    @Test
    void abcd_cdef_3() {
        assertEquals(1, getEqualSubstringsWithinBudget.equalSubstring("abcd", "cdef", 3));
    }

    @Test
    void abcd_acde_0() {
        assertEquals(1, getEqualSubstringsWithinBudget.equalSubstring("abcd", "acde", 0));
    }

    @Test
    void krpgjbjjznpzdfy_nxargkbydxmsgby_14() {
        assertEquals(4, getEqualSubstringsWithinBudget.equalSubstring("krpgjbjjznpzdfy", "nxargkbydxmsgby", 14));
    }

    @Test
    void agzxjgvcsubbxeyn_nopafyvjfvekmnfp_112() {
        assertEquals(12, getEqualSubstringsWithinBudget.equalSubstring("agzxjgvcsubbxeyn", "nopafyvjfvekmnfp", 112));
    }

}