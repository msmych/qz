package leetcode.sliding_window;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MovingStonesUntilConsecutiveIITest {

    private MovingStonesUntilConsecutiveII movingStonesUntilConsecutiveII = new MovingStonesUntilConsecutiveII.Solution();

    @Test void input749output12() {
        assertArrayEquals(new int[]{1, 2}, movingStonesUntilConsecutiveII.numMovesStonesII(new int[]{7, 4, 9}));
    }

    @Test void input6543_10output23() {
        assertArrayEquals(new int[]{2, 3}, movingStonesUntilConsecutiveII.numMovesStonesII(new int[]{6, 5, 4, 3, 10}));
    }

    @Test void input100_101_104_102_103output00() {
        assertArrayEquals(new int[]{0, 0}, movingStonesUntilConsecutiveII.numMovesStonesII(new int[]{100, 101, 104, 102, 103}));
    }
}