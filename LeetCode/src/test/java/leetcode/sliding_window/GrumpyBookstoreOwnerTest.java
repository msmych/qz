package leetcode.sliding_window;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GrumpyBookstoreOwnerTest {

    private GrumpyBookstoreOwner grumpyBookstoreOwner = new GrumpyBookstoreOwner.Solution();

    @Test void customers10121175grumpy01010101x3output16() {
        assertEquals(16, grumpyBookstoreOwner.maxSatisfied(
                new int[]{1, 0, 1, 2, 1, 1, 7, 5}, new int[]{0, 1, 0, 1, 0, 1, 0, 1}, 3));
    }

    @Test void customers58grumpy01x1output13() {
        assertEquals(13, grumpyBookstoreOwner.maxSatisfied(new int[]{5, 8}, new int[]{0, 1}, 1));
    }
}