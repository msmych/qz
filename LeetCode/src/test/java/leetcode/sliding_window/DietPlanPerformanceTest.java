package leetcode.sliding_window;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DietPlanPerformanceTest {

  private DietPlanPerformance dietPlanPerformance = new DietPlanPerformance.Solution();

  @Test void calories12345() {
    assertEquals(0, dietPlanPerformance.dietPlanPerformance(new int[]{1, 2, 3, 4, 5}, 1, 3, 3));
  }

  @Test void calories32() {
    assertEquals(1, dietPlanPerformance.dietPlanPerformance(new int[]{3, 2}, 2, 0, 1));
  }

  @Test void calories6500() {
    assertEquals(0, dietPlanPerformance.dietPlanPerformance(new int[]{6, 5, 0, 0}, 2, 1, 5));
  }

  @Test void calories6_13() {
    assertEquals(3, dietPlanPerformance.dietPlanPerformance(new int[]{6, 13, 8, 7, 10, 1, 12, 11}, 6, 5, 37));
  }
}
