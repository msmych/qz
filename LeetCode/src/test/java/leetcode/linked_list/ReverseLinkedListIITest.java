package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static leetcode.linked_list.ListNode.equalVals;
import static leetcode.linked_list.ListNode.fromVals;
import static org.junit.jupiter.api.Assertions.*;

class ReverseLinkedListIITest {

    private ReverseLinkedListII reverseLinkedListII = new ReverseLinkedListII.Solution();

    @Test void list12345m2n4output14325() {
        assertTrue(equalVals(
                reverseLinkedListII.reverseBetween(fromVals(1, 2, 3, 4, 5), 2, 4),
                1, 4, 3, 2, 5));
    }

    @Test void list12345m1n5output54321() {
        assertTrue(equalVals(
                reverseLinkedListII.reverseBetween(fromVals(1, 2, 3, 4, 5), 1, 5),
                5, 4, 3, 2, 1));
    }

    @Test void list35m1n2output53() {
        assertTrue(equalVals(reverseLinkedListII.reverseBetween(fromVals(3, 5), 1, 2), 5, 3));
    }
}