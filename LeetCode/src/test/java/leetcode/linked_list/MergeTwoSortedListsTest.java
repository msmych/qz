package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class MergeTwoSortedListsTest {

    private MergeTwoSortedLists mergeTwoSortedLists = new MergeTwoSortedLists.Solution();

    @Test void input124_134output112344() {
        ListNode l1 = ListNode.fromVals(1, 2, 4);
        ListNode l2 = ListNode.fromVals(1, 3, 4);
        assertTrue(ListNode.equalVals(mergeTwoSortedLists.mergeTwoLists(l1, l2), 1, 1, 2, 3, 4, 4));
    }
}