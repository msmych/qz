package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ConvertBinaryNumberInALinkedListToIntegerTest {

  private ConvertBinaryNumberInALinkedListToInteger convertBinaryNumberInALinkedListToInteger =
    new ConvertBinaryNumberInALinkedListToInteger.Solution();

  @Test
  void head101() {
    assertEquals(5, convertBinaryNumberInALinkedListToInteger.getDecimalValue(ListNode.fromVals(1, 0, 1)));
  }

  @Test
  void head0() {
    assertEquals(0, convertBinaryNumberInALinkedListToInteger.getDecimalValue(new ListNode(0)));
  }

  @Test
  void head1() {
    assertEquals(1, convertBinaryNumberInALinkedListToInteger.getDecimalValue(new ListNode(1)));
  }

  @Test
  void head100100111000000() {
    assertEquals(18880,
      convertBinaryNumberInALinkedListToInteger.getDecimalValue(ListNode.fromVals(1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0)));
  }

  @Test
  void head00() {
    assertEquals(0, convertBinaryNumberInALinkedListToInteger.getDecimalValue(ListNode.fromVals(0, 0)));
  }
}