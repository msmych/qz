package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RotateListTest {

    private RotateList rotateList = new RotateList.Solution();

    @Test void input12345k2output45123() {
        assertTrue(ListNode.equalVals(
                rotateList.rotateRight(ListNode.fromVals(1, 2, 3, 4, 5), 2), 4, 5, 1, 2, 3));
    }

    @Test void input012k4output201() {
        assertTrue(ListNode.equalVals(
                rotateList.rotateRight(ListNode.fromVals(0, 1, 2), 4), 2, 0, 1));
    }

    @Test void input_k0output_() {
        assertNull(rotateList.rotateRight(null, 0));
    }
}