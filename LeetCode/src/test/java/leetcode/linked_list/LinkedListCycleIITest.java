package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class LinkedListCycleIITest {

    private LinkedListCycleII linkedListCycleII = new LinkedListCycleII.Solution();

    @Test
    void list1231cycle1() {
        ListNode head = ListNode.fromVals(1, 2, 3);
        head.next.next.next = head;
        assertEquals(head, linkedListCycleII.detectCycle(head));
    }

    @Test
    void list123noCycle() {
        ListNode head = ListNode.fromVals(1, 2, 3);
        assertNull(linkedListCycleII.detectCycle(head));
    }

    @Test
    void list12342() {
        ListNode head = ListNode.fromVals(1, 2, 3, 4);
        head.next.next.next.next = head.next;
        assertEquals(head.next, linkedListCycleII.detectCycle(head));
    }
}