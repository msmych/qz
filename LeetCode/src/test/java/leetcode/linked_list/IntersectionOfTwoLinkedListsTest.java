package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IntersectionOfTwoLinkedListsTest {

    private IntersectionOfTwoLinkedLists intersectionOfTwoLinkedLists = new IntersectionOfTwoLinkedLists.Solution();

    @Test void a1a2c1c2c3_b1b2b3c1c2c3_intersection_c1() {
        ListNode nodeC = ListNode.fromVals(1, 2, 3);
        ListNode headA = ListNode.fromVals(1, 2);
        headA.next.next = nodeC;
        ListNode headB = ListNode.fromVals(1, 2, 3);
        headB.next.next.next = nodeC;
        assertEquals(nodeC, intersectionOfTwoLinkedLists.getIntersectionNode(headA, headB));
    }
}