package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class AddTwoNumbersTest {

    private AddTwoNumbers addTwoNumbers = new AddTwoNumbers.Solution();

    @Test
    void input243plus564output708() {
        assertTrue(ListNode.equalVals(
                addTwoNumbers.addTwoNumbers(ListNode.fromVals(2, 4, 3), ListNode.fromVals(5, 6, 4)), 7, 0, 8));
    }
}