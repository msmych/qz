package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static leetcode.linked_list.ListNode.equalVals;
import static leetcode.linked_list.ListNode.fromVals;
import static org.junit.jupiter.api.Assertions.*;

class RemoveDuplicatesFromSortedListTest {

    private RemoveDuplicatesFromSortedList removeDuplicatesFromSortedList = new RemoveDuplicatesFromSortedList.Solution();

    @Test void list112output12() {
        assertTrue(equalVals(removeDuplicatesFromSortedList.deleteDuplicates(fromVals(1, 1, 2)), 1, 2));
    }

    @Test void list11233output123() {
        assertTrue(equalVals(removeDuplicatesFromSortedList.deleteDuplicates(fromVals(1, 1, 2, 3, 3)), 1, 2, 3));
    }
}