package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MyLinkedListTest {

    @Test
    void addAtHead1addAtTail3addAtIndex12get1deleteAtIndex1get1() {
        assertAddAtHead1addAtTail3addAtIndex12get1deleteAtIndex1get1(new MySinglyLinkedList());
        assertAddAtHead1addAtTail3addAtIndex12get1deleteAtIndex1get1(new MyDoublyLinkedList());
    }

    private void assertAddAtHead1addAtTail3addAtIndex12get1deleteAtIndex1get1(MyAbstractLinkedList linkedList) {
        linkedList.addAtHead(1);
        linkedList.addAtTail(3);
        linkedList.addAtIndex(1, 2);
        assertEquals(2, linkedList.get(1));
        linkedList.deleteAtIndex(1);
        assertEquals(3, linkedList.get(1));
    }

    @Test
    void addAtHead1addAtIndex12get1get0get2() {
        assertAddAtHead1addAtIndex12get1get0get2(new MySinglyLinkedList());
        assertAddAtHead1addAtIndex12get1get0get2(new MyDoublyLinkedList());
    }

    private void assertAddAtHead1addAtIndex12get1get0get2(MyAbstractLinkedList linkedList) {
        linkedList.addAtHead(1);
        linkedList.addAtIndex(1, 2);
        assertEquals(2, linkedList.get(1));
        assertEquals(1, linkedList.get(0));
        assertEquals(-1, linkedList.get(2));
    }

    @Test
    void get0addAtIndex12get0get1addAtIndex01get0get1() {
        assertGet0addAtIndex12get0get1addAtIndex01get0get1(new MySinglyLinkedList());
        assertGet0addAtIndex12get0get1addAtIndex01get0get1(new MyDoublyLinkedList());
    }

    private void assertGet0addAtIndex12get0get1addAtIndex01get0get1(MyAbstractLinkedList linkedList) {
        assertEquals(-1, linkedList.get(0));
        linkedList.addAtIndex(1, 2);
        assertEquals(-1, linkedList.get(0));
        assertEquals(-1, linkedList.get(1));
        linkedList.addAtIndex(0, 1);
        assertEquals(1, linkedList.get(0));
        assertEquals(-1, linkedList.get(1));
    }
}