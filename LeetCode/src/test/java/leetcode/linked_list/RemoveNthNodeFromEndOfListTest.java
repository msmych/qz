package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class RemoveNthNodeFromEndOfListTest {

    private RemoveNthNodeFromEndOfList removeNthNodeFromEndOfList = new RemoveNthNodeFromEndOfList.Solution();

    @Test
    void list12345n2list1235() {
        ListNode head = ListNode.fromVals(1, 2, 3, 4, 5);
        assertTrue(ListNode.equalVals(removeNthNodeFromEndOfList.removeNthFromEnd(head, 2), 1, 2, 3, 5));
    }

    @Test void list12n1list1() {
        ListNode head = ListNode.fromVals(1, 2);
        assertTrue(ListNode.equalVals(removeNthNodeFromEndOfList.removeNthFromEnd(head, 1), 1));
    }

    @Test void list123n1list12() {
        ListNode head = ListNode.fromVals(1, 2, 3);
        assertTrue(ListNode.equalVals(removeNthNodeFromEndOfList.removeNthFromEnd(head, 1), 1, 2));
    }
}