package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static leetcode.linked_list.ListNode.equalVals;
import static leetcode.linked_list.ListNode.fromVals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DeleteNodeInALinkedListTest {

    private DeleteNodeInALinkedList deleteNodeInALinkedList = new DeleteNodeInALinkedList.Solution();

    @Test void head4519node5output459() {
        ListNode node = fromVals(4, 5, 1, 9);
        deleteNodeInALinkedList.deleteNode(node);
        assertTrue(equalVals(node, 5, 1, 9));
    }

    @Test void head4519node1output459() {
        ListNode node = fromVals(1, 2);
        deleteNodeInALinkedList.deleteNode(node);
        assertTrue(equalVals(node, 2));
    }
}