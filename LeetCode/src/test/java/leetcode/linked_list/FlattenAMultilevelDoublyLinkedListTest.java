package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class FlattenAMultilevelDoublyLinkedListTest {

    private FlattenAMultilevelDoublyLinkedList flattenAMultilevelDoublyLinkedList =
            new FlattenAMultilevelDoublyLinkedList.Solution();

    @Test void input1_2_3__7_8__11_12__9_10__4_5_6output1_2_3_7_8_11_12_9_10_4_5_6() {
        Node head = Node.fromVals(1, 2, 3, Node.fromVals(7, 8, Node.fromVals(11, 12), 9, 10), 4, 5, 6);
        assertTrue(Node.equalVals(flattenAMultilevelDoublyLinkedList.flatten(head),
                1, 2, 3, 7, 8, 11, 12, 9, 10, 4, 5, 6));
    }
}