package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RemoveDuplicatesFromSortedListIITest {

    private RemoveDuplicatesFromSortedList removeDuplicatesFromSortedList = new RemoveDuplicatesFromSortedList.SolutionII();

    @Test void list1233445output125() {
        assertTrue(ListNode.equalVals(
                removeDuplicatesFromSortedList.deleteDuplicates(ListNode.fromVals(1, 2, 3, 3, 4, 4, 5)), 1, 2, 5));
    }

    @Test void list11123output23() {
        assertTrue(ListNode.equalVals(removeDuplicatesFromSortedList.deleteDuplicates(ListNode.fromVals(1, 1, 1, 2, 3)), 2, 3));
    }
}