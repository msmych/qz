package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PalindromeLinkedListTest {

    private PalindromeLinkedList palindromeLinkedList = new PalindromeLinkedList.Solution();

    @Test void input1_2false() {
        assertFalse(palindromeLinkedList.isPalindrome(ListNode.fromVals(1, 2)));
    }

    @Test void input1_2_2_1true() {
        assertTrue(palindromeLinkedList.isPalindrome(ListNode.fromVals(1, 2, 2, 1)));
    }
}