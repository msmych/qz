package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ReverseLinkedListTest {

    private ReverseLinkedList iterative = new ReverseLinkedList.IterativeSolution();
    private ReverseLinkedList recursive = new ReverseLinkedList.RecursiveSolution();

    @Test
    void input1_2_3_4_5output5_4_3_2_1() {
        ListNode head = ListNode.fromVals(1, 2, 3, 4, 5);
        assertTrue(ListNode.equalVals(iterative.reverseList(head), 5, 4, 3, 2, 1));
        head = ListNode.fromVals(1, 2, 3, 4, 5);
        assertTrue(ListNode.equalVals(recursive.reverseList(head), 5, 4, 3, 2, 1));
    }
}