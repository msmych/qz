package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ReverseNodesInKGroupTest {

    private ReverseNodesInKGroup reverseNodesInKGroup = new ReverseNodesInKGroup.Solution();

    @Test void list12345k2() {
        assertTrue(ListNode.equalVals(reverseNodesInKGroup.reverseKGroup(ListNode.fromVals(1, 2, 3, 4, 5), 2), 2, 1, 4, 3, 5));
    }

    @Test void list12345k3() {
        assertTrue(ListNode.equalVals(reverseNodesInKGroup.reverseKGroup(ListNode.fromVals(1, 2, 3, 4, 5), 3), 3, 2, 1, 4, 5));
    }
}