package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class OddEvenLinkedListTest {

  private OddEvenLinkedList oddEvenLinkedList = new OddEvenLinkedList.Solution();

  @Test
  void input12345output13524() {
    ListNode head = ListNode.fromVals(1, 2, 3, 4, 5);
    assertTrue(ListNode.equalVals(oddEvenLinkedList.oddEvenList(head), 1, 3, 5, 2, 4));
  }

  @Test
  void input2135647output2367154() {
    ListNode head = ListNode.fromVals(2, 1, 3, 5, 6, 4, 7);
    assertTrue(ListNode.equalVals(oddEvenLinkedList.oddEvenList(head), 2, 3, 6, 7, 1, 5, 4));
  }
}
