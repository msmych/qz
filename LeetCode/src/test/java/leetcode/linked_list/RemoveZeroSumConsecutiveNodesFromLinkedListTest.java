package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static leetcode.linked_list.ListNode.equalVals;
import static org.junit.jupiter.api.Assertions.*;

class RemoveZeroSumConsecutiveNodesFromLinkedListTest {

    private RemoveZeroSumConsecutiveNodesFromLinkedList removeZeroSumConsecutiveNodesFromLinkedList =
        new RemoveZeroSumConsecutiveNodesFromLinkedList.Solution();

    @Test void list12N331() {
        assertTrue(equalVals(removeZeroSumConsecutiveNodesFromLinkedList.removeZeroSumSublists(
            ListNode.fromVals(1, 2, -3, 3, 1)),
            3, 1));
    }

    @Test void list123N34() {
        assertTrue(equalVals(removeZeroSumConsecutiveNodesFromLinkedList.removeZeroSumSublists(
            ListNode.fromVals(1, 2, 3, -3, 4)),
            1, 2, 4));
    }

    @Test void list123N3N2() {
        assertTrue(equalVals(removeZeroSumConsecutiveNodesFromLinkedList.removeZeroSumSublists(
            ListNode.fromVals(1, 2, 3, -3, -2)),
            1));
    }
}