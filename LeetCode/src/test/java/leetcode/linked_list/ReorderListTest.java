package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static leetcode.linked_list.ListNode.equalVals;
import static leetcode.linked_list.ListNode.fromVals;
import static org.junit.jupiter.api.Assertions.*;

class ReorderListTest {

    private ReorderList reorderList = new ReorderList.Solution();

    @Test void given1234reorder1423() {
        ListNode head = fromVals(1, 2, 3, 4);
        reorderList.reorderList(head);
        assertTrue(equalVals(head, 1, 4, 2, 3));
    }

    @Test void given12345reorder15243() {
        ListNode head = fromVals(1, 2, 3, 4, 5);
        reorderList.reorderList(head);
        assertTrue(equalVals(head, 1, 5, 2, 4, 3));
    }
}