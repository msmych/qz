package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class RemoveLinkedListElementsTest {

    private RemoveLinkedListElements removeLinkedListElements = new RemoveLinkedListElements.Solution();

    @Test void input1_2_3_4_5_6val6output1_2_3_4_5() {
        ListNode head = ListNode.fromVals(1, 2, 3, 4, 5, 6);
        assertTrue(ListNode.equalVals(removeLinkedListElements.removeElements(head, 6), 1, 2, 3, 4, 5));
    }
}