package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MiddleOfTheLinkedListTest {

    private MiddleOfTheLinkedList middleOfTheLinkedList = new MiddleOfTheLinkedList.Solution();

    @Test void list12345output3() {
        assertTrue(ListNode.equalVals(
                middleOfTheLinkedList.middleNode(ListNode.fromVals(1, 2, 3, 4, 5)), 3, 4, 5));
    }

    @Test void list123456output4() {
        assertTrue(ListNode.equalVals(
                middleOfTheLinkedList.middleNode(ListNode.fromVals(1, 2, 3, 4, 5, 6)), 4, 5, 6));
    }
}