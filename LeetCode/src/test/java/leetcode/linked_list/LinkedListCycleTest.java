package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LinkedListCycleTest {

    private LinkedListCycle linkedListCycle = new LinkedListCycle.Solution();
    private LinkedListCycle twoPointers = new LinkedListCycle.TwoPointersSolution();

    @Test
    void list1231hasCycle() {
        ListNode head = ListNode.fromVals(1, 2, 3);
        head.next.next.next = head;
        assertTrue(linkedListCycle.hasCycle(head));
        head = ListNode.fromVals(1, 2, 3);
        head.next.next.next = head;
        assertTrue(twoPointers.hasCycle(head));
    }

    @Test
    void list123hasNoCycle() {
        ListNode head = ListNode.fromVals(1, 2, 3);
        assertFalse(linkedListCycle.hasCycle(head));
        head = ListNode.fromVals(1, 2, 3);
        assertFalse(twoPointers.hasCycle(head));
    }
}