package leetcode.linked_list;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SwapNodesInPairsTest {

    private SwapNodesInPairs swapNodesInPairs = new SwapNodesInPairs.Solution();

    @Test void given1234return2143() {
        assertTrue(ListNode.equalVals(swapNodesInPairs.swapPairs(ListNode.fromVals(1, 2, 3, 4)), 2, 1, 4, 3));
    }
}