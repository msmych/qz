package leetcode.segment_tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountOfSmallerNumbersAfterSelfTest {

  private CountOfSmallerNumbersAfterSelf countOfSmallerNumbersAfterSelf = new CountOfSmallerNumbersAfterSelf.Solution();

  @Test
  void input5261() {
    assertArrayEquals(new Integer[]{2, 1, 1, 0},
      countOfSmallerNumbersAfterSelf.countSmaller(new int[]{5, 2, 6, 1}).toArray(new Integer[]{}));
  }
}