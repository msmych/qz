package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class PlusOneTest {

    private PlusOne plusOne = new PlusOne.Solution();

    @Test
    void input123output124() {
        assertArrayEquals(new int[]{1, 2, 4}, plusOne.plusOne(new int[]{1, 2, 3}));
    }

    @Test void input4321output4322() {
        assertArrayEquals(new int[]{4, 3, 2, 2}, plusOne.plusOne(new int[]{4, 3, 2, 1}));
    }

    @Test void input99output100() {
        assertArrayEquals(new int[]{1, 0, 0}, plusOne.plusOne(new int[]{9, 9}));
    }

    @Test void input199output200() {
        assertArrayEquals(new int[]{2, 0, 0}, plusOne.plusOne(new int[]{1, 9, 9}));
    }

    @Test void input0output1() {
        assertArrayEquals(new int[]{1}, plusOne.plusOne(new int[]{0}));
    }

    @Test void input4output5() {
        assertArrayEquals(new int[]{5}, plusOne.plusOne(new int[]{4}));
    }

    @Test
    void input9output10() {
        assertArrayEquals(new int[]{1, 0}, plusOne.plusOne(new int[]{9}));
    }

    @Test void input9876543210output9876543211() {
        assertArrayEquals(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 1},
                plusOne.plusOne(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0}));
    }
}