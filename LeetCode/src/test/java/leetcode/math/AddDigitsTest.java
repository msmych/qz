package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddDigitsTest {

  private AddDigits addDigits = new AddDigits.Solution();

  @Test void input38() {
    assertEquals(2, addDigits.addDigits(38));
  }
}
