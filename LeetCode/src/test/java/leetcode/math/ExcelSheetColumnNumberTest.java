package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExcelSheetColumnNumberTest {

    private ExcelSheetColumnNumber excelSheetColumnNumber = new ExcelSheetColumnNumber.Solution();

    @Test void A1() {
        assertEquals(1, excelSheetColumnNumber.titleToNumber("A"));
    }

    @Test void AB28() {
        assertEquals(28, excelSheetColumnNumber.titleToNumber("AB"));
    }

    @Test void ZY701() {
        assertEquals(701, excelSheetColumnNumber.titleToNumber("ZY"));
    }
}