package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UglyNumberTest {

  private UglyNumber uglyNumber = new UglyNumber.Solution();

  @Test void input6() {
    assertTrue(uglyNumber.isUgly(6));
  }

  @Test void input8() {
    assertTrue(uglyNumber.isUgly(8));
  }

  @Test void input14() {
    assertFalse(uglyNumber.isUgly(14));
  }

  @Test void input0() {
    assertFalse(uglyNumber.isUgly(0));
  }
}
