package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CircularPermutationInBinaryRepresentationTest {

  private CircularPermutationInBinaryRepresentation circularPermutationInBinaryRepresentation =
    new CircularPermutationInBinaryRepresentation.Solution();

  @Test
  void n2start3() {
    assertArrayEquals(new Integer[]{3, 2, 0, 1},
      circularPermutationInBinaryRepresentation.circularPermutation(2, 3).toArray(new Integer[]{}));
  }

  @Test
  void n3start2() {
    assertArrayEquals(new Integer[]{2, 6, 7, 5, 4, 0, 1, 3},
      circularPermutationInBinaryRepresentation.circularPermutation(3, 2).toArray(new Integer[]{}));
  }
}