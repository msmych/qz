package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinaryGapTest {

    private BinaryGap binaryGap = new BinaryGap.Solution();

    @Test void input22output2() {
        assertEquals(2, binaryGap.binaryGap(22));
    }

    @Test void input5output2() {
        assertEquals(2, binaryGap.binaryGap(5));
    }

    @Test void input6output1() {
        assertEquals(1, binaryGap.binaryGap(6));
    }

    @Test void input8output0() {
        assertEquals(0, binaryGap.binaryGap(8));
    }

    @Test void input343output2() {
        assertEquals(2, binaryGap.binaryGap(343));
    }
}