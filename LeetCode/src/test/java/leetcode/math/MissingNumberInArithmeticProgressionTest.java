package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MissingNumberInArithmeticProgressionTest {

  private MissingNumberInArithmeticProgression missingNumberInArithmeticProgression = new MissingNumberInArithmeticProgression.Solution();

  @Test
  void arr571113() {
    assertEquals(9, missingNumberInArithmeticProgression.missingNumber(new int[]{5, 7, 11, 13}));
  }

  @Test
  void arr151312() {
    assertEquals(14, missingNumberInArithmeticProgression.missingNumber(new int[]{15, 13, 12}));
  }

  @Test
  void arr00000() {
    assertEquals(0, missingNumberInArithmeticProgression.missingNumber(new int[]{0, 0, 0, 0, 0}));
  }
}
