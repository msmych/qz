package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortArrayByParityTest {

    private SortArrayByParity sortArrayByParity = new SortArrayByParity.Solution();

    @Test void input3124output2431() {
        assertArrayEquals(new int[]{2, 4, 3, 1}, sortArrayByParity.sortArrayByParity(new int[]{3, 1, 2, 4}));
    }

    @Test void input0output0() {
        assertArrayEquals(new int[]{0}, sortArrayByParity.sortArrayByParity(new int[]{0}));
    }

    @Test void input1output1() {
        assertArrayEquals(new int[]{1}, sortArrayByParity.sortArrayByParity(new int[]{1}));
    }
}