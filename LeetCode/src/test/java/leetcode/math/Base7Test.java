package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Base7Test {

    private Base7 base7 = new Base7.Solution();

    @Test void input100() {
        assertEquals("202", base7.convertToBase7(100));
    }

    @Test void inputN7() {
        assertEquals("-10", base7.convertToBase7(-7));
    }
}