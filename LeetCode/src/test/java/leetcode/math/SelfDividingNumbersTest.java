package leetcode.math;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertCollectionContainsAll;

class SelfDividingNumbersTest {

    private SelfDividingNumbers selfDividingNumbers = new SelfDividingNumbers.Solution();

    @Test void left1right22() {
        assertCollectionContainsAll(
                selfDividingNumbers.selfDividingNumbers(1, 22),
                1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22);
    }
}