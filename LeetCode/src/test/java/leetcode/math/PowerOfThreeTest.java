package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PowerOfThreeTest {

    private PowerOfThree powerOfThree = new PowerOfThree.Solution();

    @Test void input27() {
        assertTrue(powerOfThree.isPowerOfThree(27));
    }

    @Test void input0() {
        assertFalse(powerOfThree.isPowerOfThree(0));
    }

    @Test void input9() {
        assertTrue(powerOfThree.isPowerOfThree(9));
    }

    @Test void input45() {
        assertFalse(powerOfThree.isPowerOfThree(45));
    }

    @Test void input1() {
        assertTrue(powerOfThree.isPowerOfThree(1));
    }

    @Test void input6() {
        assertFalse(powerOfThree.isPowerOfThree(6));
    }
}