package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RectangleOverlapTest {

    private RectangleOverlap rectangleOverlap = new RectangleOverlap.Solution();

    @Test void rec0022rec1133true() {
        assertTrue(rectangleOverlap.isRectangleOverlap(new int[]{0, 0, 2, 2}, new int[]{1, 1, 3, 3}));
    }

    @Test void rec0011rec1021false() {
        assertFalse(rectangleOverlap.isRectangleOverlap(new int[]{0, 0, 1, 1}, new int[]{1, 0, 2, 1}));
    }

    @Test void rec7_8_13_15rec10_8_12_20true() {
        assertTrue(rectangleOverlap.isRectangleOverlap(new int[]{7, 8, 13, 15}, new int[]{10, 8, 12, 20}));
    }

    @Test void rec2_17_6_20rec3_8_6_20true() {
        assertTrue(rectangleOverlap.isRectangleOverlap(new int[]{2, 17, 6, 20}, new int[]{3, 8, 6, 20}));
    }
}