package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReachANumberTest {

    private ReachANumber reachANumber = new ReachANumber.Solution();

    @Test void target3output2() {
        assertEquals(2, reachANumber.reachNumber(3));
    }

    @Test void target2output3() {
        assertEquals(3, reachANumber.reachNumber(2));
    }

    @Test void targetN1000000000output44723() {
        assertEquals(44723, reachANumber.reachNumber(-1000000000));
    }

    @Test void target5output5() {
        assertEquals(5, reachANumber.reachNumber(5));
    }

    @Test void target4output3() {
        assertEquals(3, reachANumber.reachNumber(4));
    }
}