package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IntegerToRomanTest {

    private IntegerToRoman integerToRoman = new IntegerToRoman.Solution();

    @Test void III() {
        assertEquals("III", integerToRoman.intToRoman(3));
    }

    @Test void IV() {
        assertEquals("IV", integerToRoman.intToRoman(4));
    }

    @Test void IX() {
        assertEquals("IX", integerToRoman.intToRoman(9));
    }

    @Test void LVIII() {
        assertEquals("LVIII", integerToRoman.intToRoman(58));
    }

    @Test void MCMXCIV() {
        assertEquals("MCMXCIV", integerToRoman.intToRoman(1994));
    }
}