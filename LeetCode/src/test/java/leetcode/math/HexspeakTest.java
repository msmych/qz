package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HexspeakTest {

  private Hexspeak hexspeak = new Hexspeak.Solution();

  @Test
  void num257() {
    assertEquals("IOI", hexspeak.toHexspeak("257"));
  }

  @Test
  void num3() {
    assertEquals("ERROR", hexspeak.toHexspeak("3"));
  }

  @Test
  void num619879596177() {
    assertEquals("ERROR", hexspeak.toHexspeak("619879596177"));
  }

  @Test
  void num747823223228() {
    assertEquals("AEIDBCDIBC", hexspeak.toHexspeak("747823223228"));
  }
}