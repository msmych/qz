package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComplementOfBase10IntegerTest {

    private ComplementOfBase10Integer complementOfBase10Integer = new ComplementOfBase10Integer.Solution();

    @Test void input5output2() {
        assertEquals(2, complementOfBase10Integer.bitwiseComplement(5));
    }

    @Test void input7output0() {
        assertEquals(0, complementOfBase10Integer.bitwiseComplement(7));
    }

    @Test void input10output5() {
        assertEquals(5, complementOfBase10Integer.bitwiseComplement(10));
    }
}