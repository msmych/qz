package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubtractTheProductAndSumOfDigitsOfAnIntegerTest {

  private SubtractTheProductAndSumOfDigitsOfAnInteger subtractTheProductAndSumOfDigitsOfAnInteger =
    new SubtractTheProductAndSumOfDigitsOfAnInteger.Solution();

  @Test
  void n234() {
    assertEquals(15, subtractTheProductAndSumOfDigitsOfAnInteger.subtractProductAndSum(234));
  }

  @Test
  void n4421() {
    assertEquals(21, subtractTheProductAndSumOfDigitsOfAnInteger.subtractProductAndSum(4421));
  }

  @Test
  void n21() {
    assertEquals(-1, subtractTheProductAndSumOfDigitsOfAnInteger.subtractProductAndSum(21));
  }
}