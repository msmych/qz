package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SumOfSquareNumbersTest {

    private SumOfSquareNumbers sumOfSquareNumbers = new SumOfSquareNumbers.Solution();

    @Test void true5() {
        assertTrue(sumOfSquareNumbers.judgeSquareSum(5));
    }

    @Test void false3() {
        assertFalse(sumOfSquareNumbers.judgeSquareSum(3));
    }

    @Test void true4() {
        assertTrue(sumOfSquareNumbers.judgeSquareSum(4));
    }

    @Test
    void false2147482647() {
        assertFalse(sumOfSquareNumbers.judgeSquareSum( 2147482647));
    }

    @Test void true1000() {
        assertTrue(sumOfSquareNumbers.judgeSquareSum(1000));
    }
}