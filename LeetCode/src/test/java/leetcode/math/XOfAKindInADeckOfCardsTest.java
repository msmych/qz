package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class XOfAKindInADeckOfCardsTest {

    private XOfAKindInADeckOfCards xOfAKindInADeckOfCards = new XOfAKindInADeckOfCards.Solution();

    @Test void input12344321true() {
        assertTrue(xOfAKindInADeckOfCards.hasGroupsSizeX(new int[]{1, 2, 3, 4, 4, 3, 2, 1}));
    }

    @Test void input11122233false() {
        assertFalse(xOfAKindInADeckOfCards.hasGroupsSizeX(new int[]{1, 1, 1, 2, 2, 2, 3, 3}));
    }

    @Test void input1false() {
        assertFalse(xOfAKindInADeckOfCards.hasGroupsSizeX(new int[]{1}));
    }

    @Test void input11true() {
        assertTrue(xOfAKindInADeckOfCards.hasGroupsSizeX(new int[]{1, 1}));
    }

    @Test void input112222true() {
        assertTrue(xOfAKindInADeckOfCards.hasGroupsSizeX(new int[]{1, 1, 2, 2, 2, 2}));
    }

    @Test void input1111222222true() {
        assertTrue(xOfAKindInADeckOfCards.hasGroupsSizeX(new int[]{1, 1, 1, 1, 2, 2, 2, 2, 2, 2}));
    }
}