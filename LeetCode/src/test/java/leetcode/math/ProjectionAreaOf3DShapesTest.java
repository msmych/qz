package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProjectionAreaOf3DShapesTest {

    private ProjectionAreaOf3DShapes projectionAreaOf3DShapes = new ProjectionAreaOf3DShapes.Solution();

    @Test void input2output5() {
        assertEquals(5, projectionAreaOf3DShapes.projectionArea(new int[][]{{2}}));
    }

    @Test void input12_34output17() {
        assertEquals(17, projectionAreaOf3DShapes.projectionArea(new int[][]{{1, 2}, {3, 4}}));
    }

    @Test void input10_02output8() {
        assertEquals(8, projectionAreaOf3DShapes.projectionArea(new int[][]{{1, 0}, {0, 2}}));
    }

    @Test void input111_101_111output14() {
        assertEquals(14, projectionAreaOf3DShapes.projectionArea(new int[][]{{1, 1, 1}, {1, 0, 1}, {1, 1 ,1}}));
    }

    @Test void input222_212_222output21() {
        assertEquals(21, projectionAreaOf3DShapes.projectionArea(new int[][]{{2, 2, 2}, {2, 1, 2}, {2, 2, 2}}));
    }
}