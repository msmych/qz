package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FactorialTrailingZeroesTest {

    private FactorialTrailingZeroes factorialTrailingZeroes = new FactorialTrailingZeroes.Solution();

    @Test void input3output0() {
        assertEquals(0, factorialTrailingZeroes.trailingZeroes(3));
    }

    @Test
    void input5output1() {
        assertEquals(1, factorialTrailingZeroes.trailingZeroes(5));
    }

    @Test void input2147483647output536870902() {
        assertEquals(536870902, factorialTrailingZeroes.trailingZeroes(2147483647));
    }

    @Test void input26output6() {
        assertEquals(6, factorialTrailingZeroes.trailingZeroes(26));
    }

    @Test void input120output28() {
        assertEquals(28, factorialTrailingZeroes.trailingZeroes(120));
    }

    @Test void input120120output30026() {
        assertEquals(30026, factorialTrailingZeroes.trailingZeroes(120120));
    }

    @Test void input120120120output30030024() {
        assertEquals(30030024, factorialTrailingZeroes.trailingZeroes(120120120));
    }
}