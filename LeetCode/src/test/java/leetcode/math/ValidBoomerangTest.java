package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidBoomerangTest {

    private ValidBoomerang validBoomerang = new ValidBoomerang.Solution();

    @Test void input11_23_32true() {
        assertTrue(validBoomerang.isBoomerang(new int[][]{{1, 1}, {2, 3}, {3, 2}}));
    }

    @Test void input11_22_33false() {
        assertFalse(validBoomerang.isBoomerang(new int[][]{{1, 1}, {2, 2}, {3, 3}}));
    }

    @Test void input00_02_21true() {
        assertTrue(validBoomerang.isBoomerang(new int[][]{{0, 0}, {0, 2}, {2, 1}}));
    }

    @Test void input11_21_33true() {
        assertTrue(validBoomerang.isBoomerang(new int[][]{{1, 1}, {2, 1}, {3, 3}}));
    }

    @Test void input12_00_21true() {
        assertTrue(validBoomerang.isBoomerang(new int[][]{{1, 2}, {0, 0}, {2, 1}}));
    }
}