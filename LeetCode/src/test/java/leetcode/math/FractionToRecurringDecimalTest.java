package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FractionToRecurringDecimalTest {

  private FractionToRecurringDecimal fractionToRecurringDecimal = new FractionToRecurringDecimal.Solution();

  @Test
  void n1d1output0_5() {
    assertEquals("0.5", fractionToRecurringDecimal.fractionToDecimal(1, 2));
  }

  @Test
  void n2d1output2() {
    assertEquals("2", fractionToRecurringDecimal.fractionToDecimal(2, 1));
  }

  @Test
  void n2d3output0_$6$() {
    assertEquals("0.(6)", fractionToRecurringDecimal.fractionToDecimal(2, 3));
  }

  @Test
  void n1000d11output90_$90$() {
    assertEquals("90.(90)", fractionToRecurringDecimal.fractionToDecimal(1000, 11));
  }

  @Test
  void n4d333output0_$012$() {
    assertEquals("0.(012)", fractionToRecurringDecimal.fractionToDecimal(4, 333));
  }

  @Test
  void n0d3() {
    assertEquals("0", fractionToRecurringDecimal.fractionToDecimal(0, 3));
  }

  @Test
  void n0dN5() {
    assertEquals("0", fractionToRecurringDecimal.fractionToDecimal(0, -5));
  }
}
