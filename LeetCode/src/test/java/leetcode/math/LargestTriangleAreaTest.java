package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LargestTriangleAreaTest {

    private LargestTriangleArea largestTriangleArea = new LargestTriangleArea.Solution();

    @Test void points00_01_10_02_20output2() {
        assertEquals(2, largestTriangleArea.largestTriangleArea(
                new int[][]{{0, 0}, {0, 1}, {1, 0}, {0, 2}, {2, 0}}));
    }

    @Test void points2$5_7$7_10$8_10$10_1$1output14$5() {
        assertEquals(14.5, largestTriangleArea.largestTriangleArea(
                new int[][]{{2, 5}, {7, 7}, {10, 8}, {10, 10}, {1, 1}}));
    }

    @Test void points9$0_1$10_7$6_10$0_3$9output14() {
        assertEquals(14, largestTriangleArea.largestTriangleArea(
                new int[][]{{9, 0}, {1, 10}, {7, 6}, {10, 0}, {3, 9}}));
    }
}