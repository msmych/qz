package leetcode.math;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertTrue;

class DIStringMatchTest {

    private DIStringMatch diStringMatch = new DIStringMatch.Solution();

    @Test void IDID() {
        assertDI(diStringMatch.diStringMatch("DIDI"), "DIDI");
    }

    private void assertDI(int[] a, String s) {
        for (int i = 1; i < a.length; i++) {
            assertTrue(Arrays.stream(a).boxed().collect(Collectors.toList()).contains(i));
            switch (s.charAt(i - 1)) {
                case 'I':
                    assertTrue(a[i] > a[i - 1]);
                    break;
                case 'D':
                    assertTrue(a[i] < a[i - 1]);
            }
        }
    }

    @Test void III() {
        assertDI(diStringMatch.diStringMatch("III"), "III");
    }

    @Test void DDI() {
        assertDI(diStringMatch.diStringMatch("DDI"), "DDI");
    }
}