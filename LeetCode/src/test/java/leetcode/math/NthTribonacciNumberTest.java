package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NthTribonacciNumberTest {

    private NthTribonacciNumber nthTribonacciNumber = new NthTribonacciNumber.Solution();

    @Test void n4() {
        assertEquals(4, nthTribonacciNumber.tribonacci(4));
    }

    @Test void n25() {
        assertEquals(1389537, nthTribonacciNumber.tribonacci(25));
    }
}