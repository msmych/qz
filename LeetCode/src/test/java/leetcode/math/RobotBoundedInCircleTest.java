package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RobotBoundedInCircleTest {

    private RobotBoundedInCircle robotBoundedInCircle = new RobotBoundedInCircle.Solution();

    @Test void GGLLGGtrue() {
        assertTrue(robotBoundedInCircle.isRobotBounded("GGLLGG"));
    }

    @Test void GGfalse() {
        assertFalse(robotBoundedInCircle.isRobotBounded("GG"));
    }

    @Test void GLtrue() {
        assertTrue(robotBoundedInCircle.isRobotBounded("GL"));
    }

    @Test void RRGRRGLLLRLGGLGLLGRLRLGLRLRRGLGGLLRRRLRLRLLGRGLGRRRGRLGfalse() {
        assertFalse(robotBoundedInCircle.isRobotBounded("RRGRRGLLLRLGGLGLLGRLRLGLRLRRGLGGLLRRRLRLRLLGRGLGRRRGRLG"));
    }
}