package leetcode.math;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class ConstructTheRectangleTest {

    private ConstructTheRectangle constructTheRectangle = new ConstructTheRectangle.Solution();

    @Test void input4() {
        assertArrayEquals(new int[]{2,2}, constructTheRectangle.constructRectangle(4));
    }

    @Test void input1() {
        assertArrayEquals(new int[]{1, 1}, constructTheRectangle.constructRectangle(1));
    }

    @Test void input1000000() {
        assertArrayEquals(new int[]{1000, 1000}, constructTheRectangle.constructRectangle(1_000_000));
    }
}