package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PowerfulIntegersTest {

    private PowerfulIntegers powerfulIntegers = new PowerfulIntegers.Solution();

    @Test void x2y3bound10output234579_10() {
        assertArrayEquals(new Integer[]{2, 3, 4, 5, 7, 9, 10}, powerfulIntegers.powerfulIntegers(2, 3, 10).toArray(new Integer[]{}));
    }

    @Test void x3y5bound15output2468_10_14() {
        assertArrayEquals(new Integer[]{2, 4, 6, 8, 10, 14}, powerfulIntegers.powerfulIntegers(3, 5, 15).toArray(new Integer[]{}));
    }

    @Test void x1y2bound100() {
        powerfulIntegers.powerfulIntegers(1, 2, 100);
    }
}