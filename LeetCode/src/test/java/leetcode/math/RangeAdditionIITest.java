package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangeAdditionIITest {

    private RangeAdditionII rangeAdditionII = new RangeAdditionII.Solution();

    @Test void m3n3ops22_33() {
        assertEquals(4, rangeAdditionII.maxCount(3, 3, new int[][]{{2,2}, {3,3}}));
    }
}