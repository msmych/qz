package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AddBinaryTest {

    private AddBinary addBinary = new AddBinary.Solution();

    @Test
    void a11b1output100() {
        assertEquals("100", addBinary.addBinary("11", "1"));
    }

    @Test void a1010b1011output10101() {
        assertEquals("10101", addBinary.addBinary("1010", "1011"));
    }
}