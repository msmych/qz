package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LargestTimeForGivenDigitsTest {

    private LargestTimeForGivenDigits largestTimeForGivenDigits = new LargestTimeForGivenDigits.Solution();

    @Test void input1234output2341() {
        assertEquals("23:41", largestTimeForGivenDigits.largestTimeFromDigits(new int[]{1, 2, 3, 4}));
    }

    @Test void input5555output_() {
        assertEquals("", largestTimeForGivenDigits.largestTimeFromDigits(new int[]{5, 5, 5, 5}));
    }

    @Test void input2444output_() {
        assertEquals("", largestTimeForGivenDigits.largestTimeFromDigits(new int[]{2, 4, 4, 4}));
    }

    @Test void input2366output_() {
        assertEquals("", largestTimeForGivenDigits.largestTimeFromDigits(new int[]{2, 3, 6, 6}));
    }

    @Test void input2066output0626() {
        assertEquals("06:26", largestTimeForGivenDigits.largestTimeFromDigits(new int[]{2, 0, 6, 6}));
    }
}