package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClumsyFactorialTest {

    private ClumsyFactorial clumsyFactorial = new ClumsyFactorial.Solution();

    @Test
    void input4output7() {
        assertEquals(7, clumsyFactorial.clumsy(4));
    }

    @Test void input10output12() {
        assertEquals(12, clumsyFactorial.clumsy(10));
    }
}