package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LargestPerimeterTriangleTest {

    private LargestPerimeterTriangle largestPerimeterTriangle = new LargestPerimeterTriangle.Solution();

    @Test void input212output5() {
        assertEquals(5, largestPerimeterTriangle.largestPerimeter(new int[]{2, 1, 2}));
    }

    @Test void input121output0() {
        assertEquals(0, largestPerimeterTriangle.largestPerimeter(new int[]{1, 2, 1}));
    }

    @Test void input3234output10() {
        assertEquals(10, largestPerimeterTriangle.largestPerimeter(new int[]{3, 2, 3, 4}));
    }

    @Test void input3623output8() {
        assertEquals(8, largestPerimeterTriangle.largestPerimeter(new int[]{3, 6, 2, 3}));
    }
}