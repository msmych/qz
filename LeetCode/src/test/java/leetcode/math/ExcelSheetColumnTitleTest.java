package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExcelSheetColumnTitleTest {

    private ExcelSheetColumnTitle excelSheetColumnTitle = new ExcelSheetColumnTitle.Solution();

    @Test void input1outputA() {
        assertEquals("A", excelSheetColumnTitle.convertToTitle(1));
    }

    @Test void input28outputAB() {
        assertEquals("AB", excelSheetColumnTitle.convertToTitle(28));
    }

    @Test void input701outputZY() {
        assertEquals("ZY", excelSheetColumnTitle.convertToTitle(701));
    }

    @Test void input26outputZ() {
        assertEquals("Z", excelSheetColumnTitle.convertToTitle(26));
    }

    @Test void input52outputAZ() {
        assertEquals("AZ", excelSheetColumnTitle.convertToTitle(52));
    }
}