package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LetterTilePossibilitiesTest {

    private LetterTilePossibilities letterTilePossibilities = new LetterTilePossibilities.Solution();

    @Test void AAB_8() {
        assertEquals(8, letterTilePossibilities.numTilePossibilities("AAB"));
    }

    @Test void AAABBC_188() {
        assertEquals(188, letterTilePossibilities.numTilePossibilities("AAABBC"));
    }
}