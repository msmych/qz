package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrimeArrangementsTest {

  private PrimeArrangements primeArrangements = new PrimeArrangements.Solution();

  @Test void n5() {
    assertEquals(12, primeArrangements.numPrimeArrangements(5));
  }

  @Test void n100() {
    assertEquals(682289015, primeArrangements.numPrimeArrangements(100));
  }

  @Test void n2() {
    assertEquals(1, primeArrangements.numPrimeArrangements(2));
  }
}
