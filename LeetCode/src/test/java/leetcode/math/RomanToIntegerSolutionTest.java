package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RomanToIntegerSolutionTest {

    private RomanToInteger romanToInteger = new RomanToInteger.Solution();

    @Test void III3() {
        assertEquals(3, romanToInteger.romanToInt("III"));
    }

    @Test void IV4() {
        assertEquals(4, romanToInteger.romanToInt("IV"));
    }

    @Test void IX9() {
        assertEquals(9, romanToInteger.romanToInt("IX"));
    }

    @Test void LVIII58() {
        assertEquals(58, romanToInteger.romanToInt("LVIII"));
    }

    @Test void MCMXCIV1994() {
        assertEquals(1994, romanToInteger.romanToInt("MCMXCIV"));
    }
}