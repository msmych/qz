package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PalindromeNumberTest {

    private PalindromeNumber palindromeNumber = new PalindromeNumber.Solution();

    @Test void input121true() {
        assertTrue(palindromeNumber.isPalindrome(121));
    }

    @Test void inputN121false() {
        assertFalse(palindromeNumber.isPalindrome(-121));
    }

    @Test void input10false() {
        assertFalse(palindromeNumber.isPalindrome(10));
    }
}