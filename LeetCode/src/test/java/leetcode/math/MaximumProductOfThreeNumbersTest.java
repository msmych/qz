package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaximumProductOfThreeNumbersTest {

    private MaximumProductOfThreeNumbers maximumProductOfThreeNumbers = new MaximumProductOfThreeNumbers.Solution();

    @Test void input123() {
        assertEquals(6, maximumProductOfThreeNumbers.maximumProduct(new int[]{1, 2, 3}));
    }

    @Test void input1234() {
        assertEquals(24, maximumProductOfThreeNumbers.maximumProduct(new int[]{1, 2, 3, 4}));
    }

    @Test void inputN3N1N22() {
        assertEquals(12, maximumProductOfThreeNumbers.maximumProduct(new int[]{-3, -1, -2, 2}));
    }

    @Test void input0004() {
        assertEquals(0, maximumProductOfThreeNumbers.maximumProduct(new int[]{0, 0, 0, 4}));
    }

    @Test void input4004() {
        assertEquals(0, maximumProductOfThreeNumbers.maximumProduct(new int[]{4, 0, 0, 4}));
    }
}