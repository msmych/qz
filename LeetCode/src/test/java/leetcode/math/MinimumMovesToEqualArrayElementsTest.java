package leetcode.math;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinimumMovesToEqualArrayElementsTest {

  private MinimumMovesToEqualArrayElements minimumMovesToEqualArrayElements =
    new MinimumMovesToEqualArrayElements.Solution();

  @Test void input123() {
    assertEquals(3, minimumMovesToEqualArrayElements.minMoves(new int[]{1, 2, 3}));
  }

  @Test @Disabled
  void input1_2147483647() {
    assertEquals(2147483646, minimumMovesToEqualArrayElements.minMoves(new int[]{1, 2147483647}));
  }
}
