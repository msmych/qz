package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxPointsOnALineTest {

    private MaxPointsOnALine maxPointsOnALine = new MaxPointsOnALine.Solution();

    @Test void input11_22_33output3() {
        assertEquals(3, maxPointsOnALine.maxPoints(new int[][]{{1, 1}, {2, 2}, {3, 3}}));
    }

    @Test void input11_32_53_41_23_14output4() {
        assertEquals(4, maxPointsOnALine.maxPoints(new int[][]{{1, 1}, {3, 2}, {5, 3}, {4, 1}, {2, 3}}));
    }
}