package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ReverseIntegerTest {

    private ReverseInteger reverseInteger = new ReverseInteger.Solution();

    @Test void input123output321() {
        assertEquals(321, reverseInteger.reverse(123));
    }

    @Test void inputN123outputN321() {
        assertEquals(-321, reverseInteger.reverse(-123));
    }

    @Test void input120output21() {
        assertEquals(21, reverseInteger.reverse(120));
    }

    @Test void input1534236469output0() {
        assertEquals(0, reverseInteger.reverse(1534236469));
    }
}