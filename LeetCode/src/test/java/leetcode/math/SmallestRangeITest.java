package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SmallestRangeITest {

    private SmallestRangeI smallestRangeI = new SmallestRangeI.Solution();

    @Test void A1K0output0() {
        assertEquals(0, smallestRangeI.smallestRangeI(new int[]{1}, 0));
    }

    @Test void A0_10K2output6() {
        assertEquals(6, smallestRangeI.smallestRangeI(new int[]{0, 10}, 2));
    }

    @Test void A1_3_6K3output0() {
        assertEquals(0, smallestRangeI.smallestRangeI(new int[]{1, 3, 6}, 3));
    }
}