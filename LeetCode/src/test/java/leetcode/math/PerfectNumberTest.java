package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PerfectNumberTest {

    private PerfectNumber perfectNumber = new PerfectNumber.Solution();

    @Test
    void false0() {
        assertFalse(perfectNumber.checkPerfectNumber(0));
    }

    @Test
    void true28() {
        assertTrue(perfectNumber.checkPerfectNumber(28));
    }

    @Test void false100000000() {
        assertFalse(perfectNumber.checkPerfectNumber(100_000_000));
    }
}