package leetcode.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConvertToBaseN2Test {

    private ConvertToBaseN2 convertToBaseN2 = new ConvertToBaseN2.Solution();

    @Test void input2output110() {
        assertEquals("110", convertToBaseN2.baseNeg2(2));
    }

    @Test void input3output111() {
        assertEquals("111", convertToBaseN2.baseNeg2(3));
    }

    @Test void input4output100() {
        assertEquals("100", convertToBaseN2.baseNeg2(4));
    }
}