package leetcode.recursion;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class LongestUnivaluePathTest {

    private LongestUnivaluePath longestUnivaluePath = new LongestUnivaluePath.Solution();

    @Test void tree5() {
        assertEquals(2, longestUnivaluePath.longestUnivaluePath(
                fromValAndLeftAndRight(5,
                        fromValAndLeftAndRight(4,
                                new TreeNode(1),
                                new TreeNode(1)),
                        fromValAndLeftAndRight(5,
                                null,
                                new TreeNode(5)))));
    }

    @Test void tree1() {
        assertEquals(2, longestUnivaluePath.longestUnivaluePath(
                fromValAndLeftAndRight(1,
                        fromValAndLeftAndRight(4,
                                new TreeNode(4),
                                new TreeNode(4)),
                        fromValAndLeftAndRight(5,
                                null,
                                new TreeNode(5)))));
    }
}