package leetcode.recursion;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KthSymbolInGrammarTest {

    private KthSymbolInGrammar kthSymbolInGrammar = new KthSymbolInGrammar.Solution();

    @Test void n1k1output0() {
        assertEquals(0, kthSymbolInGrammar.kthGrammar(1, 1));
    }

    @Test void n2k1output0() {
        assertEquals(0, kthSymbolInGrammar.kthGrammar(2, 1));
    }

    @Test void n2k2output1() {
        assertEquals(1, kthSymbolInGrammar.kthGrammar(2, 2));
    }

    @Test void n4k5output1() {
        assertEquals(1, kthSymbolInGrammar.kthGrammar(4, 5));
    }

    @Test void n30k434991989output0() {
        assertEquals(0, kthSymbolInGrammar.kthGrammar(30, 434991989));
    }
}