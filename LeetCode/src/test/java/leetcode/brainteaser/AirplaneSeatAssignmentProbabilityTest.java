package leetcode.brainteaser;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AirplaneSeatAssignmentProbabilityTest {

  private AirplaneSeatAssignmentProbability airplaneSeatAssignmentProbability = new AirplaneSeatAssignmentProbability.Solution();

  @Test
  void n1() {
    assertEquals(1.0, airplaneSeatAssignmentProbability.nthPersonGetsNthSeat(1));
  }

  @Test
  void n2() {
    assertEquals(0.5, airplaneSeatAssignmentProbability.nthPersonGetsNthSeat(2));
  }
}
