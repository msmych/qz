package leetcode.brainteaser;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class MovingStonesUntilConsecutiveTest {

    private MovingStonesUntilConsecutive movingStonesUntilConsecutive = new MovingStonesUntilConsecutive.Solution();

    @Test
    void a1b2c5output12() {
        assertArrayEquals(new int[]{1, 2}, movingStonesUntilConsecutive.numMovesStones(1, 2, 5));
    }

    @Test void a4b3c2output00() {
        assertArrayEquals(new int[]{0, 0}, movingStonesUntilConsecutive.numMovesStones(4, 3, 2));
    }

    @Test void a2b4c1output11() {
        assertArrayEquals(new int[]{1, 1}, movingStonesUntilConsecutive.numMovesStones(2, 4, 1));
    }
}