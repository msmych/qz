package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertCollectionContainsAll;

class SubdomainVisitCountTest {

    private SubdomainVisitCount subdomainVisitCount = new SubdomainVisitCount.Solution();

    @Test void discuss_leetcode() {
        assertCollectionContainsAll(subdomainVisitCount.subdomainVisits(new String[]{"9001 discuss.leetcode.com"}),
                "9001 discuss.leetcode.com", "9001 leetcode.com", "9001 com");
    }

    @Test void google_mail() {
        assertCollectionContainsAll(subdomainVisitCount.subdomainVisits(
                        new String[]{"900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"}),
                "901 mail.com","50 yahoo.com","900 google.mail.com","5 wiki.org","5 org","1 intel.mail.com","951 com");
    }
}