package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestHarmoniousSubsequenceTest {

    private LongestHarmoniousSubsequence longestHarmoniousSubsequence = new LongestHarmoniousSubsequence.Solution();

    @Test void input13225237() {
        assertEquals(5, longestHarmoniousSubsequence.findLHS(new int[]{1, 3, 2, 2, 5, 2, 3, 7}));
    }

    @Test void input1111() {
        assertEquals(0, longestHarmoniousSubsequence.findLHS(new int[]{1, 1, 1, 1}));
    }
}