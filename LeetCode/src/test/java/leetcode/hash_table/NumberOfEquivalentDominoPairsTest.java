package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfEquivalentDominoPairsTest {

    private NumberOfEquivalentDominoPairs numberOfEquivalentDominoPairs = new NumberOfEquivalentDominoPairs.Solution();

    @Test void domino12213456() {
        assertEquals(1, numberOfEquivalentDominoPairs.numEquivDominoPairs(new int[][]{{1,2},{2,1},{3,4},{5,6}}));
    }

    @Test void domino1221111222() {
        assertEquals(3, numberOfEquivalentDominoPairs.numEquivDominoPairs(new int[][]{{1,2},{2,1},{1,1},{1,2},{2,2}}));
    }
}