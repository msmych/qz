package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KeyboardRowTest {

    private KeyboardRow keyboardRow = new KeyboardRow.Solution();

    @Test void alaska_dad() {
        assertArrayEquals(new String[]{"Alaska", "Dad"},
                keyboardRow.findWords(new String[]{"Hello", "Alaska", "Dad", "Peace"}));
    }
}