package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinimumWindowSubstringTest {

  private MinimumWindowSubstring minimumWindowSubstring = new MinimumWindowSubstring.Solution();

  @Test
  void ADOBECODEBANC_ABC_BANC() {
    assertEquals("BANC", minimumWindowSubstring.minWindow("ADOBECODEBANC", "ABC"));
  }

  @Test
  void a_aa_() {
    assertEquals("", minimumWindowSubstring.minWindow("a", "aa"));
  }
}
