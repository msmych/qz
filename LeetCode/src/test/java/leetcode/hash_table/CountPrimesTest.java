package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountPrimesTest {

    private CountPrimes countPrimes = new CountPrimes.Solution();

    @Test void input10output4() {
        assertEquals(4, countPrimes.countPrimes(10));
    }

    @Test void input100000output9592() {
        assertEquals(9592, countPrimes.countPrimes(100000));
    }

    @Test void input150000() {
        assertEquals(13848, countPrimes.countPrimes(150000));
    }
}