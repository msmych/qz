package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShortestCompletingWordTest {

    private ShortestCompletingWord shortestCompletingWord = new ShortestCompletingWord.Solution();

    @Test void step() {
        assertEquals("steps", shortestCompletingWord.shortestCompletingWord("1s3 PSt",
                new String[]{"step", "steps", "stripe", "stepple"}));
    }

    @Test void looks() {
        assertEquals("pest", shortestCompletingWord.shortestCompletingWord("1s3 456",
                new String[]{"looks", "pest", "stew", "show"}));
    }
}