package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RandomizedSetTest {

    @Test void insert1remove2insert2getRandom_remove1insert2getRandom() {
        AbstractRandomizedSet randomizedSet = new RandomizedSet();
        assertTrue(randomizedSet.insert(1));
        assertFalse(randomizedSet.remove(2));
        assertTrue(randomizedSet.insert(2));
        int random = randomizedSet.getRandom();
        assertTrue(random == 1 || random == 2);
        assertTrue(randomizedSet.remove(1));
        assertFalse(randomizedSet.insert(2));
        assertEquals(2, randomizedSet.getRandom());
    }

    @Test void remove0remove0insert0getRandom_remove0insert0() {
        AbstractRandomizedSet randomizedSet = new RandomizedSet();
        assertFalse(randomizedSet.remove(0));
        assertFalse(randomizedSet.remove(0));
        assertTrue(randomizedSet.insert(0));
        assertEquals(0, randomizedSet.getRandom());
        assertTrue(randomizedSet.remove(0));
        assertTrue(randomizedSet.insert(0));
    }

    @Test void insertN1removeN2insertN2getRandom_removeN1insertN2getRandom() {
        AbstractRandomizedSet randomizedSet = new RandomizedSet();
        assertTrue(randomizedSet.insert(-1));
        assertFalse(randomizedSet.remove(-2));
        assertTrue(randomizedSet.insert(-2));
        int random = randomizedSet.getRandom();
        assertTrue(random == -1 || random == -2);
        assertTrue(randomizedSet.remove(-1));
        assertFalse(randomizedSet.insert(-2));
        assertEquals(-2, randomizedSet.getRandom());
    }

    @Test void insert0insert1remove0insert2remove1getRandom() {
        AbstractRandomizedSet randomizedSet = new RandomizedSet();
        assertTrue(randomizedSet.insert(0));
        assertTrue(randomizedSet.insert(1));
        assertTrue(randomizedSet.remove(0));
        assertTrue(randomizedSet.insert(2));
        assertTrue(randomizedSet.remove(1));
        assertEquals(2, randomizedSet.getRandom());
    }
}