package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class VerifyingAnAlienDictionaryTest {

    private VerifyingAnAlienDictionary verifyingAnAlienDictionary = new VerifyingAnAlienDictionary.Solution();

    @Test void hello_leetcode$hlabcdefgijkmnopqrstuvwxyz() {
        assertTrue(verifyingAnAlienDictionary.isAlienSorted(
                new String[]{"hello", "leetcode"},
                "hlabcdefgijkmnopqrstuvwxyz"));
    }

    @Test void word_world_row$worldabcefghijkmnpqstuvxyz() {
        assertFalse(verifyingAnAlienDictionary.isAlienSorted(
                new String[]{"word", "world", "row"},
                "worldabcefghijkmnpqstuvxyz"));
    }

    @Test void apple_app$abcdefghijklmnopqrstuvwxyz() {
        assertFalse(verifyingAnAlienDictionary.isAlienSorted(
                new String[]{"apple", "app"},
                "abcdefghijklmnopqrstuvwxyz"));
    }
}