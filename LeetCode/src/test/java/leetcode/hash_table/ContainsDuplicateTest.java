package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ContainsDuplicateTest {

    private ContainsDuplicate containsDuplicate = new ContainsDuplicate.Solution();

    @Test void input1231true() {
        assertTrue(containsDuplicate.containsDuplicate(new int[]{1, 2, 3, 1}));
    }

    @Test void input1234false() {
        assertFalse(containsDuplicate.containsDuplicate(new int[]{1, 2, 3, 4}));
    }

    @Test void input1113343242true() {
        assertTrue(containsDuplicate.containsDuplicate(new int[]{1, 1, 1, 3, 3, 4, 3, 2, 4, 2}));
    }
}