package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SingleNumberIITest {

    private SingleNumber singleNumber = new SingleNumber.SolutionII();

    @Test void nums2232output3() {
        assertEquals(3, singleNumber.singleNumber(new int[]{2, 2, 3, 2}));
    }

    @Test void nums010101_99output99() {
        assertEquals(99, singleNumber.singleNumber(new int[]{0, 1, 0, 1, 0, 1, 99}));
    }

    @Test void nums2147483647() {
        assertEquals(2147483647, singleNumber.singleNumber(
                new int[]{43,16,45,89,45,-2147483648,45,2147483646,-2147483647,-2147483648,43,2147483647,-2147483646,
                        -2147483648,89,-2147483646,89,-2147483646,-2147483647,2147483646,-2147483647,16,16,2147483646,43}));
    }
}