package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ContainsDuplicateIITest {

    private ContainsDuplicateII containsDuplicateII = new ContainsDuplicateII.Solution();

    @Test void nums1231k3true() {
        assertTrue(containsDuplicateII.containsNearbyDuplicate(new int[]{1, 2, 3, 1}, 3));
    }

    @Test void nums1011k1true() {
        assertTrue(containsDuplicateII.containsNearbyDuplicate(new int[]{1, 0, 1, 1}, 1));
    }

    @Test void nums123123k2false() {
        assertFalse(containsDuplicateII.containsNearbyDuplicate(new int[]{1, 2, 3, 1, 2, 3}, 2));
    }
}