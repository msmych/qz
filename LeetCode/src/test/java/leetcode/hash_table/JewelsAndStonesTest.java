package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JewelsAndStonesTest {

    private JewelsAndStones jewelsAndStones = new JewelsAndStones.Solution();

    @Test void j_aA_s_aAAbbbb_output3() {
        assertEquals(3, jewelsAndStones.numJewelsInStones("aA", "aAAbbbb"));
    }

    @Test void j_z_s_ZZ_output0() {
        assertEquals(0, jewelsAndStones.numJewelsInStones("z", "ZZ"));
    }
}