package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WordPatternTest {

  private WordPattern wordPattern = new WordPattern.Solution();

  @Test void dog_cat_cat_dog() {
    assertTrue(wordPattern.wordPattern("abba", "dog cat cat dog"));
  }

  @Test void dog_cat_cat_fish() {
    assertFalse(wordPattern.wordPattern("abba", "dog cat cat fish"));
  }

  @Test void aaaa() {
    assertFalse(wordPattern.wordPattern("aaaa", "dog cat cat dog"));
  }

  @Test void dog_dog_dog_dog() {
    assertFalse(wordPattern.wordPattern("abba", "dog dog dog dog"));
  }
}
