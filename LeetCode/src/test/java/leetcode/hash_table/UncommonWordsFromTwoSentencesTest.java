package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UncommonWordsFromTwoSentencesTest {

    private UncommonWordsFromTwoSentences uncommonWordsFromTwoSentences =
            new UncommonWordsFromTwoSentences.Solution();

    @Test void sweet_sour() {
        assertArrayEquals(new String[]{"sweet", "sour"},
                uncommonWordsFromTwoSentences.uncommonFromSentences("this apple is sweet", "this apple is sour"));
    }

    @Test void banana() {
        assertArrayEquals(new String[]{"banana"},
                uncommonWordsFromTwoSentences.uncommonFromSentences("apple apple", "banana"));
    }
}