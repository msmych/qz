package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IslandPerimeterTest {

    private IslandPerimeter islandPerimeter = new IslandPerimeter.Solution();

    @Test
    void island0100() {
        assertEquals(16, islandPerimeter.islandPerimeter(new int[][]{
            {0, 1, 0, 0},
            {1, 1, 1, 0},
            {0, 1, 0, 0},
            {1, 1, 0, 0}
        }));
    }
}