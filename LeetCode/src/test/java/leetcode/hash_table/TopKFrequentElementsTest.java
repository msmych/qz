package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class TopKFrequentElementsTest {

  private TopKFrequentElements topKFrequentElements = new TopKFrequentElements.Solution();

  @Test
  void nums111223k2output12() {
    assertArrayEquals(new Integer[]{1, 2},
      topKFrequentElements.topKFrequent(new int[]{1, 1, 1, 2, 2, 3}, 2).toArray(new Integer[]{}));
  }

  @Test
  void nums1k1output1() {
    assertArrayEquals(new Integer[]{1},
      topKFrequentElements.topKFrequent(new int[]{1}, 1).toArray(new Integer[]{}));
  }

  @Test
  void nums41N12N123k2outputN12() {
    assertArrayEquals(new Integer[]{-1, 2},
      topKFrequentElements.topKFrequent(new int[]{4, 1, -1, 2, -1, 2, 3}, 2).toArray(new Integer[]{}));
  }
}
