package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SetMismatchTest {

    private SetMismatch setMismatch = new SetMismatch.Solution();

    @Test void input1224output23() {
        assertArrayEquals(new int[]{2, 3}, setMismatch.findErrorNums(new int[]{1, 2, 2, 4}));
    }

    @Test void input123445output46() {
        assertArrayEquals(new int[]{4, 6}, setMismatch.findErrorNums(new int[]{1, 2, 3, 4, 4, 5}));
    }
}