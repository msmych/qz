package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionEquivalentToArrays;

class Sum4Test {

  private Sum4 sum4 = new Sum4.Solution();

  @Test
  void nums10N10N22target0() {
    assertListCollectionEquivalentToArrays(new Integer[][]{
      {-1, 0, 0, 1},
      {-2, -1, 1, 2},
      {-2, 0, 0, 2}
    }, sum4.fourSum(new int[]{1, 0, -1, 0, -2, 2}, 0));
  }
}
