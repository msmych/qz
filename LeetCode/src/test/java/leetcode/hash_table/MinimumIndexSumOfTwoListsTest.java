package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class MinimumIndexSumOfTwoListsTest {

    private MinimumIndexSumOfTwoLists minimumIndexSumOfTwoLists = new MinimumIndexSumOfTwoLists.Solution();

    @Test void Shogun__Tapioca_Express__Burger_King_KFC___Piatti__The_Grill_at_Torrey_Pines__Hungry_Hunter_Steakhouse_Shogun___Shogun() {
        assertArrayEquals(new String[]{"Shogun"}, minimumIndexSumOfTwoLists.findRestaurant(
                new String[]{"Shogun", "Tapioca Express", "Burger King", "KFC"},
                new String[]{"Piatti", "The Grill at Torrey Pines", "Hungry Hunter Steakhouse", "Shogun"}));
    }

    @Test void Shogun__Tapioca_Express__Burger_King_KFC___KFC__Shogun__Burger_King___Shogun() {
        assertArrayEquals(new String[]{"Shogun"}, minimumIndexSumOfTwoLists.findRestaurant(
                new String[]{"Shogun", "Tapioca Express", "Burger King", "KFC"},
                new String[]{"KFC", "Shogun", "Burger King"}));
    }
}