package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SingleNumberTest {

    private SingleNumber singleNumber = new SingleNumber.Solution(),
            sumSolution = new SingleNumber.SumSolution();

    @Test
    void input221output1() {
        int[] nums = new int[]{2, 2, 1};
        assertEquals(1, singleNumber.singleNumber(nums));
        assertEquals(1, sumSolution.singleNumber(nums));
    }

    @Test void input41212output4() {
        int[] nums = new int[]{4, 1, 2, 1, 2};
        assertEquals(4, singleNumber.singleNumber(nums));
        assertEquals(4, sumSolution.singleNumber(nums));
    }
}