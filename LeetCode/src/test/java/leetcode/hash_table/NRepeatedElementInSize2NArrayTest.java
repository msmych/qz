package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NRepeatedElementInSize2NArrayTest {

    private NRepeatedElementInSize2NArray nRepeatedElementInSize2NArray = new NRepeatedElementInSize2NArray.Solution();

    @Test void input1233output3() {
        assertEquals(3, nRepeatedElementInSize2NArray.repeatedNTimes(new int[]{1, 2, 3, 3}));
    }

    @Test void input212532output2() {
        assertEquals(2, nRepeatedElementInSize2NArray.repeatedNTimes(new int[]{2, 1, 2, 5, 3, 2}));
    }

    @Test void input51525354output5() {
        assertEquals(5, nRepeatedElementInSize2NArray.repeatedNTimes(new int[]{5, 1, 5, 2, 5, 3, 5, 4}));
    }
}