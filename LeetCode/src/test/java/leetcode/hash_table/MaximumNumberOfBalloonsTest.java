package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaximumNumberOfBalloonsTest {

  private MaximumNumberOfBalloons maximumNumberOfBalloons = new MaximumNumberOfBalloons.Solution();

  @Test void nlaebolko() {
    assertEquals(1, maximumNumberOfBalloons.maxNumberOfBalloons("nlaebolko"));
  }

  @Test void loonbalxballpoon() {
    assertEquals(2, maximumNumberOfBalloons.maxNumberOfBalloons("loonbalxballpoon"));
  }

  @Test void leetcode() {
    assertEquals(0, maximumNumberOfBalloons.maxNumberOfBalloons("leetcode"));
  }

  @Test void lloo() {
    assertEquals(0, maximumNumberOfBalloons.maxNumberOfBalloons("lloo"));
  }

  @Test void balon() {
    assertEquals(0, maximumNumberOfBalloons.maxNumberOfBalloons("balon"));
  }
}
