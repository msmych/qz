package leetcode.hash_table;

import leetcode.hash_table.EmployeeImportance.Employee;
import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class EmployeeImportanceTest {

    private EmployeeImportance employeeImportance = new EmployeeImportance.Solution();

    @Test void output11() {
        Employee e1 = new Employee() {{
            id = 1;
            importance = 5;
            subordinates = asList(2, 3);
        }};
        Employee e2 = new Employee() {{
            id = 2;
            importance = 3;
            subordinates = emptyList();
        }};
        Employee e3 = new Employee() {{
            id = 3;
            importance = 3;
            subordinates = emptyList();
        }};
        assertEquals(11, employeeImportance.getImportance(asList(e1, e2, e3), 1));
    }
}