package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DistributeCandiesTest {

    private DistributeCandies distributeCandies = new DistributeCandies.Solution();

    @Test void candies112233() {
        assertEquals(3, distributeCandies.distributeCandies(new int[]{1,1,2,2,3,3}));
    }

    @Test void candies1123() {
        assertEquals(2, distributeCandies.distributeCandies(new int[]{1,1,2,3}));
    }

    @Test void candies1111222333() {
        assertEquals(3, distributeCandies.distributeCandies(new int[]{1,1,1,1,2,2,2,3,3,3}));
    }
}