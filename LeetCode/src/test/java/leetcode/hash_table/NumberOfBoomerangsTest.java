package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfBoomerangsTest {

  private NumberOfBoomerangs numberOfBoomerangs = new NumberOfBoomerangs.Solution();

  @Test void input001020() {
    assertEquals(2, numberOfBoomerangs.numberOfBoomerangs(new int[][]{{0, 0}, {1, 0}, {2, 0}}));
  }
}
