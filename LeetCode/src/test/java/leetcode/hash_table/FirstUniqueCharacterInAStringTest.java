package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FirstUniqueCharacterInAStringTest {

    private FirstUniqueCharacterInAString firstUniqueCharacterInAString = new FirstUniqueCharacterInAString.Solution();

    @Test void leetcode0() {
        assertEquals(0, firstUniqueCharacterInAString.firstUniqChar("leetcode"));
    }

    @Test void loveleetcode2() {
        assertEquals(2, firstUniqueCharacterInAString.firstUniqChar("loveleetcode"));
    }

    @Test void aadadaadN1() {
        assertEquals(-1, firstUniqueCharacterInAString.firstUniqChar("aadadaad"));
    }
}