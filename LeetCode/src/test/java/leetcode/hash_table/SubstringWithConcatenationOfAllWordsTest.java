package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SubstringWithConcatenationOfAllWordsTest {

    private SubstringWithConcatenationOfAllWords substringWithConcatenationOfAllWords =
            new SubstringWithConcatenationOfAllWords.Solution();

    @Test void foo_bar() {
        List<Integer> result = substringWithConcatenationOfAllWords.findSubstring(
                "barfoothefoobarman", new String[]{"foo" , "bar"});
        assertEquals(2, result.size());
        assertTrue(result.contains(0));
        assertTrue(result.contains(9));
    }

    @Test void word_good_best_word() {
        assertEquals(0, substringWithConcatenationOfAllWords.findSubstring(
                "wordgoodgoodgoodbestword", new String[]{"word", "good", "best", "word"}).size());
    }

    @Test void aa() {
        assertArrayEquals(new Integer[]{0, 1, 2}, substringWithConcatenationOfAllWords.findSubstring(
                "aaaaaaaa", new String[]{"aa", "aa", "aa"}).toArray(new Integer[]{}));
    }
}