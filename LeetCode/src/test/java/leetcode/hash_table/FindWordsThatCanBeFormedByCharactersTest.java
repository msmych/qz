package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FindWordsThatCanBeFormedByCharactersTest {

  private FindWordsThatCanBeFormedByCharacters findWordsThatCanBeFormedByCharacters =
    new FindWordsThatCanBeFormedByCharacters.Solution();

  @Test
  void atach() {
    assertEquals(6, findWordsThatCanBeFormedByCharacters.countCharacters(
      new String[]{"cat", "bt", "hat", "tree"}, "atach"));
  }

  @Test void welldonehoneyr() {
    assertEquals(10, findWordsThatCanBeFormedByCharacters.countCharacters(
      new String[]{"hello","world","leetcode"}, "welldonehoneyr"));
  }
}
