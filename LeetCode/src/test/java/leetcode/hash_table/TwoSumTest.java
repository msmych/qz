package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class TwoSumTest {

    private TwoSum twoSum = new TwoSum.Solution();
    private TwoSum hashMapTwoSum = new TwoSum.HashMapSolution();

    @Test
    void nums2_7_11_15target9result0_1() {
        int[] nums01 = new int[]{0, 1};
        int[] nums271115 = new int[]{2, 7, 11, 15};
        assertArrayEquals(nums01, twoSum.twoSum(nums271115, 9));
        assertArrayEquals(nums01, hashMapTwoSum.twoSum(nums271115, 9));
    }

    @Test void nums324target6output12() {
        int[] nums12 = new int[]{1, 2};
        int[] nums324 = new int[]{3, 2, 4};
        assertArrayEquals(nums12, twoSum.twoSum(nums324, 6));
        assertArrayEquals(nums12, hashMapTwoSum.twoSum(nums324, 6));
    }
}