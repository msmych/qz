package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HappyNumberTest {

    private HappyNumber happyNumber = new HappyNumber.Solution();

    @Test void input19true() {
        assertTrue(happyNumber.isHappy(19));
    }
}