package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestPalindromeTest {

    private LongestPalindrome longestPalindrome = new LongestPalindrome.Solution();

    @Test void abccccdd() {
        assertEquals(7, longestPalindrome.longestPalindrome("abccccdd"));
    }

    @Test void abccccddDrnnasdLOERdasdbjDJioeee() {
        assertEquals(21, longestPalindrome.longestPalindrome("abccccddDrnnasdLOERdasdbjDJioeee"));
    }
}