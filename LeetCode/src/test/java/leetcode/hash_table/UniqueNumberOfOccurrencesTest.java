package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UniqueNumberOfOccurrencesTest {

  private UniqueNumberOfOccurrences uniqueNumberOfOccurrences = new UniqueNumberOfOccurrences.Solution();

  @Test
  void arr122113() {
    assertTrue(uniqueNumberOfOccurrences.uniqueOccurrences(new int[]{1, 2, 2, 1, 1, 3}));
  }

  @Test
  void arr12() {
    assertFalse(uniqueNumberOfOccurrences.uniqueOccurrences(new int[]{1, 2}));
  }

  @Test
  void arrN3() {
    assertTrue(uniqueNumberOfOccurrences.uniqueOccurrences(new int[]{-3, 0, 1, -3, 1, 1, 1, -3, 10, 0}));
  }
}
