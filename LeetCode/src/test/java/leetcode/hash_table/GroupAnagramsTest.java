package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionContainsArrays;

class GroupAnagramsTest {

    private GroupAnagrams groupAnagrams = new GroupAnagrams.Solution();

    @Test void eat_tea_tan_ate_nat_bat() {
        assertListCollectionContainsArrays(new String[][]{
                {"bat"},
                {"nat", "tan"},
                {"ate", "eat", "tea"}
                }, groupAnagrams.groupAnagrams(new String[]{"eat", "tea", "tan", "ate", "nat", "bat"}));
    }
}