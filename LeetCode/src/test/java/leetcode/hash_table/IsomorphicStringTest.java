package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IsomorphicStringTest {

    private IsomorphicString isomorphicString = new IsomorphicString.Solution();

    @Test
    void s_egg_t_add_true() {
        assertTrue(isomorphicString.isIsomorphic("egg", "add"));
    }

    @Test void s_foo_t_bar_false() {
        assertFalse(isomorphicString.isIsomorphic("foo", "bar"));
    }

    @Test void s_paper_t_title_true() {
        assertTrue(isomorphicString.isIsomorphic("paper", "title"));
    }

    @Test void s_ab_t_aa_false() {
        assertFalse(isomorphicString.isIsomorphic("ab", "aa"));
    }
}