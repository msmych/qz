package leetcode.hash_table;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BullsAndCowsTest {

    private BullsAndCows bullsAndCows = new BullsAndCows.Solution();

    @Test void secret1807guess7810() {
        assertEquals("1A3B", bullsAndCows.getHint("1807", "7810"));
    }

    @Test void secret1123guess0111() {
        assertEquals("1A1B", bullsAndCows.getHint("1123", "0111"));
    }
}