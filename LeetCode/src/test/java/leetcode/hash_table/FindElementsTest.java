package leetcode.hash_table;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class FindElementsTest {

  @Test
  void tree02() {
    FindElements findElements = new FindElements(fromValAndLeftAndRight(-1, null, new TreeNode(-1)));
    assertFalse(findElements.find(1));
    assertTrue(findElements.find(2));
  }

  @Test
  void tree01234() {
    FindElements findElements = new FindElements(fromValAndLeftAndRight(-1,
      fromValAndLeftAndRight(-1, new TreeNode(-1), new TreeNode(-1)),
      new TreeNode(-1)));
    assertTrue(findElements.find(1));
    assertTrue(findElements.find(3));
    assertFalse(findElements.find(5));
  }

  @Test
  void tree02511() {
    FindElements findElements = new FindElements(fromValAndLeftAndRight(-1, null,
      fromValAndLeft(-1, fromValAndLeft(-1, new TreeNode(-1)))));
    assertTrue(findElements.find(2));
    assertFalse(findElements.find(3));
    assertFalse(findElements.find(4));
    assertTrue(findElements.find(5));
  }
}