package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RestoreIpAddressesTest {

    private RestoreIpAddresses restoreIpAddresses = new RestoreIpAddresses.Solution();

    @Test void ip25525511135() {
        assertArrayEquals(new String[]{"255.255.11.135", "255.255.111.35"},
                restoreIpAddresses.restoreIpAddresses("25525511135").toArray(new String[]{}));
    }

    @Test void ip0000() {
        assertArrayEquals(new String[]{"0.0.0.0"},
                restoreIpAddresses.restoreIpAddresses("0000").toArray(new String[]{}));
    }

    @Test void ip010010() {
        assertArrayEquals(new String[]{"0.10.0.10","0.100.1.0"},
                restoreIpAddresses.restoreIpAddresses("010010").toArray(new String[]{}));
    }
}