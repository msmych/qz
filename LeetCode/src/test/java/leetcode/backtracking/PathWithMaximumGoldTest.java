package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PathWithMaximumGoldTest {

  private PathWithMaximumGold pathWithMaximumGold = new PathWithMaximumGold.Solution();

  @Test
  void gold24() {
    assertEquals(24, pathWithMaximumGold.getMaximumGold(new int[][]{
      {0, 6, 0},
      {5, 8, 7},
      {0, 9, 0}
    }));
  }

  @Test
  void gold28() {
    assertEquals(28, pathWithMaximumGold.getMaximumGold(new int[][]{
      {1, 0, 7},
      {2, 0, 6},
      {3, 4, 5},
      {0, 3, 0},
      {9, 0, 20}
    }));
  }

  @Test
  void gold129() {
    assertEquals(129, pathWithMaximumGold.getMaximumGold(new int[][]{
      {0, 0, 0, 0, 0, 0, 32, 0, 0, 20},
      {0, 0, 2, 0, 0, 0, 0, 40, 0, 32},
      {13, 20, 36, 0, 0, 0, 20, 0, 0, 0},
      {0, 31, 27, 0, 19, 0, 0, 25, 18, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 18, 0, 6},
      {0, 0, 0, 25, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 21, 0, 30, 0, 0, 0, 0},
      {19, 10, 0, 0, 34, 0, 2, 0, 0, 27},
      {0, 0, 0, 0, 0, 34, 0, 0, 0, 0}
    }));
  }
}