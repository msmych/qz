package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NQueensIITest {

    private NQueensII nQueensII = new NQueensII.Solution();

    @Test void input4output2() {
        assertEquals(2, nQueensII.totalNQueens(4));
    }

    @Test void input5output10() {
        assertEquals(10, nQueensII.totalNQueens(5));
    }

    @Test void input6output4() {
        assertEquals(4, nQueensII.totalNQueens(6));
    }

    @Test void input7output40() {
        assertEquals(40, nQueensII.totalNQueens(7));
    }

    @Test void input8output92() {
        assertEquals(92, nQueensII.totalNQueens(8));
    }
}