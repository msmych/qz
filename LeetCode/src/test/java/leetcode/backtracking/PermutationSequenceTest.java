package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PermutationSequenceTest {

    private PermutationSequence permutationSequence = new PermutationSequence.Solution();

    @Test void n3k3output213() {
        assertEquals("213", permutationSequence.getPermutation(3, 3));
    }

    @Test void n4k9output2314() {
        assertEquals("2314", permutationSequence.getPermutation(4, 9));
    }
}