package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static utils.TestUtils.assertListCollectionContainsArrays;

class CombinationsTest {

    private Combinations combinations = new Combinations.Solution();

    @Test void n4k2() {
        assertListCollectionContainsArrays(new Integer[][]{
                {2, 4},
                {3, 4},
                {2, 3},
                {1, 2},
                {1, 3},
                {1, 4}
        }, combinations.combine(4, 2));
    }

    @Test void n5k3() {
        assertListCollectionContainsArrays(new Integer[][]{
                {1, 2, 3},
                {1, 2, 4},
                {1, 2, 5},
                {1, 3, 4},
                {1, 3, 5},
                {2, 3, 4},
                {1, 4, 5},
                {2, 3, 5},
                {2, 4, 5},
                {3, 4, 5}
        }, combinations.combine(5, 3));
    }

    @Test
    void n20k16() {
        assertEquals(4845, combinations.combine(20, 16).size());
    }
}