package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GrayCodeTest {

    private GrayCode grayCode = new GrayCode.Solution();

    @Test void input2output0132() {
        assertArrayEquals(new Integer[]{0, 1, 3, 2}, grayCode.grayCode(2).toArray(new Integer[]{}));
    }

    @Test void input0output0() {
        assertArrayEquals(new Integer[]{0}, grayCode.grayCode(0).toArray(new Integer[]{}));
    }
}