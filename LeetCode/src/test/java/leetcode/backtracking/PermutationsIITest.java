package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionContainsArrays;

class PermutationsIITest {

    private PermutationsII permutationsII = new PermutationsII.Solution();

    @Test void input112() {
        assertListCollectionContainsArrays(new Integer[][]{
                {1, 1, 2},
                {1, 2, 1},
                {2, 1, 1}
        }, permutationsII.permuteUnique(new int[]{1, 1, 2}));
    }
}