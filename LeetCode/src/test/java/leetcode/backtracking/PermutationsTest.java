package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionContainsArrays;

class PermutationsTest {

  private Permutations permutations = new Permutations.Solution();

  @Test
  void input123() {
    assertListCollectionContainsArrays(new Integer[][]{
      {1, 2, 3},
      {1, 3, 2},
      {2, 1, 3},
      {2, 3, 1},
      {3, 1, 2},
      {3, 2, 1}
    }, permutations.permute(new int[]{1, 2, 3}));
  }
}
