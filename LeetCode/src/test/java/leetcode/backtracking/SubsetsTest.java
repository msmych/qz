package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionContainsArrays;

class SubsetsTest {

  private Subsets subsets = new Subsets.Solution();

  @Test
  void nums123() {
    assertListCollectionContainsArrays(new Integer[][]{
      {}, {1}, {2}, {3},
      {1, 2}, {1, 3}, {2, 3},
      {1, 2, 3}
    }, subsets.subsets(new int[]{1, 2, 3}));
  }
}
