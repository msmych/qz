package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static utils.TestUtils.assertListCollectionContainsArrays;

class NQueensTest {

    private NQueens nQueens = new NQueens.Solution();

    @Test void input4() {
        assertListCollectionContainsArrays(new String[][]{
                {
                    ".Q..",
                    "...Q",
                    "Q...",
                    "..Q."
                }, {
                    "..Q.",
                    "Q...",
                    "...Q",
                    ".Q.."
                }
        }, nQueens.solveNQueens(4));
    }

    @Test void input5() {
        assertEquals(10, nQueens.solveNQueens(5).size());
    }

    @Test
    void input6() {
        assertEquals(4, nQueens.solveNQueens(6).size());
    }

    @Test void input7() {
        assertEquals(40, nQueens.solveNQueens(7).size());
    }

    @Test
    void input8() {
        assertEquals(92, nQueens.solveNQueens(8).size());
    }
}