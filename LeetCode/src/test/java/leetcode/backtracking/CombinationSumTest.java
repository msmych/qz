package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionContainsArrays;

class CombinationSumTest {

    private CombinationSum combinationSum = new CombinationSum.Solution();

    @Test void candidates2367target7solution7_223() {
        assertListCollectionContainsArrays(new Integer[][]{
                {7},
                {2, 2, 3}
        }, combinationSum.combinationSum(new int[]{2, 3, 6, 7}, 7));
    }

    @Test void candidates235target8solution2222_233_35() {
        assertListCollectionContainsArrays(new Integer[][]{
                {2, 2, 2, 2},
                {2, 3, 3},
                {3, 5}
        }, combinationSum.combinationSum(new int[]{2, 3, 5}, 8));
    }
}