package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VerbalBacktrackingPuzzleTest {

  private VerbalBacktrackingPuzzle verbalBacktrackingPuzzle = new VerbalBacktrackingPuzzle.Solution();

  @Test
  void send_more_money() {
    assertTrue(verbalBacktrackingPuzzle.isSolvable(new String[]{"SEND", "MORE"}, "MONEY"));
  }

  @Test
  void six_seven_seven_twenty() {
    assertTrue(verbalBacktrackingPuzzle.isSolvable(new String[]{"SIX", "SEVEN", "SEVEN"}, "TWENTY"));
  }

  @Test
  void this_is_too_funny() {
    assertTrue(verbalBacktrackingPuzzle.isSolvable(new String[]{"THIS", "IS", "TOO"}, "FUNNY"));
  }

  @Test
  void leet_code_point() {
    assertFalse(verbalBacktrackingPuzzle.isSolvable(new String[]{"LEET", "CODE"}, "POINT"));
  }
}