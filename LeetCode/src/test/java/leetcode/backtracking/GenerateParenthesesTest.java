package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertCollectionContainsAll;

class GenerateParenthesesTest {

  private GenerateParentheses generateParentheses = new GenerateParentheses.Solution();

  @Test
  void n3() {
    assertCollectionContainsAll(generateParentheses.generateParenthesis(3),
      "((()))", "(()())", "(())()", "()(())", "()()()");
  }
}
