package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertCollectionContainsAll;

class LetterCombinationsOfAPhoneNumberTest {

  private LetterCombinationsOfAPhoneNumber letterCombinationsOfAPhoneNumber = new LetterCombinationsOfAPhoneNumber.Solution();

  @Test
  void input23output_ad_ae_af_bd_be_bf_cd_ce_cf() {
    assertCollectionContainsAll(letterCombinationsOfAPhoneNumber.letterCombinations("23"),
      "ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf");
  }
}
