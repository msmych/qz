package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionContainsArrays;

class SubsetsIITest {

    private SubsetsII subsetsII = new SubsetsII.Solution();

    @Test void input122() {
        assertListCollectionContainsArrays(new Integer[][]{
                {2},
                {1},
                {1, 2, 2},
                {2, 2},
                {1, 2},
                {}
        }, subsetsII.subsetsWithDup(new int[]{1, 2, 2}));
    }
}