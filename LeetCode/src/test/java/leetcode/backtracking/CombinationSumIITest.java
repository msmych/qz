package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionContainsArrays;

class CombinationSumIITest {

    private CombinationSumII combinationSumII = new CombinationSumII.Solution();

    @Test void candidates10_1_2_7_6_1_5target8solution17_125_26_116() {
        assertListCollectionContainsArrays(new Integer[][]{
                {1, 7},
                {1, 2, 5},
                {2, 6},
                {1, 1, 6}
        }, combinationSumII.combinationSum2(new int[]{10, 1, 2, 7, 6, 1, 5}, 8));
    }

    @Test void candidates25212target5solution122_5() {
        assertListCollectionContainsArrays(new Integer[][]{
                {1, 2, 2},
                {5}
        }, combinationSumII.combinationSum2(new int[]{2, 5, 2, 1, 2}, 5));
    }
}