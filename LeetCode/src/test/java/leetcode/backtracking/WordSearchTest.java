package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WordSearchTest {

  private final char[][] BOARD = new char[][]{
    {'A', 'B', 'C', 'E'},
    {'S', 'F', 'C', 'S'},
    {'A', 'D', 'E', 'E'}};

  private WordSearch wordSearch = new WordSearch.Solution();

  @Test
  void ABCCED() {
    assertTrue(wordSearch.exist(BOARD, "ABCCED"));
  }

  @Test
  void SEE() {
    assertTrue(wordSearch.exist(BOARD, "SEE"));
  }

  @Test
  void ABCB() {
    assertFalse(wordSearch.exist(BOARD, "ABCB"));
  }

  @Test
  void a() {
    assertTrue(wordSearch.exist(new char[][]{{'a'}}, "a"));
  }
}
