package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionContainsArrays;

class PalindromePartitioningTest {

  private PalindromePartitioning palindromePartitioning = new PalindromePartitioning.Solution();

  @Test
  void aab() {
    assertListCollectionContainsArrays(new String[][]{
      {"aa", "b"},
      {"a", "a", "b"}
    }, palindromePartitioning.partition("aab"));
  }

  @Test
  void bb() {
    assertListCollectionContainsArrays(new String[][]{
      {"b", "b"},
      {"bb"}
    }, palindromePartitioning.partition("bb"));
  }
}