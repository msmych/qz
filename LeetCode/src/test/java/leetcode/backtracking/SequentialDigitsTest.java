package leetcode.backtracking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class SequentialDigitsTest {

  private SequentialDigits sequentialDigits = new SequentialDigits.Solution();

  @Test
  void low100high300() {
    assertArrayEquals(new Integer[]{123, 234},
      sequentialDigits.sequentialDigits(100, 300).toArray(new Integer[]{}));
  }

  @Test
  void low1000high13000() {
    assertArrayEquals(new Integer[]{1234, 2345, 3456, 4567, 5678, 6789, 12345},
      sequentialDigits.sequentialDigits(1000, 13000).toArray(new Integer[]{}));
  }
}