package leetcode.queue;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyCircularQueueTest {

    @Test void queue3() {
        MyCircularQueue circularQueue = new MyCircularQueue(3);
        assertTrue(circularQueue.enQueue(1));
        assertTrue(circularQueue.enQueue(2));
        assertTrue(circularQueue.enQueue(3));
        assertFalse(circularQueue.enQueue(4));
        assertEquals(3, circularQueue.Rear());
        assertTrue(circularQueue.isFull());
        assertTrue(circularQueue.deQueue());
        assertTrue(circularQueue.enQueue(4));
        assertEquals(4, circularQueue.Rear());
    }
}