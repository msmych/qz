package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class DiameterOfBinaryTreeTest {

    private DiameterOfBinaryTree diameterOfBinaryTree = new DiameterOfBinaryTree.Solution();

    @Test void tree123453() {
        assertEquals(3, diameterOfBinaryTree.diameterOfBinaryTree(
            fromValAndLeftAndRight(1,
                fromValAndLeftAndRight(2, new TreeNode(4), new TreeNode(5)),
                new TreeNode(3))));
    }

    @Test void tree1() {
        assertEquals(0, diameterOfBinaryTree.diameterOfBinaryTree(new TreeNode(1)));
    }

    @Test void tree12() {
        assertEquals(1, diameterOfBinaryTree.diameterOfBinaryTree(fromValAndLeft(1, new TreeNode(2))));
    }

    @Test void tree_() {
        assertEquals(0, diameterOfBinaryTree.diameterOfBinaryTree(null));
    }
}