package leetcode.tree;

import leetcode.tree.n.Node;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class NAryTreePreorderTraversalTest {

    private NAryTreePreorderTraversal nAryTreePreorderTraversal = new NAryTreePreorderTraversal.Solution();

    @Test void tree135624() {
        assertArrayEquals(new Integer[]{1, 3, 5, 6, 2, 4}, nAryTreePreorderTraversal.preorder(
                new Node(1, Arrays.asList(
                        new Node(3, Arrays.asList(
                                new Node(5, emptyList()),
                                new Node(6, emptyList()))),
                        new Node(2, emptyList()),
                        new Node(4, emptyList())
                ))).toArray(new Integer[]{}));
    }
}