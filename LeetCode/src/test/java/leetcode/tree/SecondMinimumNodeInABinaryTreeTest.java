package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class SecondMinimumNodeInABinaryTreeTest {

    private SecondMinimumNodeInABinaryTree secondMinimumNodeInABinaryTree = new SecondMinimumNodeInABinaryTree.Solution();

    @Test void tree5() {
        assertEquals(5, secondMinimumNodeInABinaryTree.findSecondMinimumValue(
                fromValAndLeftAndRight(2,
                        new TreeNode(2),
                        fromValAndLeftAndRight(5,
                                new TreeNode(5),
                                new TreeNode(7)))));
    }

    @Test void treeN1() {
        assertEquals(-1, secondMinimumNodeInABinaryTree.findSecondMinimumValue(
                fromValAndLeftAndRight(2,
                        new TreeNode(2),
                        new TreeNode(2))));
    }

    @Test void tree2() {
        assertEquals(2, secondMinimumNodeInABinaryTree.findSecondMinimumValue(
                fromValAndLeftAndRight(1,
                        fromValAndLeftAndRight(1,
                                fromValAndLeftAndRight(1,
                                        fromValAndLeftAndRight(3,
                                                new TreeNode(3),
                                                new TreeNode(3)),
                                        fromValAndLeftAndRight(1,
                                                new TreeNode(1),
                                                new TreeNode(6))),
                                fromValAndLeftAndRight(1,
                                        fromValAndLeftAndRight(1,
                                                new TreeNode(2),
                                                new TreeNode(1)),
                                        new TreeNode(1))),
                        fromValAndLeftAndRight(3,
                                fromValAndLeftAndRight(3,
                                        new TreeNode(3),
                                        new TreeNode(8)),
                                fromValAndLeftAndRight(4,
                                        new TreeNode(4),
                                        new TreeNode(8))))));
    }
}