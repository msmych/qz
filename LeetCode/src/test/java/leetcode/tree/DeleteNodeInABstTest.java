package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeleteNodeInABstTest {

    private DeleteNodeInABst deleteNodeInABst = new DeleteNodeInABst.Solution();

    @Test void root53624$7key3() {
        TreeNode right = TreeNode.fromValAndLeftAndRight(6, null, new TreeNode(7));
        TreeNode root = TreeNode.fromValAndLeftAndRight(5,
                TreeNode.fromValAndLeftAndRight(3, new TreeNode(2), new TreeNode(4)), right);
        String expected = TreeNode.serialize(TreeNode.fromValAndLeftAndRight(5,
                TreeNode.fromValAndLeft(4, new TreeNode(2)), right));
        assertEquals(expected, TreeNode.serialize(deleteNodeInABst.deleteNode(root, 3)));
    }

    @Test void root_key3() {
        assertNull(deleteNodeInABst.deleteNode(null, 3));
    }

    @Test void root1key3() {
        TreeNode root = new TreeNode(1);
        assertEquals(root, deleteNodeInABst.deleteNode(root, 3));
    }

    @Test void root3key3() {
        assertNull(deleteNodeInABst.deleteNode(new TreeNode(3), 3));
    }

    @Test void root1$2key2() {
        TreeNode root = TreeNode.fromValAndLeftAndRight(1, null, new TreeNode(2));
        String expected = TreeNode.serialize(new TreeNode(1));
        assertEquals(expected, TreeNode.serialize(deleteNodeInABst.deleteNode(root, 2)));
    }
}