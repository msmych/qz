package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SearchInABinarySearchTreeTest {

    private SearchInABinarySearchTree searchInABinarySearchTree = new SearchInABinarySearchTree.Solution();

    @Test void tree4_2_13__7val2() {
        TreeNode node2 = TreeNode.fromValAndLeftAndRight(2, new TreeNode(1), new TreeNode(3));
        TreeNode root = TreeNode.fromValAndLeftAndRight(4, node2, new TreeNode(7));
        assertEquals(node2, searchInABinarySearchTree.searchBST(root, 2));
        assertNull(searchInABinarySearchTree.searchBST(root, 5));
    }
}