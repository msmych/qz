package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class MinimumAbsoluteDifferenceInBstTest {

    private MinimumAbsoluteDifferenceInBst minimumAbsoluteDifferenceInBst = new MinimumAbsoluteDifferenceInBst.Solution();

    @Test void tree123() {
        assertEquals(1, minimumAbsoluteDifferenceInBst.getMinimumDifference(
            fromValAndLeftAndRight(1, null, fromValAndLeft(3, new TreeNode(2)))));
    }
}