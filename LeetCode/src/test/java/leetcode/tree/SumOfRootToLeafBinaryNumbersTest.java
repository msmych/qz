package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class SumOfRootToLeafBinaryNumbersTest {

    private SumOfRootToLeafBinaryNumbers sumOfRootToLeafBinaryNumbers = new SumOfRootToLeafBinaryNumbers.Solution();

    @Test void tree1010101output22() {
        assertEquals(22, sumOfRootToLeafBinaryNumbers.sumRootToLeaf(
                fromValAndLeftAndRight(1,
                        fromValAndLeftAndRight(0, new TreeNode(0), new TreeNode(1)),
                        fromValAndLeftAndRight(1, new TreeNode(0), new TreeNode(1)))));
    }
}