package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConvertSortedArrayToBinarySearchTreeTest {

    private ConvertSortedArrayToBinarySearchTree convertSortedArrayToBinarySearchTree =
            new ConvertSortedArrayToBinarySearchTree.Solution();

    @Test void arrayN10N3_0_5_9tree0N3N10__9_5() {
        assertEquals(TreeNode.serialize(TreeNode.fromValAndLeftAndRight(0,
                TreeNode.fromValAndLeft(-3, new TreeNode(-10)),
                TreeNode.fromValAndLeft(9, new TreeNode(5)))),
                TreeNode.serialize(convertSortedArrayToBinarySearchTree.sortedArrayToBST(new int[]{-10, -3, 0, 5, 9})));
    }
}