package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidateBinarySearchTreeTest {

    private ValidateBinarySearchTree validateBinarySearchTree = new ValidateBinarySearchTree.Solution();

    @Test void tree2_13true() {
        assertTrue(validateBinarySearchTree.isValidBST(
                TreeNode.fromValAndLeftAndRight(2, new TreeNode(1), new TreeNode(3))));
    }

    @Test void tree5_1__4_3_6false() {
        assertFalse(validateBinarySearchTree.isValidBST(
                TreeNode.fromValAndLeftAndRight(5, new TreeNode(1),
                        TreeNode.fromValAndLeftAndRight(4, new TreeNode(3), new TreeNode(6)))));
    }

    @Test void tree1$1false() {
        assertFalse(validateBinarySearchTree.isValidBST(
                TreeNode.fromValAndLeftAndRight(1, null, new TreeNode(1))));
    }

    @Test void tree10_5_$_$_15_6_20() {
        assertFalse(validateBinarySearchTree.isValidBST(
                TreeNode.fromValAndLeftAndRight(10, new TreeNode(5),
                        TreeNode.fromValAndLeftAndRight(15, new TreeNode(6), new TreeNode(20)))));
    }

    @Test void treeN2147483648$2147483647true() {
        assertTrue(validateBinarySearchTree.isValidBST(
                TreeNode.fromValAndLeftAndRight(-2147483648, null, new TreeNode(2147483647))));
    }
}