package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ContainsDuplicateIIITest {

    private ContainsDuplicateIII containsDuplicateIII = new ContainsDuplicateIII.Solution();

    @Test void nums1231k3t0true() {
        assertTrue(containsDuplicateIII.containsNearbyAlmostDuplicate(new int[]{1, 2, 3, 1}, 3, 0));
    }

    @Test void nums1011k1t2true() {
        assertTrue(containsDuplicateIII.containsNearbyAlmostDuplicate(new int[]{1, 0, 1, 1}, 1, 2));
    }

    @Test void nums159159k2t3() {
        assertFalse(containsDuplicateIII.containsNearbyAlmostDuplicate(new int[]{1, 5, 9, 1, 5, 9}, 2, 3));
    }

    @Test void numsN1_2147483647k1t2147483647() {
        assertFalse(containsDuplicateIII.containsNearbyAlmostDuplicate(new int[]{-1, 2147483647}, 1, 2147483647));
    }

    @Test void nums10_100_11_9k1t2() {
        assertTrue(containsDuplicateIII.containsNearbyAlmostDuplicate(new int[]{10, 100, 11, 9}, 1, 2));
    }
}