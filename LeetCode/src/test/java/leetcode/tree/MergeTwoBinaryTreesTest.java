package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.*;
import static org.junit.jupiter.api.Assertions.*;

class MergeTwoBinaryTreesTest {

    private MergeTwoBinaryTrees mergeTwoBinaryTrees = new MergeTwoBinaryTrees.Solution();

    @Test void tree1to7() {
        assertEquals(serialize(fromValAndLeftAndRight(3,
                fromValAndLeftAndRight(4, new TreeNode(5), new TreeNode(4)),
                fromValAndLeftAndRight(5, null, new TreeNode(7)))),
                serialize(mergeTwoBinaryTrees.mergeTrees(
                        fromValAndLeftAndRight(1,
                                fromValAndLeft(3, new TreeNode(5)),
                                new TreeNode(2)),
                        fromValAndLeftAndRight(2,
                                fromValAndLeftAndRight(1, null, new TreeNode(4)),
                                fromValAndLeftAndRight(3, null, new TreeNode(7))))));
    }
}