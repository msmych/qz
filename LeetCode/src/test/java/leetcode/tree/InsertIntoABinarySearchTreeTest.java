package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InsertIntoABinarySearchTreeTest {

    private InsertIntoABinarySearchTree insertIntoABinarySearchTree = new InsertIntoABinarySearchTree.Solution();

    @Test void tree4213__7value5() {
        TreeNode left = TreeNode.fromValAndLeftAndRight(2, new TreeNode(1), new TreeNode(3));
        TreeNode root = TreeNode.fromValAndLeftAndRight(4, left, new TreeNode(7));
        String expected = TreeNode.serialize(TreeNode.fromValAndLeftAndRight(4, left,
                TreeNode.fromValAndLeft(7, new TreeNode(5))));
        assertEquals(expected, TreeNode.serialize(insertIntoABinarySearchTree.insertIntoBST(root, 5)));
    }
}