package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class ConstructStringFromBinaryTreeTest {

    private ConstructStringFromBinaryTree constructStringFromBinaryTree = new ConstructStringFromBinaryTree.Solution();

    @Test void tree1234() {
        assertEquals("1(2(4))(3)", constructStringFromBinaryTree.tree2str(fromValAndLeftAndRight(1,
                fromValAndLeft(2, new TreeNode(4)),
                new TreeNode(3))));
    }

    @Test void tree123_4() {
        assertEquals("1(2()(4))(3)", constructStringFromBinaryTree.tree2str(fromValAndLeftAndRight(1,
                fromValAndLeftAndRight(2, null, new TreeNode(4)),
                new TreeNode(3))));
    }

    @Test void tree_() {
        assertEquals("", constructStringFromBinaryTree.tree2str(null));
    }
}