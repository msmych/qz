package leetcode.tree;

import leetcode.tree.n.Node;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;

class MaximumDepthOfNAryTreeTest {

    private MaximumDepthOfNAryTree topDown = new MaximumDepthOfNAryTree.TopDownSolution();
    private MaximumDepthOfNAryTree bottomUp = new MaximumDepthOfNAryTree.BottomUpSolution();

    @Test void tree135624depth3() {
        Node root = new Node(1, Arrays.asList(
                new Node(3, Arrays.asList(
                        new Node(5, emptyList()),
                        new Node(6, emptyList()))),
                new Node(2, emptyList()),
                new Node(4, emptyList())));
        assertEquals(3, topDown.maxDepth(root));
        assertEquals(3, bottomUp.maxDepth(root));
    }

    @Test void tree1depth1() {
        Node root = new Node(1, emptyList());
        assertEquals(1, topDown.maxDepth(root));
        assertEquals(1, bottomUp.maxDepth(root));
    }
}