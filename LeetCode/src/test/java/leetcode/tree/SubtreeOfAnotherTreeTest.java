package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class SubtreeOfAnotherTreeTest {

    private SubtreeOfAnotherTree subtreeOfAnotherTree = new SubtreeOfAnotherTree.Solution();

    @Test void tree34125() {
        assertTrue(subtreeOfAnotherTree.isSubtree(
                fromValAndLeftAndRight(3,
                        fromValAndLeftAndRight(4, new TreeNode(1), new TreeNode(2)),
                        new TreeNode(5)),
                fromValAndLeftAndRight(4, new TreeNode(1), new TreeNode(2))));
    }

    @Test void tree341205() {
        assertFalse(subtreeOfAnotherTree.isSubtree(
                fromValAndLeftAndRight(3,
                        fromValAndLeftAndRight(4,
                                new TreeNode(1),
                                fromValAndLeft(2, new TreeNode(0))),
                        new TreeNode(5)),
                fromValAndLeftAndRight(4, new TreeNode(1), new TreeNode(2))));
    }
}