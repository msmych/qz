package leetcode.tree;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionEquivalentToArrays;

class BinaryTreeLevelOrderTraversalIITest {

    private BinaryTreeLevelOrderTraversalII binaryTreeLevelOrderTraversalII =
            new BinaryTreeLevelOrderTraversalII.Solution();

    @Test void tree3() {
        assertListCollectionEquivalentToArrays(new Integer[][]{
                {15, 7},
                {9, 20},
                {3}
        }, binaryTreeLevelOrderTraversalII.levelOrderBottom(TreeNode.fromValAndLeftAndRight(3,
                new TreeNode(9),
                TreeNode.fromValAndLeftAndRight(20, new TreeNode(15), new TreeNode(7)))));
    }
}