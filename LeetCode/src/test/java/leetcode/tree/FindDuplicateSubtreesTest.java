package leetcode.tree;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertCollectionContainsAll;

class FindDuplicateSubtreesTest {

    private FindDuplicateSubtrees findDuplicateSubtrees = new FindDuplicateSubtrees.Solution();

    @Test void input1234$24$$4output24$_4() {
        TreeNode node4 = new TreeNode(4);
        TreeNode node24$ = TreeNode.fromValAndLeft(2, node4);
        TreeNode root = TreeNode.fromValAndLeftAndRight(1, node24$,
                TreeNode.fromValAndLeftAndRight(3, node24$, node4));
        assertCollectionContainsAll(findDuplicateSubtrees.findDuplicateSubtrees(root), node4, node24$);
    }
}