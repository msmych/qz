package leetcode.tree;

import leetcode.tree.n.Node;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;

class NAryTreePostorderTraversalTest {

    private NAryTreePostorderTraversal nAryTreePostorderTraversal = new NAryTreePostorderTraversal.Solution();

    @Test void tree135624postorder563241() {
        assertArrayEquals(new Integer[]{5, 6, 3, 2, 4, 1}, nAryTreePostorderTraversal.postorder(
                new Node(1, Arrays.asList(
                        new Node(3, Arrays.asList(
                                new Node(5, emptyList()),
                                new Node(6, emptyList()))),
                        new Node(2, emptyList()),
                        new Node(4, emptyList())
                ))).toArray(new Integer[]{}));
    }
}