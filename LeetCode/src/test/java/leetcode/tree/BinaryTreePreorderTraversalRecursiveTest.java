package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class BinaryTreePreorderTraversalRecursiveTest {

    private BinaryTreePreorderTraversal recursive = new BinaryTreePreorderTraversal.RecursiveSolution();
    private BinaryTreePreorderTraversal iterative = new BinaryTreePreorderTraversal.IterativeSolution();

    @Test void input1_23output123() {
        TreeNode root = TreeNode.fromValAndLeftAndRight(1, null,
                TreeNode.fromValAndLeft(2, new TreeNode(3)));
        Integer[] traversal = new Integer[]{1, 2, 3};
        assertArrayEquals(traversal, recursive.preorderTraversal(root).toArray(new Integer[]{}));
        assertArrayEquals(traversal, iterative.preorderTraversal(root).toArray(new Integer[]{}));
    }
}