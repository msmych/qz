package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class LeafSimilarTreesTest {

    private LeafSimilarTrees leafSimilarTrees = new LeafSimilarTrees.Solution();

    @Test void similar() {
        TreeNode root1 = fromValAndLeftAndRight(3,
                fromValAndLeftAndRight(5, new TreeNode(6),
                        fromValAndLeftAndRight(2, new TreeNode(7), new TreeNode(4))),
                fromValAndLeftAndRight(1, new TreeNode(9), new TreeNode(8))),
                root2 = fromValAndLeftAndRight(1,
                        new TreeNode(6), fromValAndLeftAndRight(2,
                                new TreeNode(7), fromValAndLeftAndRight(3,
                                        new TreeNode(4), fromValAndLeftAndRight(4,
                                                new TreeNode(9), new TreeNode(8)))));
        assertTrue(leafSimilarTrees.leafSimilar(root1, root2));
    }
}