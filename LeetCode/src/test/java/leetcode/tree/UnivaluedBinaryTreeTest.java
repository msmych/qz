package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UnivaluedBinaryTreeTest {

    private UnivaluedBinaryTree univaluedBinaryTree = new UnivaluedBinaryTree.Solution();

    @Test void tree11111$1true() {
        assertTrue(univaluedBinaryTree.isUnivalTree(TreeNode.fromValAndLeftAndRight(1,
                TreeNode.fromValAndLeftAndRight(1, new TreeNode(1), new TreeNode(1)),
                TreeNode.fromValAndLeftAndRight(1, null, new TreeNode(1)))));
    }

    @Test void tree22252false() {
        assertFalse(univaluedBinaryTree.isUnivalTree(TreeNode.fromValAndLeftAndRight(2,
                TreeNode.fromValAndLeftAndRight(2, new TreeNode(5), new TreeNode(2)),
                new TreeNode(2))));
    }
}