package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class BinaryTreeTiltTest {

    private BinaryTreeTilt binaryTreeTilt = new BinaryTreeTilt.Solution();

    @Test void tree123() {
        assertEquals(1,
                binaryTreeTilt.findTilt(fromValAndLeftAndRight(1, new TreeNode(2), new TreeNode(3))));
    }

    @Test
    void tree12() {
        assertEquals(2, binaryTreeTilt.findTilt(fromValAndLeft(1, new TreeNode(2))));
    }

    @Test void tree1234_5() {
        assertEquals(11, binaryTreeTilt.findTilt(fromValAndLeftAndRight(1,
                fromValAndLeft(2, new TreeNode(4)),
                fromValAndLeft(3, new TreeNode(5)))));
    }
}