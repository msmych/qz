package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class FindModeInBinarySearchTreeTest {

    private FindModeInBinarySearchTree findModeInBinarySearchTree = new FindModeInBinarySearchTree.Solution();

    @Test void bst1_22() {
        assertArrayEquals(new int[]{2},
                findModeInBinarySearchTree.findMode(fromValAndLeftAndRight(1,
                        null,
                        fromValAndLeft(2, new TreeNode(2)))));
    }

    @Test void bst628047__26() {
        assertArrayEquals(new int[]{2, 6},
                findModeInBinarySearchTree.findMode(fromValAndLeftAndRight(6,
                        fromValAndLeftAndRight(2,
                                fromValAndLeftAndRight(0, null, new TreeNode(2)),
                                new TreeNode(4)),
                        fromValAndLeft(8,
                                fromValAndLeft(7, new TreeNode(6))))));
    }
}