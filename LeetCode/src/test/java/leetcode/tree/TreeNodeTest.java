package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TreeNodeTest {

    @Test void serializeDeserialize() {
        assertEquals("1 2 . . 3 4 . . 5 . .", TreeNode.serialize(TreeNode.deserialize("1 2 . . 3 4 . . 5 . .")));
    }
}