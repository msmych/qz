package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class PathSumIIITest {

  private PathSumIII pathSumIII = new PathSumIII.Solution();

  @Test
  void tree105() {
    assertEquals(3, pathSumIII.pathSum(fromValAndLeftAndRight(10,
      fromValAndLeftAndRight(5,
        fromValAndLeftAndRight(3,
          new TreeNode(3),
          new TreeNode(-2)),
        fromValAndLeftAndRight(2, null, new TreeNode(1))),
      fromValAndLeftAndRight(-3, null, new TreeNode(11))
    ), 8));
  }
}
