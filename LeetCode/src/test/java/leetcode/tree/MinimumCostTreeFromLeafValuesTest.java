package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinimumCostTreeFromLeafValuesTest {

    private MinimumCostTreeFromLeafValues minimumCostTreeFromLeafValues = new MinimumCostTreeFromLeafValues.Solution();

    @Test void arr624() {
        assertEquals(32, minimumCostTreeFromLeafValues.mctFromLeafValues(new int[]{6,2,4}));
    }
}