package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class AverageOfLevelsInBinaryTreeTest {

    private AverageOfLevelsInBinaryTree averageOfLevelsInBinaryTree = new AverageOfLevelsInBinaryTree.Solution();

    @Test void tree3_9_20_15_7() {
        assertArrayEquals(new Double[]{3.0, 14.5, 11.0},
            averageOfLevelsInBinaryTree.averageOfLevels(fromValAndLeftAndRight(3,
                new TreeNode(9),
                fromValAndLeftAndRight(20,
                    new TreeNode(15),
                    new TreeNode(7)))).toArray(new Double[]{}));
    }
}