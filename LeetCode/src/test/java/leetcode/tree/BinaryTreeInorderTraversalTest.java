package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class BinaryTreeInorderTraversalTest {

  private BinaryTreeInorderTraversal recursive = new BinaryTreeInorderTraversal.RecursiveSolution();
  private BinaryTreeInorderTraversal iterative = new BinaryTreeInorderTraversal.IterativeSolution();
  private BinaryTreeInorderTraversal morris = new BinaryTreeInorderTraversal.MorrisSolution();

  @Test
  void tree1_23output132() {
    TreeNode root = TreeNode.fromValAndLeftAndRight(1, null,
      TreeNode.fromValAndLeft(2, new TreeNode(3)));
    Integer[] traversal = new Integer[]{1, 3, 2};
    assertArrayEquals(traversal, recursive.inorderTraversal(root).toArray(new Integer[]{}));
    assertArrayEquals(traversal, iterative.inorderTraversal(root).toArray(new Integer[]{}));
    assertArrayEquals(traversal, morris.inorderTraversal(root).toArray(new Integer[]{}));
  }
}
