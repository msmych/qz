package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class SumOfLeftLeavesTest {

    private SumOfLeftLeaves sumOfLeftLeaves = new SumOfLeftLeaves.Solution();

    @Test void tree3() {
        assertEquals(24, sumOfLeftLeaves.sumOfLeftLeaves(fromValAndLeftAndRight(3,
            new TreeNode(9),
            fromValAndLeftAndRight(20, new TreeNode(15), new TreeNode(7)))));
    }
}