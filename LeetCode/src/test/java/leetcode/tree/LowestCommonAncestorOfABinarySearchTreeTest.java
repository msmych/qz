package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LowestCommonAncestorOfABinarySearchTreeTest {

    private LowestCommonAncestorOfABinarySearchTree lowestCommonAncestorOfABinarySearchTree =
            new LowestCommonAncestorOfABinarySearchTree.Solution();

    @Test void root6_2_04_35__8_79p2q8output6() {
        TreeNode q2 = TreeNode.fromValAndLeftAndRight(4, new TreeNode(3), new TreeNode(5));
        TreeNode p = TreeNode.fromValAndLeftAndRight(2, new TreeNode(0), q2);
        TreeNode q = TreeNode.fromValAndLeftAndRight(8, new TreeNode(7), new TreeNode(9));
        TreeNode root = TreeNode.fromValAndLeftAndRight(6, p, q);
        assertEquals(root, lowestCommonAncestorOfABinarySearchTree.lowestCommonAncestor(root, p, q));
        assertEquals(p, lowestCommonAncestorOfABinarySearchTree.lowestCommonAncestor(root, p, q2));
    }
}