package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BalancedBinaryTreeTest {

    private BalancedBinaryTree balancedBinaryTree = new BalancedBinaryTree.Solution();

    @Test void tree3_9__20_15_7true() {
        assertTrue(balancedBinaryTree.isBalanced(TreeNode.fromValAndLeftAndRight(3, new TreeNode(9),
                TreeNode.fromValAndLeftAndRight(20, new TreeNode(15), new TreeNode(7)))));
    }

    @Test void tree1_2_3_4_4__3__2false() {
        assertFalse(balancedBinaryTree.isBalanced(TreeNode.fromValAndLeftAndRight(1,
                TreeNode.fromValAndLeftAndRight(2,
                        TreeNode.fromValAndLeftAndRight(3, new TreeNode(4), new TreeNode(4)),
                        new TreeNode(3)),
                new TreeNode(2))));
    }
}