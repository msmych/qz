package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IncreasingOrderSearchTreeTest {

    private IncreasingOrderSearchTree increasingOrderSearchTree = new IncreasingOrderSearchTree.Solution();

    @Test void tree1to9() {
        assertEquals(TreeNode.serialize(TreeNode.fromValAndLeftAndRight(1, null,
                TreeNode.fromValAndLeftAndRight(2, null,
                        TreeNode.fromValAndLeftAndRight(3, null,
                                TreeNode.fromValAndLeftAndRight(4, null,
                                        TreeNode.fromValAndLeftAndRight(5, null,
                                                TreeNode.fromValAndLeftAndRight(6, null,
                                                        TreeNode.fromValAndLeftAndRight(7, null,
                                                                TreeNode.fromValAndLeftAndRight(8, null,
                                                                        new TreeNode(9)))))))))),
                TreeNode.serialize(increasingOrderSearchTree.increasingBST(TreeNode.fromValAndLeftAndRight(5,
                        TreeNode.fromValAndLeftAndRight(3,
                                TreeNode.fromValAndLeft(2, new TreeNode(1)), new TreeNode(4)),
                        TreeNode.fromValAndLeftAndRight(6, null,
                                TreeNode.fromValAndLeftAndRight(8, new TreeNode(7), new TreeNode(9)))))));
    }
}