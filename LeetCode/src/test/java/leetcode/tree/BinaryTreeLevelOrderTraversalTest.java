package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static utils.TestUtils.assertListCollectionEquivalentToArrays;

class BinaryTreeLevelOrderTraversalTest {

    private BinaryTreeLevelOrderTraversal binaryTreeLevelOrderTraversal = new BinaryTreeLevelOrderTraversal.Solution();
    private BinaryTreeLevelOrderTraversal iterative = new BinaryTreeLevelOrderTraversal.IterativeSolution();

    @Test void tree3_9_20_$_$_15_7__traversal3__9_20__15_7() {
        TreeNode root = fromValAndLeftAndRight(3, new TreeNode(9),
                fromValAndLeftAndRight(20, new TreeNode(15), new TreeNode(7)));
        Integer[][] vals = {
                {3},
                {9, 20},
                {15, 7}
        };
        assertListCollectionEquivalentToArrays(vals, binaryTreeLevelOrderTraversal.levelOrder(root));
        assertListCollectionEquivalentToArrays(vals, iterative.levelOrder(root));
    }
}