package leetcode.tree;

import leetcode.tree.n.Node;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static java.util.Collections.emptyList;
import static utils.TestUtils.assertListCollectionEquivalentToArrays;

class NAryTreeLevelOrderTraversalTest {

    private NAryTreeLevelOrderTraversal nAryTreeLevelOrderTraversal = new NAryTreeLevelOrderTraversal.Solution();

    @Test void tree() {
        assertListCollectionEquivalentToArrays(new Integer[][]{
                {1},
                {3, 2, 4},
                {5, 6}
        }, nAryTreeLevelOrderTraversal.levelOrder(
                new Node(1, Arrays.asList(
                        new Node(3, Arrays.asList(
                                new Node(5, emptyList()),
                                new Node(6, emptyList()))),
                        new Node(2, emptyList()),
                        new Node(4, emptyList())
                ))));
    }
}