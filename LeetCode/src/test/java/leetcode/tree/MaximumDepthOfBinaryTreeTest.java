package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MaximumDepthOfBinaryTreeTest {

    private MaximumDepthOfBinaryTree topDown = new MaximumDepthOfBinaryTree.TopDownSolution();
    private MaximumDepthOfBinaryTree bottomUp = new MaximumDepthOfBinaryTree.BottomUpSolution();

    @Test void tree3_9_20_$_$_15_7depth3() {
        TreeNode root = TreeNode.fromValAndLeftAndRight(3, new TreeNode(9),
                TreeNode.fromValAndLeftAndRight(20, new TreeNode(15), new TreeNode(7)));
        assertEquals(3, topDown.maxDepth(root));
        assertEquals(3, bottomUp.maxDepth(root));
    }
}