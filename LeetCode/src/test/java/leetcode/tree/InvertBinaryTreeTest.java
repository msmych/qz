package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static leetcode.tree.TreeNode.serialize;
import static org.junit.jupiter.api.Assertions.*;

class InvertBinaryTreeTest {

    private InvertBinaryTree invertBinaryTree = new InvertBinaryTree.Solution();

    @Test void tree4() {
        assertEquals(
            serialize(fromValAndLeftAndRight(4,
                fromValAndLeftAndRight(7, new TreeNode(9), new TreeNode(6)),
                fromValAndLeftAndRight(2, new TreeNode(3), new TreeNode(1)))),
            serialize(invertBinaryTree.invertTree(fromValAndLeftAndRight(4,
                fromValAndLeftAndRight(2, new TreeNode(1), new TreeNode(3)),
                fromValAndLeftAndRight(7, new TreeNode(6), new TreeNode(9))))));
    }
}