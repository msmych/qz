package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.*;
import static org.junit.jupiter.api.Assertions.*;

class TrimABinarySearchTreeTest {

    private TrimABinarySearchTree trimABinarySearchTree = new TrimABinarySearchTree.Solution();

    @Test void l1r2() {
        assertEquals(serialize(fromValAndLeftAndRight(1, null, new TreeNode(2))),
                serialize(trimABinarySearchTree.trimBST(
                        fromValAndLeftAndRight(1, new TreeNode(0), new TreeNode(2)), 1, 2)));
    }

    @Test void l1r3() {
        assertEquals(serialize(fromValAndLeft(3, fromValAndLeft(2, new TreeNode(1)))),
                serialize(trimABinarySearchTree.trimBST(fromValAndLeftAndRight(3,
                        fromValAndLeftAndRight(0, null, fromValAndLeft(2, new TreeNode(1))),
                        new TreeNode(4)),
                        1, 3)));
    }
}