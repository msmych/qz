package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static utils.TestUtils.assertListCollectionEquivalentToArrays;

class PathSumIITest {

    private PathSumII pathSumII = new PathSumII.Solution();

    @Test void sum22() {
        assertListCollectionEquivalentToArrays(new Integer[][]{
                {5, 4, 11, 2},
                {5, 8, 4, 5}
        }, pathSumII.pathSum(fromValAndLeftAndRight(5,
                fromValAndLeft(4, fromValAndLeftAndRight(11, new TreeNode(7), new TreeNode(2))),
                fromValAndLeftAndRight(8, new TreeNode(13),
                        fromValAndLeftAndRight(4, new TreeNode(5), new TreeNode(1)))),
                22));
    }
}