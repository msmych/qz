package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SymmetricTreeTest {

    private SymmetricTree recursive = new SymmetricTree.RecursiveSolution();
    private SymmetricTree iterative = new SymmetricTree.IterativeSolution();

    @Test void tree1223443symmetric() {
        TreeNode root = TreeNode.fromValAndLeftAndRight(1,
                TreeNode.fromValAndLeftAndRight(2, new TreeNode(3), new TreeNode(4)),
                TreeNode.fromValAndLeftAndRight(2, new TreeNode(4), new TreeNode(3)));
        assertTrue(recursive.isSymmetric(root));
        assertTrue(iterative.isSymmetric(root));
    }

    @Test void tree122_3_3asymmetric() {
        TreeNode root = TreeNode.fromValAndLeftAndRight(1,
                TreeNode.fromValAndLeftAndRight(2, null, new TreeNode(3)),
                TreeNode.fromValAndLeftAndRight(2, null, new TreeNode(3)));
        assertFalse(recursive.isSymmetric(root));
        assertFalse(iterative.isSymmetric(root));
    }
}