package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PathSumTest {

    private PathSum pathSum = new PathSum.Solution();

    @Test void tree5_4_11_7_2_$_8_13_4_$_1sum22true() {
        assertTrue(pathSum.hasPathSum(TreeNode.fromValAndLeftAndRight(5,
                TreeNode.fromValAndLeft(4,
                        TreeNode.fromValAndLeftAndRight(11, new TreeNode(7), new TreeNode(2))),
                TreeNode.fromValAndLeftAndRight(8, new TreeNode(13),
                        TreeNode.fromValAndLeftAndRight(4, null, new TreeNode(1)))),
                22));
    }

    @Test void tree12$3$4$5sum6false() {
        assertFalse(pathSum.hasPathSum(
                TreeNode.fromValAndLeft(1,
                        TreeNode.fromValAndLeft(2,
                                TreeNode.fromValAndLeft(3,
                                        TreeNode.fromValAndLeft(4, new TreeNode(5))))), 6));
    }
}