package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.assertEquals;

class LowestCommonAncestorOfABinaryTreeTest {

  private LowestCommonAncestorOfABinaryTree lowestCommonAncestorOfABinaryTree =
    new LowestCommonAncestorOfABinaryTree.Solution();

  @Test
  void root3516208$$74p5q1() {
    assertEquals(3, lowestCommonAncestorOfABinaryTree
      .lowestCommonAncestor(fromValAndLeftAndRight(3,
        fromValAndLeftAndRight(5, new TreeNode(6),
          fromValAndLeftAndRight(2, new TreeNode(7), new TreeNode(4))),
        fromValAndLeftAndRight(1, new TreeNode(0), new TreeNode(8))),
        new TreeNode(5), new TreeNode(1)).val);
  }

  @Test
  void root3516208$$74p5q4() {
    assertEquals(5, lowestCommonAncestorOfABinaryTree
      .lowestCommonAncestor(fromValAndLeftAndRight(3,
        fromValAndLeftAndRight(5, new TreeNode(6),
          fromValAndLeftAndRight(2, new TreeNode(7), new TreeNode(4))),
        fromValAndLeftAndRight(1, new TreeNode(0), new TreeNode(8))),
        new TreeNode(5), new TreeNode(4)).val);
  }
}