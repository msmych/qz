package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.*;
import static org.junit.jupiter.api.Assertions.*;

class ConvertBstToGreaterTreeTest {

    private ConvertBstToGreaterTree convertBstToGreaterTree = new ConvertBstToGreaterTree.Solution();

    @Test void tree5213() {
        assertEquals(serialize(fromValAndLeftAndRight(18, new TreeNode(20), new TreeNode(13))),
                serialize(convertBstToGreaterTree.convertBST(fromValAndLeftAndRight(5, new TreeNode(2), new TreeNode(13)))));
    }

    @Test void tree203N41() {
        assertEquals(serialize(fromValAndLeftAndRight(5,
                fromValAndLeftAndRight(6, new TreeNode(2), new TreeNode(6)),
                new TreeNode(3))),
                serialize(convertBstToGreaterTree.convertBST(
                        fromValAndLeftAndRight(2,
                                fromValAndLeftAndRight(0, new TreeNode(-4), new TreeNode(1)),
                                new TreeNode(3)))));
    }

    @Test void tree104N2_3() {
        assertEquals(serialize(fromValAndLeftAndRight(8,
                fromValAndLeft(8, new TreeNode(6)),
                fromValAndLeft(4, new TreeNode(7)))),
                serialize(convertBstToGreaterTree.convertBST(fromValAndLeftAndRight(1,
                        fromValAndLeft(0, new TreeNode(-2)),
                        fromValAndLeft(4, new TreeNode(3))))));
    }
}