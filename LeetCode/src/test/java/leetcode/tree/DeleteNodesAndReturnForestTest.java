package leetcode.tree;

import org.junit.jupiter.api.Test;

import java.util.List;

import static leetcode.tree.TreeNode.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DeleteNodesAndReturnForestTest {

    private DeleteNodesAndReturnForest deleteNodesAndReturnForest = new DeleteNodesAndReturnForest.Solution();

    @Test void tree1234567() {
        List<TreeNode> forest = deleteNodesAndReturnForest.delNodes(fromValAndLeftAndRight(1,
                fromValAndLeftAndRight(2, new TreeNode(4), new TreeNode(5)),
                fromValAndLeftAndRight(3, new TreeNode(6), new TreeNode(7))),
                new int[]{3, 5});
        assertForest(forest,
                fromValAndLeft(1, fromValAndLeft(2, new TreeNode(4))), new TreeNode(6), new TreeNode(7));
    }

    private void assertForest(List<TreeNode> forest, TreeNode... expected) {
        assertEquals(expected.length, forest.size());
        for (TreeNode node : expected) {
            String snode = serialize(node);
            assertTrue(forest.stream().map(TreeNode::serialize).anyMatch(snode::equals));
        }
    }
}