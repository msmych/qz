package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PathInZigzagLabelledBinaryTreeTest {

    private PathInZigzagLabelledBinaryTree pathInZigzagLabelledBinaryTree =
            new PathInZigzagLabelledBinaryTree.Solution();

    @Test void label14() {
        assertArrayEquals(new Integer[]{1, 3, 4, 14},
                pathInZigzagLabelledBinaryTree.pathInZigZagTree(14).toArray(new Integer[]{}));
    }

    @Test void label26() {
        assertArrayEquals(new Integer[]{1, 2, 6, 10, 26},
                pathInZigzagLabelledBinaryTree.pathInZigZagTree(26).toArray(new Integer[]{}));
    }
}