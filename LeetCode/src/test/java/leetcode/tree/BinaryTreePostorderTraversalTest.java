package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class BinaryTreePostorderTraversalTest {

    private BinaryTreePostorderTraversal recursive = new BinaryTreePostorderTraversal.RecursiveSolution();
    private BinaryTreePostorderTraversal iterative = new BinaryTreePostorderTraversal.IterativeSolution();

    @Test void input1_23output321() {
        TreeNode root = TreeNode.fromValAndLeftAndRight(1, null,
                TreeNode.fromValAndLeft(2, new TreeNode(3)));
        Integer[] traversal = new Integer[]{3, 2, 1};
        assertArrayEquals(traversal, recursive.postorderTraversal(root).toArray(new Integer[]{}));
        assertArrayEquals(traversal, iterative.postorderTraversal(root).toArray(new Integer[]{}));
    }
}