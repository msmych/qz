package leetcode.tree;

import leetcode.tree.quad.Node;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QuadTreeInsertionTest {

    private QuadTreeInsertion quadTreeInsertion = new QuadTreeInsertion.Solution();

    @Test void ttff_tfftttf() {
        Node f = new Node(false, true, null, null, null, null);
        Node t = new Node(true, true, null, null, null, null);
        Node or = quadTreeInsertion.intersect(
            new Node(false, false, t, t, f, f),
            new Node(false, false, t, new Node(false, false, f, f, t, t), t, f));
        assertTrue(or.topLeft.val);
        assertTrue(or.topRight.val);
        assertTrue(or.bottomLeft.val);
        assertFalse(or.bottomRight.val);
    }
}