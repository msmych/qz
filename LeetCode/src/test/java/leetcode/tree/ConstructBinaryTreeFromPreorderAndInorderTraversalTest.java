package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ConstructBinaryTreeFromPreorderAndInorderTraversalTest {

  private ConstructBinaryTreeFromPreorderAndInorderTraversal constructBinaryTreeFromPreorderAndInorderTraversal =
    new ConstructBinaryTreeFromPreorderAndInorderTraversal.Solution();

  @Test
  void preorder3_9_20_15_7inorder9_3_15_20_7() {
    assertEquals("3 9 . . 20 15 . . 7 . .",
      TreeNode.serialize(constructBinaryTreeFromPreorderAndInorderTraversal
        .buildTree(new int[]{3, 9, 20, 15, 7}, new int[]{9, 3, 15, 20, 7})));
  }
}
