package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BSTIteratorTest {

    @Test void tree123() {
        BSTIterator bstIterator = new BSTIterator(
                TreeNode.fromValAndLeftAndRight(2, new TreeNode(1), new TreeNode(3)));
        assertTrue(bstIterator.hasNext());
        assertEquals(1, bstIterator.next());
        assertTrue(bstIterator.hasNext());
        assertEquals(2, bstIterator.next());
        assertTrue(bstIterator.hasNext());
        assertEquals(3, bstIterator.next());
        assertFalse(bstIterator.hasNext());
    }
}