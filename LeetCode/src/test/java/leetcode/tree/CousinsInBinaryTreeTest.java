package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CousinsInBinaryTreeTest {

    private CousinsInBinaryTree cousinsInBinaryTree = new CousinsInBinaryTree.Solution();

    @Test void root1234x4y3false() {
        assertFalse(cousinsInBinaryTree.isCousins(TreeNode.fromValAndLeftAndRight(1,
                TreeNode.fromValAndLeft(2, new TreeNode(4)), new TreeNode(3)), 4, 3));
    }

    @Test void root123_4_5x5y4true() {
        assertTrue(cousinsInBinaryTree.isCousins(TreeNode.fromValAndLeftAndRight(1,
                TreeNode.fromValAndLeftAndRight(2, null, new TreeNode(4)),
                TreeNode.fromValAndLeftAndRight(3, null, new TreeNode(5))),
                5, 4));
    }

    @Test void root123_4x2y3false() {
        assertFalse(cousinsInBinaryTree.isCousins(TreeNode.fromValAndLeftAndRight(1,
                TreeNode.fromValAndLeftAndRight(2, null, new TreeNode(4)), new TreeNode(3)), 2, 3));
    }
}