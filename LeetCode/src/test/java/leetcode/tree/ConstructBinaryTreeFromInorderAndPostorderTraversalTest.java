package leetcode.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ConstructBinaryTreeFromInorderAndPostorderTraversalTest {

    private ConstructBinaryTreeFromInorderAndPostorderTraversal constructBinaryTreeFromInorderAndPostorderTraversal =
            new ConstructBinaryTreeFromInorderAndPostorderTraversal.Solution();

    @Test void inorder9_3_15_20_7postorder9_15_7_20_3() {
        assertEquals("3 9 . . 20 15 . . 7 . .",
                TreeNode.serialize(constructBinaryTreeFromInorderAndPostorderTraversal
                        .buildTree(new int[]{9, 3, 15, 20, 7}, new int[]{9, 15, 7, 20, 3})));
    }

    @Test void inorder1234postorder2143() {
        assertEquals("3 1 . 2 . . 4 . .",
                TreeNode.serialize((constructBinaryTreeFromInorderAndPostorderTraversal
                        .buildTree(new int[]{1, 2, 3, 4}, new int[]{2, 1, 4, 3}))));
    }
}