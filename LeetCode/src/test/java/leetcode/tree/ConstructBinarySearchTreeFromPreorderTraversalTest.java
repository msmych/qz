package leetcode.tree;

import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static leetcode.tree.TreeNode.serialize;
import static org.junit.jupiter.api.Assertions.*;

class ConstructBinarySearchTreeFromPreorderTraversalTest {

    private ConstructBinarySearchTreeFromPreorderTraversal constructBinarySearchTreeFromPreorderTraversal =
            new ConstructBinarySearchTreeFromPreorderTraversal.Solution();

    @Test void tree8_5_1_7_10_12() {
        assertEquals(serialize(fromValAndLeftAndRight(8,
                fromValAndLeftAndRight(5, new TreeNode(1), new TreeNode(7)),
                fromValAndLeftAndRight(10, null, new TreeNode(12)))),
                serialize(constructBinarySearchTreeFromPreorderTraversal.bstFromPreorder(
                        new int[]{8, 5, 1, 7, 10, 12})));
    }
}