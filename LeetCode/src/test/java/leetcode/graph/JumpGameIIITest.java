package leetcode.graph;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JumpGameIIITest {

  private JumpGameIII jumpGameIII = new JumpGameIII.Solution();

  @Test
  void arr4230312start5() {
    assertTrue(jumpGameIII.canReach(new int[]{4, 2, 3, 0, 3, 1, 2}, 5));
  }

  @Test
  void arr4230312start0() {
    assertTrue(jumpGameIII.canReach(new int[]{4, 2, 3, 0, 3, 1, 2}, 0));
  }

  @Test
  void arr30212start2() {
    assertFalse(jumpGameIII.canReach(new int[]{3, 0, 2, 1, 2}, 2));
  }

  @Test
  void arr0306334start6() {
    assertTrue(jumpGameIII.canReach(new int[]{0, 3, 0, 6, 3, 3, 4}, 6));
  }
}