package leetcode.graph;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FlowerPlantingWithNoAdjacentTest {

    private FlowerPlantingWithNoAdjacent flowerPlantingWithNoAdjacent = new FlowerPlantingWithNoAdjacent.Solution();

    @Test void n3paths12_23_31() {
        assertArrayEquals(new int[]{1, 2, 3},
                flowerPlantingWithNoAdjacent.gardenNoAdj(3, new int[][]{{1, 2}, {2, 3}, {3, 1}}));
    }

    @Test void n4paths12_34() {
        assertArrayEquals(new int[]{1, 2, 1, 2},
                flowerPlantingWithNoAdjacent.gardenNoAdj(4, new int[][]{{1, 2}, {3, 4}}));
    }

    @Test void n4paths12_23_34_41_13_24() {
        assertArrayEquals(new int[]{1, 2, 3, 4},
                flowerPlantingWithNoAdjacent.gardenNoAdj(4, new int[][]{{1 ,2}, {2, 3}, {3, 4}, {4, 1}, {1, 3}, {2, 4}}));
    }
}