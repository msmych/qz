package leetcode.graph;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class MaximumLevelSumOfABinaryTreeTest {

  private MaximumLevelSumOfABinaryTree maximumLevelSumOfABinaryTree = new MaximumLevelSumOfABinaryTree.Solution();

  @Test void tree170() {
    assertEquals(2, maximumLevelSumOfABinaryTree.maxLevelSum(fromValAndLeftAndRight(1,
      fromValAndLeftAndRight(7, new TreeNode(7), new TreeNode(-8)),
      new TreeNode(0))));
  }
}
