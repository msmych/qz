package leetcode.graph;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountServersThatCommunicateTest {

  private CountServersThatCommunicate countServersThatCommunicate = new CountServersThatCommunicate.Solution();

  @Test
  void grid1001() {
    assertEquals(0, countServersThatCommunicate.countServers(new int[][]{{1, 0}, {0, 1}}));
  }

  @Test
  void grid1011() {
    assertEquals(3, countServersThatCommunicate.countServers(new int[][]{{1, 0}, {1, 1}}));
  }

  @Test
  void grid1100001000100001() {
    assertEquals(4, countServersThatCommunicate.countServers(new int[][]{
      {1, 1, 0, 0},
      {0, 0, 1, 0},
      {0, 0, 1, 0},
      {0, 0, 0, 1}}));
  }

  @Test
  void grid5x5() {
    assertEquals(11, countServersThatCommunicate.countServers(new int[][]{
      {1, 1, 1, 0, 0, 1},
      {1, 1, 0, 0, 1, 1},
      {0, 0, 0, 0, 0, 0},
      {1, 1, 0, 0, 0, 1}}));
  }
}