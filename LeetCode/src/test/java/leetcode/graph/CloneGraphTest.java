package leetcode.graph;

import org.junit.jupiter.api.Test;

class CloneGraphTest {

    private CloneGraph cloneGraph = new CloneGraph.Solution();

    @Test void graph012_12_22() {
        UndirectedGraphNode node2 = new UndirectedGraphNode(2);
        node2.neighbors.add(node2);
        UndirectedGraphNode node0 = UndirectedGraphNode.fromLabelAndNeighbors(0,
                UndirectedGraphNode.fromLabelAndNeighbors(1, node2), node2);
        cloneGraph.cloneGraph(node0);
    }
}