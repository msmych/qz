package leetcode.graph;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class KeysAndRoomsTest {

    private KeysAndRooms keysAndRooms = new KeysAndRooms.Solution();

    @Test void input123_true() {
        assertTrue(keysAndRooms.canVisitAllRooms(Arrays.asList(singletonList(1), singletonList(2), singletonList(3), emptyList())));
    }

    @Test void input13_301_2_0false() {
        assertFalse(keysAndRooms.canVisitAllRooms(Arrays.asList(Arrays.asList(1,3), Arrays.asList(3, 0, 1), singletonList(2), singletonList(0))));
    }
}