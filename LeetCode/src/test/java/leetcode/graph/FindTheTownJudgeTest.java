package leetcode.graph;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FindTheTownJudgeTest {

    private FindTheTownJudge findTheTownJudge = new FindTheTownJudge.Solution();

    @Test void n2trust12output2() {
        assertEquals(2, findTheTownJudge.findJudge(2, new int[][]{{1, 2}}));
    }

    @Test void n3trust13$23output3() {
        assertEquals(3, findTheTownJudge.findJudge(3, new int[][]{{1, 3}, {2, 3}}));
    }

    @Test void n3trust13$23$31outputN1() {
        assertEquals(-1, findTheTownJudge.findJudge(3, new int[][]{{1, 3}, {2, 3}, {3, 1}}));
    }

    @Test void n3trust12$23outputN1() {
        assertEquals(-1, findTheTownJudge.findJudge(3, new int[][]{{1, 2}, {2, 3}}));
    }

    @Test void n4trust13$14$23$24$43output3() {
        assertEquals(3, findTheTownJudge.findJudge(4, new int[][]{{1, 3}, {1, 4}, {2, 3}, {2, 4}, {4, 3}}));
    }
}