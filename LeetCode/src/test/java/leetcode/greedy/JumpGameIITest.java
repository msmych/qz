package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JumpGameIITest {

    private JumpGameII jumpGameII = new JumpGameII.Solution();

    @Test void input23114output2() {
        assertEquals(2, jumpGameII.jump(new int[]{2, 3, 1, 1, 4}));
    }

    @Test
    void jump16() {
        assertEquals(16, jumpGameII.jump(
                new int[]{5,8,1,8,9,8,7,1,7,5,8,6,5,4,7,3,9,9,0,6,6,3,4,8,0,5,8,9,5,3,7,2,1,8,2,3,8,9,4,7,6,2,5,2,8,2,7,
                        9,3,7,6,9,2,0,8,2,7,8,4,4,1,1,6,4,1,0,7,2,0,3,9,8,7,7,0,6,9,9,7,3,6,3,4,8,6,4,3,3,2,7,8,5,8,6,0}));
    }
}