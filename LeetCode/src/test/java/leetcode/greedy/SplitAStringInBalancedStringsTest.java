package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SplitAStringInBalancedStringsTest {

  private SplitAStringInBalancedStrings splitAStringInBalancedStrings = new SplitAStringInBalancedStrings.Solution();

  @Test
  void RLRRLLRLRL() {
    assertEquals(4, splitAStringInBalancedStrings.balancedStringSplit("RLRRLLRLRL"));
  }

  @Test
  void RLLLLRRRLR() {
    assertEquals(3, splitAStringInBalancedStrings.balancedStringSplit("RLLLLRRRLR"));
  }

  @Test
  void LLLLRRRR() {
    assertEquals(1, splitAStringInBalancedStrings.balancedStringSplit("LLLLRRRR"));
  }

  @Test
  void RRLRRLRLLLRL() {
    assertEquals(2, splitAStringInBalancedStrings.balancedStringSplit("RRLRRLRLLLRL"));
  }

}
