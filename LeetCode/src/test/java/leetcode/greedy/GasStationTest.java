package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GasStationTest {

    private GasStation gasStation = new GasStation.Solution();

    @Test void gas12345cost34512output3() {
        assertEquals(3, gasStation.canCompleteCircuit(new int[]{1, 2, 3, 4, 5}, new int[]{3, 4, 5, 1, 2}));
    }

    @Test void gas234cost343outputN1() {
        assertEquals(-1, gasStation.canCompleteCircuit(new int[]{2, 3, 4}, new int[]{3, 4, 3}));
    }
}