package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JumpGameTest {

  private JumpGame jumpGame = new JumpGame.Solution();

  @Test
  void input23114true() {
    assertTrue(jumpGame.canJump(new int[]{2, 3, 1, 1, 4}));
  }

  @Test
  void input32104() {
    assertFalse(jumpGame.canJump(new int[]{3, 2, 1, 0, 4}));
  }
}
