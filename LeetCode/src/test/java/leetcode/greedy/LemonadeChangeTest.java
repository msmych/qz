package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LemonadeChangeTest {

    private LemonadeChange lemonadeChange = new LemonadeChange.Solution();

    @Test void input5_5_5_10_20true() {
        assertTrue(lemonadeChange.lemonadeChange(new int[]{5, 5, 5, 10, 20}));
    }

    @Test void input5_5_10true() {
        assertTrue(lemonadeChange.lemonadeChange(new int[]{5, 5, 10}));
    }

    @Test void input10_10false() {
        assertFalse(lemonadeChange.lemonadeChange(new int[]{10, 10}));
    }

    @Test void input5_5_10_10_20false() {
        assertFalse(lemonadeChange.lemonadeChange(new int[]{5, 5, 10, 10, 20}));
    }

    @Test void input5_5_5_5_20_20_5_5_5_5false() {
        assertFalse(lemonadeChange.lemonadeChange(new int[]{5, 5, 5, 5, 20, 20, 5, 5, 5, 5}));
    }
}