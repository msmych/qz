package leetcode.greedy;

import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class DivideArrayInSetsOfKConsecutiveNumbersTest {

  private DivideArrayInSetsOfKConsecutiveNumbers divideArrayInSetsOfKConsecutiveNumbers =
    new DivideArrayInSetsOfKConsecutiveNumbers.Solution();

  @Test
  void nums12334456k4() {
    assertTrue(divideArrayInSetsOfKConsecutiveNumbers.isPossibleDivide(new int[]{1, 2, 3, 3, 4, 4, 5, 6}, 4));
  }

  @Test
  void nums32123434591011k3() {
    assertTrue(divideArrayInSetsOfKConsecutiveNumbers.isPossibleDivide(new int[]{3, 2, 1, 2, 3, 4, 3, 4, 5, 9, 10, 11}, 3));
  }

  @Test
  void nums332211k3() {
    assertTrue(divideArrayInSetsOfKConsecutiveNumbers.isPossibleDivide(new int[]{3, 3, 2, 2, 1, 1}, 3));
  }

  @Test
  void nums1234k3() {
    assertFalse(divideArrayInSetsOfKConsecutiveNumbers.isPossibleDivide(new int[]{1, 2, 3, 4}, 3));
  }

  @Test
  void nums8910k5() {
    assertTrue(divideArrayInSetsOfKConsecutiveNumbers.isPossibleDivide(new int[]{8, 9, 10, 11, 12, 10, 11, 12, 13, 14, 8, 9, 10, 11, 12, 17, 18, 19, 20, 21}, 5));
  }

  @Test
  void nums12() {
    System.out.println(divideArrayInSetsOfKConsecutiveNumbers.isPossibleDivide(IntStream.concat(IntStream.generate(() -> 1).limit(25000), IntStream.generate(() -> 2).limit(25000)).toArray(), 2));
  }
}