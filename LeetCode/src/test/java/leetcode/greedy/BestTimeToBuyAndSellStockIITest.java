package leetcode.greedy;

import leetcode.array.BestTimeToBuyAndSellStock;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BestTimeToBuyAndSellStockIITest {

    private BestTimeToBuyAndSellStock bestTimeToBuyAndSellStockII = new BestTimeToBuyAndSellStock.SolutionII();

    @Test void input715364output7() {
        assertEquals(7, bestTimeToBuyAndSellStockII.maxProfit(new int[]{7, 1, 5, 3, 6, 4}));
    }

    @Test void input12345output4() {
        assertEquals(4, bestTimeToBuyAndSellStockII.maxProfit(new int[]{1, 2, 3, 4, 5}));
    }

    @Test void input76431output0() {
        assertEquals(0, bestTimeToBuyAndSellStockII.maxProfit(new int[]{7, 6, 4, 3, 1}));
    }

    @Test void input2145297output11() {
        assertEquals(11, bestTimeToBuyAndSellStockII.maxProfit(new int[]{2, 1, 4, 5, 2, 9, 7}));
    }
}