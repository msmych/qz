package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AssignCookiesTest {

    private AssignCookies assignCookies = new AssignCookies.Solution();

    @Test void input123() {
        assertEquals(1, assignCookies.findContentChildren(new int[]{1, 2, 3}, new int[]{1, 1}));
    }

    @Test void input12() {
        assertEquals(2, assignCookies.findContentChildren(new int[]{1, 2}, new int[]{1, 2, 3}));
    }
}