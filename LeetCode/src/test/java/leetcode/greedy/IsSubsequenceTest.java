package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IsSubsequenceTest {

    private IsSubsequence isSubsequence = new IsSubsequence.Solution();

    @Test void abc() {
        assertTrue(isSubsequence.isSubsequence("abc", "ahbgdc"));
    }

    @Test void axc() {
        assertFalse(isSubsequence.isSubsequence("axc", "ahbgdc"));
    }
}