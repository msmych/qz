package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaximizeSumOfArrayAfterKNegationsTest {

    private MaximizeSumOfArrayAfterKNegations maximizeSumOfArrayAfterKNegations =
            new MaximizeSumOfArrayAfterKNegations.Solution();

    @Test void a423k1output5() {
        assertEquals(5,
                maximizeSumOfArrayAfterKNegations.largestSumAfterKNegations(new int[]{4, 2, 3}, 1));
    }

    @Test
    void a3N102k3output6() {
        assertEquals(6,
                maximizeSumOfArrayAfterKNegations.largestSumAfterKNegations(new int[]{3, -1, 0, 2}, 3));
    }

    @Test void a2N3N15N4k2output13() {
        assertEquals(13,
                maximizeSumOfArrayAfterKNegations.largestSumAfterKNegations(new int[]{2, -3, -1, 5, -4}, 2));
    }
}