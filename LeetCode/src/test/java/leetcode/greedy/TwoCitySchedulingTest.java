package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TwoCitySchedulingTest {

    private TwoCityScheduling twoCityScheduling = new TwoCityScheduling.Solution();

    @Test void input1020() {
        assertEquals(110, twoCityScheduling.twoCitySchedCost(
                new int[][]{{10, 20}, {30, 200}, {400, 50}, {30, 20}}));
    }
}