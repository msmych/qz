package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CandyTest {

    private Candy candy = new Candy.Solution();

    @Test void input102output5() {
        assertEquals(5, candy.candy(new int[]{1, 0, 2}));
    }

    @Test void input122output4() {
        assertEquals(4, candy.candy(new int[]{1, 2, 2}));
    }

    @Test void input1_6_10_8_7_3_2output18() {
        assertEquals(18, candy.candy(new int[]{1,6,10,8,7,3,2}));
    }
}