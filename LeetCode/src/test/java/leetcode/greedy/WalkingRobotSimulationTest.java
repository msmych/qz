package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WalkingRobotSimulationTest {

    private WalkingRobotSimulation walkingRobotSimulation = new WalkingRobotSimulation.Solution();

    @Test void commands4N13obstacles_output25() {
        assertEquals(25, walkingRobotSimulation.robotSim(new int[]{4, -1, 3}, new int[][]{}));
    }

    @Test void commands4N14N24obstacles24output65() {
        assertEquals(65, walkingRobotSimulation.robotSim(new int[]{4, -1, 4, -2, 4}, new int[][]{{2, 4}}));
    }
}