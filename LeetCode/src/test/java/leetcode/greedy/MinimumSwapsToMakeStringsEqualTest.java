package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinimumSwapsToMakeStringsEqualTest {

  private MinimumSwapsToMakeStringsEqual minimumSwapsToMakeStringsEqual = new MinimumSwapsToMakeStringsEqual.Solution();

  @Test
  void xx_yy() {
    assertEquals(1, minimumSwapsToMakeStringsEqual.minimumSwap("xx", "yy"));
  }

  @Test
  void xy_yx() {
    assertEquals(2, minimumSwapsToMakeStringsEqual.minimumSwap("xy", "yx"));
  }

  @Test
  void xx_xy() {
    assertEquals(-1, minimumSwapsToMakeStringsEqual.minimumSwap("xx", "xy"));
  }

  @Test
  void xxyyxyxyxx_xyyxyxxxyx() {
    assertEquals(4, minimumSwapsToMakeStringsEqual.minimumSwap("xxyyxyxyxx", "xyyxyxxxyx"));
  }
}