package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayWithChipsTest {

  private PlayWithChips playWithChips = new PlayWithChips.Solution();

  @Test
  void chips123() {
    assertEquals(1, playWithChips.minCostToMoveChips(new int[]{1, 2, 3}));
  }

  @Test
  void chips22233() {
    assertEquals(2, playWithChips.minCostToMoveChips(new int[]{2, 2, 2, 3, 3}));
  }
}