package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionEquivalentToArrays;

class ReconstructA2RowBinaryMatrixTest {

  private ReconstructA2RowBinaryMatrix reconstructA2RowBinaryMatrix = new ReconstructA2RowBinaryMatrix.Solution();

  @Test
  void u2l1colsum111() {
    assertListCollectionEquivalentToArrays(new Integer[][]{{1, 1, 0}, {0, 0, 1}},
      reconstructA2RowBinaryMatrix.reconstructMatrix(2, 1, new int[]{1, 1, 1}));
  }

  @Test
  void u2l3colsum2211() {
    assertListCollectionEquivalentToArrays(new Integer[0][0],
      reconstructA2RowBinaryMatrix.reconstructMatrix(2, 3, new int[]{2, 2, 1, 1}));
  }

  @Test
  void u5l5() {
    assertListCollectionEquivalentToArrays(new Integer[][]{{1, 1, 1, 0, 1, 0, 0, 1, 0, 0}, {1, 0, 1, 0, 0, 0, 1, 1, 0, 1}},
      reconstructA2RowBinaryMatrix.reconstructMatrix(5, 5, new int[]{2, 1, 2, 0, 1, 0, 1, 2, 0, 1}));
  }

  @Test
  void u4l7() {
    assertListCollectionEquivalentToArrays(new Integer[0][0],
      reconstructA2RowBinaryMatrix.reconstructMatrix(4, 7, new int[]{2, 1, 2, 2, 1, 1, 1}));
  }
}