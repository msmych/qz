package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HowManyApplesCanYouPutIntoTheBasketTest {

  private HowManyApplesCanYouPutIntoTheBasket howManyApplesCanYouPutIntoTheBasket =
    new HowManyApplesCanYouPutIntoTheBasket.Solution();

  @Test void arr100() {
    assertEquals(4, howManyApplesCanYouPutIntoTheBasket.maxNumberOfApples(new int[]{100, 200, 150, 1000}));
  }

  @Test void arr900() {
    assertEquals(5, howManyApplesCanYouPutIntoTheBasket.maxNumberOfApples(new int[]{900, 950, 800, 1000, 700, 800}));
  }
}
