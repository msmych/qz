package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringWithoutAAAorBBBTest {

    private StringWithoutAAAorBBB stringWithoutAAAorBBB = new StringWithoutAAAorBBB.Solution();

    @Test void A1B2_abb() {
        assertWithoutAAAorBBB(1, 2, stringWithoutAAAorBBB.strWithout3a3b(1, 2));
    }

    private void assertWithoutAAAorBBB(int a, int b, String s) {
        assertEquals(a + b, s.length());
        assertFalse(s.contains("aaa"));
        assertFalse(s.contains("bbb"));
    }

    @Test void A4B1_aabaa() {
        assertWithoutAAAorBBB(4, 1, stringWithoutAAAorBBB.strWithout3a3b(4, 1));
    }

    @Test void A1B3_bbab() {
        assertWithoutAAAorBBB(1, 3, stringWithoutAAAorBBB.strWithout3a3b(1, 3));
    }

    @Test void A71B81() {
        assertWithoutAAAorBBB(71, 81, stringWithoutAAAorBBB.strWithout3a3b(71, 81));
    }
}