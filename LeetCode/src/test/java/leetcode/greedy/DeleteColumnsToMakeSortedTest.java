package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DeleteColumnsToMakeSortedTest {

    private DeleteColumnsToMakeSorted deleteColumnsToMakeSorted = new DeleteColumnsToMakeSorted.Solution();

    @Test void input_cba_daf_ghi_output1() {
        assertEquals(1, deleteColumnsToMakeSorted.minDeletionSize(new String[]{"cba", "daf", "ghi"}));
    }

    @Test void input_a_b_output0() {
        assertEquals(0, deleteColumnsToMakeSorted.minDeletionSize(new String[]{"a", "b"}));
    }

    @Test void input_zyx_wvu_tsr_output3() {
        assertEquals(3, deleteColumnsToMakeSorted.minDeletionSize(new String[]{"zyx", "wvu", "tsr"}));
    }
}