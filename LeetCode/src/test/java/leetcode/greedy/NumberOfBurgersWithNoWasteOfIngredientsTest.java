package leetcode.greedy;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;

class NumberOfBurgersWithNoWasteOfIngredientsTest {

  private NumberOfBurgersWithNoWasteOfIngredients numberOfBurgersWithNoWasteOfIngredients =
    new NumberOfBurgersWithNoWasteOfIngredients.Solution();

  @Test
  void t16c7() {
    assertEquals(Arrays.asList(1, 6), numberOfBurgersWithNoWasteOfIngredients.numOfBurgers(16, 7));
  }

  @Test
  void t17c4() {
    assertEquals(emptyList(), numberOfBurgersWithNoWasteOfIngredients.numOfBurgers(17, 4));
  }

  @Test
  void t4c17() {
    assertEquals(emptyList(), numberOfBurgersWithNoWasteOfIngredients.numOfBurgers(4, 17));
  }

  @Test
  void t0c0() {
    assertEquals(Arrays.asList(0, 0), numberOfBurgersWithNoWasteOfIngredients.numOfBurgers(0, 0));
  }

  @Test
  void t2c1() {
    assertEquals(Arrays.asList(0, 1), numberOfBurgersWithNoWasteOfIngredients.numOfBurgers(2, 1));
  }

  @Test
  void t16c5() {
    assertEquals(Arrays.asList(3, 2), numberOfBurgersWithNoWasteOfIngredients.numOfBurgers(16, 5));
  }

  @Test
  void t7378788c2654456() {
    assertEquals(Arrays.asList(1034938, 1619518), numberOfBurgersWithNoWasteOfIngredients.numOfBurgers(7378788, 2654456));
  }

  @Test
  void t7353172c2977556() {
    assertEquals(Arrays.asList(699030, 2278526), numberOfBurgersWithNoWasteOfIngredients.numOfBurgers(7353172, 2977556));
  }
}