package leetcode.greedy;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionContainsArrays;

class GroupThePeopleGivenTheGroupSizeTheyBelongToTest {

  private GroupThePeopleGivenTheGroupSizeTheyBelongTo groupThePeopleGivenTheGroupSizeTheyBelongTo =
    new GroupThePeopleGivenTheGroupSizeTheyBelongTo.Solution();

  @Test
  void sizes3333313() {
    assertListCollectionContainsArrays(new Integer[][]{{0, 1, 2}, {3, 4, 6}, {5}},
      groupThePeopleGivenTheGroupSizeTheyBelongTo.groupThePeople(new int[]{3, 3, 3, 3, 3, 1, 3}));
  }

  @Test
  void sizes213332() {
    assertListCollectionContainsArrays(new Integer[][]{{0, 5}, {1}, {2, 3, 4}},
      groupThePeopleGivenTheGroupSizeTheyBelongTo.groupThePeople(new int[]{2, 1, 3, 3, 3, 2}));
  }
}