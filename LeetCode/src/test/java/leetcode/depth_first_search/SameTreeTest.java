package leetcode.depth_first_search;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class SameTreeTest {

    private SameTree sameTree = new SameTree.Solution();
    private SameTree iterative = new SameTree.IterativeSolution();

    @Test void tree123tree123true() {
        TreeNode tree1 = fromValAndLeftAndRight(1, new TreeNode(2), new TreeNode(3));
        TreeNode tree2 = fromValAndLeftAndRight(1, new TreeNode(2), new TreeNode(3));
        assertTrue(sameTree.isSameTree(tree1, tree2));
        assertTrue(iterative.isSameTree(tree1, tree2));
    }

    @Test void tree12_tree1_2false() {
        TreeNode tree1 = fromValAndLeft(1, new TreeNode(2));
        TreeNode tree2 = fromValAndLeftAndRight(2, null, new TreeNode(2));
        assertFalse(sameTree.isSameTree(tree1, tree2));
        assertFalse(iterative.isSameTree(tree1, tree2));
    }

    @Test void tree121tree112false() {
        TreeNode tree1 = fromValAndLeftAndRight(1, new TreeNode(2), new TreeNode(1));
        TreeNode tree2 = fromValAndLeftAndRight(1, new TreeNode(1), new TreeNode(2));
        assertFalse(sameTree.isSameTree(tree1, tree2));
        assertFalse(iterative.isSameTree(tree1, tree2));
    }
}