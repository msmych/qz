package leetcode.depth_first_search;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.linked_list.ListNode.fromVals;
import static leetcode.tree.TreeNode.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ConvertSortedListToBinarySearchTreeTest {

    private ConvertSortedListToBinarySearchTree convertSortedListToBinarySearchTree =
            new ConvertSortedListToBinarySearchTree.Solution();

    @Test void listN10N3059() {
        assertEquals(serialize(fromValAndLeftAndRight(0,
                fromValAndLeft(-3, new TreeNode(-10)),
                fromValAndLeft(9, new TreeNode(5)))),
                serialize(convertSortedListToBinarySearchTree.sortedListToBST(fromVals(-10, -3, 0, 5, 9))));
    }
}