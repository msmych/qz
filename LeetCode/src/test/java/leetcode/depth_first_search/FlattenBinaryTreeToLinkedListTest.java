package leetcode.depth_first_search;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static leetcode.tree.TreeNode.serialize;
import static org.junit.jupiter.api.Assertions.*;

class FlattenBinaryTreeToLinkedListTest {

    private FlattenBinaryTreeToLinkedList flattenBinaryTreeToLinkedList = new FlattenBinaryTreeToLinkedList.Solution();

    @Test void tree1to6() {
        TreeNode tree = fromValAndLeftAndRight(1,
                fromValAndLeftAndRight(2, new TreeNode(3), new TreeNode(4)),
                fromValAndLeftAndRight(5, null, new TreeNode(6)));
        flattenBinaryTreeToLinkedList.flatten(tree);
        assertEquals(serialize(
                fromValAndLeftAndRight(1, null,
                        fromValAndLeftAndRight(2, null,
                                fromValAndLeftAndRight(3, null,
                                        fromValAndLeftAndRight(4, null,
                                                fromValAndLeftAndRight(5, null, new TreeNode(6))))))),
                serialize(tree));
    }
}