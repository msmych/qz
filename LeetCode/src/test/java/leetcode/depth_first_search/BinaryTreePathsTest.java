package leetcode.depth_first_search;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static utils.TestUtils.assertCollectionContainsAll;

class BinaryTreePathsTest {

  private BinaryTreePaths binaryTreePaths = new BinaryTreePaths.Solution();

  @Test
  void tree1235() {
    assertCollectionContainsAll(binaryTreePaths.binaryTreePaths(fromValAndLeftAndRight(1,
      fromValAndLeftAndRight(2, null, new TreeNode(5)),
      new TreeNode(3))),
      "1->2->5", "1->3");
  }
}
