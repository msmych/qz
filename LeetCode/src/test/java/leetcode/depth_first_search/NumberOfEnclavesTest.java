package leetcode.depth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfEnclavesTest {

    private NumberOfEnclaves numberOfEnclaves = new NumberOfEnclaves.Solution();

    @Test void input0000_1010_0110_0000output3() {
        assertEquals(3, numberOfEnclaves.numEnclaves(new int[][]{
                {0, 0, 0, 0},
                {1, 0, 1, 0},
                {0, 1, 1, 0},
                {0, 0, 0, 0}
        }));
    }

    @Test void input0110_0010_0010_0000output0() {
        assertEquals(0, numberOfEnclaves.numEnclaves(new int[][]{
                {0, 1, 1, 0},
                {0, 0, 1, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 0}
        }));
    }
}