package leetcode.depth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfClosedIslandsTest {

  private NumberOfClosedIslands numberOfClosedIslands = new NumberOfClosedIslands.Solution();

  @Test
  void cx() {
    assertEquals(2, numberOfClosedIslands.closedIsland(new int[][]{
      {1, 1, 1, 1, 1, 1, 1, 0},
      {1, 0, 0, 0, 0, 1, 1, 0},
      {1, 0, 1, 0, 1, 1, 1, 0},
      {1, 0, 0, 0, 0, 1, 0, 1},
      {1, 1, 1, 1, 1, 1, 1, 0}
    }));
  }

  @Test
  void x() {
    assertEquals(1, numberOfClosedIslands.closedIsland(new int[][]{
      {0, 0, 1, 0, 0},
      {0, 1, 0, 1, 0},
      {0, 1, 1, 1, 0}
    }));
  }

  @Test
  void o() {
    assertEquals(2, numberOfClosedIslands.closedIsland(new int[][]{
      {1, 1, 1, 1, 1, 1, 1},
      {1, 0, 0, 0, 0, 0, 1},
      {1, 0, 1, 1, 1, 0, 1},
      {1, 0, 1, 0, 1, 0, 1},
      {1, 0, 1, 1, 1, 0, 1},
      {1, 0, 0, 0, 0, 0, 1},
      {1, 1, 1, 1, 1, 1, 1}
    }));
  }
}