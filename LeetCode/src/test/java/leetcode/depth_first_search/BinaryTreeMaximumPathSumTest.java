package leetcode.depth_first_search;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class BinaryTreeMaximumPathSumTest {

  private BinaryTreeMaximumPathSum binaryTreeMaximumPathSum = new BinaryTreeMaximumPathSum.Solution();

  @Test
  void tree123output6() {
    assertEquals(6, binaryTreeMaximumPathSum.maxPathSum(
      fromValAndLeftAndRight(1, new TreeNode(2), new TreeNode(3))));
  }

  @Test
  void treeN10() {
    assertEquals(42, binaryTreeMaximumPathSum.maxPathSum(
      fromValAndLeftAndRight(-10,
        new TreeNode(-9),
        fromValAndLeftAndRight(20,
          new TreeNode(15),
          new TreeNode(7)))));
  }

  @Test
  void treeN3() {
    assertEquals(-3, binaryTreeMaximumPathSum.maxPathSum(new TreeNode(-3)));
  }

  @Test
  void tree1N2N3() {
    assertEquals(3, binaryTreeMaximumPathSum.maxPathSum(
      fromValAndLeftAndRight(1,
        fromValAndLeftAndRight(-2,
          fromValAndLeft(1, new TreeNode(-1)),
          new TreeNode(3)),
        fromValAndLeft(-3, new TreeNode(-2)))));
  }
}