package leetcode.depth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeleteTreeNodesTest {

  private DeleteTreeNodes deleteTreeNodes = new DeleteTreeNodes.Solution();

  @Test
  void nodes7() {
    assertEquals(2,
      deleteTreeNodes.deleteTreeNodes(7, new int[]{-1, 0, 0, 1, 2, 2, 2}, new int[]{1, -2, 4, 0, -2, -1, -1}));
  }
}