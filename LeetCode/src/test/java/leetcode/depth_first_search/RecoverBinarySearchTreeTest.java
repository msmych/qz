package leetcode.depth_first_search;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.*;
import static org.junit.jupiter.api.Assertions.*;

class RecoverBinarySearchTreeTest {

  private RecoverBinarySearchTree recoverBinarySearchTree = new RecoverBinarySearchTree.Solution();

  @Test
  void tree132() {
    TreeNode node = fromValAndLeft(1, fromValAndLeftAndRight(3, null, new TreeNode(2)));
    recoverBinarySearchTree.recoverTree(node);
    assertEquals(serialize(fromValAndLeft(3, fromValAndLeftAndRight(1, null, new TreeNode(2)))),
      serialize(node));
  }

  @Test
  void tree3142() {
    TreeNode node = fromValAndLeftAndRight(3, new TreeNode(1), fromValAndLeft(4, new TreeNode(2)));
    recoverBinarySearchTree.recoverTree(node);
    assertEquals(serialize(fromValAndLeftAndRight(2, new TreeNode(1), fromValAndLeft(4, new TreeNode(3)))),
      serialize(node));
  }

  @Test
  void tree231() {
    TreeNode node = fromValAndLeftAndRight(2, new TreeNode(3), new TreeNode(1));
    recoverBinarySearchTree.recoverTree(node);
    assertEquals(serialize(fromValAndLeftAndRight(2, new TreeNode(1), new TreeNode(3))),
      serialize(node));
  }
}