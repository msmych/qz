package leetcode.depth_first_search;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class DeepestLeavesSumTest {

  private DeepestLeavesSum deepestLeavesSum = new DeepestLeavesSum.Solution();

  @Test
  void root12345() {
    assertEquals(15, deepestLeavesSum.deepestLeavesSum(fromValAndLeftAndRight(1,
      fromValAndLeftAndRight(2,
        fromValAndLeft(4, new TreeNode(7)),
        new TreeNode(5)),
      fromValAndLeftAndRight(3, null,
        fromValAndLeftAndRight(6, null, new TreeNode(8))))));
  }
}