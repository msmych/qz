package leetcode.depth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ColoringABorderTest {

    private ColoringABorder coloringABorder = new ColoringABorder.Solution();

    @Test void grid11_12r0c0color3output33_32() {
        assertArrayEquals(new int[][]{{3, 3}, {3, 2}}, coloringABorder.colorBorder(new int[][]{{1, 1}, {1, 2}}, 0, 0, 3));
    }

    @Test void grid122_232r0c1color3output133_233() {
        assertArrayEquals(new int[][]{{1, 3, 3}, {2, 3, 3}}, coloringABorder.colorBorder(new int[][]{{1, 2, 2}, {2, 3, 2}}, 0, 1, 3));
    }

    @Test void grid111_111_111r1c1color2output222_212_222() {
        assertArrayEquals(new int[][]{{2, 2, 2}, {2, 1, 2}, {2, 2, 2}}, coloringABorder.colorBorder(new int[][]{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}}, 1, 1, 2));
    }

    @Test void grid213112() {
        assertArrayEquals(new int[][]{
                {2,1,3,2,1,1,3},
                {1,2,3,1,3,1,3},
                {1,2,1,3,2,3,3},
                {2,1,3,3,2,3,3},
                {2,3,3,3,3,1,3}
        }, coloringABorder.colorBorder(new int[][]{
                        {2, 1, 3, 2, 1, 1, 2},
                        {1, 2, 3, 1, 2, 1, 2},
                        {1, 2, 1, 2, 2, 2, 2},
                        {2, 1, 2, 2, 2, 2, 2},
                        {2, 3, 3, 3, 2, 1, 2}
                }, 4,4, 3));
    }
}