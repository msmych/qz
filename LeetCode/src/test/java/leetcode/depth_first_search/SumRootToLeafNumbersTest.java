package leetcode.depth_first_search;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class SumRootToLeafNumbersTest {

    private SumRootToLeafNumbers sumRootToLeafNumbers = new SumRootToLeafNumbers.Solution();

    @Test void input123output25() {
        assertEquals(25, sumRootToLeafNumbers.sumNumbers(
                fromValAndLeftAndRight(1, new TreeNode(2), new TreeNode(3))));
    }

    @Test void input49051output1026() {
        assertEquals(1026, sumRootToLeafNumbers.sumNumbers(
                fromValAndLeftAndRight(4,
                        fromValAndLeftAndRight(9, new TreeNode(5), new TreeNode(1)),
                        new TreeNode(0))));
    }
}