package leetcode.depth_first_search;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.*;
import static org.junit.jupiter.api.Assertions.*;

class RecoverATreeFromPreorderTraversalTest {

    private RecoverATreeFromPreorderTraversal recoverATreeFromPreorderTraversal =
            new RecoverATreeFromPreorderTraversal.Solution();

    @Test void tree1_2__3__4_5__6__7() {
        assertEquals(serialize(fromValAndLeftAndRight(1,
                fromValAndLeftAndRight(2, new TreeNode(3), new TreeNode(4)),
                fromValAndLeftAndRight(5, new TreeNode(6), new TreeNode(7)))),
                serialize(recoverATreeFromPreorderTraversal.recoverFromPreorder("1-2--3--4-5--6--7")));
    }

    @Test void tree1_2__3___4_5__6___7() {
        assertEquals(serialize(fromValAndLeftAndRight(1,
                fromValAndLeft(2, fromValAndLeft(3, new TreeNode(4))),
                fromValAndLeft(5, fromValAndLeft(6, new TreeNode(7))))),
                serialize(recoverATreeFromPreorderTraversal.recoverFromPreorder("1-2--3---4-5--6---7")));
    }

    @Test
    void tree1_401__349___90__88() {
        assertEquals(serialize(fromValAndLeft(1,
                fromValAndLeftAndRight(401,
                        fromValAndLeft(349, new TreeNode(90)),
                        new TreeNode(88)))),
                serialize(recoverATreeFromPreorderTraversal.recoverFromPreorder("1-401--349---90--88")));
    }
}