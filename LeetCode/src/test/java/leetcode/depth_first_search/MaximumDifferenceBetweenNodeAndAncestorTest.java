package leetcode.depth_first_search;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class MaximumDifferenceBetweenNodeAndAncestorTest {

    private MaximumDifferenceBetweenNodeAndAncestor maximumDifferenceBetweenNodeAndAncestor =
            new MaximumDifferenceBetweenNodeAndAncestor.Solution();

    @Test void tree8() {
        assertEquals(7, maximumDifferenceBetweenNodeAndAncestor.maxAncestorDiff(
                fromValAndLeftAndRight(8,
                        fromValAndLeftAndRight(3,
                                new TreeNode(1),
                                fromValAndLeftAndRight(6,
                                        new TreeNode(4),
                                        new TreeNode(7))),
                        fromValAndLeftAndRight(10,
                                null,
                                fromValAndLeft(14,
                                        new TreeNode(13))))));
    }
}