package leetcode.depth_first_search;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.*;
import static org.junit.jupiter.api.Assertions.*;

class LowestCommonAncestorOfDeepestLeavesTest {

    private LowestCommonAncestorOfDeepestLeaves lowestCommonAncestorOfDeepestLeaves =
            new LowestCommonAncestorOfDeepestLeaves.Solution();

    @Test void root123() {
        TreeNode root = TreeNode.fromValAndLeftAndRight(1, new TreeNode(2), new TreeNode(3));
        assertEquals(serialize(root), serialize(lowestCommonAncestorOfDeepestLeaves.lcaDeepestLeaves(root)));
    }

    @Test void root1234() {
        assertEquals(serialize(new TreeNode(4)),
                serialize(lowestCommonAncestorOfDeepestLeaves.lcaDeepestLeaves(
                        fromValAndLeftAndRight(1, fromValAndLeft(2, new TreeNode(4)), new TreeNode(3)))));
    }

    @Test void root12345() {
        TreeNode anc = fromValAndLeftAndRight(2, new TreeNode(4), new TreeNode(5));
        assertEquals(serialize(anc),
                serialize(lowestCommonAncestorOfDeepestLeaves.lcaDeepestLeaves(
                        fromValAndLeftAndRight(1, anc, new TreeNode(3)))));
    }
}