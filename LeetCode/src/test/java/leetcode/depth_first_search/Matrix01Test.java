package leetcode.depth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Matrix01Test {

    private Matrix01 matrix01 = new Matrix01.Solution();

    @Test void input000_010_000output000_010_000() {
        int[][] matrix = new int[][]{
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}};
        assertArrayEquals(matrix, matrix01.updateMatrix(matrix));
    }

    @Test void input000_010_111output000_010_121() {
        assertArrayEquals(new int[][]{
                {0, 0, 0},
                {0, 1, 0},
                {1, 2, 1}},
                matrix01.updateMatrix(new int[][]{
                {0, 0, 0},
                {0, 1, 0},
                {1, 2, 1}}));
    }
}