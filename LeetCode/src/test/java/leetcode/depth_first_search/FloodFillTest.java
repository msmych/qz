package leetcode.depth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FloodFillTest {

    private FloodFill floodFill = new FloodFill.Solution();

    @Test void image111_110_101sr1sc1newColor2() {
        assertArrayEquals(new int[][]{
                {2, 2, 2},
                {2, 2, 0},
                {2, 0, 1}},
                floodFill.floodFill(new int[][]{
                        {1, 1, 1},
                        {1, 1, 0},
                        {1, 0, 1}}, 1, 1, 2));
    }
}