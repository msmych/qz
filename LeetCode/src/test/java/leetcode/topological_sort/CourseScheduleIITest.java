package leetcode.topological_sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CourseScheduleIITest {

  private CourseScheduleII courseScheduleII = new CourseScheduleII.Solution();

  @Test
  void n2pre10() {
    assertArrayEquals(new int[]{0, 1}, courseScheduleII.findOrder(2, new int[][]{{1, 0}}));
  }

  @Test
  void n4pre10203132() {
    assertArrayEquals(new int[]{0, 1, 2, 3}, courseScheduleII.findOrder(4, new int[][]{{1, 0}, {2, 0}, {3, 1}, {3, 2}}));
  }

  @Test
  void n2pre01() {
    assertArrayEquals(new int[]{1, 0}, courseScheduleII.findOrder(2, new int[][]{{0, 1}}));
  }

  @Test
  void n2pre0110() {
    assertArrayEquals(new int[0], courseScheduleII.findOrder(2, new int[][]{{0, 1}, {1, 0}}));
  }
}