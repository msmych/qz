package leetcode.topological_sort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CourseScheduleTest {

  private CourseSchedule courseSchedule = new CourseSchedule.Solution();

  @Test
  void input2_10true() {
    assertTrue(courseSchedule.canFinish(2, new int[][]{{1, 0}}));
  }

  @Test
  void input2_10_01false() {
    assertFalse(courseSchedule.canFinish(2, new int[][]{{1, 0}, {0, 1}}));
  }

  @Test
  void input4_01_12_23true() {
    assertTrue(courseSchedule.canFinish(4, new int[][]{{0, 1}, {1, 2}, {2, 3}}));
  }

  @Test
  void input5_01_34_43false() {
    assertFalse(courseSchedule.canFinish(5, new int[][]{{0, 1}, {2, 3}, {3, 4}, {4, 2}}));
  }
}