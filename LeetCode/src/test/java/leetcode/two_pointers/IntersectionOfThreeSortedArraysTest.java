package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class IntersectionOfThreeSortedArraysTest {

  private IntersectionOfThreeSortedArrays intersectionOfThreeSortedArrays = new IntersectionOfThreeSortedArrays.Solution();

  @Test
  void arr12345() {
    assertArrayEquals(new Integer[]{1, 5}, intersectionOfThreeSortedArrays.arraysIntersection(
      new int[]{1, 2, 3, 4, 5}, new int[]{1, 2, 5, 7, 9}, new int[]{1, 3, 4, 5, 8}).toArray(new Integer[]{}));
  }
}
