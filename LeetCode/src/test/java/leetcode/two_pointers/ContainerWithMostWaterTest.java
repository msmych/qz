package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ContainerWithMostWaterTest {

  private ContainerWithMostWater containerWithMostWater = new ContainerWithMostWater.Solution();

  @Test
  void input186254837output49() {
    assertEquals(49, containerWithMostWater.maxArea(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7}));
  }
}