package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MergeSortedArrayTest {

    private MergeSortedArray mergeSortedArray = new MergeSortedArray.Solution();

    @Test void nums123000nums256() {
        int[] nums1 = new int[]{1, 2, 3, 0, 0, 0};
        mergeSortedArray.merge(nums1, nums1.length, new int[]{2, 5, 6}, 3);
        assertArrayEquals(new int[]{1, 2, 2, 3, 5, 6}, nums1);
    }

    @Test void numsN100333000nums122() {
        int[] nums1 = new int[]{-1, 0, 0, 3, 3, 3, 0, 0, 0};
        mergeSortedArray.merge(nums1, nums1.length, new int[]{1, 2, 2}, 3);
        assertArrayEquals(new int[]{-1, 0, 0, 1, 2, 2, 3, 3, 3}, nums1);
    }

    @Test void nums0nums1output1() {
        int[] nums1 = new int[]{0};
        mergeSortedArray.merge(nums1, nums1.length, new int[]{1}, 1);
        assertArrayEquals(new int[]{1}, nums1);
    }

    @Test void nums00000nums12345output12345() {
        int[] nums1 = new int[]{0, 0, 0, 0, 0};
        mergeSortedArray.merge(nums1, nums1.length, new int[]{1, 2, 3, 4, 5}, 5);
        assertArrayEquals(new int[]{1, 2, 3, 4, 5}, nums1);
    }
}