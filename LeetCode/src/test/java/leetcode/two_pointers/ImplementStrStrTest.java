package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ImplementStrStrTest {

    private ImplementStrStr implementStrstr = new ImplementStrStr.Solution();

    @Test
    void haystack_hello_needle_ll_output2() {
        assertEquals(2, implementStrstr.strStr("hello", "ll"));
    }

    @Test void haystack_aaaaa_needle_bba_outputN1() {
        assertEquals(-1, implementStrstr.strStr("aaaaa", "bba"));
    }

    @Test
    void haystack_a_emptyNeedle_output0() {
        assertEquals(0, implementStrstr.strStr("a", ""));
    }

    @Test void haystack_a_needle_a_output0() {
        assertEquals(0, implementStrstr.strStr("a", "a"));
    }
}