package leetcode.two_pointers;

import leetcode.Interval;
import org.junit.jupiter.api.Test;

import static leetcode.Interval.intervalEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IntervalListIntersectionsTest {

    private IntervalListIntersections intervalListIntersections = new IntervalListIntersections.Solution();

    @Test void A0$2_5$10_13$23_24$25B1$5_8$12_15$24_25$26output1$2_5$5_8$10_15$23_24$24_25$25() {
        Interval[] intersection = intervalListIntersections.intervalIntersection(
                new Interval[]{new Interval(0, 2), new Interval(5, 10), new Interval(13, 23), new Interval(24, 25)},
                new Interval[]{new Interval(1, 5), new Interval(8, 12), new Interval(15, 24), new Interval(25, 26)});
        assertEquals(6, intersection.length);
        assertTrue(intervalEquals(1, 2, intersection[0]));
        assertTrue(intervalEquals(5, 5, intersection[1]));
        assertTrue(intervalEquals(8, 10, intersection[2]));
        assertTrue(intervalEquals(15, 23, intersection[3]));
        assertTrue(intervalEquals(24, 24, intersection[4]));
        assertTrue(intervalEquals(25, 25, intersection[5]));
    }
}