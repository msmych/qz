package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LongPressedNameTest {

    private LongPressedName longPressedName = new LongPressedName.Solution();

    @Test void name_alex_typed_aaleex_true() {
        assertTrue(longPressedName.isLongPressedName("alex", "aaleex"));
    }

    @Test void name_saeed_typed_ssaaedd_false() {
        assertFalse(longPressedName.isLongPressedName("saeed", "ssaaedd"));
    }

    @Test void name_leelee_typed_lleeelee_true() {
        assertTrue(longPressedName.isLongPressedName("leelee", "lleeelee"));
    }

    @Test void name_laiden_typed_laiden_true() {
        assertTrue(longPressedName.isLongPressedName("laiden", "laiden"));
    }
}