package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ReverseStringTest {

    private ReverseString reverseString = new ReverseString.Solution(),
            recursive = new ReverseString.RecursiveSolution();

    @Test
    void input_hello_output_olleh() {
        assertEquals("olleh", reverseString.reverseString("hello"));
        assertEquals("olleh", recursive.reverseString("hello"));
    }

    @Test void input_A_manC_a_planC_a_canalCC_Panama_output_amanaP_CClanac_a_Cnalp_a_Cnam_A() {
        String s = "A man, a plan, a canal: Panama", reversed = "amanaP :lanac a ,nalp a ,nam A";
        assertEquals(reversed, reverseString.reverseString(s));
        assertEquals(reversed, recursive.reverseString(s));
    }
}