package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class RemoveElementTest {

    private RemoveElement removeElement = new RemoveElement.Solution();

    @Test
    void nums3223val3length2() {
        int[] nums = new int[]{3, 2, 2, 3};
        assertEquals(2, removeElement.removeElement(nums, 3));
        assertArrayEquals(new int[]{2, 2}, Arrays.copyOfRange(nums, 0, 2));
    }

    @Test void nums01223042val2length5() {
        int[] nums = new int[]{0, 1, 2, 2, 3, 0, 4, 2};
        assertEquals(5, removeElement.removeElement(nums, 2));
        assertArrayEquals(new int[]{0, 1, 3, 0, 4}, Arrays.copyOfRange(nums, 0, 5));
    }
}