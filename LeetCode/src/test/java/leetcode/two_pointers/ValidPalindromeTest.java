package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ValidPalindromeTest {

    private ValidPalindrome validPalindrome = new ValidPalindrome.Solution();

    @Test
    void A_man_a_plan_a_canal_Panama__true() {
        assertTrue(validPalindrome.isPalindrome("A man, a plan, a canal: Panama"));
    }

    @Test void race_a_car__false() {
        assertFalse(validPalindrome.isPalindrome("race a car"));
    }
}