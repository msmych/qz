package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class RemoveDuplicatesFromSortedArrayTest {

    private RemoveDuplicatesFromSortedArray removeDuplicatesFromSortedArray =
            new RemoveDuplicatesFromSortedArray.Solution();

    @Test
    void nums112length2() {
        int[] nums = new int[]{1, 1, 2};
        assertEquals(2, removeDuplicatesFromSortedArray.removeDuplicates(nums));
        assertArrayEquals(new int[]{1, 2}, Arrays.copyOfRange(nums, 0, 2));
    }

    @Test void nums0011122334length5() {
        int[] nums = new int[]{0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        assertEquals(5, removeDuplicatesFromSortedArray.removeDuplicates(nums));
        assertArrayEquals(new int[]{0, 1, 2, 3, 4}, Arrays.copyOfRange(nums, 0, 5));
    }
}