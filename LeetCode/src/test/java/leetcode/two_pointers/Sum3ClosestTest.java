package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Sum3ClosestTest {

    private Sum3Closest sum3Closest = new Sum3Closest.Solution();

    @Test void numsN121N4target1closest2() {
        assertEquals(2, sum3Closest.threeSumClosest(new int[]{-1, 2, 1, -4}, 1));
    }

    @Test void nums012target0closest3() {
        assertEquals(3, sum3Closest.threeSumClosest(new int[]{0, 1, 2}, 0));
    }
}