package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static utils.TestUtils.assertListCollectionContainsArrays;

class Sum3Test {

    private Sum3 sum3 = new Sum3.Solution();

    @Test void numsN1012N1N4solutionN101_N1N12() {
        assertListCollectionContainsArrays(new Integer[][]{
                {-1, 0, 1},
                {-1, -1, 2}
        }, sum3.threeSum(new int[]{-1, 0, 1, 2, -1, -4}));
    }

    @Test void numsN101solutionN101() {
        assertListCollectionContainsArrays(new Integer[][]{{-1, 0, 1}}, sum3.threeSum(new int[]{-1, 0, 1}));
    }
}