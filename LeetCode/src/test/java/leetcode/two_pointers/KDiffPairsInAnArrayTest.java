package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KDiffPairsInAnArrayTest {

    private KDiffPairsInAnArray kDiffPairsInAnArray = new KDiffPairsInAnArray.Solution();

    @Test void input31415k2() {
        assertEquals(2, kDiffPairsInAnArray.findPairs(new int[]{3, 1, 4, 1, 5}, 2));
    }

    @Test void input12345k1() {
        assertEquals(4, kDiffPairsInAnArray.findPairs(new int[]{1, 2, 3, 4, 5}, 1));
    }

    @Test void input13154k0() {
        assertEquals(1, kDiffPairsInAnArray.findPairs(new int[]{1, 3, 1, 5, 4}, 0));
    }

    @Test void input11121k1() {
        assertEquals(1, kDiffPairsInAnArray.findPairs(new int[]{1, 1, 1, 2, 1}, 1));
    }

    @Test void k3414150() {
        assertEquals(0, kDiffPairsInAnArray.findPairs(
            new int[]{5816848,4160415,7533527,2956265,4279124,9527555,436133,7438296,7634095,8439619,1933159,7843867,
                      2971885,5415063,3597106,2010864,8471713,3381662,4712093,2474085,1984917,7099308,3199407,1601002,
                      8907755,9272612,1141026,542064,4716676,4997745,6550152,8064816,455878,7092288,1185402,4371297,
                      3913896,4630056,4444850,4712632,2097658,6179915,4464254,1569817,7195438,108628,9799520,2063061,
                      4968337,8995188,7934242,3628529,8975558,7980365,2205390,9912592,1118962,2737440,7593087,3156384,
                      9201152,5565787,63636,8598001,7426073,4337423,4777114,6669185,9735201,8795228,8033571,7430742,
                      550906,3707482,8273694,5355242,1775183,4592861,1092929,7368565,3590314,2790299,8724408,6892832,
                      5329303,1099586,2412630,6380624,127213,814660,919509,3816094,7103178,6548697,2481431,563006,
                      6124270,323741,8328862,3005307
        }, 3414150));
    }
}