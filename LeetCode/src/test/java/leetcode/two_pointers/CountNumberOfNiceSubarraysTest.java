package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountNumberOfNiceSubarraysTest {

  private CountNumberOfNiceSubarrays countNumberOfNiceSubarrays = new CountNumberOfNiceSubarrays.Solution();

  @Test
  void nums11211k3() {
    assertEquals(2, countNumberOfNiceSubarrays.numberOfSubarrays(new int[]{1, 1, 2, 1, 1}, 3));
  }

  @Test
  void nums246k1() {
    assertEquals(0, countNumberOfNiceSubarrays.numberOfSubarrays(new int[]{2, 4, 6}, 1));
  }

  @Test
  void nums2221221222k2() {
    assertEquals(16, countNumberOfNiceSubarrays.numberOfSubarrays(new int[]{2, 2, 2, 1, 2, 2, 1, 2, 2, 2}, 2));
  }
}