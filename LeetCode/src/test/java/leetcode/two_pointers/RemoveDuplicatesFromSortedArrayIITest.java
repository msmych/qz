package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class RemoveDuplicatesFromSortedArrayIITest {

    private RemoveDuplicatesFromSortedArray removeDuplicatesFromSortedArray =
            new RemoveDuplicatesFromSortedArray.SolutionII();

    @Test void nums111223length5() {
        int[] nums = new int[]{1, 1, 1, 2, 2, 3};
        assertEquals(5, removeDuplicatesFromSortedArray.removeDuplicates(nums));
        assertArrayEquals(new int[]{1, 1, 2, 2, 3}, Arrays.copyOf(nums, 5));
    }

    @Test void nums001111233length7() {
        int[] nums = new int[]{0, 0, 1, 1, 1, 1, 2, 3, 3};
        assertEquals(7, removeDuplicatesFromSortedArray.removeDuplicates(nums));
        assertArrayEquals(new int[]{0, 0, 1, 1, 2, 3, 3}, Arrays.copyOf(nums, 7));
    }
}