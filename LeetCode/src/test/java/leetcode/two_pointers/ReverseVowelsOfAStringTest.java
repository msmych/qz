package leetcode.two_pointers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReverseVowelsOfAStringTest {

    private ReverseVowelsOfAString reverseVowelsOfAString = new ReverseVowelsOfAString.Solution();

    @Test void hello() {
        assertEquals("holle", reverseVowelsOfAString.reverseVowels("hello"));
    }

    @Test void leetcode() {
        assertEquals("leotcede", reverseVowelsOfAString.reverseVowels("leetcode"));
    }

    @Test void a() {
        assertEquals("a.", reverseVowelsOfAString.reverseVowels("a."));
    }

    @Test void aA() {
        assertEquals("Aa", reverseVowelsOfAString.reverseVowels("aA"));
    }
}