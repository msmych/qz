package leetcode.two_pointers;

import leetcode.linked_list.ListNode;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PartitionListTest {

    private PartitionList partitionList = new PartitionList.Solution();

    @Test void head143252x3output122435() {
        assertTrue(ListNode.equalVals(
                partitionList.partition(ListNode.fromVals(1, 4, 3, 2, 5, 2), 3), 1, 2, 2, 4, 3, 5));
    }
}