package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PowerOfTwoTest {

    private PowerOfTwo powerOfTwo = new PowerOfTwo.Solution();

    @Test void true1() {
        assertTrue(powerOfTwo.isPowerOfTwo(1));
    }

    @Test void true16() {
        assertTrue(powerOfTwo.isPowerOfTwo(16));
    }

    @Test void false218() {
        assertFalse(powerOfTwo.isPowerOfTwo(218));
    }
}