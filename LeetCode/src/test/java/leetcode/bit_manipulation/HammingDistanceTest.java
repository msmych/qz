package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HammingDistanceTest {

    private HammingDistance hammingDistance = new HammingDistance.Solution();

    @Test void x1y4() {
        assertEquals(2, hammingDistance.hammingDistance(1, 4));
    }

    @Test void x1577962638y1727613287() {
        assertEquals(16, hammingDistance.hammingDistance(1577962638, 1727613287));
    }
}