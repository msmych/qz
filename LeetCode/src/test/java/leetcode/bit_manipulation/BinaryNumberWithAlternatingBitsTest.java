package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinaryNumberWithAlternatingBitsTest {

    private BinaryNumberWithAlternatingBits binaryNumberWithAlternatingBits =
            new BinaryNumberWithAlternatingBits.Solution();

    @Test void input5true() {
        assertTrue(binaryNumberWithAlternatingBits.hasAlternatingBits(5));
    }

    @Test void input7false() {
        assertFalse(binaryNumberWithAlternatingBits.hasAlternatingBits(7));
    }

    @Test void input11false() {
        assertFalse(binaryNumberWithAlternatingBits.hasAlternatingBits(11));
    }

    @Test void input10true() {
        assertTrue(binaryNumberWithAlternatingBits.hasAlternatingBits(10));
    }

    @Test void input1431655765true() {
        assertTrue(binaryNumberWithAlternatingBits.hasAlternatingBits(1431655765));
    }
}