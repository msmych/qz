package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfValidWordsForEachPuzzleTest {

    private NumberOfValidWordsForEachPuzzle numberOfValidWordsForEachPuzzle = new NumberOfValidWordsForEachPuzzle.Solution();

    @Test void aaaa() {
        assertArrayEquals(new Integer[]{1,1,3,2,4,0}, numberOfValidWordsForEachPuzzle.findNumOfValidWords(
            new String[]{"aaaa","asas","able","ability","actt","actor","access"},
            new String[]{"aboveyz","abrodyz","abslute","absoryz","actresz","gaswxyz"})
            .toArray(new Integer[]{}));
    }

    @Test void apple() {
        assertArrayEquals(new Integer[]{0,1,3,2,0}, numberOfValidWordsForEachPuzzle.findNumOfValidWords(
            new String[]{"apple","pleas","please"},
            new String[]{"aelwxyz","aelpxyz","aelpsxy","saelpxy","xaelpsy"})
            .toArray(new Integer[]{}));
    }
}