package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberComplementTest {

    private NumberComplement numberComplement = new NumberComplement.Solution();

    @Test void input5() {
        assertEquals(2, numberComplement.findComplement(5));
    }

    @Test void input1() {
        assertEquals(0, numberComplement.findComplement(1));
    }

    @Test
    void input2147483647() {
        assertEquals(0, numberComplement.findComplement(2147483647));
    }
}