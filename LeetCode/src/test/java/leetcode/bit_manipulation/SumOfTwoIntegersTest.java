package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumOfTwoIntegersTest {

  private SumOfTwoIntegers sumOfTwoIntegers = new SumOfTwoIntegers.Solution();

  @Test
  void a1b2() {
    assertEquals(3, sumOfTwoIntegers.getSum(1, 2));
  }

  @Test
  void aN2b3() {
    assertEquals(1, sumOfTwoIntegers.getSum(-2, 3));
  }
}
