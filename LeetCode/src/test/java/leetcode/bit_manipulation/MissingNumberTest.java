package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MissingNumberTest {

    private MissingNumber missingNumber = new MissingNumber.Solution();
    private MissingNumber gauss = new MissingNumber.GaussSolution();

    @Test void input301() {
        int[] nums = {3, 0, 1};
        assertEquals(2, missingNumber.missingNumber(nums));
        assertEquals(2, gauss.missingNumber(nums));
    }

    @Test void input964235701() {
        int[] nums = {9, 6, 4, 2, 3, 5, 7, 0, 1};
        assertEquals(8, missingNumber.missingNumber(nums));
        assertEquals(8, gauss.missingNumber(nums));
    }
}