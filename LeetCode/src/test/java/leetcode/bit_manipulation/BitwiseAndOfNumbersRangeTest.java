package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BitwiseAndOfNumbersRangeTest {

    private BitwiseAndOfNumbersRange bitwiseAndOfNumbersRange = new BitwiseAndOfNumbersRange.Solution();

    @Test void m5n7output4() {
        assertEquals(4, bitwiseAndOfNumbersRange.rangeBitwiseAnd(5, 7));
    }

    @Test void m0n1output0() {
        assertEquals(0, bitwiseAndOfNumbersRange.rangeBitwiseAnd(0, 1));
    }

    @Test void m600000000n2147483645output0() {
        assertEquals(0, bitwiseAndOfNumbersRange.rangeBitwiseAnd(600_000_000, 2147483645));
    }

    @Test void m2147483646n2147483647output2147483646() {
        assertEquals(2147483646, bitwiseAndOfNumbersRange.rangeBitwiseAnd(2147483646, 2147483647));
    }
}