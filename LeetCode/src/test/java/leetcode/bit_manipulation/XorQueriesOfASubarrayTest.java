package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class XorQueriesOfASubarrayTest {

  private XorQueriesOfASubarray xorQueriesOfASubarray = new XorQueriesOfASubarray.Solution();

  @Test
  void arr1348() {
    assertArrayEquals(new int[]{2, 7, 14, 8},
      xorQueriesOfASubarray.xorQueries(new int[]{1, 3, 4, 8}, new int[][]{{0, 1}, {1, 2}, {0, 3}, {3, 3}}));
  }

  @Test
  void arr48210() {
    assertArrayEquals(new int[]{8, 0, 4, 4},
      xorQueriesOfASubarray.xorQueries(new int[]{4, 8, 2, 10}, new int[][]{{2, 3}, {1, 3}, {0, 0}, {0, 3}}));
  }
}