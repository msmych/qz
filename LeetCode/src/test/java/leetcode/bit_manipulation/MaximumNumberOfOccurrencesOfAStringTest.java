package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaximumNumberOfOccurrencesOfAStringTest {

  private MaximumNumberOfOccurrencesOfAString maximumNumberOfOccurrencesOfAString = new MaximumNumberOfOccurrencesOfAString.Solution();

  @Test
  void aababcaab() {
    assertEquals(2, maximumNumberOfOccurrencesOfAString.maxFreq("aababcaab", 2, 3, 4));
  }

  @Test
  void aaaa() {
    assertEquals(2, maximumNumberOfOccurrencesOfAString.maxFreq("aaaa", 1, 3, 3));
  }

  @Test
  void aabcabcab() {
    assertEquals(3, maximumNumberOfOccurrencesOfAString.maxFreq("aabcabcab", 2, 2, 3));
  }

  @Test
  void abcde() {
    assertEquals(0, maximumNumberOfOccurrencesOfAString.maxFreq("abcde", 2, 3, 3));
  }
}