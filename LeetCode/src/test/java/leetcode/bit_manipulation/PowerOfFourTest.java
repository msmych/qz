package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PowerOfFourTest {

    private PowerOfFour powerOfFour = new PowerOfFour.Solution();

    @Test void true16() {
        assertTrue(powerOfFour.isPowerOfFour(16));
    }

    @Test void false5() {
        assertFalse(powerOfFour.isPowerOfFour(5));
    }
}