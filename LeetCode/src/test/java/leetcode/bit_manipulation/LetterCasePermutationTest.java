package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertCollectionContainsAll;

class LetterCasePermutationTest {

    private LetterCasePermutation letterCasePermutation = new LetterCasePermutation.Solution();

    @Test void a1b2() {
        assertCollectionContainsAll(letterCasePermutation.letterCasePermutation("a1b2"),
                "a1b2", "a1B2", "A1b2", "A1B2");
    }

    @Test void _3z4() {
        assertCollectionContainsAll(letterCasePermutation.letterCasePermutation("3z4"),
                "3z4", "3Z4");
    }

    @Test void _12345() {
        assertCollectionContainsAll(letterCasePermutation.letterCasePermutation("12345"),
                "12345");
    }

    @Test void __() {
        assertCollectionContainsAll(letterCasePermutation.letterCasePermutation(""), "");
    }
}