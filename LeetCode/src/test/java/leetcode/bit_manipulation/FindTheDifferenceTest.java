package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FindTheDifferenceTest {

  private FindTheDifference findTheDifference = new FindTheDifference.Solution();

  @Test void abcd() {
    assertEquals('e', findTheDifference.findTheDifference("abcd", "abcde"));
  }
}
