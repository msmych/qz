package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaximumScoreWordsFormedByLettersTest {

  private MaximumScoreWordsFormedByLetters maximumScoreWordsFormedByLetters = new MaximumScoreWordsFormedByLetters.Solution();

  @Test
  void dog_cat_dad_good() {
    assertEquals(23, maximumScoreWordsFormedByLetters.maxScoreWords(
      new String[]{"dog", "cat", "dad", "good"},
      new char[]{'a', 'a', 'c', 'd', 'd', 'd', 'g', 'o', 'o'},
      new int[]{1, 0, 9, 5, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
  }

  @Test
  void xxxz_ax_bx_cx() {
    assertEquals(27, maximumScoreWordsFormedByLetters.maxScoreWords(
      new String[]{"xxxz", "ax", "bx", "cx"},
      new char[]{'z', 'a', 'b', 'c', 'x', 'x', 'x'},
      new int[]{4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 10}));
  }

  @Test
  void leetcode() {
    assertEquals(0, maximumScoreWordsFormedByLetters.maxScoreWords(
      new String[]{"leetcode"},
      new char[]{'l', 'e', 't', 'c', 'o', 'd'},
      new int[]{0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}));
  }

  @Test
  void add_dda_bb_ba_add() {
    assertEquals(51, maximumScoreWordsFormedByLetters.maxScoreWords(
      new String[]{"add", "dda", "bb", "ba", "add"},
      new char[]{'a', 'a', 'a', 'a', 'b', 'b', 'b', 'b', 'c', 'c', 'c', 'c', 'c', 'd', 'd', 'd'},
      new int[]{3, 9, 8, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    ));
  }
}