package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class MaximumLengthOfAConcatenatedStringWithUniqueCharactersTest {

  private MaximumLengthOfAConcatenatedStringWithUniqueCharacters maximumLengthOfAConcatenatedStringWithUniqueCharacters =
    new MaximumLengthOfAConcatenatedStringWithUniqueCharacters.Solution();

  @Test
  void unique() {
    assertEquals(4, maximumLengthOfAConcatenatedStringWithUniqueCharacters.maxLength(Arrays.asList("un", "iq", "ue")));
  }

  @Test
  void characters() {
    assertEquals(6, maximumLengthOfAConcatenatedStringWithUniqueCharacters.maxLength(Arrays.asList("cha", "r", "act", "ers")));
  }

  @Test
  void abcdefghijklmnopqrstuvwxyz() {
    assertEquals(26, maximumLengthOfAConcatenatedStringWithUniqueCharacters.maxLength(Collections.singletonList("abcdefghijklmnopqrstuvwxyz")));
  }

  @Test
  void jnfbyktlrqumowxdmvhgcpxnjzrdei() {
    assertEquals(16, maximumLengthOfAConcatenatedStringWithUniqueCharacters.maxLength(Arrays.asList("jnfbyktlrqumowxd","mvhgcpxnjzrdei")));
  }

  @Test
  void abcdefghijklmnop() {
    assertEquals(16, maximumLengthOfAConcatenatedStringWithUniqueCharacters.maxLength(Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p")));
  }
}