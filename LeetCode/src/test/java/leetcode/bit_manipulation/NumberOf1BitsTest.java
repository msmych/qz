package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOf1BitsTest {

    private NumberOf1Bits numberOf1Bits = new NumberOf1Bits.Solution();

    @Test void input00000000000000000000000000001011output3() {
        assertEquals(3, numberOf1Bits.hammingWeight(11));
    }

    @Test void input00000000000000000000000010000000output1() {
        assertEquals(1, numberOf1Bits.hammingWeight(128));
    }

    @Test void input11111111111111111111111111111101output31() {
        assertEquals(31, numberOf1Bits.hammingWeight(-3));
    }
}