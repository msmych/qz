package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConvertANumberToHexadecimalTest {

    private ConvertANumberToHexadecimal convertANumberToHexadecimal = new ConvertANumberToHexadecimal.Solution();

    @Test void input26() {
        assertEquals("1a", convertANumberToHexadecimal.toHex(26));
    }

    @Test void inputN1() {
        assertEquals("ffffffff", convertANumberToHexadecimal.toHex(-1));
    }
}