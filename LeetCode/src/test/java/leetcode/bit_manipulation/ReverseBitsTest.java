package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReverseBitsTest {

    private ReverseBits reverseBits = new ReverseBits.Solution();

    @Test void input00000010100101000001111010011100output00111001011110000010100101000000() {
        assertEquals(964176192, reverseBits.reverseBits(43261596));
    }

    @Test void input11111111111111111111111111111101output10111111111111111111111111111111() {
        assertEquals(-1073741825, reverseBits.reverseBits(-3));
    }
}