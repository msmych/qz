package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RepeatedDnaSequencesTest {

    private RepeatedDnaSequences repeatedDnaSequences = new RepeatedDnaSequences.Solution();

    @Test void AAAAACCCCCAAAAACCCCCCAAAAAGGGTTToutputAAAAACCCCC_CCCCCAAAAA() {
        assertArrayEquals(new String[]{"AAAAACCCCC", "CCCCCAAAAA"},
                repeatedDnaSequences.findRepeatedDnaSequences("AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT").toArray(new String[]{}));
    }
}