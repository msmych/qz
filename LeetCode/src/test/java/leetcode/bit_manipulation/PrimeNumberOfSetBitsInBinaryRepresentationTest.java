package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PrimeNumberOfSetBitsInBinaryRepresentationTest {

    private PrimeNumberOfSetBitsInBinaryRepresentation primeNumberOfSetBitsInBinaryRepresentation =
            new PrimeNumberOfSetBitsInBinaryRepresentation.Solution();

    @Test
    void l6r10output4() {
        assertEquals(4, primeNumberOfSetBitsInBinaryRepresentation.countPrimeSetBits(6, 10));
    }

    @Test void l10r15output5() {
        assertEquals(5, primeNumberOfSetBitsInBinaryRepresentation.countPrimeSetBits(10, 15));
    }

    @Test void l842r888output23() {
        assertEquals(23, primeNumberOfSetBitsInBinaryRepresentation.countPrimeSetBits(842, 888));
    }
}