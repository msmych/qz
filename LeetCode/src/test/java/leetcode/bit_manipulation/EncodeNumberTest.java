package leetcode.bit_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EncodeNumberTest {

  private EncodeNumber encodeNumber = new EncodeNumber.Solution();

  @Test
  void num23() {
    assertEquals("1000", encodeNumber.encode(23));
  }

  @Test
  void num107() {
    assertEquals("101100", encodeNumber.encode(107));
  }
}