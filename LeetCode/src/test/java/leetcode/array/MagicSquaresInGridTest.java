package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MagicSquaresInGridTest {

    private MagicSquaresInGrid magicSquaresInGrid = new MagicSquaresInGrid.Solution();

    @Test void input4x4() {
        assertEquals(1, magicSquaresInGrid.numMagicSquaresInside(new int[][]{
                {4, 3, 8, 4},
                {9, 5, 1, 9},
                {2, 7, 6, 2}
        }));
    }

    @Test void input555() {
        assertEquals(0, magicSquaresInGrid.numMagicSquaresInside(new int[][]{
                {5, 5, 5},
                {5, 5, 5},
                {5, 5, 5}
        }));
    }

    @Test void input10_3_5() {
        assertEquals(0, magicSquaresInGrid.numMagicSquaresInside(new int[][]{
                {10, 3, 5},
                {1, 6, 11},
                {7, 9, 2}
        }));
    }

    @Test void input5x5() {
        assertEquals(1, magicSquaresInGrid.numMagicSquaresInside(new int[][]{
                {3,2,9,2,7},
                {6,1,8,4,2},
                {7,5,3,2,7},
                {2,9,4,9,6},
                {4,3,8,2,5}
        }));
    }
}