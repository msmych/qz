package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FindAllNumbersDisappearedInAnArrayTest {

    private FindAllNumbersDisappearedInAnArray findAllNumbersDisappearedInAnArray =
        new FindAllNumbersDisappearedInAnArray.Solution();

    @Test void input43278231() {
        assertArrayEquals(new Integer[]{5, 6}, findAllNumbersDisappearedInAnArray.findDisappearedNumbers(
            new int[]{4, 3, 2, 7, 8, 2, 3, 1}).toArray(new Integer[]{}));
    }
}