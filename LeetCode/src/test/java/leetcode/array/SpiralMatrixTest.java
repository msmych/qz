package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class SpiralMatrixTest {

  private SpiralMatrix spiralMatrix = new SpiralMatrix.Solution();

  @Test
  void input1to9output123698745() {
    assertArrayEquals(new Integer[]{1, 2, 3, 6, 9, 8, 7, 4, 5},
      spiralMatrix.spiralOrder(new int[][]{
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
      }).toArray(new Integer[]{}));
  }

  @Test
  void input1to12output1_2_3_4_8_12_11_10_9_5_6_7() {
    assertArrayEquals(new Integer[]{1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7},
      spiralMatrix.spiralOrder(new int[][]{
        {1, 2, 3, 4},
        {5, 6, 7, 8},
        {9, 10, 11, 12}
      }).toArray(new Integer[]{}));
  }
}