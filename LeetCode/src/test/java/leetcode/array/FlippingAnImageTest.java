package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FlippingAnImageTest {

    private FlippingAnImage flippingAnImage = new FlippingAnImage.Solution();

    @Test void matrix3x3() {
        assertArrayEquals(new int[][]{
                {1, 0, 0},
                {0, 1, 0},
                {1, 1, 1}
        }, flippingAnImage.flipAndInvertImage(new int[][]{
                {1, 1, 0},
                {1, 0, 1},
                {0, 0, 0}
        }));
    }

    @Test void matrix4x4() {
        assertArrayEquals(new int[][]{
                {1, 1, 0, 0},
                {0, 1, 1, 0},
                {0, 0, 0, 1},
                {1, 0, 1, 0}
        }, flippingAnImage.flipAndInvertImage(new int[][]{
                {1, 1, 0, 0},
                {1, 0, 0, 1},
                {0, 1, 1, 1},
                {1, 0, 1, 0}
        }));
    }
}