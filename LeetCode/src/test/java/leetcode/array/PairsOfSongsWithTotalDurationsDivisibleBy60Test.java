package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PairsOfSongsWithTotalDurationsDivisibleBy60Test {

    private PairsOfSongsWithTotalDurationsDivisibleBy60 pairsOfSongsWithTotalDurationsDivisibleBy60 =
            new PairsOfSongsWithTotalDurationsDivisibleBy60.Solution();

    @Test void input30_20_150_100_40output3() {
        assertEquals(3,
                pairsOfSongsWithTotalDurationsDivisibleBy60.numPairsDivisibleBy60(new int[]{30, 20, 150, 100, 40}));
    }

    @Test void input60_60_60output3() {
        assertEquals(3, pairsOfSongsWithTotalDurationsDivisibleBy60.numPairsDivisibleBy60(new int[]{60, 60, 60}));
    }

    @Test void input15_63_451_213_37_209_343_319output1() {
        assertEquals(1,
                pairsOfSongsWithTotalDurationsDivisibleBy60.numPairsDivisibleBy60(
                        new int[]{15,63,451,213,37,209,343,319}));
    }
}