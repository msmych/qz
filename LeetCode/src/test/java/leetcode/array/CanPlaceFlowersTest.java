package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CanPlaceFlowersTest {

    private CanPlaceFlowers canPlaceFlowers = new CanPlaceFlowers.Solution();

    @Test void flowerbed10001n1true() {
        assertTrue(canPlaceFlowers.canPlaceFlowers(new int[]{1,0,0,0,1}, 1));
    }

    @Test void flowerbed10001n2false() {
        assertFalse(canPlaceFlowers.canPlaceFlowers(new int[]{1,0,0,0,1}, 2));
    }

    @Test void flowerbed1000100n2true() {
        assertTrue(canPlaceFlowers.canPlaceFlowers(new int[]{1,0,0,0,1,0,0}, 2));
    }
}