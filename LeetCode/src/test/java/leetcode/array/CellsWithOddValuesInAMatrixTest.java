package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CellsWithOddValuesInAMatrixTest {

  private CellsWithOddValuesInAMatrix cellsWithOddValuesInAMatrix = new CellsWithOddValuesInAMatrix.Solution();

  @Test
  void ind0111() {
    assertEquals(6, cellsWithOddValuesInAMatrix.oddCells(2, 3, new int[][]{{0, 1}, {1, 1}}));
  }

  @Test
  void ind1100() {
    assertEquals(0, cellsWithOddValuesInAMatrix.oddCells(2, 2, new int[][]{{1, 1}, {0, 0}}));
  }
}