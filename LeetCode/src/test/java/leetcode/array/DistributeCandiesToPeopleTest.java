package leetcode.array;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class DistributeCandiesToPeopleTest {

    private DistributeCandiesToPeople distributeCandiesToPeople = new DistributeCandiesToPeople.Solution();

    @Test void candies7people4() {
        assertArrayEquals(new int[]{1, 2, 3, 1}, distributeCandiesToPeople.distributeCandies(7, 4));
    }

    @Test void candies10people3() {
        assertArrayEquals(new int[]{5, 2, 3}, distributeCandiesToPeople.distributeCandies(10, 3));
    }

    @Test void candies80people4() {
        System.out.println(Arrays.toString(distributeCandiesToPeople.distributeCandies(80, 4)));
    }
}