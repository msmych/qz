package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeightCheckerTest {

    private HeightChecker heightChecker = new HeightChecker.Solution();

    @Test void input114213output3() {
        assertEquals(3, heightChecker.heightChecker(new int[]{1, 1, 4, 2, 1, 3}));
    }

    @Test void input21211221output4() {
        assertEquals(4, heightChecker.heightChecker(new int[]{2, 1, 2, 1, 1, 2, 2, 1}));
    }

    @Test void input121211121output4() {
        assertEquals(4, heightChecker.heightChecker(new int[]{1, 2, 1, 2, 1, 1, 1, 2, 1}));
    }
}