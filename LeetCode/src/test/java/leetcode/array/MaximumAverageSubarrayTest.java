package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MaximumAverageSubarrayTest {

    private MaximumAverageSubarray maximumAverageSubarray = new MaximumAverageSubarray.Solution();

    @Test void average12$75() {
        assertEquals(12.75, maximumAverageSubarray.findMaxAverage(new int[]{1,12,-5,-6,50,3}, 4));
    }
}