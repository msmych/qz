package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductOfArrayExceptSelfTest {

  private ProductOfArrayExceptSelf productOfArrayExceptSelf = new ProductOfArrayExceptSelf.Solution();

  @Test
  void input1234() {
    assertArrayEquals(new int[]{24, 12, 8, 6}, productOfArrayExceptSelf.productExceptSelf(new int[]{1, 2, 3, 4}));
  }
}
