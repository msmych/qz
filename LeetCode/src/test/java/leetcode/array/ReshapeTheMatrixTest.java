package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReshapeTheMatrixTest {

    private ReshapeTheMatrix reshapeTheMatrix = new ReshapeTheMatrix.Solution();

    @Test void r1c4() {
        assertArrayEquals(new int[][]{{1,2,3,4}},
                reshapeTheMatrix.matrixReshape(new int[][]{{1,2},{3,4}}, 1, 4));
    }

    @Test void r2c4() {
        assertArrayEquals(new int[][]{{1,2},{3,4}},
                reshapeTheMatrix.matrixReshape(new int[][]{{1,2},{3,4}}, 2, 4));
    }

    @Test void r2c3() {
        assertArrayEquals(new int[][]{{1,2,3},{4,5,6}},
                reshapeTheMatrix.matrixReshape(new int[][]{{1,2},{3,4},{5,6}}, 2, 3));
    }
}