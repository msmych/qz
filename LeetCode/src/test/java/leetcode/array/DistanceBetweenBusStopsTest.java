package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DistanceBetweenBusStopsTest {

  private DistanceBetweenBusStops distanceBetweenBusStops = new DistanceBetweenBusStops.Solution();

  @Test
  void distance1234() {
    assertEquals(1, distanceBetweenBusStops.distanceBetweenBusStops(new int[]{1, 2, 3, 4}, 0, 1));
  }

  @Test void distance1234destination2() {
    assertEquals(3, distanceBetweenBusStops.distanceBetweenBusStops(new int[]{1, 2, 3, 4}, 0, 2));
  }

  @Test void distance1234destination3() {
    assertEquals(4, distanceBetweenBusStops.distanceBetweenBusStops(new int[]{1, 2, 3, 4}, 0, 3));
  }

  @Test void distance1234312() {
    assertEquals(6, distanceBetweenBusStops.distanceBetweenBusStops(new int[]{1,2,3,4,3,1,2}, 2, 5));
  }
}
