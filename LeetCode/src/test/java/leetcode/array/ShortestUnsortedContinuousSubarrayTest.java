package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShortestUnsortedContinuousSubarrayTest {

    private ShortestUnsortedContinuousSubarray shortestUnsortedContinuousSubarray =
        new ShortestUnsortedContinuousSubarray.Solution();

    @Test void input264810915() {
        assertEquals(5, shortestUnsortedContinuousSubarray.findUnsortedSubarray(new int[]{2, 6, 4, 8, 10, 9, 15}));
    }

    @Test void input13222() {
        assertEquals(4, shortestUnsortedContinuousSubarray.findUnsortedSubarray(new int[]{1, 3, 2, 2, 2}));
    }

    @Test void input55527() {
        assertEquals(4, shortestUnsortedContinuousSubarray.findUnsortedSubarray(new int[]{5, 5, 5, 2, 7}));
    }

    @Test void input5552() {
        assertEquals(4, shortestUnsortedContinuousSubarray.findUnsortedSubarray(new int[]{5, 5, 5, 2}));
    }

    @Test void input13245() {
        assertEquals(2, shortestUnsortedContinuousSubarray.findUnsortedSubarray(new int[]{1, 3, 2, 4, 5}));
    }

    @Test void input52222() {
        assertEquals(5, shortestUnsortedContinuousSubarray.findUnsortedSubarray(new int[]{5, 2, 2, 2, 2}));
    }
}