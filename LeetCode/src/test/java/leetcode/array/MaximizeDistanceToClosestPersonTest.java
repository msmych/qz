package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaximizeDistanceToClosestPersonTest {

    private MaximizeDistanceToClosestPerson maximizeDistanceToClosestPerson = new MaximizeDistanceToClosestPerson.Solution();

    @Test void input1000101output2() {
        assertEquals(2, maximizeDistanceToClosestPerson.maxDistToClosest(new int[]{1, 0, 0, 0, 1, 0, 1}));
    }

    @Test void input1000output3() {
        assertEquals(3, maximizeDistanceToClosestPerson.maxDistToClosest(new int[]{1, 0, 0, 0}));
    }
}