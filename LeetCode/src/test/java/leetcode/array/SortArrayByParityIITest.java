package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortArrayByParityIITest {

    private SortArrayByParityII sortArrayByParityII = new SortArrayByParityII.Solution();

    @Test void input4257output4527() {
        assertArrayEquals(new int[]{4, 5, 2, 7}, sortArrayByParityII.sortArrayByParityII(new int[]{4, 2, 5, 7}));
    }
}