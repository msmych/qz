package leetcode.array;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionEquivalentToArrays;

class MinimumAbsoluteDifferenceTest {

  private MinimumAbsoluteDifference minimumAbsoluteDifference = new MinimumAbsoluteDifference.Solution();

  @Test void arr4213() {
    assertListCollectionEquivalentToArrays(new Integer[][]{{1, 2}, {2, 3}, {3, 4}},
      minimumAbsoluteDifference.minimumAbsDifference(new int[]{1, 2, 3, 4}));
  }

  @Test void arr136() {
    assertListCollectionEquivalentToArrays(new Integer[][]{{1, 3}},
      minimumAbsoluteDifference.minimumAbsDifference(new int[]{1, 3, 6, 10, 15}));
  }

  @Test void arr38() {
    assertListCollectionEquivalentToArrays(new Integer[][]{{-14, -10}, {19, 23}, {23, 27}},
      minimumAbsoluteDifference.minimumAbsDifference(new int[]{3, 8, -10, 23, 19, -4, -14, 27}));
  }

  @Test void arr40() {
    assertListCollectionEquivalentToArrays(new Integer[][]{{26, 27}},
      minimumAbsoluteDifference.minimumAbsDifference(new int[]{40, 11, 26, 27, -20}));
  }
}
