package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PartitionArrayIntoThreePartsWithEqualSumTest {

    private PartitionArrayIntoThreePartsWithEqualSum partitionArrayIntoThreePartsWithEqualSum =
            new PartitionArrayIntoThreePartsWithEqualSum.Solution();

    @Test void input021N66N791201true() {
        assertTrue(partitionArrayIntoThreePartsWithEqualSum.canThreePartsEqualSum(
                new int[]{0, 2, 1, -6, 6, -7, 9, 1, 2, 0, 1}));
    }

    @Test void input021N6679N1201false() {
        assertFalse(partitionArrayIntoThreePartsWithEqualSum.canThreePartsEqualSum(
                new int[]{0, 2, 1, -6, 6, 7, 9, -1, 2, 0, 1}));
    }

    @Test void input3365N2251N94true() {
        assertTrue(partitionArrayIntoThreePartsWithEqualSum.canThreePartsEqualSum(
                new int[]{3, 3, 6, 5, -2, 2, 5, 1, -9, 4}));
    }
}