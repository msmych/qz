package leetcode.array;

import leetcode.sort.CarPooling;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CarPoolingTest {

    private CarPooling carPooling = new CarPooling.Solution();

    @Test void trips215_337capacity4false() {
        assertFalse(carPooling.carPooling(new int[][]{{2, 1, 5}, {3, 3, 7}}, 4));
    }

    @Test void trips215_337capacity5true() {
        assertTrue(carPooling.carPooling(new int[][]{{2, 1, 5}, {3, 3, 7}}, 5));
    }

    @Test void trips215_357capacity3true() {
        assertTrue(carPooling.carPooling(new int[][]{{2, 1, 5}, {3, 5, 7}}, 3));
    }

    @Test void trips327_379_839capacity11true() {
        assertTrue(carPooling.carPooling(new int[][]{{3, 2, 7}, {3, 7, 9}, {8, 3, 9}}, 11));
    }

    @Test void trips226_247_867capacity11true() {
        assertTrue(carPooling.carPooling(new int[][]{{2, 2, 6}, {2, 4, 7}, {8, 6, 7}}, 11));
    }
}