package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MonotonicArrayTest {

    private MonotonicArray monotonicArray = new MonotonicArray.Solution();

    @Test void input1223true() {
        assertTrue(monotonicArray.isMonotonic(new int[]{1, 2, 2, 3}));
    }

    @Test void input6544true() {
        assertTrue(monotonicArray.isMonotonic(new int[]{6, 5, 4, 4}));
    }

    @Test void input132false() {
        assertFalse(monotonicArray.isMonotonic(new int[]{1, 3, 2}));
    }

    @Test void input1245true() {
        assertTrue(monotonicArray.isMonotonic(new int[]{1, 2, 4, 5}));
    }

    @Test void input111true() {
        assertTrue(monotonicArray.isMonotonic(new int[]{1, 1, 1}));
    }
}