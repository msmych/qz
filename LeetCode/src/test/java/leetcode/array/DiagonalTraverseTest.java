package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class DiagonalTraverseTest {

    private DiagonalTraverse diagonalTraverse = new DiagonalTraverse.Solution();

    @Test void input1to9output124753689() {
        assertArrayEquals(new int[]{1, 2, 4, 7, 5, 3, 6, 8, 9},
                diagonalTraverse.findDiagonalOrder(new int[][]{
                        {1, 2, 3},
                        {4, 5, 6},
                        {7, 8, 9}
                }));
    }

    @Test void input1to16output1_2_5_9_6_3_4_7_10_13_14_11_8_12_15_16() {
        assertArrayEquals(new int[]{1, 2, 5, 9, 6, 3, 4, 7, 10, 13 ,14 ,11, 8, 12, 15, 16},
                diagonalTraverse.findDiagonalOrder(new int[][]{
                        {1, 2, 3, 4},
                        {5, 6, 7, 8},
                        {9, 10, 11, 12},
                        {13, 14, 15, 16}
                }));
    }
}