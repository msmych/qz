package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FindPivotIndexTest {

    private FindPivotIndex findPivotIndex = new FindPivotIndex.Solution();

    @Test
    void nums173656output3() {
        assertEquals(3, findPivotIndex.pivotIndex(new int[]{1, 7, 3, 6, 5, 6}));
    }

    @Test void nums123outputN1() {
        assertEquals(-1, findPivotIndex.pivotIndex(new int[]{1, 2, 3}));
    }

    @Test void numsN1N1N1011output0() {
        assertEquals(0, findPivotIndex.pivotIndex(new int[]{-1, -1, -1, 0, 1, 1}));
    }
}