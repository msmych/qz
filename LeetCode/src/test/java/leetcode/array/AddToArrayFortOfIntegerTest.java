package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddToArrayFortOfIntegerTest {

    private AddToArrayFortOfInteger addToArrayFortOfInteger = new AddToArrayFortOfInteger.Solution();

    @Test void A1200K34output1234() {
        assertArrayEquals(new Integer[]{1, 2, 3, 4},
                addToArrayFortOfInteger.addToArrayForm(new int[]{1, 2, 0, 0}, 34).toArray(new Integer[]{}));
    }

    @Test void A274K181output455() {
        assertArrayEquals(new Integer[]{4, 5, 5},
                addToArrayFortOfInteger.addToArrayForm(new int[]{2, 7, 4}, 181).toArray(new Integer[]{}));
    }

    @Test void A215K806output1021() {
        assertArrayEquals(new Integer[]{1, 0, 2, 1},
                addToArrayFortOfInteger.addToArrayForm(new int[]{2, 1, 5}, 806).toArray(new Integer[]{}));
    }

    @Test void A9999999999K1output10000000000() {
        assertArrayEquals(new Integer[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                addToArrayFortOfInteger.addToArrayForm(new int[]{9, 9, 9, 9, 9, 9, 9, 9, 9, 9}, 1).toArray(new Integer[]{}));
    }
}