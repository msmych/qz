package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestContinuousIncreasingSubsequenceTest {

    private LongestContinuousIncreasingSubsequence longestContinuousIncreasingSubsequence =
            new LongestContinuousIncreasingSubsequence.Solution();

    @Test void input13547output3() {
        assertEquals(3, longestContinuousIncreasingSubsequence.findLengthOfLCIS(new int[]{1, 3, 5, 4, 7}));
    }

    @Test void input22222output1() {
        assertEquals(1, longestContinuousIncreasingSubsequence.findLengthOfLCIS(new int[]{2, 2, 2, 2, 2}));
    }
}