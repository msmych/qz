package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LargestNumberAtLeastTwiceOfOthersTest {

    private LargestNumberAtLeastTwiceOfOthers largestNumberAtLeastTwiceOfOthers =
            new LargestNumberAtLeastTwiceOfOthers.Solution();

    @Test
    void nums3610output1() {
        assertEquals(1, largestNumberAtLeastTwiceOfOthers.dominantIndex(new int[]{3, 6, 1, 0}));
    }

    @Test void nums1234outputN1() {
        assertEquals(-1, largestNumberAtLeastTwiceOfOthers.dominantIndex(new int[]{1, 2, 3, 4}));
    }

    @Test void nums0001output3() {
        assertEquals(3, largestNumberAtLeastTwiceOfOthers.dominantIndex(new int[]{0, 0, 0, 1}));
    }

    @Test void nums0203outputN1() {
        assertEquals(-1, largestNumberAtLeastTwiceOfOthers.dominantIndex(new int[]{0, 2, 0, 3}));
    }
}