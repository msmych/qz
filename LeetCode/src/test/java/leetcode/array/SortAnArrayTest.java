package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortAnArrayTest {

    private SortAnArray sortAnArray = new SortAnArray.Solution();

    @Test void arr5231() {
        assertArrayEquals(new int[]{1, 2, 3, 5}, sortAnArray.sortArray(new int[]{5, 2, 3, 1}));
    }

    @Test void arr511200() {
        assertArrayEquals(new int[]{0, 0, 1, 1, 2, 5}, sortAnArray.sortArray(new int[]{5, 1, 1, 2, 0, 0}));
    }
}