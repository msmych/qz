package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShortestDistanceToACharacterTest {

    private ShortestDistanceToACharacter shortestDistanceToACharacter = new ShortestDistanceToACharacter.Solution();

    @Test void loveleetcode_e_321010012210() {
        assertArrayEquals(new int[]{3, 2, 1, 0, 1, 0, 0, 1, 2, 2, 1, 0},
                shortestDistanceToACharacter.shortestToChar("loveleetcode", 'e'));
    }

    @Test void aaba_b_2101() {
        assertArrayEquals(new int[]{2, 1, 0, 1}, shortestDistanceToACharacter.shortestToChar("aaba", 'b'));
    }

    @Test void abaa_b_1012() {
        assertArrayEquals(new int[]{1, 0, 1, 2}, shortestDistanceToACharacter.shortestToChar("abaa", 'b'));
    }
}