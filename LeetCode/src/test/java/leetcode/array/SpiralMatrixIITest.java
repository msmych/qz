package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SpiralMatrixIITest {

    private SpiralMatrixII spiralMatrixII = new SpiralMatrixII.Solution();

    @Test void n3() {
        assertArrayEquals(new int[][]{
                {1, 2, 3},
                {8, 9, 4},
                {7, 6, 5}
        }, spiralMatrixII.generateMatrix(3));
    }
}