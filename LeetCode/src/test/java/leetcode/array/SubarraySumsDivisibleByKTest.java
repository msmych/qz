package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubarraySumsDivisibleByKTest {

    private SubarraySumsDivisibleByK subarraySumsDivisibleByK = new SubarraySumsDivisibleByK.Solution();

    @Test void a450N2N31k5output7() {
        assertEquals(7, subarraySumsDivisibleByK.subarraysDivByK(new int[]{4, 5, 0, -2, -3, 1}, 5));
    }

    @Test void aN5k5output1() {
        assertEquals(1, subarraySumsDivisibleByK.subarraysDivByK(new int[]{-5}, 5));
    }
}