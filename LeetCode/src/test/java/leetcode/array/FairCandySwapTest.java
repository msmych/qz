package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FairCandySwapTest {

    private FairCandySwap fairCandySwap = new FairCandySwap.Solution();

    @Test void A11B22output12() {
        assertArrayEquals(new int[]{1, 2}, fairCandySwap.fairCandySwap(new int[]{1, 1}, new int[]{2, 2}));
    }

    @Test void A12B23output12() {
        assertArrayEquals(new int[]{1, 2}, fairCandySwap.fairCandySwap(new int[]{1, 2}, new int[]{2, 3}));
    }

    @Test void A2B13output23() {
        assertArrayEquals(new int[]{2, 3}, fairCandySwap.fairCandySwap(new int[]{2}, new int[]{1, 3}));
    }

    @Test void A125B24output54() {
        assertArrayEquals(new int[]{5, 4}, fairCandySwap.fairCandySwap(new int[]{1, 2, 5}, new int[]{2, 4}));
    }
}