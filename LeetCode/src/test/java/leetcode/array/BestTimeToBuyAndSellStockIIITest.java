package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BestTimeToBuyAndSellStockIIITest {

    private BestTimeToBuyAndSellStock bestTimeToBuyAndSellStock = new BestTimeToBuyAndSellStock.SolutionIII();

    @Test void input33500314output6() {
        assertEquals(6, bestTimeToBuyAndSellStock.maxProfit(new int[]{3, 3, 5, 0, 0, 3, 1, 4}));
    }

    @Test void input12345output4() {
        assertEquals(4, bestTimeToBuyAndSellStock.maxProfit(new int[]{1, 2, 3, 4, 5}));
    }

    @Test void input76431output0() {
        assertEquals(0, bestTimeToBuyAndSellStock.maxProfit(new int[]{7, 6, 4, 3, 1}));
    }

    @Test void input1242572490output13() {
        assertEquals(13, bestTimeToBuyAndSellStock.maxProfit(new int[]{1, 2, 4, 2, 5, 7, 2, 4, 9, 0}));
    }

    @Test void input21201output2() {
        assertEquals(2, bestTimeToBuyAndSellStock.maxProfit(new int[]{2, 1, 2, 0, 1}));
    }
}