package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayTransformationTest {

  private ArrayTransformation arrayTransformation = new ArrayTransformation.Solution();

  @Test
  void arr6234() {
    assertArrayEquals(new Integer[]{6, 3, 3, 4}, arrayTransformation.transformArray(new int[]{6, 2, 3, 4}).toArray(new Integer[]{}));
  }

  @Test
  void arr163435() {
    assertArrayEquals(new Integer[]{1, 4, 4, 4, 4, 5}, arrayTransformation.transformArray(new int[]{1, 6, 3, 4, 3, 5}).toArray(new Integer[]{}));
  }

  @Test
  void arr154792512588384463() {
    assertArrayEquals(new Integer[]{1, 4, 5, 7, 7, 4, 3, 2, 2, 5, 8, 8, 6, 5, 4, 4, 4, 3}, arrayTransformation.transformArray(new int[]{1, 5, 4, 7, 9, 2, 5, 1, 2, 5, 8, 8, 3, 8, 4, 4, 6, 3}).toArray(new Integer[]{}));
  }
}
