package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DegreeOfAnArrayTest {

    private DegreeOfAnArray degreeOfAnArray = new DegreeOfAnArray.Solution();

    @Test void input12231output2() {
        assertEquals(2, degreeOfAnArray.findShortestSubArray(new int[]{1, 2, 2, 3, 1}));
    }

    @Test void input1223142output6() {
        assertEquals(6, degreeOfAnArray.findShortestSubArray(new int[]{1, 2, 2, 3, 1, 4, 2}));
    }
}