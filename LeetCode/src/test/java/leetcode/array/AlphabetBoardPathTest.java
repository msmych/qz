package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AlphabetBoardPathTest {

    private AlphabetBoardPath alphabetBoardPath = new AlphabetBoardPath.Solution();

    @Test void leet() {
        assertEquals("RDD!UURRR!!DDD!", alphabetBoardPath.alphabetBoardPath("leet"));
    }

    @Test void code() {
        assertEquals("RR!RRDD!UUL!R!", alphabetBoardPath.alphabetBoardPath("code"));
    }

    @Test void izi() {
        assertEquals("RRRD!LLLDDDD!UUUURRR!", alphabetBoardPath.alphabetBoardPath("izi"));
    }
}