package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class PascalsTriangleIITest {

    private PascalsTriangleII pascalsTriangleII = new PascalsTriangleII.Solution(),
            recursive = new PascalsTriangleII.RecursiveSolution();

    @Test
    void input3output1331() {
        Integer[] row = new Integer[]{1, 3, 3, 1};
        assertArrayEquals(row, pascalsTriangleII.getRow(3).toArray(new Integer[]{}));
        assertArrayEquals(row, recursive.getRow(3).toArray(new Integer[]{}));
    }
}