package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ToeplitzMatrixTest {

    private ToeplitzMatrix toeplitzMatrix = new ToeplitzMatrix.Solution();

    @Test void matrix1234() {
        assertTrue(toeplitzMatrix.isToeplitzMatrix(new int[][]{
                {1, 2, 3, 4},
                {5, 1, 2, 3},
                {9, 5, 1, 2}
        }));
    }

    @Test void matrix12() {
        assertFalse(toeplitzMatrix.isToeplitzMatrix(new int[][]{{1, 2}, {2, 2}}));
    }
}