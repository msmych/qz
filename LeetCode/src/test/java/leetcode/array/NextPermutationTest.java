package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NextPermutationTest {

    private NextPermutation nextPermutation = new NextPermutation.Solution();

    @Test void input123output132() {
        int[] nums = new int[]{1, 2, 3};
        nextPermutation.nextPermutation(nums);
        assertArrayEquals(new int[]{1, 3, 2}, nums);
    }

    @Test void input321output123() {
        int[] nums = new int[]{3, 2, 1};
        nextPermutation.nextPermutation(nums);
        assertArrayEquals(new int[]{1, 2, 3}, nums);
    }

    @Test void input115output151() {
        int[] nums = new int[]{1, 1, 5};
        nextPermutation.nextPermutation(nums);
        assertArrayEquals(new int[]{1, 5, 1}, nums);
    }

    @Test void input132output213() {
        int[] nums = new int[]{1, 3, 2};
        nextPermutation.nextPermutation(nums);
        assertArrayEquals(new int[]{2, 1, 3}, nums);
    }

    @Test void input4202320output4203022() {
        int[] nums = new int[]{4, 2, 0, 2, 3, 2, 0};
        nextPermutation.nextPermutation(nums);
        assertArrayEquals(new int[]{4, 2, 0, 3, 0, 2, 2}, nums);
    }
}