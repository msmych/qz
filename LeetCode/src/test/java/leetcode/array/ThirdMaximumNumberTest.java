package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ThirdMaximumNumberTest {

  private ThirdMaximumNumber thirdMaximumNumber = new ThirdMaximumNumber.Solution();

  @Test void input321() {
    assertEquals(1, thirdMaximumNumber.thirdMax(new int[]{3, 2, 1}));
  }

  @Test void input12() {
    assertEquals(2, thirdMaximumNumber.thirdMax(new int[]{1, 2}));
  }

  @Test void input2231() {
    assertEquals(1, thirdMaximumNumber.thirdMax(new int[]{2, 2, 3, 1}));
  }

  @Test void input112() {
    assertEquals(2, thirdMaximumNumber.thirdMax(new int[]{1, 1, 2}));
  }

  @Test
  void input122535() {
    assertEquals(2, thirdMaximumNumber.thirdMax(new int[]{1, 2, 2, 5, 3, 5}));
  }

  @Test
  void input334343033() {
    assertEquals(0, thirdMaximumNumber.thirdMax(new int[]{3, 3, 4, 3, 4, 3, 0, 3, 3}));
  }
}
