package leetcode.array;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertCollectionContainsAll;

class FindCommonCharactersTest {

    private FindCommonCharacters findCommonCharacters = new FindCommonCharacters.Solution();

    @Test void bella$label$roller_e$l$l() {
        assertCollectionContainsAll(
                findCommonCharacters.commonChars(new String[]{"bella", "label", "roller"}), "e", "l", "l");
    }

    @Test void cool$lock$cook_c$o() {
        assertCollectionContainsAll(findCommonCharacters.commonChars(new String[]{"cool", "lock", "cook"}), "c", "o");
    }
}