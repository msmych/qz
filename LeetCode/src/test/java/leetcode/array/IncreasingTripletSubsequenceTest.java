package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IncreasingTripletSubsequenceTest {

    private IncreasingTripletSubsequence increasingTripletSubsequence = new IncreasingTripletSubsequence.Solution();

    @Test
    void nums12345() {
        assertTrue(increasingTripletSubsequence.increasingTriplet(new int[]{1, 2, 3, 4, 5}));
    }

    @Test
    void nums54321() {
        assertFalse(increasingTripletSubsequence.increasingTriplet(new int[]{5, 4, 3, 2, 1}));
    }

    @Test
    void nums32415() {
        assertTrue(increasingTripletSubsequence.increasingTriplet(new int[]{3, 2, 4, 1, 5}));
    }
}