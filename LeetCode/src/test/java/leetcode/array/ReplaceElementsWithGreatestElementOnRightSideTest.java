package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReplaceElementsWithGreatestElementOnRightSideTest {

  private ReplaceElementsWithGreatestElementOnRightSide replaceElementsWithGreatestElementOnRightSide =
    new ReplaceElementsWithGreatestElementOnRightSide.Solution();

  @Test
  void arr17185461() {
    assertArrayEquals(new int[]{18, 6, 6, 6, 1, -1},
      replaceElementsWithGreatestElementOnRightSide.replaceElements(new int[]{17, 18, 5, 4, 6, 1}));
  }
}