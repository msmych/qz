package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LargestValuesFromLabelsTest {

    private LargestValuesFromLabels largestValuesFromLabels = new LargestValuesFromLabels.Solution();

    @Test void values54321labels11223wanted3limit1output9() {
        assertEquals(9,
                largestValuesFromLabels.largestValsFromLabels(
                        new int[]{5, 4, 3, 2, 1},
                        new int[]{1, 1, 2, 2, 3},
                        3,
                        1));
    }

    @Test void values54321labels13332wanted3limit2output12() {
        assertEquals(12,
                largestValuesFromLabels.largestValsFromLabels(
                        new int[]{5, 4, 3, 2, 1},
                        new int[]{1, 3, 3, 3, 2},
                        3,
                        2));
    }

    @Test void values98876labels00011wanted3limit1output16() {
        assertEquals(16,
                largestValuesFromLabels.largestValsFromLabels(
                        new int[]{9, 8, 8, 7, 6},
                        new int[]{0, 0, 0, 1, 1},
                        3,
                        1));
    }

    @Test void values98876labels00011wanted3limit2output24() {
        assertEquals(24,
                largestValuesFromLabels.largestValsFromLabels(
                        new int[]{9, 8, 8, 7, 6},
                        new int[]{0, 0, 0, 1, 1},
                        3,
                        2));
    }
}