package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TransposeMatrixTest {

    private TransposeMatrix transposeMatrix = new TransposeMatrix.Solution();

    @Test void matrix123456789() {
        assertArrayEquals(new int[][]{
                {1, 4, 7},
                {2, 5, 8},
                {3, 6, 9}
        }, transposeMatrix.transpose(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}}));
    }

    @Test void matrix123456() {
        assertArrayEquals(new int[][]{
                {1, 4},
                {2, 5},
                {3, 6}
        }, transposeMatrix.transpose(new int[][]{
                {1, 2, 3},
                {4, 5, 6}}));
    }
}