package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ImageSmootherTest {

    private ImageSmoother imageSmoother = new ImageSmoother.Solution();

    @Test void image111() {
        assertArrayEquals(new int[][]{
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0}
        }, imageSmoother.imageSmoother(new int[][]{
                {1, 1, 1},
                {1, 0, 1},
                {1, 1, 1}
        }));
    }
}