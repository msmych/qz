package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class RotateArrayTest {

    private RotateArray rotateArray = new RotateArray.Solution(),
            singlePass = new RotateArray.SinglePassSolution(),
            reverse = new RotateArray.ReverseSolution();

    @Test void input1to7k3output5to7_1to3() {
        int[] nums = new int[]{1, 2, 3, 4, 5, 6, 7},
                rotated = new int[]{5, 6, 7, 1, 2, 3, 4};
        rotateArray.rotate(nums,3);
        assertArrayEquals(rotated, nums);
        nums = new int[]{1, 2, 3, 4, 5, 6, 7};
        singlePass.rotate(nums, 3);
        assertArrayEquals(rotated, nums);
        nums = new int[]{1, 2, 3, 4, 5, 6, 7};
        reverse.rotate(nums, 3);
        assertArrayEquals(rotated, nums);
    }

    @Test void inputN1N100_3_99k2output3_99_N1N100() {
        int[] nums = new int[]{-1, -100, 3, 99},
                rotated = new int[]{3, 99, -1, -100};
        rotateArray.rotate(nums, 2);
        assertArrayEquals(rotated, nums);
        nums = new int[]{-1, -100, 3, 99};
        singlePass.rotate(nums, 2);
        assertArrayEquals(rotated, nums);
        nums = new int[]{-1, -100, 3, 99};
        reverse.rotate(nums, 2);
        assertArrayEquals(rotated, nums);
    }

    @Test void intputN1k2outputN1() {
        int[] nums = new int[]{-1},
                rotated = new int[]{-1};
        rotateArray.rotate(nums, 2);
        assertArrayEquals(rotated, nums);
        nums = new int[]{-1};
        singlePass.rotate(nums, 2);
        assertArrayEquals(rotated, nums);
        nums = new int[]{-1};
        reverse.rotate(nums, 2);
        assertArrayEquals(rotated, nums);
    }

    @Test void input123456k4output234561() {
        int[] nums = new int[]{1, 2, 3, 4, 5, 6},
                rotated = new int[]{3, 4, 5, 6, 1, 2};
        rotateArray.rotate(nums, 4);
        assertArrayEquals(rotated, nums);
        nums = new int[]{1, 2, 3, 4, 5, 6};
        singlePass.rotate(nums, 4);
        assertArrayEquals(rotated, nums);
        nums = new int[]{1, 2, 3, 4, 5, 6};
        reverse.rotate(nums, 4);
        assertArrayEquals(rotated, nums);
    }

    @Test void input1k0output1() {
        int[] nums = new int[]{1},
                rotated = new int[]{1};
        rotateArray.rotate(nums, 0);
        assertArrayEquals(rotated, nums);
        nums = new int[]{1};
        singlePass.rotate(nums, 0);
        assertArrayEquals(rotated, nums);
        nums = new int[]{1};
        reverse.rotate(nums, 0);
        assertArrayEquals(rotated, nums);
    }

    @Test void input12k3output21() {
        int[] nums = new int[]{1, 2},
                rotated = new int[]{2, 1};
        rotateArray.rotate(nums, 3);
        assertArrayEquals(rotated, nums);
        nums = new int[]{1, 2};
        singlePass.rotate(nums, 3);
        assertArrayEquals(rotated, nums);
        nums = new int[]{1, 2};
        reverse.rotate(nums, 3);
        assertArrayEquals(rotated, nums);
    }

    @Test void input1to27k38() {
        int[] nums = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27},
                rotated = new int[]{17,18,19,20,21,22,23,24,25,26,27,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
        rotateArray.rotate(nums, 38);
        assertArrayEquals(rotated, nums);
        nums = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27};
        singlePass.rotate(nums, 38);
        assertArrayEquals(rotated, nums);
        nums = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27};
        reverse.rotate(nums, 38);
        assertArrayEquals(rotated, nums);
    }
}