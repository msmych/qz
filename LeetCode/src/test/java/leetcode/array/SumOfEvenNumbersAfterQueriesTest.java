package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumOfEvenNumbersAfterQueriesTest {

    private SumOfEvenNumbersAfterQueries sumOfEvenNumbersAfterQueries = new SumOfEvenNumbersAfterQueries.Solution();

    @Test void A1234queries10_N31_N40_23output8624() {
        assertArrayEquals(new int[]{8, 6, 2, 4}, sumOfEvenNumbersAfterQueries.sumEvenAfterQueries(
                new int[]{1, 2, 3, 4}, new int[][]{{1, 0}, {-3, 1}, {-4, 0}, {2, 3}}));
    }

    @Test void A1queries40output0() {
        assertArrayEquals(new int[]{0}, sumOfEvenNumbersAfterQueries.sumEvenAfterQueries(new int[]{1}, new int[][]{{4, 0}}));
    }
}