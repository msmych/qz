package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FindNumbersWithEvenNumberOfDigitsTest {

  private FindNumbersWithEvenNumberOfDigits findNumbersWithEvenNumberOfDigits = new FindNumbersWithEvenNumberOfDigits.Solution();

  @Test
  void nums12345() {
    assertEquals(2, findNumbersWithEvenNumberOfDigits.findNumbers(new int[]{12, 345, 2, 6, 7896}));
  }

  @Test
  void nums555() {
    assertEquals(1, findNumbersWithEvenNumberOfDigits.findNumbers(new int[]{555, 901, 482, 1771}));
  }
}