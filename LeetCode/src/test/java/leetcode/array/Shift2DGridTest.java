package leetcode.array;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;

class Shift2DGridTest {

  private Shift2DGrid shift2DGrid = new Shift2DGrid.Solution();

  @Test
  void grid123456789k1() {
    assertEquals(Arrays.asList(
      Arrays.asList(9, 1, 2),
      Arrays.asList(3, 4, 5),
      Arrays.asList(6, 7, 8)), shift2DGrid.shiftGrid(new int[][]{
      {1, 2, 3},
      {4, 5, 6},
      {7, 8, 9}
    }, 1));
  }

  @Test
  void grid3819k4() {
    assertEquals(Arrays.asList(
      Arrays.asList(12, 0, 21, 13),
      Arrays.asList(3, 8, 1, 9),
      Arrays.asList(19, 7, 2, 5),
      Arrays.asList(4, 6, 11, 10)), shift2DGrid.shiftGrid(new int[][]{
      {3, 8, 1, 9},
      {19, 7, 2, 5},
      {4, 6, 11, 10},
      {12, 0, 21, 13}
    }, 4));
  }

  @Test
  void grid123456789k9() {
    assertEquals(Arrays.asList(
      Arrays.asList(1, 2, 3),
      Arrays.asList(4, 5, 6),
      Arrays.asList(7, 8, 9)), shift2DGrid.shiftGrid(new int[][]{
      {1, 2, 3},
      {4, 5, 6},
      {7, 8, 9}}, 9));
  }

  @Test
  void grid1234675k23() {
    assertEquals(Arrays.asList(
      singletonList(7),
      singletonList(5),
      singletonList(1),
      singletonList(2),
      singletonList(3),
      singletonList(4),
      singletonList(6)),
      shift2DGrid.shiftGrid(new int[][]{{1},{2},{3},{4},{6},{7},{5}}, 23));
  }
}