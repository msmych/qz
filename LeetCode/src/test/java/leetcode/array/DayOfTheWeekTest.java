package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DayOfTheWeekTest {

  private DayOfTheWeek dayOfTheWeek = new DayOfTheWeek.Solution();

  @Test
  void day31_8_2019() {
    assertEquals("Saturday", dayOfTheWeek.dayOfTheWeek(31, 8, 2019));
  }

  @Test void day18_7_1999() {
    assertEquals("Sunday", dayOfTheWeek.dayOfTheWeek(18, 7, 1999));
  }

  @Test void day15_8_1993() {
    assertEquals("Sunday", dayOfTheWeek.dayOfTheWeek(15, 8, 1993));
  }

  @Test
  void day30_8_2019() {
    assertEquals("Friday", dayOfTheWeek.dayOfTheWeek(30, 8, 2019));
  }

  @Test
  void day29_8_2019() {
    assertEquals("Thursday", dayOfTheWeek.dayOfTheWeek(29, 8, 2019));
  }

  @Test
  void day28_8_2019() {
    assertEquals("Wednesday", dayOfTheWeek.dayOfTheWeek(28, 8, 2019));
  }

  @Test
  void day27_8_2019() {
    assertEquals("Tuesday", dayOfTheWeek.dayOfTheWeek(27, 8, 2019));
  }

  @Test
  void day26_8_2019() {
    assertEquals("Monday", dayOfTheWeek.dayOfTheWeek(26, 8, 2019));
  }
}
