package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FindNUniqueIntegersSumUpToZeroTest {

  private FindNUniqueIntegersSumUpToZero findNUniqueIntegersSumUpToZero = new FindNUniqueIntegersSumUpToZero.Solution();

  @Test
  void n5() {
    assertArrayEquals(new int[]{-2, -1, 0, 1, 2}, findNUniqueIntegersSumUpToZero.sumZero(5));
  }

  @Test
  void n3() {
    assertArrayEquals(new int[]{-1, 0, 1}, findNUniqueIntegersSumUpToZero.sumZero(3));
  }

  @Test
  void n1() {
    assertArrayEquals(new int[]{0}, findNUniqueIntegersSumUpToZero.sumZero(1));
  }

  @Test
  void n4() {
    assertArrayEquals(new int[]{-2, -1, 1, 2}, findNUniqueIntegersSumUpToZero.sumZero(4));
  }
}