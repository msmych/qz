package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FibonacciNumberTest {

    private FibonacciNumber fibonacciNumber = new FibonacciNumber.Solution();

    @Test void input2output1() {
        assertEquals(1, fibonacciNumber.fib(2));
    }

    @Test void input3output2() {
        assertEquals(2, fibonacciNumber.fib(3));
    }

    @Test void input4output3() {
        assertEquals(3, fibonacciNumber.fib(4));
    }
}