package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class SetMatrixZeroesTest {

    private SetMatrixZeroes setMatrixZeroes = new SetMatrixZeroes.Solution();

    @Test
    void matrix111_101_111() {
        int[][] nums = new int[][]{
            {1, 1, 1},
            {1, 0, 1},
            {1, 1, 1}};
        setMatrixZeroes.setZeroes(nums);
        assertArrayEquals(new int[][]{
            {1, 0, 1},
            {0, 0, 0},
            {1, 0, 1}
        }, nums);
    }

    @Test void matrix0120_3452_1315() {
        int[][] nums = new int[][]{
            {0, 1, 2, 0},
            {3, 4, 5, 2},
            {1, 3, 1, 5}};
        setMatrixZeroes.setZeroes(nums);
        assertArrayEquals(new int[][]{
            {0, 0, 0, 0},
            {0, 4, 5, 0},
            {0, 3, 1, 0}
        }, nums);
    }

    @Test void matrix111_012() {
        int[][] nums = {
            {1, 1, 1},
            {0, 1, 2}
        };
        setMatrixZeroes.setZeroes(nums);
        assertArrayEquals(new int[][]{
            {0, 1, 1},
            {0, 0, 0}
        }, nums);
    }

    @Test void matrix103() {
        int[][] nums = {{1, 0, 3}};
        setMatrixZeroes.setZeroes(nums);
        assertArrayEquals(new int[1][3], nums);
    }
}