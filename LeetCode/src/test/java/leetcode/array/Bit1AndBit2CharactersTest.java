package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bit1AndBit2CharactersTest {

    private Bit1AndBit2Characters bit1AndBit2Characters = new Bit1AndBit2Characters.Solution();

    @Test void bits100true() {
        assertTrue(bit1AndBit2Characters.isOneBitCharacter(new int[]{1, 0, 0}));
    }

    @Test void bits1110false() {
        assertFalse(bit1AndBit2Characters.isOneBitCharacter(new int[]{1, 1, 1, 0}));
    }
}