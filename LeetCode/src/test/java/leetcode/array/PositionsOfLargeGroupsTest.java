package leetcode.array;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionEquivalentToArrays;

class PositionsOfLargeGroupsTest {

    private PositionsOfLargeGroups positionsOfLargeGroups = new PositionsOfLargeGroups.Solution();

    @Test void abbxxxxzzy3$6() {
        assertListCollectionEquivalentToArrays(new Integer[][]{{3, 6}},
                positionsOfLargeGroups.largeGroupPositions("abbxxxxzzy"));
    }

    @Test void abc() {
        assertListCollectionEquivalentToArrays(new Integer[][]{}, positionsOfLargeGroups.largeGroupPositions("abc"));
    }

    @Test void abcdddeeeeaabbbcd3$5_6$9_12$14() {
        assertListCollectionEquivalentToArrays(new Integer[][]{{3, 5}, {6, 9}, {12, 14}},
                positionsOfLargeGroups.largeGroupPositions("abcdddeeeeaabbbcd"));
    }
}