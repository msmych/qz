package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FirstMissingPositiveTest {

  private FirstMissingPositive firstMissingPositive = new FirstMissingPositive.Solution();

  @Test
  void input120output3() {
    assertEquals(3, firstMissingPositive.firstMissingPositive(new int[]{1, 2, 0}));
  }

  @Test
  void input34N11output2() {
    assertEquals(2, firstMissingPositive.firstMissingPositive(new int[]{3, 4, -1, 1}));
  }

  @Test
  void input7_8_9_11_12output1() {
    assertEquals(1, firstMissingPositive.firstMissingPositive(new int[]{7, 8, 9, 11, 12}));
  }
}

