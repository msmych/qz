package leetcode.array;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionContainsArrays;

class QueensThatCanAttackTheKingTest {

  private QueensThatCanAttackTheKing queensThatCanAttackTheKing = new QueensThatCanAttackTheKing.Solution();

  @Test
  void q6() {
    assertListCollectionContainsArrays(new Integer[][]{{0,1},{1,0},{3,3}},
      queensThatCanAttackTheKing.queensAttacktheKing(new int[][]{{0,1},{1,0},{4,0},{0,4},{3,3},{2,4}}, new int[]{0,0}));
  }

  @Test
  void q7() {
    assertListCollectionContainsArrays(new Integer[][]{{2,2},{3,4},{4,4}},
      queensThatCanAttackTheKing.queensAttacktheKing(new int[][]{{0,0},{1,1},{2,2},{3,4},{3,5},{4,4},{4,5}}, new int[]{3,3}));
  }

  @Test
  void q34() {
    assertListCollectionContainsArrays(new Integer[][]{{2,3},{1,4},{1,6},{3,7},{4,3},{5,4},{4,5}},
      queensThatCanAttackTheKing.queensAttacktheKing(
        new int[][]{{5,6},{7,7},{2,1},{0,7},{1,6},{5,1},{3,7},{0,3},{4,0},{1,2},{6,3},{5,0},{0,4},{2,2},{1,1},{6,4},{5,4},
          {0,0},{2,6},{4,5},{5,2},{1,4},{7,5},{2,3},{0,5},{4,2},{1,0},{2,7},{0,1},{4,6},{6,1},{0,6},{4,3},{1,7}}, new int[]{3,4}));
  }
}
