package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FindWinnerOnATicTacToeGameTest {

  private FindWinnerOnATicTacToeGame findWinnerOnATicTacToeGame = new FindWinnerOnATicTacToeGame.Solution();

  @Test
  void XX00X() {
    assertEquals("A",
      findWinnerOnATicTacToeGame.tictactoe(new int[][]{{0, 0}, {2, 0}, {1, 1}, {2, 1}, {2, 2}}));
  }

  @Test
  void XX0X00() {
    assertEquals("B",
      findWinnerOnATicTacToeGame.tictactoe(new int[][]{{0, 0}, {1, 1}, {0, 1}, {0, 2}, {1, 0}, {2, 0}}));
  }

  @Test
  void XX000XX0X() {
    assertEquals("Draw",
      findWinnerOnATicTacToeGame.tictactoe(new int[][]{{0, 0}, {1, 1}, {2, 0}, {1, 0}, {1, 2}, {2, 1}, {0, 1}, {0, 2}, {2, 2}}));
  }

  @Test
  void X0() {
    assertEquals("Pending", findWinnerOnATicTacToeGame.tictactoe(new int[][]{{0, 0}, {1, 1}}));
  }
}