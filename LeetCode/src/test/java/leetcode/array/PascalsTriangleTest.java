package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static utils.TestUtils.assertListCollectionEquivalentToArrays;

class PascalsTriangleTest {

    private PascalsTriangle pascalsTriangle = new PascalsTriangle.Solution(),
            recursive = new PascalsTriangle.RecursiveSolution();

    @Test void input5output1_11_121_1331_14641() {
        Integer[][] triangle = new Integer[][]{
                {1},
                {1, 1},
                {1, 2, 1},
                {1, 3, 3, 1},
                {1, 4, 6, 4, 1}};
        assertListCollectionEquivalentToArrays(triangle, pascalsTriangle.generate(5));
        assertListCollectionEquivalentToArrays(triangle, recursive.generate(5));
    }

    @Test
    void input0outputVoidList() {
        assertEquals(0, pascalsTriangle.generate(0).size());
        assertEquals(0, recursive.generate(0).size());
    }
}