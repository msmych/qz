package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ArrayPartitionTest {

    private ArrayPartition arrayPartition = new ArrayPartition.SolutionI();

    @Test
    void input1432output4() {
        assertEquals(4, arrayPartition.arrayPairSum(new int[]{1, 4, 3, 2}));
    }
}