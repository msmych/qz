package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinaryPrefixDivisibleBy5Test {

    private BinaryPrefixDivisibleBy5 binaryPrefixDivisibleBy5 = new BinaryPrefixDivisibleBy5.Solution();

    @Test void input011true_false_false() {
        assertArrayEquals(new Boolean[]{true, false, false},
                binaryPrefixDivisibleBy5.prefixesDivBy5(new int[]{0, 1, 1}).toArray(new Boolean[]{}));
    }

    @Test void input111false_false_false() {
        assertArrayEquals(new Boolean[]{false, false, false},
                binaryPrefixDivisibleBy5.prefixesDivBy5(new int[]{1, 1, 1}).toArray(new Boolean[]{}));
    }

    @Test void input011111true_false_false_false_true_false() {
        assertArrayEquals(new Boolean[]{true, false, false, false, true, false},
                binaryPrefixDivisibleBy5.prefixesDivBy5(new int[]{0, 1, 1, 1, 1, 1}).toArray(new Boolean[]{}));
    }

    @Test void input11101false_false_false_false_false() {
        assertArrayEquals(new Boolean[]{false, false, false, false, false},
                binaryPrefixDivisibleBy5.prefixesDivBy5(new int[]{1, 1, 1, 0, 1}).toArray(new Boolean[]{}));
    }
}