package leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ValidMountainArrayTest {

    private ValidMountainArray validMountainArray = new ValidMountainArray.Solution();

    @Test void input21false() {
        assertFalse(validMountainArray.validMountainArray(new int[]{2, 1}));
    }

    @Test void input355false() {
        assertFalse(validMountainArray.validMountainArray(new int[]{3, 5, 5}));
    }

    @Test void input0321true() {
        assertTrue(validMountainArray.validMountainArray(new int[]{0, 3, 2, 1}));
    }

    @Test void input_false() {
        assertFalse(validMountainArray.validMountainArray(new int[0]));
    }

    @Test void input2false() {
        assertFalse(validMountainArray.validMountainArray(new int[]{2}));
    }

    @Test void input0123456789false() {
        assertFalse(validMountainArray.validMountainArray(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}));
    }
}