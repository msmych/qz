package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ToLowerCaseTest {

    private ToLowerCase toLowerCase = new ToLowerCase.Solution();

    @Test void Hello_hello() {
        assertEquals("hello", toLowerCase.toLowerCase("Hello"));
    }

    @Test void here_here() {
        assertEquals("here", toLowerCase.toLowerCase("here"));
    }

    @Test void LOVELY_lovely() {
        assertEquals("lovely", toLowerCase.toLowerCase("LOVELY"));
    }
}