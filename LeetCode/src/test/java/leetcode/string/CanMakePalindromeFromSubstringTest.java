package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class CanMakePalindromeFromSubstringTest {

  private CanMakePalindromeFromSubstring canMakePalindromeFromSubstring = new CanMakePalindromeFromSubstring.Solution();

  @Test
  void abcda() {
    assertArrayEquals(new Boolean[]{true, false, false, true, true},
      canMakePalindromeFromSubstring.canMakePaliQueries("abcda", new int[][]{
        {3, 3, 0}, {1, 2, 0}, {0, 3, 1}, {0, 3, 2}, {0, 4, 1}
      }).toArray(new Boolean[]{}));
  }

  @Test
  void hunu() {
    assertArrayEquals(new Boolean[]{true, false, true, true, true, true, true, false, true, false, true, true, true},
      canMakePalindromeFromSubstring.canMakePaliQueries("hunu", new int[][]
        {{1, 1, 1}, {2, 3, 0}, {3, 3, 1}, {0, 3, 2}, {1, 3, 3}, {2, 3, 1}, {3, 3, 1}, {0, 3, 0}, {1, 1, 1}, {2, 3, 0}, {3, 3, 1}, {0, 3, 1}, {1, 1, 1}})
        .toArray(new Boolean[]{}));
  }

  @Test
  void rkzavgdmdgt() {
    assertArrayEquals(new Boolean[]{false, true, true, true, false, true, true, true},
      canMakePalindromeFromSubstring.canMakePaliQueries("rkzavgdmdgt", new int[][]{{5, 8, 0}, {7, 9, 1}, {3, 6, 4}, {5, 5, 1}, {8, 10, 0}, {3, 9, 5}, {0, 10, 10}, {6, 8, 3}})
        .toArray(new Boolean[]{}));
  }

}
