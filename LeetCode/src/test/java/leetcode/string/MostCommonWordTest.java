package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MostCommonWordTest {

    private MostCommonWord mostCommonWord = new MostCommonWord.Solution();

    @Test void Bob_hit_a_ball() {
        assertEquals("ball", mostCommonWord.mostCommonWord(
                "Bob hit a ball, the hit BALL flew far after it was hit.", new String[]{"hit"}));
    }

    @Test void aaaabbbcc() {
        assertEquals("b", mostCommonWord.mostCommonWord("a, a, a, a, b,b,b,c, c", new String[]{"a"}));
    }
}