package leetcode.string;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class StringCompressionTest {

  private StringCompression stringCompression = new StringCompression.Solution();

  @Test void aabbccc() {
    char[] chars = {'a', 'a', 'b', 'b', 'c', 'c', 'c'};
    assertEquals(6, stringCompression.compress(chars));
    assertArrayEquals(new char[]{'a', '2', 'b', '2', 'c', '3'}, Arrays.copyOf(chars, 6));
  }

  @Test void a() {
    char[] chars = new char[]{'a'};
    assertEquals(1, stringCompression.compress(chars));
    assertArrayEquals(new char[]{'a'}, chars);
  }

  @Test void abbbbbbbbbbbb() {
    char[] chars = new char[]{'a','b','b','b','b','b','b','b','b','b','b','b','b'};
    assertEquals(4, stringCompression.compress(chars));
    assertArrayEquals(new char[]{'a','b','1','2'}, Arrays.copyOf(chars, 4));
  }

  @Test void aaabbaa() {
    char[] chars = {'a', 'a', 'a', 'b', 'b', 'a', 'a'};
    assertEquals(6, stringCompression.compress(chars));
    assertArrayEquals(new char[]{'a','3','b','2','a','2'}, Arrays.copyOf(chars, 6));
  }
}
