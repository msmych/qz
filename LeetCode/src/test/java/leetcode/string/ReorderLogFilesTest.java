package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class ReorderLogFilesTest {

    private ReorderLogFiles reorderLogFiles = new ReorderLogFiles.Solution();

    @Test void a1_9_2_3_1$g1_act_car$zo4_4_7$ab1_off_key_dog$a8_act_zoo() {
        assertArrayEquals(new String[]{"g1 act car","a8 act zoo","ab1 off key dog","a1 9 2 3 1","zo4 4 7"},
                reorderLogFiles.reorderLogFiles(
                        new String[]{"a1 9 2 3 1","g1 act car","zo4 4 7","ab1 off key dog","a8 act zoo"}));
    }

    @Test void j_mo() {
        assertArrayEquals(new String[]{"5 m w","j mo","t q h","g 07","o 2 0"},
                reorderLogFiles.reorderLogFiles(
                        new String[]{"j mo", "5 m w", "g 07", "o 2 0", "t q h"}));
    }
}