package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidNumberTest {

    private ValidNumber validNumber = new ValidNumber.Solution();

    @Test void input0true() {
        assertTrue(validNumber.isNumber("0"));
    }

    @Test void _0p1_true() {
        assertTrue(validNumber.isNumber(" 0.1 "));
    }

    @Test void abc_false() {
        assertFalse(validNumber.isNumber("abc"));
    }

    @Test void input1_a_false() {
        assertFalse(validNumber.isNumber("1 a"));
    }

    @Test void input2e10true() {
        assertTrue(validNumber.isNumber("2e10"));
    }

    @Test void _N90e3___true() {
        assertTrue(validNumber.isNumber(" -90e3   "));
    }

    @Test void _1e_false() {
        assertFalse(validNumber.isNumber(" 1e"));
    }

    @Test void e3false() {
        assertFalse(validNumber.isNumber("e3"));
    }

    @Test void _6eN1true() {
        assertTrue(validNumber.isNumber(" 6e-1"));
    }

    @Test void _99e2p5false() {
        assertFalse(validNumber.isNumber(" 99e2.5"));
    }

    @Test void input53p5e93true() {
        assertTrue(validNumber.isNumber("53.5e93"));
    }

    @Test void _NN6false() {
        assertFalse(validNumber.isNumber(" --6"));
    }

    @Test void NP3false() {
        assertFalse(validNumber.isNumber("-+3"));
    }

    @Test void input95a54e53false() {
        assertFalse(validNumber.isNumber("95a54e53"));
    }

    @Test void input1p_true() {
        assertTrue(validNumber.isNumber("1."));
    }

    @Test void p1true() {
        assertTrue(validNumber.isNumber(".1"));
    }

    @Test void Pp8true() {
        assertTrue(validNumber.isNumber("+.8"));
    }
}