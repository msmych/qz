package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinaryStringWithSubstringsRepresenting1ToNTest {

    private BinaryStringWithSubstringsRepresenting1ToN binaryStringWithSubstringsRepresenting1ToN =
            new BinaryStringWithSubstringsRepresenting1ToN.Solution();

    @Test void s0110n3true() {
        assertTrue(binaryStringWithSubstringsRepresenting1ToN.queryString("0110", 3));
    }

    @Test void s0110n4false() {
        assertFalse(binaryStringWithSubstringsRepresenting1ToN.queryString("0110", 4));
    }
}