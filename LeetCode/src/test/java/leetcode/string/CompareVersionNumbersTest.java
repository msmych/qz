package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CompareVersionNumbersTest {

    private CompareVersionNumbers compareVersionNumbers = new CompareVersionNumbers.Solution();

    @Test void v0_1v1_1outputN1() {
        assertEquals(-1, compareVersionNumbers.compareVersion("0.1", "1.1"));
    }

    @Test void v1_0_1v1output1() {
        assertEquals(1, compareVersionNumbers.compareVersion("1.0.1", "1"));
    }

    @Test void v7_5_2_4v7_5_3outputN1() {
        assertEquals(-1, compareVersionNumbers.compareVersion("7.5.2.4", "7.5.3"));
    }

    @Test void v1_01v1_001output0() {
        assertEquals(0, compareVersionNumbers.compareVersion("1.01", "1.001"));
    }

    @Test void v1_0v1_0_0output0() {
        assertEquals(0, compareVersionNumbers.compareVersion("1.0", "1.0.0"));
    }

    @Test void v1v1_1outputN1() {
        assertEquals(-1, compareVersionNumbers.compareVersion("1", "1.1"));
    }
}