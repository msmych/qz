package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RansomNoteTest {

    private RansomNote ransomNote = new RansomNote.Solution();

    @Test void a_b() {
        assertFalse(ransomNote.canConstruct("a", "b"));
    }

    @Test void aa_ab() {
        assertFalse(ransomNote.canConstruct("aa", "ab"));
    }

    @Test void aa_aab() {
        assertTrue(ransomNote.canConstruct("aa", "aab"));
    }
}