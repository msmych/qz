package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringToIntegerAtoiTest {

    private StringToIntegerAtoi stringToIntegerAtoi = new StringToIntegerAtoi.Solution();

    @Test void input42output42() {
        assertEquals(42, stringToIntegerAtoi.myAtoi("42"));
    }

    @Test void input___N42outputN42() {
        assertEquals(-42, stringToIntegerAtoi.myAtoi("   -42"));
    }

    @Test void input4193_with_words_output4193() {
        assertEquals(4193, stringToIntegerAtoi.myAtoi("4193 with words"));
    }

    @Test void input_words_and_987output0() {
        assertEquals(0, stringToIntegerAtoi.myAtoi("words and 987"));
    }

    @Test void inputN91283472332outputN2147483648() {
        assertEquals(-2147483648, stringToIntegerAtoi.myAtoi("-91283472332"));
    }
}