package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DetectCapitalTest {

    private DetectCapital detectCapital = new DetectCapital.Solution();

    @Test void USA() {
        assertTrue(detectCapital.detectCapitalUse("USA"));
    }

    @Test void FlaG() {
        assertFalse(detectCapital.detectCapitalUse("FlaG"));
    }
}