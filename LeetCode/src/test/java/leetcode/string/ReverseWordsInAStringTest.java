package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ReverseWordsInAStringTest {

    private ReverseWordsInAString reverseWordsInAString = new ReverseWordsInAString.Solution();

    @Test void input_the_sky_is_blue_output_blue_is_sky_the() {
        assertEquals("blue is sky the", reverseWordsInAString.reverseWords("the sky is blue"));
    }

    @Test void emptyInputEmptyOutput() {
        assertEquals("", reverseWordsInAString.reverseWords(""));
    }

    @Test void input_a_output_a() {
        assertEquals("a", reverseWordsInAString.reverseWords("a"));
    }

    @Test void input_output() {
        assertEquals("", reverseWordsInAString.reverseWords(" "));
    }

    @Test void input____output() {
        assertEquals("", reverseWordsInAString.reverseWords("    "));
    }

    @Test void input_1output1() {
        assertEquals("1", reverseWordsInAString.reverseWords(" 1"));
    }
}