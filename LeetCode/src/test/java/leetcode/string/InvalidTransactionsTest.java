package leetcode.string;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertCollectionContainsAll;

class InvalidTransactionsTest {

    private InvalidTransactions invalidTransactions = new InvalidTransactions.Solution();

    @Test void alice() {
        assertCollectionContainsAll(
            invalidTransactions.invalidTransactions(new String[]{"alice,20,800,mtv","alice,50,100,beijing"}),
            "alice,20,800,mtv","alice,50,100,beijing");
    }

    @Test void alice20() {
        assertCollectionContainsAll(
            invalidTransactions.invalidTransactions(new String[]{"alice,20,800,mtv","alice,50,1200,mtv"}),
            "alice,50,1200,mtv");
    }

    @Test void alice20_800() {
        assertCollectionContainsAll(
            invalidTransactions.invalidTransactions(new String[]{"alice,20,800,mtv","bob,50,1200,mtv"}),
            "bob,50,1200,mtv");
    }
}