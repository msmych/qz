package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RotatedDigitsTest {

    private RotatedDigits rotatedDigits = new RotatedDigits.Solution();

    @Test void input10output4() {
        assertEquals(4, rotatedDigits.rotatedDigits(10));
    }
}