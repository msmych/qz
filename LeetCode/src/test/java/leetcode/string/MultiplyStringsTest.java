package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultiplyStringsTest {

    private MultiplyStrings multiplyStrings = new MultiplyStrings.Solution();

    @Test void num2num3output6() {
        assertEquals("6", multiplyStrings.multiply("2", "3"));
    }

    @Test void num123num456output56088() {
        assertEquals("56088", multiplyStrings.multiply("123", "456"));
    }

    @Test void num9num9output81() {
        assertEquals("81", multiplyStrings.multiply("9", "9"));
    }

    @Test void num999num999output998001() {
        assertEquals("998001", multiplyStrings.multiply("999", "999"));
    }

    @Test void num9133num0output0() {
        assertEquals("0", multiplyStrings.multiply("9133", "0"));
    }
}