package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BuddyStringsTest {

    private BuddyStrings buddyStrings = new BuddyStrings.Solution();

    @Test void ab_ba_true() {
        assertTrue(buddyStrings.buddyStrings("ab", "ba"));
    }

    @Test void ab_ab_false() {
        assertFalse(buddyStrings.buddyStrings("ab", "ab"));
    }

    @Test void aa_aa_true() {
        assertTrue(buddyStrings.buddyStrings("aa", "aa"));
    }

    @Test void aaaaaaabc_aaaaaaacb_true() {
        assertTrue(buddyStrings.buddyStrings("aaaaaaabc", "aaaaaaacb"));
    }

    @Test void _aa_false() {
        assertFalse(buddyStrings.buddyStrings("", "aa"));
    }
}