package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DayOfTheYearTest {

    private DayOfTheYear dayOfTheYear = new DayOfTheYear.Solution();

    @Test void date20190109() {
        assertEquals(9, dayOfTheYear.dayOfYear("2019-01-09"));
    }

    @Test void date20190210() {
        assertEquals(41, dayOfTheYear.dayOfYear("2019-02-10"));
    }

    @Test void date20030301() {
        assertEquals(60, dayOfTheYear.dayOfYear("2003-03-01"));
    }

    @Test void date20040301() {
        assertEquals(61, dayOfTheYear.dayOfYear("2004-03-01"));
    }

    @Test void date19000325() {
        assertEquals(84, dayOfTheYear.dayOfYear("1900-03-25"));
    }
}