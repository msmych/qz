package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CompareStringsByFrequencyOfTheSmallestCharacterTest {

    private CompareStringsByFrequencyOfTheSmallestCharacter compareStringsByFrequencyOfTheSmallestCharacter =
        new CompareStringsByFrequencyOfTheSmallestCharacter.Solution();

    @Test void cbd() {
        assertArrayEquals(new int[]{1}, compareStringsByFrequencyOfTheSmallestCharacter.numSmallerByFrequency(
            new String[]{"cbd"}, new String[]{"zaaaz"}));
    }

    @Test void bbb() {
        assertArrayEquals(new int[]{1, 2}, compareStringsByFrequencyOfTheSmallestCharacter.numSmallerByFrequency(
            new String[]{"bbb", "cc"}, new String[]{"a", "aa", "aaa", "aaaa"}));
    }
}