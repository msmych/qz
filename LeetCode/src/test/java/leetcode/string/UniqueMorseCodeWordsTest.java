package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UniqueMorseCodeWordsTest {

    private UniqueMorseCodeWords uniqueMorseCodeWords = new UniqueMorseCodeWords.Solution();

    @Test void gin_zen_gig_msg() {
        assertEquals(2,
                uniqueMorseCodeWords.uniqueMorseRepresentations(new String[]{"gin", "zen", "gig", "msg"}));
    }
}