package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OccurrencesAfterBigramTest {

    private OccurrencesAfterBigram occurrencesAfterBigram = new OccurrencesAfterBigram.Solution();

    @Test void girl_student() {
        assertArrayEquals(new String[]{"girl", "student"},
                occurrencesAfterBigram.findOcurrences("alice is a good girl she is a good student", "a", "good"));
    }

    @Test void we_rock() {
        assertArrayEquals(new String[]{"we", "rock"},
                occurrencesAfterBigram.findOcurrences("we will we will rock you", "we", "will"));
    }

    @Test void kcyxdfnoa_jkypmsxd() {
        assertArrayEquals(new String[]{"kcyxdfnoa","kcyxdfnoa","kcyxdfnoa"},
                occurrencesAfterBigram.findOcurrences(
                        "jkypmsxd jkypmsxd kcyxdfnoa jkypmsxd kcyxdfnoa jkypmsxd kcyxdfnoa kcyxdfnoa jkypmsxd kcyxdfnoa",
                        "kcyxdfnoa", "jkypmsxd"));
    }
}