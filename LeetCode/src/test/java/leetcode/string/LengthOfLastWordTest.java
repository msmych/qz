package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LengthOfLastWordTest {

    private LengthOfLastWord lengthOfLastWord = new LengthOfLastWord.Solution();

    @Test void Hello_World5() {
        assertEquals(5, lengthOfLastWord.lengthOfLastWord("Hello World"));
    }
}