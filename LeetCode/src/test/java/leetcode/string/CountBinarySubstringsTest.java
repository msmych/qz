package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountBinarySubstringsTest {

    private CountBinarySubstrings countBinarySubstrings = new CountBinarySubstrings.Solution();

    @Test void input00110011output6() {
        assertEquals(6, countBinarySubstrings.countBinarySubstrings("00110011"));
    }

    @Test void input10101output4() {
        assertEquals(4, countBinarySubstrings.countBinarySubstrings("10101"));
    }
}