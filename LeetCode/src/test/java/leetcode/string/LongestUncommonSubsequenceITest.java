package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestUncommonSubsequenceITest {

    private LongestUncommonSubsequenceI longestUncommonSubsequenceI = new LongestUncommonSubsequenceI.Solution();

    @Test void aba_cdc() {
        assertEquals(3, longestUncommonSubsequenceI.findLUSlength("aba", "cdc"));
    }

    @Test void abc_abc() {
        assertEquals(-1, longestUncommonSubsequenceI.findLUSlength("abc", "abc"));
    }
}