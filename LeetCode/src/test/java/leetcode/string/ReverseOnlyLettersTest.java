package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ReverseOnlyLettersTest {

    private ReverseOnlyLetters reverseOnlyLetters = new ReverseOnlyLetters.Solution();

    @Test void input_ab_cd_output_dc_ba() {
        assertEquals("dc-ba", reverseOnlyLetters.reverseOnlyLetters("ab-cd"));
    }

    @Test void input_a_bC_dEf_ghIj_output_j_Ih_gfE_dCba() {
        assertEquals("j-Ih-gfE-dCba", reverseOnlyLetters.reverseOnlyLetters("a-bC-dEf-ghIj"));
    }

    @Test void input_Test1ng_Leet__code_Q$outputQedo1ct_eeLg__ntse_T$() {
        assertEquals("Qedo1ct-eeLg=ntse-T!", reverseOnlyLetters.reverseOnlyLetters("Test1ng-Leet=code-Q!"));
    }
}