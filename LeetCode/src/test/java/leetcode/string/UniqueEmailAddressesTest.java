package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UniqueEmailAddressesTest {

    private UniqueEmailAddresses uniqueEmailAddresses = new UniqueEmailAddresses.Solution();

    @Test void alex_bob_david() {
        assertEquals(2, uniqueEmailAddresses.numUniqueEmails(new String[]{
                "test.email+alex@leetcode.com",
                "test.e.mail+bob.cathy@leetcode.com",
                "testemail+david@lee.tcode.com"}));
    }
}