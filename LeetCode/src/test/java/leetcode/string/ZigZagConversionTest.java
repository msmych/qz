package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ZigZagConversionTest {

    private ZigZagConversion zigZagConversion = new ZigZagConversion.Solution();

    @Test void PAYPALISHIRING3PAHNAPLSIIGYIR() {
        assertEquals("PAHNAPLSIIGYIR", zigZagConversion.convert("PAYPALISHIRING", 3));
    }

    @Test
    void PAYPALISHIRING4PINALSIGYAHRPI() {
        assertEquals("PINALSIGYAHRPI", zigZagConversion.convert("PAYPALISHIRING", 4));
    }
}