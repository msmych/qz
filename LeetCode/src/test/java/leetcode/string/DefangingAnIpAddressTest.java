package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DefangingAnIpAddressTest {

    private DefangingAnIpAddress defangingAnIpAddress = new DefangingAnIpAddress.Solution();

    @Test void ip1111() {
        assertEquals("1[.]1[.]1[.]1", defangingAnIpAddress.defangIPaddr("1.1.1.1"));
    }

    @Test void ip255100500() {
        assertEquals("255[.]100[.]50[.]0", defangingAnIpAddress.defangIPaddr("255.100.50.0"));
    }
}