package leetcode.string;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertCollectionContainsAll;

class RemoveSubFoldersFromTheFilesystemTest {

  private RemoveSubFoldersFromTheFilesystem removeSubFoldersFromTheFilesystem = new RemoveSubFoldersFromTheFilesystem.Solution();

  @Test
  void acdcf() {
    assertCollectionContainsAll(removeSubFoldersFromTheFilesystem.removeSubfolders(new String[]{"/a", "/a/b", "/c/d", "/c/d/e", "/c/f"}),
      "/a", "/c/d", "/c/f");
  }

  @Test
  void a() {
    assertCollectionContainsAll(removeSubFoldersFromTheFilesystem.removeSubfolders(new String[]{"/a", "/a/b/c", "/a/b/d"}),
      "/a");
  }

  @Test
  void abcabcaabd() {
    assertCollectionContainsAll(removeSubFoldersFromTheFilesystem.removeSubfolders(new String[]{"/a/b/c", "/a/b/ca", "/a/b/d"}),
      "/a/b/c", "/a/b/ca", "/a/b/d");
  }
}
