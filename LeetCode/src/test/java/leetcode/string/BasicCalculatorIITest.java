package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BasicCalculatorIITest {

  private BasicCalculatorII basicCalculatorII = new BasicCalculatorII.Solution();

  @Test
  void calc3plus2times2() {
    assertEquals(7, basicCalculatorII.calculate("3+2*2"));
  }

  @Test
  void calc3div2() {
    assertEquals(1, basicCalculatorII.calculate("3/2 "));
  }

  @Test
  void calc3plus5div2() {
    assertEquals(5, basicCalculatorII.calculate("3+5 / 2"));
  }

  @Test
  void calc3225() {
    assertEquals(11, basicCalculatorII.calculate("3+2*2-5/3*2-6+7+8-5+5/2"));
  }

  @Test
  void calc14div3div2() {
    assertEquals(2, basicCalculatorII.calculate("14/3/2"));
  }

  @Test
  void calc125() {
    assertEquals(6, basicCalculatorII.calculate("1+2*5/3+6/4*2"));
  }
}
