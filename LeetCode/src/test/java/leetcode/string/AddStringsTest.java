package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddStringsTest {

  private AddStrings addStrings = new AddStrings.Solution();

  @Test void input1234567890() {
    assertEquals("1234567901", addStrings.addStrings("1234567890", "11"));
  }

  @Test void input92() {
    assertEquals("118", addStrings.addStrings("92", "26"));
  }
}
