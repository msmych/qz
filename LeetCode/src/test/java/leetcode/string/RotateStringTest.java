package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RotateStringTest {

    private RotateString rotateString = new RotateString.Solution();

    @Test void abcde_cdeab_true() {
        assertTrue(rotateString.rotateString("abcde", "cdeab"));
    }

    @Test void abcde_abced_false() {
        assertFalse(rotateString.rotateString("abcde", "abced"));
    }

    @Test void gcmbf_fgcmb_true() {
        assertTrue(rotateString.rotateString("gcmbf", "fgcmb"));
    }

    @Test void __true() {
        assertTrue(rotateString.rotateString("", ""));
    }
}