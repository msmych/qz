package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LicenseKeyFormattingTest {

    private LicenseKeyFormatting licenseKeyFormatting = new LicenseKeyFormatting.Solution();

    @Test void key5F3Z() {
        assertEquals("5F3Z-2E9W", licenseKeyFormatting.licenseKeyFormatting("5F3Z-2e-9-w", 4));
    }

    @Test void key2() {
        assertEquals("2-5G-3J", licenseKeyFormatting.licenseKeyFormatting("2-5g-3-J", 2));
    }

    @Test void keya0001() {
        assertEquals("A-0001-AFDS", licenseKeyFormatting.licenseKeyFormatting("a0001afds-", 4));
    }
}