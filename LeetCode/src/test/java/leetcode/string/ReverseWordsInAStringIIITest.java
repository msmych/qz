package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ReverseWordsInAStringIIITest {

    private ReverseWordsInAString reverseWordsInAString = new ReverseWordsInAString.SolutionIII();

    @Test
    void input_Lets_take_LeetCode_contest_output_steL_ekat_edoCteeL_tsetnoc() {
        assertEquals("s'teL ekat edoCteeL tsetnoc",
                reverseWordsInAString.reverseWords("Let's take LeetCode contest"));
    }
}