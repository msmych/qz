package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class NumberOfLinesToWriteStringTest {

    private NumberOfLinesToWriteString numberOfLinesToWriteString = new NumberOfLinesToWriteString.Solution();

    @Test void abcdefghijklmnopqrstuvwxyz() {
        assertArrayEquals(new int[]{3, 60},
                numberOfLinesToWriteString.numberOfLines(
                        new int[]{10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10},
                        "abcdefghijklmnopqrstuvwxyz"));
    }

    @Test void bbbcccdddaaa() {
        assertArrayEquals(new int[]{2, 4},
                numberOfLinesToWriteString.numberOfLines(
                        new int[]{4,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10},
                        "bbbcccdddaaa"));
    }
}
