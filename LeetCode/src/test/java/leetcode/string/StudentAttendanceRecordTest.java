package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentAttendanceRecordTest {

    private StudentAttendanceRecord studentAttendanceRecord = new StudentAttendanceRecord.Solution();

    @Test void PPALLP() {
        assertTrue(studentAttendanceRecord.checkRecord("PPALLP"));
    }

    @Test void PPALLL() {
        assertFalse(studentAttendanceRecord.checkRecord("PPALLL"));
    }

    @Test
    void AAAA() {
        assertFalse(studentAttendanceRecord.checkRecord("AAAA"));
    }
}