package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParsingABooleanExpressionTest {

    private ParsingABooleanExpression parsingABooleanExpression = new ParsingABooleanExpression.Solution();

    @Test void not_f() {
        assertTrue(parsingABooleanExpression.parseBoolExpr("!(f)"));
    }

    @Test void or_f_t() {
        assertTrue(parsingABooleanExpression.parseBoolExpr("|(f,t)"));
    }

    @Test void and_t_f() {
        assertFalse(parsingABooleanExpression.parseBoolExpr("&(t,f)"));
    }

    @Test void or_and_t_f_t_not_t() {
        assertFalse(parsingABooleanExpression.parseBoolExpr("|(&(t,f,t),!(t))"));
    }
}