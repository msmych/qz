package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RobotReturnToOriginTest {

    private RobotReturnToOrigin robotReturnToOrigin = new RobotReturnToOrigin.Solution();

    @Test void UDtrue() {
        assertTrue(robotReturnToOrigin.judgeCircle("UD"));
    }

    @Test void LLfalse() {
        assertFalse(robotReturnToOrigin.judgeCircle("LL"));
    }
}