package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TextJustificationTest {

    private TextJustification textJustification = new TextJustification.Solution();

    @Test void this_is_an_example() {
        assertArrayEquals(new String[]{
                "This    is    an",
                "example  of text",
                "justification.  "
        }, textJustification.fullJustify(
                new String[]{"This", "is", "an", "example", "of", "text", "justification."}, 16)
                .toArray(new String[]{}));
    }

    @Test void what_must_be() {
        assertArrayEquals(new String[]{
                "What   must   be",
                "acknowledgment  ",
                "shall be        "
        }, textJustification.fullJustify(new String[]{"What","must","be","acknowledgment","shall","be"}, 16)
                .toArray(new String[]{}));
    }

    @Test void science() {
        assertArrayEquals(new String[]{
                "Science  is  what we",
                "understand      well",
                "enough to explain to",
                "a  computer.  Art is",
                "everything  else  we",
                "do                  "
        }, textJustification.fullJustify(new String[]{
                "Science","is","what","we","understand","well","enough","to","explain",
                "to","a","computer.","Art","is","everything","else","we","do"}, 20).toArray(new String[]{}));
    }
}