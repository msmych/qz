package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RepeatedStringMatchTest {

    private RepeatedStringMatch repeatedStringMatch = new RepeatedStringMatch.Solution();

    @Test void abcd_cdabcdab_3() {
        assertEquals(3, repeatedStringMatch.repeatedStringMatch("abcd", "cdabcdab"));
    }

    @Test void aaaaaaaaaaaaaaaaaaaaaab_ba_2() {
        assertEquals(2, repeatedStringMatch.repeatedStringMatch("aaaaaaaaaaaaaaaaaaaaaab", "ba"));
    }
}