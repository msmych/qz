package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GreatestCommonDivisorOfStringsTest {

    private GreatestCommonDivisorOfStrings greatestCommonDivisorOfStrings = new GreatestCommonDivisorOfStrings.Solution();

    @Test void ABCABC_ABC() {
        assertEquals("ABC", greatestCommonDivisorOfStrings.gcdOfStrings("ABCABC", "ABC"));
    }

    @Test void ABABAB_ABAB() {
        assertEquals("AB", greatestCommonDivisorOfStrings.gcdOfStrings("ABABAB", "ABAB"));
    }

    @Test void LEET_CODE() {
        assertEquals("", greatestCommonDivisorOfStrings.gcdOfStrings("LEET", "CODE"));
    }
}