package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidPalindromeIITest {

    private ValidPalindromeII validPalindromeII = new ValidPalindromeII.Solution();

    @Test void aba_true() {
        assertTrue(validPalindromeII.validPalindrome("aba"));
    }

    @Test void abca_true() {
        assertTrue(validPalindromeII.validPalindrome("abca"));
    }
}