package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfSegmentsInAStringTest {

  private NumberOfSegmentsInAString numberOfSegmentsInAString = new NumberOfSegmentsInAString.Solution();

  @Test void john() {
    assertEquals(5, numberOfSegmentsInAString.countSegments("Hello, my name is John"));
  }
}
