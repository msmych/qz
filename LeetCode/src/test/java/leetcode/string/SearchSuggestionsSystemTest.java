package leetcode.string;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionEquivalentToArrays;

class SearchSuggestionsSystemTest {

  private SearchSuggestionsSystem searchSuggestionsSystem = new SearchSuggestionsSystem.Solution();

  @Test
  void mouse() {
    assertListCollectionEquivalentToArrays(new String[][]{
      {"mobile", "moneypot", "monitor"},
      {"mobile", "moneypot", "monitor"},
      {"mouse", "mousepad"},
      {"mouse", "mousepad"},
      {"mouse", "mousepad"}
    }, searchSuggestionsSystem.suggestedProducts(new String[]{"mobile", "mouse", "moneypot", "monitor", "mousepad"}, "mouse"));
  }

  @Test
  void havana() {
    assertListCollectionEquivalentToArrays(new String[][]{{"havana"}, {"havana"}, {"havana"}, {"havana"}, {"havana"}, {"havana"}},
      searchSuggestionsSystem.suggestedProducts(new String[]{"havana"}, "havana"));
  }

  @Test
  void bags() {
    assertListCollectionEquivalentToArrays(new String[][]{
      {"baggage", "bags", "banner"},
      {"baggage", "bags", "banner"},
      {"baggage", "bags"},
      {"bags"}
    }, searchSuggestionsSystem.suggestedProducts(new String[]{"bags", "baggage", "banner", "box", "cloths"}, "bags"));
  }

  @Test
  void tatiana() {
    assertListCollectionEquivalentToArrays(new String[][]{{}, {}, {}, {}, {}, {}, {}},
      searchSuggestionsSystem.suggestedProducts(new String[]{"havana"}, "tatiana"));
  }
}