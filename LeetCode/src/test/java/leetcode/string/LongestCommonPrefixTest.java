package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LongestCommonPrefixTest {

    private LongestCommonPrefix longestCommonPrefix = new LongestCommonPrefix.Solution();

    @Test void input_flower_flow_flight_output_fl() {
        assertEquals("fl", longestCommonPrefix.longestCommonPrefix(new String[]{"flower", "flow", "flight"}));
    }

    @Test void input_dog_racecar_car_output_() {
        assertEquals("", longestCommonPrefix.longestCommonPrefix(new String[]{"dog", "racecar", "car"}));
    }

    @Test void input_a_output_a() {
        assertEquals("a", longestCommonPrefix.longestCommonPrefix(new String[]{"a"}));
    }
}