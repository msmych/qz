package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CountAndSayTest {

    private CountAndSay countAndSay = new CountAndSay.Solution();

    @Test void n1output1() {
        assertEquals("1", countAndSay.countAndSay(1));
    }

    @Test void n4output1211() {
        assertEquals("1211", countAndSay.countAndSay(4));
    }

    @Test void n5output111221() {
        assertEquals("111221", countAndSay.countAndSay(5));
    }
}