package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GroupsOfSpecialEquivalentStringsTest {

    private GroupsOfSpecialEquivalentStrings groupsOfSpecialEquivalentStrings =
            new GroupsOfSpecialEquivalentStrings.Solution();

    @Test void abcacc3() {
        assertEquals(3,
                groupsOfSpecialEquivalentStrings.numSpecialEquivGroups(new String[]{"a", "b", "c", "a", "c", "c"}));
    }

    @Test void aa_bb_ab_ba4() {
        assertEquals(4,
                groupsOfSpecialEquivalentStrings.numSpecialEquivGroups(new String[]{"aa", "bb", "ab", "ba"}));
    }

    @Test void abc_acb_bac_bca_cab_cba3() {
        assertEquals(3, groupsOfSpecialEquivalentStrings.numSpecialEquivGroups(
                new String[]{"abc", "acb", "bac", "bca", "cab", "cba"}));
    }

    @Test void abcd_cdab_adcb_cbad1() {
        assertEquals(1,
                groupsOfSpecialEquivalentStrings.numSpecialEquivGroups(new String[]{"abcd", "cdab", "adcb", "cbad"}));
    }
}