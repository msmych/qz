package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReverseStringIITest {

    private ReverseStringII reverseStringII = new ReverseStringII.Solution();

    @Test void abcdefg2() {
        assertEquals("bacdfeg", reverseStringII.reverseStr("abcdefg", 2));
    }

    @Test void abcdefgh3() {
        assertEquals("cbadefhg", reverseStringII.reverseStr("abcdefgh", 3));
    }
}