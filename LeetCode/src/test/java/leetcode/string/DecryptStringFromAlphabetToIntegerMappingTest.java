package leetcode.string;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DecryptStringFromAlphabetToIntegerMappingTest {

  private DecryptStringFromAlphabetToIntegerMapping decryptStringFromAlphabetToIntegerMapping =
    new DecryptStringFromAlphabetToIntegerMapping.Solution();

  @Test
  void jkab() {
    assertEquals("jkab", decryptStringFromAlphabetToIntegerMapping.freqAlphabets("10#11#12"));
  }

  @Test
  void acz() {
    assertEquals("acz", decryptStringFromAlphabetToIntegerMapping.freqAlphabets("1326#"));
  }

  @Test
  void y() {
    assertEquals("y", decryptStringFromAlphabetToIntegerMapping.freqAlphabets("25#"));
  }

  @Test
  void abcdefghijklmnopqrstuvwxyz() {
    assertEquals("abcdefghijklmnopqrstuvwxyz",
      decryptStringFromAlphabetToIntegerMapping.freqAlphabets("12345678910#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#"));
  }
}