package leetcode.union_find;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SurroundedRegionsTest {

  private SurroundedRegions surroundedRegions = new SurroundedRegions.Solution();

  @Test
  void before4after1() {
    char[][] board = new char[][]{
      {'X', 'X', 'X', 'X'},
      {'X', 'O', 'O', 'X'},
      {'X', 'X', 'O', 'X'},
      {'X', 'O', 'X', 'X'}};
    surroundedRegions.solve(board);
    assertArrayEquals(new char[][]{
      {'X', 'X', 'X', 'X'},
      {'X', 'X', 'X', 'X'},
      {'X', 'X', 'X', 'X'},
      {'X', 'O', 'X', 'X'}
    }, board);
  }
}