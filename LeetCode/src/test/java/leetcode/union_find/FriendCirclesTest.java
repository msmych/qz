package leetcode.union_find;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FriendCirclesTest {

  private FriendCircles friendCircles = new FriendCircles.Solution();

  @Test
  void circles2() {
    assertEquals(2, friendCircles.findCircleNum(new int[][]{
      {1, 1, 0},
      {1, 1, 0},
      {0, 0, 1}}));
  }

  @Test
  void circle1() {
    assertEquals(1, friendCircles.findCircleNum(new int[][]{
      {1, 1, 0},
      {1, 1, 1},
      {0, 1, 1}}));
  }
}