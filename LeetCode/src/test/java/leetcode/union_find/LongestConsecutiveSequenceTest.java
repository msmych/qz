package leetcode.union_find;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestConsecutiveSequenceTest {

  private LongestConsecutiveSequence longestConsecutiveSequence = new LongestConsecutiveSequence.Solution();

  @Test
  void input100_4_200_1_3_2() {
    assertEquals(4, longestConsecutiveSequence.longestConsecutive(new int[]{100, 4, 200, 1, 3, 2}));
  }

  @Test
  void inputN7N13N9N47N32494N98N75N1N7() {
    assertEquals(4, longestConsecutiveSequence.longestConsecutive(new int[]{-7, -1, 3, -9, -4, 7, -3, 2, 4, 9, 4, -9, 8, -7, 5, -1, -7}));
  }
}