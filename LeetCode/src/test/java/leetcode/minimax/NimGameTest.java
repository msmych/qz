package leetcode.minimax;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NimGameTest {

    private NimGame nimGame = new NimGame.Solution();

    @Test void input4() {
        assertFalse(nimGame.canWinNim(4));
    }

    @Test void input7() {
        assertTrue(nimGame.canWinNim(7));
    }

    @Test void input46() {
        assertTrue(nimGame.canWinNim(46));
    }

    @Test void input10() {
        assertTrue(nimGame.canWinNim(10));
    }

    @Test void input11() {
        assertTrue(nimGame.canWinNim(11));
    }

    @Test void input12() {
        assertFalse(nimGame.canWinNim(12));
    }

    @Test void input13() {
        assertTrue(nimGame.canWinNim(13));
    }

    @Test void input5() {
        assertTrue(nimGame.canWinNim(5));
    }

    @Test void input6() {
        assertTrue(nimGame.canWinNim(6));
    }

    @Test void input1() {
        assertTrue(nimGame.canWinNim(1));
    }
}