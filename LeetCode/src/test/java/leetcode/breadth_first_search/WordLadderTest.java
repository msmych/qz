package leetcode.breadth_first_search;

import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

class WordLadderTest {

  private WordLadder wordLadder = new WordLadder.Solution();

  @Test
  void hit_cog5() {
    assertEquals(5,
      wordLadder.ladderLength("hit", "cog", asList("hot", "dot", "dog", "lot", "log", "cog")));
  }

  @Test
  void hit_cog0() {
    assertEquals(0,
      wordLadder.ladderLength("hit", "cog", asList("hot", "dot", "dog", "lot", "log")));
  }

  @Test
  void qa_sq() {
    assertEquals(5, wordLadder.ladderLength("qa", "sq", asList(
      "si", "go", "se", "cm", "so", "ph", "mt", "db", "mb", "sb", "kr", "ln", "tm", "le", "av", "sm", "ar", "ci", "ca", "br",
      "ti", "ba", "to", "ra", "fa", "yo", "ow", "sn", "ya", "cr", "po", "fe", "ho", "ma", "re", "or", "rn", "au", "ur", "rh",
      "sr", "tc", "lt", "lo", "as", "fr", "nb", "yb", "if", "pb", "ge", "th", "pm", "rb", "sh", "co", "ga", "li", "ha", "hz",
      "no", "bi", "di", "hi", "qa", "pi", "os", "uh", "wm", "an", "me", "mo", "na", "la", "st", "er", "sc", "ne", "mn", "mi",
      "am", "ex", "pt", "io", "be", "fm", "ta", "tb", "ni", "mr", "pa", "he", "lr", "sq", "ye")));
  }

  @Test
  void hot_dog() {
    assertEquals(0, wordLadder.ladderLength("hot", "dog", asList("hot", "dog")));
  }
}