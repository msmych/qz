package leetcode.breadth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RemoveInvalidParenthesesTest {

  private RemoveInvalidParentheses removeInvalidParentheses = new RemoveInvalidParentheses.Solution();

  @Test
  void oolo() {
    assertArrayEquals(new String[]{"()()()", "(())()"},
      removeInvalidParentheses.removeInvalidParentheses("()())()").toArray(new String[]{}));
  }

  @Test
  void aolo() {
    assertArrayEquals(new String[]{"(a)()()", "(a())()"},
      removeInvalidParentheses.removeInvalidParentheses("(a)())()").toArray(new String[]{}));
  }

  @Test
  void x() {
    assertArrayEquals(new String[]{""}, removeInvalidParentheses.removeInvalidParentheses(")(").toArray(new String[]{}));
  }
}