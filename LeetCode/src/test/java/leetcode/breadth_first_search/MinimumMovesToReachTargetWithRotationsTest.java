package leetcode.breadth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinimumMovesToReachTargetWithRotationsTest {

    private MinimumMovesToReachTargetWithRotations minimumMovesToReachTargetWithRotations = new MinimumMovesToReachTargetWithRotations.Solution();

    @Test
    void grid11() {
        assertEquals(11, minimumMovesToReachTargetWithRotations.minimumMoves(new int[][]{
            {0, 0, 0, 0, 0, 1},
            {1, 1, 0, 0, 1, 0},
            {0, 0, 0, 0, 1, 1},
            {0, 0, 1, 0, 1, 0},
            {0, 1, 1, 0, 0, 0},
            {0, 1, 1, 0, 0, 0}
        }));
    }

    @Test
    void grid9() {
        assertEquals(9, minimumMovesToReachTargetWithRotations.minimumMoves(new int[][]{
            {0, 0, 1, 1, 1, 1},
            {0, 0, 0, 0, 1, 1},
            {1, 1, 0, 0, 0, 1},
            {1, 1, 1, 0, 0, 1},
            {1, 1, 1, 0, 0, 1},
            {1, 1, 1, 0, 0, 0}
        }));
    }
}