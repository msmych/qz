package leetcode.breadth_first_search;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static utils.TestUtils.assertListCollectionEquivalentToArrays;

class BinaryTreeZigzagLevelOrderTraversalTest {

  private BinaryTreeZigzagLevelOrderTraversal binaryTreeZigzagLevelOrderTraversal =
    new BinaryTreeZigzagLevelOrderTraversal.Solution();

  @Test
  void tree3() {
    assertListCollectionEquivalentToArrays(new Integer[][]{
      {3},
      {20, 9},
      {15, 7}
    }, binaryTreeZigzagLevelOrderTraversal.zigzagLevelOrder(fromValAndLeftAndRight(3,
      new TreeNode(9),
      fromValAndLeftAndRight(20, new TreeNode(15), new TreeNode(7)))));
  }

  @Test
  void tree12345() {
    assertListCollectionEquivalentToArrays(new Integer[][]{
      {1},
      {3, 2},
      {4, 5}
    }, binaryTreeZigzagLevelOrderTraversal.zigzagLevelOrder(fromValAndLeftAndRight(1,
      fromValAndLeft(2, new TreeNode(4)),
      fromValAndLeftAndRight(3, null, new TreeNode(5)))));
  }
}
