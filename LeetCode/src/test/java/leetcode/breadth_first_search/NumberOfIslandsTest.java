package leetcode.breadth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfIslandsTest {

  private NumberOfIslands queue = new NumberOfIslands.QueueSolution();
  private NumberOfIslands stack = new NumberOfIslands.StackSolution();

  @Test
  void input11110_11010_11000_00000output1() {
    char[][] grid = new char[][]{
      {'1', '1', '1', '1', '0'},
      {'1', '1', '0', '1', '0'},
      {'1', '1', '0', '0', '0'},
      {'0', '0', '0', '0', '0'}};
    assertEquals(1, queue.numIslands(grid));
    assertEquals(1, stack.numIslands(grid));
  }

  @Test
  void input11000_11000_00100_00011output3() {
    char[][] grid = new char[][]{
      {'1', '1', '0', '0', '0'},
      {'1', '1', '0', '0', '0'},
      {'0', '0', '1', '0', '0'},
      {'0', '0', '0', '1', '1'}};
    assertEquals(3, queue.numIslands(grid));
    assertEquals(3, stack.numIslands(grid));
  }
}
