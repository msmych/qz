package leetcode.breadth_first_search;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class GetWatchedVideosByYourFriendsTest {

  private GetWatchedVideosByYourFriends getWatchedVideosByYourFriends = new GetWatchedVideosByYourFriends.Solution();

  @Test
  void abcbcd01() {
    assertArrayEquals(new String[]{"B", "C"},
      getWatchedVideosByYourFriends.watchedVideosByFriends(
        Arrays.asList(Arrays.asList("A", "B"), singletonList("C"), Arrays.asList("B", "C"), singletonList("D")),
        new int[][]{{1, 2}, {0, 3}, {0, 3}, {1, 2}}, 0, 1).toArray(new String[]{}));
  }

  @Test
  void abcbcd02() {
    assertArrayEquals(new String[]{"D"},
      getWatchedVideosByYourFriends.watchedVideosByFriends(
        Arrays.asList(Arrays.asList("A", "B"), singletonList("C"), Arrays.asList("B", "C"), singletonList("D")),
        new int[][]{{1, 2}, {0, 3}, {0, 3}, {1, 2}}, 0, 2).toArray(new String[]{}));
  }

  @Test
  void ayyihidz() {
    assertArrayEquals(new String[]{"ayyihidz", "bjwtssmu", "bvcca", "fiq", "ljc", "viu"},
      getWatchedVideosByYourFriends.watchedVideosByFriends(
        Arrays.asList(singletonList("bjwtssmu"),
          Arrays.asList("aygr", "mqls"),
          Arrays.asList("vrtxa", "zxqzeqy", "nbpl", "qnpl"),
          Arrays.asList("r", "otazhu", "rsf"),
          Arrays.asList("bvcca", "ayyihidz", "ljc", "fiq", "viu")),
        new int[][]{{3, 2, 1, 4}, {0, 4}, {4, 0}, {0, 4}, {2, 3, 1, 0}}, 3, 1).toArray(new String[]{}));
  }

}