package leetcode.breadth_first_search;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class MinimumDepthOfBinaryTreeTest {

    private MinimumDepthOfBinaryTree minimumDepthOfBinaryTree = new MinimumDepthOfBinaryTree.Solution();

    @Test void tree3() {
        assertEquals(2, minimumDepthOfBinaryTree.minDepth(fromValAndLeftAndRight(3, new TreeNode(9),
                fromValAndLeftAndRight(20, new TreeNode(15), new TreeNode(7)))));
    }

    @Test void tree12() {
        assertEquals(2, minimumDepthOfBinaryTree.minDepth(fromValAndLeft(1, new TreeNode(2))));
    }
}