package leetcode.breadth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PerfectSquaresTest {

  private PerfectSquares perfectSquares = new PerfectSquares.Solution();

  @Test
  void n12output3() {
    assertEquals(3, perfectSquares.numSquares(12));
  }

  @Test
  void n13output2() {
    assertEquals(2, perfectSquares.numSquares(13));
  }

  @Test
  void n7168() {
    System.out.println(perfectSquares.numSquares(7168));
  }

  @Test
  void n220() {
    assertEquals(4, perfectSquares.numSquares(220));
  }
}