package leetcode.breadth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinimumNumberOfFlipsToConvertBinaryMatrixToZeroMatrixTest {

  private MinimumNumberOfFlipsToConvertBinaryMatrixToZeroMatrix flipsToConvertBinaryMatrixToZeroMatrix =
    new MinimumNumberOfFlipsToConvertBinaryMatrixToZeroMatrix.Solution();

  @Test
  void mat0001() {
    assertEquals(3, flipsToConvertBinaryMatrixToZeroMatrix.minFlips(new int[][]{{0, 0}, {0, 1}}));
  }

  @Test
  void mat0() {
    assertEquals(0, flipsToConvertBinaryMatrixToZeroMatrix.minFlips(new int[][]{{0}}));
  }

  @Test
  void mat111101000() {
    assertEquals(6, flipsToConvertBinaryMatrixToZeroMatrix.minFlips(new int[][]{{1, 1, 1}, {1, 0, 1}, {0, 0, 0}}));
  }

  @Test
  void mat100100() {
    assertEquals(-1, flipsToConvertBinaryMatrixToZeroMatrix.minFlips(new int[][]{{1, 0, 0}, {1, 0, 0}}));
  }
}