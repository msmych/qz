package leetcode.breadth_first_search;

import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static utils.TestUtils.assertListCollectionContainsArrays;

class WordLadderIITest {

    private WordLadderII wordLadderII = new WordLadderII.Solution();

    @Test void hit_cog2() {
        assertListCollectionContainsArrays(new String[][]{
                {"hit","hot","dot","dog","cog"},
                {"hit","hot","lot","log","cog"}
        }, wordLadderII.findLadders("hit", "cog", asList("hot","dot","dog","lot","log","cog")));
    }

    @Test
    void hit_cog0() {
        assertTrue(wordLadderII.findLadders("hit", "cog", asList("hot","dot","dog","lot","log"))
                .isEmpty());
    }

    @Test void a_c1() {
        assertEquals(singletonList(asList("a", "c")), wordLadderII.findLadders("a", "c", asList("a", "b", "c")));
    }

    @Test void hot_dog0() {
        assertTrue(wordLadderII.findLadders("hot", "dog", asList("hot", "dog")).isEmpty());
    }

    @Test void hot_dog1() {
        assertEquals(singletonList(asList("hot", "dot", "dog")), wordLadderII.findLadders("hot", "dog", asList("hot","dog","dot")));
    }
}