package leetcode.breadth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RottingOrangesTest {

    private RottingOranges rottingOranges = new RottingOranges.Solution();

    @Test void input211_110_011output4() {
        assertEquals(4, rottingOranges.orangesRotting(new int[][]{
                {2, 1, 1},
                {1, 1, 0},
                {0, 1, 1}
        }));
    }

    @Test void input211_011_101outputN1() {
        assertEquals(-1, rottingOranges.orangesRotting(new int[][]{
                {2, 1, 1},
                {0, 1, 1},
                {1, 0, 1}
        }));
    }

    @Test void input02output0() {
        assertEquals(0, rottingOranges.orangesRotting(new int[][]{{0, 2}}));
    }
}