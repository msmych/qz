package leetcode.breadth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OpenTheLockTest {

    private OpenTheLock openTheLock = new OpenTheLock.Solution();

    @Test void deadends0201_0101_0102_1212_2002target0202output6() {
        assertEquals(6, openTheLock.openLock(new String[]{"0201","0101","0102","1212","2002"}, "0202"));
    }

    @Test void deadends8888target0009() {
        assertEquals(1, openTheLock.openLock(new String[]{"8888"}, "0009"));
    }

    @Test void deadends8887_8889_8878_8898_8788_8988_7888_9888target8888() {
        assertEquals(-1,
                openTheLock.openLock(new String[]{"8887","8889","8878","8898","8788","8988","7888","9888"}, "8888"));
    }

    @Test void deadends0000target8888() {
        assertEquals(-1, openTheLock.openLock(new String[]{"0000"}, "8888"));
    }
}