package leetcode.breadth_first_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaximumCandiesYouCanGetFromBoxesTest {

  private MaximumCandiesYouCanGetFromBoxes maximumCandiesYouCanGetFromBoxes = new MaximumCandiesYouCanGetFromBoxes.Solution();

  @Test
  void candies754100() {
    assertEquals(16, maximumCandiesYouCanGetFromBoxes.maxCandies(
      new int[]{1, 0, 1, 0}, new int[]{7, 5, 4, 100}, new int[][]{{}, {}, {1}, {}}, new int[][]{{1, 2}, {3}, {}, {}}, new int[]{0}));
  }

  @Test
  void candies111111() {
    assertEquals(6, maximumCandiesYouCanGetFromBoxes.maxCandies(
      new int[]{1, 0, 0, 0, 0, 0}, new int[]{1, 1, 1, 1, 1, 1}, new int[][]{{1, 2, 3, 4, 5}, {}, {}, {}, {}, {}}, new int[][]{{1, 2, 3, 4, 5}, {}, {}, {}, {}, {}}, new int[]{0}));
  }

  @Test
  void candies1001100() {
    assertEquals(1, maximumCandiesYouCanGetFromBoxes.maxCandies(
      new int[]{1, 1, 1}, new int[]{100, 1, 100}, new int[][]{{}, {0, 2}, {}}, new int[][]{{}, {}, {}}, new int[]{1}));
  }

  @Test
  void candies100() {
    assertEquals(0, maximumCandiesYouCanGetFromBoxes.maxCandies(
      new int[]{1}, new int[]{100}, new int[][]{{}}, new int[][]{{}}, new int[]{}));
  }

  @Test
  void candies232() {
    assertEquals(7, maximumCandiesYouCanGetFromBoxes.maxCandies(
      new int[]{1, 1, 1}, new int[]{2, 3, 2}, new int[][]{{}, {}, {}}, new int[][]{{}, {}, {}}, new int[]{2, 1, 0}));
  }
}