package leetcode.design;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CombinationIteratorTest {

  @Test
  void abc2() {
    CombinationIterator iterator = new CombinationIterator("abc", 2);
    assertEquals("ab", iterator.next());
    assertTrue(iterator.hasNext());
    assertEquals("ac", iterator.next());
    assertTrue(iterator.hasNext());
    assertEquals("bc", iterator.next());
    assertFalse(iterator.hasNext());
  }

  @Test
  void abcdefghijk4() {
    CombinationIterator iterator = new CombinationIterator("abcdefghijk", 4);
    while (iterator.hasNext()) {
      System.out.println(iterator.next());
    }
  }
}