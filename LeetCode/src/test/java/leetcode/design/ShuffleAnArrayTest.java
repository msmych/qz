package leetcode.design;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShuffleAnArrayTest {

    @Test void test123() {
        ShuffleAnArray shuffleAnArray = new ShuffleAnArray(new int[]{1, 2, 3});
        shuffleAnArray.shuffle();
        shuffleAnArray.reset();
        shuffleAnArray.shuffle();
    }
}