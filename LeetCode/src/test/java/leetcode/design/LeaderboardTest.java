package leetcode.design;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LeaderboardTest {

  @Test
  void leader1_73() {
    Leaderboard leaderboard = new Leaderboard();
    leaderboard.addScore(1, 73);
    leaderboard.addScore(2,56);
    leaderboard.addScore(3,39);
    leaderboard.addScore(4,51);
    leaderboard.addScore(5, 4);
    assertEquals(73, leaderboard.top(1));
    leaderboard.reset(1);
    leaderboard.reset(2);
    leaderboard.addScore(2, 51);
    assertEquals(141, leaderboard.top(3));
  }
}