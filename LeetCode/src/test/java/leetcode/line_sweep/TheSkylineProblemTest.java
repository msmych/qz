package leetcode.line_sweep;

import org.junit.jupiter.api.Test;
import utils.TestUtils;

import static org.junit.jupiter.api.Assertions.assertTrue;

class TheSkylineProblemTest {

    private TheSkylineProblem theSkylineProblem = new TheSkylineProblem.Solution();

    @Test void input2910() {
        TestUtils.assertListCollectionEquivalentToArrays(new Integer[][]{
                {2, 10}, {3, 15}, {7, 12}, {12, 0}, {15, 10}, {20, 8}, {24, 0}
        }, theSkylineProblem.getSkyline(new int[][]{
                {2, 9, 10}, {3, 7, 15}, {5, 12, 12}, {15, 20, 10}, {19, 24, 8}}));
    }

    @Test void input_() {
        assertTrue(theSkylineProblem.getSkyline(new int[][]{}).isEmpty());
    }
}