package leetcode.line_sweep;

import org.junit.jupiter.api.Test;

import static utils.TestUtils.assertListCollectionEquivalentToArrays;

class RemoveIntervalTest {

  private RemoveInterval removeInterval = new RemoveInterval.Solution();

  @Test
  void intervals023457() {
    assertListCollectionEquivalentToArrays(new Integer[][]{{0, 1}, {6, 7}},
      removeInterval.removeInterval(new int[][]{{0, 2}, {3, 4}, {5, 7}}, new int[]{1, 6}));
  }

  @Test
  void intervals05() {
    assertListCollectionEquivalentToArrays(new Integer[][]{{0, 2}, {3, 5}},
      removeInterval.removeInterval(new int[][]{{0, 5}}, new int[]{2, 3}));
  }
}