package leetcode.line_sweep;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RemoveCoveredIntervalsTest {

  private RemoveCoveredIntervals removeCoveredIntervals = new RemoveCoveredIntervals.Solution();

  @Test
  void intervals143628() {
    assertEquals(2, removeCoveredIntervals.removeCoveredIntervals(new int[][]{{1, 4}, {3, 6}, {2, 8}}));
  }

  @Test
  void intervals25341867() {
    assertEquals(1, removeCoveredIntervals.removeCoveredIntervals(new int[][]{{2, 5}, {3, 4}, {1, 8}, {6, 7}}));
  }
}