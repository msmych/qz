package leetcode.heap;

import leetcode.linked_list.ListNode;
import org.junit.jupiter.api.Test;

class MergeKSortedListsTest {

  private MergeKSortedLists mergeKSortedLists = new MergeKSortedLists.Solution();

  @Test
  void input145_134_26output11234456() {
    mergeKSortedLists.mergeKLists(new ListNode[]{ListNode.fromVals(1, 4, 5), ListNode.fromVals(1, 3, 4), ListNode.fromVals(2, 6)});
  }
}
