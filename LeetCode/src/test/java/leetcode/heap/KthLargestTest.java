package leetcode.heap;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class KthLargestTest {

    @Test void k3arr4582() {
        KthLargest kthLargest = new KthLargest(3, new int[]{4, 5, 8, 2});
        assertEquals(4, kthLargest.add(3));
        assertEquals(5, kthLargest.add(5));
        assertEquals(5, kthLargest.add(10));
        assertEquals(8, kthLargest.add(9));
        assertEquals(8, kthLargest.add(4));
    }

    @Test void k1arr_() {
        KthLargest kthLargest = new KthLargest(1, new int[0]);
        assertEquals(-3, kthLargest.add(-3));
        assertEquals(-2, kthLargest.add(-2));
        assertEquals(-2, kthLargest.add(-4));
        assertEquals(0, kthLargest.add(0));
        assertEquals(4, kthLargest.add(4));
    }

    @Test void k2arr0() {
        KthLargest kthLargest = new KthLargest(2, new int[]{0});
        assertEquals(-1, kthLargest.add(-1));
        assertEquals(0, kthLargest.add(1));
        assertEquals(0, kthLargest.add(-2));
        assertEquals(0, kthLargest.add(-4));
        assertEquals(1, kthLargest.add(3));
    }

    @Test void k3arr5N1() {
        KthLargest kthLargest = new KthLargest(3, new int[]{5, -1});
        assertEquals(-1, kthLargest.add(2));
        assertEquals(1, kthLargest.add(1));
        assertEquals(1, kthLargest.add(-1));
        assertEquals(2, kthLargest.add(3));
        assertEquals(3, kthLargest.add(4));
    }

    @Test void k7arrN10_1_3_1_4_10_3_9_4_5_1() {
        KthLargest kthLargest = new KthLargest(7, new int[]{-10, 1, 3, 1, 4, 10, 3, 9, 4, 5, 1});
        assertEquals(3, kthLargest.add(3));
    }
}