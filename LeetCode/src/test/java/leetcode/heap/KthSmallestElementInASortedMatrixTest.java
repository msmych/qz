package leetcode.heap;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KthSmallestElementInASortedMatrixTest {

  private KthSmallestElementInASortedMatrix kthSmallestElementInASortedMatrix = new KthSmallestElementInASortedMatrix.Solution();

  @Test
  void matrix159() {
    assertEquals(13, kthSmallestElementInASortedMatrix.kthSmallest(new int[][]{
      {1, 5, 9},
      {10, 11, 13},
      {12, 13, 15}
    }, 8));
  }

  @Test
  void matrix1213() {
    assertEquals(1, kthSmallestElementInASortedMatrix.kthSmallest(new int[][]{{1, 2}, {1, 3}}, 1));
  }

  @Test
  void matrix135() {
    assertEquals(7, kthSmallestElementInASortedMatrix.kthSmallest(new int[][]{
      {1, 3, 5},
      {6, 7, 12},
      {11, 14, 14}
    }, 5));
  }
}