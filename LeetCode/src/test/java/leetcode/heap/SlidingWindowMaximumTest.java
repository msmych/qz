package leetcode.heap;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SlidingWindowMaximumTest {

  private SlidingWindowMaximum slidingWindowMaximum = new SlidingWindowMaximum.Solution();

  @Test
  void max335567() {
    assertArrayEquals(new int[]{3, 3, 5, 5, 6, 7}, slidingWindowMaximum.maxSlidingWindow(new int[]{1, 3, -1, -3, 5, 3, 6, 7}, 3));
  }

  @Test
  void max_() {
    assertArrayEquals(new int[]{}, slidingWindowMaximum.maxSlidingWindow(new int[]{}, 0));
  }
}