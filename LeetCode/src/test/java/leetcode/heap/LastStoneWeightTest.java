package leetcode.heap;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LastStoneWeightTest {

    private LastStoneWeight lastStoneWeight = new LastStoneWeight.Solution();

    @Test void input274181output1() {
        assertEquals(1, lastStoneWeight.lastStoneWeight(new int[]{2, 7, 4, 1, 8, 1}));
    }

    @Test void input22output0() {
        assertEquals(0, lastStoneWeight.lastStoneWeight(new int[]{2, 2}));
    }
}