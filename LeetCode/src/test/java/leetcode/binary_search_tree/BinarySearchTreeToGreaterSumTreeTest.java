package leetcode.binary_search_tree;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static leetcode.tree.TreeNode.serialize;
import static org.junit.jupiter.api.Assertions.*;

class BinarySearchTreeToGreaterSumTreeTest {

    private BinarySearchTreeToGreaterSumTree binarySearchTreeToGreaterSumTree = new BinarySearchTreeToGreaterSumTree.Solution();

    @Test void tree4_30() {
        assertEquals(serialize(
                fromValAndLeftAndRight(30,
                        fromValAndLeftAndRight(36,
                                new TreeNode(36),
                                fromValAndLeftAndRight(35, null, new TreeNode(33))),
                        fromValAndLeftAndRight(21,
                                new TreeNode(26),
                                fromValAndLeftAndRight(15, null, new TreeNode(8))))),
                serialize(binarySearchTreeToGreaterSumTree.bstToGst(fromValAndLeftAndRight(4,
                        fromValAndLeftAndRight(1,
                                new TreeNode(0),
                                fromValAndLeftAndRight(2, null, new TreeNode(3))),
                        fromValAndLeftAndRight(6,
                                new TreeNode(5),
                                fromValAndLeftAndRight(7, null, new TreeNode(8)))))));
    }
}