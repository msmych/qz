package leetcode.binary_search_tree;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class MinimumDistanceBetweenBstNodesTest {

    private MinimumDistanceBetweenBstNodes minimumDistanceBetweenBstNodes = new MinimumDistanceBetweenBstNodes.Solution();

    @Test void root42613$$output1() {
        assertEquals(1, minimumDistanceBetweenBstNodes.minDiffInBST(
                fromValAndLeftAndRight(4,
                        fromValAndLeftAndRight(2, new TreeNode(1), new TreeNode(3)),
                        new TreeNode(6))));
    }
}