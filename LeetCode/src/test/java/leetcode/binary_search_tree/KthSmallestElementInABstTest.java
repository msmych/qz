package leetcode.binary_search_tree;

import leetcode.tree.TreeNode;
import org.junit.jupiter.api.Test;

import static leetcode.tree.TreeNode.fromValAndLeft;
import static leetcode.tree.TreeNode.fromValAndLeftAndRight;
import static org.junit.jupiter.api.Assertions.*;

class KthSmallestElementInABstTest {

    private KthSmallestElementInABst kthSmallestElementInABst = new KthSmallestElementInABst.Solution();

    @Test void k1() {
        assertEquals(1,
            kthSmallestElementInABst.kthSmallest(fromValAndLeftAndRight(3,
                fromValAndLeftAndRight(1, null, new TreeNode(2)),
                new TreeNode(4)),
                1));
    }

    @Test void k3() {
        assertEquals(3,
            kthSmallestElementInABst.kthSmallest(fromValAndLeftAndRight(5,
                fromValAndLeftAndRight(3,
                    fromValAndLeft(2, new TreeNode(1)),
                    new TreeNode(4)),
                new TreeNode(6)),
                3));
    }

    @Test void k2() {
      assertEquals(2, kthSmallestElementInABst.kthSmallest(fromValAndLeftAndRight(1, null, new TreeNode(2)), 2));
    }

    @Test void tree314_2() {
      assertEquals(2, kthSmallestElementInABst.kthSmallest(fromValAndLeftAndRight(3,
        fromValAndLeftAndRight(1, null, new TreeNode(2)),
        new TreeNode(4)),
        2));
    }
}
