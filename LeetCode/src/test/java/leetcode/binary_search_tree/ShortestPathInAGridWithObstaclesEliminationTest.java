package leetcode.binary_search_tree;

import leetcode.breadth_first_search.ShortestPathInAGridWithObstaclesElimination;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShortestPathInAGridWithObstaclesEliminationTest {

  private ShortestPathInAGridWithObstaclesElimination shortestPathInAGridWithObstaclesElimination =
    new ShortestPathInAGridWithObstaclesElimination.Solution();

  @Test
  void grid2() {
    assertEquals(6, shortestPathInAGridWithObstaclesElimination.shortestPath(new int[][]{
      {0, 0, 0},
      {1, 1, 0},
      {0, 0, 0},
      {0, 1, 1},
      {0, 0, 0}
    }, 1));
  }

  @Test
  void gridN1() {
    assertEquals(-1, shortestPathInAGridWithObstaclesElimination.shortestPath(new int[][]{
      {0, 1, 1},
      {1, 1, 1},
      {1, 0, 0}
    }, 1));
  }

  @Test
  void grid13() {
    assertEquals(13, shortestPathInAGridWithObstaclesElimination.shortestPath(new int[][]{
      {0, 1, 0, 0, 0, 1, 0, 0},
      {0, 1, 0, 1, 0, 1, 0, 1},
      {0, 0, 0, 1, 0, 0, 1, 0}
    }, 1));
  }
}