package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumOfMutatedArrayClosestToTargetTest {

  private SumOfMutatedArrayClosestToTarget sumOfMutatedArrayClosestToTarget = new SumOfMutatedArrayClosestToTarget.Solution();

  @Test
  void arr493target10() {
    assertEquals(3, sumOfMutatedArrayClosestToTarget.findBestValue(new int[]{4, 9, 3}, 10));
  }

  @Test
  void arr235target10() {
    assertEquals(5, sumOfMutatedArrayClosestToTarget.findBestValue(new int[]{2, 3, 5}, 10));
  }

  @Test
  void target56803() {
    assertEquals(11361, sumOfMutatedArrayClosestToTarget.findBestValue(new int[]{60864, 25176, 27249, 21296, 20204}, 56803));
  }

  @Test
  void target40876() {
    assertEquals(8175, sumOfMutatedArrayClosestToTarget.findBestValue(new int[]{48772, 52931, 14253, 32289, 75263}, 40876));
  }

  @Test
  void target71237() {
    assertEquals(17422, sumOfMutatedArrayClosestToTarget.findBestValue(new int[]{1547, 83230, 57084, 93444, 70879}, 71237));
  }
}