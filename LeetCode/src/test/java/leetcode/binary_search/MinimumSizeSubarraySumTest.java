package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MinimumSizeSubarraySumTest {

    private MinimumSizeSubarraySum minimumSizeSubarraySum = new MinimumSizeSubarraySum.Solution();

    @Test void s7nums231243output2() {
        assertEquals(2, minimumSizeSubarraySum.minSubArrayLen(7, new int[]{2, 3, 1, 2, 4, 3}));
    }

    @Test void s11nums12345output3() {
        assertEquals(3, minimumSizeSubarraySum.minSubArrayLen(11, new int[]{1, 2, 3, 4, 5}));
    }

    @Test void s15nums12345output5() {
        assertEquals(5, minimumSizeSubarraySum.minSubArrayLen(15, new int[]{1, 2, 3, 4, 5}));
    }

    @Test void s15nums5_1_3_5_10_7_4_9_2_8output2() {
        assertEquals(2,
                minimumSizeSubarraySum.minSubArrayLen(15, new int[]{5, 1, 3, 5, 10, 7, 3, 9, 2, 8}));
    }
}