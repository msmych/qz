package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MedianOfTwoSortedArraysTest {

  private MedianOfTwoSortedArrays medianOfTwoSortedArrays = new MedianOfTwoSortedArrays.Solution();

  @Test
  void nums13nums2median2() {
    assertEquals(2, medianOfTwoSortedArrays.findMedianSortedArrays(new int[]{1, 3}, new int[]{2}));
  }

  @Test
  void nums12nums34median2$5() {
    assertEquals(2.5, medianOfTwoSortedArrays.findMedianSortedArrays(new int[]{1, 2}, new int[]{3, 4}));
  }
}