package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PowXNTest {

    private PowXN powXN = new PowXN.Solution();

    @Test void x2n10output1024() {
        assertEquals(1024.00000, powXN.myPow(2.00000, 10), 0.000001);
    }

    @Test void x2$1n3output9$261() {
        assertEquals(9.26100, powXN.myPow(2.10000, 3), 0.000001);
    }

    @Test void x2nN2output0$25() {
        assertEquals(0.25000, powXN.myPow(2.00000, -2), 0.000001);
    }

    @Test void x0$00001n2147483647output0() {
        assertEquals(0, powXN.myPow(0.00001, 2147483647));
    }

    @Test void xN2n2output4() {
        assertEquals(4.0, powXN.myPow(-2.00000, 2));
    }

    @Test void x1n2147483647output1() {
        assertEquals(1.0, powXN.myPow(1.00000, 2147483647));
    }
}