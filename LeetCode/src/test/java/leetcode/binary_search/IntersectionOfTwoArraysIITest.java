package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class IntersectionOfTwoArraysIITest {

    private IntersectionOfTwoArraysII intersectionOfTwoArraysII = new IntersectionOfTwoArraysII.Solution();

    @Test
    void nums1221nums22output22() {
        assertArrayEquals(new int[]{2, 2}, intersectionOfTwoArraysII.intersect(new int[]{1, 2, 2, 1}, new int[]{2 ,2}));
    }

    @Test void nums495nums94984output49() {
        assertArrayEquals(new int[]{4, 9},
                intersectionOfTwoArraysII.intersect(new int[]{4, 9, 5}, new int[]{9, 4, 9, 8, 4}));
    }
}