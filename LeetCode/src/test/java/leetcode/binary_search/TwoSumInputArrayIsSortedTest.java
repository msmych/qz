package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class TwoSumInputArrayIsSortedTest {

    private TwoSumInputArrayIsSorted twoSum2 = new TwoSumInputArrayIsSorted.Solution();
    private TwoSumInputArrayIsSorted binarySearch = new TwoSumInputArrayIsSorted.BinarySearchSolution();

    @Test void input2_7_11_15output1_2() {
        int[] arr = new int[]{2, 7, 11, 15};
        int[] expected = new int[]{1, 2};
        assertArrayEquals(expected, twoSum2.twoSum(arr, 9));
        assertArrayEquals(expected, binarySearch.twoSum(arr, 9));
    }
}