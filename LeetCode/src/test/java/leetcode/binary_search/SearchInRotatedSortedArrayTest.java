package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SearchInRotatedSortedArrayTest {

  private SearchInRotatedSortedArray searchInRotatedSortedArray = new SearchInRotatedSortedArray.Solution();

  @Test
  void nums4567012target0output4() {
    assertEquals(4, searchInRotatedSortedArray.search(new int[]{4, 5, 6, 7, 0, 1, 2}, 0));
  }

  @Test
  void nums4567012target3outputN1() {
    assertEquals(-1, searchInRotatedSortedArray.search(new int[]{4, 5, 6, 7, 0, 1, 2}, 3));
  }

  @Test
  void nums31target3output0() {
    assertEquals(0, searchInRotatedSortedArray.search(new int[]{3, 1}, 3));
  }

  @Test
  void nums13target3output1() {
    assertEquals(1, searchInRotatedSortedArray.search(new int[]{1, 3}, 3));
  }

  @Test
  void nums351target3output0() {
    assertEquals(0, searchInRotatedSortedArray.search(new int[]{3, 5, 1}, 3));
  }

  @Test
  void nums351target5output1() {
    assertEquals(1, searchInRotatedSortedArray.search(new int[]{3, 5, 1}, 5));
  }
}
