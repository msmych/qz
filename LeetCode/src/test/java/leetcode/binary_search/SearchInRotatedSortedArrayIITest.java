package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SearchInRotatedSortedArrayIITest {

    private SearchInRotatedSortedArrayII searchInRotatedSortedArray = new SearchInRotatedSortedArrayII.Solution();

    @Test void nums2560012target0true() {
        assertTrue(searchInRotatedSortedArray.search(new int[]{2, 5, 6, 0, 0, 1, 2}, 0));
    }

    @Test void nums2560012target3false() {
        assertFalse(searchInRotatedSortedArray.search(new int[]{2, 5, 6, 0, 0, 1, 2}, 3));
    }

    @Test void nums789123456target4true() {
        assertTrue(searchInRotatedSortedArray.search(new int[]{7, 8, 9, 1, 2, 3, 4, 5, 6}, 4));
    }

    @Test void nums13111target3true() {
        assertTrue(searchInRotatedSortedArray.search(new int[]{1, 3, 1, 1, 1}, 3));
    }

    @Test void nums11113target3true() {
        assertTrue(searchInRotatedSortedArray.search(new int[]{1, 1, 1, 1, 3}, 3));
    }

    @Test void nums22231target1true() {
        assertTrue(searchInRotatedSortedArray.search(new int[]{2, 2, 2, 3, 1}, 1));
    }
}