package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrangingCoinsTest {

    private ArrangingCoins arrangingCoins = new ArrangingCoins.Solution();

    @Test void n5() {
        assertEquals(2, arrangingCoins.arrangeCoins(5));
    }

    @Test void n8() {
        assertEquals(3, arrangingCoins.arrangeCoins(8));
    }

    @Test void n_int_max() {
        assertEquals(65535, arrangingCoins.arrangeCoins(Integer.MAX_VALUE));
    }

    @Test void n1() {
        assertEquals(1, arrangingCoins.arrangeCoins(1));
    }

    @Test void n0() {
        assertEquals(0, arrangingCoins.arrangeCoins(0));
    }

    @Test void n3() {
        assertEquals(2, arrangingCoins.arrangeCoins(3));
    }
}