package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FirstBadVersionTest {

    @Test void n5version4() {
        assertEquals(4, new FirstBadVersion(4).firstBadVersion(5));
    }

    @Test void n5version5() {
        assertEquals(5, new FirstBadVersion(5).firstBadVersion(5));
    }

    @Test void n5versionN1() {
        assertEquals(-1, new FirstBadVersion(-1).firstBadVersion(5));
    }

    @Test void n5version1() {
        assertEquals(1, new FirstBadVersion(1).firstBadVersion(5));
    }

    @Test void n3version1() {
        assertEquals(1, new FirstBadVersion(1).firstBadVersion(3));
    }

    @Test
    void n2126753390version1702766719() {
        assertEquals(1702766719, new FirstBadVersion(1702766719).firstBadVersion(2126753390));
    }

    @Test void n3version2() {
        assertEquals(2, new FirstBadVersion(2).firstBadVersion(3));
    }
}