package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FindSmallestCommonElementInAllRowsTest {

  private FindSmallestCommonElementInAllRows findSmallestCommonElementInAllRows = new FindSmallestCommonElementInAllRows.Solution();

  @Test void mat12345() {
    assertEquals(5, findSmallestCommonElementInAllRows.smallestCommonElement(new int[][]{
      {1, 2, 3, 4, 5},
      {2, 4, 5, 8, 10},
      {3, 5, 7, 9, 11},
      {1, 3, 5, 7, 9}
    }));
  }
}
