package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FindTheSmallestDivisorGivenAThresholdTest {

  private FindTheSmallestDivisorGivenAThreshold findTheSmallestDivisorGivenAThreshold =
    new FindTheSmallestDivisorGivenAThreshold.Solution();

  @Test
  void nums1259threshold6() {
    assertEquals(5, findTheSmallestDivisorGivenAThreshold.smallestDivisor(new int[]{1, 2, 5, 9}, 6));
  }

  @Test
  void nums235711threshold11() {
    assertEquals(3, findTheSmallestDivisorGivenAThreshold.smallestDivisor(new int[]{2, 3, 5, 7, 11}, 11));
  }

  @Test
  void nums19threshold5() {
    assertEquals(4, findTheSmallestDivisorGivenAThreshold.smallestDivisor(new int[]{19}, 5));
  }

  @Test
  void nums9_63_69_43_95_11_7threshold302() {
    assertEquals(1, findTheSmallestDivisorGivenAThreshold.smallestDivisor(new int[]{9,63,69,43,95,11,7}, 302));
  }
}