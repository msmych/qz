package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FindKthSmallestPairDistanceTest {

    private FindKthSmallestPairDistance findKthSmallestPairDistance = new FindKthSmallestPairDistance.Solution();

    @Test void nums131k1output0() {
        assertEquals(0, findKthSmallestPairDistance.smallestDistancePair(new int[]{1, 3, 1}, 1));
    }

    @Test void nums111k2output0() {
        assertEquals(0, findKthSmallestPairDistance.smallestDistancePair(new int[]{1, 1, 1}, 2));
    }
}