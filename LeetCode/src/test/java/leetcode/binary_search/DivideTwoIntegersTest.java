package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DivideTwoIntegersTest {

    private DivideTwoIntegers divideTwoIntegers = new DivideTwoIntegers.Solution();

    @Test void dividend10divisor3output3() {
        assertEquals(3, divideTwoIntegers.divide(10, 3));
    }

    @Test void dividend7divisorN3outputN2() {
        assertEquals(-2, divideTwoIntegers.divide(7, -3));
    }

    @Test void dividendN1divisor1outputN1() {
        assertEquals(-1, divideTwoIntegers.divide(-1, 1));
    }

    @Test void dividendN2147483648divisorN1output2147483647() {
        assertEquals(2147483647, divideTwoIntegers.divide(-2147483648, -1));
    }

    @Test
    void dividend2147483647divisor2output1073741823() {
        assertEquals(1073741823, divideTwoIntegers.divide(2147483647, 2));
    }

    @Test void dividendN2147483648divisor1outputN2147483648() {
        assertEquals(-2147483648, divideTwoIntegers.divide(-2147483648, 1));
    }

    @Test void dividendN2147483648divisor2outputN1073741824() {
        assertEquals(-1073741824, divideTwoIntegers.divide(-2147483648, 2));
    }

    @Test void dividendN1067893067divisorN648632514output1() {
        assertEquals(1, divideTwoIntegers.divide(-1067893067, -648632514));
    }
}