package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static utils.TestUtils.assertArrayContainsAll;

class IntersectionOfTwoArraysTest {

    private IntersectionOfTwoArrays intersectionOfTwoArrays = new IntersectionOfTwoArrays.Solution();

    @Test void nums1221nums22output2() {
        assertArrayEquals(new int[]{2}, intersectionOfTwoArrays.intersection(new int[]{1, 2, 2, 1}, new int[]{2, 2}));
    }

    @Test void nums495nums94984output94() {
        assertArrayContainsAll(intersectionOfTwoArrays.intersection(new int[]{4, 9, 5}, new int[]{9, 4, 9, 8, 4}), 4, 9);
    }
}