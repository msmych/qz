package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ValidPerfectSquareTest {

    private ValidPerfectSquare validPerfectSquare = new ValidPerfectSquare.Solution();

    @Test void input16true() {
        assertTrue(validPerfectSquare.isPerfectSquare(16));
    }

    @Test void input14false() {
        assertFalse(validPerfectSquare.isPerfectSquare(14));
    }

    @Test void input808201true() {
        assertTrue(validPerfectSquare.isPerfectSquare(808201));
    }
}