package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static utils.TestUtils.assertListCollectionContainsArrays;

class FindPositiveIntegerSolutionForAGivenEquationTest {

  private FindPositiveIntegerSolutionForAGivenEquation findPositiveIntegerSolutionForAGivenEquation =
    new FindPositiveIntegerSolutionForAGivenEquation.Solution();

  @Test
  void f1z5() {
    assertListCollectionContainsArrays(new Integer[][]{{1, 4}, {2, 3}, {3, 2}, {4, 1}},
      findPositiveIntegerSolutionForAGivenEquation.findSolution(Integer::sum, 5));
  }

  @Test
  void f2z5() {
    assertListCollectionContainsArrays(new Integer[][]{{1, 5}, {5, 1}},
      findPositiveIntegerSolutionForAGivenEquation.findSolution(((x, y) -> x * y), 5));
  }
}
