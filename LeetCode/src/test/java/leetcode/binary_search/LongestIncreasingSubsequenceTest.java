package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestIncreasingSubsequenceTest {

  private LongestIncreasingSubsequence longestIncreasingSubsequence = new LongestIncreasingSubsequence.Solution();

  @Test
  void arr10() {
    assertEquals(4, longestIncreasingSubsequence.lengthOfLIS(new int[]{10, 9, 2, 5, 3, 7, 101, 18}));
  }

  @Test
  void arrN2N1() {
    assertEquals(2, longestIncreasingSubsequence.lengthOfLIS(new int[]{-2, -1}));
  }
}
