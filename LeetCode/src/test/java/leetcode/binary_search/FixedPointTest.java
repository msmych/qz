package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FixedPointTest {

    private FixedPoint fixedPoint = new FixedPoint.Solution();

    @Test void inputN10N5037output3() {
        assertEquals(3, fixedPoint.fixedPoint(new int[]{-10, -5, 0, 3, 7}));
    }

    @Test void input025817output0() {
        assertEquals(0, fixedPoint.fixedPoint(new int[]{0, 2, 5, 8, 17}));
    }

    @Test void inputN10N53479outputN1() {
        assertEquals(-1, fixedPoint.fixedPoint(new int[]{-10, -5, 3, 4, 7, 9}));
    }
}