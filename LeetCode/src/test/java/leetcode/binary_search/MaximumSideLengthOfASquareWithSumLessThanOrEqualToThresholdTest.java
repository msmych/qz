package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaximumSideLengthOfASquareWithSumLessThanOrEqualToThresholdTest {

  private MaximumSideLengthOfASquareWithSumLessThanOrEqualToThreshold maximumSideLengthOfASquareWithSumLessThanOrEqualToThreshold =
    new MaximumSideLengthOfASquareWithSumLessThanOrEqualToThreshold.Solution();

  @Test
  void mat1132432() {
    assertEquals(2, maximumSideLengthOfASquareWithSumLessThanOrEqualToThreshold.maxSideLength(new int[][]{
      {1, 1, 3, 2, 4, 3, 2},
      {1, 1, 3, 2, 4, 3, 2},
      {1, 1, 3, 2, 4, 3, 2}
    }, 4));
  }

  @Test
  void mat22222() {
    assertEquals(0, maximumSideLengthOfASquareWithSumLessThanOrEqualToThreshold.maxSideLength(new int[][]{
      {2, 2, 2, 2, 2},
      {2, 2, 2, 2, 2},
      {2, 2, 2, 2, 2},
      {2, 2, 2, 2, 2},
      {2, 2, 2, 2, 2}
    }, 1));
  }

  @Test
  void mat1111() {
    assertEquals(3, maximumSideLengthOfASquareWithSumLessThanOrEqualToThreshold.maxSideLength(new int[][]{
      {1, 1, 1, 1},
      {1, 0, 0, 0},
      {1, 0, 0, 0},
      {1, 0, 0, 0}
    }, 6));
  }

  @Test
  void mat1870() {
    assertEquals(2, maximumSideLengthOfASquareWithSumLessThanOrEqualToThreshold.maxSideLength(new int[][]{
      {18, 70},
      {61, 1},
      {25, 85},
      {14, 40},
      {11, 96},
      {97, 96},
      {63, 45}
    }, 40184));
  }
}