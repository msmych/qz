package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PeakIndexInAMountainArrayTest {

    private PeakIndexInAMountainArray peakIndexInAMountainArray = new PeakIndexInAMountainArray.Solution();

    @Test void input010output1() {
        assertEquals(1, peakIndexInAMountainArray.peakIndexInMountainArray(new int[]{0, 1, 0}));
    }

    @Test void input0210output1() {
        assertEquals(1, peakIndexInAMountainArray.peakIndexInMountainArray(new int[]{0, 2, 1, 0}));
    }
}