package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FindMinimumInRotatedSortedArrayTest {

    private FindMinimumInRotatedSortedArray findMinimumInRotatedSortedArray =
            new FindMinimumInRotatedSortedArray.Solution();

    @Test void input34512output1() {
        assertEquals(1, findMinimumInRotatedSortedArray.findMin(new int[]{3, 4, 5, 1, 2}));
    }

    @Test void input4567012output0() {
        assertEquals(0, findMinimumInRotatedSortedArray.findMin(new int[]{4, 5, 6, 7, 0, 1, 2}));
    }
}