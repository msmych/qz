package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FindTheDuplicateNumberTest {

  private FindTheDuplicateNumber findTheDuplicateNumber = new FindTheDuplicateNumber.Solution();

  @Test
  void input13422output2() {
    assertEquals(2, findTheDuplicateNumber.findDuplicate(new int[]{1, 3, 4, 2, 2}));
  }

  @Test
  void input31342output3() {
    assertEquals(3, findTheDuplicateNumber.findDuplicate(new int[]{3, 1, 3, 4, 2}));
  }

  @Test
  void input259() {
    assertEquals(9, findTheDuplicateNumber.findDuplicate(new int[]{2, 5, 9, 6, 9, 3, 8, 9, 7, 1}));
  }
}
