package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SearchA2DMatrixTest {

  private SearchA2DMatrix searchA2DMatrix = new SearchA2DMatrix.Solution();

  @Test
  void matrix1357target3true() {
    assertTrue(searchA2DMatrix.searchMatrix(new int[][]{
      {1, 3, 5, 7},
      {10, 11, 16, 20},
      {23, 30, 34, 50}
    }, 3));
  }

  @Test
  void matrix1357target13false() {
    assertFalse(searchA2DMatrix.searchMatrix(new int[][]{
      {1, 3, 5, 7},
      {10, 11, 16, 20},
      {23, 30, 34, 50}
    }, 13));
  }

  @Test
  void matrix11target2false() {
    assertFalse(searchA2DMatrix.searchMatrix(new int[][]{{1, 1}}, 2));
  }
}
