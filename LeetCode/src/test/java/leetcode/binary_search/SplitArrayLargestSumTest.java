package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SplitArrayLargestSumTest {

    private SplitArrayLargestSum splitArrayLargestSum = new SplitArrayLargestSum.Solution();

    @Test void nums7_2_5_10_8m2output18() {
        assertEquals(18, splitArrayLargestSum.splitArray(new int[]{7, 2, 5, 10, 8}, 2));
    }

    @Test void nums10_5_13_4_8_4_5_11_14_9_16_10_20_8m8output25() {
        assertEquals(25, splitArrayLargestSum.splitArray(
                new int[]{10, 5, 13, 4, 8, 4, 5, 11, 14, 9, 16, 10, 20, 8}, 8));
    }
}