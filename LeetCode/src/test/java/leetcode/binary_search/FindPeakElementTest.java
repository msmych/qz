package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static utils.TestUtils.assertEqualsOneOf;

class FindPeakElementTest {

  private FindPeakElement templateII = new FindPeakElement.TemplateIISolution();
  private FindPeakElement templateIII = new FindPeakElement.TemplateIIISolution();

  @Test
  void nums1231output2() {
    assertEquals(2, templateII.findPeakElement(new int[]{1, 2, 3, 1}));
  }

  @Test
  void nums1213564() {
    int[] nums = new int[]{1, 2, 1, 3, 5, 6, 4};
    assertEqualsOneOf(templateII.findPeakElement(nums), 1, 5);
    assertEqualsOneOf(templateIII.findPeakElement(nums), 1, 5);
  }

  @Test
  void nums21() {
    int[] nums = new int[]{2, 1};
    assertEquals(0, templateII.findPeakElement(nums));
    assertEquals(0, templateIII.findPeakElement(nums));
  }
}
