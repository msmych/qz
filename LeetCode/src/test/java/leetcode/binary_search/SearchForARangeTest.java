package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class SearchForARangeTest {

  private SearchForARange searchForARange = new SearchForARange.Solution();

  @Test
  void nums5_7_7_8_8_10taget8output3_4() {
    assertArrayEquals(new int[]{3, 4}, searchForARange.searchRange(new int[]{5, 7, 7, 8, 8, 10}, 8));
  }

  @Test
  void nums5_7_7_8_8_10target6outputN1N1() {
    assertArrayEquals(new int[]{-1, -1}, searchForARange.searchRange(new int[]{5, 7, 7, 8, 8, 10}, 6));
  }

  @Test
  void nums_target0outputN1N1() {
    assertArrayEquals(new int[]{-1, -1}, searchForARange.searchRange(new int[0], 0));
  }

  @Test
  void nums1_3target1output0_0() {
    assertArrayEquals(new int[]{0, 0}, searchForARange.searchRange(new int[]{1, 3}, 1));
  }

  @Test
  void nums3_3_3target3output0_2() {
    assertArrayEquals(new int[]{0, 2}, searchForARange.searchRange(new int[]{3, 3, 3}, 3));
  }
}
