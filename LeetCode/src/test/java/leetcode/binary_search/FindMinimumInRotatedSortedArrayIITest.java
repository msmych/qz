package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FindMinimumInRotatedSortedArrayIITest {

    private FindMinimumInRotatedSortedArray findMinimumInRotatedSortedArray =
            new FindMinimumInRotatedSortedArray.SolutionII();

    @Test void input135output1() {
        assertEquals(1, findMinimumInRotatedSortedArray.findMin(new int[]{1, 3, 5}));
    }

    @Test void input22201output0() {
        assertEquals(0, findMinimumInRotatedSortedArray.findMin(new int[]{2, 2, 2, 0, 1}));
    }

    @Test void input31output1() {
        assertEquals(1, findMinimumInRotatedSortedArray.findMin(new int[]{3, 1}));
    }
}