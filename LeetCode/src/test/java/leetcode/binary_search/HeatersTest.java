package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeatersTest {

    private Heaters heaters = new Heaters.Solution();

    @Test void houses123() {
        assertEquals(1, heaters.findRadius(new int[]{1, 2, 3}, new int[]{2}));
    }

    @Test void houses1234() {
        assertEquals(1, heaters.findRadius(new int[]{1, 2, 3, 4}, new int[]{1, 4}));
    }

    @Test void houses282475249() {
        assertEquals(161834419, heaters.findRadius(
                new int[]{282475249,622650073,984943658,144108930,470211272,101027544,457850878,458777923},
                new int[]{823564440,115438165,784484492,74243042,114807987,137522503,441282327,16531729,823378840,143542612}));
    }

    @Test void houses15() {
        assertEquals(9, heaters.findRadius(new int[]{1, 5}, new int[]{10}));
    }

    @Test void houses1() {
        assertEquals(0, heaters.findRadius(new int[]{1}, new int[]{1, 2, 3, 4}));
    }
}