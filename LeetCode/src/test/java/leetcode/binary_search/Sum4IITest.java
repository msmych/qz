package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Sum4IITest {

  private Sum4II sum4II = new Sum4II.Solution();

  @Test
  void a12bN2N1cN12d02output2() {
    assertEquals(2,
      sum4II.fourSumCount(new int[]{1, 2}, new int[]{-2, -1}, new int[]{-1, 2}, new int[]{0, 2}));
  }

  @Test
  void aN1N1bN11cN11d1N1output6() {
    assertEquals(6,
      sum4II.fourSumCount(new int[]{-1, -1}, new int[]{-1, 1}, new int[]{-1, 1}, new int[]{1, -1}));
  }

  @Test
  void _output0() {
    assertEquals(0, sum4II.fourSumCount(new int[]{}, new int[]{}, new int[]{}, new int[]{}));
  }
}