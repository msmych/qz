package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GuessGameTest {

    @Test void n10pick6() {
        assertEquals(6, new GuessGame(6).guessNumber(10));
    }

    @Test void n2126753390pick1702766719() {
        assertEquals(1702766719, new GuessGame(1702766719).guessNumber(2126753390));
    }
}