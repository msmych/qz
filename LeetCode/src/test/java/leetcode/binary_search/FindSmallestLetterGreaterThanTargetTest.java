package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FindSmallestLetterGreaterThanTargetTest {

    private FindSmallestLetterGreaterThanTarget findSmallestLetterGreaterThanTarget =
            new FindSmallestLetterGreaterThanTarget.Solution();

    @Test
    void letters_cfj_target_a_output_c() {
        assertEquals('c', findSmallestLetterGreaterThanTarget.nextGreatestLetter(new char[]{'c', 'f', 'j'}, 'a'));
    }

    @Test void letters_cfj_target_c_output_f() {
        assertEquals('f', findSmallestLetterGreaterThanTarget.nextGreatestLetter(new char[]{'c', 'f', 'j'}, 'c'));
    }

    @Test void letters_cfj_target_d_output_f() {
        assertEquals('f', findSmallestLetterGreaterThanTarget.nextGreatestLetter(new char[]{'c', 'f', 'j'}, 'd'));
    }

    @Test void letters_cfj_target_g_output_j() {
        assertEquals('j', findSmallestLetterGreaterThanTarget.nextGreatestLetter(new char[]{'c', 'f', 'j'}, 'g'));
    }

    @Test void letters_cfj_target_j_output_c() {
        assertEquals('c', findSmallestLetterGreaterThanTarget.nextGreatestLetter(new char[]{'c', 'f', 'j'}, 'j'));
    }

    @Test void letters_cfj_target_k_output_c() {
        assertEquals('c', findSmallestLetterGreaterThanTarget.nextGreatestLetter(new char[]{'c', 'f', 'j'}, 'k'));
    }
}