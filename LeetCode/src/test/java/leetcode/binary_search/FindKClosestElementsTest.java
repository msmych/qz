package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class FindKClosestElementsTest {

    private FindKClosestElements findKClosestElements = new FindKClosestElements.Solution();

    @Test void input12345k4x3output1234() {
        assertArrayEquals(new Integer[]{1, 2, 3, 4},
                findKClosestElements.findClosestElements(new int[]{1, 2, 3, 4, 5}, 4, 3)
                        .toArray(new Integer[]{}));
    }

    @Test void input12345k4xN1output1234() {
        assertArrayEquals(new Integer[]{1, 2, 3, 4},
                findKClosestElements.findClosestElements(new int[]{1, 2, 3, 4, 5}, 4, -1)
                        .toArray(new Integer[]{}));
    }

    @Test void input0012334778k3x5output334() {
        assertArrayEquals(new Integer[]{3, 3, 4},
                findKClosestElements.findClosestElements(new int[]{0, 0, 1, 2, 3, 3, 4, 7, 7, 8}, 3, 5)
                        .toArray(new Integer[]{}));
    }

    @Test void input0001356788k2x2output13() {
        assertArrayEquals(new Integer[]{1, 3},
                findKClosestElements.findClosestElements(new int[]{0, 0, 0, 1, 3, 5, 6, 7, 8, 8}, 2, 2)
                        .toArray(new Integer[]{}));
    }
}