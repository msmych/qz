package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SqrtXTest {

    private SqrtX sqrtX = new SqrtX.Solution();

    @Test void input4output2() {
        assertEquals(2, sqrtX.mySqrt(4));
    }

    @Test void input8output2() {
        assertEquals(2, sqrtX.mySqrt(8));
    }

    @Test void input8192output90() {
        assertEquals(90, sqrtX.mySqrt(8192));
    }

    @Test void input2147395599output46339() {
        assertEquals(46339, sqrtX.mySqrt(2147395599));
    }
}