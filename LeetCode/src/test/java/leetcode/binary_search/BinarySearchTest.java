package leetcode.binary_search;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinarySearchTest {

    private BinarySearch binarySearch = new BinarySearch.Solution();

    @Test void inputN1_0_3_5_9_12target9output4() {
        assertEquals(4, binarySearch.search(new int[]{-1, 0, 3, 5, 9, 12}, 9));
    }

    @Test void inputN1_0_3_5_9_12target2outputN1() {
        assertEquals(-1, binarySearch.search(new int[]{-1, 0, 3, 5, 9, 12}, 2));
    }
}