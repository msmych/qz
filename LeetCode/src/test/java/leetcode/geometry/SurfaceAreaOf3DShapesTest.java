package leetcode.geometry;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SurfaceAreaOf3DShapesTest {

    private SurfaceAreaOf3DShapes surfaceAreaOf3DShapes = new SurfaceAreaOf3DShapes.Solution();

    @Test void input2output10() {
        assertEquals(10, surfaceAreaOf3DShapes.surfaceArea(new int[][]{{2}}));
    }

    @Test
    void input12_34output34() {
        assertEquals(34, surfaceAreaOf3DShapes.surfaceArea(new int[][]{{1, 2}, {3, 4}}));
    }

    @Test void input10_02output16() {
        assertEquals(16, surfaceAreaOf3DShapes.surfaceArea(new int[][]{{1, 0}, {0, 2}}));
    }

    @Test void input111_101_111output32() {
        assertEquals(32, surfaceAreaOf3DShapes.surfaceArea(new int[][]{{1, 1, 1}, {1, 0, 1}, {1, 1, 1}}));
    }

    @Test void input222_212_222output46() {
        assertEquals(46, surfaceAreaOf3DShapes.surfaceArea(new int[][]{{2, 2, 2}, {2, 1, 2}, {2, 2, 2}}));
    }
}