package leetcode.geometry;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinimumTimeVisitingAllPointsTest {

  private MinimumTimeVisitingAllPoints minimumTimeVisitingAllPoints = new MinimumTimeVisitingAllPoints.Solution();

  @Test
  void points1134N10() {
    assertEquals(7, minimumTimeVisitingAllPoints.minTimeToVisitAllPoints(new int[][]{{1, 1}, {3, 4}, {-1, 0}}));
  }

  @Test
  void points32N22() {
    assertEquals(5, minimumTimeVisitingAllPoints.minTimeToVisitAllPoints(new int[][]{{3, 2,}, {-2, 2}}));
  }
}