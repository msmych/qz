package leetcode.geometry;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CheckIfItIsAStraightLineTest {

  private CheckIfItIsAStraightLine checkIfItIsAStraightLine = new CheckIfItIsAStraightLine.Solution();

  @Test
  void line123() {
    assertTrue(checkIfItIsAStraightLine.checkStraightLine(new int[][]{{1,2},{2,3},{3,4},{4,5},{5,6},{6,7}}));
  }

  @Test
  void line1122() {
    assertFalse(checkIfItIsAStraightLine.checkStraightLine(new int[][]{{1,1},{2,2},{3,4},{4,5},{5,6},{7,7}}));
  }
}
