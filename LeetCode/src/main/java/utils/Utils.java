package utils;

import java.util.List;

public class Utils {

    public static void printMatrix(char[][] matrix) {
        System.out.println();
        for (char[] row : matrix) {
            System.out.println();
            for (char c : row) {
                System.out.print(c + " ");
            }
        }
        System.out.println();
    }

    public static void printMatrix(int[][] matrix) {
        System.out.println();
        if (matrix.length == 0)
            System.out.println("[]");
        int maxLength = 0;
        for (int[] row : matrix) {
            for (int j = 0; j < matrix[0].length; j++) {
                int length = String.valueOf(row[j]).length();
                if (length > maxLength)
                    maxLength = length;
            }
        }
        for (int[] row : matrix) {
            System.out.println();
            for (int n : row) {
                for (int length = String.valueOf(n).length(); length < maxLength; length++) {
                    System.out.print(" ");
                }
                System.out.print(n + " ");
            }
        }
        System.out.println();
    }

    public static <T> void printMatrix(List<List<T>> matrix) {
        System.out.println();
        int maxLength = 0;
        for (List<T> row : matrix) {
            for (T t : row) {
                int length = t.toString().length();
                if (length > maxLength)
                    maxLength = length;
            }
        }
        for (List<T> row : matrix) {
            System.out.println();
            for (T t : row) {
                for (int length = t.toString().length(); length < maxLength; length++) {
                    System.out.print(" ");
                }
                System.out.print(t.toString() + " ");
            }
        }
        System.out.println();
    }
}
