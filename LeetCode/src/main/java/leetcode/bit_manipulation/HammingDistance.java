package leetcode.bit_manipulation;

public interface HammingDistance {

    int hammingDistance(int x, int y);

    class Solution implements HammingDistance {

        @Override
        public int hammingDistance(int x, int y) {
            int dist = 0;
            for (long i = 1; i <= x || i <= y; i *= 2) {
                if ((x & i) != (y & i)) dist++;
            }
            return dist;
        }
    }
}
