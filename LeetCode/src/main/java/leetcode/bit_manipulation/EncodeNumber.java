package leetcode.bit_manipulation;

public interface EncodeNumber {

  String encode(int num);

  class Solution implements EncodeNumber {
    @Override
    public String encode(int num) {
      return Integer.toString(num + 1, 2).substring(1);
    }
  }
}
