package leetcode.bit_manipulation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface RepeatedDnaSequences {

    List<String> findRepeatedDnaSequences(String s);

    class Solution implements RepeatedDnaSequences {

        @Override
        public List<String> findRepeatedDnaSequences(String s) {
            Map<String, Integer> map = new HashMap<>();
            for (int i = 0; i <= s.length() - 10; i++) {
                String chunk = s.substring(i, i + 10);
                map.put(chunk, map.getOrDefault(chunk, 0) + 1);
            }
            return map.entrySet().stream()
                    .filter(e -> e.getValue() > 1)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());
        }
    }
}
