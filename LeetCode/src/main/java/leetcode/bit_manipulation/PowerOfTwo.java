package leetcode.bit_manipulation;

public interface PowerOfTwo {

    boolean isPowerOfTwo(int n);

    class Solution implements PowerOfTwo {

        @Override
        public boolean isPowerOfTwo(int n) {
            return n > 0 && 1073741824 % n == 0;
        }
    }
}
