package leetcode.bit_manipulation;

import java.util.*;
import java.util.stream.Collectors;

public interface NumberOfValidWordsForEachPuzzle {

    List<Integer> findNumOfValidWords(String[] words, String[] puzzles);

    class Solution implements NumberOfValidWordsForEachPuzzle {

        private static class Puzzle {
            char first;
            final Set<Character> letters;

            Puzzle(String puzzle) {
                first = puzzle.charAt(0);
                letters = puzzle.chars().mapToObj(l -> (char) l).collect(Collectors.toSet());
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Puzzle puzzle = (Puzzle) o;
                return first == puzzle.first &&
                    Objects.equals(letters, puzzle.letters);
            }

            @Override
            public int hashCode() {
                return Objects.hash(first, letters);
            }
        }

        private final Map<Puzzle, Integer> cache = new HashMap<>();

        @Override
        public List<Integer> findNumOfValidWords(String[] words, String[] puzzles) {
            Map<Set<Character>, Integer> wordMap = new HashMap<>();
            for (String word : words) {
                Set<Character> wordSet = word.chars().mapToObj(c -> (char) c).collect(Collectors.toSet());
                wordMap.merge(wordSet, 1, Integer::sum);
            }
            List<Integer> valid = new ArrayList<>();
            for (String p : puzzles) {
                Puzzle puzzle = new Puzzle(p);
                if (cache.containsKey(puzzle)) {
                    valid.add(cache.get(puzzle));
                }
                int sum = wordMap.entrySet().stream()
                    .filter(e -> e.getKey().contains(puzzle.first))
                    .filter(e -> puzzle.letters.containsAll(e.getKey()))
                    .mapToInt(Map.Entry::getValue)
                    .sum();
                valid.add(sum);
                cache.put(puzzle, sum);
            }
            return valid;
        }
    }
}
