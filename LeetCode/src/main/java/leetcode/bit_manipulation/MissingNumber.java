package leetcode.bit_manipulation;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface MissingNumber {

    int missingNumber(int[] nums);

    class Solution implements MissingNumber {

        @Override
        public int missingNumber(int[] nums) {
            Set<Integer> set = IntStream.range(0, nums.length + 1).boxed().collect(Collectors.toSet());
            Arrays.stream(nums).forEach(set::remove);
            return set.iterator().next();
        }
    }

    class GaussSolution implements MissingNumber {

        @Override
        public int missingNumber(int[] nums) {
            return nums.length * (nums.length + 1) / 2 - Arrays.stream(nums).sum();
        }
    }
}
