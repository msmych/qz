package leetcode.bit_manipulation;

import java.util.HashMap;
import java.util.Map;

public interface FindTheDifference {

  char findTheDifference(String s, String t);

  class Solution implements FindTheDifference {
    @Override
    public char findTheDifference(String s, String t) {
      Map<Character, Integer> sMap = occurrences(s);
      Map<Character, Integer> tMap = occurrences(t);
      for (Map.Entry<Character, Integer> tEntry : tMap.entrySet()) {
        Character c = tEntry.getKey();
        if (!sMap.containsKey(c) || sMap.get(c) != tEntry.getValue()) {
          return c;
        }
      }
      throw new IllegalArgumentException();
    }

    private Map<Character, Integer> occurrences(String s) {
      Map<Character, Integer> occurrences = new HashMap<>();
      for (char c : s.toCharArray()) {
        occurrences.merge(c, 1, Integer::sum);
      }
      return occurrences;
    }
  }
}
