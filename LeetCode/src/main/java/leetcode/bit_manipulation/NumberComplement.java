package leetcode.bit_manipulation;

public interface NumberComplement {

    int findComplement(int num);

    class Solution implements NumberComplement {
        @Override
        public int findComplement(int num) {
            int complement = 0;
            for (long i = 1; i <= num; i *= 2) {
                if ((num & i) == 0) {
                    complement |= i;
                }
            }
            return complement;
        }
    }
}
