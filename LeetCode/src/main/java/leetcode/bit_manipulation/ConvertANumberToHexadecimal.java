package leetcode.bit_manipulation;

public interface ConvertANumberToHexadecimal {

    String toHex(int num);

    class Solution implements ConvertANumberToHexadecimal {

        @Override
        public String toHex(int num) {
            long n = num;
            if (n == 0) {
                return "0";
            } else if (n < 0) {
                n += ((long) Integer.MAX_VALUE + 1) * 2;
            }
            StringBuilder sb = new StringBuilder();
            while (n > 0) {
                sb.insert(0, decToHex(n % 16));
                n /= 16;
            }
            return sb.toString();
        }

        private char decToHex(long n) {
            return n < 10
                   ? (char) ('0' + n)
                   : (char) ('a' + n - 10);
        }
    }

}
