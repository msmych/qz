package leetcode.bit_manipulation;

public interface SumOfTwoIntegers {

  int getSum(int a, int b);

  class Solution implements SumOfTwoIntegers {

    @Override
    public int getSum(int a, int b) {
      if (a == 0) {
        return b;
      } else if (b == 0) {
        return a;
      }
      while (b != 0) {
        int carry = a & b;
        a = a ^ b;
        b = carry << 1;
      }
      return a;
    }
  }
}
