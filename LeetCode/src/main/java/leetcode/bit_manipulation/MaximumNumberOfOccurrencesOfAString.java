package leetcode.bit_manipulation;

import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.stream;
import static java.util.Comparator.naturalOrder;
import static java.util.stream.IntStream.rangeClosed;

public interface MaximumNumberOfOccurrencesOfAString {

  int maxFreq(String s, int maxLetters, int minSize, int maxSize);

  class Solution implements MaximumNumberOfOccurrencesOfAString {

    private final Map<String, Boolean> checked = new HashMap<>();
    private final Map<String, Integer> occurrences = new HashMap<>();

    private int maxLetters;

    @Override
    public int maxFreq(String s, int maxLetters, int minSize, int maxSize) {
      this.maxLetters = maxLetters;
      rangeClosed(0, s.length() - minSize)
        .mapToObj(i -> s.substring(i, i + minSize))
        .filter(this::fits)
        .forEach(sub -> occurrences.merge(sub, 1, Integer::sum));
      return occurrences.values().stream().max(naturalOrder()).orElse(0);
    }

    private boolean fits(String s) {
      if (checked.containsKey(s)) {
        return checked.get(s);
      }
      int[] letters = new int[26];
      s.chars().map(c -> c - 'a').forEach(c -> letters[c]++);
      boolean fits = stream(letters).filter(c -> c > 0).count() <= maxLetters;
      checked.put(s, fits);
      return fits;
    }
  }
}
