package leetcode.bit_manipulation;

import java.util.*;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.IntStream.range;

public interface MaximumLengthOfAConcatenatedStringWithUniqueCharacters {

  int maxLength(List<String> arr);

  class Solution implements MaximumLengthOfAConcatenatedStringWithUniqueCharacters {

    private final Map<Integer, Integer> map = range(0, 26).boxed().collect(toMap(a -> a + 'a', a -> 1 << a));
    private final Map<List<Integer>, Integer> cache = new HashMap<>();

    @Override
    public int maxLength(List<String> arr) {
      if (arr.stream().allMatch(s -> s.length() == 1)) {
        return countBits(arr.stream().map(a -> 1 << a.charAt(0) - 'a').reduce((a, b) -> a | b).orElse(0));
      }
      return countBits(maxUnionLength(0, arr.stream()
        .filter(s -> s.length() == s.chars().distinct().count())
        .map(s -> s.chars().map(map::get).reduce((a, b) -> a | b).orElse(0))
        .collect(toSet())));
    }

    private int maxUnionLength(int union, Set<Integer> parts) {
      if (countBits(union) == 26 || parts.isEmpty()) {
        return union;
      }
      List<Integer> cacheKey = new ArrayList<>(parts);
      cacheKey.add(union);
      if (cache.containsKey(cacheKey)) {
        return cache.get(cacheKey);
      }
      int max = union;
      int maxBitsCount = countBits(max);
      for (int part : parts) {
        if ((union & part) > 0) {
          continue;
        }
        int next = maxUnionLength(union | part, parts.stream().filter(bitmap -> bitmap != part).collect(toSet()));
        int nextBitsCount = countBits(next);
        if (nextBitsCount == 26) {
          return next;
        }
        if (nextBitsCount > maxBitsCount) {
          max = next;
          maxBitsCount = nextBitsCount;
        }
      }
      cache.put(cacheKey, max);
      return max;
    }

    private int countBits(int n) {
      return (int) range(0, 26).map(a -> 1 << a).filter(bit -> (n & bit) > 0).count();
    }
  }
}
