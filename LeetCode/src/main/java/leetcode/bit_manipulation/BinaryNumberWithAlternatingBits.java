package leetcode.bit_manipulation;

public interface BinaryNumberWithAlternatingBits {

    boolean hasAlternatingBits(int n);

    class Solution implements BinaryNumberWithAlternatingBits {

        @Override
        public boolean hasAlternatingBits(int n) {
            Boolean lastBit = null;
            for (long bit = 1; bit <= n; bit *= 2) {
                Boolean currentBit = (n & bit) > 0;
                if (lastBit != null && lastBit == currentBit) return false;
                lastBit = currentBit;
            }
            return true;
        }
    }
}
