package leetcode.bit_manipulation;

import static java.util.Arrays.stream;
import static java.util.stream.IntStream.rangeClosed;

public interface XorQueriesOfASubarray {

  int[] xorQueries(int[] arr, int[][] queries);

  class Solution implements XorQueriesOfASubarray {
    @Override
    public int[] xorQueries(int[] arr, int[][] queries) {
      return stream(queries)
        .mapToInt(query -> rangeClosed(query[0], query[1])
          .map(i -> arr[i])
          .reduce((a, b) -> a ^ b)
          .getAsInt())
        .toArray();
    }
  }
}
