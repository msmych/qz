package leetcode.bit_manipulation;

import java.util.HashSet;
import java.util.Set;

public interface BitwiseAndOfNumbersRange {

    int rangeBitwiseAnd(int m, int n);

    class Solution implements BitwiseAndOfNumbersRange {

        @Override
        public int rangeBitwiseAnd(int m, int n) {
            if (m < n / 2) return 0;
            int and = m & n;
            Set<Integer> bits = new HashSet<>();
            for (long bit = 1; bit <= and; bit *= 2) {
                if ((and & bit) > 0) bits.add((int) bit);
            }
            for (long i = m; i <= n; i++) {
                if (bits.isEmpty()) return 0;
                Set<Integer> toRemove = new HashSet<>();
                for (int bit : bits) {
                    if ((bit & i) == 0) toRemove.add(bit);
                }
                bits.removeAll(toRemove);
            }
            return bits.stream().reduce((b1, b2) -> b1 | b2).orElse(0);
        }
    }
}
