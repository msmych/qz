package leetcode.bit_manipulation;

public interface PrimeNumberOfSetBitsInBinaryRepresentation {

    int countPrimeSetBits(int L, int R);

    class Solution implements PrimeNumberOfSetBitsInBinaryRepresentation {

        @Override
        public int countPrimeSetBits(int L, int R) {
            int primes = 0;
            for (int n = L; n <= R; n++) {
                int bits = 0;
                for (int bit = 1; bit <= n; bit *= 2) {
                    if ((n & bit) > 0)
                        bits++;
                }
                boolean isPrime = bits != 1;
                for (int d = 2; d <= bits / 2; d++) {
                    if (bits % d == 0)
                        isPrime = false;
                }
                if (isPrime)
                    primes++;
            }
            return primes;
        }
    }
}
