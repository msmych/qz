package leetcode.bit_manipulation;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public interface BinaryWatch {
  List<String> readBinaryWatch(int num);

  class Solution implements BinaryWatch {

    @Override
    public List<String> readBinaryWatch(int num) {
      return times(0, num, 512).stream()
        .map(this::timeFromBinary)
        .collect(Collectors.toList());
    }

    private Set<Integer> times(int time, int num, int bit) {
      if (bit == 0 && num > 0 || num == 0 && (time >= 768 || time % 64 >= 60)) {
        return Collections.emptySet();
      }
      if (num == 0) {
        return Collections.singleton(time);
      }
      Set<Integer> times = new HashSet<>();
      times.addAll(times(time | bit, num - 1, bit / 2));
      times.addAll(times(time, num, bit / 2));
      return times;
    }

    private String timeFromBinary(int binary) {
      int hours = binary / 64;
      String minutes = String.valueOf(binary % 64);
      if (minutes.length() < 2) {
        minutes = '0' + minutes;
      }
      return hours + ":" + minutes;
    }
  }
}
