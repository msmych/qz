package leetcode.bit_manipulation;

import java.util.*;

public interface LetterCasePermutation {

    List<String> letterCasePermutation(String S);

    class Solution implements LetterCasePermutation {

        @Override
        public List<String> letterCasePermutation(String S) {
            if (S.isEmpty())
                return Collections.singletonList("");
            return new ArrayList<>(nextPermutations(S));
        }

        private Set<String> nextPermutations(String s) {
            if (s.isEmpty())
                return new HashSet<>();
            Set<String> permutations = new HashSet<>();
            if (s.length() == 1) {
                permutations.add(s.toUpperCase());
                permutations.add(s.toLowerCase());
                return permutations;
            }
            for (String next : nextPermutations(s.substring(1))) {
                char c = s.charAt(0);
                permutations.add(Character.toUpperCase(c) + next);
                permutations.add(Character.toLowerCase(c) + next);
            }
            return permutations;
        }
    }
}
