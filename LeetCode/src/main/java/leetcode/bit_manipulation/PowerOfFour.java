package leetcode.bit_manipulation;

public interface PowerOfFour {

    boolean isPowerOfFour(int num);

    class Solution implements PowerOfFour {

        @Override
        public boolean isPowerOfFour(int num) {
            return num > 0 && Integer.toString(num, 4).matches("^10*$");
        }
    }
}
