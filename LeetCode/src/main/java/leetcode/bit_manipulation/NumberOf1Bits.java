package leetcode.bit_manipulation;

public interface NumberOf1Bits {

    int hammingWeight(int n);

    class Solution implements NumberOf1Bits {

        @Override
        public int hammingWeight(int n) {
            int ones = 0;
            while (n != 0) {
                ones++;
                n &= (n - 1);
            }
            return ones;
        }
    }
}
