package leetcode.bit_manipulation;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static java.util.Optional.empty;

public interface MaximumScoreWordsFormedByLetters {

  int maxScoreWords(String[] words, char[] letters, int[] score);

  class Solution implements MaximumScoreWordsFormedByLetters {

    private static class WordsLetters {
      Map<String, Integer> words;
      Map<Character, Integer> occurrences;

      WordsLetters(Map<String, Integer> words, Map<Character, Integer> occurrences) {
        this.occurrences = occurrences;
        this.words = words;
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WordsLetters that = (WordsLetters) o;
        return Objects.equals(occurrences, that.occurrences) &&
          Objects.equals(words, that.words);
      }

      @Override
      public int hashCode() {
        return Objects.hash(occurrences, words);
      }
    }

    private final Map<WordsLetters, Integer> cache = new HashMap<>();

    private int[] score;

    @Override
    public int maxScoreWords(String[] words, char[] letters, int[] score) {
      this.score = score;
      return nextMax(wordOccurrences(words), occurrences(letters));
    }

    private int nextMax(Map<String, Integer> words, Map<Character, Integer> occurrences) {
      int max = 0;
      WordsLetters wordsLetters = new WordsLetters(words, occurrences);
      if (cache.containsKey(wordsLetters)) {
        return cache.get(wordsLetters);
      }
      for (String word : words.keySet()) {
        Optional<Map<Character, Integer>> nextOccurrences = canFormWord(word, occurrences);
        if (nextOccurrences.isPresent()) {
          Map<String, Integer> nextWords = new HashMap<>(words);
          if (nextWords.get(word) == 1) {
            nextWords.remove(word);
          } else {
            nextWords.merge(word, -1, Integer::sum);
          }
          int next = scores(word) + nextMax(nextWords, nextOccurrences.get());
          if (next > max) {
            max = next;
          }
        }
      }
      cache.put(wordsLetters, max);
      return max;
    }

    private int scores(String word) {
      return word.chars().map(c -> score[c - 'a']).sum();
    }

    private Optional<Map<Character, Integer>> canFormWord(String word, Map<Character, Integer> occurrences) {
      Map<Character, Integer> map = new HashMap<>(occurrences);
      for (char c : word.toCharArray()) {
        if (!map.containsKey(c) || map.get(c) < 1) {
          return empty();
        } else {
          map.merge(c, -1, Integer::sum);
        }
      }
      return Optional.of(map);
    }

    private Map<String, Integer> wordOccurrences(String[] words) {
      Map<String, Integer> map = new HashMap<>();
      for (String word : words) {
        map.merge(word, 1, Integer::sum);
      }
      return map;
    }

    private Map<Character, Integer> occurrences(char[] letters) {
      Map<Character, Integer> map = new HashMap<>();
      for (char letter : letters) {
        map.merge(letter, 1, Integer::sum);
      }
      return map;
    }
  }
}
