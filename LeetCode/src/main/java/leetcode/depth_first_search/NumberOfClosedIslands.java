package leetcode.depth_first_search;

import java.util.HashSet;
import java.util.Set;

public interface NumberOfClosedIslands {

  int closedIsland(int[][] grid);

  class Solution implements NumberOfClosedIslands {

    private final Set<Integer> visited = new HashSet<>();
    private final Set<Integer> localVisited = new HashSet<>();
    private int[][] grid;
    private int islands = 0;

    @Override
    public int closedIsland(int[][] grid) {
      if (grid.length == 0) {
        return 0;
      }
      this.grid = grid;
      for (int i = 0; i < grid.length; i++) {
        for (int j = 0; j < grid[i].length; j++) {
          if (grid[i][j] == 0 && !visited.contains(1000*i + j)) {
            if (isIsland(i, j)) {
              islands++;
            }
            visited.addAll(localVisited);
            localVisited.clear();
          }
        }
      }
      return islands;
    }

    private boolean isIsland(int i, int j) {
      if (i == 0 || i == grid.length - 1 || j == 0 || j == grid[0].length - 1) {
        return false;
      }
      int cell = 1000*i + j;
      if (localVisited.contains(cell)) {
        return true;
      }
      localVisited.add(cell);
      if (grid[i - 1][j] != 1 && !isIsland(i - 1, j)) {
        return false;
      }
      if (grid[i + 1][j] != 1 && !isIsland(i + 1, j)) {
        return false;
      }
      if (grid[i][j - 1] != 1 && !isIsland(i, j - 1)) {
        return false;
      }
      if (grid[i][j + 1] != 1 && !isIsland(i, j + 1)) {
        return false;
      }
      return true;
    }
  }
}
