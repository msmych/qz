package leetcode.depth_first_search;

import leetcode.tree.TreeNode;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface BinaryTreePaths {

  List<String> binaryTreePaths(TreeNode root);

  class Solution implements BinaryTreePaths {
    @Override
    public List<String> binaryTreePaths(TreeNode root) {
      if (root == null) {
        return Collections.emptyList();
      }
      String val = String.valueOf(root.val);
      if (root.left == null && root.right == null) {
        return Collections.singletonList(val);
      }
      return Stream.of(binaryTreePaths(root.left), binaryTreePaths(root.right))
        .flatMap(Collection::stream)
        .map(path -> val + "->" + path)
        .collect(Collectors.toList());
    }
  }
}
