package leetcode.depth_first_search;

import leetcode.tree.TreeNode;

public interface MaximumDifferenceBetweenNodeAndAncestor {

    int maxAncestorDiff(TreeNode root);

    class Solution implements MaximumDifferenceBetweenNodeAndAncestor {

        @Override
        public int maxAncestorDiff(TreeNode root) {
            return diff(root, null, null);
        }

        private int diff(TreeNode root, Integer min, Integer max) {
            if (root == null) return -1;
            int diff = min == null || max == null
                    ? -1
                    : Math.max(Math.abs(root.val - min), Math.abs(max - root.val));
            if (min == null || root.val < min)
                min = root.val;
            if (max == null || root.val > max)
                max = root.val;
            int nextDiff = Math.max(diff(root.left, min, max), diff(root.right, min, max));
            return Math.max(diff, nextDiff);
        }
    }
}
