package leetcode.depth_first_search;

import java.util.*;

public interface NumberOfEnclaves {

    int numEnclaves(int[][] A);

    class Solution implements NumberOfEnclaves {

        private class Point {
            int i, j;

            Point(int i, int j) {
                this.i = i;
                this.j = j;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Point point = (Point) o;
                return i == point.i &&
                        j == point.j;
            }

            @Override
            public int hashCode() {
                return Objects.hash(i, j);
            }
        }

        private final Map<Point, Boolean> cache = new HashMap<>();

        private Set<Point> visited = new HashSet<>();
        private int[][] a;

        @Override
        public int numEnclaves(int[][] A) {
            this.a = A;
            int num = 0;
            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < a[0].length; j++) {
                    visited = new HashSet<>();
                    if (a[i][j] == 1 && isEnclave(i, j)) {
                        cache.put(new Point(i, j), true);
                        num++;
                    } else cache.put(new Point(i, j), false);
                }
            }
            return num;
        }

        private boolean isEnclave(int i, int j) {
            Point point = new Point(i, j);
            visited.add(point);
            if (i < 0 || j < 0 || i >= a.length || j >= a[0].length)
                return false;
            if (a[i][j] == 0)
                return true;
            if (cache.containsKey(point))
                return cache.get(point);
            boolean isEnclave = true;
            if (!visited.contains(new Point(i, j + 1)))
                isEnclave = isEnclave(i, j + 1);
            if (isEnclave && !visited.contains(new Point(i, j - 1)))
                isEnclave = isEnclave(i, j - 1);
            if (isEnclave && !visited.contains(new Point(i + 1, j)))
                isEnclave = isEnclave(i + 1, j);
            if (isEnclave && !visited.contains(new Point(i - 1, j)))
                isEnclave = isEnclave(i - 1, j);
            return isEnclave;
        }
    }
}
