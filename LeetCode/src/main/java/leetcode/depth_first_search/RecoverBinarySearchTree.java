package leetcode.depth_first_search;

import leetcode.tree.TreeNode;

public interface RecoverBinarySearchTree {

  void recoverTree(TreeNode root);

  class Solution implements RecoverBinarySearchTree {

    private TreeNode first;
    private TreeNode second;
    private TreeNode previous;

    @Override
    public void recoverTree(TreeNode root) {
      traverse(root);
      int val = first.val;
      first.val = second.val;
      second.val = val;
    }

    private void traverse(TreeNode node) {
      if (node == null) {
        return;
      }
      traverse(node.left);
      if (first == null && previous != null && previous.val >= node.val) {
        first = previous;
      }
      if (first != null && previous != null && previous.val >= node.val) {
        second = node;
      }
      previous = node;
      traverse(node.right);
    }
  }
}
