package leetcode.depth_first_search;

import leetcode.tree.TreeNode;

public interface DeepestLeavesSum {

  int deepestLeavesSum(TreeNode root);

  class Solution implements DeepestLeavesSum {

    private int maxDepth = 0;
    private int sum = 0;

    @Override
    public int deepestLeavesSum(TreeNode root) {
      next(root, 0);
      return sum;
    }

    private void next(TreeNode node, int depth) {
      if (node == null) {
        return;
      }
      if (node.left == null && node.right == null) {
        if (depth == maxDepth) {
          sum += node.val;
        } else if (depth > maxDepth) {
          maxDepth = depth;
          sum = node.val;
        }
        return;
      }
      next(node.left, depth + 1);
      next(node.right, depth + 1);
    }
  }
}
