package leetcode.depth_first_search;

import leetcode.tree.TreeNode;

import java.util.ArrayDeque;
import java.util.Deque;

public interface SameTree {

    boolean isSameTree(TreeNode p, TreeNode q);

    class Solution implements SameTree {

        @Override
        public boolean isSameTree(TreeNode p, TreeNode q) {
            if (p == null) return q == null;
            if (q == null) return false;
            if (p.val != q.val) return false;
            return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
        }
    }

    class IterativeSolution implements SameTree {

        @Override
        public boolean isSameTree(TreeNode p, TreeNode q) {
            if (p == null && q == null) return true;
            if (notEqualNodes(p, q)) return false;
            Deque<TreeNode> deqp = new ArrayDeque<>();
            Deque<TreeNode> deqq = new ArrayDeque<>();
            deqp.addLast(p);
            deqq.addLast(q);
            while (!deqp.isEmpty()) {
                p = deqp.removeFirst();
                q = deqq.removeFirst();
                if (notEqualNodes(p, q)) return false;
                if (p != null) {
                    if (notEqualNodes(p.left, q.left)) return false;
                    if (p.left != null) {
                        deqp.addLast(p.left);
                        deqq.addLast(q.left);
                    }
                    if (notEqualNodes(p.right, q.right)) return false;
                    if (p.right != null) {
                        deqp.addLast(p.right);
                        deqq.addLast(q.right);
                    }
                }
            }
            return true;
        }

        private boolean notEqualNodes(TreeNode p, TreeNode q) {
            if (p == null && q == null) return false;
            if (p == null || q == null) return true;
            return p.val != q.val;
        }
    }
}
