package leetcode.depth_first_search;

import java.util.*;

public interface DeleteTreeNodes {

  int deleteTreeNodes(int nodes, int[] parent, int[] value);

  class Solution implements DeleteTreeNodes {

    private static class Node {
      int val;
      Set<Node> children = new HashSet<>();

      Node(int val) {
        this.val = val;
      }
    }

    private int[] values;
    private int[] parents;

    @Override
    public int deleteTreeNodes(int nodes, int[] parent, int[] value) {
      values = value;
      parents = parent;
      Node node = nextNode(0);
      if (removeZeroSumSubtrees(node) == 0) {
        return 0;
      }
      return countNodes(node);
    }

    private Node nextNode(int index) {
      Node node = new Node(values[index]);
      for (int i = 0; i < parents.length; i++) {
        if (parents[i] == index) {
          node.children.add(nextNode(i));
        }
      }
      return node;
    }

    private int removeZeroSumSubtrees(Node node) {
      int sum = node.val;
      for (Iterator<Node> i = node.children.iterator(); i.hasNext(); ) {
        Node child = i.next();
        int childSum = removeZeroSumSubtrees(child);
        if (childSum == 0) {
          i.remove();
        } else {
          sum += childSum;
        }
      }
      return sum;
    }

    private int countNodes(Node node) {
      return 1 + node.children.stream().mapToInt(this::countNodes).sum();
    }
  }
}
