package leetcode.depth_first_search;

import java.util.*;

public interface ColoringABorder {

    int[][] colorBorder(int[][] grid, int r0, int c0, int color);

    class Solution implements ColoringABorder {

        private class Point {
            int i, j;

            public Point(int i, int j) {
                this.i = i;
                this.j = j;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Point point = (Point) o;
                return i == point.i &&
                        j == point.j;
            }

            @Override
            public int hashCode() {
                return Objects.hash(i, j);
            }
        }

        private int[][] grid;
        private int initialColor;
        private final Set<Point> visited = new HashSet<>();
        private final Set<Point> border = new HashSet<>();

        @Override
        public int[][] colorBorder(int[][] grid, int r0, int c0, int color) {
            if (grid.length == 0) return grid;
            this.grid = grid;
            initialColor = grid[r0][c0];
            findBoard(r0, c0);
            border.forEach(p -> grid[p.i][p.j] = color);
            return this.grid;
        }

        private boolean findBoard(int i, int j) {
            if (i < 0 || i >= grid.length || j < 0 || j >= grid[0].length ||
                    grid[i][j] != initialColor)
                return true;
            Point point = new Point(i, j);
            if (visited.contains(point)) return false;
            visited.add(point);
            boolean isBoard = findBoard(i + 1, j);
            isBoard |= findBoard(i - 1, j);
            isBoard |= findBoard(i, j + 1);
            isBoard |= findBoard(i, j - 1);
            if (isBoard) border.add(point);
            return false;
        }
    }
}
