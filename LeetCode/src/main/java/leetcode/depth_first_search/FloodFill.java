package leetcode.depth_first_search;

import java.util.*;

public interface FloodFill {

    int[][] floodFill(int[][] image, int sr, int sc, int newColor);

    class Solution implements FloodFill {

        class Point {
            int r;
            int c;

            Point(int r, int c) {
                this.r = r;
                this.c = c;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Point point = (Point) o;
                return r == point.r &&
                        c == point.c;
            }

            @Override
            public int hashCode() {
                return Objects.hash(r, c);
            }
        }

        private final Queue<Point> points = new LinkedList<>();
        private final Set<Point> visited = new HashSet<>();

        private int[][] image;

        @Override
        public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
            this.image = image;
            Point ps = new Point(sr, sc);
            points.offer(ps);
            visited.add(ps);
            int color = image[ps.r][ps.c];
            while (!points.isEmpty()) {
                int size = points.size();
                for (int i = 0; i < size; i++) {
                    Point point = points.poll();
                    if (this.image[point.r][point.c] != color)
                        continue;
                    this.image[point.r][point.c] = newColor;
                    addNextPoints(point.r, point.c);
                }
            }
            return this.image;
        }

        private void addNextPoints(int r, int c) {
            if (r > 0)
                addPoint(new Point(r - 1, c));
            if (r < image.length - 1)
                addPoint(new Point(r + 1, c));
            if (c > 0)
                addPoint(new Point(r, c - 1));
            if (c < image[0].length - 1)
                addPoint(new Point(r, c + 1));
        }

        private void addPoint(Point p) {
            if (!visited.contains(p)) {
                points.offer(p);
                visited.add(p);
            }
        }
    }
}
