package leetcode.depth_first_search;

import leetcode.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

public interface FlattenBinaryTreeToLinkedList {

    void flatten(TreeNode root);

    class Solution implements FlattenBinaryTreeToLinkedList {

        private final List<Integer> vals = new ArrayList<>();

        @Override
        public void flatten(TreeNode root) {
            traverse(root);
            toLine(root, 0);
        }

        private void traverse(TreeNode node) {
            if (node == null)
                return;
            vals.add(node.val);
            traverse(node.left);
            traverse(node.right);
        }

        private void toLine(TreeNode node, int index) {
            if (index == vals.size())
                return;
            node.val = vals.get(index);
            node.left = null;
            if (node.right == null && index + 1 < vals.size())
                node.right = new TreeNode(0);
            toLine(node.right, index + 1);
        }
    }
}
