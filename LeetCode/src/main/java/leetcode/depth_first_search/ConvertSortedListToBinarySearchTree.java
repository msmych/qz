package leetcode.depth_first_search;

import leetcode.linked_list.ListNode;
import leetcode.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

public interface ConvertSortedListToBinarySearchTree {

    TreeNode sortedListToBST(ListNode head);

    class Solution implements ConvertSortedListToBinarySearchTree {

        @Override
        public TreeNode sortedListToBST(ListNode head) {
            List<Integer> list = toList(head);
            return toBst(list);
        }

        private TreeNode toBst(List<Integer> list) {
            if (list.isEmpty())
                return null;
            int mid = list.size() / 2;
            TreeNode node = new TreeNode(list.get(mid));
            node.left = toBst(list.subList(0, mid));
            node.right = toBst(list.subList(mid + 1, list.size()));
            return node;
        }

        private List<Integer> toList(ListNode head) {
            List<Integer> list = new ArrayList<>();
            while (head != null) {
                list.add(head.val);
                head = head.next;
            }
            return list;
        }
    }
}
