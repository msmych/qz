package leetcode.depth_first_search;

import java.util.*;

public interface Matrix01 {

    int[][] updateMatrix(int[][] matrix);

    class Solution implements Matrix01 {

        class Point {
            int i;
            int j;

            Point(int i, int j) {
                this.i = i;
                this.j = j;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Point point = (Point) o;
                return i == point.i &&
                        j == point.j;
            }

            @Override
            public int hashCode() {
                return Objects.hash(i, j);
            }
        }

        private final Queue<Point> points = new LinkedList<>();
        private final Set<Point> visited = new HashSet<>();

        private int[][] matrix;

        @Override
        public int[][] updateMatrix(int[][] matrix) {
            this.matrix = matrix;
            int[][] result = new int[matrix.length][matrix[0].length];
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    Point point = new Point(i, j);
                    points.offer(point);
                    result[i][j] = distanceFrom0();
                    points.clear();
                    visited.clear();
                }
            }
            return result;
        }

        private int distanceFrom0() {
            int steps = 0;
            while (!points.isEmpty()) {
                int size = points.size();
                for (int k = 0; k < size; k++) {
                    Point point = points.poll();
                    if (matrix[point.i][point.j] == 0)
                        return steps;
                    if (point.i > 0) {
                        Point p = new Point(point.i - 1, point.j);
                        if (!visited.contains(p)) {
                            points.offer(p);
                            visited.add(p);
                        }
                    }
                    if (point.i < matrix.length - 1) {
                        Point p = new Point(point.i + 1, point.j);
                        if (!visited.contains(p)) {
                            points.offer(p);
                            visited.add(p);
                        }
                    }
                    if (point.j > 0) {
                        Point p = new Point(point.i, point.j - 1);
                        if (!visited.contains(p)) {
                            points.offer(p);
                            visited.add(p);
                        }
                    }
                    if (point.j < matrix[0].length - 1) {
                        Point p = new Point(point.i, point.j + 1);
                        if (!visited.contains(p)) {
                            points.offer(p);
                            visited.add(p);
                        }
                    }
                }
                steps++;
            }
            return steps;
        }
    }
}
