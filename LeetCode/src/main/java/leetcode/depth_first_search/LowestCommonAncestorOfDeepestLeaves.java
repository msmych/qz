package leetcode.depth_first_search;

import leetcode.tree.TreeNode;

import java.util.*;
import java.util.stream.Collectors;

public interface LowestCommonAncestorOfDeepestLeaves {

    TreeNode lcaDeepestLeaves(TreeNode root);

    class Solution implements LowestCommonAncestorOfDeepestLeaves {

        private final Set<List<TreeNode>> set = new HashSet<>();
        private int maxDepth = 0;

        @Override
        public TreeNode lcaDeepestLeaves(TreeNode root) {
            traverse(root, new ArrayList<>());
            for (int i = maxDepth - 1; i >= 0; i--) {
                int index = i;
                Set<TreeNode> anc = set.stream()
                        .map(p -> p.get(index))
                        .collect(Collectors.toSet());
                if (anc.size() == 1) return anc.iterator().next();
            }
            throw new IllegalArgumentException();
        }

        private void traverse(TreeNode node, List<TreeNode> path) {
            if (node == null) return;
            path.add(node);
            if (node.left == null && node.right == null) {
                if (path.size() > maxDepth) {
                    maxDepth = path.size();
                    set.removeIf(p -> p.size() < maxDepth);
                }
                if (path.size() >= maxDepth) set.add(path);
                return;
            }
            traverse(node.left, new ArrayList<>(path));
            traverse(node.right, new ArrayList<>(path));
        }
    }
}
