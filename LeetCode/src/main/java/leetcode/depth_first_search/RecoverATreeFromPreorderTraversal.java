package leetcode.depth_first_search;

import leetcode.tree.TreeNode;

public interface RecoverATreeFromPreorderTraversal {

    TreeNode recoverFromPreorder(String S);

    class Solution implements RecoverATreeFromPreorderTraversal {

        private String s;

        @Override
        public TreeNode recoverFromPreorder(String S) {
            this.s = S;
            return recoverNext(1);
        }

        private TreeNode recoverNext(int d) {
            if (s.isEmpty())
                return null;
            int dashIndex = s.indexOf('-');
            int val = Integer.valueOf(dashIndex == -1 ? s : s.substring(0, dashIndex));
            if (dashIndex != -1)
                s = s.substring(dashIndex);
            TreeNode node = new TreeNode(val);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < d; i++) {
                sb.append('-');
            }
            if (s.startsWith(sb.toString())) {
                s = s.substring(sb.length());
                node.left = recoverNext(d + 1);
            }
            if (s.startsWith(sb.toString())) {
                s = s.substring(sb.length());
                node.right = recoverNext(d + 1);
            }
            return node;
        }
    }
}
