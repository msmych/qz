package leetcode.depth_first_search;

import leetcode.tree.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public interface SumRootToLeafNumbers {

    int sumNumbers(TreeNode root);

    class Solution implements SumRootToLeafNumbers {

        @Override
        public int sumNumbers(TreeNode root) {
            return nextNumbers(root).stream()
                    .mapToInt(this::toNumber)
                    .sum();
        }

        private List<List<Integer>> nextNumbers(TreeNode node) {
            if (node == null)
                return new ArrayList<>();
            List<List<Integer>> numbers = new ArrayList<>();
            if (node.left == null && node.right == null)
                return Collections.singletonList(Collections.singletonList(node.val));
            for (List<Integer> num : nextNumbers(node.left)) {
                List<Integer> next = new ArrayList<>(num);
                next.add(node.val);
                numbers.add(next);
            }
            for (List<Integer> num : nextNumbers(node.right)) {
                List<Integer> next = new ArrayList<>(num);
                next.add(node.val);
                numbers.add(next);
            }
            return numbers;
        }

        private int toNumber(List<Integer> list) {
            int num = 0;
            for (int i = list.size() - 1; i >= 0; i--) {
                num *= 10;
                num += list.get(i);
            }
            return num;
        }
    }
}
