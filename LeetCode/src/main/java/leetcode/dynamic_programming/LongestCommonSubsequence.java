package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public interface LongestCommonSubsequence {

    int longestCommonSubsequence(String text1, String text2);

    class Solution implements LongestCommonSubsequence {

        private static class Text12 {
            String t1, t2;

            public Text12(String t1, String t2) {
                this.t1 = t1;
                this.t2 = t2;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Text12 text12 = (Text12) o;
                return Objects.equals(t1, text12.t1) &&
                        Objects.equals(t2, text12.t2) ||
                        t1.equals(text12.t2) &&
                        t2.equals(text12.t1);
            }

            @Override
            public int hashCode() {
                return Objects.hash(t1, t2) + Objects.hash(t2, t1);
            }
        }

        private final Map<Text12, Integer> cache = new HashMap<>();

        @Override
        public int longestCommonSubsequence(String text1, String text2) {
            if (text1.isEmpty() || text2.isEmpty()) {
                return 0;
            }
            Text12 text12 = new Text12(text1, text2);
            if (cache.containsKey(text12)) {
                return cache.get(text12);
            }
            if (text1.equals(text2)) {
                cache.put(text12, text1.length());
                return text1.length();
            }
            if (text1.charAt(0) == text2.charAt(0)) {
                int max = 1 + longestCommonSubsequence(text1.substring(1), text2.substring(1));
                cache.put(text12, max);
                return max;
            }
            int max = Math.max(
                    longestCommonSubsequence(text1, text2.substring(1)),
                    longestCommonSubsequence(text1.substring(1), text2));
            cache.put(text12, max);
            return max;
        }
    }
}
