package leetcode.dynamic_programming;

public interface DungeonGame {

    int calculateMinimumHP(int[][] dungeon);

    class Solution implements DungeonGame {

        @Override
        public int calculateMinimumHP(int[][] dungeon) {
            return 0;
        }
    }
}
