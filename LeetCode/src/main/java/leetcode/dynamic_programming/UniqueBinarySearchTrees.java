package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface UniqueBinarySearchTrees {

    int numTrees(int n);

    class Solution implements UniqueBinarySearchTrees {

        private final Map<Set<Integer>, Integer> cache = new HashMap<>();

        @Override
        public int numTrees(int n) {
            return getNum(IntStream.range(1, n + 1).boxed().collect(Collectors.toSet()));
        }

        private int getNum(Set<Integer> nums) {
            if (nums.isEmpty())
                return 0;
            if (nums.size() == 1)
                return 1;
            if (cache.containsKey(nums))
                return cache.get(nums);
            int count = 0;
            for (int num : nums) {
                int left = getNum(nums.stream().filter(n -> n < num).collect(Collectors.toSet())),
                    right = getNum(nums.stream().filter(n -> n > num).collect(Collectors.toSet()));
                if (left == 0 || right == 0)
                    count += left + right;
                else
                    count += left * right;
            }
            cache.put(nums, count);
            return count;
        }
    }
}
