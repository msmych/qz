package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.Map;

public interface TargetSum {

    int findTargetSumWays(int[] nums, int S);

    class Solution implements TargetSum {

        private int[] nums;

        @Override
        public int findTargetSumWays(int[] nums, int S) {
            this.nums = nums;
            return possibleSums(0).getOrDefault(S, 0);
        }

        private Map<Integer, Integer> possibleSums(int index) {
            Map<Integer, Integer> sums = new HashMap<>();
            if (index == nums.length - 1) {
                sums.put(nums[index], 1);
                sums.put(-nums[index], sums.getOrDefault(-nums[index], 0) + 1);
                return sums;
            }
            for (Map.Entry<Integer, Integer> sum : possibleSums(index + 1).entrySet()) {
                int positive = sum.getKey() + nums[index],
                        negative = sum.getKey() - nums[index];
                sums.put(positive, sums.getOrDefault(positive, 0) + sum.getValue());
                sums.put(negative, sums.getOrDefault(negative, 0) + sum.getValue());
            }
            return sums;
        }
    }
}
