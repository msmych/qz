package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public interface PalindromePartitioningIII {

  int palindromePartition(String s, int k);

  class Solution implements PalindromePartitioningIII {

    private static class StringWithK {
      String s;
      int k;

      public StringWithK(String s, int k) {
        this.s = s;
        this.k = k;
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StringWithK that = (StringWithK) o;
        return k == that.k &&
          Objects.equals(s, that.s);
      }

      @Override
      public int hashCode() {
        return Objects.hash(s, k);
      }
    }

    private final Map<StringWithK, Integer> minCache = new HashMap<>();
    private final Map<String, Boolean> palindromeCache = new HashMap<>();
    private final Map<String, Integer> changesCache = new HashMap<>();

    @Override
    public int palindromePartition(String s, int k) {
      if (k == s.length()) {
        return 0;
      }
      if (k == 1) {
        return changes(s);
      }
      StringWithK stringWithK = new StringWithK(s, k);
      if (minCache.containsKey(stringWithK)) {
        return minCache.get(stringWithK);
      }
      int min = -1;
      for (int i = 1; i + k <= s.length() + 1; i++) {
        int changes = changes(s.substring(0, i));
        int nextMin = palindromePartition(s.substring(i), k - 1);
        if (nextMin == -1) {
          continue;
        }
        changes += nextMin;
        if (min == - 1 || changes < min) {
          min = changes;
        }
        if (min == 0) {
          break;
        }
      }
      minCache.put(stringWithK, min);
      return min;
    }

    private boolean isPalindrome(String s) {
      if (palindromeCache.containsKey(s)) {
        return palindromeCache.get(s);
      }
      boolean isPalindrome = new StringBuilder(s).reverse().toString().equals(s);
      palindromeCache.put(s, isPalindrome);
      return isPalindrome;
    }

    private int changes(String s) {
      if (changesCache.containsKey(s)) {
        return changesCache.get(s);
      }
      int min = 0;
      for (int i = 0; i < s.length() / 2; i++) {
        if (s.charAt(i) != s.charAt(s.length() - i - 1)) {
          min++;
        }
      }
      changesCache.put(s, min);
      return min;
    }
  }
}
