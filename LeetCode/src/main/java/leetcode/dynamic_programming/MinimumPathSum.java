package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public interface MinimumPathSum {

    int minPathSum(int[][] grid);

    class Solution implements MinimumPathSum {

        private class Position {
            int i, j;

            Position(int i, int j) {
                this.i = i;
                this.j = j;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Position position = (Position) o;
                return i == position.i &&
                        j == position.j;
            }

            @Override
            public int hashCode() {
                return Objects.hash(i, j);
            }
        }

        private final Map<Position, Integer> cache = new HashMap<>();

        private int[][] grid;

        @Override
        public int minPathSum(int[][] grid) {
            this.grid = grid;
            return minPath(0, 0);
        }

        private int minPath(int i, int j) {
            if (i == grid.length || j == grid[0].length)
                return Integer.MAX_VALUE;
            if (i == grid.length - 1 && j == grid[0].length - 1)
                return grid[i][j];
            Position position = new Position(i, j);
            if (cache.containsKey(position))
                return cache.get(position);
            int sum = grid[i][j] + Math.min(minPath(i, j + 1), minPath(i + 1, j));
            cache.put(position, sum);
            return sum;
        }
    }
}
