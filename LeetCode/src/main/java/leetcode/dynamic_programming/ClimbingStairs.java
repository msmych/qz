package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.Map;

public interface ClimbingStairs {

    int climbStairs(int n);

    class Solution implements ClimbingStairs {

        private final Map<Integer, Integer> cache = new HashMap<>();

        @Override
        public int climbStairs(int n) {
            if (n < 1)
                return 0;
            if (n == 1)
                return 1;
            if (n == 2)
                return 2;
            if (cache.containsKey(n))
                return cache.get(n);
            int steps = climbStairs(n - 1) + climbStairs(n - 2);
            cache.put(n, steps);
            return steps;
        }
    }

    class FibonacciSolution implements ClimbingStairs {

        @Override
        public int climbStairs(int n) {
            double square5 = Math.sqrt(5);
            double fibonacci = Math.pow((1 + square5)/2, n + 1) - Math.pow((1 - square5)/2, n + 1);
            return (int) (fibonacci / square5);
        }
    }
}
