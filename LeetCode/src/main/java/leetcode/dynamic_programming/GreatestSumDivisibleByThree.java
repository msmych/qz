package leetcode.dynamic_programming;

public interface GreatestSumDivisibleByThree {

  int maxSumDivThree(int[] nums);

  class Solution implements GreatestSumDivisibleByThree {
    @Override
    public int maxSumDivThree(int[] nums) {
      int[] mod = new int[3];
      for (int num : nums) {
        int m0 = mod[0] + num;
        int m1 = mod[1] + num;
        int m2 = mod[2] + num;
        if (m0 > mod[m0 % 3]) {
          mod[m0 % 3] = m0;
        }
        if (m1 > mod[m1 % 3]) {
          mod[m1 % 3] = m1;
        }
        if (m2 > mod[m2 % 3]) {
          mod[m2 % 3] = m2;
        }
      }
      return mod[0];
    }
  }
}
