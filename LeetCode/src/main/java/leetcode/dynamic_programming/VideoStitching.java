package leetcode.dynamic_programming;

import java.util.*;
import java.util.stream.Collectors;

public interface VideoStitching {

    int videoStitching(int[][] clips, int T);

    class Solution implements VideoStitching {

        private int T;

        @Override
        public int videoStitching(int[][] clips, int T) {
            this.T = T;
            return nextStitching(Arrays.stream(clips).collect(Collectors.toSet()), 0);
        }

        private int nextStitching(Set<int[]> clips, int t) {
            if (t >= T)
                return 0;
           int min = clips.stream()
                   .filter(clip -> clip[0] <= t)
                   .filter(clip -> clip[1] > t)
                   .max(Comparator.comparingInt(clip -> clip[1]))
                   .map(clip -> {
                       Set<int[]> nextClips = new HashSet<>(clips);
                       nextClips.remove(clip);
                       return nextStitching(nextClips, clip[1]);
                   })
                   .orElse(-1);
           return min == -1 ? -1 : 1 + min;
        }
    }
}
