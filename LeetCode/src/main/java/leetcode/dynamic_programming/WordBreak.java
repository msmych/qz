package leetcode.dynamic_programming;

import java.util.*;

public interface WordBreak {

  boolean wordBreak(String s, List<String> wordDict);

  class Solution implements WordBreak {

    private final Map<String, Boolean> cache = new HashMap<>();
    private final Set<String> dict = new HashSet<>();

    @Override
    public boolean wordBreak(String s, List<String> wordDict) {
      dict.addAll(wordDict);
      return nextBreak(s);
    }

    private boolean nextBreak(String s) {
      if (s.isEmpty()) return true;
      if (cache.containsKey(s))
        return cache.get(s);
      if (dict.contains(s)) return true;
      boolean breaks = dict.stream()
        .filter(s::startsWith)
        .map(String::length)
        .map(s::substring)
        .anyMatch(this::nextBreak);
      cache.put(s, breaks);
      return breaks;
    }
  }
}
