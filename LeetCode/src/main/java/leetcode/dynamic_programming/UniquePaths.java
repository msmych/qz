package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public interface UniquePaths {

  int uniquePaths(int m, int n);

  class Solution implements UniquePaths {

    private class Grid {
      int m, n;

      Grid(int m, int n) {
        this.m = m;
        this.n = n;
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Grid grid = (Grid) o;
        return m == grid.m &&
          n == grid.n;
      }

      @Override
      public int hashCode() {
        return Objects.hash(m, n);
      }
    }

    private final Map<Grid, Integer> cache = new HashMap<>();

    @Override
    public int uniquePaths(int m, int n) {
      if (m == 0 || n == 0) {
        return 0;
      }
      if (m == 1 && n <= 2 || m <= 2 && n == 1) {
        return 1;
      }
      Grid grid = new Grid(m, n);
      if (cache.containsKey(grid)) {
        return cache.get(grid);
      }
      int count = uniquePaths(m, n - 1) + uniquePaths(m - 1, n);
      cache.put(grid, count);
      return count;
    }
  }
}
