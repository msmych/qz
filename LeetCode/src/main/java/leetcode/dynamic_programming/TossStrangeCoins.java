package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

public interface TossStrangeCoins {

  double probabilityOfHeads(double[] prob, int target);

  class Solution implements TossStrangeCoins {

    private static class Toss {
      List<Double> prob;
      int target;

      public Toss(List<Double> prob, int target) {
        this.prob = prob;
        this.target = target;
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Toss toss = (Toss) o;
        return target == toss.target &&
          Objects.equals(prob, toss.prob);
      }

      @Override
      public int hashCode() {
        return Objects.hash(prob, target);
      }
    }

    private final Map<Toss, Double> cache = new HashMap<>();

    @Override
    public double probabilityOfHeads(double[] prob, int target) {
      return nextProb(stream(prob).boxed().collect(toList()), target);
    }

    private double nextProb(List<Double> prob, int target) {
      if (target < 0) {
        return -1.0;
      } else if (target > prob.size()) {
        return -1.0;
      } else if (prob.isEmpty()) {
        return 1.0;
      }
      Toss toss = new Toss(prob, target);
      if (cache.containsKey(toss)) {
        return cache.get(toss);
      }
      if (prob.size() == 1) {
        double p = target == 1 ? prob.get(0) : 1.0 - prob.get(0);
        cache.put(toss, p);
        return p;
      }
      List<Double> nextProb = prob.subList(1, prob.size());
      double nextHead = nextProb(nextProb, target - 1);
      double nextTail = nextProb(nextProb, target);
      if (nextHead == -1.0 && nextTail == -1.0) {
        cache.put(toss, -1.0);
        return -1.0;
      } else if (nextTail == -1.0) {
        double p = prob.get(0) * nextHead;
        cache.put(toss, p);
        return p;
      } else if (nextHead == -1.0) {
        double p = (1 - prob.get(0)) * nextTail;
        cache.put(toss, p);
        return p;
      } else {
        double p = prob.get(0) * nextHead + (1 - prob.get(0)) * nextTail;
        cache.put(toss, p);
        return p;
      }
    }
  }
}
