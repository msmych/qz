package leetcode.dynamic_programming;

import java.util.*;
import java.util.stream.Collectors;

public interface LongestArithmeticSubsequenceOfGivenDifference {

  int longestSubsequence(int[] arr, int difference);

  class Solution implements LongestArithmeticSubsequenceOfGivenDifference {

    private static class Sublist {
      int n;
      List<Integer> list;

      public Sublist(int n, List<Integer> list) {
        this.n = n;
        this.list = list;
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sublist sublist = (Sublist) o;
        return n == sublist.n &&
          Objects.equals(list, sublist.list);
      }

      @Override
      public int hashCode() {
        return Objects.hash(n, list);
      }
    }

    private final Map<Sublist, int[]> cache = new HashMap<>();

    private int diff;

    @Override
    public int longestSubsequence(int[] arr, int difference) {
      if (arr.length == 0) {
        return 0;
      }
      diff = difference;
      int[] len = nextLongest(arr[0], Arrays.stream(arr).skip(1).boxed().collect(Collectors.toList()));
      return Math.max(len[0], len[1]);
    }

    private int[] nextLongest(int n, List<Integer> list) {
      if (list.isEmpty()) {
        return new int[]{1, 1};
      }
      Sublist sublist = new Sublist(n, list);
      if (cache.containsKey(sublist)) {
        return cache.get(sublist);
      }
      int take = 0;
      for (int i = 0; i < list.size(); i++) {
        int num = list.get(i);
        if (n + diff == num) {
          int nextTake = nextLongest(num, list.subList(i + 1, list.size()))[0] + 1;
          if (nextTake == 1) {
            nextTake++;
          }
          if (nextTake > take) {
            take = nextTake;
          }
        }
      }
      int[] nextNoTake = nextLongest(list.get(0), list.subList(1, list.size()));
      int noTake = Math.max(nextNoTake[0], nextNoTake[1]);
      int[] len = {take, noTake};
      cache.put(sublist, len);
      return len;
    }
  }
}
