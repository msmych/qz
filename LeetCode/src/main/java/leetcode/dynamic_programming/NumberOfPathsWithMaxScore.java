package leetcode.dynamic_programming;

import java.util.List;

public interface NumberOfPathsWithMaxScore {

  int[] pathsWithMaxScore(List<String> board);

  class Solution implements NumberOfPathsWithMaxScore {
    @Override
    public int[] pathsWithMaxScore(List<String> board) {
      return new int[0];
    }
  }
}
