package leetcode.dynamic_programming;

public interface ShortestCommonSupersequence {

    String shortestCommonSupersequence(String str1, String str2);

    class Solution implements ShortestCommonSupersequence {

        @Override
        public String shortestCommonSupersequence(String str1, String str2) {
            return null;
        }
    }
}
