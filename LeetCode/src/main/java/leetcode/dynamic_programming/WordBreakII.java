package leetcode.dynamic_programming;

import java.util.*;
import java.util.stream.Collectors;

public interface WordBreakII {

  List<String> wordBreak(String s, List<String> wordDict);

  class Solution implements WordBreakII {

    private final List<String> dict = new ArrayList<>();
    private final Map<String, Set<String>> cache = new HashMap<>();

    @Override
    public List<String> wordBreak(String s, List<String> wordDict) {
      dict.addAll(wordDict);
      return new ArrayList<>(nextBreak(s));
    }

    private Set<String> nextBreak(String s) {
      if (s.isEmpty()) return new HashSet<>();
      if (cache.containsKey(s)) return cache.get(s);
      Set<String> phrases = dict.stream().filter(s::equals).collect(Collectors.toSet());
      for (String word : dict.stream()
        .filter(w -> !w.equals(s))
        .filter(s::startsWith)
        .collect(Collectors.toSet())) {
        for (String nextPhrase : nextBreak(s.substring(word.length()))) {
          phrases.add(word + " " + nextPhrase);
        }
      }
      cache.put(s, phrases);
      return phrases;
    }
  }
}
