package leetcode.dynamic_programming;

import java.util.stream.IntStream;

public interface CountVowelsPermutation {

  int countVowelPermutation(int n);

  class Solution implements CountVowelsPermutation {
    @Override
    public int countVowelPermutation(int n) {
      if (n == 0) {
        return 0;
      }
      int[] perms = new int[]{1, 1, 1, 1, 1};
      for (; n > 1; n--) {
        int a = mod(perms[1], perms[2], perms[4]);
        int e = mod(perms[0], perms[2]);
        int i = mod(perms[1], perms[3]);
        int o = perms[2];
        int u = mod(perms[2], perms[3]);
        perms = new int[]{a, e, i, o, u};
      }
      return mod(perms);
    }

    private int mod(int... nums) {
      return (int) (IntStream.of(nums).mapToLong(p -> p).sum() % 1_000_000_007);
    }
  }
}
