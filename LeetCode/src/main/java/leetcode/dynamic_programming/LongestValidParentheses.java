package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public interface LongestValidParentheses {

    int longestValidParentheses(String s);

    class Solution implements LongestValidParentheses {

        private class FromTo {
            int from;
            int to;

            FromTo(int from, int to) {
                this.from = from;
                this.to = to;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                FromTo fromTo = (FromTo) o;
                return from == fromTo.from &&
                        to == fromTo.to;
            }

            @Override
            public int hashCode() {
                return Objects.hash(from, to);
            }
        }

        private final Map<FromTo, Integer> cache = new HashMap<>();
        private String s;

        @Override
        public int longestValidParentheses(String s) {
            this.s = s;
            return getLongest(0, s.length());
        }

        private int getLongest(int from, int to) {
            if (from == to)
                return 0;
            FromTo fromTo = new FromTo(from, to);
            if (cache.containsKey(fromTo))
                return cache.get(fromTo);
            String sub = s.substring(from, to);
            if (!sub.contains(")")) {
                cache.put(fromTo, 0);
                return 0;
            }
            int i = from, index = 0;
            while (i < to && index >= 0) {
                if (s.charAt(i) == '(')
                    index++;
                else
                    index--;
                i++;
            }
            int longest;
            if (index == 0)
                longest = i - from;
            else if (index > 0)
                longest = getLongestRightToLeft(to, from);
            else
                longest = Math.max(i - from - 1, getLongest(i, to));
            cache.put(fromTo, longest);
            return longest;
        }

        private int getLongestRightToLeft(int from, int to) {
            if (from == to)
                return 0;
            int i = from - 1, index = 0;
            while (i >= to && index >= 0) {
                if (s.charAt(i) == ')')
                    index++;
                else
                    index--;
                i--;
            }
            int longest = Math.max(from - i - 2, getLongest(to, i + 1));
            cache.put(new FromTo(to, from), longest);
            return longest;
        }
    }
}
