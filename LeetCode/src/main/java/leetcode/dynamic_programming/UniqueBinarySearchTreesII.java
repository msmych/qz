package leetcode.dynamic_programming;

import leetcode.tree.TreeNode;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface UniqueBinarySearchTreesII {

    List<TreeNode> generateTrees(int n);

    class Solution implements UniqueBinarySearchTreesII {

        @Override
        public List<TreeNode> generateTrees(int n) {
            return next(IntStream.range(1, n + 1).boxed().collect(Collectors.toSet()));
        }

        private List<TreeNode> next(Set<Integer> vals) {
            if (vals.isEmpty())
                return new ArrayList<>();
            if (vals.size() == 1)
                return Collections.singletonList(new TreeNode(vals.iterator().next()));
            List<TreeNode> trees = new ArrayList<>();
            for (int val : vals) {
                Map<Boolean, List<Integer>> nextVals = vals.stream()
                        .filter(v -> v != val)
                        .collect(Collectors.groupingBy(v -> v > val));
                List<TreeNode> leftTrees = nextVals.containsKey(false) ? next(new HashSet<>(nextVals.get(false))) : new ArrayList<>(),
                        rightTrees = nextVals.containsKey(true) ? next(new HashSet<>(nextVals.get(true))) : new ArrayList<>();
                if (leftTrees.isEmpty() && rightTrees.isEmpty())
                    trees.add(new TreeNode(val));
                else if (leftTrees.isEmpty())
                    trees.addAll(rightTrees.stream()
                            .map(right -> {
                                TreeNode tree = new TreeNode(val);
                                tree.right = right;
                                return tree;
                            }).collect(Collectors.toList()));
                else if (rightTrees.isEmpty())
                    trees.addAll(leftTrees.stream()
                            .map(left -> {
                                TreeNode tree = new TreeNode(val);
                                tree.left = left;
                                return tree;
                            }).collect(Collectors.toList()));
                else {
                    for (TreeNode left : leftTrees) {
                        for (TreeNode right : rightTrees) {
                            TreeNode tree = new TreeNode(val);
                            tree.left = left;
                            tree.right = right;
                            trees.add(tree);
                        }
                    }
                }
            }
            return trees;
        }
    }
}
