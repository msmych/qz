package leetcode.dynamic_programming;

import java.util.*;

public interface DivisorGame {

    boolean divisorGame(int N);

    class Solution implements DivisorGame {

        private final Map<Integer, Set<Integer>> cache = new HashMap<>();
        private final Map<Integer, Boolean> aliceCache = new HashMap<>();
        private final Map<Integer, Boolean> bobCache = new HashMap<>();

        @Override
        public boolean divisorGame(int N) {
            return aliceWins(N);
        }

        private boolean aliceWins(int n) {
            if (aliceCache.containsKey(n))
                return aliceCache.get(n);
            Set<Integer> nextMoves = cache.containsKey(n) ? cache.get(n) : getNextMoves(n);
            if (nextMoves.isEmpty())
                return false;
            boolean wins = nextMoves.stream()
                    .map(x -> n - x)
                    .anyMatch(x -> !bobWins(x));
            aliceCache.put(n, wins);
            return wins;
        }

        private boolean bobWins(int n) {
            if (bobCache.containsKey(n))
                return bobCache.get(n);
            Set<Integer> nextMoves = cache.containsKey(n) ? cache.get(n) : getNextMoves(n);
            if (nextMoves.isEmpty())
                return false;
            boolean wins = nextMoves.stream()
                    .map(x -> n - x)
                    .anyMatch(x -> !aliceWins(x));
            bobCache.put(n, wins);
            return wins;
        }

        private Set<Integer> getNextMoves(int n) {
            if (n <= 1) {
                cache.put(n, new HashSet<>());
                return new HashSet<>();
            }
            Set<Integer> nextMoves = new HashSet<>();
            for (int i = 1; i <= n / 2; i++) {
                if (n % i == 0)
                    nextMoves.add(i);
            }
            cache.put(n, nextMoves);
            return nextMoves;
        }
    }
}
