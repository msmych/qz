package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Integer.parseInt;

public interface DecodeWays {

  int numDecodings(String s);

  class Solution implements DecodeWays {

    private final Map<String, Integer> cache = new HashMap<>();

    @Override
    public int numDecodings(String s) {
      if (s.isEmpty() || s.startsWith("0")) {
          return 0;
      }
      if (s.length() == 1) {
          return 1;
      }
      if (cache.containsKey(s)) {
          return cache.get(s);
      }
      int n = parseInt(s.substring(0, 2));
      if (n % 10 == 0 && n != 10 && n != 20) {
          return 0;
      }
      if (s.length() == 2)
        return n >= 1 && n <= 9 || n == 10 || n == 20 || n > 26 ? 1 : 2;
      int num = numDecodings(s.substring(1));
      if (n <= 26) {
          num += numDecodings(s.substring(2));
      }
      cache.put(s, num);
      return num;
    }
  }
}
