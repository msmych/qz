package leetcode.dynamic_programming;

public interface DistinctSubsequences {

    int numDistinct(String s, String t);

    class Solution implements DistinctSubsequences {

        @Override
        public int numDistinct(String s, String t) {
            return 0;
        }
    }
}
