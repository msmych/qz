package leetcode.dynamic_programming;

import java.util.Arrays;

public interface CoinChange {

  int coinChange(int[] coins, int amount);

  class Solution implements CoinChange {

    @Override
    public int coinChange(int[] coins, int amount) {
      int max = amount + 1;
      int[] changes = new int[amount + 1];
      Arrays.fill(changes, max);
      changes[0] = 0;
      for (int i = 1; i <= amount; i++) {
        for (int coin : coins) {
          if (coin <= i) {
            if (changes[i - coin] + 1 < changes[i]) {
              changes[i] = changes[i - coin] + 1;
            }
          }
        }
      }
      return changes[amount] > amount ? -1 : changes[amount];
    }
  }
}
