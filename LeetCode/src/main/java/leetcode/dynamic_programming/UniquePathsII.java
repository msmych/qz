package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public interface UniquePathsII {

    int uniquePathsWithObstacles(int[][] obstacleGrid);

    class Solution implements UniquePathsII {

        private class Position {
            int i, j;

            Position(int i, int j) {
                this.i = i;
                this.j = j;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Position position = (Position) o;
                return i == position.i &&
                        j == position.j;
            }

            @Override
            public int hashCode() {
                return Objects.hash(i, j);
            }
        }

        private final Map<Position, Integer> cache = new HashMap<>();

        private int[][] grid;

        @Override
        public int uniquePathsWithObstacles(int[][] obstacleGrid) {
            grid = obstacleGrid;
            if (grid[grid.length - 1][grid[0].length - 1] == 1)
                return 0;
            return uniquePaths(0, 0);
        }

        private int uniquePaths(int i, int j) {
            if (i == grid.length || j == grid[0].length || grid[i][j] == 1)
                return 0;
            if (i == grid.length - 1 && j >= grid[0].length - 2
                || i >= grid.length - 2 && j == grid[0].length - 1)
                return 1;
            Position position = new Position(i, j);
            if (cache.containsKey(position))
                return cache.get(position);
            int count = uniquePaths(i, j + 1) + uniquePaths(i + 1, j);
            cache.put(position, count);
            return count;
        }
    }
}
