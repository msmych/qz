package leetcode.dynamic_programming;

import static java.lang.Math.min;

public interface NumberOfWaysToStayInTheSamePlaceAfterSomeSteps {

  int numWays(int steps, int arrLen);

  class Solution implements NumberOfWaysToStayInTheSamePlaceAfterSomeSteps {

    private static final int MOD = 1_000_000_007;

    @Override
    public int numWays(int steps, int arrLen) {
      int[] dp = new int[arrLen];
      dp[0] = 1;
      dp[1] = 1;
      for (int i = 1; i < steps; i++) {
        int[] nextSteps = new int[arrLen];
        for (int j = 0; j <= min(arrLen - 1, steps - i); j++) {
          int step = dp[j];
          if (j > 0) {
            step = (step + dp[j - 1]) % MOD;
          }
          if (j < arrLen - 1) {
            step = (step + dp[j + 1]) % MOD;
          }
          nextSteps[j] = step;
        }
        dp = nextSteps;
      }
      return dp[0];
    }
  }
}
