package leetcode.dynamic_programming;

public interface CountSquareSubmatricesWithAllOnes {

  int countSquares(int[][] matrix);

  class Solution implements CountSquareSubmatricesWithAllOnes {

    private int[][] matrix;

    @Override
    public int countSquares(int[][] matrix) {
      this.matrix = matrix;
      int squares = 0;
      for (int n = Math.min(matrix.length, matrix[0].length); n > 0; n--) {
        for (int i = 0; i + n <= matrix.length; i++) {
          for (int j = 0; j + n <= matrix[i].length; j++) {
            if (allOnes(i, j, n)) {
              squares++;
            }
          }
        }
      }
      return squares;
    }

    private boolean allOnes(int y, int x, int n) {
      for (int i = y; i < y + n; i++) {
        for (int j = x; j < x + n; j++) {
          if (matrix[i][j] != 1) {
            return false;
          }
        }
      }
      return true;
    }

  }
}
