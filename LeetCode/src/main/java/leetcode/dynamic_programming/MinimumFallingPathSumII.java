package leetcode.dynamic_programming;

import static java.util.Arrays.stream;
import static java.util.stream.IntStream.range;

public interface MinimumFallingPathSumII {

  int minFallingPathSum(int[][] arr);

  class Solution implements MinimumFallingPathSumII {

    @Override
    public int minFallingPathSum(int[][] arr) {
      if (arr.length == 1) {
        return arr[0][0];
      }
      int[] dp = arr[0];
      for (int i = 1; i < arr.length; i++) {
        int[] sum = new int[dp.length];
        for (int j = 0; j < sum.length; j++) {
          sum[j] = min(dp, j) + arr[i][j];
        }
        dp = sum;
      }
      return stream(dp).min().getAsInt();
    }

    private int min(int[] arr, int skip) {
      return range(0, arr.length).filter(i -> i != skip).map(i -> arr[i]).min().getAsInt();
    }
  }
}
