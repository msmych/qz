package leetcode.dynamic_programming;

public interface ScrambleString {

  boolean isScramble(String s1, String s2);

  class Solution implements ScrambleString {

    @Override
    public boolean isScramble(String s1, String s2) {
      boolean[][][] dp = new boolean[s1.length()][s1.length()][s1.length() + 1];
      for (int k = 1; k <= s1.length(); k++) {
        for (int i = 0; i + k <= s1.length(); i++) {
          for (int j = 0; j + k <= s1.length(); j++) {
            if (k == 1) {
              dp[i][j][k] = s1.charAt(i) == s2.charAt(j);
            } else {
              for (int q = 1; q < k && !dp[i][j][k]; q++) {
                dp[i][j][k] = (dp[i][j][q] && dp[i + q][j + q][k - q]) || (dp[i][j + k - q][q] && dp[i + q][j][k - q]);
              }
            }
          }
        }
      }
      return dp[0][0][s1.length()];
    }

  }
}
