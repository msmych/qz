package leetcode.dynamic_programming;

public interface RegularExpressionMatching {

  boolean isMatch(String s, String p);

  class Solution implements RegularExpressionMatching {

    @Override
    public boolean isMatch(String s, String p) {
      if (p.isEmpty()) return s.isEmpty();
      boolean firstMatch = false;
      if (!s.isEmpty()) {
        char cs = s.charAt(0);
        char cp = p.charAt(0);
        firstMatch = cs == cp || cp == '.';
      }
      if (p.length() > 1 && p.charAt(1) == '*')
        return isMatch(s, p.substring(2))
          || firstMatch && isMatch(s.substring(1), p);
      else
        return firstMatch && isMatch(s.substring(1), p.substring(1));
    }
  }
}
