package leetcode.dynamic_programming;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class NumArray {

    private static class Pair {
        int i, j;

        public Pair(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pair pair = (Pair) o;
            return i == pair.i &&
                j == pair.j;
        }

        @Override
        public int hashCode() {
            return Objects.hash(i, j);
        }
    }

    private final Map<Pair, Integer> cache = new HashMap<>();
    private final int[] nums;

    public NumArray(int[] nums) {
        this.nums = nums;
    }

    public int sumRange(int i, int j) {
        Pair pair = new Pair(i, j);
        if (cache.containsKey(pair)) {
            return cache.get(pair);
        }
        int sum = Arrays.stream(Arrays.copyOfRange(nums, i, j + 1)).sum();
        cache.put(pair, sum);
        return sum;
    }
}
