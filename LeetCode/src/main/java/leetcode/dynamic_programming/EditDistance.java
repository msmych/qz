package leetcode.dynamic_programming;

import java.util.stream.IntStream;

public interface EditDistance {

  int minDistance(String word1, String word2);

  class Solution implements EditDistance {

    @Override
    public int minDistance(String word1, String word2) {
      int[][] dp = new int[word1.length() + 1][word2.length() + 1];
      for (int i = 0; i < dp.length; i++) {
        dp[i][0] = i;
      }
      for (int j = 0; j < dp[0].length; j++) {
        dp[0][j] = j;
      }
      for (int i = 0; i < word1.length(); i++) {
        for (int j = 0; j < word2.length(); j++) {
          dp[i + 1][j + 1] = word1.charAt(i) == word2.charAt(j)
            ? dp[i][j]
            : IntStream.of(dp[i + 1][j], dp[i][j + 1], dp[i][j]).min().getAsInt() + 1;
        }
      }
      return dp[word1.length()][word2.length()];
    }
  }
}
