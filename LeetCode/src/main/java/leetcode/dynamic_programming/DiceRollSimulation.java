package leetcode.dynamic_programming;

public interface DiceRollSimulation {

  int dieSimulator(int n, int[] rollMax);

  class Solution implements DiceRollSimulation {
    @Override
    public int dieSimulator(int n, int[] rollMax) {
      return 0;
    }
  }
}
