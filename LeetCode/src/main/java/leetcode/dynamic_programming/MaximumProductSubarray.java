package leetcode.dynamic_programming;

import java.util.*;
import java.util.stream.Collectors;

public interface MaximumProductSubarray {

    int maxProduct(int[] nums);

    class Solution implements MaximumProductSubarray {

        private final Map<List<Integer>, Integer> cache = new HashMap<>();

        @Override
        public int maxProduct(int[] nums) {
            return nextMax(Arrays.stream(nums).boxed().collect(Collectors.toList()));
        }

        private int nextMax(List<Integer> list) {
            if (list.size() == 1) return list.get(0);
            if (cache.containsKey(list)) return cache.get(list);
            List<List<Integer>> nonZeroSublists = splitZeroesTrimOnes(list);
            int max = nonZeroSublists.stream()
                    .mapToInt(nzs -> nzs.stream()
                            .reduce((n1, n2) -> n1 * n2)
                            .orElse(Integer.MIN_VALUE))
                    .max()
                    .orElse(Integer.MIN_VALUE);
            if (max < 0 && list.stream().anyMatch(n -> n == 0)) max = 0;
            for (List<Integer> nonZeroSublist : nonZeroSublists) {
                for (int i = 0; i < nonZeroSublist.size(); i++) {
                    List<Integer> left = nonZeroSublist.subList(0, i);
                    int leftMax = nextMax(left);
                    if (leftMax > max) max = leftMax;
                    List<Integer> right = nonZeroSublist.subList(i + 1, nonZeroSublist.size());
                    int rightMax = nextMax(right);
                    if (rightMax > max) max = rightMax;
                }
            }
            cache.put(list, max);
            return max;
        }

        private List<List<Integer>> splitZeroesTrimOnes(List<Integer> list) {
            List<List<Integer>> nonZeroSublists = new ArrayList<>();
            List<Integer> nonZeroSublist = new ArrayList<>();
            for (Integer num : list) {
                if (num == 0) {
                    nonZeroSublists.addAll(trimOnes(nonZeroSublist));
                    nonZeroSublist = new ArrayList<>();
                } else
                    nonZeroSublist.add(num);
            }
            nonZeroSublists.addAll(trimOnes(nonZeroSublist));
            return nonZeroSublists;
        }

        private List<List<Integer>> trimOnes(List<Integer> list) {
            if (list.isEmpty()) return new ArrayList<>();
            if (list.stream().allMatch(n -> Math.abs(n) == 1)) {
                if (list.size() == 1) return Collections.singletonList(list);
                return Arrays.asList(Collections.singletonList(1), Collections.singletonList(-1));
            }
            int leadingLength = 0;
            while (leadingLength < list.size()) {
                if (Math.abs(list.get(leadingLength)) == 1)
                    leadingLength++;
                else break;
            }
            int trainlingLength = 0;
            while (list.size() - 1 - trainlingLength >= 0) {
                if (Math.abs(list.get(list.size() - 1 - trainlingLength)) == 1)
                    trainlingLength++;
                else break;
            }
            List<List<Integer>> trimmed = new ArrayList<>();
            trimmed.add(list.subList(leadingLength, list.size() - trainlingLength));
            List<Integer> leading = leadingLength > 0 ? list.subList(0, leadingLength) : new ArrayList<>();
            List<Integer> trailing = trainlingLength > 0 ? list.subList(list.size() - trainlingLength, list.size()) : new ArrayList<>();
            if (leading.contains(1) || trailing.contains(1)) trimmed.add(Collections.singletonList(1));
            if ((leading.contains(-1) || trailing.contains(-1))) {
                List<Integer> opposite = new ArrayList<>(trimmed.get(0));
                opposite.add(-1);
                trimmed.add(opposite);
            }
            return trimmed;
        }
    }
}
