package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public interface BestTimeToBuyAndSellStockWithCooldown {

  int maxProfit(int[] prices);

  class Solution implements BestTimeToBuyAndSellStockWithCooldown {

    private static class Profit {
      int i;
      boolean bought, cooldown;

      public Profit(int i, boolean bought, boolean cooldown) {
        this.i = i;
        this.bought = bought;
        this.cooldown = cooldown;
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profit profit = (Profit) o;
        return i == profit.i &&
          bought == profit.bought &&
          cooldown == profit.cooldown;
      }

      @Override
      public int hashCode() {
        return Objects.hash(i, bought, cooldown);
      }
    }

    private final Map<Profit, Integer> cache = new HashMap<>();

    private int[] prices;

    @Override
    public int maxProfit(int[] prices) {
      this.prices = prices;
      return nextProfit(new Profit(0, false, false));
    }

    private int nextProfit(Profit profit) {
      if (profit.i >= prices.length) {
        return 0;
      }
      if (profit.i == prices.length - 1) {
        return profit.bought ? prices[profit.i] : 0;
      }
      if (cache.containsKey(profit)) {
        return cache.get(profit);
      }
      int p;
      if (profit.cooldown) {
        p = nextProfit(new Profit(profit.i + 1, false, false));
      } else {
        if (profit.bought) {
          p = Math.max(
            nextProfit(new Profit(profit.i + 1, true, false)),
            nextProfit(new Profit(profit.i + 1, false, true)) + prices[profit.i]);
        } else {
          p = Math.max(
            nextProfit(new Profit(profit.i + 1, false, false)),
            nextProfit(new Profit(profit.i + 1, true, false)) - prices[profit.i]);
        }
      }
      cache.put(profit, p);
      return p;
    }
  }
}
