package leetcode.dynamic_programming;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public interface MinimumCostForTickets {

    int mincostTickets(int[] days, int[] costs);

    class Solution implements MinimumCostForTickets {

        private final Map<Integer, Integer> cache = new HashMap<>();

        private int[] days;
        private int t1, t7, t30;

        @Override
        public int mincostTickets(int[] days, int[] costs) {
            this.days = days;
            t1 = costs[0];
            t7 = costs[1];
            t30 = costs[2];
            return minCostFrom(0, 0);
        }

        private int minCostFrom(int i, int validUntil) {
            if (i >= days.length)
                return 0;
            int day = days[i];
            if (day <= validUntil)
                return minCostFrom(i + 1, validUntil);
            if (cache.containsKey(i))
                return cache.get(i);
            int cost = Stream.of(
                    t1 + minCostFrom(i + 1, day),
                    t7 + minCostFrom(i + 1, day + 6),
                    t30 + minCostFrom(i + 1, day + 29))
                    .min(Comparator.naturalOrder())
                    .get();
            cache.put(i, cost);
            return cost;
        }
    }
}
