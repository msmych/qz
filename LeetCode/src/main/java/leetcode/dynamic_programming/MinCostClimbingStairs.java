package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.Map;

public interface MinCostClimbingStairs {

    int minCostClimbingStairs(int[] cost);

    class Solution implements MinCostClimbingStairs {

        private final Map<Integer, Integer> cache = new HashMap<>();
        private int[] cost;

        @Override
        public int minCostClimbingStairs(int[] cost) {
            if (cost.length == 0)
                return 0;
            this.cost = cost;
            return Math.min(nextCost(0), nextCost(1));
        }

        private int nextCost(int i) {
            if (i >= cost.length)
                return 0;
            if (cache.containsKey(i))
                return cache.get(i);
            int c = cost[i];
            c += Math.min(nextCost(i + 1), nextCost(i + 2));
            cache.put(i, c);
            return c;
        }
    }
}
