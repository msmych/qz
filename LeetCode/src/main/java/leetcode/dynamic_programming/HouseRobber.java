package leetcode.dynamic_programming;

import java.util.HashMap;
import java.util.Map;

public interface HouseRobber {

    int rob(int[] nums);

    class Solution implements HouseRobber {

        private final Map<Integer, Integer> cache = new HashMap<>();

        private int[] nums;

        @Override
        public int rob(int[] nums) {
            this.nums = nums;
            return robNext(0);
        }

        private int robNext(int i) {
            if (i >= nums.length) return 0;
            if (i == nums.length - 1) return nums[i];
            if (cache.containsKey(i)) return cache.get(i);
            int max = Math.max(robNext(i + 1), nums[i] + robNext(i + 2));
            cache.put(i, max);
            return max;
        }
    }

    class SolutionII implements HouseRobber {

        private final Map<Integer, Integer> pickedFirstCache = new HashMap<>();
        private final Map<Integer, Integer> notPickedFirstCache = new HashMap<>();

        private int[] nums;

        @Override
        public int rob(int[] nums) {
            if (nums.length == 1) return nums[0];
            this.nums = nums;
            return Math.max(robNextPickedFirst(0), robNextNotPickedFirst(1));
        }

        private int robNextPickedFirst(int i) {
            if (i >= nums.length - 1) return 0;
            if (pickedFirstCache.containsKey(i)) return pickedFirstCache.get(i);
            int max = Math.max(robNextPickedFirst(i + 1), nums[i] + robNextPickedFirst(i + 2));
            pickedFirstCache.put(i, max);
            return max;
        }

        private int robNextNotPickedFirst(int i) {
            if (i >= nums.length) return 0;
            if (i == nums.length - 1) return nums[i];
            if (notPickedFirstCache.containsKey(i)) return notPickedFirstCache.get(i);
            int max = Math.max(robNextNotPickedFirst(i + 1), nums[i] + robNextNotPickedFirst(i + 2));
            notPickedFirstCache.put(i, max);
            return max;
        }
    }
}
