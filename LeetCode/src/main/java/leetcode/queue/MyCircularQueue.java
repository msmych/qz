package leetcode.queue;

public class MyCircularQueue {

    private final int[] values;

    private int head = -1;
    private int tail = -1;

    /** Initialize your data structure here. Set the size of the queue to be k. */
    public MyCircularQueue(int k) {
        values = new int[k];
    }

    /** Insert an element into the circular queue. Return true if the operation is successful. */
    public boolean enQueue(int value) {
        if (tail == head + 1 || head == values.length - 1 && tail == 0)
            return false;
        if (head == values.length - 1)
            head = 0;
        else
            head++;
        if (tail == -1)
            tail = 0;
        values[head] = value;
        return true;
    }

    /** Delete an element from the circular queue. Return true if the operation is successful. */
    public boolean deQueue() {
        if (tail == -1)
            return false;
        if (tail == head) {
            tail = -1;
            head = -1;
        } else if (tail == values.length - 1)
            tail = 0;
        else
            tail++;
        return true;
    }

    /** Get the front item from the queue. */
    public int Front() {
        return tail == -1 ? -1 : values[tail];
    }

    /** Get the last item from the queue. */
    public int Rear() {
        return head == -1 ? -1 : values[head];
    }

    /** Checks whether the circular queue is empty or not. */
    public boolean isEmpty() {
        return head == -1;
    }

    /** Checks whether the circular queue is full or not. */
    public boolean isFull() {
        return tail == head + 1 || head == values.length - 1 && tail == 0;
    }
}

/**
 * Your MyCircularQueue object will be instantiated and called as such:
 * MyCircularQueue obj = new MyCircularQueue(k);
 * boolean param_1 = obj.enQueue(value);
 * boolean param_2 = obj.deQueue();
 * int param_3 = obj.Front();
 * int param_4 = obj.Rear();
 * boolean param_5 = obj.isEmpty();
 * boolean param_6 = obj.isFull();
 */