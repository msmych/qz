package leetcode.queue;

import java.util.ArrayList;
import java.util.List;

public class RecentCounter {

    private final List<Integer> calls = new ArrayList<>();
    private int head = -1;

    public RecentCounter() {}

    public int ping(int t) {
        if (head == -1)
            head = 0;
        calls.add(t);
        for (int i = head; i < calls.size() && t - calls.get(i) > 3000; i++) {
            head++;
        }
        return calls.size() - head;
    }
}

/**
 * Your RecentCounter object will be instantiated and called as such:
 * RecentCounter obj = new RecentCounter();
 * int param_1 = obj.ping(t);
 */
