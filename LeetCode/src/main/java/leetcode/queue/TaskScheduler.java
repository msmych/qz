package leetcode.queue;

import static java.lang.Math.min;
import static java.util.Arrays.sort;

public interface TaskScheduler {

  int leastInterval(char[] tasks, int n);

  class Solution implements TaskScheduler {

    @Override
    public int leastInterval(char[] tasks, int n) {
      if (n == 0) {
        return tasks.length;
      }
      int[] schedule = new int[26];
      for (char task : tasks) {
        schedule[task - 'A']++;
      }
      sort(schedule);
      int max = schedule[25] - 1;
      int idle = max * n;
      for (int i = 24; i >= 0 && schedule[i] > 0; i--) {
        idle -= min(schedule[i], max);
      }
      return idle > 0 ? tasks.length + idle : tasks.length;
    }
  }
}
