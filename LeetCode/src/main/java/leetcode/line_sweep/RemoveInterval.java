package leetcode.line_sweep;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.stream;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.empty;

public interface RemoveInterval {

  List<List<Integer>> removeInterval(int[][] intervals, int[] toBeRemoved);

  class Solution implements RemoveInterval {

    private int[] toRemove;

    @Override
    public List<List<Integer>> removeInterval(int[][] intervals, int[] toBeRemoved) {
      toRemove = toBeRemoved;
      return stream(intervals)
        .flatMap(this::maybeRemove)
        .sorted(comparingInt(i -> i[0]))
        .map(i -> Arrays.asList(i[0], i[1]))
        .collect(toList());
    }

    private Stream<int[]> maybeRemove(int[] interval) {
      if (toRemove[0] <= interval[0] && toRemove[1] >= interval[1]) {
        return empty();
      }
      if (toRemove[1] <= interval[0] || toRemove[0] >= interval[1]) {
        return Stream.of(interval);
      }
      if (toRemove[0] <= interval[0]) {
        return Stream.of(new int[]{toRemove[1], interval[1]});
      }
      if (toRemove[1] >= interval[1]) {
        return Stream.of(new int[]{interval[0], toRemove[0]});
      }
      return Stream.of(new int[]{interval[0], toRemove[0]}, new int[]{toRemove[1], interval[1]});
    }
  }
}
