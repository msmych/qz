package leetcode.line_sweep;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface TheSkylineProblem {

    List<List<Integer>> getSkyline(int[][] buildings);

    class Solution implements TheSkylineProblem {

        private class Point {
            int x;
            int h;
            boolean isLeft;

            public Point(int x, int h, boolean isLeft) {
                this.x = x;
                this.h = h;
                this.isLeft = isLeft;
            }
        }

        @Override
        public List<List<Integer>> getSkyline(int[][] buildings) {
            List<Point> points = Arrays.stream(buildings)
                    .flatMap(b -> Stream.of(new Point(b[0], b[2], true), new Point(b[1], b[2], false)))
                    .sorted(Comparator.comparingInt(p -> p.x))
                    .collect(Collectors.toList());
            List<List<Integer>> skyline = new ArrayList<>();
            Queue<Integer> queue = new PriorityQueue<>(Comparator.reverseOrder());
            queue.offer(0);
            int lastMax = 0;
            int i = 0;
            while (i < points.size()) {
                int x = points.get(i).x;
                for (; i < points.size() && points.get(i).x == x; i++) {
                    Point p = points.get(i);
                    if (p.isLeft) queue.offer(p.h);
                    else queue.remove(p.h);
                }
                if (queue.peek() != lastMax) {
                    skyline.add(Arrays.asList(x, queue.peek()));
                    lastMax = queue.peek();
                }
            }
            return skyline;
        }
    }
}
