package leetcode.geometry;

public interface MinimumTimeVisitingAllPoints {

  int minTimeToVisitAllPoints(int[][] points);

  class Solution implements MinimumTimeVisitingAllPoints {
    @Override
    public int minTimeToVisitAllPoints(int[][] points) {
      int[] point = points[0];
      int seconds = 0;
      for (int i = 1; i < points.length; i++) {
        while (point[0] != points[i][0] || point[1] != points[i][1]) {
          if (point[1] == points[i][1]) {
            moveHorizontally(points, point, i);
          } else if (point[0] == points[i][0]) {
            moveVertically(points, point, i);
          } else {
            moveHorizontally(points, point, i);
            moveVertically(points, point, i);
          }
          seconds++;
        }
      }
      return seconds;
    }

    private void moveHorizontally(int[][] points, int[] point, int i) {
      point[0] = point[0] < points[i][0] ? point[0] + 1 : point[0] - 1;
    }

    private void moveVertically(int[][] points, int[] point, int i) {
      point[1] = point[1] < points[i][1] ? point[1] + 1 : point[1] - 1;
    }
  }
}
