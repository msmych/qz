package leetcode.geometry;

public interface CheckIfItIsAStraightLine {

  boolean checkStraightLine(int[][] coordinates);

  class Solution implements CheckIfItIsAStraightLine {
    @Override
    public boolean checkStraightLine(int[][] coordinates) {
      if (coordinates.length < 3) {
        return true;
      }
      int[] a = coordinates[0];
      int[] b = coordinates[1];
      for (int i = 2; i < coordinates.length; i++) {
        int[] c = coordinates[i];
        if (a[0] * (b[1] - c[1]) + b[0] * (c[1] - a[1]) + c[0] * (a[1] - b[1]) != 0) {
          return false;
        }
      }
      return true;
    }
  }
}
