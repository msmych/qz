package leetcode.geometry;

public interface SurfaceAreaOf3DShapes {

    int surfaceArea(int[][] grid);

    class Solution implements SurfaceAreaOf3DShapes {

        @Override
        public int surfaceArea(int[][] grid) {
            int area = 0;
            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid.length; j++) {
                    int v = grid[i][j];
                    if (v == 0)
                        continue;
                    area += 2;
                    area += i > 0 ? Math.max(v - grid[i - 1][j], 0) : v;
                    area += i < grid.length - 1 ? Math.max(v - grid[i + 1][j], 0) : v;
                    area += j > 0 ? Math.max(v - grid[i][j - 1], 0) : v;
                    area += j < grid.length - 1 ? Math.max(v - grid[i][j + 1], 0) : v;
                }
            }
            return area;
        }
    }
}
