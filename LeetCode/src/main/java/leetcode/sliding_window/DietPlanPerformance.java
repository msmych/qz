package leetcode.sliding_window;

public interface DietPlanPerformance {

  int dietPlanPerformance(int[] calories, int k, int lower, int upper);

  class Solution implements DietPlanPerformance {
    @Override
    public int dietPlanPerformance(int[] calories, int k, int lower, int upper) {
      int points = 0;
      int cals = 0;
      for (int i = 0; i + k <= calories.length; i++) {
        if (i == 0) {
          for (int j = 0; j < k; j++) {
            cals += calories[j];
          }
        } else {
          cals -= calories[i - 1];
          cals += calories[i + k - 1];
        }
        if (cals < lower) {
          points--;
        } else if (cals > upper) {
          points++;
        }
      }
      return points;
    }
  }
}
