package leetcode.sliding_window;

public interface GetEqualSubstringsWithinBudget {

    int equalSubstring(String s, String t, int maxCost);

    class Solution implements GetEqualSubstringsWithinBudget {

        @Override
        public int equalSubstring(String s, String t, int maxCost) {
            int[] costs = new int[s.length()];
            for (int i = 0; i < costs.length; i++) {
                costs[i] = Math.abs(t.charAt(i) - s.charAt(i));
            }
            int max = 0;
            int cost = 0;
            for (int i = 0; i + max <= costs.length; i++) {
                if (i > 0) {
                    cost -= costs[i - 1];
                }
                for (int j = i + max; j < costs.length; j++) {
                    cost += costs[j];
                    if (cost > maxCost) {
                        break;
                    }
                    if (j - i + 1 > max) {
                        max = j - i + 1;
                    }
                }
            }
            return max;
        }
    }
}
