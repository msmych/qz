package leetcode.sliding_window;

public interface MovingStonesUntilConsecutiveII {

    int[] numMovesStonesII(int[] stones);

    class Solution implements MovingStonesUntilConsecutiveII {

        @Override
        public int[] numMovesStonesII(int[] stones) {
            return new int[0];
        }
    }
}
