package leetcode.sliding_window;

public interface GrumpyBookstoreOwner {

    int maxSatisfied(int[] customers, int[] grumpy, int X);

    class Solution implements GrumpyBookstoreOwner {

        @Override
        public int maxSatisfied(int[] customers, int[] grumpy, int X) {
            int max = 0;
            for (int i = 0; i < customers.length; i++) {
                if (i < X || grumpy[i] == 0) max += customers[i];
            }
            int lastMax = max;
            for (int i = 1; i <= customers.length - X; i++) {
                int satisfied = lastMax;
                if (grumpy[i - 1] == 1) satisfied -= customers[i - 1];
                if (grumpy[i + X - 1] == 1) satisfied += customers[i + X - 1];
                if (satisfied > max) max = satisfied;
                lastMax = satisfied;
            }
            return max;
        }
    }
}
