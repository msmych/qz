package leetcode.two_pointers;

import leetcode.linked_list.ListNode;

public interface PartitionList {

    ListNode partition(ListNode head, int x);

    class Solution implements PartitionList {

        @Override
        public ListNode partition(ListNode head, int x) {
            ListNode dummyHead = new ListNode(0);
            dummyHead.next = new ListNode(0);
            ListNode nextHead = dummyHead.next;
            ListNode dummyTail = new ListNode(0);
            dummyTail.next = new ListNode(0);
            ListNode nextTail = dummyTail.next;
            while (head != null) {
                if (head.val < x) {
                    nextHead.next = head;
                    nextHead = nextHead.next;
                } else {
                    nextTail.next = head;
                    nextTail = nextTail.next;
                }
                head = head.next;
            }
            nextTail.next = null;
            nextHead.next = dummyTail.next.next;
            return dummyHead.next.next;
        }
    }
}
