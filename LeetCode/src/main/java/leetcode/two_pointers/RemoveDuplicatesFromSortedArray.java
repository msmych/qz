package leetcode.two_pointers;

public interface RemoveDuplicatesFromSortedArray {

    int removeDuplicates(int[] nums);

    class Solution implements RemoveDuplicatesFromSortedArray {

        @Override
        public int removeDuplicates(int[] nums) {
            int k = 0;
            Integer last = null;
            for (int i = 0; i < nums.length; i++) {
                int num = nums[i];
                if (last == null || num != last) {
                    nums[k++] = num;
                    last = num;
                }
            }
            return k;
        }
    }

    class SolutionII implements RemoveDuplicatesFromSortedArray {

        @Override
        public int removeDuplicates(int[] nums) {
            if (nums.length <= 1)
                return nums.length;
            int k = 0;
            for (int i = 1; i < nums.length - 1; i++) {
                if (nums[i] != nums[i - 1] || nums[i] != nums[i + 1])
                    k++;
                nums[k] = nums[i];
            }
            nums[++k] = nums[nums.length - 1];
            return k + 1;
        }
    }
}
