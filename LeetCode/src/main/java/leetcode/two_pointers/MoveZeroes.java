package leetcode.two_pointers;

public interface MoveZeroes {

    void moveZeroes(int[] nums);

    class Solution implements MoveZeroes {

        @Override
        public void moveZeroes(int[] nums) {
            for (int slow = 0, fast = 0; fast < nums.length; fast++) {
                if (nums[fast] != 0) {
                    int n = nums[slow];
                    nums[slow++] = nums[fast];
                    nums[fast] = n;
                }
            }
        }
    }
}
