package leetcode.two_pointers;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public interface KDiffPairsInAnArray {

    int findPairs(int[] nums, int k);

    class Solution implements KDiffPairsInAnArray {

        private class Diff {
            int i, j;

            public Diff(int i, int j) {
                this.i = i;
                this.j = j;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Diff diff = (Diff) o;
                return i == diff.i &&
                    j == diff.j ||
                    i == diff.j &&
                        j == diff.i;
            }

            @Override
            public int hashCode() {
                return Objects.hash(i, j) + Objects.hash(j, i);
            }
        }

        @Override
        public int findPairs(int[] nums, int k) {
            Set<Diff> diffs = new HashSet<>();
            for (int i = 0; i < nums.length - 1; i++) {
                for (int j = i + 1; j < nums.length; j++) {
                    if (Math.abs(nums[j] - nums[i]) == k) {
                        diffs.add(new Diff(nums[i], nums[j]));
                    }
                }
            }
            return diffs.size();
        }
    }

}
