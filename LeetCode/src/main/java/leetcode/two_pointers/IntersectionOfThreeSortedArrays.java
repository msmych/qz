package leetcode.two_pointers;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.concat;

public interface IntersectionOfThreeSortedArrays {

  List<Integer> arraysIntersection(int[] arr1, int[] arr2, int[] arr3);

  class Solution implements IntersectionOfThreeSortedArrays {
    @Override
    public List<Integer> arraysIntersection(int[] arr1, int[] arr2, int[] arr3) {
      return concat(intersection(arr1, arr2, arr3),
        concat(intersection(arr2, arr1, arr3), intersection(arr3, arr1, arr2)))
        .distinct()
        .boxed()
        .collect(toList());
    }

    private IntStream intersection(int[] a, int[] b, int[] c) {
      return IntStream.of(a)
        .filter(n -> IntStream.of(b).anyMatch(bn -> bn == n))
        .filter(n -> IntStream.of(c).anyMatch(cn -> cn == n));
    }
  }
}
