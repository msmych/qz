package leetcode.two_pointers;

public interface ValidPalindrome {

    boolean isPalindrome(String s);

    class Solution implements ValidPalindrome {

        @Override
        public boolean isPalindrome(String s) {
            s = s.replaceAll("[^a-zA-Z0-9]", "");
            String reversed = new StringBuilder(s).reverse().toString();
            return s.equalsIgnoreCase(reversed);
        }
    }
}
