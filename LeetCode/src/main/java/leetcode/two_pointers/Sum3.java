package leetcode.two_pointers;

import java.util.*;

public interface Sum3 {

    List<List<Integer>> threeSum(int[] nums);

    class Solution implements Sum3 {

        @Override
        public List<List<Integer>> threeSum(int[] nums) {
            Arrays.sort(nums);
            List<List<Integer>> triplets = new LinkedList<>();
            for (int i = 0; i < nums.length - 2; i++) {
                if (i > 0 && nums[i] == nums[i - 1]) {
                    continue;
                }
                int left = i + 1;
                int right = nums.length - 1;
                int first = -nums[i];
                while (left < right) {
                    int secondThird = nums[left] + nums[right];
                    if (secondThird == first) {
                        triplets.add(Arrays.asList(nums[i], nums[left], nums[right]));
                        while (left < right && nums[left] == nums[left + 1]) {
                            left++;
                        }
                        while (left < right && nums[right] == nums[right - 1]) {
                            right--;
                        }
                        left++;
                        right--;
                    } else if (secondThird < first) {
                        left++;
                    } else {
                        right--;
                    }
                }
            }
            return triplets;
        }
    }
}
