package leetcode.two_pointers;

import java.util.ArrayList;
import java.util.List;

public interface LongPressedName {

    boolean isLongPressedName(String name, String typed);

    class Solution implements LongPressedName {

        @Override
        public boolean isLongPressedName(String name, String typed) {
            List<Pair> nameList = toList(name);
            List<Pair> typedList = toList(typed);
            if (typedList.size() != nameList.size())
                return false;
            for (int i = 0; i < nameList.size(); i++) {
                Pair namePair = nameList.get(i);
                Pair typedPair = typedList.get(i);
                if (typedPair.val != namePair.val || typedPair.occurrences < namePair.occurrences)
                    return false;
            }
            return true;
        }

        class Pair {
            char val;
            int occurrences;
        }

        private List<Pair> toList(String text) {
            List<Pair> list = new ArrayList<>();
            for (char t : text.toCharArray()) {
                if (list.isEmpty() || list.get(list.size() - 1).val != t)
                    list.add(new Pair() {{ val = t; occurrences = 1; }});
                else
                    list.get(list.size() - 1).occurrences++;
            }
            return list;
        }
    }
}
