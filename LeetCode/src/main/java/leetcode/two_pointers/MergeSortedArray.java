package leetcode.two_pointers;

public interface MergeSortedArray {

    void merge(int[] nums1, int m, int[] nums2, int n);

    class Solution implements MergeSortedArray {

        @Override
        public void merge(int[] nums1, int m, int[] nums2, int n) {
            boolean positive = false;
            for (int i = 0, j = 0; i < nums1.length && j < nums2.length; i++) {
                if (nums1[i] > 0)
                    positive = true;
                if (nums1[i] >= nums2[j]) {
                    if (nums1.length - 1 - i >= 0)
                        System.arraycopy(nums1, i, nums1, i + 1, nums1.length - 1 - i);
                    nums1[i] = nums2[j++];
                } else if (positive && nums1[i] == 0 || nums1[i] == 0 && nums1.length - i <= nums2.length - j)
                    nums1[i] = nums2[j++];
            }
        }
    }
}
