package leetcode.two_pointers;

public interface ImplementStrStr {

    int strStr(String haystack, String needle);

    class Solution implements ImplementStrStr {
        @Override
        public int strStr(String haystack, String needle) {
            if (needle.isEmpty())
                return 0;
            if (needle.length() > haystack.length())
                return -1;
            for (int i = 0; i < haystack.length() - needle.length() + 1; i++) {
                if (haystack.substring(i, i + needle.length()).equals(needle))
                    return i;
            }
            return -1;
        }
    }
}
