package leetcode.two_pointers;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface ReverseVowelsOfAString {

    String reverseVowels(String s);

    class Solution implements ReverseVowelsOfAString {

        private final Set<Character> vowels = Stream.of('a', 'e', 'i', 'u', 'o').collect(Collectors.toSet());

        @Override
        public String reverseVowels(String s) {
            int left = 0, right = s.length() - 1;
            StringBuilder sb = new StringBuilder();
            while (left < s.length() || right >= 0) {
                if (left < s.length()) {
                    if (isVowel(s.charAt(left))) {
                        if (isVowel(s.charAt(right))) {
                            sb.append(s.charAt(right--));
                            left++;
                        } else {
                            right--;
                        }
                    } else {
                        sb.append(s.charAt(left++));
                    }
                } else if (isVowel(s.charAt(right))) {
                    sb.append(s.charAt(right--));
                } else {
                    right--;
                }
            }
            return sb.toString();
        }

        private boolean isVowel(char c) {
            return vowels.contains(Character.toLowerCase(c));
        }
    }
}
