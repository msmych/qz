package leetcode.two_pointers;

public interface ContainerWithMostWater {

  int maxArea(int[] height);

  class Solution implements ContainerWithMostWater {

    @Override
    public int maxArea(int[] height) {
      int maxArea = 0;
      for (int i = 0; i < height.length - 1; i++) {
        for (int j = i + 1; j < height.length; j++) {
          int area = (j - i) * Math.min(height[i], height[j]);
          if (area > maxArea)
            maxArea = area;
        }
      }
      return maxArea;
    }
  }
}
