package leetcode.two_pointers;

import leetcode.Interval;

public interface IntervalListIntersections {

    Interval[] intervalIntersection(Interval[] A, Interval[] B);

    class Solution implements IntervalListIntersections {

        @Override
        public Interval[] intervalIntersection(Interval[] A, Interval[] B) {
            return new Interval[0];
        }
    }
}
