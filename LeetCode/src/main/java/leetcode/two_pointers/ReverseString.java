package leetcode.two_pointers;

public interface ReverseString {

    String reverseString(String s);

    class Solution implements ReverseString {

        @Override
        public String reverseString(String s) {
            int i = 0, j = s.length() - 1;
            char[] chars = new char[s.length()];
            while (i <= j) {
                chars[i] = s.charAt(j);
                chars[j] = s.charAt(i);
                i++;
                j--;
            }
            return new String(chars);
        }
    }

    class RecursiveSolution implements ReverseString {

        @Override
        public String reverseString(String s) {
            return s.isEmpty()
                    ? ""
                    : s.charAt(s.length() - 1) + reverseString(s.substring(0, s.length() - 1));
        }
    }
}
