package leetcode.two_pointers;

import static java.util.Arrays.stream;

public interface CountNumberOfNiceSubarrays {

  int numberOfSubarrays(int[] nums, int k);

  class Solution implements CountNumberOfNiceSubarrays {
    @Override
    public int numberOfSubarrays(int[] nums, int k) {
      int oddSubarrays = 0;
      for (int i = 0; i <= nums.length - k; i++) {
        int odds = (int) stream(nums, i, i + k).filter(n -> n % 2 == 1).count();
        if (odds > k) {
          continue;
        } else if (odds == k) {
          oddSubarrays++;
        }
        for (int j = i + k; j < nums.length; j++) {
          if (nums[j] % 2 == 1) {
            odds++;
          }
          if (odds > k) {
            break;
          } else if (odds == k) {
            oddSubarrays++;
          }
        }
      }
      return oddSubarrays;
    }
  }
}
