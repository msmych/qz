package leetcode.two_pointers;

public interface Sum3Closest {

    int threeSumClosest(int[] nums, int target);

    class Solution implements Sum3Closest {

        @Override
        public int threeSumClosest(int[] nums, int target) {
            int closest = Integer.MIN_VALUE;
            for (int i = 0; i < nums.length - 2; i++) {
                for (int j = i + 1; j < nums.length - 1; j++) {
                    int sum2 = nums[i] + nums[j];
                    for (int k = j + 1; k < nums.length; k++) {
                        int sum3 = sum2 + nums[k];
                        if (Math.abs(target - sum3) < Math.abs((long) target - closest))
                            closest = sum3;
                    }
                }
            }
            return closest;
        }
    }
}
