package leetcode.hash_table;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface TopKFrequentElements {

  List<Integer> topKFrequent(int[] nums, int k);

  class Solution implements TopKFrequentElements {

    @Override
    public List<Integer> topKFrequent(int[] nums, int k) {
      Map<Integer, Integer> frequencies = new HashMap<>();
      for (int num : nums) {
        frequencies.merge(num, 1, Integer::sum);
      }
      return frequencies.entrySet().stream()
        .sorted((f1, f2) -> Integer.compare(f2.getValue(), f1.getValue()))
        .map(Map.Entry::getKey)
        .limit(k)
        .collect(Collectors.toList());
    }
  }
}
