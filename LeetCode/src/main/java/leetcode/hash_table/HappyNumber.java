package leetcode.hash_table;

import java.util.HashSet;
import java.util.Set;

public interface HappyNumber {

    boolean isHappy(int n);

    class Solution implements HappyNumber {

        @Override
        public boolean isHappy(int n) {
            Set<Integer> numberSet = new HashSet<>();
            do {
                numberSet.add(n);
                n = next(n);
                if (n == 1)
                    return true;
            } while (!numberSet.contains(n));
            return false;
        }

        private int next(int n) {
            int sum = 0;
            while (n > 0) {
                int digit = n % 10;
                sum += digit * digit;
                n = n / 10;
            }
            return sum;
        }
    }
}
