package leetcode.hash_table;

import java.util.*;
import java.util.stream.Collectors;

public interface BullsAndCows {

  String getHint(String secret, String guess);

  class Solution implements BullsAndCows {

    @Override
    public String getHint(String secret, String guess) {
      Map<Character, Set<Integer>> secretDigits = digitsPositions(secret);
      Map<Character, Set<Integer>> guessDigits = digitsPositions(guess);
      int a = 0, b = 0;
      for (Map.Entry<Character, Set<Integer>> g : guessDigits.entrySet()) {
        char c = g.getKey();
        if (secretDigits.containsKey(c)) {
          Set<Integer> secretPositions = secretDigits.get(c);
          Map<Boolean, Long> bullsCows = g.getValue().stream()
            .collect(Collectors.partitioningBy(secretPositions::contains, Collectors.counting()));
          a += bullsCows.get(true);
          b += Math.min(bullsCows.get(false), secretPositions.size() - bullsCows.get(true));
        }
      }
      return a + "A" + b + "B";
    }

    private Map<Character, Set<Integer>> digitsPositions(String s) {
      Map<Character, Set<Integer>> digitsPositions = new HashMap<>();
      for (int i = 0; i < s.toCharArray().length; i++) {
        char c = s.charAt(i);
        if (digitsPositions.containsKey(c)) {
          digitsPositions.get(c).add(i);
        } else {
          Set<Integer> positions = new HashSet<>();
          positions.add(i);
          digitsPositions.put(c, positions);
        }
      }
      return digitsPositions;
    }
  }
}
