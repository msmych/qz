package leetcode.hash_table;

import java.util.HashMap;
import java.util.Map;

public interface JewelsAndStones {

    int numJewelsInStones(String J, String S);

    class Solution implements JewelsAndStones {

        @Override
        public int numJewelsInStones(String J, String S) {
            int jewels = 0;
            Map<Character, Integer> stones = new HashMap<>();
            for (char s : S.toCharArray()) {
                stones.put(s, stones.getOrDefault(s, 0) + 1);
            }
            for (char j : J.toCharArray()) {
                if (stones.containsKey(j))
                    jewels += stones.get(j);
            }
            return jewels;
        }
    }
}
