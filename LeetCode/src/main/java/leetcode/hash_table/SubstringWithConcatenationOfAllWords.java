package leetcode.hash_table;

import java.util.*;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public interface SubstringWithConcatenationOfAllWords {

    List<Integer> findSubstring(String s, String[] words);

    class Solution implements SubstringWithConcatenationOfAllWords {

        private final Map<Integer, String> cache = new HashMap<>();

        private String s;
        private Map<String, Integer> wordMap;
        private int wordCount;
        private int wordLength;

        @Override
        public List<Integer> findSubstring(String s, String[] words) {
            this.s = s;
            if (words.length == 0) {
                return emptyList();
            }
            wordMap = Arrays.stream(words).collect(groupingBy(word -> word, summingInt(word -> 1)));
            wordCount = words.length;
            wordLength = words[0].length();
            List<Integer> starting = new ArrayList<>();
            for (int i = 0; i <= s.length() - wordCount * wordLength; i++) {
                if (isConcatenation(i)) {
                    starting.add(i);
                }
            }
            return starting;
        }

        private boolean isConcatenation(int i) {
            Map<String, Integer> map = new HashMap<>(wordMap);
            for (int j = i; j < i + wordCount * wordLength; j += wordLength) {
                String word;
                if (cache.containsKey(j)) {
                    word = cache.get(j);
                } else {
                    word = s.substring(j, j + wordLength);
                    cache.put(j, word);
                }
                if (!map.containsKey(word) || map.get(word) < 1) {
                    return false;
                } else {
                    map.merge(word, -1, Integer::sum);
                }
            }
            return true;
        }
    }
}
