package leetcode.hash_table;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface LongestHarmoniousSubsequence {

    int findLHS(int[] nums);

    class Solution implements LongestHarmoniousSubsequence {

        @Override
        public int findLHS(int[] nums) {
            Set<Integer> set = Arrays.stream(nums).boxed().collect(Collectors.toSet());
            Map<Set<Integer>, Integer> map = new HashMap<>();
            for (int num : nums) {
                if (set.contains(num - 1)) {
                    Set<Integer> duplex = Stream.of(num, num - 1).collect(Collectors.toSet());
                    map.put(duplex, map.getOrDefault(duplex, 0) + 1);
                }
                if (set.contains(num + 1)) {
                    Set<Integer> duplex = Stream.of(num, num + 1).collect(Collectors.toSet());
                    map.put(duplex, map.getOrDefault(duplex, 0) + 1);
                }
            }
            return map.values().stream().mapToInt(n -> n).max().orElse(0);
        }
    }

}
