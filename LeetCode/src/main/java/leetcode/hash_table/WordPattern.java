package leetcode.hash_table;

import java.util.HashMap;
import java.util.Map;

public interface WordPattern {

  boolean wordPattern(String pattern, String str);

  class Solution implements WordPattern {
    @Override
    public boolean wordPattern(String pattern, String str) {
      String[] words = str.split(" ");
      if (pattern.length() != words.length) {
        return false;
      }
      Map<Character, String> map = new HashMap<>();
      for (int i = 0; i < pattern.toCharArray().length; i++) {
        char c = pattern.charAt(i);
        String word = words[i];
        if (map.containsKey(c)) {
          if (!map.get(c).equals(word)) {
            return false;
          }
        } else {
          if (map.values().stream().anyMatch(word::equals)) {
            return false;
          }
          map.put(c, word);
        }
      }
      return true;
    }
  }
}
