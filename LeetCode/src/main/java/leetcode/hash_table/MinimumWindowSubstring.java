package leetcode.hash_table;

import java.util.HashMap;
import java.util.Map;

public interface MinimumWindowSubstring {

  String minWindow(String s, String t);

  class Solution implements MinimumWindowSubstring {

    @Override
    public String minWindow(String s, String t) {
      if (t.length() > s.length())
        return "";
      String minWindow = "";
      Map<Character, Integer> target = new HashMap<>();
      for (char c : t.toCharArray()) {
        target.put(c, target.getOrDefault(c, 0) + 1);
      }
      Map<Character, Integer> window = new HashMap<>();
      int i = 0, j = 0;
      for (; j < t.length(); j++) {
        char c = s.charAt(j);
        window.put(c, window.getOrDefault(c, 0) + 1);
      }
      while (i <= s.length() - t.length() && j <= s.length() && i < j) {
        boolean valid = true;
        for (Map.Entry<Character, Integer> entry : target.entrySet()) {
          if (!window.containsKey(entry.getKey())
            || window.get(entry.getKey()) < entry.getValue()) {
            valid = false;
            break;
          }
        }
        if (valid) {
          if (minWindow.isEmpty() || j - i < minWindow.length())
            minWindow = s.substring(i, j);
          char c = s.charAt(i++);
          if (window.get(c) > 0)
            window.put(c, window.get(c) - 1);
        } else if (j < s.length()) {
          char c = s.charAt(j++);
          window.put(c, window.getOrDefault(c, 0) + 1);
        } else
          break;
      }
      return minWindow;
    }
  }
}
