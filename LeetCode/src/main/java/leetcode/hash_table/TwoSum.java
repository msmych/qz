package leetcode.hash_table;

import java.util.HashMap;
import java.util.Map;

public interface TwoSum {

    int[] twoSum(int[] nums, int target);

    class Solution implements TwoSum {

        @Override
        public int[] twoSum(int[] nums, int target) {
            for (int i = 0; i < nums.length; i++) {
                for (int j = i + 1; j < nums.length; j++) {
                    if (nums[i] + nums[j] == target) {
                        return new int[]{i, j};
                    }
                }
            }
            throw new IllegalArgumentException();
        }
    }

    class HashMapSolution implements TwoSum {

        @Override
        public int[] twoSum(int[] nums, int target) {
            Map<Integer, Integer> map = new HashMap<>();
            for (int i = 0; i < nums.length; i++) {
                int difference = target - nums[i];
                if (map.containsKey(difference))
                    return new int[]{map.get(difference), i};
                map.put(nums[i], i);
            }
            throw new IllegalArgumentException();
        }
    }
}
