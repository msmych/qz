package leetcode.hash_table;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface LongestPalindrome {

    int longestPalindrome(String s);

    class Solution implements LongestPalindrome {

        @Override
        public int longestPalindrome(String s) {
            Map<Character, Integer> occurrences = new HashMap<>();
            for (char c : s.toCharArray()) {
                occurrences.merge(c, 1, Integer::sum);
            }
            Map<Boolean, List<Integer>> oddEven = occurrences.values().stream()
                .collect(Collectors.partitioningBy(v -> v % 2 == 0));
            return oddEven.get(false).stream().mapToInt(i -> i).max().orElse(0)
                + oddEven.get(false).stream().sorted(Comparator.reverseOrder()).skip(1).mapToInt(i -> i - 1).sum()
                + oddEven.get(true).stream().mapToInt(i -> i).sum();
        }
    }

}
