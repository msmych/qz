package leetcode.hash_table;

import java.util.*;

public interface FindWordsThatCanBeFormedByCharacters {

  int countCharacters(String[] words, String chars);

  class Solution implements FindWordsThatCanBeFormedByCharacters {

    private Map<Character, Integer> charsOccurrences;

    @Override
    public int countCharacters(String[] words, String chars) {
      charsOccurrences = occurrences(chars);
      return Arrays.stream(words)
        .map(this::occurrences)
        .filter(this::isGood)
        .map(Map::values)
        .flatMap(Collection::stream)
        .mapToInt(i -> i)
        .sum();
    }

    private Map<Character, Integer> occurrences(String word) {
      Map<Character, Integer> occurrences = new HashMap<>();
      for (char c : word.toCharArray()) {
        occurrences.merge(c, 1, Integer::sum);
      }
      return occurrences;
    }

    private boolean isGood(Map<Character, Integer> word) {
      for (Map.Entry<Character, Integer> entry : word.entrySet()) {
        if (!charsOccurrences.containsKey(entry.getKey())
          || charsOccurrences.get(entry.getKey()) < entry.getValue()) {
          return false;
        }
      }
      return true;
    }
  }
}
