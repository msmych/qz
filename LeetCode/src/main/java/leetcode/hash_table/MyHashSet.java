package leetcode.hash_table;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MyHashSet extends MyAbstractHashSet {

    private final List[] buckets = new List[1000];

    @Override
    public void add(int key) {
        int hash = hashCode(key);
        List bucket = buckets[hash];
        if (bucket == null)
            bucket = new ArrayList(1000);
        for (Object k : bucket) {
            if (k.equals(key)) return;
        }
        bucket.add(key);
        buckets[hash] = bucket;
    }

    @Override
    public void remove(int key) {
        List bucket = buckets[hashCode(key)];
        if (bucket == null) return;
        Iterator iterator = bucket.iterator();
        while (iterator.hasNext()) {
            Object k = iterator.next();
            if (k.equals(key)) {
                iterator.remove();
                return;
            }
        }
    }

    @Override
    public boolean contains(int key) {
        List bucket = buckets[hashCode(key)];
        if (bucket == null) return false;
        for (Object k : bucket) {
            if (k.equals(key))
                return true;
        }
        return false;
    }

    private int hashCode(int key) {
        return key % 1000;
    }
}
