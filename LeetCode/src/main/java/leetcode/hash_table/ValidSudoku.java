package leetcode.hash_table;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public interface ValidSudoku {

    boolean isValidSudoku(char[][] board);

    class Solution implements ValidSudoku {

        @Override
        public boolean isValidSudoku(char[][] board) {
            Map<Integer, Set<Character>> rows = new HashMap<>();
            Map<Integer, Set<Character>> columns = new HashMap<>();
            Map<Integer, Set<Character>> boxes = new HashMap<>();
            for (int i = 0; i < board.length; i++) {
                char[] line = board[i];
                for (int j = 0; j < line.length; j++) {
                    char c = line[j];
                    if (c == '.')
                        continue;
                    rows.putIfAbsent(i, new HashSet<>());
                    if (rows.get(i).contains(c))
                        return false;
                    rows.get(i).add(c);
                    columns.putIfAbsent(j, new HashSet<>());
                    if (columns.get(j).contains(c))
                        return false;
                    columns.get(j).add(c);
                    int boxIndex = getBoxIndex(i, j);
                    boxes.putIfAbsent(boxIndex, new HashSet<>());
                    if (boxes.get(boxIndex).contains(c))
                        return false;
                    boxes.get(boxIndex).add(c);
                }
            }
            return true;
        }

        private int getBoxIndex(int i, int j) {
            return (i - i % 3) + j / 3;
        }
    }
}
