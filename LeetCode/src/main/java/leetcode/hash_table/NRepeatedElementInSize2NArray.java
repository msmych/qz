package leetcode.hash_table;

import java.util.HashSet;
import java.util.Set;

public interface NRepeatedElementInSize2NArray {

    int repeatedNTimes(int[] A);

    class Solution implements NRepeatedElementInSize2NArray {

        @Override
        public int repeatedNTimes(int[] A) {
            Set<Integer> elements = new HashSet<>();
            for (int a : A) {
                if (elements.contains(a))
                    return a;
                elements.add(a);
            }
            throw new IllegalArgumentException();
        }
    }
}
