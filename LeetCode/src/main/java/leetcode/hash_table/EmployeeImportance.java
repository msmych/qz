package leetcode.hash_table;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface EmployeeImportance {

    // Employee info
    class Employee {
        // It's the unique id of each node;
        // unique id of this employee
        public int id;
        // the importance value of this employee
        public int importance;
        // the id of direct subordinates
        public List<Integer> subordinates;
    }

    int getImportance(List<Employee> employees, int id);

    class Solution implements EmployeeImportance {

        private Map<Integer, Employee> map;

        @Override
        public int getImportance(List<Employee> employees, int id) {
            map = employees.stream().collect(Collectors.toMap(e -> e.id, e -> e));
            return nextSum(id);
        }

        private int nextSum(int id) {
            Employee employee = map.get(id);
            return employee.importance +
                    employee.subordinates.stream()
                    .mapToInt(this::nextSum)
                    .sum();
        }
    }
}
