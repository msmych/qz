package leetcode.hash_table;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

public interface SetMismatch {

    int[] findErrorNums(int[] nums);

    class Solution implements SetMismatch {

        @Override
        public int[] findErrorNums(int[] nums) {
            int[] error = new int[2];
            Set<Integer> set = new HashSet<>();
            int min = Integer.MAX_VALUE;
            int max = Integer.MIN_VALUE;
            for (int num : nums) {
                if (set.contains(num)) error[0] = num;
                else set.add(num);
                if (num < min) min = num;
                if (num > max) max = num;
            }
            if (min > 1) error[1] = 1;
            else error[1] = IntStream.range(min, max)
                    .filter(num -> !set.contains(num))
                    .findAny()
                    .orElse(max + 1);
            return error;
        }
    }
}
