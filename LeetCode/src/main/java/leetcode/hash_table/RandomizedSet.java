package leetcode.hash_table;

import java.util.*;

public class RandomizedSet extends AbstractRandomizedSet {

    private final Map<Integer, Integer> map = new HashMap<>();
    private final List<Integer> keys = new ArrayList<>();
    private final Random random = new Random();

    @Override
    public boolean insert(int val) {
        if (map.containsKey(val))
            return false;
        map.put(val, keys.size());
        keys.add(val);
        return true;
    }

    @Override
    public boolean remove(int val) {
        if (!map.containsKey(val))
            return false;
        int index = map.get(val);
        if (index != keys.size() - 1) {
            int last = keys.get(keys.size() - 1);
            keys.set(index, last);
            map.put(last, index);
        }
        map.remove(val);
        keys.remove(keys.size() - 1);
        return true;
    }

    @Override
    public int getRandom() {
        return keys.get(random.nextInt(keys.size()));
    }
}
