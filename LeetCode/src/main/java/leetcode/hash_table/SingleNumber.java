package leetcode.hash_table;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public interface SingleNumber {

    int singleNumber(int[] nums);

    class Solution implements SingleNumber {

        @Override
        public int singleNumber(int[] nums) {
            Set<Integer> numsSet = new HashSet<>();
            for (int num : nums) {
                if (numsSet.contains(num)) {
                    numsSet.remove(num);
                } else {
                    numsSet.add(num);
                }
            }
            return numsSet.iterator().next();
        }
    }

    class SumSolution implements SingleNumber {

        @Override
        public int singleNumber(int[] nums) {
            return 2 * Arrays.stream(nums).distinct().sum() - Arrays.stream(nums).sum();
        }
    }

    class SolutionII implements SingleNumber {

        @Override
        public int singleNumber(int[] nums) {
            return (int) ((
                    3 * Arrays.stream(nums).distinct().mapToLong(num -> num).sum() -
                            Arrays.stream(nums).mapToLong(num -> num).sum())
                    / 2);
        }
    }
}
