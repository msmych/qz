package leetcode.hash_table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;

public interface GroupAnagrams {

  List<List<String>> groupAnagrams(String[] strs);

  class Solution implements GroupAnagrams {

    @Override
    public List<List<String>> groupAnagrams(String[] strs) {
      return new ArrayList<>(Arrays.stream(strs)
        .collect(groupingBy(this::occurrences, toList()))
        .values());
    }

    private Map<Character, Integer> occurrences(String s) {
      return s.chars()
        .mapToObj(c -> (char) c)
        .collect(groupingBy(c -> c, summingInt(c -> 1)));
    }
  }
}
