package leetcode.hash_table;

public interface VerifyingAnAlienDictionary {

    boolean isAlienSorted(String[] words, String order);

    class Solution implements VerifyingAnAlienDictionary {

        @Override
        public boolean isAlienSorted(String[] words, String order) {
            for (int i = 0; i < words.length - 1; i++) {
                String word = words[i], next = words[i + 1];
                if (word.startsWith(next) && word.length() > next.length())
                    return false;
                for (int j = 0; j < word.length(); j++) {
                    if (j >= next.length())
                        break;
                    char w = word.charAt(j), n = next.charAt(j);
                    if (order.indexOf(n) < order.indexOf(w))
                        return false;
                    if (n != w)
                        break;
                }
            }
            return true;
        }
    }
}
