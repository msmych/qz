package leetcode.hash_table;

import java.util.*;

public interface IslandPerimeter {

    int islandPerimeter(int[][] grid);

    class Solution implements IslandPerimeter {

        private static class Cell {
            int i, j;

            public Cell(int i, int j) {
                this.i = i;
                this.j = j;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Cell cell = (Cell) o;
                return i == cell.i &&
                    j == cell.j;
            }

            @Override
            public int hashCode() {
                return Objects.hash(i, j);
            }
        }

        private final Set<Cell> visited = new HashSet<>();
        private int[][] grid;

        @Override
        public int islandPerimeter(int[][] grid) {
            if (grid.length == 0) {
                return 0;
            }
            this.grid = grid;
            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[0].length; j++) {
                    if (grid[i][j] == 1) {
                        return perimeter(i, j);
                    }
                }
            }
            return 0;
        }

        private int perimeter(int i, int j) {
            if (i < 0 || j < 0 || i >= grid.length || j >= grid[0].length) {
                return 1;
            }
            if (grid[i][j] == 0) {
                return 1;
            }
            Cell cell = new Cell(i, j);
            if (visited.contains(cell)) {
                return 0;
            }
            visited.add(cell);
            return perimeter(i, j - 1) + perimeter(i, j + 1) + perimeter(i - 1, j) + perimeter(i + 1, j);
        }
    }
}
