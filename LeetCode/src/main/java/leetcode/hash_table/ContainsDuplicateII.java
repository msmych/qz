package leetcode.hash_table;

import java.util.*;

public interface ContainsDuplicateII {

    boolean containsNearbyDuplicate(int[] nums, int k);

    class Solution implements ContainsDuplicateII {

        @Override
        public boolean containsNearbyDuplicate(int[] nums, int k) {
            Map<Integer, List<Integer>> map = toMap(nums);
            for (List<Integer> indexes : map.values()) {
                indexes.sort(Comparator.naturalOrder());
                for (int i = 0; i < indexes.size() - 1; i++) {
                    if (indexes.get(i + 1) - indexes.get(i) <= k) {
                        return true;
                    }
                }
            }
            return false;
        }

        private Map<Integer, List<Integer>> toMap(int[] nums) {
            Map<Integer, List<Integer>> map = new HashMap<>();
            for (int i = 0; i < nums.length; i++) {
                int num = nums[i];
                map.putIfAbsent(num, new ArrayList<>());
                map.get(num).add(i);
            }
            return map;
        }
    }
}
