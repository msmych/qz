package leetcode.hash_table;

import java.util.HashMap;
import java.util.Map;

public interface IsomorphicString {

    boolean isIsomorphic(String s, String t);

    class Solution implements IsomorphicString {

        @Override
        public boolean isIsomorphic(String s, String t) {
            Map<Character, Character> map = new HashMap<>();
            for (int i = 0; i < s.length(); i++) {
                char sc = s.charAt(i);
                char tc = t.charAt(i);
                if (map.containsKey(sc) || map.containsValue(tc)) {
                    if (!map.containsKey(sc) || map.get(sc) != tc)
                        return false;
                } else {
                    map.put(sc, tc);
                }
            }
            return true;
        }
    }
}
