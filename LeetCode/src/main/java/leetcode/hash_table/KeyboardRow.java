package leetcode.hash_table;

import java.util.*;
import java.util.stream.Collectors;

public interface KeyboardRow {

    String[] findWords(String[] words);

    class Solution implements KeyboardRow {

        private final static Set<Character> first = keyboardRow("qwertyuiop");
        private final static Set<Character> second = keyboardRow("asdfghjkl");
        private final static Set<Character> third = keyboardRow("zxcvbnm");

        private static Set<Character> keyboardRow(String line) {
            Set<Character> row = new HashSet<>();
            for (char c : line.toCharArray()) {
                row.add(c);
            }
            return row;
        }

        @Override
        public String[] findWords(String[] words) {
            return Arrays.stream(words)
                    .filter(this::isOnSingleLine)
                    .collect(Collectors.toList())
                    .toArray(new String[]{});
        }

        private boolean isOnSingleLine(String s) {
            Set<Set<Character>> set = new HashSet<>();
            for (char c : s.toCharArray()) {
                char lowerChar = Character.toLowerCase(c);
                if (first.contains(lowerChar)) {
                    set.add(first);
                } else if (second.contains(lowerChar)) {
                    set.add(second);
                } else if (third.contains(lowerChar)) {
                    set.add(third);
                }
            }
            return set.size() == 1;
        }
    }
}
