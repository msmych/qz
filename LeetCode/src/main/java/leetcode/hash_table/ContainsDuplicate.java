package leetcode.hash_table;

import java.util.HashSet;
import java.util.Set;

public interface ContainsDuplicate {

    boolean containsDuplicate(int[] nums);

    class Solution implements ContainsDuplicate {

        @Override
        public boolean containsDuplicate(int[] nums) {
            Set<Integer> numSet = new HashSet<>();
            for (int num : nums) {
                if (numSet.contains(num))
                    return true;
                numSet.add(num);
            }
            return false;
        }
    }
}
