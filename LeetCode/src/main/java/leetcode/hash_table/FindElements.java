package leetcode.hash_table;

import leetcode.tree.TreeNode;

import java.util.HashSet;
import java.util.Set;

public class FindElements {

  private final Set<Integer> vals = new HashSet<>();

  public FindElements(TreeNode root) {
    traverse(root, 0);
  }

  private void traverse(TreeNode node, int val) {
    if (node == null) {
      return;
    }
    vals.add(val);
    traverse(node.left, 2 * val + 1);
    traverse(node.right, 2 * val + 2);
  }

  public boolean find(int target) {
    return vals.contains(target);
  }
}
