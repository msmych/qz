package leetcode.hash_table;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public interface DistributeCandies {

    int distributeCandies(int[] candies);

    class Solution implements DistributeCandies {

        @Override
        public int distributeCandies(int[] candies) {
            Map<Integer, Integer> map = new HashMap<>();
            for (int candy : candies) {
                map.put(candy, map.getOrDefault(candy, 0) + 1);
            }
            Set<Integer> kinds = new HashSet<>();
            int i = 0;
            while (i < candies.length / 2) {
                for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                    if (i < candies.length / 2) {
                        if (entry.getValue() > 0) {
                            kinds.add(entry.getKey());
                            i++;
                            map.put(entry.getKey(), entry.getValue() - 1);
                        }
                    } else {
                        break;
                    }
                }
            }
            return kinds.size();
        }
    }
}
