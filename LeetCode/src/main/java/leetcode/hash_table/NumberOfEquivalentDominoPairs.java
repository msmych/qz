package leetcode.hash_table;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface NumberOfEquivalentDominoPairs {

    int numEquivDominoPairs(int[][] dominoes);

    class Solution implements NumberOfEquivalentDominoPairs {

        @Override
        public int numEquivDominoPairs(int[][] dominoes) {
            Map<Set<Integer>, Integer> map = new HashMap<>();
            for (int[] domino : dominoes) {
                Set<Integer> d = Stream.of(domino[0], domino[1]).collect(Collectors.toSet());
                map.put(d, map.getOrDefault(d, 0) + 1);
            }
            return map.values().stream().mapToInt(this::pairs).sum();
        }

        private int pairs(int d) {
            return d * (d - 1) / 2;
        }
    }
}
