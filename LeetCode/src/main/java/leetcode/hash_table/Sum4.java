package leetcode.hash_table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;

public interface Sum4 {

  List<List<Integer>> fourSum(int[] nums, int target);

  class Solution implements Sum4 {

    private int[] nums;

    @Override
    public List<List<Integer>> fourSum(int[] nums, int target) {
      this.nums = nums;
      Arrays.sort(nums);
      return nextSum(4, 0, target);
    }

    private List<List<Integer>> nextSum(int k, int index, int target) {
      if (index >= nums.length) {
        return emptyList();
      }
      if (k == 2) {
        return twoSum(index, target);
      }
      List<List<Integer>> sums = new ArrayList<>();
      for (int i = index; i < nums.length - k + 1; i++) {
        List<List<Integer>> nextSums = nextSum(k - 1, i + 1, target - nums[i]);
        for (List<Integer> nextSum : nextSums) {
          nextSum.add(0, nums[i]);
        }
        sums.addAll(nextSums);
        while (i < nums.length - 1 && nums[i] == nums[i + 1]) {
          i++;
        }
      }
      return sums;
    }

    private List<List<Integer>> twoSum(int index, int target) {
      List<List<Integer>> pairs = new ArrayList<>();
      int i = index;
      int j = nums.length - 1;
      while (i < j) {
        int sum = nums[i] + nums[j];
        if (sum == target) {
          List<Integer> pair = new ArrayList<>();
          pair.add(nums[i]);
          pair.add(nums[j]);
          pairs.add(pair);
          while (i < j && nums[i] == nums[i + 1]) {
            i++;
          }
          while (i < j && nums[j] == nums[j - 1]) {
            j--;
          }
          i++;
          j--;
        } else if (sum < target){
          i++;
        } else {
          j--;
        }
      }
      return pairs;
    }
  }
}
