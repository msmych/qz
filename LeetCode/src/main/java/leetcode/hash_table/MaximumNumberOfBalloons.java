package leetcode.hash_table;

import java.util.HashMap;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public interface MaximumNumberOfBalloons {

  int maxNumberOfBalloons(String text);

  class Solution implements MaximumNumberOfBalloons {
    @Override
    public int maxNumberOfBalloons(String text) {
      HashMap<Character, Integer> map = new HashMap<Character, Integer>() {{
        put('b', 1);
        put('a', 1);
        put('l', 2);
        put('o', 2);
        put('n', 1);
      }};
      Map<Character, Integer> frequency = text.chars()
        .mapToObj(c -> (char) c)
        .filter(map.keySet()::contains)
        .collect(groupingBy(c -> c, summingInt(c -> 1)));
      int max = 0;
      while (frequency.size() == map.size()
        && frequency.entrySet().stream()
        .noneMatch(e -> e.getValue() < map.get(e.getKey()))) {
        for (Map.Entry<Character, Integer> entry : frequency.entrySet()) {
          char c = entry.getKey();
          frequency.put(c, entry.getValue() - map.get(c));
        }
        max++;
      }
      return max;
    }
  }
}
