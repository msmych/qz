package leetcode.hash_table;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MyHashMap extends MyAbstractHashMap {

    class Entry {
        int key;
        int value;

        Entry(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    private final List[] buckets = new List[1000];

    @Override
    public void put(int key, int value) {
        int hash = hashCode(key);
        List bucket = buckets[hash];
        if (bucket == null)
            bucket = new LinkedList();
        for (Object e : bucket) {
            if (((Entry) e).key == key) {
                ((Entry) e).value = value;
                buckets[hash] = bucket;
                return;
            }
        }
        bucket.add(new Entry(key, value));
        buckets[hash] = bucket;
    }

    @Override
    public int get(int key) {
        List bucket = buckets[hashCode(key)];
        if (bucket == null) return -1;
        for (Object e : bucket) {
            if (((Entry) e).key == key) {
                return ((Entry) e).value;
            }
        }
        return -1;
    }

    @Override
    public void remove(int key) {
        List bucket = buckets[hashCode(key)];
        if (bucket == null) return;
        Iterator iterator = bucket.iterator();
        while (iterator.hasNext()) {
            if (((Entry) iterator.next()).key == key) {
                iterator.remove();
                return;
            }
        }
    }

    private int hashCode(int key) {
        return key % 1000;
    }
}
