package leetcode.hash_table;

import java.util.HashMap;
import java.util.Map;

public interface ShortestCompletingWord {

    String shortestCompletingWord(String licensePlate, String[] words);

    class Solution implements ShortestCompletingWord {

        @Override
        public String shortestCompletingWord(String licensePlate, String[] words) {
            Map<Character, Integer> licenseMap = new HashMap<>();
            for (char c : licensePlate.toCharArray()) {
                c = Character.toLowerCase(c);
                if (Character.isAlphabetic(c))
                    licenseMap.put(c, licenseMap.getOrDefault(c, 0) + 1);
            }
            String shortest = "";
            for (String word : words) {
                if (!shortest.isEmpty() && word.length() >= shortest.length())
                    continue;
                Map<Character, Integer> wordMap = new HashMap<>();
                for (char c : word.toCharArray()) {
                    c = Character.toLowerCase(c);
                    wordMap.put(c, wordMap.getOrDefault(c, 0) + 1);
                }
                boolean completing = true;
                for (Map.Entry<Character, Integer> entry : licenseMap.entrySet()) {
                    if (!wordMap.containsKey(entry.getKey()) || wordMap.get(entry.getKey()) < entry.getValue()) {
                        completing = false;
                        break;
                    }
                }
                if (completing)
                    shortest = word;
            }
            return shortest;
        }
    }
}
