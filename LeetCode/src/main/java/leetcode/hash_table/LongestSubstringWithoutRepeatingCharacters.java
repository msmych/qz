package leetcode.hash_table;

import java.util.HashMap;
import java.util.Map;

public interface LongestSubstringWithoutRepeatingCharacters {

    int lengthOfLongestSubstring(String s);

    class Solution implements LongestSubstringWithoutRepeatingCharacters {

        @Override
        public int lengthOfLongestSubstring(String s) {
            Map<Character, Integer> charIndexes = new HashMap<>();
            int maxLength = 0;
            for (int i = 0, j = 0; j < s.length(); j++) {
                char c = s.charAt(j);
                Integer index = charIndexes.get(c);
                if (index != null && index > i)
                    i = index;
                int length = j - i + 1;
                if (length > maxLength)
                    maxLength = length;
                charIndexes.put(c, j + 1);
            }
            return maxLength;
        }
    }
}
