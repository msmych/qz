package leetcode.hash_table;

import java.util.*;
import java.util.stream.Collectors;

public interface UncommonWordsFromTwoSentences {

    String[] uncommonFromSentences(String A, String B);

    class Solution implements UncommonWordsFromTwoSentences {

        @Override
        public String[] uncommonFromSentences(String A, String B) {
            Map<String, Integer> awords = new HashMap<>(),
                    bwords = new HashMap<>();
            List<String> uncommon = new ArrayList<>();
            for (String a : A.split(" "))
                awords.put(a, awords.getOrDefault(a, 0) + 1);
            for (String b : B.split(" "))
                bwords.put(b, bwords.getOrDefault(b, 0) + 1);
            uncommon.addAll(getUncommon(awords, bwords));
            uncommon.addAll(getUncommon(bwords, awords));
            return uncommon.toArray(new String[]{});
        }

        private List<String> getUncommon(Map<String, Integer> source, Map<String, Integer> target) {
            return source.entrySet().stream()
                    .filter(entry -> entry.getValue() == 1)
                    .map(Map.Entry::getKey)
                    .filter(word -> !target.containsKey(word))
                    .collect(Collectors.toList());
        }
    }
}
