package leetcode.hash_table;

public abstract class MyAbstractHashMap {

    /** Initialize your data structure here. */
    public MyAbstractHashMap() {}

    /** value will always be non-negative. */
    public abstract void put(int key, int value);

    /** Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key */
    public abstract int get(int key);

    /** Removes the mapping of the specified value key if this map contains a mapping for the key */
    public abstract void remove(int key);
}

/**
 * Your MyAbstractHashMap object will be instantiated and called as such:
 * MyAbstractHashMap obj = new MyAbstractHashMap();
 * obj.put(key,value);
 * int param_2 = obj.get(key);
 * obj.remove(key);
 */
