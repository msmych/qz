package leetcode.hash_table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface MinimumIndexSumOfTwoLists {

    String[] findRestaurant(String[] list1, String[] list2);

    class Solution implements MinimumIndexSumOfTwoLists {

        @Override
        public String[] findRestaurant(String[] list1, String[] list2) {
            Map<String, Integer> map1 = new HashMap<>();
            for (int i = 0; i < list1.length; i++) {
                map1.put(list1[i], i);
            }
            Map<Integer, List<String>> mutual = new HashMap<>();
            for (int i = 0; i < list2.length; i++) {
                String item2 = list2[i];
                if (map1.containsKey(item2)) {
                    int sum = map1.get(item2) + i;
                    mutual.putIfAbsent(sum, new ArrayList<>());
                    List<String> list = mutual.get(sum);
                    list.add(item2);
                    mutual.put(sum, list);
                }
            }
            int min = Integer.MAX_VALUE;
            for (Integer i : mutual.keySet()) {
                if (i < min) {
                    min = i;
                }
            }
            return mutual.get(min).toArray(new String[]{});
        }
    }
}
