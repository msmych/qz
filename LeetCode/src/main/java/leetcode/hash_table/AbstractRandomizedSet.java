package leetcode.hash_table;

public abstract class AbstractRandomizedSet {

    /** Initialize your data structure here. */
    public AbstractRandomizedSet() {}

    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    public abstract boolean insert(int val);

    /** Removes a value from the set. Returns true if the set contained the specified element. */
    public abstract boolean remove(int val);

    /** Get a random element from the set. */
    public abstract int getRandom();
}

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet obj = new RandomizedSet();
 * boolean param_1 = obj.insert(val);
 * boolean param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */