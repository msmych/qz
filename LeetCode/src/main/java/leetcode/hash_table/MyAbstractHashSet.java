package leetcode.hash_table;

public abstract class MyAbstractHashSet {

    /** Initialize your data structure here. */
    public MyAbstractHashSet() {}

    public abstract void add(int key);

    public abstract void remove(int key);

    /** Returns true if this set contains the specified element */
    public abstract boolean contains(int key);
}
/**
 * Your MyAbstractHashSet object will be instantiated and called as such:
 * MyAbstractHashSet obj = new MyAbstractHashSet();
 * obj.add(key);
 * obj.remove(key);
 * boolean param_3 = obj.contains(key);
 */
