package leetcode.hash_table;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public interface UniqueNumberOfOccurrences {

  boolean uniqueOccurrences(int[] arr);

  class Solution implements UniqueNumberOfOccurrences {
    @Override
    public boolean uniqueOccurrences(int[] arr) {
      Map<Integer, Integer> occurrencesMap = new HashMap<>();
      for (int n : arr) {
        occurrencesMap.merge(n, 1, Integer::sum);
      }
      Collection<Integer> occurrences = occurrencesMap.values();
      return occurrences.size() == occurrences.stream().distinct().count();
    }
  }
}
