package leetcode.hash_table;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface SubdomainVisitCount {

    List<String> subdomainVisits(String[] cpdomains);

    class Solution implements SubdomainVisitCount {

        @Override
        public List<String> subdomainVisits(String[] cpdomains) {
            Map<String, Integer> visits = new HashMap<>();
            for (String cpdomain : cpdomains) {
                String[] cpdomainParts = cpdomain.split(" ");
                int visitCount = Integer.valueOf(cpdomainParts[0]);
                String subdomain = cpdomainParts[1];
                int domainCount = subdomain.split("\\.").length,
                        i = 0;
                do {
                    visits.put(subdomain, visits.getOrDefault(subdomain, 0) + visitCount);
                    subdomain = subdomain.substring(subdomain.indexOf(".") + 1);
                } while (++i < domainCount);
            }
            return visits.entrySet().stream()
                    .map(visit -> visit.getValue() + " " + visit.getKey())
                    .collect(Collectors.toList());
        }
    }
}
