package leetcode.hash_table;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface CountPrimes {

    int countPrimes(int n);

    class Solution implements CountPrimes {

        @Override
        public int countPrimes(int n) {
            Set<Integer> nums = IntStream.range(2, n).boxed().collect(Collectors.toSet());
            for (int i = 2; i <= Math.sqrt(n); i++) {
                if (!nums.contains(i)) continue;
                for (int j = 2; j * i <= n; j++) {
                    nums.remove(j * i);
                }
            }
            return nums.size();
        }
    }
}
