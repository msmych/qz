package leetcode.hash_table;

import java.util.HashMap;
import java.util.Map;

public interface FirstUniqueCharacterInAString {

    int firstUniqChar(String s);

    class Solution implements FirstUniqueCharacterInAString {

        @Override
        public int firstUniqChar(String s) {
            Map<Character, Integer> map = new HashMap<>();
            for (char c : s.toCharArray()) {
                map.put(c, map.getOrDefault(c, 0) + 1);
            }
            for (int i = 0; i < s.length(); i++) {
                if (map.get(s.charAt(i)) == 1)
                    return i;
            }
            return -1;
        }
    }
}
