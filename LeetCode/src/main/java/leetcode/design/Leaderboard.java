package leetcode.design;

import java.util.HashMap;
import java.util.Map;

import static java.util.Comparator.reverseOrder;

public class Leaderboard {

  private final Map<Integer, Integer> scores;

  public Leaderboard() {
    scores = new HashMap<>();
  }

  public void addScore(int playerId, int score) {
    scores.merge(playerId, score, Integer::sum);
  }

  public int top(int K) {
    return scores.values().stream()
      .sorted(reverseOrder())
      .mapToInt(i -> i)
      .limit(K)
      .sum();
  }

  public void reset(int playerId) {
    scores.put(playerId, 0);
  }
}
