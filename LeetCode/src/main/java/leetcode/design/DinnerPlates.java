package leetcode.design;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class DinnerPlates {

  private final List<Stack<Integer>> stacks = new ArrayList<>();
  private final int capacity;

  private int left = 0;
  private int right = 0;

  public DinnerPlates(int capacity) {
    this.capacity = capacity;
    stacks.add(new Stack<>());
  }

  public void push(int val) {
    stacks.get(left).push(val);
    while (stacks.size() > left && stacks.get(left).size() == capacity) {
      left++;
    }
    if (stacks.size() == left) {
      stacks.add(new Stack<>());
    }
    if (left > right) {
      right = left;
    }
  }

  public int pop() {
    while (right > 0 && stacks.get(right).isEmpty()) {
      right--;
    }
    Stack<Integer> stack = stacks.get(right);
    if (stack.isEmpty()) {
      return -1;
    }
    return stack.pop();
  }

  public int popAtStack(int index) {
    if (index >= stacks.size()) {
      return -1;
    }
    Stack<Integer> stack = stacks.get(index);
    if (stack.isEmpty()) {
      return -1;
    }
    Integer val = stack.pop();
    if (index < this.left) {
      this.left = index;
    }
    return val;
  }
}
