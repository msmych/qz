package leetcode.design;

import java.util.Random;

public class ShuffleAnArray {

    private final Random random = new Random();

    private final int[] nums;
    private int[] shuffled;

    public ShuffleAnArray(int[] nums) {
        this.nums = nums;
        shuffled = nums.clone();
    }

    /** Resets the array to its original configuration and return it. */
    public int[] reset() {
        return nums;
    }

    /** Returns a random shuffling of the array. */
    public int[] shuffle() {
        for (int i = 0; i < shuffled.length; i++) {
            swapAt(i, randomRange(i, shuffled.length));
        }
        return shuffled;
    }

    private int randomRange(int from, int to) {
        return random.nextInt(to - from) + from;
    }

    private void swapAt(int i, int j) {
        int num = shuffled[i];
        shuffled[i] = shuffled[j];
        shuffled[j] = num;
    }
}

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(nums);
 * int[] param_1 = obj.reset();
 * int[] param_2 = obj.shuffle();
 */