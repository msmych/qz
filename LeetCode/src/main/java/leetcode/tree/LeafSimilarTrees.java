package leetcode.tree;

import java.util.LinkedList;
import java.util.Queue;

public interface LeafSimilarTrees {

    boolean leafSimilar(TreeNode root1, TreeNode root2);

    class Solution implements LeafSimilarTrees {

        private final Queue<Integer> leaves = new LinkedList<>();

        @Override
        public boolean leafSimilar(TreeNode root1, TreeNode root2) {
            populateLeaves(root1);
            return isSimilar(root2);
        }

        private void populateLeaves(TreeNode node) {
            if (node == null)
                return;
            populateLeaves(node.left);
            if (node.left == null && node.right == null)
                leaves.offer(node.val);
            populateLeaves(node.right);
        }

        private boolean isSimilar(TreeNode node) {
            if (node == null)
                return true;
            if (node.left == null && node.right == null)
                return !leaves.isEmpty() && leaves.poll() == node.val;
            return isSimilar(node.left) && isSimilar(node.right);
        }
    }
}
