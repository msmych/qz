package leetcode.tree;

public interface SumOfRootToLeafBinaryNumbers {

    int sumRootToLeaf(TreeNode root);

    class Solution implements SumOfRootToLeafBinaryNumbers {

        @Override
        public int sumRootToLeaf(TreeNode root) {
            return nextSum(root, 0);
        }

        private int nextSum(TreeNode node, int sum) {
            if (node == null)
                return 0;
            sum <<= 1;
            sum += node.val;
            if (node.left == null && node.right == null)
                return sum;
            else
                return nextSum(node.left, sum) + nextSum(node.right, sum);
        }
    }
}
