package leetcode.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public interface BinaryTreeInorderTraversal {

  List<Integer> inorderTraversal(TreeNode root);

  class RecursiveSolution implements BinaryTreeInorderTraversal {

    private final List<Integer> vals = new ArrayList<>();

    @Override
    public List<Integer> inorderTraversal(TreeNode root) {
      if (root == null)
        return vals;
      inorderTraversal(root.left);
      vals.add(root.val);
      inorderTraversal(root.right);
      return vals;
    }
  }

  class IterativeSolution implements BinaryTreeInorderTraversal {

    @Override
    public List<Integer> inorderTraversal(TreeNode root) {
      List<Integer> vals = new ArrayList<>();
      if (root == null)
        return vals;
      Stack<Object> nodes = new Stack<>();
      nodes.push(root);
      while (!nodes.isEmpty()) {
        Object item = nodes.peek();
        nodes.pop();
        if (item == null)
          continue;
        if (item instanceof TreeNode) {
          TreeNode node = (TreeNode) item;
          nodes.push(node.right);
          nodes.push(node.val);
          nodes.push(node.left);
        } else
          vals.add((Integer) item);
      }
      return vals;
    }
  }

  class MorrisSolution implements BinaryTreeInorderTraversal {

    @Override
    public List<Integer> inorderTraversal(TreeNode root) {
      List<Integer> vals = new ArrayList<>();
      TreeNode node = root;
      TreeNode pre;
      while (node != null) {
        if (node.left == null) {
          vals.add(node.val);
          node = node.right;
        } else {
          pre = node.left;
          while (pre.right != null) {
            pre = pre.right;
          }
          pre.right = node;
          TreeNode temp = node;
          node = node.left;
          temp.left = null;
        }
      }
      return vals;
    }
  }
}
