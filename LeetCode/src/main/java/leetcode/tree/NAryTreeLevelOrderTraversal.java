package leetcode.tree;

import leetcode.tree.n.Node;

import java.util.*;

public interface NAryTreeLevelOrderTraversal {

    List<List<Integer>> levelOrder(Node root);

    class Solution implements NAryTreeLevelOrderTraversal {

        private final Queue<Node> nodes = new LinkedList<>();
        private final List<List<Integer>> traversal = new ArrayList<>();

        @Override
        public List<List<Integer>> levelOrder(Node root) {
            nodes.offer(root);
            traverse();
            return traversal;
        }

        private void traverse() {
            if (nodes.isEmpty())
                return;
            List<Integer> level = new ArrayList<>();
            for (int i = nodes.size(); i > 0; i--) {
                Node node = nodes.poll();
                if (node == null)
                    continue;
                level.add(node.val);
                for (Node child : node.children)
                    nodes.offer(child);
            }
            if (!level.isEmpty())
                traversal.add(level);
            traverse();
        }
    }
}
