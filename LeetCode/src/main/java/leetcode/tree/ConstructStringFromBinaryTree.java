package leetcode.tree;

public interface ConstructStringFromBinaryTree {

    String tree2str(TreeNode t);

    class Solution implements ConstructStringFromBinaryTree {

        @Override
        public String tree2str(TreeNode t) {
            if (t == null) return "";
            String s = String.valueOf(t.val);
            if (t.left != null || t.right != null) {
                s += t.left == null ? "()" : "(" + tree2str(t.left) + ")";
                if (t.right != null)
                    s += "(" + tree2str(t.right) + ")";
            }
            return s;
        }
    }
}
