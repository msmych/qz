package leetcode.tree;

public interface UnivaluedBinaryTree {

    boolean isUnivalTree(TreeNode root);

    class Solution implements UnivaluedBinaryTree {

        private int value;

        @Override
        public boolean isUnivalTree(TreeNode root) {
            this.value = root.val;
            return isUnivalued(root.left) && isUnivalued(root.right);
        }

        private boolean isUnivalued(TreeNode node) {
            if (node == null)
                return true;
            return node.val == value && isUnivalued(node.left) && isUnivalued(node.right);
        }
    }
}
