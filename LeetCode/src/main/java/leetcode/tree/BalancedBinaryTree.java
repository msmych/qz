package leetcode.tree;

public interface BalancedBinaryTree {

    boolean isBalanced(TreeNode root);

    class Solution implements BalancedBinaryTree {

        @Override
        public boolean isBalanced(TreeNode root) {
            if (root == null)
                return true;
            return Math.abs(depth(root.left, 1) - depth(root.right, 1)) <= 1
                    && isBalanced(root.left) && isBalanced(root.right);
        }

        private int depth(TreeNode node, int d) {
            if (node == null)
                return d;
            return Math.max(depth(node.left, d + 1), depth(node.right, d + 1));
        }
    }
}
