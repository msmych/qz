package leetcode.tree;

public interface ConvertBstToGreaterTree {

    TreeNode convertBST(TreeNode root);

    class Solution implements ConvertBstToGreaterTree {
        @Override
        public TreeNode convertBST(TreeNode root) {
            greater(root, 0);
            return root;
        }

        private int greater(TreeNode node, int greater) {
            if (node == null) {
                return 0;
            }
            int val = node.val;
            int right = greater(node.right, greater);
            node.val += right + greater;
            return val + right + greater(node.left, val + greater + right);
        }
    }
}
