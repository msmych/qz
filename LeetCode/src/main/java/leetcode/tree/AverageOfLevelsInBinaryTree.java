package leetcode.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public interface AverageOfLevelsInBinaryTree {

    List<Double> averageOfLevels(TreeNode root);

    class Solution implements AverageOfLevelsInBinaryTree {

        @Override
        public List<Double> averageOfLevels(TreeNode root) {
            Queue<TreeNode> queue = new LinkedList<>();
            queue.offer(root);
            List<Double> averages = new ArrayList<>();
            while (!queue.isEmpty()) {
                int elements = 0;
                long sum = 0;
                for (int size = queue.size(); size > 0; size--) {
                    TreeNode node = queue.poll();
                    sum += node.val;
                    elements++;
                    if (node.left != null) queue.offer(node.left);
                    if (node.right != null) queue.offer(node.right);
                }
                averages.add((double) sum / elements);
            }
            return averages;
        }
    }
}
