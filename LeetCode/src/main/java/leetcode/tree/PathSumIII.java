package leetcode.tree;

import java.util.ArrayList;
import java.util.List;

public interface PathSumIII {

  int pathSum(TreeNode root, int sum);

  class Solution implements PathSumIII {

    private static class TreeSum {
      final List<Integer> partialSums = new ArrayList<>();
      int lockedPaths = 0;
    }

    private int sum;

    @Override
    public int pathSum(TreeNode root, int sum) {
      this.sum = sum;
      TreeSum treeSum = treeSum(root);
      return (int) (treeSum.lockedPaths +
        treeSum.partialSums.stream()
          .filter(s -> s == sum)
          .count());
    }

    private TreeSum treeSum(TreeNode node) {
      if (node == null) {
        return new TreeSum();
      }
      TreeSum left = treeSum(node.left);
      TreeSum right = treeSum(node.right);
      List<Integer> partialSums = new ArrayList<>();
      partialSums.addAll(left.partialSums);
      partialSums.addAll(right.partialSums);
      TreeSum treeSum = new TreeSum();
      treeSum.partialSums.add(node.val);
      treeSum.partialSums.addAll(partialSums);
      for (Integer partialSum : partialSums) {
        treeSum.partialSums.add(partialSum + node.val);
      }
      treeSum.lockedPaths = left.lockedPaths + right.lockedPaths;
      for (Integer leftSum : left.partialSums) {
        for (Integer rightSum : right.partialSums) {
          if (leftSum + node.val + rightSum == sum) {
            treeSum.lockedPaths++;
          }
        }
      }
      return treeSum;
    }
  }

}
