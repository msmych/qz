package leetcode.tree;

public interface ConstructBinarySearchTreeFromPreorderTraversal {

    TreeNode bstFromPreorder(int[] preorder);

    class Solution implements ConstructBinarySearchTreeFromPreorderTraversal {

        private int[] vals;
        private int index = 0;

        @Override
        public TreeNode bstFromPreorder(int[] preorder) {
            vals = preorder;
            return nextNode(null);
        }

        private TreeNode nextNode(Integer parent) {
            if (index >= vals.length) return null;
            int val = vals[index];
            if (parent != null && val >= parent)
                return null;
            TreeNode node = new TreeNode(val);
            index++;
            node.left = nextNode(val);
            node.right = nextNode(parent);
            return node;
        }
    }
}
