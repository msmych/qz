package leetcode.tree;

public interface LowestCommonAncestorOfABinarySearchTree {

    TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q);

    class Solution implements LowestCommonAncestorOfABinarySearchTree {

        @Override
        public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
            if (root.val == p.val || root.val == q.val
                || p.val < root.val && q.val > root.val || p.val > root.val && q.val < root.val)
                return root;
            return lowestCommonAncestor(p.val < root.val ? root.left : root.right, p, q);
        }
    }
}
