package leetcode.tree;

public class TreeNode {

    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(int val) {
        this.val = val;
    }

    public static TreeNode fromValAndLeft(int val, TreeNode left) {
        TreeNode node = new TreeNode(val);
        node.left = left;
        return node;
    }

    public static TreeNode fromValAndLeftAndRight(int val, TreeNode left, TreeNode right) {
        TreeNode node = new TreeNode(val);
        node.left = left;
        node.right = right;
        return node;
    }

    public static String serialize(TreeNode root) {
        if (root == null)
            return ".";
        return root.val + " " + serialize(root.left) + " " + serialize(root.right);
    }

    public static TreeNode deserialize(String data) {
        if (data.isEmpty())
            return null;
        return deserialize(data.split(" "));
    }

    private static TreeNode deserialize(String[] nodes) {
        int i = 0;
        while (i < nodes.length) {
            if (nodes[i] != null)
                break;
            i++;
        }
        if (nodes[i].equals(".")) {
            nodes[i] = null;
            return null;
        }
        TreeNode node = new TreeNode(Integer.valueOf(nodes[i]));
        nodes[i] = null;
        node.left = deserialize(nodes);
        node.right = deserialize(nodes);
        return node;
    }
}
