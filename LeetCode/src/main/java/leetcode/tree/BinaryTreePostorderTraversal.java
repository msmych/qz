package leetcode.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public interface BinaryTreePostorderTraversal {

    List<Integer> postorderTraversal(TreeNode root);

    class RecursiveSolution implements BinaryTreePostorderTraversal {

        private final List<Integer> vals = new ArrayList<>();

        @Override
        public List<Integer> postorderTraversal(TreeNode root) {
            if (root == null)
                return vals;
            postorderTraversal(root.left);
            postorderTraversal(root.right);
            vals.add(root.val);
            return vals;
        }
    }

    class IterativeSolution implements BinaryTreePostorderTraversal {

        @Override
        public List<Integer> postorderTraversal(TreeNode root) {
            List<Integer> vals = new ArrayList<>();
            if (root == null)
                return vals;
            Stack<Object> nodes = new Stack<>();
            nodes.add(root);
            while (!nodes.isEmpty()) {
                Object item = nodes.peek();
                nodes.pop();
                if (item == null)
                    continue;
                if (item instanceof TreeNode) {
                    TreeNode node = (TreeNode) item;
                    nodes.push(node.val);
                    nodes.push(node.right);
                    nodes.push(node.left);
                } else
                    vals.add((Integer) item);
            }
            return vals;
        }
    }
}
