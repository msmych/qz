package leetcode.tree;

import leetcode.tree.quad.Node;

import java.util.stream.Stream;

public interface QuadTreeInsertion {

    Node intersect(Node quadTree1, Node quadTree2);

    class Solution implements QuadTreeInsertion {

        @Override
        public Node intersect(Node quadTree1, Node quadTree2) {
            if (quadTree1 == null && quadTree2 == null) {
                return null;
            }
            if (quadTree1 == null) {
                return quadTree2;
            }
            if (quadTree2 == null) {
                return quadTree1;
            }
            if (quadTree1.isLeaf && quadTree2.isLeaf) {
                return new Node(quadTree1.val || quadTree2.val, true, null, null, null, null);
            }
            if (quadTree1.isLeaf) {
                return new Node(quadTree1.val || anyTrue(quadTree2), true, null, null, null, null);
            }
            if (quadTree2.isLeaf) {
                return new Node(quadTree2.val || anyTrue(quadTree1), true, null, null, null, null);
            }
            Node topLeft = intersect(quadTree1.topLeft, quadTree2.topLeft);
            Node topRight = intersect(quadTree1.topRight, quadTree2.topRight);
            Node bottomLeft = intersect(quadTree1.bottomLeft, quadTree2.bottomLeft);
            Node bottomRight = intersect(quadTree1.bottomRight, quadTree2.bottomRight);
            if (topLeft.isLeaf && topRight.isLeaf && bottomLeft.isLeaf && bottomRight.isLeaf) {
                if (topLeft.val == topRight.val && topLeft.val == bottomLeft.val && topLeft.val == bottomRight.val) {
                    return new Node(topLeft.val, true, null, null, null, null);
                }
            }
            return new Node(false, false, topLeft, topRight, bottomLeft, bottomRight);
        }

        private boolean anyTrue(Node node) {
            if (node == null) {
                return false;
            }
            return node.val ||
                Stream.of(node.topLeft, node.topRight, node.bottomLeft, node.bottomRight).anyMatch(this::anyTrue);
        }
    }
}
