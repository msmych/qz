package leetcode.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface FindDuplicateSubtrees {

    List<TreeNode> findDuplicateSubtrees(TreeNode root);

    class Solution implements FindDuplicateSubtrees {

        private final Map<String, Integer> subtrees = new HashMap<>();
        private final List<TreeNode> duplicates = new ArrayList<>();

        @Override
        public List<TreeNode> findDuplicateSubtrees(TreeNode root) {
            analise(root);
            return duplicates;
        }

        private String analise(TreeNode node) {
            if (node == null)
                return ".";
            String s = node.val + analise(node.left) + analise(node.right);
            subtrees.put(s, subtrees.getOrDefault(s, 0) + 1);
            if (subtrees.get(s) == 2)
                duplicates.add(node);
            return s;
        }
    }
}
