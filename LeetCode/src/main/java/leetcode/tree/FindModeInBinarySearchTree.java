package leetcode.tree;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public interface FindModeInBinarySearchTree {

    int[] findMode(TreeNode root);

    class Solution implements FindModeInBinarySearchTree {

        private final Map<Integer, Integer> map = new HashMap<>();

        @Override
        public int[] findMode(TreeNode root) {
            nextMode(root);
            int max = map.values().stream().max(Comparator.naturalOrder()).orElse(0);
            map.entrySet().removeIf(entry -> entry.getValue() < max);
            int[] mode = new int[map.size()];
            int i = 0;
            for (int key : map.keySet()) {
                mode[i++] = key;
            }
            return mode;
        }

        private void nextMode(TreeNode node) {
            if (node == null) {
                return;
            }
            map.put(node.val, map.getOrDefault(node.val, 0) + 1);
            nextMode(node.left);
            nextMode(node.right);
        }
    }
}
