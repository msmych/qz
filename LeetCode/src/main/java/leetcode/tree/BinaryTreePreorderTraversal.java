package leetcode.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public interface BinaryTreePreorderTraversal {

    List<Integer> preorderTraversal(TreeNode root);

    class RecursiveSolution implements BinaryTreePreorderTraversal {

        private final List<Integer> vals = new ArrayList<>();

        @Override
        public List<Integer> preorderTraversal(TreeNode root) {
            if (root == null)
                return vals;
            vals.add(root.val);
            preorderTraversal(root.left);
            preorderTraversal(root.right);
            return vals;
        }
    }

    class IterativeSolution implements BinaryTreePreorderTraversal {

        @Override
        public List<Integer> preorderTraversal(TreeNode root) {
            List<Integer> vals = new ArrayList<>();
            if (root == null)
                return vals;
            Stack<Object> nodes = new Stack<>();
            nodes.add(root);
            while (!nodes.isEmpty()) {
                Object item = nodes.peek();
                nodes.pop();
                if (item == null)
                    continue;
                if (item instanceof TreeNode) {
                    TreeNode node = (TreeNode) item;
                    nodes.push(node.right);
                    nodes.push(node.left);
                    nodes.push(node.val);
                } else
                    vals.add((Integer) item);
            }
            return vals;
        }
    }
}
