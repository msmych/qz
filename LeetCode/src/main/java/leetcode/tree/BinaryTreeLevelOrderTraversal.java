package leetcode.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public interface BinaryTreeLevelOrderTraversal {

    List<List<Integer>> levelOrder(TreeNode root);

    class Solution implements BinaryTreeLevelOrderTraversal {

        private final LinkedList<TreeNode> nodes = new LinkedList<>();
        private final List<List<Integer>> vals = new ArrayList<>();

        @Override
        public List<List<Integer>> levelOrder(TreeNode root) {
            nodes.offer(root);
            traverse();
            return vals;
        }

        private void traverse() {
            if (nodes.isEmpty())
                return;
            List<Integer> level = new ArrayList<>();
            for (int i = nodes.size(); i > 0; i--) {
                TreeNode node = nodes.poll();
                if (node == null)
                    continue;
                level.add(node.val);
                nodes.offer(node.left);
                nodes.offer(node.right);
            }
            if (!level.isEmpty())
                vals.add(level);
            traverse();
        }
    }

    class IterativeSolution implements BinaryTreeLevelOrderTraversal {

        private final LinkedList<TreeNode> nodes = new LinkedList<>();
        private final List<List<Integer>> vals = new ArrayList<>();

        @Override
        public List<List<Integer>> levelOrder(TreeNode root) {
            nodes.offer(root);
            while (!nodes.isEmpty()) {
                List<Integer> level = new ArrayList<>();
                for (int i = nodes.size(); i > 0; i--) {
                    TreeNode node = nodes.poll();
                    if (node == null)
                        continue;
                    level.add(node.val);
                    nodes.offer(node.left);
                    nodes.offer(node.right);
                }
                if (!level.isEmpty())
                    vals.add(level);
            }
            return vals;
        }
    }
}
