package leetcode.tree;

import java.util.stream.IntStream;

public interface DiameterOfBinaryTree {

    int diameterOfBinaryTree(TreeNode root);

    class Solution implements DiameterOfBinaryTree {
        @Override
        public int diameterOfBinaryTree(TreeNode root) {
            int[] diameter = nextDiameter(root);
            return Math.max(diameter[0], diameter[1]);
        }

        private int[] nextDiameter(TreeNode node) {
            if (node == null) {
                return new int[]{-1, 0};
            }
            if (node.left == null && node.right == null) {
                return new int[]{0, 0};
            }
            int[] left = nextDiameter(node.left);
            left[0]++;
            int[] right = nextDiameter(node.right);
            right[0]++;
            return new int[]{
                Math.max(left[0], right[0]),
                IntStream.of(left[1], right[1], left[0] + right[0]).max().orElse(0)
            };
        }
    }
}
