package leetcode.tree;

public interface TrimABinarySearchTree {

    TreeNode trimBST(TreeNode root, int L, int R);

    class Solution implements TrimABinarySearchTree {

        private int l, r;

        @Override
        public TreeNode trimBST(TreeNode root, int L, int R) {
            l = L;
            r = R;
            return trimNext(root);
        }

        private TreeNode trimNext(TreeNode node) {
            if (node == null) return null;
            if (node.val < l)
                return trimNext(node.right);
            if (node.val > r)
                return trimNext(node.left);
            node.left = trimNext(node.left);
            node.right = trimNext(node.right);
            return node;
        }
    }
}
