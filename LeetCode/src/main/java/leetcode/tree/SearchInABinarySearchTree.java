package leetcode.tree;

public interface SearchInABinarySearchTree {

    TreeNode searchBST(TreeNode root, int val);

    class Solution implements SearchInABinarySearchTree {

        @Override
        public TreeNode searchBST(TreeNode root, int val) {
            if (root == null)
                return null;
            if (root.val == val)
                return root;
            else if (root.val > val)
                return searchBST(root.left, val);
            else
                return searchBST(root.right, val);
        }
    }
}
