package leetcode.tree;

public interface ValidateBinarySearchTree {

    boolean isValidBST(TreeNode root);

    class Solution implements ValidateBinarySearchTree {

        @Override
        public boolean isValidBST(TreeNode root) {
            if (root == null) return true;
            return verify(root.left, null, root.val) &&
                    verify(root.right, root.val, null);
        }

        private boolean verify(TreeNode node, Integer min, Integer max) {
            if (node == null) return true;
            if (min != null && node.val <= min || max != null && node.val >= max) return false;
            return verify(node.left, min, node.val) &&
                    verify(node.right, node.val, max);
        }
    }
}
