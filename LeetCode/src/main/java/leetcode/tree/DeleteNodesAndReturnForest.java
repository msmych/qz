package leetcode.tree;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface DeleteNodesAndReturnForest {

    List<TreeNode> delNodes(TreeNode root, int[] to_delete);

    class Solution implements DeleteNodesAndReturnForest {

        private final Set<Integer> toDelete = new HashSet<>();

        @Override
        public List<TreeNode> delNodes(TreeNode root, int[] to_delete) {
            if (root == null) return Collections.emptyList();
            toDelete.addAll(Arrays.stream(to_delete).boxed().collect(Collectors.toList()));
            return forest(root);
        }

        private List<TreeNode> forest(TreeNode node) {
            if (node == null) return new ArrayList<>();
            if (toDelete.contains(node.val))
                return Stream.of(forest(node.left), forest(node.right))
                    .flatMap(f -> f.stream())
                    .collect(Collectors.toList());
            if (node.left == null && node.right == null) return Collections.singletonList(node);
            List<TreeNode> list = new ArrayList<>();
            list.addAll(forest(node.left));
            list.addAll(forest(node.right));
            if (node.left != null)
                if (toDelete.contains(node.left.val)) node.left = null;
                else list.remove(node.left);
            if (node.right != null)
                if (toDelete.contains(node.right.val)) node.right = null;
                else list.remove(node.right);
            list.add(node);
            return list;
        }
    }
}
