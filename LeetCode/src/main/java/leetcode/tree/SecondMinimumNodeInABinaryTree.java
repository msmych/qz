package leetcode.tree;

public interface SecondMinimumNodeInABinaryTree {

    int findSecondMinimumValue(TreeNode root);

    class Solution implements SecondMinimumNodeInABinaryTree {

        private int min = -1, min2 = -1;

        @Override
        public int findSecondMinimumValue(TreeNode root) {
            nextMin(root);
            return min2;
        }

        private void nextMin(TreeNode node) {
            if (node == null) return;
            if (min == -1)
                min = node.val;
            else if (node.val < min) {
                min2 = min;
                min = node.val;
            } else if ((min2 == -1 || node.val < min2) && node.val != min)
                min2 = node.val;
            nextMin(node.left);
            nextMin(node.right);
        }
    }
}
