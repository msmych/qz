package leetcode.tree;

public interface BinaryTreeTilt {

    int findTilt(TreeNode root);

    class Solution implements BinaryTreeTilt {
        @Override
        public int findTilt(TreeNode root) {
            if (root == null) {
                return 0;
            }
            int[] left = nextTiltWithSum(root.left);
            int[] right = nextTiltWithSum(root.right);
            return left[0] + right[0] + Math.abs(left[1] - right[1]);
        }

        private int[] nextTiltWithSum(TreeNode node) {
            if (node == null) {
                return new int[]{0, 0};
            }
            int[] left = nextTiltWithSum(node.left);
            int[] right = nextTiltWithSum(node.right);
            return new int[]{left[0] + right[0] + Math.abs(left[1] - right[1]), node.val + left[1] + right[1]};
        }
    }

}
