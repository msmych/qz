package leetcode.tree;

public interface SubtreeOfAnotherTree {

    boolean isSubtree(TreeNode s, TreeNode t);

    class Solution implements SubtreeOfAnotherTree {

        @Override
        public boolean isSubtree(TreeNode s, TreeNode t) {
            if (s == null) {
                return t == null;
            }
            if (t == null) {
                return false;
            }
            if (s.val == t.val && isEqual(s, t)) {
                return true;
            }
            return isSubtree(s.left, t) || isSubtree(s.right, t);
        }

        private boolean isEqual(TreeNode s, TreeNode t) {
            if (s == null) {
                return t == null;
            }
            if (t == null) {
                return false;
            }
            if (s.val != t.val) {
                return false;
            }
            return isEqual(s.left, t.left) && isEqual(s.right, t.right);
        }
    }
}
