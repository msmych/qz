package leetcode.tree;

public interface MinimumCostTreeFromLeafValues {

    int mctFromLeafValues(int[] arr);

    class Solution implements MinimumCostTreeFromLeafValues {

        @Override
        public int mctFromLeafValues(int[] arr) {
            return 0;
        }
    }
}
