package leetcode.tree;

import java.util.LinkedList;
import java.util.Queue;

public interface SymmetricTree {

    boolean isSymmetric(TreeNode root);

    class RecursiveSolution implements SymmetricTree {

        @Override
        public boolean isSymmetric(TreeNode root) {
            if (root == null)
                return true;
            return isSymmetric(root.left, root.right);
        }

        private boolean isSymmetric(TreeNode left, TreeNode right) {
            if (left == null || right == null)
                return left == right;
            if (left.val != right.val)
                return false;
            return isSymmetric(left.left, right.right)
                    && isSymmetric(left.right, right.left);
        }
    }

    class IterativeSolution implements SymmetricTree {

        @Override
        public boolean isSymmetric(TreeNode root) {
            if (root == null) return true;
            Queue<TreeNode> queue = new LinkedList<>();
            queue.offer(root.left);
            queue.offer(root.right);
            while (!queue.isEmpty()) {
                TreeNode ll = queue.poll();
                TreeNode rr = queue.poll();
                TreeNode lr = queue.poll();
                TreeNode rl = queue.poll();
                if (asymmetric(queue, ll, rr)) return false;
                if (asymmetric(queue, lr, rl)) return false;
            }
            return true;
        }

        private boolean asymmetric(Queue<TreeNode> queue, TreeNode left, TreeNode right) {
            if (onlyOnePresent(left, right)) return true;
            else if (bothPresent(left, right)) {
                if (left.val != right.val) return true;
                queue.offer(left.left);
                queue.offer(right.right);
                queue.offer(left.right);
                queue.offer(right.left);
            }
            return false;
        }

        private boolean onlyOnePresent(TreeNode left, TreeNode right) {
            return (left == null) != (right == null);
        }

        private boolean bothPresent(TreeNode left, TreeNode right) {
            return left != null && right != null;
        }
    }
}