package leetcode.tree;

import java.util.Arrays;

public interface ConstructBinaryTreeFromPreorderAndInorderTraversal {

  TreeNode buildTree(int[] preorder, int[] inorder);

  class Solution implements ConstructBinaryTreeFromPreorderAndInorderTraversal {

    @Override
    public TreeNode buildTree(int[] preorder, int[] inorder) {
      if (preorder.length == 0 || inorder.length == 0) {
        return null;
      }
      int rootValue = preorder[0];
      int rootIndex = -1;
      for (int i = 0; i < inorder.length; i++) {
        if (inorder[i] == rootValue) {
          rootIndex = i;
          break;
        }
      }
      int[] nextPreorder = Arrays.copyOfRange(preorder, 1, preorder.length);
      if (rootIndex == -1) {
        return buildTree(nextPreorder, inorder);
      }
      TreeNode node = new TreeNode(rootValue);
      node.left = buildTree(nextPreorder, Arrays.copyOf(inorder, rootIndex));
      node.right = buildTree(nextPreorder, Arrays.copyOfRange(inorder, rootIndex + 1, inorder.length));
      return node;
    }
  }
}
