package leetcode.tree;

import java.util.Arrays;

public interface ConvertSortedArrayToBinarySearchTree {

    TreeNode sortedArrayToBST(int[] nums);

    class Solution implements ConvertSortedArrayToBinarySearchTree {

        @Override
        public TreeNode sortedArrayToBST(int[] nums) {
            if (nums.length == 0)
                return null;
            if (nums.length == 1)
                return new TreeNode(nums[0]);
            int mid = nums.length / 2;
            TreeNode node = new TreeNode(nums[mid]);
            node.left = sortedArrayToBST(Arrays.copyOf(nums, mid));
            node.right = sortedArrayToBST(Arrays.copyOfRange(nums, mid + 1, nums.length));
            return node;
        }
    }
}
