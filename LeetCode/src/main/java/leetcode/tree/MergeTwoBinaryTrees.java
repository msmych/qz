package leetcode.tree;

public interface MergeTwoBinaryTrees {

    TreeNode mergeTrees(TreeNode t1, TreeNode t2);

    class Solution implements MergeTwoBinaryTrees {
        @Override
        public TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
            if (t1 == null && t2 == null) {
                return null;
            }
            int val;
            if (t1 == null) {
                val = t2.val;
            } else if (t2 == null) {
                val = t1.val;
            } else {
                val = t1.val + t2.val;
            }
            TreeNode node = new TreeNode(val);
            if (t1 == null) {
                node.left = mergeTrees(null, t2.left);
                node.right = mergeTrees(null, t2.right);
            } else if (t2 == null) {
                node.left = mergeTrees(t1.left, null);
                node.right = mergeTrees(t1.right, null);
            } else {
                node.left = mergeTrees(t1.left, t2.left);
                node.right = mergeTrees(t1.right, t2.right);
            }
            return node;
        }
    }

}
