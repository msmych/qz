package leetcode.tree;

public interface MaximumDepthOfBinaryTree {

    int maxDepth(TreeNode root);

    class TopDownSolution implements MaximumDepthOfBinaryTree {

        private int max;

        @Override
        public int maxDepth(TreeNode root) {
            if (root == null)
                return 0;
            nodeDepth(root, 1);
            return max;
        }

        private void nodeDepth(TreeNode node, int depth) {
            if (node == null)
                return;
            if (node.left == null && node.right == null)
                max = Math.max(max, depth);
            if (node.left != null)
                nodeDepth(node.left, depth + 1);
            if (node.right != null)
                nodeDepth(node.right, depth + 1);
        }
    }

    class BottomUpSolution implements MaximumDepthOfBinaryTree {

        @Override
        public int maxDepth(TreeNode root) {
            if (root == null)
                return 0;
            int left = maxDepth(root.left);
            int right = maxDepth(root.right);
            return Math.max(left, right) + 1;
        }
    }
}
