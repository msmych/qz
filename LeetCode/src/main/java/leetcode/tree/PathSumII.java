package leetcode.tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public interface PathSumII {

    List<List<Integer>> pathSum(TreeNode root, int sum);

    class Solution implements PathSumII {

        @Override
        public List<List<Integer>> pathSum(TreeNode root, int sum) {
            if (root == null)
                return new ArrayList<>();
            List<List<Integer>> paths = new ArrayList<>();
            if (root.left == null && root.right == null) {
                if (root.val == sum)
                    paths.add(Collections.singletonList(root.val));
                return paths;
            }
            for (List<Integer> leftPath : pathSum(root.left, sum - root.val)) {
                List<Integer> path = new ArrayList<>(leftPath);
                path.add(0, root.val);
                paths.add(path);
            }
            for (List<Integer> rightPath : pathSum(root.right, sum - root.val)) {
                List<Integer> path = new ArrayList<>(rightPath);
                path.add(0, root.val);
                paths.add(path);
            }
            return paths;
        }
    }
}
