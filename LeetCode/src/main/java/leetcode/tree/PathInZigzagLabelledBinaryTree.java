package leetcode.tree;

import java.util.List;

public interface PathInZigzagLabelledBinaryTree {

    List<Integer> pathInZigZagTree(int label);

    class Solution implements PathInZigzagLabelledBinaryTree {
        @Override
        public List<Integer> pathInZigZagTree(int label) {
            return null;
        }
    }
}
