package leetcode.tree;

import java.util.LinkedList;
import java.util.Queue;

public class BSTIterator {

    private final Queue<Integer> vals = new LinkedList<>();

    public BSTIterator(TreeNode root) {
        traverse(root);
    }

    private void traverse(TreeNode node) {
        if (node == null)
            return;
        traverse(node.left);
        vals.offer(node.val);
        traverse(node.right);
    }

    public boolean hasNext() {
        return !vals.isEmpty();
    }

    public int next() {
        return vals.poll();
    }
}
