package leetcode.tree;

import java.util.*;

public interface BinaryTreeLevelOrderTraversalII {

    List<List<Integer>> levelOrderBottom(TreeNode root);

    class Solution implements BinaryTreeLevelOrderTraversalII {

        @Override
        public List<List<Integer>> levelOrderBottom(TreeNode root) {
            if (root == null)
                return new ArrayList<>();
            Stack<List<Integer>> vals = new Stack<>();
            Queue<TreeNode> queue = new LinkedList<>();
            queue.offer(root);
            while (!queue.isEmpty()) {
                List<Integer> row = new ArrayList<>();
                for (int size = queue.size(); size > 0; size--) {
                    TreeNode node = queue.poll();
                    if (node == null)
                        continue;
                    row.add(node.val);
                    queue.offer(node.left);
                    queue.offer(node.right);
                }
                if (!row.isEmpty())
                    vals.push(row);
            }
            List<List<Integer>> list = new ArrayList<>();
            while (!vals.isEmpty()) {
                list.add(vals.pop());
            }
            return list;
        }
    }
}
