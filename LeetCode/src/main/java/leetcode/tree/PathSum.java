package leetcode.tree;

import java.util.HashSet;
import java.util.Set;

public interface PathSum {

    boolean hasPathSum(TreeNode root, int sum);

    class Solution implements PathSum {

        @Override
        public boolean hasPathSum(TreeNode root, int sum) {
            if (root == null)
                return false;
            if (root.left == null && root.right == null)
                return root.val == sum;
            return partialSums(root.left).contains(sum - root.val)
                    || partialSums(root.right).contains(sum - root.val);
        }

        private Set<Integer> partialSums(TreeNode node) {
            Set<Integer> partialSums = new HashSet<>();
            if (node == null)
                return partialSums;
            Set<Integer> leftSet = partialSums(node.left);
            Set<Integer> rightSet = partialSums(node.right);
            if (leftSet.isEmpty() && rightSet.isEmpty())
                partialSums.add(node.val);
            for (int v : leftSet) {
                partialSums.add(v + node.val);
            }
            for (int v : rightSet) {
                partialSums.add(v + node.val);
            }
            return partialSums;
        }
    }
}
