package leetcode.tree;

public interface InvertBinaryTree {

    TreeNode invertTree(TreeNode root);

    class Solution implements InvertBinaryTree {

        @Override
        public TreeNode invertTree(TreeNode root) {
            if (root == null) {
                return null;
            }
            TreeNode left = invertTree(root.right);
            TreeNode right = invertTree(root.left);
            root.left = left;
            root.right = right;
            return root;
        }
    }
}
