package leetcode.tree;

import java.util.Arrays;

public interface ConstructBinaryTreeFromInorderAndPostorderTraversal {

    TreeNode buildTree(int[] inorder, int[] postorder);

    class Solution implements ConstructBinaryTreeFromInorderAndPostorderTraversal {

        @Override
        public TreeNode buildTree(int[] inorder, int[] postorder) {
            if (inorder.length == 0 || postorder.length == 0)
                return null;
            int rootValue = postorder[postorder.length - 1];
            int rootIndex = -1;
            for (int i = 0; i < inorder.length; i++) {
                if (inorder[i] == rootValue) {
                    rootIndex = i;
                    break;
                }
            }
            int[] nextPostorder = Arrays.copyOf(postorder, postorder.length - 1);
            if (rootIndex == -1)
                return buildTree(inorder, nextPostorder);
            TreeNode node = new TreeNode(rootValue);
            node.left = buildTree(Arrays.copyOf(inorder, rootIndex), nextPostorder);
            node.right = buildTree(Arrays.copyOfRange(inorder, rootIndex + 1, inorder.length), nextPostorder);
            return node;
        }
    }
}
