package leetcode.tree;

public interface SumOfLeftLeaves {

    int sumOfLeftLeaves(TreeNode root);

    class Solution implements SumOfLeftLeaves {

        @Override
        public int sumOfLeftLeaves(TreeNode root) {
            return nextSum(root, false);
        }

        private int nextSum(TreeNode node, boolean isLeft) {
            if (node == null) {
                return 0;
            }
            if (node.left == null && node.right == null) {
                return isLeft ? node.val : 0;
            }
            return nextSum(node.left, true) + nextSum(node.right, false);
        }
    }

}
