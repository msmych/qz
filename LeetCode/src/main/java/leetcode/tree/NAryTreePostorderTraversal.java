package leetcode.tree;

import leetcode.tree.n.Node;

import java.util.ArrayList;
import java.util.List;

public interface NAryTreePostorderTraversal {

    List<Integer> postorder(Node root);

    class Solution implements NAryTreePostorderTraversal {

        @Override
        public List<Integer> postorder(Node root) {
            List<Integer> traversal = new ArrayList<>();
            if (root == null)
                return traversal;
            for (Node node : root.children)
                traversal.addAll(postorder(node));
            traversal.add(root.val);
            return traversal;
        }
    }
}
