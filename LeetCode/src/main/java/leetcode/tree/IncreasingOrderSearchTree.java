package leetcode.tree;

import java.util.LinkedList;
import java.util.Queue;

public interface IncreasingOrderSearchTree {

    TreeNode increasingBST(TreeNode root);

    class Solution implements IncreasingOrderSearchTree {

        private final Queue<Integer> nodes = new LinkedList<>();

        @Override
        public TreeNode increasingBST(TreeNode root) {
            traverse(root);
            TreeNode dummy = new TreeNode(0);
            TreeNode node = dummy;
            while (!nodes.isEmpty()) {
                node.right = new TreeNode(nodes.poll());
                node = node.right;
            }
            return dummy.right;
        }

        private void traverse(TreeNode node) {
            if (node == null)
                return;
            traverse(node.left);
            nodes.offer(node.val);
            traverse(node.right);
        }
    }
}
