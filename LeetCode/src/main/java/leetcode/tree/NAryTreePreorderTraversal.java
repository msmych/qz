package leetcode.tree;

import leetcode.tree.n.Node;

import java.util.ArrayList;
import java.util.List;

public interface NAryTreePreorderTraversal {

    List<Integer> preorder(Node root);

    class Solution implements NAryTreePreorderTraversal {

        @Override
        public List<Integer> preorder(Node root) {
            List<Integer> traversal = new ArrayList<>();
            if (root == null)
                return traversal;
            traversal.add(root.val);
            for (Node node : root.children)
                traversal.addAll(preorder(node));
            return traversal;
        }
    }
}
