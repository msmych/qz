package leetcode.tree;

public interface MinimumAbsoluteDifferenceInBst {

    int getMinimumDifference(TreeNode root);

    class Solution implements MinimumAbsoluteDifferenceInBst {

        private int minDiff = Integer.MAX_VALUE;

        @Override
        public int getMinimumDifference(TreeNode root) {
            findMinDiff(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
            return minDiff;
        }

        private void findMinDiff(TreeNode node, int min, int max) {
            if (node == null) {
                return;
            }
            int leftDiff = node.val - min;
            if (leftDiff >= 0 && leftDiff < minDiff) {
                minDiff = leftDiff;
            }
            int rightDiff = max - node.val;
            if (rightDiff >= 0 && rightDiff < minDiff) {
                minDiff = rightDiff;
            }
            findMinDiff(node.left, min, node.val);
            findMinDiff(node.right, node.val, max);
        }
    }
}
