package leetcode.tree;

import leetcode.tree.n.Node;

import java.util.Comparator;

public interface MaximumDepthOfNAryTree {

    int maxDepth(Node root);

    class TopDownSolution implements MaximumDepthOfNAryTree {

        private int depth = 0;

        @Override
        public int maxDepth(Node root) {
            depth(root, 0);
            return depth;
        }

        private void depth(Node node, int d) {
            if (node == null)
                return;
            if (d == depth)
                depth++;
            for (Node child : node.children)
                depth(child, d + 1);
        }
    }

    class BottomUpSolution implements MaximumDepthOfNAryTree {

        @Override
        public int maxDepth(Node root) {
            if (root == null)
                return 0;
            return root.children.stream()
                    .map(this::maxDepth)
                    .max(Comparator.naturalOrder())
                    .map(max -> max + 1)
                    .orElse(1);
        }
    }
}
