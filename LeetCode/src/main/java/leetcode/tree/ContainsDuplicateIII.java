package leetcode.tree;

public interface ContainsDuplicateIII {

    boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t);

    class Solution implements ContainsDuplicateIII {

        class Node {

            int val;
            int index;
            Node left;
            Node right;

            Node(int val, int index) {
                this.val = val;
                this.index = index;
            }
        }

        private int k;
        private int t;
        private Node root;

        @Override
        public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
            this.k = k;
            this.t = t;
            for (int i = 0; i < nums.length; i++) {
                if (containsDuplicate(root, nums[i], i))
                    return true;
                root = addNode(root, nums[i], i);
            }
            return false;
        }

        private Node addNode(Node node, int val, int index) {
            if (node == null)
                return new Node(val, index);
            else if (val < node.val)
                node.left = addNode(node.left, val, index);
            else if (val > node.val)
                node.right = addNode(node.right, val, index);
            return node;
        }

        private boolean containsDuplicate(Node node, int val, int index) {
            if (node == null)
                return false;
            long valDistance = Math.abs((long) val - node.val);
            if (valDistance <= t) {
                if (Math.abs(index - node.index) <= k)
                    return true;
                return containsDuplicate(node.left, val, index)
                        || containsDuplicate(node.right, val, index);
            }
            return val < node.val
                    ? containsDuplicate(node.left, val, index)
                    : containsDuplicate(node.right, val, index);
        }
    }
}
