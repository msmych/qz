package leetcode.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public interface PopulatingNextRightPointersInEachNode {

  void connect(TreeLinkNode root);

  class Solution implements PopulatingNextRightPointersInEachNode {

    private final LinkedList<TreeLinkNode> nodes = new LinkedList<>();
    private final List<LinkedList<TreeLinkNode>> levels = new ArrayList<>();

    @Override
    public void connect(TreeLinkNode root) {
      nodes.offer(root);
      traverse();
      next(root, 0);
    }

    private void traverse() {
      if (nodes.isEmpty()) {
        return;
      }
      LinkedList<TreeLinkNode> level = new LinkedList<>();
      for (int i = nodes.size(); i > 0; i--) {
        TreeLinkNode node = nodes.poll();
        if (node == null) {
          continue;
        }
        level.offer(node);
        nodes.offer(node.left);
        nodes.offer(node.right);
      }
      levels.add(level);
      traverse();
    }

    private void next(TreeLinkNode root, int levelIndex) {
      if (root == null) {
        return;
      }
      LinkedList<TreeLinkNode> level = levels.get(levelIndex);
      TreeLinkNode node = level.poll();
      if (node == root) {
        node = level.poll();
      }
      root.next = node;
      next(root.left, levelIndex + 1);
      next(root.right, levelIndex + 1);
    }
  }

  class SolutionII implements PopulatingNextRightPointersInEachNode {

    private final LinkedList<TreeLinkNode> nodes = new LinkedList<>();
    private final List<LinkedList<TreeLinkNode>> levels = new ArrayList<>();

    @Override
    public void connect(TreeLinkNode root) {
      nodes.offer(root);
      traverse();
      next(root, 0);
    }

    private void traverse() {
      if (nodes.isEmpty()) {
        return;
      }
      LinkedList<TreeLinkNode> level = new LinkedList<>();
      for (int i = nodes.size(); i > 0; i--) {
        TreeLinkNode node = nodes.poll();
        if (node == null) {
          continue;
        }
        level.offer(node);
        nodes.offer(node.left);
        nodes.offer(node.right);
      }
      levels.add(level);
      traverse();
    }

    private void next(TreeLinkNode root, int levelIndex) {
      if (root == null) {
        return;
      }
      LinkedList<TreeLinkNode> level = levels.get(levelIndex);
      TreeLinkNode node = level.poll();
      if (node == root) {
        node = level.poll();
      }
      root.next = node;
      next(root.left, levelIndex + 1);
      next(root.right, levelIndex + 1);
    }
  }
}
