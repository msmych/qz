package leetcode.tree;

public interface DeleteNodeInABst {

    TreeNode deleteNode(TreeNode root, int key);

    class Solution implements DeleteNodeInABst {

        @Override
        public TreeNode deleteNode(TreeNode root, int key) {
            if (root == null)
                return null;
            if (root.val > key)
                root.left = deleteNode(root.left, key);
            else if (root.val < key)
                root.right = deleteNode(root.right, key);
            else {
                if (root.left == null || root.right == null) {
                    TreeNode node = root.left == null ? root.right : root.left;
                    if (node == null)
                        return null;
                    else
                        return node;
                } else {
                    TreeNode right = findRightSuccessor(root);
                    root.val = right.val;
                    root.right = deleteNode(root.right, right.val);
                }
            }
            return root;
        }

        private TreeNode findRightSuccessor(TreeNode root) {
            if (root == null)
                return null;
            TreeNode node = root.right;
            if (node != null) {
                while (node.left != null)
                    node = node.left;
            }
            return node;
        }
    }
}
