package leetcode.tree;

import java.util.LinkedList;
import java.util.Queue;

public interface CousinsInBinaryTree {

    boolean isCousins(TreeNode root, int x, int y);

    class Solution implements CousinsInBinaryTree {

        private class TreeNodeWithParent {
            TreeNode node, parent;

            TreeNodeWithParent(TreeNode node, TreeNode parent) {
                this.node = node;
                this.parent = parent;
            }
        }

        @Override
        public boolean isCousins(TreeNode root, int x, int y) {
            Queue<TreeNodeWithParent> nodes = new LinkedList<>();
            nodes.offer(new TreeNodeWithParent(root, null));
            while (!nodes.isEmpty()) {
                TreeNodeWithParent current = null;
                int n = nodes.size();
                for (; n > 0; n--) {
                    TreeNodeWithParent node = nodes.poll();
                    if (node.node == null)
                        continue;
                    if (node.node.val == x) {
                        if (current != null && current.node.val == y && current.parent != node.parent)
                            return true;
                        else
                            current = node;
                    } else if (node.node.val == y) {
                        if (current != null && current.node.val == x && current.parent != node.parent)
                            return true;
                        else
                            current = node;
                    }
                    nodes.offer(new TreeNodeWithParent(node.node.left, node.node));
                    nodes.offer(new TreeNodeWithParent(node.node.right, node.node));
                }
            }
            return false;
        }
    }
}
