package leetcode.stack;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public interface ValidParentheses {

    boolean isValid(String s);

    class Solution implements ValidParentheses {

        private final Stack<Character> brackets = new Stack<>();
        private final List<Character> opening = Arrays.asList('(', '[', '{'),
                closing = Arrays.asList(')', ']', '}');

        @Override
        public boolean isValid(String s) {
            for (char c : s.toCharArray()) {
                if (opening.contains(c))
                    brackets.push(c);
                else {
                    if (brackets.isEmpty())
                        return false;
                    char b = brackets.peek();
                    if (opening.indexOf(b) != closing.indexOf(c))
                        return false;
                    brackets.pop();
                }
            }
            return brackets.isEmpty();
        }
    }
}
