package leetcode.stack;

import java.util.Stack;

public interface MaximalRectangle {

  int maximalRectangle(char[][] matrix);

  class Solution implements MaximalRectangle {

    @Override
    public int maximalRectangle(char[][] matrix) {
      if (matrix.length == 0) {
        return 0;
      }
      int[] histogram = new int[matrix[0].length];
      for (int i = 0; i < matrix[0].length; i++) {
        histogram[i] = matrix[0][i] - '0';
      }
      int max = maxArea(histogram);
      for (int i = 1; i < matrix.length; i++) {
        for (int j = 0; j < matrix[i].length; j++) {
          histogram[j] = matrix[i][j] == '0' ? 0 : histogram[j] + 1;
        }
        int area = maxArea(histogram);
        if (area > max) {
          max = area;
        }
      }
      return max;
    }

    private int maxArea(int[] histogram) {
      Stack<Integer> stack = new Stack<>();
      int max = 0;
      for (int i = 0; i <= histogram.length; i++) {
        int last = i == histogram.length ? -1 : histogram[i];
        while (!stack.isEmpty() && histogram[stack.peek()] > last) {
          int height = histogram[stack.pop()];
          int width = stack.isEmpty() ? i : i - stack.peek() - 1;
          int area = height * width;
          if (area > max) max = area;
        }
        stack.push(i);
      }
      return max;
    }
  }
}
