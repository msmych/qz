package leetcode.stack;

import java.util.Stack;

public interface BasicCalculator {

  int calculate(String s);

  class Solution implements BasicCalculator {
    @Override
    public int calculate(String s) {
      return calcNext(s.replaceAll(" ", ""));
    }

    private int calcNext(String s) {
      int val = 0;
      int i = s.length() - 1;
      int rightIndex = i;
      while (i >= 0) {
        char c = s.charAt(i);
        if (c == '+') {
          val += Integer.parseInt(s.substring(i + 1, rightIndex + 1));
          rightIndex = --i;
        } else if (c == '-') {
          val -= Integer.parseInt(s.substring(i + 1, rightIndex + 1));
          rightIndex = --i;
        } else if (c == ')') {
          Stack<Integer> stack = new Stack<>();
          stack.push(i--);
          rightIndex = i;
          while (!stack.isEmpty()) {
            if (s.charAt(i) == '(') {
              rightIndex = stack.pop();
            } else if (s.charAt(i) == ')') {
              stack.push(i);
            }
            i--;
          }
          int nextVal = calcNext(s.substring(i + 2, rightIndex));
          if (i == -1 || s.charAt(i) == '+') {
            val += nextVal;
          } else {
            val -= nextVal;
          }
          rightIndex = --i;
        } else if (i == 0) {
          val += Integer.parseInt(s.substring(0, rightIndex + 1));
          i--;
        } else {
          i--;
        }
      }
      return val;
    }
  }
}
