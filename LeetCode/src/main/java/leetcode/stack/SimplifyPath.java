package leetcode.stack;

import java.util.Stack;

public interface SimplifyPath {

    String simplifyPath(String path);

    class Solution implements SimplifyPath {

        @Override
        public String simplifyPath(String path) {
            Stack<String> dirs = new Stack<>();
            for (String dir : path.split("/")) {
                if (dir.equals("..")) {
                    if (!dirs.isEmpty())
                        dirs.pop();
                } else if (!dir.isEmpty() && !dir.equals("."))
                    dirs.push(dir);
            }
            return '/' + String.join("/", dirs);
        }
    }
}
