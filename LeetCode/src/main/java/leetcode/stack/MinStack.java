package leetcode.stack;

import java.util.*;

public class MinStack {

    private final List<Integer> vals = new ArrayList<>();
    private final Queue<Integer> queue = new PriorityQueue<>(Comparator.naturalOrder());

    /** initialize your data structure here. */
    public MinStack() {}

    public void push(int x) {
        vals.add(x);
        queue.add(x);
    }

    public void pop() {
        if (vals.isEmpty())
            return;
        int v = vals.get(vals.size() - 1);
        vals.remove(vals.size() - 1);
        queue.remove(v);
    }

    public int top() {
        return vals.get(vals.size() - 1);
    }

    public int getMin() {
        return queue.peek();
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */