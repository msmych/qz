package leetcode.stack;

import java.util.Stack;

public interface RemoveOutermostParentheses {

    String removeOuterParentheses(String S);

    class Solution implements RemoveOutermostParentheses {

        @Override
        public String removeOuterParentheses(String S) {
            StringBuilder sb = new StringBuilder();
            Stack<Character> stack = new Stack<>();
            for (int i = 0; i < S.length(); i++) {
                int left = i;
                stack.push(S.charAt(i));
                while (!stack.empty()) {
                    i++;
                    char right = S.charAt(i);
                    if (right == '(')
                        stack.push(right);
                    else
                        stack.pop();
                }
                sb.append(S, left + 1, i);
            }
            return sb.toString();
        }
    }
}
