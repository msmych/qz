package leetcode.stack;

import java.util.Stack;

import static java.lang.Math.abs;

public interface MinimumRemoveToMakeValidParentheses {

  String minRemoveToMakeValid(String s);

  class Solution implements MinimumRemoveToMakeValidParentheses {

    @Override
    public String minRemoveToMakeValid(String s) {
      StringBuilder sb = new StringBuilder(s);
      Stack<Integer> stack = new Stack<>();
      for (int i = 0; i < sb.length(); i++) {
        char c = s.charAt(i);
        if (c == '(') {
          stack.push(i + 1);
        } else if (c == ')') {
          if (stack.isEmpty() || stack.peek() < 0) {
            stack.push(-1 - i);
          } else {
            stack.pop();
          }
        }
      }
      StringBuilder valid = new StringBuilder();
      for (int i = 0, j = 0; i < sb.length(); i++) {
        if (j >= stack.size() || i != abs(stack.elementAt(j)) - 1) {
          valid.append(sb.charAt(i));
        } else {
          j++;
        }
      }
      return valid.toString();
    }
  }
}

