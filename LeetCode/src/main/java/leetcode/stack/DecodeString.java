package leetcode.stack;

import java.util.Arrays;
import java.util.List;

public interface DecodeString {

    String decodeString(String s);

    class Solution implements DecodeString {

        private final List<Character> numbers = Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

        @Override
        public String decodeString(String s) {
            if (s.isEmpty())
                return "";
            char first = s.charAt(0);
            if (!numbers.contains(first))
                return first + decodeString(s.substring(1));
            StringBuilder sb = new StringBuilder();
            int last = s.indexOf("[");
            int brackets = 1;
            do {
                last++;
                if (s.charAt(last) == '[')
                    brackets++;
                else if (s.charAt(last) == ']')
                    brackets--;
            } while (brackets > 0);
            int countIndex = s.indexOf("[");
            for (int i = 0; i < Integer.valueOf(s.substring(0, countIndex)); i++)
                sb.append(decodeString(s.substring(countIndex + 1, last)));
            String next = s.substring(last + 1);
            while (!next.isEmpty() && next.charAt(0) == ']')
                next = next.substring(1);
            return sb.toString() + decodeString(next);
        }
    }
}
