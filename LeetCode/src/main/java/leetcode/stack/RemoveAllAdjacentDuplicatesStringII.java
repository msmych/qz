package leetcode.stack;

import java.util.Stack;
import java.util.stream.Collectors;

public interface RemoveAllAdjacentDuplicatesStringII {

    String removeDuplicates(String s, int k);

    class Solution implements RemoveAllAdjacentDuplicatesStringII {

        private static class Letter {
            final char c;
            int occurrences;

            public Letter(char c, int occurrences) {
                this.c = c;
                this.occurrences = occurrences;
            }
        }

        @Override
        public String removeDuplicates(String s, int k) {
            Stack<Letter> stack = new Stack<>();
            for (char c : s.toCharArray()) {
                if (stack.isEmpty() || stack.peek().c != c) {
                    stack.push(new Letter(c, 1));
                } else {
                    stack.peek().occurrences++;
                }
                while (!stack.isEmpty() && stack.peek().occurrences >= k) {
                    stack.pop();
                }
            }
            return stack.stream()
                .map(this::build)
                .collect(Collectors.joining());
        }

        private String build(Letter letter) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < letter.occurrences; i++) {
                sb.append(letter.c);
            }
            return sb.toString();
        }
    }
}
