package leetcode.stack;

import java.util.Stack;

public interface BackspaceStringCompare {

    boolean backspaceCompare(String S, String T);

    class Solution implements BackspaceStringCompare {

        @Override
        public boolean backspaceCompare(String S, String T) {
            Stack<Character> s = getStack(S),
                    t = getStack(T);
            if (s.size() != t.size())
                return false;
            while (!s.isEmpty()) {
                if (s.peek() != t.peek())
                    return false;
                s.pop();
                t.pop();
            }
            return true;
        }

        private Stack<Character> getStack(String s) {
            Stack<Character> stack = new Stack<>();
            for (char c : s.toCharArray()) {
                if (c == '#') {
                    if (!stack.isEmpty())
                        stack.pop();
                } else
                    stack.push(c);
            }
            return stack;
        }
    }
}
