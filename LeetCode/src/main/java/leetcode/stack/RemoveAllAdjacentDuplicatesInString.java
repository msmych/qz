package leetcode.stack;

import java.util.Stack;
import java.util.stream.Collectors;

public interface RemoveAllAdjacentDuplicatesInString {

    String removeDuplicates(String S);

    class Solution implements RemoveAllAdjacentDuplicatesInString {

        @Override
        public String removeDuplicates(String S) {
            Stack<Character> stack = new Stack<>();
            for (char c : S.toCharArray()) {
                if (!stack.isEmpty() && stack.peek() == c)
                    stack.pop();
                else
                    stack.push(c);
            }
            return stack.stream().map(Object::toString).collect(Collectors.joining());
        }
    }
}
