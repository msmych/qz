package leetcode.stack;

import java.util.LinkedList;
import java.util.Queue;

public class MyStack {

    private final Queue<Integer> queue = new LinkedList<>();

    /** Initialize your data structure here. */
    public MyStack() {}

    /** Push element x onto stack. */
    public void push(int x) {
        queue.offer(x);
    }

    /** Removes the element on top of the stack and returns that element. */
    public int pop() {
        Queue<Integer> backup = new LinkedList<>();
        while (true) {
            int val = queue.poll();
            backup.offer(val);
            if (queue.isEmpty()) {
                while (true) {
                    int v = backup.poll();
                    if (backup.isEmpty())
                        return val;
                    queue.offer(v);
                }
            }
        }
    }

    /** Get the top element. */
    public int top() {
        Queue<Integer> backup = new LinkedList<>();
        int val = 0;
        while (!queue.isEmpty()) {
            val = queue.poll();
            backup.offer(val);
        }
        while (!backup.isEmpty())
            queue.offer(backup.poll());
        return val;
    }

    /** Returns whether the stack is empty. */
    public boolean empty() {
        return queue.isEmpty();
    }
}

/**
 * Your MyStack object will be instantiated and called as such:
 * MyStack obj = new MyStack();
 * obj.push(x);
 * int param_2 = obj.pop();
 * int param_3 = obj.top();
 * boolean param_4 = obj.empty();
 */
