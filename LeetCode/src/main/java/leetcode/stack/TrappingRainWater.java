package leetcode.stack;

import java.util.Stack;

public interface TrappingRainWater {

    int trap(int[] height);

    class Solution implements TrappingRainWater {

        @Override
        public int trap(int[] height) {
            if (height.length == 0)
                return 0;
            int water = 0;
            Stack<int[]> levels = new Stack<>();
            levels.push(new int[]{0, height[0]});
            int lastLevel = height[0];
            for (int i = 1; i < height.length; i++) {
                if (height[i] > lastLevel && !levels.isEmpty()) {
                    int[] level;
                    while (!levels.isEmpty()) {
                        level = levels.peek();
                        water += (i - level[0] - 1) * (Math.min(height[i], level[1]) - lastLevel);
                        if (height[i] > level[1]) {
                            levels.pop();
                            lastLevel = level[1];
                        } else {
                            levels.push(new int[]{i, height[i]});
                            break;
                        }
                    }
                    if (levels.isEmpty())
                        levels.push(new int[]{i, height[i]});
                } else {
                    levels.push(new int[]{i, height[i]});
                    lastLevel = height[i];
                }
            }
            return water;
        }
    }
}
