package leetcode.stack;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public interface EvaluateReversePolishNotation {

    int evalRPN(String[] tokens);

    class Solution implements EvaluateReversePolishNotation {

        private final List<String> operations = Arrays.asList("+", "-", "*", "/");
        private final Stack<Integer> operands = new Stack<>();

        @Override
        public int evalRPN(String[] tokens) {
            for (String token : tokens) {
                if (operations.contains(token))
                    operate(token);
                else
                    operands.push(Integer.valueOf(token));
            }
            return operands.peek();
        }

        private void operate(String operation) {
            int b = operands.peek();
            operands.pop();
            int a = operands.peek();
            operands.pop();
            switch (operation) {
                case "+":
                    operands.push(a + b);
                    break;
                case "-":
                    operands.push(a - b);
                    break;
                case "*":
                    operands.push(a * b);
                    break;
                case "/":
                    operands.push(a / b);
            }
        }
    }
}
