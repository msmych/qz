package leetcode.stack;

import java.util.Stack;

public interface LargestRectangleInHistogram {

  int largestRectangleArea(int[] heights);

  class Solution implements LargestRectangleInHistogram {

    @Override
    public int largestRectangleArea(int[] heights) {
      Stack<Integer> stack = new Stack<>();
      int max = 0;
      for (int i = 0; i <= heights.length; i++) {
        int last = i == heights.length ? -1 : heights[i];
        while (!stack.isEmpty() && heights[stack.peek()] > last) {
          int height = heights[stack.pop()];
          int width = stack.isEmpty() ? i : i - stack.peek() - 1;
          int area = height * width;
          if (area > max) max = area;
        }
        stack.push(i);
      }
      return max;
    }
  }
}
