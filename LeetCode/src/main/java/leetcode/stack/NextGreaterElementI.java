package leetcode.stack;

import java.util.*;

public interface NextGreaterElementI {

    int[] nextGreaterElement(int[] nums1, int[] nums2);

    class Solution implements NextGreaterElementI {

        @Override
        public int[] nextGreaterElement(int[] nums1, int[] nums2) {
            int[] nge = new int[nums1.length];
            Stack<Integer> stack = stack(nums2);
            for (int i = 0; i < nums1.length; i++) {
                Stack<Integer> s = new Stack<>();
                s.addAll(stack);
                while (!s.isEmpty() && s.peek() != nums1[i]) {
                    s.pop();
                }
                while (!s.isEmpty()) {
                    if (s.peek() > nums1[i]) {
                        nge[i] = s.peek();
                        break;
                    }
                    s.pop();
                }
                if (nge[i] == 0) {
                    nge[i] = -1;
                }
            }
            return nge;
        }

        private Stack<Integer> stack(int[] nums) {
            Stack<Integer> stack = new Stack<>();
            for (int i = nums.length - 1; i >= 0; i--) {
                stack.push(nums[i]);
            }
            return stack;
        }
    }
}
