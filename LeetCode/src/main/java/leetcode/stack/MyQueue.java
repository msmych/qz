package leetcode.stack;

import java.util.Stack;

public class MyQueue {

    private final Stack<Integer> stack = new Stack<>();
    private final Stack<Integer> backup = new Stack<>();

    /** Initialize your data structure here. */
    public MyQueue() {}

    /** Push element x to the back of queue. */
    public void push(int x) {
        while (!stack.isEmpty()) {
            int val = stack.peek();
            stack.pop();
            backup.push(val);
        }
        backup.push(x);
        while (!backup.isEmpty()) {
            int val = backup.peek();
            backup.pop();
            stack.push(val);
        }
    }

    /** Removes the element from in front of queue and returns that element. */
    public int pop() {
        int val = stack.peek();
        stack.pop();
        return val;
    }

    /** Get the front element. */
    public int peek() {
        return stack.peek();
    }

    /** Returns whether the queue is empty. */
    public boolean empty() {
        return stack.isEmpty();
    }
}

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue obj = new MyQueue();
 * obj.push(x);
 * int param_2 = obj.pop();
 * int param_3 = obj.peek();
 * boolean param_4 = obj.empty();
 */