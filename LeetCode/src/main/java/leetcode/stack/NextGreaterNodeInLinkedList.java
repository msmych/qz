package leetcode.stack;

import leetcode.linked_list.ListNode;

import java.util.ArrayList;
import java.util.List;

public interface NextGreaterNodeInLinkedList {

    int[] nextLargerNodes(ListNode head);

    class Solution implements NextGreaterNodeInLinkedList {

        @Override
        public int[] nextLargerNodes(ListNode head) {
            List<Integer> list = new ArrayList<>();
            while (head != null) {
                list.add(head.val);
                head = head.next;
            }
            int[] larger = new int[list.size()];
            for (int i = 0; i < larger.length - 1; i++) {
                for (int j = i + 1; j < larger.length; j++) {
                    if (list.get(j) > list.get(i)) {
                        larger[i] = list.get(j);
                        break;
                    }
                }
            }
            return larger;
        }
    }
}
