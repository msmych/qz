package leetcode.stack;

public interface DailyTemperatures {

    int[] dailyTemperatures(int[] T);

    class Solution implements DailyTemperatures {

        @Override
        public int[] dailyTemperatures(int[] T) {
            int[] warmer = new int[T.length];
            for (int i = 0; i < T.length; i++) {
                for (int j = i + 1; j < T.length; j++) {
                    if (T[j] > T[i]) {
                        warmer[i] = j - i;
                        break;
                    }
                }
            }
            return warmer;
        }
    }
}
