package leetcode.stack;

import java.util.Stack;

public interface ReverseSubstringsBetweenEachPairOfParentheses {

  String reverseParentheses(String s);

  class Solution implements ReverseSubstringsBetweenEachPairOfParentheses {
    @Override
    public String reverseParentheses(String s) {
      Stack<Integer> stack = new Stack<>();
      StringBuilder sb = new StringBuilder(s);
      int i = 0;
      while (i < sb.length()) {
        char c = sb.charAt(i);
        if (c == '(') {
          stack.push(i);
          i++;
        } else if (c == ')') {
          int open = stack.pop();
          sb.replace(open, i + 1, new StringBuilder(sb.substring(open + 1, i)).reverse().toString());
          i--;
        } else {
          i++;
        }
      }
      return sb.toString();
    }
  }
}
