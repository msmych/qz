package leetcode.greedy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;

public interface ReconstructA2RowBinaryMatrix {

  List<List<Integer>> reconstructMatrix(int upper, int lower, int[] colsum);

  class Solution implements ReconstructA2RowBinaryMatrix {
    @Override
    public List<List<Integer>> reconstructMatrix(int upper, int lower, int[] colsum) {
      if (stream(colsum).sum() != upper + lower) {
        return emptyList();
      }
      long twos = stream(colsum).filter(n -> n == 2).count();
      if (twos > upper || twos > lower) {
        return emptyList();
      }
      upper -= twos;
      List<Integer> upperRow = new ArrayList<>();
      for (int n : colsum) {
        switch (n) {
          case 0:
            upperRow.add(0);
            break;
          case 1:
            if (upper > 0) {
              upper--;
            } else {
              upperRow.add(0);
              break;
            }
          case 2:
            upperRow.add(1);
        }
      }
      List<Integer> lowerRow = new ArrayList<>();
      for (int i = 0; i < colsum.length; i++) {
        switch (colsum[i]) {
          case 0:
            lowerRow.add(0);
            break;
          case 1:
            lowerRow.add(upperRow.get(i) == 1 ? 0 : 1);
            break;
          case 2:
            lowerRow.add(1);
        }
      }
      return Arrays.asList(upperRow, lowerRow);
    }
  }
}
