package leetcode.greedy;

public interface JumpGame {

  boolean canJump(int[] nums);

  class Solution implements JumpGame {

    @Override
    public boolean canJump(int[] nums) {
      int good = nums.length - 1;
      for (int i = nums.length - 1; i >= 0; i--) {
        if (i + nums[i] >= good) {
          good = i;
        }
      }
      return good == 0;
    }
  }
}
