package leetcode.greedy;

import java.util.HashMap;
import java.util.Map;

public interface LemonadeChange {

    boolean lemonadeChange(int[] bills);

    class Solution implements LemonadeChange {

        private Map<Integer, Integer> changes = new HashMap<>();

        @Override
        public boolean lemonadeChange(int[] bills) {
            for (int bill : bills) {
                if (canProvideChange(changes, bill - 5))
                    changes.put(bill, changes.getOrDefault(bill, 0) + 1);
                else
                    return false;
            }
            return true;
        }

        private boolean canProvideChange(Map<Integer, Integer> changes, int bill) {
            if (bill == 0) {
                this.changes = changes;
                return true;
            }
            if (bill < 0)
                return false;
            for (Map.Entry<Integer, Integer> change : changes.entrySet()) {
                if (change.getValue() > 0) {
                    Map<Integer, Integer> next = new HashMap<>(changes);
                    next.put(change.getKey(), change.getValue() - 1);
                    if (canProvideChange(next, bill - change.getKey()))
                        return true;
                }
            }
            return false;
        }
    }
}
