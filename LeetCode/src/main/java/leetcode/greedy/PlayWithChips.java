package leetcode.greedy;

public interface PlayWithChips {

  int minCostToMoveChips(int[] chips);

  class Solution implements PlayWithChips {
    @Override
    public int minCostToMoveChips(int[] chips) {
      int evenSum = 0;
      int oddSum = 0;
      for (int i = 0; i < chips.length; i++) {
        if (chips[i] % 2 == 0) {
          evenSum++;
        } else {
          oddSum++;
        }
      }
      return Math.min(evenSum, oddSum);
    }
  }
}
