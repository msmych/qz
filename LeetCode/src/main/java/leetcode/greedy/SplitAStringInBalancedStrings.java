package leetcode.greedy;

import java.util.HashMap;
import java.util.Map;

public interface SplitAStringInBalancedStrings {

  int balancedStringSplit(String s);

  class Solution implements SplitAStringInBalancedStrings {

    private final Map<String, Integer> cache = new HashMap<>();

    @Override
    public int balancedStringSplit(String s) {
      if (s.isEmpty()) {
        return 0;
      }
      if (cache.containsKey(s)) {
        return cache.get(s);
      }
      if (!isBalanced(s)) {
        cache.put(s, 0);
        return 0;
      }
      int max = 1;
      for (int i = 2; i <= s.length() - 2; i += 2) {
        int split = balancedStringSplit(s.substring(0, i)) + balancedStringSplit(s.substring(i));
        if (split > max) {
          max = split;
        }
      }
      cache.put(s, max);
      return max;
    }

    private boolean isBalanced(String s) {
      int b = 0;
      for (char c : s.toCharArray()) {
        if (c == 'L') {
          b--;
        } else {
          b++;
        }
      }
      return b == 0;
    }
  }
}
