package leetcode.greedy;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;

public interface NumberOfBurgersWithNoWasteOfIngredients {

  List<Integer> numOfBurgers(int tomatoSlices, int cheeseSlices);

  class Solution implements NumberOfBurgersWithNoWasteOfIngredients {
    @Override
    public List<Integer> numOfBurgers(int tomatoSlices, int cheeseSlices) {
      if (tomatoSlices % 2 == 1 || 4 * cheeseSlices < tomatoSlices || 2 * cheeseSlices > tomatoSlices) {
        return emptyList();
      }
      if (4 * cheeseSlices == tomatoSlices) {
        return Arrays.asList(cheeseSlices, 0);
      } else if (2 * cheeseSlices == tomatoSlices) {
        return Arrays.asList(0, cheeseSlices);
      } else if (3 * cheeseSlices > tomatoSlices) {
        int small = (4 * cheeseSlices - tomatoSlices) / 2;
        return Arrays.asList(cheeseSlices - small, small);
      } else {
        int jumbo = (tomatoSlices - 2 * cheeseSlices) / 2;
        return Arrays.asList(jumbo, cheeseSlices - jumbo);
      }
    }
  }
}
