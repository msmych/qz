package leetcode.greedy;

import java.util.HashMap;
import java.util.Map;

public interface JumpGameII {

    int jump(int[] nums);

    class Solution implements JumpGameII {

        private final Map<Integer, Integer> cache = new HashMap<>();
        private int[] nums;

        @Override
        public int jump(int[] nums) {
            this.nums = nums;
            return minJumps(0, 0);
        }

        private int minJumps(int from, int jumps) {
            if (from >= nums.length - 1)
                return jumps;
            int val = nums[from];
            if (from + val >= nums.length - 1)
                return jumps + 1;
            if (cache.containsKey(from)) {
                int nextJumps = cache.get(from);
                return Integer.MAX_VALUE - nextJumps > jumps ? jumps + nextJumps : Integer.MAX_VALUE;
            }
            int minNextJumps = Integer.MAX_VALUE;
            for (; val > 0; val--) {
                int nextJumps = minJumps(from + val, jumps + 1);
                if (nextJumps < Integer.MAX_VALUE)
                    nextJumps -= jumps;
                if (nextJumps < minNextJumps)
                    minNextJumps = nextJumps;
            }
            cache.put(from, minNextJumps);
            return Integer.MAX_VALUE - minNextJumps > jumps ? jumps + minNextJumps : Integer.MAX_VALUE;
        }
    }
}
