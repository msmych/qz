package leetcode.greedy;

public interface StringWithoutAAAorBBB {

    String strWithout3a3b(int A, int B);

    class Solution implements StringWithoutAAAorBBB {

        @Override
        public String strWithout3a3b(int A, int B) {
            return next(A, B, "");
        }

        private String next(int a, int b, String s) {
            if (a == 0 && b == 0)
                return s;
            if (s.endsWith("aaa") || s.endsWith("bbb")
                    || (b == 0 && (s.endsWith("a") && a > 1 || s.endsWith("aa") && a > 0))
                    || (a == 0 && (s.endsWith("b") && b > 1 || s.endsWith("bb") && b > 0)))
                return null;
            if (a > b) {
                String next = next(a - 1, b, s + 'a');
                if (next != null)
                    return next;
                if (b > 0)
                    next = next(a, b - 1, s + 'b');
                return next;
            } else {
                String next = next(a, b - 1, s + 'b');
                if (next != null)
                    return next;
                if (a > 0)
                    next = next(a - 1, b, s + 'a');
                return next;
            }
        }
    }
}
