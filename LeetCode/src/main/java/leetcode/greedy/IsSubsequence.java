package leetcode.greedy;

public interface IsSubsequence {

    boolean isSubsequence(String s, String t);

    class Solution implements IsSubsequence {

        @Override
        public boolean isSubsequence(String s, String t) {
            int i = 0;
            for (char c : s.toCharArray()) {
                while (true) {
                    if (i >= t.length()) {
                        return false;
                    } else if (t.charAt(i++) == c) {
                        break;
                    }
                }
            }
            return true;
        }
    }
}
