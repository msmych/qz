package leetcode.greedy;

public interface DeleteColumnsToMakeSorted {

    int minDeletionSize(String[] A);

    class Solution implements DeleteColumnsToMakeSorted {

        @Override
        public int minDeletionSize(String[] A) {
            int deletionSize = 0;
            for (int i = 0; i < A[0].length(); i++) {
                for (int j = 1; j < A.length; j++) {
                    if (A[j].charAt(i) < A[j - 1].charAt(i)) {
                        deletionSize++;
                        break;
                    }
                }
            }
            return deletionSize;
        }
    }
}
