package leetcode.greedy;

import java.util.*;
import java.util.stream.IntStream;

public interface MaximizeSumOfArrayAfterKNegations {

    int largestSumAfterKNegations(int[] A, int K);

    class Solution implements MaximizeSumOfArrayAfterKNegations {

        @Override
        public int largestSumAfterKNegations(int[] A, int K) {
            Queue<int[]> negativeIndexes = new PriorityQueue<>(Comparator.comparingInt(pos -> pos[1]));
            int min = Integer.MAX_VALUE, minIndex = -1;
            for (int i = 0; i < A.length; i++) {
                if (A[i] < 0)
                    negativeIndexes.offer(new int[]{i, A[i]});
                int a = Math.abs(A[i]);
                if (a < min) {
                    min = a;
                    minIndex = i;
                }
            }
            for (; K > 0; K--) {
                if (!negativeIndexes.isEmpty()) {
                    int[] pos = negativeIndexes.poll();
                    A[pos[0]] = -A[pos[0]];
                } else
                    A[minIndex] = -A[minIndex];
            }
            return IntStream.of(A).sum();
        }
    }
}
