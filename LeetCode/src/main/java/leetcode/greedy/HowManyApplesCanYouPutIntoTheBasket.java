package leetcode.greedy;

import java.util.Arrays;

public interface HowManyApplesCanYouPutIntoTheBasket {

  int maxNumberOfApples(int[] arr);

  class Solution implements HowManyApplesCanYouPutIntoTheBasket {
    @Override
    public int maxNumberOfApples(int[] arr) {
      Arrays.sort(arr);
      int i = 0;
      int sum = 0;
      while (i < arr.length && sum + arr[i] <= 5000) {
        sum += arr[i++];
      }
      return i;
    }
  }
}
