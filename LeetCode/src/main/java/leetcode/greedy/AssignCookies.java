package leetcode.greedy;

import java.util.Arrays;

public interface AssignCookies {

    int findContentChildren(int[] g, int[] s);

    class Solution implements AssignCookies {
        @Override
        public int findContentChildren(int[] g, int[] s) {
            Arrays.sort(g);
            Arrays.sort(s);
            int content = 0;
            for (int i = s.length - 1, j = g.length - 1; i >= 0; i--) {
                if (j < 0) {
                    break;
                }
                while (j >= 0) {
                    if (s[i] >= g[j]) {
                        content++;
                        break;
                    }
                    j--;
                }
                j--;
            }
            return content;
        }
    }
}
