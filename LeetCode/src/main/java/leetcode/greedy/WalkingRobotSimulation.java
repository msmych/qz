package leetcode.greedy;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public interface WalkingRobotSimulation {

    int robotSim(int[] commands, int[][] obstacles);

    class Solution implements WalkingRobotSimulation {

        private enum Direction { NORTH, EAST, SOUTH, WEST }

        private class Point {
            int x;
            int y;

            Point(int x, int y) {
                this.x = x;
                this.y = y;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Point point = (Point) o;
                return x == point.x &&
                        y == point.y;
            }

            @Override
            public int hashCode() {
                return Objects.hash(x, y);
            }
        }

        private Set<Point> obstacles;
        private int x = 0, y = 0;
        private Direction direction = Direction.NORTH;

        @Override
        public int robotSim(int[] commands, int[][] obstacles) {
            int max = 0;
            this.obstacles = Arrays.stream(obstacles)
                    .map(obstacle -> new Point(obstacle[0], obstacle[1]))
                    .collect(Collectors.toSet());
            for (int command : commands) {
                if (command < 0)
                    turn(command);
                else
                    max = Math.max(max, move(command));
            }
            return max;
        }

        private void turn(int command) {
            switch (direction) {
                case NORTH:
                    direction = command == -1 ? Direction.EAST : Direction.WEST;
                    break;
                case EAST:
                    direction = command == -1 ? Direction.SOUTH : Direction.NORTH;
                    break;
                case SOUTH:
                    direction = command == -1 ? Direction.WEST : Direction.EAST;
                    break;
                case WEST:
                    direction = command == -1 ? Direction.NORTH : Direction.SOUTH;
            }
        }

        private int move(int command) {
            int max = 0;
            for (; command > 0; command--) {
                switch (direction) {
                    case NORTH:
                        if (!obstacles.contains(new Point(x, y + 1))) {
                            y++;
                            max = getMax(max);
                        }
                        break;
                    case EAST:
                        if (!obstacles.contains(new Point(x + 1, y))) {
                            x++;
                            max = getMax(max);
                        }
                        break;
                    case SOUTH:
                        if (!obstacles.contains(new Point(x, y - 1))) {
                            y--;
                            max = getMax(max);
                        }
                        break;
                    case WEST:
                        if (!obstacles.contains(new Point(x - 1, y))) {
                            x--;
                            max = getMax(max);
                        }
                }
            }
            return max;
        }

        private int getMax(int max) {
            return Math.max(max, x*x + y*y);
        }
    }
}
