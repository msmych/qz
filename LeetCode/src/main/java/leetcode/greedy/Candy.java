package leetcode.greedy;

import java.util.Arrays;

public interface Candy {

    int candy(int[] ratings);

    class Solution implements Candy {

        @Override
        public int candy(int[] ratings) {
            int[] candies = new int[ratings.length];
            int kids = 0;
            while (kids++ < candies.length) {
                int min = Integer.MAX_VALUE;
                int minIndex = -1;
                for (int i = 0; i < candies.length; i++) {
                    if (candies[i] == 0) {
                        if (ratings[i] < min) {
                            min = ratings[i];
                            minIndex = i;
                        }
                    }
                }
                candies[minIndex] = 1;
                if (minIndex > 0 &&
                        ratings[minIndex] > ratings[minIndex - 1] &&
                        candies[minIndex] <= candies[minIndex - 1])
                    candies[minIndex] = candies[minIndex - 1] + 1;
                if (minIndex < candies.length - 1 &&
                        ratings[minIndex] > ratings[minIndex + 1] &&
                        candies[minIndex] <= candies[minIndex + 1]) {
                    candies[minIndex] = candies[minIndex + 1] + 1;
                }
            }
            return Arrays.stream(candies).sum();
        }
    }
}
