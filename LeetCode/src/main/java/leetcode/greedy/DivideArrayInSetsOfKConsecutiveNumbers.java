package leetcode.greedy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.util.Arrays.sort;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public interface DivideArrayInSetsOfKConsecutiveNumbers {

  boolean isPossibleDivide(int[] nums, int k);

  class Solution implements DivideArrayInSetsOfKConsecutiveNumbers {
    @Override
    public boolean isPossibleDivide(int[] nums, int k) {
      if (nums.length % k != 0) {
        return false;
      }
      Map<Integer, Integer> occurrences = occurrences(nums);
      if (occurrences.size() == k && occurrences.values().stream().distinct().count() == 1) {
        return true;
      }
      sort(nums);
      Set<Integer> picked = new HashSet<>();
      int[] arr = new int[k];
      int i = 0, j = -1, last = 0;
      while (i < nums.length && picked.size() < nums.length) {
        if (picked.contains(i)) {
          if (last == i) {
            last++;
          }
          i++;
        } else if (j == arr.length - 1) {
          i = last;
          last = i;
          j = -1;
        } else if (j == -1 || nums[i] == arr[j] + 1) {
          arr[++j] = nums[i];
          if (last == i) {
            last++;
          }
          picked.add(i++);
        } else {
          i++;
        }
      }
      return picked.size() == nums.length;
    }

    private Map<Integer, Integer> occurrences(int[] nums) {
      return Arrays.stream(nums).boxed().collect(groupingBy(n -> n, summingInt(n -> 1)));
    }
  }
}
