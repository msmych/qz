package leetcode.greedy;

import java.util.*;
import java.util.stream.Collectors;

public interface TwoCityScheduling {

    int twoCitySchedCost(int[][] costs);

    class Solution implements TwoCityScheduling {

        private class City {
            int a, b;

            City(int a, int b) {
                this.a = a;
                this.b = b;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                City city = (City) o;
                return a == city.a &&
                        b == city.b;
            }

            @Override
            public int hashCode() {
                return Objects.hash(a, b);
            }
        }

        private final Map<List<City>, Integer> aCache = new HashMap<>();
        private final Map<List<City>, Integer> bCache = new HashMap<>();

        @Override
        public int twoCitySchedCost(int[][] costs) {
            return nextA(Arrays.stream(costs)
                    .map(cost -> new City(cost[0], cost[1]))
                    .collect(Collectors.toList()));
        }

        private int nextA(List<City> cities) {
            if (cities.isEmpty()) return 0;
            if (aCache.containsKey(cities))
                return aCache.get(cities);
            if (cities.size() == 1) return cities.get(0).a;
            int min = Integer.MAX_VALUE;
            for (City a : cities) {
                List<City> nextCities = new ArrayList<>(cities);
                nextCities.remove(a);
                int next = a.a + nextB(nextCities);
                if (next < min) min = next;
            }
            aCache.put(cities, min);
            return min;
        }

        private int nextB(List<City> cities) {
            if (cities.isEmpty()) return 0;
            if (bCache.containsKey(cities))
                return bCache.get(cities);
            if (cities.size() == 1) return cities.get(0).b;
            int min = Integer.MAX_VALUE;
            for (City b : cities) {
                List<City> nextCities = new ArrayList<>(cities);
                nextCities.remove(b);
                int next = b.b + nextA(nextCities);
                if (next < min) min = next;
            }
            bCache.put(cities, min);
            return min;
        }
    }
}
