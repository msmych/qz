package leetcode.greedy;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.IntStream.range;

public interface MinimumSwapsToMakeStringsEqual {

  int minimumSwap(String s1, String s2);

  class Solution implements MinimumSwapsToMakeStringsEqual {

    @Override
    public int minimumSwap(String s1, String s2) {
      Set<Integer> matches = matches(s1, s2);
      Map<Integer, Character> positionMap1 = positionMap(s1, matches);
      Map<Integer, Character> positionMap2 = positionMap(s2, matches);
      int swaps = 0;
      while (!positionMap1.isEmpty()) {
        int[] swap = null;
        for (Map.Entry<Integer, Character> entry : positionMap1.entrySet()) {
          Optional<Integer> maybeKey = positionMap1.entrySet().stream()
            .filter(e -> !e.getKey().equals(entry.getKey()))
            .filter(e -> e.getValue() == entry.getValue())
            .filter(e -> positionMap2.get(e.getKey()).equals(positionMap2.get(entry.getKey())))
            .findAny()
            .map(Map.Entry::getKey);
          if (maybeKey.isPresent()) {
            swap = new int[]{entry.getKey(), maybeKey.get()};
            break;
          }
        }
        if (swap == null) {
          break;
        }
        positionMap1.remove(swap[0]);
        positionMap1.remove(swap[1]);
        positionMap2.remove(swap[0]);
        positionMap2.remove(swap[1]);
        swaps++;
      }
      while (!positionMap1.isEmpty()) {
        int[] swap = null;
        for (Map.Entry<Integer, Character> entry : positionMap1.entrySet()) {
          Optional<Integer> maybeKey = positionMap1.entrySet().stream()
            .filter(e -> e.getValue().equals(positionMap2.get(entry.getKey())))
            .findAny()
            .map(Map.Entry::getKey);
          if (maybeKey.isPresent()) {
            swap = new int[]{entry.getKey(), maybeKey.get()};
            break;
          }
        }
        if (swap == null) {
          return -1;
        }
        positionMap1.remove(swap[0]);
        positionMap1.remove(swap[1]);
        positionMap2.remove(swap[0]);
        positionMap2.remove(swap[1]);
        swaps += 2;
      }
      return swaps;
    }

    private Set<Integer> matches(String s1, String s2) {
      return range(0, s1.length())
        .filter(i -> s1.charAt(i) == s2.charAt(i))
        .boxed()
        .collect(toSet());
    }

    private Map<Integer, Character> positionMap(String s, Set<Integer> matches) {
      return range(0, s.length())
        .filter(i -> !matches.contains(i))
        .boxed()
        .collect(toMap(i -> i, s::charAt));
    }
  }
}
