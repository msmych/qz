package leetcode.greedy;

public interface GasStation {

    int canCompleteCircuit(int[] gas, int[] cost);

    class Solution implements GasStation {

        @Override
        public int canCompleteCircuit(int[] gas, int[] cost) {
            for (int i = 0; i < gas.length; i++) {
                boolean canFinish = true;
                int g = 0;
                for (int j = 0; j < gas.length; j++) {
                    g += gas[(i + j) % gas.length];
                    g -= cost[(i + j) % cost.length];
                    if (g < 0) {
                        canFinish = false;
                        break;
                    }
                }
                if (canFinish) return i;
            }
            return -1;
        }
    }
}
