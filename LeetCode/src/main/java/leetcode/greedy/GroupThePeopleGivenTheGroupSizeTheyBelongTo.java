package leetcode.greedy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface GroupThePeopleGivenTheGroupSizeTheyBelongTo {

  List<List<Integer>> groupThePeople(int[] groupSizes);

  class Solution implements GroupThePeopleGivenTheGroupSizeTheyBelongTo {
    @Override
    public List<List<Integer>> groupThePeople(int[] groupSizes) {
      List<List<Integer>> groups = new ArrayList<>();
      Map<Integer, List<Integer>> groupsBySizes = new HashMap<>();
      for (int i = 0; i < groupSizes.length; i++) {
        int groupSize = groupSizes[i];
        if (groupsBySizes.containsKey(groupSize)) {
          groupsBySizes.get(groupSize).add(i);
        } else {
          List<Integer> group = new ArrayList<>();
          group.add(i);
          groupsBySizes.put(groupSize, group);
        }
        if (groupsBySizes.get(groupSize).size() == groupSize) {
          groups.add(groupsBySizes.get(groupSize));
          groupsBySizes.remove(groupSize);
        }
      }
      return groups;
    }
  }
}
