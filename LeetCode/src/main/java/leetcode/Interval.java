package leetcode;

public class Interval {
    public int start;
    public int end;
    public Interval(){ start = 0; end = 0; }
    public Interval(int s, int e) { start = s; end = e; }

    public static boolean intervalEquals(int start, int end, Interval interval) {
        return start == interval.start && end == interval.end;
    }
}
