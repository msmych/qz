package leetcode.heap;

import leetcode.linked_list.ListNode;

public interface MergeKSortedLists {

  ListNode mergeKLists(ListNode[] lists);

  class Solution implements MergeKSortedLists {

    @Override
    public ListNode mergeKLists(ListNode[] lists) {
      ListNode dummy = new ListNode(0);
      ListNode node = dummy;
      while (notEmpty(lists)) {
        node.next = new ListNode(getMin(lists));
        node = node.next;
      }
      return dummy.next;
    }

    private boolean notEmpty(ListNode[] lists) {
      for (ListNode list : lists)
        if (list != null)
          return true;
      return false;
    }

    private int getMin(ListNode[] lists) {
      int min = Integer.MAX_VALUE, minIndex = 0;
      for (int i = 0; i < lists.length; i++) {
        if (lists[i] != null && lists[i].val < min) {
          min = lists[i].val;
          minIndex = i;
        }
      }
      lists[minIndex] = lists[minIndex].next;
      return min;
    }
  }
}
