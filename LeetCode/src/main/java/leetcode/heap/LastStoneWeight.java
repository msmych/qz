package leetcode.heap;

import java.util.Arrays;

public interface LastStoneWeight {

    int lastStoneWeight(int[] stones);

    class Solution implements LastStoneWeight {

        @Override
        public int lastStoneWeight(int[] stones) {
            if (stones.length == 0) return 0;
            if (stones.length == 1) return stones[0];
            while (Arrays.stream(stones).filter(stone -> stone > 0).count() > 1) {
                int max1 = 0;
                int maxIndex1 = 0;
                int max2 = 0;
                int maxIndex2 = 0;
                for (int i = 0; i < stones.length; i++) {
                    int stone = stones[i];
                    if (stone > max1) {
                        max2 = max1;
                        maxIndex2 = maxIndex1;
                        max1 = stone;
                        maxIndex1 = i;
                    } else if (stone > max2) {
                        max2 = stone;
                        maxIndex2 = i;
                    }
                }
                stones[maxIndex2] = 0;
                stones[maxIndex1] = max1 - max2;
            }
            return Arrays.stream(stones).filter(stone -> stone > 0).findAny().orElse(0);
        }
    }
}
