package leetcode.heap;

public class KthLargest {

    class Node {

        int val;
        int count = 1;
        Node left;
        Node right;

        public Node(int val) {
            this.val = val;
        }
    }

    private final int k;
    private Node root;

    public KthLargest(int k, int[] nums) {
        this.k = k;
        for (int num : nums)
            root = addNode(root, num);
    }

    private Node addNode(Node node, int val) {
        if (node == null)
            return new Node(val);
        node.count++;
        if (val < node.val)
            node.left = addNode(node.left, val);
        else
            node.right = addNode(node.right, val);
        return node;
    }

    public int add(int val) {
        root = addNode(root, val);
        return find(root, k);
    }

    private int find(Node node, int k) {
        int v = node.right == null ? 0 : node.right.count;
        if (k == 1 && node.right == null || k == v + 1)
            return node.val;
        else if (k < v + 1)
            return find(node.right, k);
        else
            return find(node.left, k - v - 1);
    }
}

/**
 * Your KthLargest object will be instantiated and called as such:
 * KthLargest obj = new KthLargest(k, nums);
 * int param_1 = obj.add(val);
 */
