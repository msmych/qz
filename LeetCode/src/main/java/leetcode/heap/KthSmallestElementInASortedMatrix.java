package leetcode.heap;

public interface KthSmallestElementInASortedMatrix {

  int kthSmallest(int[][] matrix, int k);

  class Solution implements KthSmallestElementInASortedMatrix {
    @Override
    public int kthSmallest(int[][] matrix, int k) {
      int left = matrix[0][0], right = matrix[matrix.length - 1][matrix[0].length - 1];
      while (left < right) {
        int mid = left + (right - left) / 2;
        int count = 0;
        int j = matrix[0].length - 1;
        for (int i = 0; i < matrix.length; i++) {
          while (j >= 0 && matrix[i][j] > mid) {
            j--;
          }
          count += j + 1;
        }
        if (count < k) {
          left = mid + 1;
        } else {
          right = mid;
        }
      }
      return left;
    }
  }
}
