package leetcode.heap;

import java.util.LinkedList;
import java.util.Queue;

public interface SlidingWindowMaximum {

  int[] maxSlidingWindow(int[] nums, int k);

  class Solution implements SlidingWindowMaximum {
    @Override
    public int[] maxSlidingWindow(int[] nums, int k) {
      if (nums.length == 0) {
        return new int[0];
      }
      Queue<Integer> queue = new LinkedList<>();
      for (int i = 0; i < k; i++) {
        queue.offer(nums[i]);
      }
      int[] max = new int[nums.length - k + 1];
      for (int i = k; i < nums.length; i++) {
        max[i - k] = queue.stream().mapToInt(n -> n).max().getAsInt();
        queue.poll();
        queue.offer(nums[i]);
      }
      max[nums.length - k] = queue.stream().mapToInt(n -> n).max().getAsInt();
      return max;
    }
  }
}
