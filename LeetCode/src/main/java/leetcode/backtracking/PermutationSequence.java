package leetcode.backtracking;

public interface PermutationSequence {

    String getPermutation(int n, int k);

    class Solution implements PermutationSequence {

        @Override
        public String getPermutation(int n, int k) {
            int[] nums = new int[n];
            for (int i = 0; i < n; i++) {
                nums[i] = i + 1;
            }
            for (int i = 0; i < k - 1; i++) {
                nextPermutation(nums);
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < n; i++) {
                sb.append(nums[i]);
            }
            return sb.toString();
        }

        private void nextPermutation(int[] nums) {
            int sourceIndex = -1, targetIndex = -1;
            for (int i = nums.length - 1; i > 0; i--) {
                int source = nums[i];
                for (int j = i - 1; j >= 0; j--) {
                    int target = nums[j];
                    if (source > target && (targetIndex == -1 || j > targetIndex)) {
                        sourceIndex = i;
                        targetIndex = j;
                    }
                }
            }
            if (sourceIndex != -1) {
                int temp = nums[sourceIndex];
                nums[sourceIndex] = nums[targetIndex];
                nums[targetIndex] = temp;
                sort(nums, targetIndex + 1);
            } else
                sort(nums, 0);
        }

        private void sort(int[] nums, int from) {
            for (int i = from; i < nums.length - 1; i++) {
                int minIndex = i;
                for (int j = i + 1; j < nums.length; j++) {
                    if (nums[j] < nums[minIndex])
                        minIndex = j;
                }
                int temp = nums[i];
                nums[i] = nums[minIndex];
                nums[minIndex] = temp;
            }
        }
    }
}
