package leetcode.backtracking;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface GrayCode {

    List<Integer> grayCode(int n);

    class Solution implements GrayCode {

        private final Set<Integer> visited = new HashSet<>();

        private int size = 1;

        @Override
        public List<Integer> grayCode(int n) {
            for (; n > 0; n--)
                size *= 2;
            return next(new ArrayList<>(), 0);
        }

        public List<Integer> next(List<Integer> list, int a) {
            if (visited.contains(a))
                return new ArrayList<>();
            list.add(a);
            visited.add(a);
            if (list.size() == size)
                return list;
            for (int bit = 1; bit <= size; bit *= 2) {
                int next = a;
                if ((next & bit) > 0)
                    next -= bit;
                else
                    next += bit;
                List<Integer> nextList = next(list, next);
                if (!nextList.isEmpty())
                    return nextList;
            }
            return new ArrayList<>();
        }
    }
}
