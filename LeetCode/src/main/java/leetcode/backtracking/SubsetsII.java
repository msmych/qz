package leetcode.backtracking;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface SubsetsII {

    List<List<Integer>> subsetsWithDup(int[] nums);

    class Solution implements SubsetsII {

        @Override
        public List<List<Integer>> subsetsWithDup(int[] nums) {
            return new ArrayList<>(nextPower(IntStream.of(nums).boxed().collect(Collectors.toList())));
        }

        private Set<List<Integer>> nextPower(List<Integer> nums) {
            Set<List<Integer>> power = new HashSet<>();
            if (nums.isEmpty()) {
                power.add(new ArrayList<>());
                return power;
            }
            power.add(nums);
            for (int num : new HashSet<>(nums)) {
                List<Integer> next = new ArrayList<>(nums);
                next.remove(Integer.valueOf(num));
                power.addAll(nextPower(next));
            }
            return power;
        }
    }
}
