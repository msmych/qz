package leetcode.backtracking;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface Subsets {

  List<List<Integer>> subsets(int[] nums);

  class Solution implements Subsets {

    @Override
    public List<List<Integer>> subsets(int[] nums) {
      return nextSubsets(IntStream.of(nums).boxed().collect(Collectors.toSet())).stream()
        .map(ArrayList::new)
        .collect(Collectors.toList());
    }

    private Set<Set<Integer>> nextSubsets(Set<Integer> nums) {
      Set<Set<Integer>> subsets = new HashSet<>();
      if (nums.isEmpty()) {
        subsets.add(new HashSet<>());
        return subsets;
      }
      subsets.add(nums);
      for (int num : nums) {
        Set<Integer> next = new HashSet<>(nums);
        next.remove(num);
        subsets.addAll(nextSubsets(next));
      }
      return subsets;
    }
  }
}
