package leetcode.backtracking;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.rangeClosed;

public interface SequentialDigits {

  List<Integer> sequentialDigits(int low, int high);

  class Solution implements SequentialDigits {

    private static final String DIGITS = "123456789";

    private int low, high;

    @Override
    public List<Integer> sequentialDigits(int low, int high) {
      this.low = low;
      this.high = high;
      return rangeClosed(String.valueOf(low).length(), String.valueOf(high).length())
        .mapToObj(this::sequentialDigitsOfLength)
        .flatMap(List::stream)
        .collect(toList());
    }

    private List<Integer> sequentialDigitsOfLength(int len) {
      return rangeClosed(0, DIGITS.length() - len)
        .mapToObj(i -> DIGITS.substring(i, i + len))
        .map(Integer::parseInt)
        .filter(i -> i >= low)
        .filter(i -> i <= high)
        .collect(toList());
    }
  }
}
