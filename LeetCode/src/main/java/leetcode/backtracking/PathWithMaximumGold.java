package leetcode.backtracking;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public interface PathWithMaximumGold {

  int getMaximumGold(int[][] grid);

  class Solution implements PathWithMaximumGold {

    private static class Cell {
      int i, j;

      public Cell(int i, int j) {
        this.i = i;
        this.j = j;
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return i == cell.i &&
          j == cell.j;
      }

      @Override
      public int hashCode() {
        return Objects.hash(i, j);
      }
    }

    private int[][] grid;
    private int gold = 0;

    @Override
    public int getMaximumGold(int[][] grid) {
      if (grid.length == 0 || grid[0].length == 0) {
        return 0;
      }
      this.grid = grid;
      for (int i = 0; i < grid.length; i++) {
        for (int j = 0; j < grid[0].length; j++) {
          if (grid[i][j] > 0) {
            nextGold(0, new Cell(i, j), new HashSet<>());
          }
        }
      }
      return gold;
    }

    private void nextGold(int gold, Cell cell, Set<Cell> visited) {
      gold += grid[cell.i][cell.j];
      if (gold > this.gold) {
        this.gold = gold;
      }
      visited.add(cell);
      Cell left = new Cell(cell.i, cell.j - 1);
      if (canVisit(left, visited)) {
        nextGold(gold, left, new HashSet<>(visited));
      }
      Cell right = new Cell(cell.i, cell.j + 1);
      if (canVisit(right, visited)) {
        nextGold(gold, right, new HashSet<>(visited));
      }
      Cell up = new Cell(cell.i - 1, cell.j);
      if (canVisit(up, visited)) {
        nextGold(gold, up, new HashSet<>(visited));
      }
      Cell down = new Cell(cell.i + 1, cell.j);
      if (canVisit(down, visited)) {
        nextGold(gold, down, new HashSet<>(visited));
      }
    }

    private boolean canVisit(Cell cell, Set<Cell> visited) {
      if (cell.i < 0 || cell.i >= grid.length || cell.j < 0 || cell.j >= grid[0].length) {
        return false;
      }
      return grid[cell.i][cell.j] > 0 && !visited.contains(cell);
    }
  }
}
