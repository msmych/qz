package leetcode.backtracking;

import java.util.*;

public interface GenerateParentheses {

  List<String> generateParenthesis(int n);

  class Solution implements GenerateParentheses {

    private final Map<Integer, List<String>> cache = new HashMap<>();

    @Override
    public List<String> generateParenthesis(int n) {
      if (cache.containsKey(n)) {
        return cache.get(n);
      }
      List<String> parentheses = new ArrayList<>();
      if (n == 0) {
        parentheses.add("");
      } else {
        for (int i = 0; i < n; i++) {
          for (String left : generateParenthesis(i)) {
            for (String right : generateParenthesis(n - i - 1)) {
              parentheses.add("(" + left + ")" + right);
            }
          }
        }
      }
      cache.put(n, parentheses);
      return parentheses;
    }
  }
}
