package leetcode.backtracking;

import java.util.*;

public interface PalindromePartitioning {

  List<List<String>> partition(String s);

  class Solution implements PalindromePartitioning {

    private final Map<String, Boolean> cache = new HashMap<>();

    @Override
    public List<List<String>> partition(String s) {
      if (s.isEmpty())
        return new ArrayList<>();
      List<List<String>> partitions = new ArrayList<>();
      if (isPalindrome(s))
        partitions.add(Collections.singletonList(s));
      for (int i = 1; i < s.length(); i++) {
        String left = s.substring(0, i);
        if (isPalindrome(left)) {
          for (List<String> nextPartition : partition(s.substring(i))) {
            List<String> partition = new ArrayList<>(nextPartition);
            partition.add(0, left);
            partitions.add(partition);
          }
        }
      }
      return partitions;
    }

    private boolean isPalindrome(String s) {
      if (cache.containsKey(s))
        return cache.get(s);
      boolean isPalindrome = new StringBuffer(s).reverse().toString().equals(s);
      cache.put(s, isPalindrome);
      return isPalindrome;
    }
  }
}
