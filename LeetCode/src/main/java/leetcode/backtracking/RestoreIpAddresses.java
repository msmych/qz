package leetcode.backtracking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public interface RestoreIpAddresses {

    List<String> restoreIpAddresses(String s);

    class Solution implements RestoreIpAddresses {

        @Override
        public List<String> restoreIpAddresses(String s) {
            return nextIps(s, 4);
        }

        private List<String> nextIps(String s, int n) {
            if (n == 0 || s.isEmpty() || 3 * n < s.length())
                return new ArrayList<>();
            if (n == 1) {
                if (s.length() > 1 && s.startsWith("0"))
                    return new ArrayList<>();
                if (Integer.valueOf(s) > 255)
                    return new ArrayList<>();
                return Collections.singletonList(s);
            }
            List<String> ips = new ArrayList<>();
            for (int i = 1; i <= Math.min(3, s.length()); i++) {
                String part = s.substring(0, i);
                if (part.length() > 1 && part.startsWith("0"))
                    continue;
                if (Integer.valueOf(part) > 255)
                    continue;
                for (String ip : nextIps(s.substring(i), n - 1)) {
                    ips.add(part + '.' + ip);
                }
            }
            return ips;
        }
    }
}
