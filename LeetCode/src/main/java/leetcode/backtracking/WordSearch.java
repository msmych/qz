package leetcode.backtracking;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public interface WordSearch {

  boolean exist(char[][] board, String word);

  class Solution implements WordSearch {

    private static class Letter {
      char c;
      int i, j;

      Letter(char c, int i, int j) {
        this.c = c;
        this.i = i;
        this.j = j;
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Letter letter = (Letter) o;
        return c == letter.c &&
          i == letter.i &&
          j == letter.j;
      }

      @Override
      public int hashCode() {
        return Objects.hash(c, i, j);
      }
    }

    private final Set<Letter> visited = new HashSet<>();

    private char[][] board;

    @Override
    public boolean exist(char[][] board, String word) {
      if (board.length == 0)
        return false;
      this.board = board;
      if (word.length() > board.length * board[0].length)
        return false;
      for (int i = 0; i < board.length; i++) {
        for (int j = 0; j < board[0].length; j++) {
          if (existsNext(word, i, j))
            return true;
        }
      }
      return false;
    }

    private boolean existsNext(String word, int i, int j) {
      if (word.isEmpty())
        return true;
      char c = board[i][j];
      Letter letter = new Letter(c, i, j);
      if (c != word.charAt(0))
        return false;
      if (visited.contains(letter))
        return false;
      visited.add(letter);
      String next = word.substring(1);
      if (next.isEmpty())
        return true;
      if (j > 0 && existsNext(next, i, j - 1))
        return true;
      if (i > 0 && existsNext(next, i - 1, j))
        return true;
      if (j < board[0].length - 1 && existsNext(next, i, j + 1))
        return true;
      if (i < board.length - 1 && existsNext(next, i + 1, j))
        return true;
      visited.remove(letter);
      return false;
    }
  }
}
