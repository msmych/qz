package leetcode.backtracking;

import java.util.*;
import java.util.stream.Collectors;

public interface PermutationsII {

    List<List<Integer>> permuteUnique(int[] nums);

    class Solution implements PermutationsII {

        @Override
        public List<List<Integer>> permuteUnique(int[] nums) {
            Set<List<Integer>> perms = new HashSet<>();
            List<Integer> original = Arrays.stream(nums).boxed().collect(Collectors.toList());
            perms.add(original);
            List<Integer> perm;
            while (true) {
                perm = nextPermutation(nums);
                if (!perm.equals(original))
                    perms.add(perm);
                else
                    break;
            }
            return new ArrayList<>(perms);
        }

        private List<Integer> nextPermutation(int[] nums) {
            int sourceIndex = -1, targetIndex = -1;
            for (int i = nums.length - 1; i > 0; i--) {
                int source = nums[i];
                for (int j = i - 1; j >= 0; j--) {
                    int target = nums[j];
                    if (source > target && (targetIndex == -1 || j > targetIndex)) {
                        sourceIndex = i;
                        targetIndex = j;
                    }
                }
            }
            if (sourceIndex != -1) {
                int temp = nums[sourceIndex];
                nums[sourceIndex] = nums[targetIndex];
                nums[targetIndex] = temp;
                sort(nums, targetIndex + 1);
            } else
                sort(nums, 0);
            return Arrays.stream(nums).boxed().collect(Collectors.toList());
        }

        private void sort(int[] nums, int from) {
            for (int i = from; i < nums.length - 1; i++) {
                int minIndex = i;
                for (int j = i + 1; j < nums.length; j++) {
                    if (nums[j] < nums[minIndex])
                        minIndex = j;
                }
                int temp = nums[i];
                nums[i] = nums[minIndex];
                nums[minIndex] = temp;
            }
        }
    }
}
