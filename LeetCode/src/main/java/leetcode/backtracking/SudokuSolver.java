package leetcode.backtracking;

import java.util.*;

public interface SudokuSolver {

    void solveSudoku(char[][] board);

    class Solution implements SudokuSolver {

        @Override
        public void solveSudoku(char[][] board) {
            List<List<Character>> rows = getRows(board),
                    columns = getColumns(board),
                    boxes = getBoxes(board);
            int blanks = countBlanks(board), 
                    lastBlanks = blanks + 1;
            while (blanks > 0) {
                if (blanks == lastBlanks) {
                    int[] rowBlanks = getMinBlanksWithPosition(rows),
                            columnBlanks = getMinBlanksWithPosition(columns),
                            boxBlanks = getMinBlanksWithPosition(boxes);
                    if (isMinimumBlanks(rowBlanks, columnBlanks, boxBlanks))
                        guess(board, rowBlanks[1], rowBlanks[2]);
                    else if (isMinimumBlanks(columnBlanks, rowBlanks, boxBlanks))
                        guess(board, columnBlanks[2], columnBlanks[1]);
                    else
                        guess(board, getRowByBox(boxBlanks[1], boxBlanks[2]), getColumnByBox(boxBlanks[1], boxBlanks[2]));
                    return;
                }
                lastBlanks = blanks;
                for (int i = 0; i < board.length; i++) {
                    for (int j = 0; j < board.length; j++) {
                        if (board[i][j] != '.')
                            continue;
                        Set<Character> usedValues = new HashSet<>();
                        usedValues.addAll(rows.get(i));
                        usedValues.addAll(columns.get(j));
                        int boxIndex = getBoxIndex(i, j);
                        usedValues.addAll(boxes.get(boxIndex));
                        if (usedValues.size() == 9) {
                            char c = getOptions(usedValues).iterator().next();
                            board[i][j] = c;
                            rows.get(i).set(j, c);
                            columns.get(j).set(i, c);
                            boxes.get(boxIndex).set(getInnerBoxIndex(i, j), c);
                            blanks--;
                            if (hasContradiction(rows, columns, boxes))
                                return;
                        }
                    }
                }
            }
        }

        private List<List<Character>> getRows(char[][] board) {
            List<List<Character>> rows = new ArrayList<>();
            for (char[] row : board) {
                List<Character> cells = new ArrayList<>();
                for (char cell : row)
                    cells.add(cell);
                rows.add(cells);
            }
            return rows;
        }

        private List<List<Character>> getColumns(char[][] board) {
            List<List<Character>> columns = new ArrayList<>();
            for (char[] row : board) {
                for (int j = 0; j < board.length; j++) {
                    if (columns.size() <= j)
                        columns.add(new ArrayList<>());
                    columns.get(j).add(row[j]);
                }
            }
            return columns;
        }

        private List<List<Character>> getBoxes(char[][] board) {
            List<List<Character>> boxes = new ArrayList<>();
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board.length; j++) {
                    int boxIndex = getBoxIndex(i, j);
                    if (boxes.size() <= boxIndex)
                        boxes.add(new ArrayList<>());
                    boxes.get(boxIndex).add(board[i][j]);
                }
            }
            return boxes;
        }

        private int getBoxIndex(int i, int j) {
            return i - i % 3 + j / 3;
        }

        private int getInnerBoxIndex(int i, int j) {
            return 3 * (i % 3) + j % 3;
        }

        private int getRowByBox(int b, int bb) {
            return 3 * (b / 3) + bb / 3;
        }

        private int getColumnByBox(int b, int bb) {
            return 3 * (b % 3) + bb % 3;
        }

        private int countBlanks(char[][] board) {
            int blanks = 0;
            for (char[] row : board) {
                for (char cell : row) {
                    if (cell == '.')
                        blanks++;
                }
            }
            return blanks;
        }

        private int[] getMinBlanksWithPosition(List<List<Character>> board) {
            int minBlanks = 10, posi = -1, posj = -1;
            for (int i = 0; i < board.size(); i++) {
                int blanks = 0, blankj = -1;
                for (int j = 0; j < board.size(); j++) {
                    if (board.get(i).get(j) == '.') {
                        blanks++;
                        blankj = j;
                    }
                }
                if (blanks > 0 && blanks < minBlanks) {
                    minBlanks = blanks;
                    posi = i;
                    posj = blankj;
                }
            }
            return new int[]{minBlanks, posi, posj};
        }

        private boolean isMinimumBlanks(int[] min, int[] min1, int[] min2) {
            return min[0] < min1[0] && min[0] < min2[0];
        }

        private Set<Character> getOptions(Set<Character> usedNumbers) {
            Set<Character> options = new HashSet<>();
            for (int n = 1; n <= 9; n++) {
                char c = Character.forDigit(n, 10);
                if (!usedNumbers.contains(c))
                    options.add(c);
            }
            return options;
        }

        private boolean hasContradiction(List<List<Character>> rows,
                                         List<List<Character>> columns,
                                         List<List<Character>> boxes) {
            int[] rowBlanks = getMinBlanksWithPosition(rows),
                    columnBlanks = getMinBlanksWithPosition(columns),
                    boxBlanks = getMinBlanksWithPosition(boxes);
            if (rowBlanks[0] == 1) {
                char c = getOptions(new HashSet<>(rows.get(rowBlanks[1]))).iterator().next();
                if (columns.get(rowBlanks[2]).contains(c)
                        || boxes.get(getBoxIndex(rowBlanks[1], rowBlanks[2])).contains(c))
                    return true;
            }
            if (columnBlanks[0] == 1) {
                char c = getOptions(new HashSet<>(columns.get(columnBlanks[1]))).iterator().next();
                if (rows.get(columnBlanks[2]).contains(c)
                        || boxes.get(getBoxIndex(columnBlanks[2], columnBlanks[1])).contains(c))
                    return true;
            }
            if (boxBlanks[0] == 1) {
                char c = getOptions(new HashSet<>(boxes.get(boxBlanks[1]))).iterator().next();
                if (rows.get(getRowByBox(boxBlanks[1], boxBlanks[2])).contains(c)
                        || columns.get(getColumnByBox(boxBlanks[1], boxBlanks[2])).contains(c))
                    return true;
            }
            return false;
        }

        private void guess(char[][] board, int i, int j) {
            Set<Character> usedNumbers = new HashSet<>();
            for (int k = 0; k < board.length; k++) {
                usedNumbers.add(board[i][k]);
                usedNumbers.add(board[k][j]);
                usedNumbers.add(board[i - i % 3 + k / 3][j - j % 3 + k % 3]);
            }
            Set<Character> options = getOptions(usedNumbers);
            if (options.isEmpty())
                return;
            Iterator<Character> optionsIterator = options.iterator();
            char[][] guessBoard = new char[board.length][board.length];
            do {
                for (int x = 0; x < board.length; x++)
                    System.arraycopy(board[x], 0, guessBoard[x], 0, board.length);
                guessBoard[i][j] = optionsIterator.next();
                solveSudoku(guessBoard);
            } while (countBlanks(guessBoard) > 0 && optionsIterator.hasNext());
            if (countBlanks(guessBoard) == 0)
                System.arraycopy(guessBoard, 0, board, 0, board.length);
        }
    }
}
