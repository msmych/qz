package leetcode.backtracking;

import java.util.*;

public interface CombinationSumII {

    List<List<Integer>> combinationSum2(int[] candidates, int target);

    class Solution implements CombinationSumII {

        @Override
        public List<List<Integer>> combinationSum2(int[] candidates, int target) {
            List<Integer> candidatesList = new ArrayList<>();
            for (int candidate : candidates)
                candidatesList.add(candidate);
            List<List<Integer>> combinations = new ArrayList<>();
            for (Map<Integer, Integer> combinationMap : getCombinations(candidatesList, target)) {
                List<Integer> combination = new ArrayList<>();
                for (Map.Entry<Integer, Integer> item : combinationMap.entrySet()) {
                    for (int i = 0; i < item.getValue(); i++)
                        combination.add(item.getKey());
                }
                combinations.add(combination);
            }
            return combinations;
        }

        private Set<Map<Integer, Integer>> getCombinations(List<Integer> candidates, int target) {
            if (candidates.isEmpty() || target <= 0)
                return new HashSet<>();
            Set<Map<Integer, Integer>> combinations = new HashSet<>();
            for (int candidate : candidates) {
                if (candidate == target) {
                    Map<Integer, Integer> combination = new HashMap<>();
                    combination.put(candidate, 1);
                    combinations.add(combination);
                }
                List<Integer> next = new ArrayList<>(candidates);
                next.remove(next.indexOf(candidate));
                for (Map<Integer, Integer> nextCombination : getCombinations(next, target - candidate)) {
                    nextCombination.put(candidate, nextCombination.getOrDefault(candidate, 0) + 1);
                    combinations.add(nextCombination);
                }
            }
            return combinations;
        }
    }
}
