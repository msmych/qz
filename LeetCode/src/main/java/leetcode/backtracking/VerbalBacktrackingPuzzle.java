package leetcode.backtracking;

public interface VerbalBacktrackingPuzzle {

  boolean isSolvable(String[] words, String result);

  class Solution implements VerbalBacktrackingPuzzle {
    @Override
    public boolean isSolvable(String[] words, String result) {
      return false;
    }
  }
}
