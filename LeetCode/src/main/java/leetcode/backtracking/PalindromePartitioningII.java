package leetcode.backtracking;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public interface PalindromePartitioningII {

    int minCut(String s);

    class Solution implements PalindromePartitioningII {

        private final Map<String, Boolean> cache = new HashMap<>();
        private final Map<String, Integer> minCache = new HashMap<>();

        @Override
        public int minCut(String s) {
            if (s.isEmpty()) return 0;
            if (minCache.containsKey(s)) return minCache.get(s);
            if (isPalindrome(s)) return 0;
            for (int i = 1; i < s.length() - 1; i++) {
                if (isPalindrome(s.substring(0, i)) && isPalindrome(s.substring(i))) return 1;
            }
            int minCut = 1 + IntStream.range(1, s.length())
                    .boxed()
                    .map(i -> s.substring(0, i))
                    .filter(this::isPalindrome)
                    .map(String::length)
                    .map(s::substring)
                    .mapToInt(this::minCut)
                    .min()
                    .getAsInt();
            minCache.put(s, minCut);
            return minCut;
        }

        private boolean isPalindrome(String s) {
            if (cache.containsKey(s)) return cache.get(s);
            boolean isPalindrome = new StringBuffer(s).reverse().toString().equals(s);
            cache.put(s, isPalindrome);
            return isPalindrome;
        }
    }
}
