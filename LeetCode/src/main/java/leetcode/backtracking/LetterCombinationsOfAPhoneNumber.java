package leetcode.backtracking;

import java.util.*;

import static java.util.Collections.emptyList;

public interface LetterCombinationsOfAPhoneNumber {

  List<String> letterCombinations(String digits);

  class Solution implements LetterCombinationsOfAPhoneNumber {

    private final Map<Character, List<String>> keyboard = new HashMap<>();

    @Override
    public List<String> letterCombinations(String digits) {
      putButton('2', "a", "b", "c");
      putButton('3', "d", "e", "f");
      putButton('4', "g", "h", "i");
      putButton('5', "j", "k", "l");
      putButton('6', "m", "n", "o");
      putButton('7', "p", "q", "r", "s");
      putButton('8', "t", "u", "v");
      putButton('9', "w", "x", "y", "z");
      return getCombinations(digits);
    }

    private void putButton(char digit, String... letters) {
      List<String> button = new ArrayList<>();
      Collections.addAll(button, letters);
      keyboard.put(digit, button);
    }

    private List<String> getCombinations(String digits) {
      if (digits.isEmpty()) {
        return emptyList();
      }
      if (digits.length() == 1) {
        return keyboard.get(digits.charAt(0));
      }
      List<String> combinations = new ArrayList<>();
      for (String next : getCombinations(digits.substring(1))) {
        for (String letter : keyboard.get(digits.charAt(0))) {
          combinations.add(letter + next);
        }
      }
      return combinations;
    }
  }
}
