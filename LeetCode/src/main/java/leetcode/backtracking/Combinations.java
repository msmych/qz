package leetcode.backtracking;

import java.util.ArrayList;
import java.util.List;

public interface Combinations {

    List<List<Integer>> combine(int n, int k);

    class Solution implements Combinations {

        @Override
        public List<List<Integer>> combine(int n, int k) {
            List<List<Integer>> combinations = new ArrayList<>();
            nextCombination(n, k, 1, new ArrayList<>(), combinations);
            return combinations;
        }

        private void nextCombination(int n, int k, int index, ArrayList<Integer> current, List<List<Integer>> combinations) {
            if (current.size() == k) {
                combinations.add(new ArrayList<>(current));
                return;
            }
            for (int i = index; i <= n; i++) {
                current.add(i);
                nextCombination(n, k, i + 1, current, combinations);
                current.remove(current.size() - 1);
            }
        }
    }
}
