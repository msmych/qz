package leetcode.backtracking;

import java.util.*;

public interface NQueens {

    List<List<String>> solveNQueens(int n);

    class Solution implements NQueens {

        private class Cell {
            int i, j;

            Cell(int i, int j) {
                this.i = i;
                this.j = j;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Cell cell = (Cell) o;
                return i == cell.i &&
                        j == cell.j;
            }

            @Override
            public int hashCode() {
                return Objects.hash(i, j);
            }
        }

        private int size;

        @Override
        public List<List<String>> solveNQueens(int n) {
            this.size = n;
            List<List<String>> boards = new ArrayList<>();
            for (Set<Cell> queens : solveNext(n, new HashSet<>(), new HashSet<>())) {
                List<String> board = new ArrayList<>();
                for (int i = 0; i < size; i++) {
                    StringBuilder sb = new StringBuilder();
                    for (int j = 0; j < size; j++) {
                        sb.append(queens.contains(new Cell(i, j)) ? 'Q' : '.');
                    }
                    board.add(sb.toString());
                }
                boards.add(board);
            }
            return boards;
        }

        private Set<Set<Cell>> solveNext(int n, Set<Cell> lastQueens, Set<Cell> lastOccupied) {
            Set<Set<Cell>> queens = new HashSet<>();
            if (n == 0) {
                queens.add(lastQueens);
                return queens;
            }
            if (size * size - lastOccupied.size() < n)
                return new HashSet<>();
            int i = size - n;
            for (int j = 0; j < size; j++) {
                Cell queen = new Cell(i, j);
                if (lastOccupied.contains(queen))
                    continue;
                Set<Cell> nextQueens = new HashSet<>(lastQueens);
                nextQueens.add(queen);
                Set<Cell> nextOccupied = new HashSet<>(lastOccupied);
                nextOccupied.addAll(getOccupiedRow(i));
                nextOccupied.addAll(getOccupiedColumn(j));
                nextOccupied.addAll(getOccupiedDownDiagonal(i, j));
                nextOccupied.addAll(getOccupiedUpDiagonal(i, j));
                queens.addAll(solveNext(n - 1, nextQueens, nextOccupied));
            }
            return queens;
        }

        private Set<Cell> getOccupiedRow(int i) {
            Set<Cell> row = new HashSet<>();
            for (int j = 0; j < size; j++) {
                row.add(new Cell(i, j));
            }
            return row;
        }

        private Set<Cell> getOccupiedColumn(int j) {
            Set<Cell> column = new HashSet<>();
            for (int i = 0; i < size; i++) {
                column.add(new Cell(i, j));
            }
            return column;
        }

        private Set<Cell> getOccupiedDownDiagonal(int i, int j) {
            Set<Cell> diagonal = new HashSet<>();
            int x = i, y = j;
            while (--x >= 0 && --y >= 0) {
                diagonal.add(new Cell(x, y));
            }
            x = i;
            y = j;
            while (++x < size && ++y < size) {
                diagonal.add(new Cell(x, y));
            }
            return diagonal;
        }

        private Set<Cell> getOccupiedUpDiagonal(int i, int j) {
            Set<Cell> diagonal = new HashSet<>();
            int x = i, y = j;
            while (--x >= 0 && ++y < size) {
                diagonal.add(new Cell(x, y));
            }
            x = i;
            y = j;
            while (++x < size && --y >= 0) {
                diagonal.add(new Cell(x, y));
            }
            return diagonal;
        }
    }
}
