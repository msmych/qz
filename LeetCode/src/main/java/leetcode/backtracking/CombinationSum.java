package leetcode.backtracking;

import java.util.*;

public interface CombinationSum {

    List<List<Integer>> combinationSum(int[] candidates, int target);

    class Solution implements CombinationSum {

        private int[] candidates;

        @Override
        public List<List<Integer>> combinationSum(int[] candidates, int target) {
            this.candidates = candidates;
            List<List<Integer>> combinations = new ArrayList<>();
            for (Map<Integer, Integer> combinationMap : getCombinations(target)) {
                List<Integer> combination = new ArrayList<>();
                for (Map.Entry<Integer, Integer> item : combinationMap.entrySet()) {
                    for (int i = 0; i < item.getValue(); i++)
                        combination.add(item.getKey());
                }
                combinations.add(combination);
            }
            return combinations;
        }

        private Set<Map<Integer, Integer>> getCombinations(int target) {
            Set<Map<Integer, Integer>> combinations = new HashSet<>();
            if (target <= 0)
                return combinations;
            for (int candidate : candidates) {
                if (candidate == target) {
                    Map<Integer, Integer> combination = new HashMap<>();
                    combination.put(candidate, 1);
                    combinations.add(combination);
                }
                for (Map<Integer, Integer> combination : getCombinations(target - candidate)) {
                    combination.put(candidate, combination.getOrDefault(candidate, 0) + 1);
                    combinations.add(combination);
                }
            }
            return combinations;
        }
    }
}
