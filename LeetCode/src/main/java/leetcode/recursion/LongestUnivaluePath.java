package leetcode.recursion;

import leetcode.tree.TreeNode;

public interface LongestUnivaluePath {

    int longestUnivaluePath(TreeNode root);

    class Solution implements LongestUnivaluePath {

        @Override
        public int longestUnivaluePath(TreeNode root) {
            int[] longest = nextLongest(root, null);
            return Math.max(longest[0], longest[1]);
        }

        private int[] nextLongest(TreeNode node, Integer parentVal) {
            if (node == null) return new int[2];
            int[] leftPath = nextLongest(node.left, node.val);
            int[] rightPath = nextLongest(node.right, node.val);
            int downPath = Math.max(leftPath[1] + rightPath[1],
                    Math.max(leftPath[0], rightPath[0]));
            int upPath = parentVal != null && node.val == parentVal
                    ? 1 + Math.max(leftPath[1], rightPath[1])
                    : 0;
            return new int[]{downPath, upPath};
        }
    }
}
