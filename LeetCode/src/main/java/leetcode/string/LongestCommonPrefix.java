package leetcode.string;

public interface LongestCommonPrefix {

    String longestCommonPrefix(String[] strs);

    class Solution implements LongestCommonPrefix {

        @Override
        public String longestCommonPrefix(String[] strs) {
            if (strs.length == 0)
                return "";
            StringBuilder sb = new StringBuilder();
            while (true) {
                if (strs[0].length() <= sb.length())
                    break;
                char c = strs[0].charAt(sb.length());
                boolean isCommon = true;
                for (String str : strs) {
                    if (str.length() <= sb.length() || str.charAt(sb.length()) != c) {
                        isCommon = false;
                        break;
                    }
                }
                if (isCommon)
                    sb.append(c);
                else
                    break;
            }
            return sb.toString();
        }
    }
}
