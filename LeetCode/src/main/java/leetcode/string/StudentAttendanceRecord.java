package leetcode.string;

public interface StudentAttendanceRecord {

    boolean checkRecord(String s);

    class Solution implements StudentAttendanceRecord {

        @Override
        public boolean checkRecord(String s) {
            if (s.contains("LLL")) {
                return false;
            }
            int aIndex = s.indexOf("A");
            if (aIndex == -1) {
                return true;
            }
            return !s.substring(aIndex + 1).contains("A");
        }
    }
}
