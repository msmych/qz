package leetcode.string;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface TextJustification {

    List<String> fullJustify(String[] words, int maxWidth);

    class Solution implements TextJustification {

        private String[] words;
        private int maxWidth;
        private List<String> line;

        @Override
        public List<String> fullJustify(String[] words, int maxWidth) {
            this.words = words;
            this.maxWidth = maxWidth;
            List<String> justified = new ArrayList<>();
            int i = 0;
            while (i < words.length && isNotLast(0, i)) {
                line = new ArrayList<>();
                do {
                    if (!line.isEmpty())
                        line.add(" ");
                    line.add(words[i]);
                } while (isJustifiableNotLast(getLineLength(), ++i));
                while (getLineLength() < maxWidth) {
                    if (line.size() == 1)
                        line.add(" ");
                    for (int k = 1; k < line.size() && getLineLength() < maxWidth; k += 2) {
                        line.set(k, line.get(k) + " ");
                    }
                }
                justified.add(String.join("", line));
            }
            StringBuilder last = new StringBuilder(
                    String.join(" ", Arrays.copyOfRange(words, i, words.length)));
            while (last.length() < maxWidth)
                last.append(" ");
            justified.add(last.toString());
            return justified;
        }

        private boolean isJustifiableNotLast(int occupied, int i) {
            return getLineLength() + 1 + words[i].length() <= maxWidth
                    && isNotLast(occupied, i);
        }

        private int getLineLength() {
            return line.stream().mapToInt(String::length).sum();
        }

        private boolean isNotLast(int occupied, int i) {
            int length = occupied == 0 ? words[i].length() : occupied + 1 + words[i].length();
            for (int k = i + 1; k < words.length; k++) {
                length += 1 + words[k].length();
                if (length > maxWidth)
                    return true;
            }
            return false;
        }
    }
}
