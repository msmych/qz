package leetcode.string;

public interface LengthOfLastWord {

    int lengthOfLastWord(String s);

    class Solution implements LengthOfLastWord {

        @Override
        public int lengthOfLastWord(String s) {
            String[] words = s.split(" ");
            if (words.length == 0)
                return 0;
            return words[words.length - 1].length();
        }
    }
}
