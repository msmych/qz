package leetcode.string;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public interface UniqueEmailAddresses {

    int numUniqueEmails(String[] emails);

    class Solution implements UniqueEmailAddresses {

        @Override
        public int numUniqueEmails(String[] emails) {
            Map<String, Set<String>> addresses = new HashMap<>();
            for (String email : emails) {
                String[] localDomain = email.split("@");
                addresses.putIfAbsent(localDomain[1], new HashSet<>());
                addresses.get(localDomain[1])
                        .add(localDomain[0].split("\\+")[0].replaceAll("\\.", ""));
            }
            return addresses.values().stream().map(Set::size).reduce((s1, s2) -> s1 + s2).orElse(0);
        }
    }
}
