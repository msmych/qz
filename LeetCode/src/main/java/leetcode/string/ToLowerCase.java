package leetcode.string;

public interface ToLowerCase {

    String toLowerCase(String str);

    class Solution implements ToLowerCase {

        @Override
        public String toLowerCase(String str) {
            StringBuilder sb = new StringBuilder();
            for (char c : str.toCharArray()) {
                sb.append(Character.toLowerCase(c));
            }
            return sb.toString();
        }
    }
}
