package leetcode.string;

import java.util.Stack;

public interface ReverseOnlyLetters {

    String reverseOnlyLetters(String S);

    class Solution implements ReverseOnlyLetters {

        private final Stack<Character> letters = new Stack<>();

        @Override
        public String reverseOnlyLetters(String S) {
            for (char c : S.toCharArray()) {
                if (Character.isAlphabetic(c))
                    letters.push(c);
            }
            StringBuilder stringBuilder = new StringBuilder();
            for (char c : S.toCharArray()) {
                if (Character.isAlphabetic(c)) {
                    stringBuilder.append(letters.peek());
                    letters.pop();
                } else
                    stringBuilder.append(c);
            }
            return stringBuilder.toString();
        }
    }
}
