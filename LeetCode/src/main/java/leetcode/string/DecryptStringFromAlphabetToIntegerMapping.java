package leetcode.string;

import static java.lang.Integer.parseInt;

public interface DecryptStringFromAlphabetToIntegerMapping {

  String freqAlphabets(String s);

  class Solution implements DecryptStringFromAlphabetToIntegerMapping {
    @Override
    public String freqAlphabets(String s) {
      StringBuilder sb = new StringBuilder();
      int i = s.length() - 1;
      while (i >= 0) {
        if (s.charAt(i) == '#') {
          sb.insert(0, (char) ('a' + parseInt(s.substring(i - 2, i)) - 1));
          i -= 3;
        } else {
          sb.insert(0, (char) ('a' + s.charAt(i) - '1'));
          i--;
        }
      }
      return sb.toString();
    }
  }
}
