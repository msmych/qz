package leetcode.string;

public interface RotateString {

    boolean rotateString(String A, String B);

    class Solution implements RotateString {

        @Override
        public boolean rotateString(String A, String B) {
            if (A.length() != B.length())
                return false;
            if (A.isEmpty())
                return true;
            for (int i = 0; i < A.length(); i++) {
                if (B.startsWith(A.substring(i)) && B.endsWith(A.substring(0, i)))
                    return true;
            }
            return false;
        }
    }
}
