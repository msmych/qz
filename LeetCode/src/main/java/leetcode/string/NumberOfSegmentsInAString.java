package leetcode.string;

import java.util.Arrays;

public interface NumberOfSegmentsInAString {

  int countSegments(String s);

  class Solution implements NumberOfSegmentsInAString {
    @Override
    public int countSegments(String s) {
      return (int) Arrays.stream(s.split(" "))
        .filter(segment -> !segment.isEmpty())
        .count();
    }
  }
}
