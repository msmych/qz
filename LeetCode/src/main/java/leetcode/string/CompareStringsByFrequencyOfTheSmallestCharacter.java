package leetcode.string;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public interface CompareStringsByFrequencyOfTheSmallestCharacter {

    int[] numSmallerByFrequency(String[] queries, String[] words);

    class Solution implements CompareStringsByFrequencyOfTheSmallestCharacter {

        @Override
        public int[] numSmallerByFrequency(String[] queries, String[] words) {
            List<Integer> wordsFrequencies = Arrays.stream(words)
                .map(this::smallestCharacterFrequency)
                .collect(Collectors.toList());
            int[] nums = new int[queries.length];
            for (int i = 0; i < queries.length; i++) {
                int frequency = smallestCharacterFrequency(queries[i]);
                nums[i] = (int) wordsFrequencies.stream()
                    .filter(f -> f > frequency)
                    .count();
            }
            return nums;
        }

        private int smallestCharacterFrequency(String s) {
            int smallest = s.chars().min().getAsInt();
            return (int) s.chars()
                .filter(c -> c == smallest)
                .count();
        }
    }

}
