package leetcode.string;

public interface RepeatedSubstringPattern {

    boolean repeatedSubstringPattern(String s);

    class Solution implements RepeatedSubstringPattern {
        @Override
        public boolean repeatedSubstringPattern(String s) {
            for (int i = 1; i <= s.length() / 2; i += s.length() % 2 == 0 ? 1 : 2) {
                if (s.length() % i != 0) {
                    continue;
                }
                String part = s.substring(0, i);
                boolean repeating = true;
                for (int j = 0; j < s.length() / i; j++) {
                    if (!s.substring(i * j, i * j + i).equals(part)) {
                        repeating = false;
                        break;
                    }
                }
                if (repeating) {
                    return true;
                }
            }
            return false;
        }
    }
}
