package leetcode.string;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.rangeClosed;

public interface SearchSuggestionsSystem {

  List<List<String>> suggestedProducts(String[] products, String searchWord);

  class Solution implements SearchSuggestionsSystem {
    @Override
    public List<List<String>> suggestedProducts(String[] products, String searchWord) {
      return rangeClosed(1, searchWord.length())
        .mapToObj(i -> searchWord.substring(0, i))
        .map(prefix -> stream(products)
          .filter(product -> product.startsWith(prefix))
          .sorted()
          .limit(3)
          .collect(toList()))
        .collect(toList());
    }
  }
}
