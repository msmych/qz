package leetcode.string;

public interface ReverseStringII {

    String reverseStr(String s, int k);

    class Solution implements ReverseStringII {

        @Override
        public String reverseStr(String s, int k) {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            for (; s.length() - i * k >= k; i++) {
                String substring = s.substring(i * k, (i + 1) * k);
                if (i % 2 == 0) {
                    sb.append(reverseString(substring));
                } else {
                    sb.append(substring);
                }
            }
            String substring = s.substring(i * k);
            if (i % 2 == 0) {
                sb.append(reverseString(substring));
            } else {
                sb.append(substring);
            }
            return sb.toString();
        }

        public String reverseString(String s) {
            int i = 0, j = s.length() - 1;
            char[] chars = new char[s.length()];
            while (i <= j) {
                chars[i] = s.charAt(j);
                chars[j] = s.charAt(i);
                i++;
                j--;
            }
            return new String(chars);
        }
    }

}
