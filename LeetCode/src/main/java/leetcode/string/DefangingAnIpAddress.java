package leetcode.string;

public interface DefangingAnIpAddress {

    String defangIPaddr(String address);

    class Solution implements DefangingAnIpAddress {

        @Override
        public String defangIPaddr(String address) {
            return address.replaceAll("\\.", "[.]");
        }
    }
}
