package leetcode.string;

import java.util.Arrays;

public interface BasicCalculatorII {

  int calculate(String s);

  class Solution implements BasicCalculatorII {

    @Override
    public int calculate(String s) {
      return Arrays.stream(s.replaceAll(" ", "").split("\\+"))
        .mapToInt(this::calcNext)
        .sum();
    }

    private int calcNext(String s) {
      int minusIndex = s.lastIndexOf('-');
      if (minusIndex != -1 && minusIndex != 0) {
        return calcNext(s.substring(0, minusIndex)) - calcNext(s.substring(minusIndex + 1));
      }
      if (!s.contains("*") && !s.contains("/")) {
        return Integer.parseInt(s);
      }
      int timesIndex = s.lastIndexOf('*');
      int divideIndex = s.lastIndexOf('/');
      return timesIndex > divideIndex
        ? calcNext(s.substring(0, timesIndex)) * calcNext(s.substring(timesIndex + 1))
        : calcNext(s.substring(0, divideIndex)) / calcNext(s.substring(divideIndex + 1));
    }
  }
}
