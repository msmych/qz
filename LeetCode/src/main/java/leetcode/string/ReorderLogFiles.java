package leetcode.string;

import java.util.*;
import java.util.stream.Collectors;

public interface ReorderLogFiles {

    String[] reorderLogFiles(String[] logs);

    class Solution implements ReorderLogFiles {

        @Override
        public String[] reorderLogFiles(String[] logs) {
            List<Character> digits = Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
            Map<Boolean, List<String>> separated = Arrays.stream(logs)
                    .collect(Collectors.groupingBy(log -> digits.contains(log.split(" ")[1].charAt(0))));
            List<String> ordered = new LinkedList<>();
            ordered.addAll(separated.get(false).stream().sorted((log1, log2) -> {
                String[] parts1 = log1.split(" "),
                        parts2 = log2.split(" ");
                for (int i = 1; i < (parts1.length > parts2.length ? parts1.length : parts2.length); i++) {
                    if (i >= parts1.length)
                        return 1;
                    if (i >= parts2.length)
                        return -1;
                    int comp = compare(parts1[i], parts2[i]);
                    if (comp != 0)
                        return comp;
                }
                return compare(parts1[0], parts2[0]);
            }).collect(Collectors.toList()));
            ordered.addAll(separated.get(true));
            return ordered.toArray(new String[]{});
        }

        private int compare(String s1, String s2) {
            for (int i = 0; i < (s1.length() > s2.length() ? s1.length() : s2.length()); i++) {
                if (i >= s1.length())
                    return -1;
                if (i >= s2.length())
                    return 1;
                char c1 = s1.charAt(i),
                        c2 = s2.charAt(i);
                if (c1 > c2)
                    return 1;
                if (c1 < c2)
                    return -1;
            }
            return 0;
        }
    }
}
