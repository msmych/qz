package leetcode.string;

public interface RobotReturnToOrigin {

    boolean judgeCircle(String moves);

    class Solution implements RobotReturnToOrigin {

        @Override
        public boolean judgeCircle(String moves) {
            int x = 0;
            int y = 0;
            for (char move : moves.toCharArray()) {
                switch (move) {
                    case 'U': y++; break;
                    case 'R': x++; break;
                    case 'D': y--; break;
                    case 'L': x--;
                }
            }
            return x == 0 && y == 0;
        }
    }
}
