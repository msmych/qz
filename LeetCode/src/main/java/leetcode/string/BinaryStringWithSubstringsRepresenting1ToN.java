package leetcode.string;

public interface BinaryStringWithSubstringsRepresenting1ToN {

    boolean queryString(String S, int N);

    class Solution implements BinaryStringWithSubstringsRepresenting1ToN {

        @Override
        public boolean queryString(String S, int N) {
            for (; N > 0; N--) {
                if (!S.contains(getBinary(N)))
                    return false;
            }
            return true;
        }

        private String getBinary(int n) {
            StringBuilder sb = new StringBuilder();
            for (int bit = 1; bit <= n; bit *= 2) {
                sb.insert(0, (n & bit) == 0 ? '0' : '1');
            }
            return sb.toString();
        }
    }
}
