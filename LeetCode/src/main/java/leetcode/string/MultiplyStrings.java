package leetcode.string;

public interface MultiplyStrings {

    String multiply(String num1, String num2);

    class Solution implements MultiplyStrings {

        @Override
        public String multiply(String num1, String num2) {
            if (num1.isEmpty() || num2.isEmpty())
                return "0";
            String[] subProducts = new String[num2.length()];
            int reminder = 0;
            for (int i = num2.length() - 1; i >= 0; i--) {
                int i2 = num2.length() - i - 1, n2 = num2.charAt(i) - '0';
                subProducts[i2] = "";
                for (int shift = 0; shift < i2; shift++)
                    subProducts[i2] = subProducts[i2] + '0';
                for (int j = num1.length() - 1; j >= 0; j--) {
                    int n1 = num1.charAt(j) - '0';
                    int p = n1 * n2 + reminder;
                    reminder = p / 10;
                    subProducts[i2] = p % 10 + subProducts[i2];
                }
                if (reminder > 0) {
                    subProducts[i2] = reminder + subProducts[i2];
                    reminder = 0;
                }
                int extra = num2.length() + num1.length() - subProducts[i2].length();
                for (; extra > 0; extra--)
                    subProducts[i2] = '0' + subProducts[i2];
            }
            StringBuilder sb = new StringBuilder();
            reminder = 0;
            for (int i = subProducts[subProducts.length - 1].length() - 1; i >= 0; i--) {
                int sum = reminder;
                for (String subProduct : subProducts)
                    sum += subProduct.charAt(i) - '0';
                reminder = sum / 10;
                sb.insert(0, sum % 10);
            }
            String product = sb.toString();
            while (product.length() > 1 && product.charAt(0) == '0')
                product = product.substring(1);
            return product;
        }
    }
}
