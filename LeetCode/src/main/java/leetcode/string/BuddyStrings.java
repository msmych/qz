package leetcode.string;

import java.util.HashMap;
import java.util.Map;

public interface BuddyStrings {

    boolean buddyStrings(String A, String B);

    class Solution implements BuddyStrings {

        @Override
        public boolean buddyStrings(String A, String B) {
            if (A.length() != B.length())
                return false;
            Map<Character, Integer> amap = new HashMap<>(),
                    bmap = new HashMap<>();
            int difCount = 0;
            for (int i = 0; i < A.length(); i++) {
                char a = A.charAt(i),
                        b = B.charAt(i);
                if (a != b)
                    difCount++;
                if (difCount > 2)
                    return false;
                amap.put(a, amap.getOrDefault(a, 0) + 1);
                bmap.put(b, bmap.getOrDefault(b, 0) + 1);
            }
            if (amap.size() != bmap.size())
                return false;
            for (Map.Entry<Character, Integer> entry : amap.entrySet()) {
                if (!bmap.containsKey(entry.getKey()))
                    return false;
                if (!bmap.get(entry.getKey()).equals(entry.getValue()))
                    return false;
            }
            return difCount == 2
                    || difCount == 0 && amap.values().stream()
                            .anyMatch(v -> v > 1);
        }
    }
}
