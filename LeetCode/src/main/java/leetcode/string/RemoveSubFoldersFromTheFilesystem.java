package leetcode.string;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.emptyList;

public interface RemoveSubFoldersFromTheFilesystem {

  List<String> removeSubfolders(String[] folder);

  class Solution implements RemoveSubFoldersFromTheFilesystem {

    @Override
    public List<String> removeSubfolders(String[] folder) {
      if (folder.length == 0) {
        return emptyList();
      }
      Arrays.sort(folder);
      List<String> list = new ArrayList<>();
      String lastPath = folder[0];
      int lastDepth = depth(lastPath);
      list.add(lastPath);
      for (int i = 1; i < folder.length; i++) {
        int depth = depth(folder[i]);
        if (!folder[i].startsWith(lastPath) || depth <= lastDepth) {
          list.add(folder[i]);
          lastPath = folder[i];
          lastDepth = depth;
        }
      }
      return list;
    }

    private int depth(String path) {
      return path.split("/").length;
    }
  }
}
