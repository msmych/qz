package leetcode.string;

public interface ReverseWordsInAString {

    String reverseWords(String s);

    class Solution implements ReverseWordsInAString {

        @Override
        public String reverseWords(String s) {
            String[] words = s.split(" ");
            int i = 0;
            int j = words.length - 1;
            while (i <= j) {
                String left = words[i];
                String right = words[j];
                words[i] = right;
                words[j] = left;
                i++;
                j--;
            }
            StringBuilder sb = new StringBuilder();
            if (words.length > 0 && !words[0].isEmpty()) {
                sb.append(words[0]);
            }
            for (int k = 1; k < words.length; k++) {
                if (!words[k].isEmpty()) {
                    sb.append(" ").append(words[k]);
                }
            }
            return sb.toString();
        }
    }

    class SolutionIII implements ReverseWordsInAString {
        @Override
        public String reverseWords(String s) {
            String[] words = s.split(" ");
            for (int k = 0; k < words.length; k++) {
                char[] wordChars = words[k].toCharArray();
                int i = 0;
                int j = wordChars.length - 1;
                while (i <= j) {
                    char left = wordChars[i];
                    char right = wordChars[j];
                    wordChars[i] = right;
                    wordChars[j] = left;
                    i++;
                    j--;
                }
                words[k] = new String(wordChars);
            }
            StringBuilder sb = new StringBuilder();
            if (words.length > 0) {
                sb.append(words[0]);
            }
            for (int i = 1; i < words.length; i++) {
                sb.append(" ").append(words[i]);
            }
            return sb.toString();
        }
    }
}
