package leetcode.string;

public interface NumberOfLinesToWriteString {

    int[] numberOfLines(int[] widths, String S);

    class Solution implements NumberOfLinesToWriteString {

        @Override
        public int[] numberOfLines(int[] widths, String S) {
            int lineCount = 1, lineWidth = 0;
            for (char c : S.toCharArray()) {
                int width = widths[c - 'a'];
                if (lineWidth + width > 100) {
                    lineCount++;
                    lineWidth = width;
                } else
                    lineWidth += width;
            }
            return new int[]{lineCount, lineWidth};
        }
    }
}
