package leetcode.string;

import java.util.HashMap;
import java.util.Map;

public interface RansomNote {

    boolean canConstruct(String ransomNote, String magazine);

    class Solution implements RansomNote {

        @Override
        public boolean canConstruct(String ransomNote, String magazine) {
            Map<Character, Integer> ransomMap = occurrences(ransomNote);
            Map<Character, Integer> magazineMap = occurrences(magazine);
            for (Map.Entry<Character, Integer> entry : ransomMap.entrySet()) {
                Character c = entry.getKey();
                if (!magazineMap.containsKey(c) || magazineMap.get(c) < entry.getValue()) {
                    return false;
                }
            }
            return true;
        }

        private Map<Character, Integer> occurrences(String s) {
            Map<Character, Integer> occurrences = new HashMap<>();
            for (char c : s.toCharArray()) {
                occurrences.merge(c, 1, Integer::sum);
            }
            return occurrences;
        }
    }
}
