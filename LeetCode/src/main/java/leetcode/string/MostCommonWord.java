package leetcode.string;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface MostCommonWord {

    String mostCommonWord(String paragraph, String[] banned);

    class Solution implements MostCommonWord {

        @Override
        public String mostCommonWord(String paragraph, String[] banned) {
            Set<String> bannedSet = Stream.of(banned).collect(Collectors.toSet());
            List<String> words = Stream.of(paragraph.split("[ !?',;.]"))
                    .filter(word -> !word.isEmpty())
                    .map(String::toLowerCase)
                    .filter(word -> !bannedSet.contains(word))
                    .collect(Collectors.toList());
            return words.stream()
                    .distinct()
                    .collect(Collectors.toMap(
                            Function.identity(),
                            word -> (int) words.stream()
                                    .filter(w -> w.equals(word))
                                    .count()))
                    .entrySet().stream()
                    .max(Comparator.comparingInt(Map.Entry::getValue))
                    .get()
                    .getKey();
        }
    }
}
