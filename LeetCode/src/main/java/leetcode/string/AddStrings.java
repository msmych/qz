package leetcode.string;

public interface AddStrings {

  String addStrings(String num1, String num2);

  class Solution implements AddStrings {
    @Override
    public String addStrings(String num1, String num2) {
      StringBuilder sb = new StringBuilder();
      boolean remainder = false;
      int lastIndex = Math.max(num1.length(), num2.length());
      for (int i = 0; i < lastIndex; i++) {
        int n1 = num1.length() > i ? num1.charAt(num1.length() - i - 1) - '0' : 0;
        int n2 = num2.length() > i ? num2.charAt(num2.length() - i - 1) - '0' : 0;
        int sum = n1 + n2;
        if (remainder) {
          sum++;
        }
        remainder = sum >= 10;
        sb.insert(0, sum % 10);
      }
      if (remainder) {
        sb.insert(0, '1');
      }
      return sb.toString();
    }
  }
}
