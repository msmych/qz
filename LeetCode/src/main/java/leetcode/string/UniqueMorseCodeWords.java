package leetcode.string;

import java.util.HashSet;
import java.util.Set;

public interface UniqueMorseCodeWords {

    int uniqueMorseRepresentations(String[] words);

    class Solution implements UniqueMorseCodeWords {

        private final String[] morse = new String[]{
                ".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.",
                "--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};

        @Override
        public int uniqueMorseRepresentations(String[] words) {
            Set<String> transformations = new HashSet<>();
            for (String word : words) {
                StringBuilder sb = new StringBuilder();
                for (char c : word.toCharArray()) {
                    sb.append(morse[c - 'a']);
                }
                transformations.add(sb.toString());
            }
            return transformations.size();
        }
    }
}
