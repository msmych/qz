package leetcode.string;

public interface CountBinarySubstrings {

    int countBinarySubstrings(String s);

    class Solution implements CountBinarySubstrings {

        private String s;
        private String order = "";

        @Override
        public int countBinarySubstrings(String s) {
            this.s = s;
            int count = 0;
            for (int i = 0; i < s.length() - 1; i++) {
                order = "";
                int k = 0;
                while (isBorder(i, k++)) {
                    order = Character.toString(s.charAt(i)) + s.charAt(i + 1);
                    count++;
                }
            }
            return count;
        }

        private boolean isBorder(int i, int k) {
            int leftIndex = i - k, rightIndex = i + k + 1;
            if (leftIndex < 0 || rightIndex >= s.length()) return false;
            char left = s.charAt(leftIndex), right = s.charAt(rightIndex);
            return order.isEmpty() && left != right ||
                    order.equals("01") && left == '0' && right == '1' ||
                    order.equals("10") && left == '1' && right == '0';
        }
    }
}
