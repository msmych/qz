package leetcode.string;

import java.util.Arrays;
import java.util.stream.Collectors;

public interface CompareVersionNumbers {

    int compareVersion(String version1, String version2);

    class Solution implements CompareVersionNumbers {

        @Override
        public int compareVersion(String version1, String version2) {
            Integer[] revisions1 = getRevisions(version1);
            Integer[] revisions2 = getRevisions(version2);
            for (int i = 0; i < Math.min(revisions1.length, revisions2.length); i++) {
                int r1 = revisions1[i], r2 = revisions2[i];
                if (r1 != r2) return r1 > r2 ? 1 : -1;
            }
            if (revisions1.length > revisions2.length)
                return compareExtraRevisions(revisions1, revisions2);
            else if (revisions2.length > revisions1.length)
                return - compareExtraRevisions(revisions2, revisions1);
            return 0;
        }

        private Integer[] getRevisions(String version1) {
            return Arrays.stream(version1.split("\\."))
                    .map(Integer::valueOf)
                    .collect(Collectors.toList())
                    .toArray(new Integer[]{});
        }

        private int compareExtraRevisions(Integer[] revisions1, Integer[] revisions2) {
            return Arrays.stream(revisions1)
                    .skip(revisions2.length)
                    .anyMatch(r -> r > 0)
                    ? 1 : 0;
        }
    }
}
