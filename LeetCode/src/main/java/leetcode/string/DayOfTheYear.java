package leetcode.string;

public interface DayOfTheYear {

    int dayOfYear(String date);

    class Solution implements DayOfTheYear {
        @Override
        public int dayOfYear(String date) {
            int[] months = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
            String[] parts = date.split("-");
            int doy = 0;
            int year = Integer.parseInt(parts[0]);
            int month = Integer.parseInt(parts[1]);
            if (month > 2 && year % 4 == 0 && year != 1900) {
                doy++;
            }
            for (int i = 0; i < month - 1; i++) {
                doy += months[i];
            }
            doy += Integer.parseInt(parts[2]);
            return doy;
        }
    }
}
