package leetcode.string;

import java.util.regex.Pattern;

public interface ValidNumber {

    boolean isNumber(String s);

    class Solution implements ValidNumber {

        @Override
        public boolean isNumber(String s) {
            return Pattern.matches("[ ]*[\\+\\-]?(([0-9]+(\\.[0-9]*)?)|(\\.[0-9]+))(e[\\+\\-]?[0-9]+)?[ ]*", s);
        }
    }
}
