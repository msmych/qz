package leetcode.string;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public interface RotatedDigits {

    int rotatedDigits(int N);

    class Solution implements RotatedDigits {

        @Override
        public int rotatedDigits(int N) {
            List<Character> normal = Arrays.asList('2', '5', '6', '9'),
                    asymmetric = Arrays.asList('3', '4', '7');
            int goodCount = 0;
            for (; N > 0; N--) {
                String s = String.valueOf(N);
                if (IntStream.range(0, s.length())
                        .mapToObj(s::charAt)
                        .noneMatch(asymmetric::contains)
                        &&
                        IntStream.range(0, s.length())
                        .mapToObj(s::charAt)
                        .anyMatch(normal::contains))
                    goodCount++;
            }
            return goodCount;
        }
    }
}
