package leetcode.string;

import java.util.ArrayList;
import java.util.List;

public interface OccurrencesAfterBigram {

    String[] findOcurrences(String text, String first, String second);

    class Solution implements OccurrencesAfterBigram {

        @Override
        public String[] findOcurrences(String text, String first, String second) {
            List<String> occurrences = new ArrayList<>();
            int index = 0;
            for (String word : text.split(" ")) {
                switch (index) {
                    case 0: if (word.equals(first)) index++;
                    break;
                    case 1: index = word.equals(second) ? 2 : 0;
                    if (word.equals(first)) index = 1;
                    break;
                    case 2: occurrences.add(word);
                    index = word.equals(first) ? 1 : 0;
                }
            }
            return occurrences.toArray(new String[]{});
        }
    }
}
