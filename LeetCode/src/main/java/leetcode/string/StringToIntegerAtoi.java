package leetcode.string;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface StringToIntegerAtoi {

    int myAtoi(String str);

    class Solution implements StringToIntegerAtoi {

        @Override
        public int myAtoi(String str) {
            Pattern pattern = Pattern.compile("[ ]*[/+/-0-9]?[0-9]+.*");
            Matcher matcher = pattern.matcher(str);
            if (matcher.matches()) {
                String group = matcher.group().trim();
                int sign;
                if (group.charAt(0) == '-') {
                    sign = -1;
                    group = group.substring(1);
                } else {
                    sign = 1;
                    if (group.charAt(0) == '+') group = group.substring(1);
                }
                group = group.split("[^0-9]")[0];
                while (group.length() > 0 && group.charAt(0) == '0') {
                    group = group.substring(1);
                }
                if (group.length() > 11) group = group.substring(0, 11);
                if (group.length() == 0) group = "0";
                long number = sign * Long.valueOf(group);
                if (number > Integer.MAX_VALUE) number = Integer.MAX_VALUE;
                if (number < Integer.MIN_VALUE) number = Integer.MIN_VALUE;
                return (int) number;
            }
            return 0;
        }
    }
}
