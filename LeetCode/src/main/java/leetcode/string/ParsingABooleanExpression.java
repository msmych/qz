package leetcode.string;

import java.util.HashSet;
import java.util.Set;

public interface ParsingABooleanExpression {

    boolean parseBoolExpr(String expression);

    class Solution implements ParsingABooleanExpression {

        @Override
        public boolean parseBoolExpr(String expression) {
            if (expression.equals("t")) return true;
            if (expression.equals("f")) return false;
            switch (expression.charAt(0)) {
                case '!': return !parseBoolExpr(arguments(expression));
                case '&': return argSet(expression).stream().allMatch(this::parseBoolExpr);
                case '|': return argSet(expression).stream().anyMatch(this::parseBoolExpr);
                default: throw new IllegalArgumentException();
            }
        }

        private String arguments(String expression) {
            return expression.substring(2, expression.length() - 1);
        }

        private Set<String> argSet(String expression) {
            String arguments = arguments(expression);
            int left = 0;
            int parenthesis = 0;
            Set<String> set = new HashSet<>();
            for (int i = 0; i < arguments.length(); i++) {
                switch (arguments.charAt(i)) {
                    case ',':
                        if (parenthesis == 0) {
                            set.add(arguments.substring(left, i));
                            left = i + 1;
                        }
                        break;
                    case '(':
                        parenthesis++;
                        break;
                    case ')':
                        parenthesis--;
                }
            }
            if (left < arguments.length()) set.add(arguments.substring(left));
            return set;
        }
    }
}
