package leetcode.string;

import java.util.Arrays;
import java.util.List;

public interface GoatLatin {

    String toGoatLatin(String S);

    class Solution implements GoatLatin {

        private final List<Character> VOWELS = Arrays.asList('a', 'e', 'i', 'o', 'u');

        @Override
        public String toGoatLatin(String S) {
            StringBuilder sb = new StringBuilder();
            String[] words = S.split(" ");
            for (int i = 0; i < words.length; i++) {
                String word = words[i];
                char first = word.charAt(0);
                if (VOWELS.contains(Character.toLowerCase(first)))
                    sb.append(word);
                else
                    sb.append(word.substring(1))
                            .append(first);
                sb.append("ma");
                for (int a = 0; a <= i; a++)
                    sb.append('a');
                if (i < words.length - 1)
                    sb.append(' ');
            }
            return sb.toString();
        }
    }
}
