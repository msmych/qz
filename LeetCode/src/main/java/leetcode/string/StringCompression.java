package leetcode.string;

public interface StringCompression {

  int compress(char[] chars);

  class Solution implements StringCompression {
    @Override
    public int compress(char[] chars) {
      if (chars.length == 0) {
        return 0;
      }
      int lastIndex = 0;
      int writingIndex = 0;
      int length = 1;
      for (int i = 1; i < chars.length; i++) {
        if (chars[lastIndex] == chars[i]) {
          length++;
        } else {
          writingIndex = compressSequence(chars, lastIndex, length, writingIndex);
          lastIndex += length;
          if (lastIndex < chars.length) {
            length = 1;
          }
        }
      }
      return compressSequence(chars, lastIndex, length, writingIndex);
    }

    private int compressSequence(char[] chars, int lastIndex, int length, int writingIndex) {
      chars[writingIndex] = chars[lastIndex];
      if (length > 1) {
        for (char c : String.valueOf(length).toCharArray()) {
          chars[++writingIndex] = c;
        }
      }
      return writingIndex + 1;
    }
  }
}
