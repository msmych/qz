package leetcode.string;

public interface ZigZagConversion {

    String convert(String s, int numRows);

    class Solution implements ZigZagConversion {
        @Override
        public String convert(String s, int numRows) {
            int numCols = getNumCols(s.length(), numRows);
            String[][] matrix = new String[numCols][numRows];
            int r = 0;
            int c = 0;
            boolean direction = true;
            while (s.length() > 0) {
                matrix[c][r] = s.substring(0, 1);
                s = s.substring(1);
                if (direction) {
                    if (r < numRows - 1) {
                        r++;
                    } else {
                        direction = false;
                        if (numRows > 1) {
                            r--;
                        }
                        c++;
                    }
                } else {
                    if (r > 0) {
                        r--;
                        c++;
                    } else {
                        direction = true;
                        if (numRows > 1) {
                            r++;
                        } else {
                            c++;
                        }
                    }
                }
            }
            StringBuilder zigzag = new StringBuilder();
            for (int i = 0; i < numRows; i++) {
                for (int j = 0; j < numCols; j++) {
                    String letter = matrix[j][i];
                    if (letter != null) {
                        zigzag.append(letter);
                    }
                }
            }
            return zigzag.toString();
        }

        private int getNumCols(int length, int numRows) {
            int vLength = 2*numRows - 2;
            int vWidth = numRows - 1;
            if (vLength == 0 || vWidth == 0) {
                return length;
            }
            int colRows = length / vLength * vWidth;
            int reminder = length % vLength;
            if (reminder < numRows) {
                colRows++;
            } else {
                reminder = reminder - numRows;
                colRows = colRows + reminder + 1;
            }
            return colRows;
        }
    }
}
