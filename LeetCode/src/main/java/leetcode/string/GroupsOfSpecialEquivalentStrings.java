package leetcode.string;

import java.util.*;

public interface GroupsOfSpecialEquivalentStrings {

    int numSpecialEquivGroups(String[] A);

    class Solution implements GroupsOfSpecialEquivalentStrings {

        private class CharWithParity {
            char c;
            boolean isEven;

            CharWithParity(char c, boolean isEven) {
                this.c = c;
                this.isEven = isEven;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                CharWithParity that = (CharWithParity) o;
                return c == that.c &&
                        isEven == that.isEven;
            }

            @Override
            public int hashCode() {
                return Objects.hash(c, isEven);
            }
        }

        @Override
        public int numSpecialEquivGroups(String[] A) {
            Set<Map<CharWithParity, Integer>> groups = new HashSet<>();
            int num = 0;
            for (String a : A) {
                Map<CharWithParity, Integer> group = new HashMap<>();
                for (int i = 0; i < a.length(); i++) {
                    CharWithParity charWithParity = new CharWithParity(a.charAt(i), i % 2 == 0);
                    group.put(charWithParity, group.getOrDefault(charWithParity, 0) + 1);
                }
                if (!groups.contains(group)) {
                    num++;
                    groups.add(group);
                }
            }
            return num;
        }
    }
}
