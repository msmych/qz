package leetcode.string;

public interface LongestUncommonSubsequenceI {

    int findLUSlength(String a, String b);

    class Solution implements LongestUncommonSubsequenceI {

        @Override
        public int findLUSlength(String a, String b) {
            if (a.length() != b.length()) {
                return Math.max(a.length(), b.length());
            }
            for (int i = a.length(); i > 0; i--) {
                for (int j = 0; j <= a.length() - i; j++) {
                    if (!b.contains(a.substring(j, j + i)) || !a.contains(b.substring(j, j + i))) {
                        return i - j;
                    }
                }
            }
            return -1;
        }
    }
}
