package leetcode.string;

public interface DetectCapital {

    boolean detectCapitalUse(String word);

    class Solution implements DetectCapital {
        @Override
        public boolean detectCapitalUse(String word) {
            return word.equals(word.toUpperCase()) ||
                    word.equals(word.toLowerCase()) ||
                    word.equals(word.substring(0, 1).toUpperCase() + word.toLowerCase().substring(1));
        }
    }
}
