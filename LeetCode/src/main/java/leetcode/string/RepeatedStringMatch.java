package leetcode.string;

import java.util.HashSet;
import java.util.Set;

public interface RepeatedStringMatch {

    int repeatedStringMatch(String A, String B);

    class Solution implements RepeatedStringMatch {

        @Override
        public int repeatedStringMatch(String A, String B) {
            StringBuilder sb = new StringBuilder(A);
            Set<Character> bset = new HashSet<>();
            for (char c : B.toCharArray()) {
                bset.add(c);
            }
            for (char c : bset) {
                if (!A.contains(Character.toString(c))) return -1;
            }
            for (int i = 1; i <= 2 || sb.length() < 3 * B.length(); i++) {
                if (sb.toString().contains(B)) return i;
                sb.append(A);
            }
            return -1;
        }
    }
}
