package leetcode.string;

public interface GreatestCommonDivisorOfStrings {

    String gcdOfStrings(String str1, String str2);

    class Solution implements GreatestCommonDivisorOfStrings {

        @Override
        public String gcdOfStrings(String str1, String str2) {
            if (str1.equals(str2)) return str1;
            return str1.length() > str2.length()
                    ? gcd(str1, str2) : gcd(str2, str1);
        }

        private String gcd(String big, String small) {
            for (int i = 1; i <= small.length(); i++) {
                if (small.length() % i > 0) continue;
                String b = big;
                String s = small;
                String gcd = small.substring(0, small.length() / i);
                while (!b.isEmpty()) {
                    if (!(s.startsWith(gcd) || s.isEmpty()) || !b.startsWith(gcd)) break;
                    if (!s.isEmpty()) s = s.substring(gcd.length());
                    b = b.substring(gcd.length());
                }
                if (b.isEmpty()) return gcd;
            }
            return "";
        }
    }
}
