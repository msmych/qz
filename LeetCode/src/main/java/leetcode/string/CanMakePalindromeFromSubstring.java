package leetcode.string;

import java.util.*;

public interface CanMakePalindromeFromSubstring {

  List<Boolean> canMakePaliQueries(String s, int[][] queries);

  class Solution implements CanMakePalindromeFromSubstring {

    private static class Query {
      int from, to, k;

      Query(int[] query) {
        from = query[0];
        to = query[1];
        k = query[2];
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Query query = (Query) o;
        return from == query.from &&
          to == query.to &&
          k == query.k;
      }

      @Override
      public int hashCode() {
        return Objects.hash(from, to, k);
      }
    }

    private final Map<Query, Boolean> cache = new HashMap<>();
    private final Map<Integer, Map<Character, Integer>> frequencies = new HashMap<>();

    @Override
    public List<Boolean> canMakePaliQueries(String s, int[][] queries) {
      Map<Character, Integer> frequency = new HashMap<>();
      for (int i = 0; i < s.length(); i++) {
        char c = s.charAt(i);
        Map<Character, Integer> map = new HashMap<>(frequency);
        map.merge(c, 1, Integer::sum);
        frequencies.put(i, map);
        frequency = map;
      }
      List<Boolean> list = new ArrayList<>();
      for (int[] query : queries) {
        list.add(canMakePalindrome(new Query(query)));
      }
      return list;
    }

    private boolean canMakePalindrome(Query query) {
      if (cache.containsKey(query)) {
        return cache.get(query);
      }
      long odds = (query.from == 0
        ? frequencies.get(query.to)
        : subract(frequencies.get(query.to), frequencies.get(query.from - 1)))
        .values()
        .stream()
        .filter(n -> n % 2 == 1)
        .count();
      boolean canMake = odds / 2 <= query.k;
      cache.put(query, canMake);
      return canMake;
    }

    private Map<Character, Integer> subract(Map<Character, Integer> mapa, Map<Character, Integer> mapb) {
      Map<Character, Integer> map = new HashMap<>(mapa);
      for (Map.Entry<Character, Integer> entry : mapb.entrySet()) {
        map.merge(entry.getKey(), entry.getValue(), (a, b) -> a - b);
      }
      return map;
    }
  }
}
