package leetcode.string;

public interface CountAndSay {

    String countAndSay(int n);

    class Solution implements CountAndSay {

        @Override
        public String countAndSay(int n) {
            String s = "1";
            for (; n > 1; n--)
                s = spell(s);
            return s;
        }

        private String spell(String s) {
            StringBuilder sb = new StringBuilder();
            while (!s.isEmpty()) {
                int count = 0;
                char c = s.charAt(0);
                while (!s.isEmpty() && s.charAt(0) == c) {
                    count++;
                    s = s.substring(1);
                }
                sb.append(count).append(c);
            }
            return sb.toString();
        }
    }
}
