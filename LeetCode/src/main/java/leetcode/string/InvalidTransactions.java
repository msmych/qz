package leetcode.string;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface InvalidTransactions {

    List<String> invalidTransactions(String[] transactions);

    class Solution implements InvalidTransactions {

        private static class Transaction {
            String name;
            int time;
            int amount;
            String city;
            boolean valid;

            Transaction(String transaction) {
                String[] parts = transaction.split(",");
                this.name = parts[0];
                this.time = Integer.valueOf(parts[1]);
                this.amount = Integer.valueOf(parts[2]);
                this.city = parts[3];
                this.valid = this.amount <= 1000;
            }

            String csv() {
                return String.join(",", name, String.valueOf(time), String.valueOf(amount), city);
            }
        }

        @Override
        public List<String> invalidTransactions(String[] transactions) {
            Map<String, List<Transaction>> txsByName = Arrays.stream(transactions)
                .map(Transaction::new)
                .collect(Collectors.groupingBy(t -> t.name));
            for (List<Transaction> txByName : txsByName.values()) {
                for (int i = 0; i < txByName.size() - 1; i++) {
                    for (int j = 1; j < txByName.size(); j++) {
                        if (Math.abs(txByName.get(j).time - txByName.get(i).time) <= 60
                            && !txByName.get(i).city.equals(txByName.get(j).city)) {
                            txByName.get(i).valid = false;
                            txByName.get(j).valid = false;
                        }
                    }
                }
            }
            return txsByName.values().stream()
                .flatMap(Collection::stream)
                .filter( t -> !t.valid)
                .map(Transaction::csv)
                .collect(Collectors.toList());
        }
    }
}
