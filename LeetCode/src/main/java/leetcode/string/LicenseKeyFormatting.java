package leetcode.string;

public interface LicenseKeyFormatting {

    String licenseKeyFormatting(String S, int K);

    class Solution implements LicenseKeyFormatting {

        @Override
        public String licenseKeyFormatting(String S, int K) {
            String s = String.join("", S.split("-"));
            StringBuilder sb = new StringBuilder();
            while (s.length() >= K) {
                if (sb.length() > 0) {
                    sb.insert(0, '-');
                }
                sb.insert(0, s.substring(s.length() - K).toUpperCase());
                s = s.substring(0, s.length() - K);
            }
            if (sb.length() > 0 && s.length() > 0) {
                sb.insert(0, '-');
            }
            sb.insert(0, s.toUpperCase());
            return sb.toString();
        }
    }

}
