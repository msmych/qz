package leetcode.string;

public interface ValidPalindromeII {

    boolean validPalindrome(String s);

    class Solution implements ValidPalindromeII {

        private boolean removed = false;

        @Override
        public boolean validPalindrome(String s) {
            for (int i = 0; i < s.length() / 2; i++) {
                if (s.charAt(i) != s.charAt(s.length() - i - 1)) {
                    if (!removed) {
                        removed = true;
                        return validPalindrome(s.substring(i + 1, s.length() - i))
                                || validPalindrome(s.substring(i, s.length() - i - 1));
                    } else
                        return false;
                }
            }
            return true;
        }
    }
}
