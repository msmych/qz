package leetcode.math;

public interface ConstructTheRectangle {

    int[] constructRectangle(int area);

    class Solution implements ConstructTheRectangle {

        @Override
        public int[] constructRectangle(int area) {
            for (int i = (int) Math.sqrt(area); i > 0; i--) {
                if (area % i == 0) {
                    return new int[]{area / i, i};
                }
            }
            throw new IllegalArgumentException();
        }
    }

}
