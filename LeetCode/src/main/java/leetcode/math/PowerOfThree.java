package leetcode.math;

public interface PowerOfThree {

    boolean isPowerOfThree(int n);

    class Solution implements PowerOfThree {

        @Override
        public boolean isPowerOfThree(int n) {
            if (n < 1) return false;
            for (; n > 1; n /= 3) {
                if (n % 3 != 0) return false;
            }
            return true;
        }
    }
}
