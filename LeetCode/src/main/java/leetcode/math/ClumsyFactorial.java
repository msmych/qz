package leetcode.math;

import java.util.ArrayList;
import java.util.List;

public interface ClumsyFactorial {

    int clumsy(int N);

    class Solution implements ClumsyFactorial {

        @Override
        public int clumsy(int N) {
            List<Integer> dividends = new ArrayList<>();
            List<Integer> multipliers = new ArrayList<>();
            List<Integer> divisors = new ArrayList<>();
            List<Integer> addends = new ArrayList<>();
            for (int i = 0; i < N; i++) {
                switch (i % 4) {
                    case 0:
                        dividends.add(N - i);
                        break;
                    case 1:
                        multipliers.add(N - i);
                        break;
                    case 2:
                        divisors.add(N - i);
                        break;
                    case 3:
                        addends.add(N - i);
                }
            }
            int factorial = 0;
            for (int i = 0; i < dividends.size(); i++) {
                int n = dividends.get(i);
                if (multipliers.size() > i) n *= multipliers.get(i);
                if (divisors.size() > i) n /= divisors.get(i);
                factorial += i == 0 ? n : -n;
                if (addends.size() > i) factorial += addends.get(i);
            }
            return factorial;
        }
    }
}
