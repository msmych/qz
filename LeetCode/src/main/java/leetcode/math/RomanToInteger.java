package leetcode.math;

public interface RomanToInteger {

    int romanToInt(String s);

    class Solution implements RomanToInteger {

        enum Roman {
            I(1),
            V(5),
            X(10),
            L(50),
            C(100),
            D(500),
            M(1000);

            int value;

            Roman(int value) {
                this.value = value;
            }
        }

        enum UncommonRoman {
            IV(4),
            IX(9),
            XL(40),
            XC(90),
            CD(400),
            CM(900);

            int value;

            UncommonRoman(int value) {
                this.value = value;
            }
        }

        @Override
        public int romanToInt(String s) {
            int result = 0;
            UncommonRoman[] uncommonRomans = UncommonRoman.values();
            for (int i = 0; i < uncommonRomans.length; i++) {
                UncommonRoman uncommonRoman = uncommonRomans[i];
                if (s.contains(uncommonRoman.name())) {
                    s = s.replace(uncommonRoman.name(), "");
                    result = result + uncommonRoman.value;
                }
            }
            for (int i = 0; i < s.length(); i++) {
                Roman roman = Roman.valueOf(String.valueOf(s.charAt(i)));
                int value = roman.value;
                result = result + value;
            }

            return result;
        }
    }
}
