package leetcode.math;

public interface ComplementOfBase10Integer {

    int bitwiseComplement(int N);

    class Solution implements ComplementOfBase10Integer {

        @Override
        public int bitwiseComplement(int N) {
            if (N == 0)
                return 1;
            int complement = 0;
            for (int bit = 1; bit <= N; bit *= 2) {
                if ((N & bit) == 0)
                    complement += bit;
            }
            return complement;
        }
    }
}
