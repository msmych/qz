package leetcode.math;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public interface XOfAKindInADeckOfCards {

    boolean hasGroupsSizeX(int[] deck);

    class Solution implements XOfAKindInADeckOfCards {

        @Override
        public boolean hasGroupsSizeX(int[] deck) {
            Map<Integer, Integer> map = new HashMap<>();
            for (int card : deck)
                map.put(card, map.getOrDefault(card, 0) + 1);
            int min = map.values().stream().min(Comparator.naturalOrder()).get();
            for (int i = 2; i <= min; i++) {
                int divider = i;
                if (map.values().stream().allMatch(v -> v % divider == 0))
                    return true;
            }
            return false;
        }
    }
}
