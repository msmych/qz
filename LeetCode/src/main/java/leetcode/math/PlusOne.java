package leetcode.math;

import java.util.ArrayList;
import java.util.List;

public interface PlusOne {

    int[] plusOne(int[] digits);

    class Solution implements PlusOne {

        @Override
        public int[] plusOne(int[] digits) {
            List<Integer> list = getList(digits);
            increment(list);
            return getArray(list);
        }

        private List<Integer> getList(int[] digits) {
            List<Integer> list = new ArrayList<>();
            for (int digit : digits) {
                list.add(digit);
            }
            return list;
        }

        private void increment(List<Integer> list) {
            boolean shift;
            int i = list.size() - 1;
            do {
                int digit = list.get(i) + 1;
                shift = digit == 10;
                list.set(i, digit % 10);
            } while (--i >= 0 && shift);
            if (shift)
                list.add(0, 1);
        }

        private int[] getArray(List<Integer> digits) {
            int[] array = new int[digits.size()];
            for (int i = 0; i < array.length; i++) {
                array[i] = digits.get(i);
            }
            return array;
        }
    }
}
