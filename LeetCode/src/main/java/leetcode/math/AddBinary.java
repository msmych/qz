package leetcode.math;

import java.util.ArrayList;
import java.util.List;

public interface AddBinary {

    String addBinary(String a, String b);

    class Solution implements AddBinary {

        @Override
        public String addBinary(String a, String b) {
            List<Boolean> aBools = toBoolArray(a);
            List<Boolean> bBools = toBoolArray(b);
            List<Boolean> sumBools = add(aBools, bBools);
            return boolsToString(sumBools);
        }

        private List<Boolean> toBoolArray(String s) {
            List<Boolean> bools = new ArrayList<>();
            for (int i = s.length() - 1; i >= 0; i--) {
                bools.add(s.charAt(i) == '1');
            }
            return bools;
        }

        private List<Boolean> add(List<Boolean> aBools, List<Boolean> bBools) {
            List<Boolean> sumBools = new ArrayList<>();
            boolean shift = false;
            for (int i = 0; i < aBools.size() || i < bBools.size() || shift; i++) {
                boolean a = i < aBools.size() ? aBools.get(i) : false;
                boolean b = i < bBools.size() ? bBools.get(i) : false;
                boolean sum = a != b;
                if (shift) sum = !sum;
                sumBools.add(sum);
                shift = (a && b) || ((a || b) && shift);
            }
            return sumBools;
        }

        private String boolsToString(List<Boolean> bools) {
            StringBuilder sb = new StringBuilder();
            for (int i = bools.size() - 1; i >= 0; i--) {
                sb.append(bools.get(i) ? '1' : '0');
            }
            return sb.toString();
        }
    }
}
