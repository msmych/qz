package leetcode.math;

public interface SmallestRangeI {

    int smallestRangeI(int[] A, int K);

    class Solution implements SmallestRangeI {

        @Override
        public int smallestRangeI(int[] A, int K) {
            int min = A[0], max = A[0];
            for (int i = 1; i < A.length; i++) {
                if (A[i] < min)
                    min = A[i];
                if (A[i] > max)
                    max = A[i];
            }
            int delta = max - min;
            return delta < 2*K
                    ? 0
                    : delta - 2*K;
        }
    }
}
