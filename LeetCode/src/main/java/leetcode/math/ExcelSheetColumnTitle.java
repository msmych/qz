package leetcode.math;

public interface ExcelSheetColumnTitle {

    String convertToTitle(int n);

    class Solution implements ExcelSheetColumnTitle {

        @Override
        public String convertToTitle(int n) {
            StringBuilder col = new StringBuilder();
            int range = 'Z' - 'A' + 1;
            while (n > 0) {
                col.append(Character.valueOf((char) ('A' + ((n - 1) % range))));
                if (n / range == 1 && n % range == 0) break;
                n = (n - 1) / range;
            }
            return col.reverse().toString();
        }
    }
}
