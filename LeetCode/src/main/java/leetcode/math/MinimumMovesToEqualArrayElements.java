package leetcode.math;

import java.util.Arrays;

public interface MinimumMovesToEqualArrayElements {

  int minMoves(int[] nums);

  class Solution implements MinimumMovesToEqualArrayElements {

    @Override
    public int minMoves(int[] nums) {
      if (nums.length < 2) {
        return 0;
      }
      int moves = 0;
      while (Arrays.stream(nums).min().getAsInt() != Arrays.stream(nums).max().getAsInt()) {
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 1; i++) {
          nums[i]++;
        }
        moves++;
      }
      return moves;
    }
  }
}
