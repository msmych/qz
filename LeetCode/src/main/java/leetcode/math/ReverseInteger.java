package leetcode.math;

public interface ReverseInteger {

    int reverse(int x);

    class Solution implements ReverseInteger {

        @Override
        public int reverse(int x) {
            int reversed = 0, MAX = Integer.MAX_VALUE, MIN = Integer.MIN_VALUE;
            while (x != 0) {
                int next = x % 10;
                x /= 10;
                if (reversed > MAX/10 || (reversed == MAX/10 && next > 7))
                    return 0;
                if (reversed < MIN/10 || (reversed == MIN/10 && next < -8))
                    return 0;
                reversed *= 10;
                reversed += next;
            }
            return reversed;
        }
    }
}
