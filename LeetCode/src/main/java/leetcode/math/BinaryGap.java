package leetcode.math;

public interface BinaryGap {

    int binaryGap(int N);

    class Solution implements BinaryGap {

        @Override
        public int binaryGap(int N) {
            int max = 0, distance = 0, power = 1;
            while (N >= power) {
                if ((N & power) > 0) {
                    if (distance == 0)
                        distance++;
                    else {
                        if (distance > max)
                            max = distance;
                        distance = 1;
                    }
                } else if (distance > 0)
                    distance++;
                power *= 2;
            }
            return max;
        }
    }
}
