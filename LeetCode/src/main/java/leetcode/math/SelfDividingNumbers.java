package leetcode.math;

import java.util.ArrayList;
import java.util.List;

public interface SelfDividingNumbers {

    List<Integer> selfDividingNumbers(int left, int right);

    class Solution implements SelfDividingNumbers {

        @Override
        public List<Integer> selfDividingNumbers(int left, int right) {
            List<Integer> numbers = new ArrayList<>();
            for (int i = left; i <= right; i++) {
                int n = i;
                boolean dividing = true;
                while (n > 0) {
                    int d = n % 10;
                    if (d == 0 || i % d != 0) {
                        dividing = false;
                        break;
                    }
                    n /= 10;
                }
                if (dividing)
                    numbers.add(i);
            }
            return numbers;
        }
    }
}
