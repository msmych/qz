package leetcode.math;

import java.util.ArrayList;
import java.util.List;

public interface CircularPermutationInBinaryRepresentation {

  List<Integer> circularPermutation(int n, int start);

  class Solution implements CircularPermutationInBinaryRepresentation {
    @Override
    public List<Integer> circularPermutation(int n, int start) {
      List<Integer> list = new ArrayList<>();
      for (int i = 0; i < Math.pow(2, n); i++) {
        list.add((i >> 1) ^ i);
      }
      int startIndex = list.indexOf(start);
      list.addAll(list.subList(0, startIndex));
      return list.subList(startIndex, list.size());
    }
  }
}
