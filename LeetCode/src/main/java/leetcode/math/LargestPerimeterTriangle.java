package leetcode.math;

import java.util.Arrays;

public interface LargestPerimeterTriangle {

    int largestPerimeter(int[] A);

    class Solution implements LargestPerimeterTriangle {

        @Override
        public int largestPerimeter(int[] A) {
            Arrays.sort(A);
            for (int i = A.length - 1; i >= 2; i--) {
                int a = A[i - 2], b = A[i - 1], c = A[i];
                if (a + b > c)
                    return a + b + c;
            }
            return 0;
        }
    }
}
