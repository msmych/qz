package leetcode.math;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public interface LargestTimeForGivenDigits {

    String largestTimeFromDigits(int[] A);

    class Solution implements LargestTimeForGivenDigits {

        @Override
        public String largestTimeFromDigits(int[] A) {
            Map<Integer, String> times = new HashMap<>();
            for (int h1 = 0; h1 < A.length; h1++) {
                for (int h2 = 0; h2 < A.length; h2++) {
                    if (h1 == h2)
                        continue;
                    for (int m1 = 0; m1 < A.length; m1++) {
                        if (h1 == m1 || h2 == m1)
                            continue;
                        for (int m2 = 0; m2 < A.length; m2++) {
                            if (h1 == m2 || h2 == m2 || m1 == m2)
                                continue;
                            if (A[h1] > 2)
                                continue;
                            if (A[h1] == 2 && A[h2] > 3)
                                continue;
                            if (A[m1] > 5)
                                continue;
                            times.put(A[h1] * 1000 + A[h2] * 100 + A[m1] * 10 + A[m2],
                                    String.valueOf(A[h1]) + A[h2] + ":" + A[m1] + A[m2]);
                        }
                    }
                }
            }
            return times.isEmpty() ? ""
                    : times.entrySet().stream().max(Comparator.comparingInt(Map.Entry::getKey)).get().getValue();
        }
    }
}
