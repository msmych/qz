package leetcode.math;

public interface FactorialTrailingZeroes {

    int trailingZeroes(int n);

    class Solution implements FactorialTrailingZeroes {

        @Override
        public int trailingZeroes(int n) {
            int zeroes = 0;
            for (long i = 5; i <= n; i *= 5) {
                zeroes += n / i;
            }
            return zeroes;
        }
    }
}
