package leetcode.math;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.abs;

public interface FractionToRecurringDecimal {

  String fractionToDecimal(int numerator, int denominator);

  class Solution implements FractionToRecurringDecimal {

    @Override
    public String fractionToDecimal(int numerator, int denominator) {
      if (numerator == 0) {
        return "0";
      }
      boolean isPositive = numerator > 0 == denominator > 0;
      long num = abs((long) numerator);
      long den = abs((long) denominator);
      Map<Long, Integer> remainders = new HashMap<>();
      long quot = num / den;
      StringBuilder sb = new StringBuilder();
      sb.append(quot);
      num %= den;
      if (num != 0) {
        sb.append('.');
      }
      int index = 0;
      int indexAfterPeriod = sb.indexOf(".") + 1;
      while (num != 0) {
        num *= 10;
        quot = abs(num / den);
        if (remainders.containsKey(num)) {
          sb.insert(indexAfterPeriod + remainders.get(num), '(')
            .append(')');
          break;
        } else {
          sb.append(quot);
          remainders.put(num, index++);
        }
        num %= den;
      }
      if (!isPositive) {
        sb.insert(0, '-');
      }
      return sb.toString();
    }
  }
}
