package leetcode.math;

public interface DIStringMatch {

    int[] diStringMatch(String S);

    class Solution implements DIStringMatch {

        @Override
        public int[] diStringMatch(String S) {
            int lowerBound = 0, upperBound = S.length();
            int[] arr = new int[S.length() + 1];
            arr[0] = S.charAt(0) == 'I' ? lowerBound++ : upperBound--;
            for (int i = 0; i < S.length() - 1; i++) {
                switch (S.substring(i, i + 2)) {
                    case "II":
                    case "DI":
                        arr[i + 1] = lowerBound++;
                        break;
                    case "DD":
                    case "ID":
                        arr[i + 1] = upperBound--;
                }
            }
            arr[S.length()] = lowerBound;
            return arr;
        }
    }
}
