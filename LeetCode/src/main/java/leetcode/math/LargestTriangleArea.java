package leetcode.math;

import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public interface LargestTriangleArea {

    double largestTriangleArea(int[][] points);

    class Solution implements LargestTriangleArea {

        private int xMin, xMax, yMin, yMax;

        @Override
        public double largestTriangleArea(int[][] points) {
            double largestArea = 0;
            for (int i = 0; i < points.length; i++) {
                for (int j = i + 1; j < points.length; j++) {
                    for (int k = j + 1; k < points.length; k++) {
                        IntSummaryStatistics xStats =
                                IntStream.of(points[i][0], points[j][0], points[k][0]).summaryStatistics(),
                                yStats = IntStream.of(points[i][1], points[j][1], points[k][1]).summaryStatistics();
                        xMin = xStats.getMin();
                        xMax = xStats.getMax();
                        yMin = yStats.getMin();
                        yMax = yStats.getMax();
                        double area = (xMax - xMin) * (yMax - yMin)
                                - triangleArea(points[i], points[j])
                                - triangleArea(points[i], points[k])
                                - triangleArea(points[j], points[k])
                                - extraSquare(points[i], points[j], points[k]);
                        if (area > largestArea)
                            largestArea = area;
                    }
                }
            }
            return largestArea;
        }

        private double triangleArea(int[] p1, int[] p2) {
            return (Math.abs(p2[0] - p1[0]) * Math.abs(p2[1] - p1[1])) / 2.0;
        }

        private int extraSquare(int[] p1, int[] p2, int[] p3) {
            return Stream.of(
                    getHatExtra(p1, p2), getHatExtra(p1, p3),
                    getHatExtra(p2, p1), getHatExtra(p2, p3),
                    getHatExtra(p3, p1), getHatExtra(p3, p2))
                    .max(Comparator.naturalOrder())
                    .get();
        }

        private int getHatExtra(int[] pLeft, int[] p) {
            if (p[0] <= xMin || p[0] >= xMax || p[1] <= yMin || p[1] >= yMax)
                return 0;
            if (pLeft[0] == xMin && pLeft[1] == yMin) {
                return (p[1] - yMin) / ((double) (p[0] - xMin)) > (yMax - yMin) / ((double) (xMax - xMin))
                        ? (p[0] - xMin) * (yMax - p[1])
                        : (xMax - p[0]) * (p[1] - yMin);
            } else if (pLeft[0] == xMin && pLeft[1] == yMax) {
                return (p[0] - xMin) / ((double) (yMax - p[1])) > (xMax - xMin) / ((double) (yMax - yMin))
                        ? (xMax - p[0]) * (yMax - p[1])
                        : (p[0] - xMin) * (p[1] - yMin);
            }
            return 0;
        }
    }
}
