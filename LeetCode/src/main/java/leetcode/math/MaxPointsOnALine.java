package leetcode.math;

public interface MaxPointsOnALine {

    int maxPoints(int[][] points);

    class Solution implements MaxPointsOnALine {

        @Override
        public int maxPoints(int[][] points) {
            return 0;
        }
    }
}
