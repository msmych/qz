package leetcode.math;

public interface UglyNumber {

  boolean isUgly(int num);

  class Solution implements UglyNumber {
    @Override
    public boolean isUgly(int num) {
      if (num < 1) {
        return false;
      }
      num = getRidOfFactor(num, 2);
      num = getRidOfFactor(num, 3);
      num = getRidOfFactor(num, 5);
      return num == 1;
    }

    private int getRidOfFactor(int num, int factor) {
      for (; num % factor == 0;) {
        num /= factor;
      }
      return num;
    }
  }
}
