package leetcode.math;

public interface PalindromeNumber {

    boolean isPalindrome(int x);

    class Solution implements PalindromeNumber {

        @Override
        public boolean isPalindrome(int x) {
            int newNumber = 0;
            int div10 = x;
            if (x < 0) {
                return false;
            } else {
                do {
                    int divNoReminder10 = div10 % 10;
                    div10 /= 10;
                    newNumber = newNumber * 10 + divNoReminder10;
                } while (div10 != 0);
            }
            return x == newNumber;
        }
    }
}
