package leetcode.math;

public interface LetterTilePossibilities {

    int numTilePossibilities(String tiles);

    class Solution implements LetterTilePossibilities {

        @Override
        public int numTilePossibilities(String tiles) {
            return 0;
        }
    }
}
