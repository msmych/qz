package leetcode.math;

import java.util.stream.IntStream;

import static java.util.stream.IntStream.range;

public interface SubtractTheProductAndSumOfDigitsOfAnInteger {

  int subtractProductAndSum(int n);

  class Solution implements SubtractTheProductAndSumOfDigitsOfAnInteger {
    @Override
    public int subtractProductAndSum(int n) {
      String s = Integer.toString(n);
      return digitStream(s).reduce((a, b) -> a * b).getAsInt() - digitStream(s).sum();
    }

    private IntStream digitStream(String s) {
      return range(0, s.length()).map(s::charAt).map(i -> i - '0');
    }
  }
}
