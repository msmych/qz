package leetcode.math;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public interface PrimeArrangements {

  int numPrimeArrangements(int n);

  class Solution implements PrimeArrangements {
    @Override
    public int numPrimeArrangements(int n) {
      int primes = countPrimes(n);
      return (int) (factorial(primes) * factorial(n - primes) % (1_000_000_000 + 7));
    }

    private int countPrimes(int n) {
      Set<Integer> nums = IntStream.rangeClosed(2, n).boxed().collect(Collectors.toSet());
      for (int i = 2; i <= Math.sqrt(n); i++) {
        if (!nums.contains(i)) continue;
        for (int j = 2; j * i <= n; j++) {
          nums.remove(j * i);
        }
      }
      return nums.size();
    }

    private long factorial(int n) {
      return LongStream.rangeClosed(1, n)
        .reduce((a, b) -> a * b % (1_000_000_000 + 7))
        .orElse(1);
    }
  }
}
