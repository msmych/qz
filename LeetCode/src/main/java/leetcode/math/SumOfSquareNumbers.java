package leetcode.math;

public interface SumOfSquareNumbers {

    boolean judgeSquareSum(int c);

    class Solution implements SumOfSquareNumbers {

        @Override
        public boolean judgeSquareSum(int c) {
            System.out.println(c);
            for (int i = (int) Math.sqrt(c); i * i >= 0; i--) {
                int j = (int) Math.sqrt(c - i * i);
                if (i * i + j * j == c) return true;
            }
            return false;
        }
    }
}
