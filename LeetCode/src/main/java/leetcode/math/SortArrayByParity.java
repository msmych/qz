package leetcode.math;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface SortArrayByParity {

    int[] sortArrayByParity(int[] A);

    class Solution implements SortArrayByParity {

        @Override
        public int[] sortArrayByParity(int[] A) {
            Map<Boolean, List<Integer>> split = Arrays.stream(A)
                    .boxed().collect(Collectors.groupingBy(n -> n % 2 == 0));
            int[] sorted = new int[A.length];
            int index = 0;
            for (List<Integer> half = split.get(true); half != null && index < half.size(); index++)
                sorted[index] = half.get(index);
            List<Integer> half = split.get(false);
            for (int i = 0; half != null && i < half.size(); i++)
                sorted[index + i] = half.get(i);
            return sorted;
        }
    }
}
