package leetcode.math;

public interface Base7 {

    String convertToBase7(int num);

    class Solution implements Base7 {
        @Override
        public String convertToBase7(int num) {
            if (num == 0) {
                return "0";
            }
            StringBuilder sb = new StringBuilder();
            boolean negative = num < 0;
            if (negative) {
                num = -num;
            }
            for (; num > 0; num /= 7) {
                sb.insert(0, num % 7);
            }
            if (negative) {
                sb.insert(0, '-');
            }
            return sb.toString();
        }
    }
}
