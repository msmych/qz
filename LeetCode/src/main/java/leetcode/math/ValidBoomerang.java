package leetcode.math;

public interface ValidBoomerang {

    boolean isBoomerang(int[][] points);

    class Solution implements ValidBoomerang {

        @Override
        public boolean isBoomerang(int[][] points) {
            if (equalPoints(points[0], points[1]) ||
                    equalPoints(points[1], points[2]) ||
                    equalPoints(points[0], points[2])) return false;
            if (points[0][0] == points[1][0]) return points[2][0] != points[0][0];
            if (points[0][1] == points[1][1]) return points[2][1] != points[0][1];
            return (points[0][0] - points[1][0]) * (points[0][1] - points[1][1]) !=
                    (points[1][0] - points[2][0]) * (points[1][1] - points[2][1]) ||
                    (points[0][0] == points[2][1] && points[0][1] == points[2][0]);
        }

        private boolean equalPoints(int[] p1, int[] p2) {
            return p1[0] == p2[0] && p1[1] == p2[1];
        }
    }
}
