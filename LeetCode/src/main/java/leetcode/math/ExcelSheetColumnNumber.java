package leetcode.math;

public interface ExcelSheetColumnNumber {

    int titleToNumber(String s);

    class Solution implements ExcelSheetColumnNumber {

        @Override
        public int titleToNumber(String s) {
            int num = 0;
            int d = 1;
            for (int i = 0; i < s.length(); i++) {
                num += (s.charAt(s.length() - i - 1) - 'A' + 1) * d;
                d *= 'Z' - 'A' + 1;
            }
            return num;
        }
    }
}
