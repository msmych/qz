package leetcode.math;

import static java.lang.Long.parseLong;

public interface Hexspeak {

  String toHexspeak(String num);

  class Solution implements Hexspeak {

    @Override
    public String toHexspeak(String num) {
      String hexspeak = Long.toString(parseLong(num), 16)
        .replaceAll("0", "O")
        .replaceAll("1", "I")
        .toUpperCase();
      return hexspeak.chars().allMatch(c -> c >= 'A') ? hexspeak : "ERROR";
    }
  }
}
