package leetcode.math;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public interface MaximumProductOfThreeNumbers {

    int maximumProduct(int[] nums);

    class Solution implements MaximumProductOfThreeNumbers {

        @Override
        public int maximumProduct(int[] nums) {
            if (nums.length == 3) return nums[0] * nums[1] * nums[2];
            if (Arrays.stream(nums).noneMatch(n -> n > 0)) {
                return Arrays.stream(nums)
                    .boxed()
                    .sorted(Comparator.reverseOrder())
                    .limit(3)
                    .reduce((n1, n2) -> n1 * n2)
                    .orElse(0);
            }
            List<Integer> positive = Arrays.stream(nums)
                .boxed()
                .filter(n -> n > 0)
                .sorted(Comparator.reverseOrder())
                .limit(3)
                .collect(Collectors.toList());
            int[] negative = Arrays.stream(nums)
                .filter(n -> n < 0)
                .sorted()
                .limit(2)
                .toArray();
            if (negative.length < 2 && positive.size() < 2) return 0;
            if (negative.length < 2) return positive.size() == 3
                                            ? positive.stream().reduce((n1, n2) -> n1 * n2).orElse(0)
                                            : 0;
            if (positive.size() < 2) return positive.get(0) * negative[0] * negative[1];
            return positive.get(0) * Math.max(positive.get(1) * positive.get(2), negative[0] * negative[1]);
        }
    }
}
