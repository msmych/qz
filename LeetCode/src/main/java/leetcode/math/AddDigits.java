package leetcode.math;

public interface AddDigits {

  int addDigits(int num);

  class Solution implements AddDigits {
    @Override
    public int addDigits(int num) {
      if (num < 10) {
        return num;
      }
      int sum = 0;
      for (; num > 0; num /= 10) {
        sum += num % 10;
      }
      return addDigits(sum);
    }
  }
}
