package leetcode.math;

public interface MissingNumberInArithmeticProgression {

  int missingNumber(int[] arr);

  class Solution implements MissingNumberInArithmeticProgression {
    @Override
    public int missingNumber(int[] arr) {
      int left = arr[1] - arr[0];
      int right = arr[arr.length - 1] - arr[arr.length - 2];
      int diff = Math.abs(left) < Math.abs(right) ? left : right;
      if (diff == 0) {
        return arr[0];
      }
      for (int i = 1; i < arr.length; i++) {
        if (arr[i] - arr[i - 1] != diff) {
          return arr[i] - diff;
        }
      }
      throw new IllegalArgumentException();
    }
  }
}
