package leetcode.math;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface PowerfulIntegers {

    List<Integer> powerfulIntegers(int x, int y, int bound);

    class Solution implements PowerfulIntegers {

        private final Set<Integer> powers = new HashSet<>();

        @Override
        public List<Integer> powerfulIntegers(int x, int y, int bound) {
            addPowers(x, y, bound);
            addPowers(y, x, bound);
            return new ArrayList<>(powers);
        }

        private void addPowers(int a, int b, int bound) {
            for (int i = 0; true; i++) {
                int apow = getPower(a, i);
                if (apow > bound)
                    break;
                for (int j = 0; true; j++) {
                    int bpow = getPower(b, j);
                    int sum = apow + bpow;
                    if (sum <= bound)
                        powers.add(sum);
                    else
                        break;
                    if (b == 1)
                        break;
                }
                if (a == 1)
                    break;
            }
        }

        private int getPower(int n, int power) {
            int pow = 1;
            for (; power > 0; power--)
                pow *= n;
            return pow;
        }
    }
}
