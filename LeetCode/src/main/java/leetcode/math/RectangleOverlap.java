package leetcode.math;

public interface RectangleOverlap {

    boolean isRectangleOverlap(int[] rec1, int[] rec2);

    class Solution implements RectangleOverlap {

        @Override
        public boolean isRectangleOverlap(int[] rec1, int[] rec2) {
            return isOverlap(rec1, rec2)
                    || isOverlap(rec2, rec1);
        }

        private boolean isOverlap(int[] rec1, int[] rec2) {
            return isBetween(rec1[0], rec1[2], rec2[0], rec2[2])
                    && isBetween(rec1[1], rec1[3], rec2[1], rec2[3]);
        }

        private boolean isBetween(int z11, int z12, int z21, int z22) {
            return z11 > z21 && z11 < z22
                    || z12 > z21 && z12 < z22
                    || z11 < z21 && z12 >= z22
                    || z11 <= z21 && z12 > z22;
        }
    }
}
