package leetcode.math;

public interface ProjectionAreaOf3DShapes {

    int projectionArea(int[][] grid);

    class Solution implements ProjectionAreaOf3DShapes {

        private int[][] grid;
        private int area = 0;

        @Override
        public int projectionArea(int[][] grid) {
            this.grid = grid;
            addX();
            addY();
            addZ();
            return area;
        }

        private void addX() {
            for (int[] row : grid) {
                for (int cell : row) {
                    if (cell > 0)
                        area++;
                }
            }
        }

        private void addY() {
            for (int[] row : grid) {
                int max = 0;
                for (int y = 0; y < grid.length; y++) {
                    if (row[y] > max)
                        max = row[y];
                }
                area += max;
            }
        }

        private void addZ() {
            for (int y = 0; y < grid.length; y++) {
                int max = 0;
                for (int[] row : grid) {
                    if (row[y] > max)
                        max = row[y];
                }
                area += max;
            }
        }
    }
}
