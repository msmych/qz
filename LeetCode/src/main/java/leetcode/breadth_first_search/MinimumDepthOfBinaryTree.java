package leetcode.breadth_first_search;

import leetcode.tree.TreeNode;

public interface MinimumDepthOfBinaryTree {

    int minDepth(TreeNode root);

    class Solution implements MinimumDepthOfBinaryTree {

        @Override
        public int minDepth(TreeNode root) {
            if (root == null)
                return 0;
            int left = minDepth(root.left), right = minDepth(root.right);
            if (left == 0)
                return 1 + right;
            if (right == 0)
                return 1 + left;
            return 1 + Math.min(left, right);
        }
    }
}
