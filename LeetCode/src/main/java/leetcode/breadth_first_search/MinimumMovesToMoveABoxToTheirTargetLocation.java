package leetcode.breadth_first_search;

import java.util.*;

import static java.util.Arrays.stream;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.toSet;

public interface MinimumMovesToMoveABoxToTheirTargetLocation {

  int minPushBox(char[][] grid);

  class Solution implements MinimumMovesToMoveABoxToTheirTargetLocation {

    private enum Direction {UP, RIGHT, DOWN, LEFT}

    public class Cell {
      int i, j;

      Cell(int i, int j) {
        this.i = i;
        this.j = j;
      }

      boolean isStop(Direction direction) {
        switch (direction) {
          case UP:
            return i == 0 || grid[i - 1][j] == '#';
          case RIGHT:
            return j == grid[i].length - 1 || grid[i][j + 1] == '#';
          case DOWN:
            return i == grid.length - 1 || grid[i + 1][j] == '#';
          case LEFT:
            return j == 0 || grid[i][j - 1] == '#';
          default:
            throw new IllegalArgumentException();
        }
      }

      Cell move(Direction direction) {
        switch (direction) {
          case UP:
            return new Cell(i - 1, j);
          case RIGHT:
            return new Cell(i, j + 1);
          case DOWN:
            return new Cell(i + 1, j);
          case LEFT:
            return new Cell(i, j - 1);
          default:
            throw new IllegalArgumentException();
        }
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return i == cell.i &&
          j == cell.j;
      }

      @Override
      public int hashCode() {
        return Objects.hash(i, j);
      }
    }

    public class PlayerBox {
      Cell player, box;
      int pushes = 0;

      PlayerBox() {
        for (int i = 0; i < grid.length; i++) {
          for (int j = 0; j < grid[i].length; j++) {
            char c = grid[i][j];
            switch (c) {
              case 'S':
                player = new Cell(i, j);
                break;
              case 'B':
                box = new Cell(i, j);
                break;
              case 'T':
                target = new Cell(i, j);
            }
          }
        }
      }

      private PlayerBox(PlayerBox pbt, Direction direction) {
        player = pbt.player.move(direction);
        boolean isPush = pbt.isPush(direction);
        box = isPush ? pbt.box.move(direction) : pbt.box;
        pushes = isPush ? pbt.pushes + 1 : pbt.pushes;
      }

      private Set<PlayerBox> nextMoves() {
        return stream(Direction.values())
          .map(this::tryMove)
          .filter(Optional::isPresent)
          .map(Optional::get)
          .collect(toSet());
      }

      private Optional<PlayerBox> tryMove(Direction direction) {
        if (isStop(direction)) {
          return empty();
        }
        PlayerBox pbt = new PlayerBox(this, direction);
        if (visited.contains(pbt)) {
          return empty();
        }
        return Optional.of(pbt);
      }

      private boolean isStop(Direction direction) {
        if (player.isStop(direction)) {
          return true;
        }
        if (isPush(direction)) {
          return box.isStop(direction);
        }
        return false;
      }

      private boolean isPush(Direction direction) {
        switch (direction) {
          case UP:
            return player.i == box.i + 1 && player.j == box.j;
          case RIGHT:
            return player.i == box.i && player.j == box.j - 1;
          case DOWN:
            return player.i == box.i - 1 && player.j == box.j;
          case LEFT:
            return player.i == box.i && player.j == box.j + 1;
          default:
            throw new IllegalArgumentException();
        }
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerBox playerBox = (PlayerBox) o;
        return Objects.equals(player, playerBox.player) &&
          Objects.equals(box, playerBox.box);
      }

      @Override
      public int hashCode() {
        return Objects.hash(player, box);
      }
    }

    private final Set<PlayerBox> visited = new HashSet<>();

    private char[][] grid;
    private Cell target;

    @Override
    public int minPushBox(char[][] grid) {
      this.grid = grid;
      Queue<PlayerBox> queue = new LinkedList<>();
      queue.offer(new PlayerBox());
      int minPushes = -1;
      while (!queue.isEmpty()) {
        for (int i = queue.size(); i > 0; i--) {
          PlayerBox pbt = queue.poll();
          if (minPushes != -1 && pbt.pushes >= minPushes) {
            continue;
          }
          if (pbt.box.equals(target)) {
            minPushes = pbt.pushes;
            continue;
          }
          Set<PlayerBox> nextMoves = pbt.nextMoves();
          nextMoves.forEach(queue::offer);
          visited.addAll(nextMoves);
        }
      }
      return minPushes;
    }

  }
}
