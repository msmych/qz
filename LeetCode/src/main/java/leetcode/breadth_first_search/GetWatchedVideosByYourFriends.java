package leetcode.breadth_first_search;

import java.util.*;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.*;

public interface GetWatchedVideosByYourFriends {

  List<String> watchedVideosByFriends(List<List<String>> watchedVideos, int[][] friends, int id, int level);

  class Solution implements GetWatchedVideosByYourFriends {
    @Override
    public List<String> watchedVideosByFriends(List<List<String>> watchedVideos, int[][] friends, int id, int level) {
      Set<Integer> visited = new HashSet<>();
      visited.add(id);
      Queue<Integer> queue = new LinkedList<>();
      queue.offer(id);
      while (level-- > 0) {
        for (int q = queue.size(); q > 0; q--) {
          stream(friends[queue.poll()])
            .filter(f -> !visited.contains(f))
            .peek(visited::add)
            .forEach(queue::offer);
        }
      }
      return queue.stream()
        .map(watchedVideos::get)
        .flatMap(List::stream)
        .collect(groupingBy(v -> v, summingInt(v -> 1)))
        .entrySet()
        .stream()
        .sorted((a, b) -> {
          if (a.getValue() > b.getValue()) {
            return 1;
          } else if (a.getValue() < b.getValue()) {
            return -1;
          } else {
            return a.getKey().compareTo(b.getKey());
          }
        })
        .map(Map.Entry::getKey)
        .collect(toList());
    }
  }
}
