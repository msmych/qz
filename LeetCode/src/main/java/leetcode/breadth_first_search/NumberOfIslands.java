package leetcode.breadth_first_search;

import java.util.*;

public interface NumberOfIslands {

  int numIslands(char[][] grid);

  class QueueSolution implements NumberOfIslands {

    class Point {
      int x;
      int y;

      Point(int x, int y) {
        this.x = x;
        this.y = y;
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
          y == point.y;
      }

      @Override
      public int hashCode() {
        return Objects.hash(x, y);
      }
    }

    private final Queue<Point> points = new LinkedList<>();
    private final Set<Point> visited = new HashSet<>();

    private char[][] grid;

    @Override
    public int numIslands(char[][] grid) {
      this.grid = grid;
      if (grid.length == 0)
        return 0;
      int count = 0;
      for (int y = 0; y < grid.length; y++) {
        for (int x = 0; x < grid[0].length; x++) {
          if (grid[y][x] == '0')
            continue;
          Point point = new Point(x, y);
          if (visited.contains(point))
            continue;
          count++;
          points.offer(point);
          visited.add(point);
          while (!points.isEmpty()) {
            Point p = points.poll();
            check4Directions(p.y, p.x);
          }
        }
      }
      return count;
    }

    private void check4Directions(int i, int j) {
      if (isValidIsland(i, j + 1)) {
        Point p = new Point(j + 1, i);
        if (grid[i][j + 1] == '1')
          points.offer(p);
        visited.add(p);
      }
      if (isValidIsland(i, j - 1)) {
        Point p = new Point(j - 1, i);
        if (grid[i][j - 1] == '1')
          points.offer(p);
        visited.add(p);
      }
      if (isValidIsland(i + 1, j)) {
        Point p = new Point(j, i + 1);
        if (grid[i + 1][j] == '1')
          points.offer(p);
        visited.add(p);
      }
      if (isValidIsland(i - 1, j)) {
        Point p = new Point(j, i - 1);
        if (grid[i - 1][j] == '1')
          points.offer(p);
        visited.add(p);
      }
    }

    private boolean isValidIsland(int i, int j) {
      if (i < 0 || j < 0)
        return false;
      if (i >= grid.length || j >= grid[0].length)
        return false;
      return !visited.contains(new Point(j, i));
    }
  }

  class StackSolution implements NumberOfIslands {

    class Point {
      int x;
      int y;

      Point(int x, int y) {
        this.x = x;
        this.y = y;
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
          y == point.y;
      }

      @Override
      public int hashCode() {
        return Objects.hash(x, y);
      }
    }

    private final Set<Point> visited = new HashSet<>();

    private char[][] grid;
    private int numIslands = 0;

    @Override
    public int numIslands(char[][] grid) {
      if (grid.length == 0)
        return 0;
      this.grid = grid;
      for (int y = 0; y < grid.length; y++) {
        for (int x = 0; x < grid[0].length; x++) {
          if (grid[y][x] == '1' && !visited.contains(new Point(x, y))) {
            numIslands++;
            explore(x, y);
          }
        }
      }
      return numIslands;
    }

    private void explore(int x, int y) {
      if (y < 0 || y >= grid.length || x < 0 || x >= grid[0].length)
        return;
      Point point = new Point(x, y);
      if (visited.contains(point))
        return;
      visited.add(point);
      if (grid[y][x] == '0')
        return;
      explore(x + 1, y);
      explore(x - 1, y);
      explore(x, y + 1);
      explore(x, y - 1);
    }
  }
}
