package leetcode.breadth_first_search;

import java.util.*;

import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toSet;

public interface RemoveInvalidParentheses {

  List<String> removeInvalidParentheses(String s);

  class Solution implements RemoveInvalidParentheses {

    private final Map<String, Boolean> validCache = new HashMap<>();
    private final Map<String, Set<String>> cache = new HashMap<>();

    @Override
    public List<String> removeInvalidParentheses(String s) {
      return new ArrayList<>(next(s));
    }

    private Set<String> next(String s) {
      if (cache.containsKey(s)) {
        return cache.get(s);
      }
      if (isValid(s)) {
        return singleton(s);
      }
      Set<String> nextSet = new HashSet<>();
      for (int i = 0; i < s.length(); i++) {
        char c = s.charAt(i);
        if (c != '(' && c != ')') {
          continue;
        }
        String left = s.substring(0, i);
        String right = s.substring(i + 1);
        nextSet.addAll(next(left + right));
      }
      int max = nextSet.stream().mapToInt(String::length).max().getAsInt();
      nextSet = nextSet.stream().filter(next -> next.length() == max).collect(toSet());
      cache.put(s, nextSet);
      return nextSet;
    }

    private boolean isValid(String s) {
      if (validCache.containsKey(s)) {
        return validCache.get(s);
      }
      int p = 0;
      for (int i = 0; i < s.toCharArray().length; i++) {
        char c = s.charAt(i);
        if (c == '(') {
          p++;
        } else if (c == ')') {
          if (p > 0) {
            p--;
          } else {
            validCache.put(s, false);
            return false;
          }
        }
      }
      boolean isValid = p == 0;
      validCache.put(s, isValid);
      return isValid;
    }
  }
}
