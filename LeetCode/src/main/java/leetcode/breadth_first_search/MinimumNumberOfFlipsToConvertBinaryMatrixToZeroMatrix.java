package leetcode.breadth_first_search;

import java.util.*;

import static java.util.Optional.empty;

public interface MinimumNumberOfFlipsToConvertBinaryMatrixToZeroMatrix {

  int minFlips(int[][] mat);

  class Solution implements MinimumNumberOfFlipsToConvertBinaryMatrixToZeroMatrix {

    private final Set<String> visited = new HashSet<>();

    private int[][] mat;

    @Override
    public int minFlips(int[][] mat) {
      this.mat = mat;
      int flips = 0;
      Queue<String> queue = new LinkedList<>();
      queue.offer(serialize());
      while (!queue.isEmpty()) {
        for (int size = queue.size(); size > 0; size--) {
          String s = queue.poll();
          if (!s.contains("1")) {
            return flips;
          }
          for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
              flip(s, i, j).ifPresent(queue::offer);
            }
          }
        }
        flips++;
      }
      return -1;
    }

    private String serialize() {
      StringBuilder sb = new StringBuilder();
      for (int[] row : mat) {
        for (int cell : row) {
          sb.append(cell);
        }
      }
      return sb.toString();
    }

    private Optional<String> flip(String s, int i, int j) {
      if (i < 0 || i >= mat.length || j < 0 || j >= mat[0].length) {
        return empty();
      }
      StringBuilder sb = new StringBuilder(s);
      flip(sb, i, j);
      if (i > 0) {
        flip(sb, i - 1, j);
      }
      if (i < mat.length - 1) {
        flip(sb, i + 1, j);
      }
      if (j > 0) {
        flip(sb, i, j - 1);
      }
      if (j < mat[0].length - 1) {
        flip(sb, i, j + 1);
      }
      if (visited.contains(sb.toString())) {
        return empty();
      }
      visited.add(sb.toString());
      return Optional.of(sb.toString());
    }

    private void flip(StringBuilder sb, int i, int j) {
      sb.setCharAt(mat[i].length * i + j, sb.charAt(mat[i].length * i + j) == '0' ? '1' : '0');
    }
  }
}
