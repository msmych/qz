package leetcode.breadth_first_search;

import java.util.*;

public interface MinimumMovesToReachTargetWithRotations {

    int minimumMoves(int[][] grid);

    class Solution implements MinimumMovesToReachTargetWithRotations {

        private static class Snake {
            int itail, jtail, ihead, jhead;

            public Snake(int itail, int jtail, int ihead, int jhead) {
                this.itail = itail;
                this.jtail = jtail;
                this.ihead = ihead;
                this.jhead = jhead;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Snake snake = (Snake) o;
                return itail == snake.itail &&
                    jtail == snake.jtail &&
                    ihead == snake.ihead &&
                    jhead == snake.jhead;
            }

            @Override
            public int hashCode() {
                return Objects.hash(itail, jtail, ihead, jhead);
            }
        }

        private int[][] grid;

        @Override
        public int minimumMoves(int[][] grid) {
            this.grid = grid;
            Queue<Snake> queue = new LinkedList<>();
            Snake initial = new Snake(0, 0, 0, 1);
            queue.offer(initial);
            Set<Snake> visited = new HashSet<>();
            visited.add(initial);
            int moves = 0;
            while (!queue.isEmpty()) {
                int size = queue.size();
                for (int i = 0; i < size; i++) {
                    Snake snake = queue.poll();
                    if (finished(snake)) {
                        return moves;
                    }
                    if (canMoveRight(snake)) {
                        Snake s = new Snake(snake.itail, snake.jtail + 1, snake.ihead, snake.jhead + 1);
                        if (!visited.contains(s)) {
                            queue.offer(s);
                            visited.add(s);
                        }
                    }
                    if (canMoveDown(snake)) {
                        Snake s = new Snake(snake.itail + 1, snake.jtail, snake.ihead + 1, snake.jhead);
                        if (!visited.contains(s)) {
                            queue.offer(s);
                            visited.add(s);
                        }
                    }
                    if (canRotateClockwise(snake)) {
                        Snake s = new Snake(snake.itail, snake.jtail, snake.ihead + 1, snake.jhead - 1);
                        if (!visited.contains(s)) {
                            queue.offer(s);
                            visited.add(s);
                        }
                    }
                    if (canRotateCounterclockwise(snake)) {
                        Snake s = new Snake(snake.itail, snake.jtail, snake.ihead - 1, snake.jhead + 1);
                        if (!visited.contains(s)) {
                            queue.offer(s);
                            visited.add(s);
                        }
                    }
                }
                moves++;
            }
            return -1;
        }

        private boolean finished(Snake snake) {
            return snake.itail == grid.length - 1 && snake.jtail == grid[0].length - 2
                && snake.ihead == grid.length - 1 && snake.jhead == grid[0].length - 1;
        }

        private boolean canMoveRight(Snake snake) {
            return snake.jhead < grid[0].length - 1
                && grid[snake.itail][snake.jtail + 1] != 1 && grid[snake.ihead][snake.jhead + 1] != 1;
        }

        private boolean canMoveDown(Snake snake) {
            return snake.ihead < grid.length - 1
                && grid[snake.itail + 1][snake.jtail] != 1 && grid[snake.ihead + 1][snake.jhead] != 1;
        }

        private boolean canRotateClockwise(Snake snake) {
            return snake.ihead == snake.itail && snake.ihead < grid.length - 1
                && grid[snake.itail + 1][snake.jtail] != 1 && grid[snake.ihead + 1][snake.jhead] != 1;
        }

        private boolean canRotateCounterclockwise(Snake snake) {
            return snake.jhead == snake.jtail && snake.jhead < grid[0].length - 1
                && grid[snake.itail][snake.jtail + 1] != 1 && grid[snake.ihead][snake.jhead + 1] != 1;
        }
    }
}
