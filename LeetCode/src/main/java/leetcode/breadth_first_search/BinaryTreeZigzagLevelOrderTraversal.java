package leetcode.breadth_first_search;

import leetcode.tree.TreeNode;

import java.util.*;

import static java.util.Collections.emptyList;

public interface BinaryTreeZigzagLevelOrderTraversal {

  List<List<Integer>> zigzagLevelOrder(TreeNode root);

  class Solution implements BinaryTreeZigzagLevelOrderTraversal {

    private final Stack<TreeNode> stack = new Stack<>();

    @Override
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
      if (root == null) {
        return emptyList();
      }
      List<List<Integer>> zigzag = new ArrayList<>();
      stack.push(root);
      int count = 0;
      while (!stack.isEmpty()) {
        List<Integer> row = new ArrayList<>();
        List<TreeNode> next = new ArrayList<>();
        for (int size = stack.size(); size > 0; size--) {
          TreeNode node = stack.pop();
          if (node == null) {
            continue;
          }
          if (count % 2 == 0) {
            row.add(node.val);
            next.add(node.left);
            next.add(node.right);
          } else {
            row.add(node.val);
            next.add(node.right);
            next.add(node.left);
          }
        }
        stack.addAll(next);
        if (!row.isEmpty()) {
          zigzag.add(row);
        }
        count++;
      }
      return zigzag;
    }
  }
}
