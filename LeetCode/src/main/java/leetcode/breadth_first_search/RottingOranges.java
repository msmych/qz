package leetcode.breadth_first_search;

import java.util.HashSet;
import java.util.Set;

public interface RottingOranges {

    int orangesRotting(int[][] grid);

    class Solution implements RottingOranges {

        private class Orange {
            int i, j;

            Orange(int i, int j) {
                this.i = i;
                this.j = j;
            }
        }

        @Override
        public int orangesRotting(int[][] grid) {
            int lastRottenCount = -1, minutes = 0;
            while (getGood(grid).size() > 0) {
                Set<Orange> rottenOranges = getRotten(grid);
                if (rottenOranges.size() == lastRottenCount)
                    return -1;
                lastRottenCount = rottenOranges.size();
                for (Orange rottenOrange : getRotten(grid)) {
                    int i = rottenOrange.i, j = rottenOrange.j;
                    if (j > 0 && grid[i][j - 1] == 1)
                        grid[i][j - 1] = 2;
                    if (i > 0 && grid[i - 1][j] == 1)
                        grid[i - 1][j] = 2;
                    if (j < grid[0].length - 1 && grid[i][j + 1] == 1)
                        grid[i][j + 1] = 2;
                    if (i < grid.length - 1 && grid[i + 1][j] == 1)
                        grid[i + 1][j] = 2;
                }
                minutes++;
            }
            return minutes;
        }

        private Set<Orange> getRotten(int[][] grid) { return getOranges(grid, 2); }

        private Set<Orange> getGood(int[][] grid) { return getOranges(grid, 1); }

        private Set<Orange> getOranges(int[][] grid, int kind) {
            Set<Orange> good = new HashSet<>();
            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[0].length; j++) {
                    if (grid[i][j] == kind)
                        good.add(new Orange(i, j));
                }
            }
            return good;
        }
    }
}
