package leetcode.breadth_first_search;

import java.util.*;

import static java.util.Arrays.stream;

public interface MaximumCandiesYouCanGetFromBoxes {

  int maxCandies(int[] status, int[] candies, int[][] keys, int[][] containedBoxes, int[] initialBoxes);

  class Solution implements MaximumCandiesYouCanGetFromBoxes {

    @Override
    public int maxCandies(int[] status, int[] candies, int[][] keys, int[][] containedBoxes, int[] initialBoxes) {
      int candy = 0;
      Queue<Integer> boxes = new LinkedList<>();
      Set<Integer> keySet = new HashSet<>();
      Set<Integer> visited = new HashSet<>();
      stream(initialBoxes).forEach(boxes::offer);
      boolean openedAnyBox = true;
      while (!boxes.isEmpty() && openedAnyBox) {
        openedAnyBox = false;
        for (int i = boxes.size(); i > 0; i--) {
          int box = boxes.poll();
          if (visited.contains(box)) {
            continue;
          }
          if (status[box] == 0 && !keySet.contains(box)) {
            boxes.offer(box);
            continue;
          }
          openedAnyBox = true;
          candy += candies[box];
          stream(keys[box]).forEach(keySet::add);
          stream(containedBoxes[box]).forEach(boxes::offer);
          visited.add(box);
        }
      }
      return candy;
    }
  }
}
