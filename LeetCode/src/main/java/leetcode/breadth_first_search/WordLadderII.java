package leetcode.breadth_first_search;

import java.util.*;
import java.util.stream.Collectors;

public interface WordLadderII {

    List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList);

    class Solution implements WordLadderII {

        private final Queue<List<String>> queue = new LinkedList<>();

        private String endWord;

        @Override
        public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
            this.endWord = endWord;
            queue.offer(Collections.singletonList(beginWord));
            List<String> wordOptions = new ArrayList<>(wordList);
            Set<String> allNextWords = new HashSet<>();
            while (!wordOptions.isEmpty()) {
                if (queue.stream().anyMatch(list -> list.get(list.size() - 1).equals(endWord)))
                    return relevantLadders();
                for (int size = queue.size(); size > 0; size--) {
                    List<String> words = queue.poll();
                    Set<String> nextSet = wordOptions.stream()
                            .filter(word -> isOneLetterDifference(word, words.get(words.size() - 1)))
                            .collect(Collectors.toSet());
                    allNextWords.addAll(nextSet);
                    for (String next : nextSet) {
                        List<String> list = new ArrayList<>(words);
                        list.add(next);
                        queue.offer(list);
                    }
                }
                if (allNextWords.isEmpty())
                    return new ArrayList<>();
                wordOptions.removeAll(allNextWords);
            }
            if (queue.stream().noneMatch(words -> words.get(words.size() - 1).equals(endWord)))
                return new ArrayList<>();
            return relevantLadders();
        }

        private boolean isOneLetterDifference(String w1, String w2) {
            int diff = 0;
            for (int i = 0; i < w1.length(); i++) {
                if (w1.charAt(i) != w2.charAt(i))
                    diff++;
                if (diff > 1)
                    return false;
            }
            return diff == 1;
        }

        private List<List<String>> relevantLadders() {
            return queue.stream()
                    .filter(list -> list.get(list.size() - 1).equals(endWord))
                    .collect(Collectors.toList());
        }
    }
}
