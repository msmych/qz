package leetcode.breadth_first_search;

import java.util.*;
import java.util.stream.Collectors;

public interface OpenTheLock {

    int openLock(String[] deadends, String target);

    class Solution implements OpenTheLock {

        private final Queue<String> combinations = new LinkedList<>();
        private final Set<String> visited = new HashSet<>();

        @Override
        public int openLock(String[] deadends, String target) {
            if (target.equals("0000"))
                return 0;
            List<String> deadList = Arrays.stream(deadends).collect(Collectors.toList());
            combinations.offer("0000");
            visited.add("0000");
            int turns = -1;
            while (!combinations.isEmpty()) {
                turns++;
                int size = combinations.size();
                for (int i = 0; i < size; i++) {
                    String combination = combinations.poll();
                    if (combination.equals(target))
                        return turns;
                    if (deadList.contains(combination))
                        continue;
                    for (int j = 3; j >= 0; j--) {
                        int num = Integer.valueOf(combination.substring(j, j + 1));
                        String up = combination.substring(0, j)
                                + ((num + 1) % 10) + combination.substring(j + 1);
                        String down = combination.substring(0, j)
                                + (num == 0 ? 9 : num - 1) + combination.substring(j + 1);
                        if (!visited.contains(up)) {
                            combinations.offer(up);
                            visited.add(up);
                        }
                        if (!visited.contains(down)) {
                            combinations.offer(down);
                            visited.add(down);
                        }
                    }
                }
            }
            return -1;
        }
    }
}
