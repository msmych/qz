package leetcode.breadth_first_search;

import java.util.*;
import java.util.stream.Collectors;

public interface WordLadder {

  int ladderLength(String beginWord, String endWord, List<String> wordList);

  class Solution implements WordLadder {

    @Override
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
      int steps = 0;
      Queue<String> queue = new LinkedList<>();
      queue.offer(beginWord);
      List<String> wordOptions = new ArrayList<>(wordList);
      while (!wordOptions.isEmpty()) {
        for (int size = queue.size(); size > 0; size--) {
          String word = queue.poll();
          if (word.equals(endWord))
            return steps + 1;
          Set<String> nextSet = wordOptions.stream()
            .filter(w -> isOneLetterDifference(w, word))
            .collect(Collectors.toSet());
          if (nextSet.isEmpty() && queue.isEmpty())
            return 0;
          wordOptions.removeAll(nextSet);
          for (String next : nextSet) {
            queue.offer(next);
          }
        }
        steps++;
      }
      return queue.contains(endWord) ? steps + 1 : 0;
    }

    private boolean isOneLetterDifference(String w1, String w2) {
      int diff = 0;
      for (int i = 0; i < w1.length(); i++) {
        if (w1.charAt(i) != w2.charAt(i))
          diff++;
        if (diff > 1)
          return false;
      }
      return diff == 1;
    }
  }
}
