package leetcode.breadth_first_search;

import static java.lang.Integer.MAX_VALUE;
import static java.util.Arrays.fill;

public interface PerfectSquares {

  int numSquares(int n);

  class Solution implements PerfectSquares {

    @Override
    public int numSquares(int n) {
      int[] dp = new int[n + 1];
      fill(dp, MAX_VALUE);
      dp[0] = 0;
      for (int i = 0; i <= n; i++) {
        for (int j = 1; i + j * j <= n; j++) {
          if (dp[i] + 1 < dp[i + j * j]) {
            dp[i + j * j] = dp[i] + 1;
          }
        }
      }
      return dp[n];
    }
  }
}
