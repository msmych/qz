package leetcode.breadth_first_search;

import java.util.*;
import java.util.stream.Stream;

import static java.util.Collections.singleton;
import static java.util.Optional.empty;


public interface ShortestPathInAGridWithObstaclesElimination {

  int shortestPath(int[][] grid, int k);

  class Solution implements ShortestPathInAGridWithObstaclesElimination {

    private class Path {
      int i, j, k;
      Set<Integer> visited;

      Path(int i, int j, int k, Set<Integer> visited) {
        this.i = i;
        this.j = j;
        this.k = k;
        this.visited = visited;
      }

      Optional<Path> next(int i, int j) {
        if (i < 0 || i >= grid.length || j < 0 || j >= grid[i].length) {
          return empty();
        }
        int cell = (i << 16) + j;
        if (visited.contains(cell)) {
          return empty();
        }
        Path next = new Path(i, j, k, new HashSet<>(visited));
        next.visited.add(cell);
        if (grid[i][j] == 1) {
          if (k == 0) {
            return empty();
          }
          next.k--;
        }
        return Optional.of(next);
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Path path = (Path) o;
        return i == path.i &&
          j == path.j &&
          k == path.k &&
          Objects.equals(visited, path.visited);
      }

      @Override
      public int hashCode() {
        return Objects.hash(i, j, k, visited);
      }
    }

    private final Set<Path> paths = new HashSet<>();

    private int[][] grid;

    @Override
    public int shortestPath(int[][] grid, int k) {
      this.grid = grid;
      Queue<Path> queue = new LinkedList<>();
      queue.offer(new Path(0, 0, k, singleton(0)));
      int steps = 0;
      while (!queue.isEmpty()) {
        for (int size = queue.size(); size > 0; size--) {
          Path path = queue.poll();
          if (path.i == grid.length - 1 && path.j == grid[0].length - 1) {
            return steps;
          }
          Stream.of(
            path.next(path.i, path.j + 1),
            path.next(path.i, path.j - 1),
            path.next(path.i + 1, path.j),
            path.next(path.i - 1, path.j))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .filter(next -> !paths.contains(next))
            .forEach(next -> {
              paths.add(next);
              queue.offer(next);
            });
        }
        steps++;
      }
      return -1;
    }
  }
}
