package leetcode.segment_tree;

import java.util.Arrays;
import java.util.List;

public interface CountOfSmallerNumbersAfterSelf {

  List<Integer> countSmaller(int[] nums);

  class Solution implements CountOfSmallerNumbersAfterSelf {

    private static class Node {
      int val, leftChildrenCount, duplicates = 1;
      Node left, right;

      Node(int val, int leftChildrenCount) {
        this.val = val;
        this.leftChildrenCount = leftChildrenCount;
      }
    }

    private Integer[] smaller;

    @Override
    public List<Integer> countSmaller(int[] nums) {
      smaller = new Integer[nums.length];
      Node node = null;
      for (int i = nums.length - 1; i >= 0; i--) {
        node = insert(nums[i], node, i, 0);
      }
      return Arrays.asList(smaller);
    }

    private Node insert(int val, Node node, int i, int previousLeftChildrenCount) {
      if (node == null) {
        node = new Node(val, 0);
        smaller[i] = previousLeftChildrenCount;
      } else if (val == node.val) {
        node.duplicates++;
        smaller[i] = previousLeftChildrenCount + node.leftChildrenCount;
      } else if (val < node.val) {
        node.leftChildrenCount++;
        node.left = insert(val, node.left, i, previousLeftChildrenCount);
      } else {
        node.right = insert(val, node.right, i, previousLeftChildrenCount + node.duplicates + node.leftChildrenCount);
      }
      return node;
    }

  }
}
