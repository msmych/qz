package leetcode.array;

import java.util.ArrayList;
import java.util.List;

public interface Shift2DGrid {

  List<List<Integer>> shiftGrid(int[][] grid, int k);

  class Solution implements Shift2DGrid {

    private int m;
    private int n;
    private int k;

    @Override
    public List<List<Integer>> shiftGrid(int[][] grid, int k) {
      this.m = grid.length;
      this.n = grid[0].length;
      this.k = m*n - k % (m*n);
      List<List<Integer>> shiftedList = new ArrayList<>();
      for (int i = 0; i < m; i++) {
        List<Integer> rowList = new ArrayList<>();
        for (int j = 0; j < n; j++) {
          rowList.add(grid[row(i, j)][column(j)]);
        }
        shiftedList.add(rowList);
      }
      return shiftedList;
    }

    private int row(int i, int j) {
      return (i + (k + j)/n) % m;
    }

    private int column(int j) {
      return (j + k % n) % n;
    }
  }
}
