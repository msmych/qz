package leetcode.array;

import java.util.HashSet;
import java.util.Set;

public interface MagicSquaresInGrid {

    int numMagicSquaresInside(int[][] grid);

    class Solution implements MagicSquaresInGrid {

        @Override
        public int numMagicSquaresInside(int[][] grid) {
            int magicCount = 0;
            for (int i = 0; i < grid.length - 2; i++) {
                for (int j = 0; j < grid[0].length - 2; j++) {
                    Set<Integer> nums = new HashSet<>();
                    boolean magic = true;
                    for (int rowIndex = i; rowIndex < i + 3; rowIndex++) {
                        if (!magic)
                            break;
                        for (int columnIndex = j; columnIndex < j + 3; columnIndex++) {
                            int num = grid[rowIndex][columnIndex];
                            if (num < 1 || num > 9 || nums.contains(num)) {
                                magic = false;
                                break;
                            } else
                                nums.add(num);
                        }
                    }
                    int sum = grid[i][j] + grid[i + 1][j + 1] + grid[i + 2][j + 2];
                    if (sum != grid[i][j + 2] + grid[i + 1][j + 1] + grid[i + 2][j])
                        continue;
                    for (int k = 0; k < 3; k++) {
                        if (sum != grid[i][j + k] + grid[i + 1][j + k] + grid[i + 2][j + k]) {
                            magic = false;
                            break;
                        }
                        if (sum != grid[i + k][j] + grid[i + k][j + 1] + grid[i + k][j + 2]) {
                            magic = false;
                            break;
                        }
                    }
                    if (magic)
                        magicCount++;
                }
            }
            return magicCount;
        }
    }
}
