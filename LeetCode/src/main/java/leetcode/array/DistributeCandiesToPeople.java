package leetcode.array;

public interface DistributeCandiesToPeople {

    int[] distributeCandies(int candies, int num_people);

    class Solution implements DistributeCandiesToPeople {

        @Override
        public int[] distributeCandies(int candies, int num_people) {
            int[] distribution = new int[num_people];
            int c = 1;
            int i = 0;
            while (candies >= c) {
                for (i = 0; i < num_people && candies >= c; i++) {
                    distribution[i] += c;
                    candies -= c++;
                }
            }
            distribution[i % num_people] += candies;
            return distribution;
        }
    }
}
