package leetcode.array;

import java.util.HashMap;
import java.util.Map;

public interface AlphabetBoardPath {

    String alphabetBoardPath(String target);

    class Solution implements AlphabetBoardPath {

        private final Map<Character, int[]> board = new HashMap<Character, int[]>() {{
            put('a', new int[]{0, 0});
            put('b', new int[]{0, 1});
            put('c', new int[]{0, 2});
            put('d', new int[]{0, 3});
            put('e', new int[]{0, 4});
            put('f', new int[]{1, 0});
            put('g', new int[]{1, 1});
            put('h', new int[]{1, 2});
            put('i', new int[]{1, 3});
            put('j', new int[]{1, 4});
            put('k', new int[]{2, 0});
            put('l', new int[]{2, 1});
            put('m', new int[]{2, 2});
            put('n', new int[]{2, 3});
            put('o', new int[]{2, 4});
            put('p', new int[]{3, 0});
            put('q', new int[]{3, 1});
            put('r', new int[]{3, 2});
            put('s', new int[]{3, 3});
            put('t', new int[]{3, 4});
            put('u', new int[]{4, 0});
            put('v', new int[]{4, 1});
            put('w', new int[]{4, 2});
            put('x', new int[]{4, 3});
            put('y', new int[]{4, 4});
            put('z', new int[]{5, 0});
        }};
        private int i = 0, j = 0;

        @Override
        public String alphabetBoardPath(String target) {
            StringBuilder sb = new StringBuilder();
            for (char c : target.toCharArray()) {
                sb.append(next(c));
            }
            return sb.toString();
        }

        private String next(char c) {
            return board.get(c)[0] > i
                    ? nextHorizontal(c) + nextVertical(c) + '!'
                    : nextVertical(c) + nextHorizontal(c) + '!';
        }

        private String nextHorizontal(char c) {
            int t = board.get(c)[1];
            StringBuilder sb = new StringBuilder();
            boolean right = t > j;
            for (; j != t; j += right ? 1 : -1) {
                sb.append(right ? 'R' : 'L');
            }
            return sb.toString();
        }

        private String nextVertical(char c) {
            int t = board.get(c)[0];
            StringBuilder sb = new StringBuilder();
            boolean right = t > i;
            for (; i != t; i += right ? 1 : -1) {
                sb.append(right ? 'D' : 'U');
            }
            return sb.toString();
        }
    }
}
