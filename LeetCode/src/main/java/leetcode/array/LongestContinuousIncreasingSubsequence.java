package leetcode.array;

public interface LongestContinuousIncreasingSubsequence {

    int findLengthOfLCIS(int[] nums);

    class Solution implements LongestContinuousIncreasingSubsequence {

        @Override
        public int findLengthOfLCIS(int[] nums) {
            int i = 0;
            int maxIncreasing = 0;
            int increasing = 0;
            Integer last = null;
            while (i < nums.length) {
                int num = nums[i];
                if (last == null || num > last)
                    increasing++;
                else {
                    if (increasing > maxIncreasing)
                        maxIncreasing = increasing;
                    increasing = 1;
                }
                last = num;
                i++;
            }
            if (increasing > maxIncreasing)
                maxIncreasing = increasing;
            return maxIncreasing;
        }
    }
}
