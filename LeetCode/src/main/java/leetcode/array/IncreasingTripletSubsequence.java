package leetcode.array;

public interface IncreasingTripletSubsequence {

    boolean increasingTriplet(int[] nums);

    class Solution implements IncreasingTripletSubsequence {

        @Override
        public boolean increasingTriplet(int[] nums) {
            if (nums.length < 3) {
                return false;
            }
            int first = nums[0];
            Integer second = null;
            for (int i = 1; i < nums.length; i++) {
                if (second == null) {
                    if (nums[i] > first) {
                        second = nums[i];
                    } else {
                        first = nums[i];
                    }
                } else {
                    if (nums[i] > second) {
                        return true;
                    } else if (nums[i] > first) {
                        second = nums[i];
                    } else {
                        first = nums[i];
                    }
                }
            }
            return false;
        }
    }

}
