package leetcode.array;

public interface CanPlaceFlowers {

    boolean canPlaceFlowers(int[] flowerbed, int n);

    class Solution implements CanPlaceFlowers {

        @Override
        public boolean canPlaceFlowers(int[] flowerbed, int n) {
            for (int i = 0; i < flowerbed.length; i++) {
                if (n == 0) break;
                if (isFree(flowerbed, i)) {
                    flowerbed[i] = 1;
                    n--;
                }
            }
            return n == 0;
        }

        private boolean isFree(int[] flowerbed, int i) {
            return flowerbed[i] == 0 &&
                (i == 0 || flowerbed[i - 1] == 0) &&
                (i == flowerbed.length - 1 || flowerbed[i + 1] == 0);
        }
    }
}
