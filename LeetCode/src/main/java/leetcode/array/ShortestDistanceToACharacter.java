package leetcode.array;

public interface ShortestDistanceToACharacter {

    int[] shortestToChar(String S, char C);

    class Solution implements ShortestDistanceToACharacter {

        @Override
        public int[] shortestToChar(String S, char C) {
            int left = -S.length();
            int[] distances = new int[S.length()];
            for (int i = 0; i < S.length(); i++) {
                char c = S.charAt(i);
                if (c == C) {
                    for (int j = left < 0 ? 0 : left; j < i; j++) {
                        distances[j] = Math.min(j - left, i - j);
                    }
                    left = i;
                }
            }
            for (int j = left < 0 ? 0 : left; j < S.length(); j++) {
                distances[j] = j - left;
            }
            return distances;
        }
    }
}
