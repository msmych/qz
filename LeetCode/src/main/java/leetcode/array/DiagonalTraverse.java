package leetcode.array;

public interface DiagonalTraverse {

    int[] findDiagonalOrder(int[][] matrix);

    class Solution implements DiagonalTraverse {

        enum Direction { TOP_RIGHT, DOWN_LEFT }

        private int i = 0, j = 0;
        private Direction direction = Direction.TOP_RIGHT;
        private int height, width;

        @Override
        public int[] findDiagonalOrder(int[][] matrix) {
            height = matrix.length;
            if (height == 0) {
                return new int[]{};
            }
            width = matrix[0].length;
            int[] result = new int[height * width];
            for (int index = 0; i < height && j < width; index++) {
                result[index] = matrix[i][j];
                updateIndexes();
            }
            return result;
        }

        private void updateIndexes() {
            if (direction == Direction.TOP_RIGHT) {
                updateTopRight();
            } else {
                updateDownLeft();
            }
        }

        private void updateTopRight() {
            if (topBound() || rightBound()) {
                direction = Direction.DOWN_LEFT;
                if (rightBound()) {
                    i++;
                } else {
                    j++;
                }
            } else {
                i--;
                j++;
            }
        }

        private void updateDownLeft() {
            if (leftBound() || bottomBound()) {
                direction = Direction.TOP_RIGHT;
                if (bottomBound()) {
                    j++;
                } else {
                    i++;
                }
            } else {
                j--;
                i++;
            }
        }

        private boolean topBound() {
            return i == 0;
        }

        private boolean rightBound() {
            return j + 1 == width;
        }

        private boolean leftBound() {
            return j == 0;
        }

        private boolean bottomBound() {
            return i + 1 == height;
        }
    }
}
