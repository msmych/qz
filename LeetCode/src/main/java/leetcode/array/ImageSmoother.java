package leetcode.array;

public interface ImageSmoother {

    int[][] imageSmoother(int[][] M);

    class Solution implements ImageSmoother {

        @Override
        public int[][] imageSmoother(int[][] M) {
            if (M.length == 0) return M;
            int[][] smoothed = new int[M.length][M[0].length];
            for (int i = 0; i < M.length; i++) {
                for (int j = 0; j < M[0].length; j++) {
                    int neighbors = 1;
                    int pixel = M[i][j];
                    if (i > 0 && j > 0) {
                        neighbors++;
                        pixel += M[i - 1][j - 1];
                    }
                    if (i > 0) {
                        neighbors++;
                        pixel += M[i - 1][j];
                    }
                    if (i > 0 && j < M[0].length - 1) {
                        neighbors++;
                        pixel += M[i - 1][j + 1];
                    }
                    if (j < M[0].length - 1) {
                        neighbors++;
                        pixel += M[i][j + 1];
                    }
                    if (i < M.length - 1 && j < M[0].length - 1) {
                        neighbors++;
                        pixel += M[i + 1][j + 1];
                    }
                    if (i < M.length - 1) {
                        neighbors++;
                        pixel += M[i + 1][j];
                    }
                    if (i < M.length - 1 && j > 0) {
                        neighbors++;
                        pixel += M[i + 1][j - 1];
                    }
                    if (j > 0) {
                        neighbors++;
                        pixel += M[i][j - 1];
                    }
                    smoothed[i][j] = pixel / neighbors;
                }
            }
            return smoothed;
        }
    }
}
