package leetcode.array;

import java.util.List;

public interface QueensThatCanAttackTheKing {

  List<List<Integer>> queensAttacktheKing(int[][] queens, int[] king);

  class Solution implements QueensThatCanAttackTheKing {
    @Override
    public List<List<Integer>> queensAttacktheKing(int[][] queens, int[] king) {
      return null;
    }
  }
}
