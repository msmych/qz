package leetcode.array;

public interface RotateImage {

    void rotate(int[][] matrix);

    class Solution implements RotateImage {

        @Override
        public void rotate(int[][] matrix) {
            if (matrix.length == 0)
                return;
            for (int i = 0; i < matrix.length - 1; i++) {
                for (int j = i; j < matrix.length - i - 1; j++) {
                    int temp1 = matrix[j][matrix.length - i - 1];
                    matrix[j][matrix.length - i - 1] = matrix[i][j];
                    int temp2 = matrix[matrix.length - i - 1][matrix.length - j - 1];
                    matrix[matrix.length - i - 1][matrix.length - j - 1] = temp1;
                    int temp3 = matrix[matrix.length - j - 1][i];
                    matrix[matrix.length - j - 1][i] = temp2;
                    matrix[i][j] = temp3;
                }
            }
        }
    }
}
