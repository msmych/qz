package leetcode.array;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface FindAllNumbersDisappearedInAnArray {

    List<Integer> findDisappearedNumbers(int[] nums);

    class Solution implements FindAllNumbersDisappearedInAnArray {

        @Override
        public List<Integer> findDisappearedNumbers(int[] nums) {
            Set<Integer> set = Arrays.stream(nums).boxed().collect(Collectors.toSet());
            return IntStream.rangeClosed(1, nums.length)
                .filter(num -> !set.contains(num))
                .boxed()
                .collect(Collectors.toList());
        }
    }
}
