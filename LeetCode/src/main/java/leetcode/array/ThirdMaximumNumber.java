package leetcode.array;

import java.util.stream.IntStream;

public interface ThirdMaximumNumber {

  int thirdMax(int[] nums);

  class Solution implements ThirdMaximumNumber {
    @Override
    public int thirdMax(int[] nums) {
      if (IntStream.of(nums).distinct().count() < 3) {
        return IntStream.of(nums).max().orElse(0);
      }
      int[] three = IntStream.of(nums[0], nums[1], nums[2])
        .sorted()
        .toArray();
      for (int i = 3; i < nums.length; i++) {
        if (nums[i] > three[2]) {
          three[0] = three[1];
          three[1] = three[2];
          three[2] = nums[i];
        } else if (nums[i] != three[2] && nums[i] > three[1]) {
          three[0] = three[1];
          three[1] = nums[i];
        } else if (nums[i] != three[2] && nums[i] != three[1] && nums[i] > three[0]) {
          three[0] = nums[i];
        } else if (three[1] == three[2]) {
          three[1] = three[0];
          if (nums[i] < three[0]) {
            three[0] = nums[i];
          }
        } else if (three[0] == three[1] && nums[i] < three[0]) {
          three[0] = nums[i];
        }
      }
      return three[0];
    }
  }
}
