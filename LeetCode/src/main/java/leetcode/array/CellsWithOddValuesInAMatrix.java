package leetcode.array;

public interface CellsWithOddValuesInAMatrix {

  int oddCells(int n, int m, int[][] indices);

  class Solution implements CellsWithOddValuesInAMatrix {
    @Override
    public int oddCells(int n, int m, int[][] indices) {
      int[][] matrix = new int[n][m];
      for (int[] index : indices) {
        incrementRow(index[0], matrix);
        incrementColumn(index[1], matrix);
      }
      int odds = 0;
      for (int[] row : matrix) {
        for (int i : row) {
          if (i % 2 == 1) {
            odds++;
          }
        }
      }
      return odds;
    }

    private void incrementRow(int r, int[][] matrix) {
      for (int i = 0; i < matrix[0].length; i++) {
        matrix[r][i]++;
      }
    }

    private void incrementColumn(int c, int[][] matrix) {
      for (int i = 0; i < matrix.length; i++) {
        matrix[i][c]++;
      }
    }
  }
}
