package leetcode.array;

import static java.util.Arrays.stream;

public interface FindNumbersWithEvenNumberOfDigits {

  int findNumbers(int[] nums);

  class Solution implements FindNumbersWithEvenNumberOfDigits {

    @Override
    public int findNumbers(int[] nums) {
      return (int) stream(nums)
        .mapToObj(String::valueOf)
        .map(String::length)
        .filter(n -> n % 2 == 0)
        .count();
    }

  }
}
