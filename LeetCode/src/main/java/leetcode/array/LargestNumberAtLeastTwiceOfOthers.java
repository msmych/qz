package leetcode.array;

public interface LargestNumberAtLeastTwiceOfOthers {

    int dominantIndex(int[] nums);

    class Solution implements LargestNumberAtLeastTwiceOfOthers {

        @Override
        public int dominantIndex(int[] nums) {
            int largest = 0, second = 0, index = -1;
            for (int i = 0; i < nums.length; i++) {
                int num = nums[i];
                if (num > largest) {
                    second = largest;
                    largest = num;
                    index = i;
                } else if (num > second) {
                    second = num;
                }
            }
            return largest >= 2*second ? index : -1;
        }
    }
}
