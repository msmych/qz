package leetcode.array;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public interface PascalsTriangle {

    List<List<Integer>> generate(int numRows);

    class Solution implements PascalsTriangle {

        @Override
        public List<List<Integer>> generate(int numRows) {
            List<List<Integer>> triangle = new ArrayList<>();
            if (numRows == 0) return  triangle;
            triangle.add(new ArrayList<>());
            triangle.get(0).add(1);
            for (int i = 1; i < numRows; i++) {
                triangle.add(new ArrayList<>());
                for (int j = 0; j <= i; j++) {
                    int num;
                    List<Integer> previousRow = triangle.get(i - 1);
                    if (j == 0) {
                        num = previousRow.get(0);
                    } else if (j == i) {
                        num = previousRow.get(j - 1);
                    } else {
                        num = previousRow.get(j - 1) + previousRow.get(j);
                    }
                    triangle.get(i).add(num);
                }
            }
            return triangle;
        }
    }

    class RecursiveSolution implements PascalsTriangle {

        private int numRows;

        @Override
        public List<List<Integer>> generate(int numRows) {
            this.numRows = numRows;
            return generateNext(Collections.emptyList(), 0);
        }

        private List<List<Integer>> generateNext(List<Integer> previous, int i) {
            if (i == numRows)
                return new ArrayList<>();
            List<List<Integer>> triangle = new ArrayList<>();
            List<Integer> row = new ArrayList<>();
            for (int j = 0; j <= i; j++) {
                row.add(j == 0 || j == i
                        ? 1
                        : previous.get(j - 1) + previous.get(j));
            }
            triangle.add(row);
            triangle.addAll(generateNext(row, i + 1));
            return triangle;
        }
    }
}
