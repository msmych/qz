package leetcode.array;

public interface FairCandySwap {

    int[] fairCandySwap(int[] A, int[] B);

    class Solution implements FairCandySwap {

        @Override
        public int[] fairCandySwap(int[] A, int[] B) {
            int asum = 0;
            for (int a : A)
                asum += a;
            int bsum = 0;
            for (int b : B)
                bsum += b;
            int average = (asum + bsum) / 2;
            for (int a : A) {
                if (asum - a >= average)
                    continue;
                for (int b : B) {
                    if (asum - a + b == average && bsum - b + a == average)
                        return new int[]{a, b};
                }
            }
            throw new IllegalArgumentException();
        }
    }
}
