package leetcode.array;

public interface ReplaceElementsWithGreatestElementOnRightSide {

  int[] replaceElements(int[] arr);

  class Solution implements ReplaceElementsWithGreatestElementOnRightSide {
    @Override
    public int[] replaceElements(int[] arr) {
      int max = arr[arr.length - 1];
      arr[arr.length - 1] = -1;
      for (int i = arr.length - 2; i >= 0; i--) {
        int n = arr[i];
        arr[i] = max;
        if (n > max) {
          max = n;
        }
      }
      return arr;
    }
  }
}
