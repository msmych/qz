package leetcode.array;

import java.util.Arrays;

public interface BestTimeToBuyAndSellStock {

    int maxProfit(int[] prices);

    class Solution implements BestTimeToBuyAndSellStock {

        @Override
        public int maxProfit(int[] prices) {
            if (prices.length < 2)
                return 0;
            int profit = 0, max = prices[prices.length - 1];
            for (int i = prices.length - 2; i >= 0; i--) {
                if (prices[i + 1] > max)
                    max = prices[i + 1];
                if (max - prices[i] > profit)
                    profit = max - prices[i];
            }
            return profit;
        }
    }

    class SolutionII implements BestTimeToBuyAndSellStock {

        @Override
        public int maxProfit(int[] prices) {
            if (prices.length == 0)
                return 0;
            int profit = 0, lastPrice = prices[0];
            for (int i = 1; i < prices.length; i++) {
                int price = prices[i];
                if (price > lastPrice)
                    profit += price - lastPrice;
                lastPrice = price;
            }
            return profit;
        }
    }

    class SolutionIII implements BestTimeToBuyAndSellStock {

        @Override
        public int maxProfit(int[] prices) {
            int max = maxSingleProfit(prices);
            for (int i = 2; i < prices.length - 1; i++) {
                int profit = maxSingleProfit(Arrays.copyOf(prices, i)) +
                        maxSingleProfit(Arrays.copyOfRange(prices, i, prices.length));
                if (profit > max)
                    max = profit;
            }
            return max;
        }

        private int maxSingleProfit(int[] prices) {
            if (prices.length < 2)
                return 0;
            int profit = 0, max = prices[prices.length - 1];
            for (int i = prices.length - 2; i >= 0; i--) {
                if (prices[i + 1] > max)
                    max = prices[i + 1];
                if (max - prices[i] > profit)
                    profit = max - prices[i];
            }
            return profit;
        }
    }
}
