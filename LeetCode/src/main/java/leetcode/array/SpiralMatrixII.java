package leetcode.array;

public interface SpiralMatrixII {

    int[][] generateMatrix(int n);

    class Solution implements SpiralMatrixII {

        private final int RIGHT = 0, DOWN = 1, LEFT = 2, UP = 3;

        private int direction = RIGHT, i = 0, j = 0,
                rightBorder, downBorder, leftBorder = 0, upBorder = 0;

        @Override
        public int[][] generateMatrix(int n) {
            int[][] matrix = new int[n][n];
            downBorder = n - 1;
            rightBorder = n - 1;
            for (int k = 1; k <= n * n; k++) {
                matrix[i][j] = k;
                updateIndexes();
            }
            return matrix;
        }

        private void updateIndexes() {
            switch (direction) {
                case RIGHT:
                    goRight();
                    break;
                case DOWN:
                    goDown();
                    break;
                case LEFT:
                    goLeft();
                    break;
                case UP:
                    goUp();
            }
        }

        private void goRight() {
            if (++j == rightBorder) {
                direction = DOWN;
                upBorder++;
            }
        }

        private void goDown() {
            if (++i == downBorder) {
                direction = LEFT;
                rightBorder--;
            }
        }

        private void goLeft() {
            if (--j == leftBorder) {
                direction = UP;
                downBorder--;
            }
        }

        private void goUp() {
            if (--i == upBorder) {
                direction = RIGHT;
                leftBorder++;
            }
        }
    }
}
