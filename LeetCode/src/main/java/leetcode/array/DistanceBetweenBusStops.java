package leetcode.array;

public interface DistanceBetweenBusStops {

  int distanceBetweenBusStops(int[] distance, int start, int destination);

  class Solution implements DistanceBetweenBusStops {

    private int len;

    @Override
    public int distanceBetweenBusStops(int[] distance, int start, int destination) {
      len = distance.length;
      int clockwiseDist = 0;
      int counterclockwiseDist = 0;
      for (int clockwise = start, counterclockwise = start; clockwise != destination || counterclockwise != destination;) {
        if (clockwise == destination) {
          if (counterclockwiseDist >= clockwiseDist) {
            return clockwiseDist;
          }
          counterclockwiseDist += distance[counterclockwise(counterclockwise)];
          counterclockwise = counterclockwise(counterclockwise);

        } else if (counterclockwise == destination) {
          if (clockwiseDist >= counterclockwiseDist) {
            return counterclockwiseDist;
          }
          clockwiseDist += distance[clockwise];
          clockwise = clockwise(clockwise);
        } else {
          clockwiseDist += distance[clockwise];
          counterclockwiseDist += distance[counterclockwise(counterclockwise)];
          clockwise = clockwise(clockwise);
          counterclockwise = counterclockwise(counterclockwise);
        }
      }
      return Math.min(clockwiseDist, counterclockwiseDist);
    }

    private int clockwise(int index) {
      return (index + 1) % len;
    }

    private int counterclockwise(int index) {
      if (index == 0) {
        return len - 1;
      }
      return index - 1;
    }
  }
}
