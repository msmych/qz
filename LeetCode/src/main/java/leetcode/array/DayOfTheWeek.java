package leetcode.array;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;

public interface DayOfTheWeek {

  String dayOfTheWeek(int day, int month, int year);

  class Solution implements DayOfTheWeek {
    @Override
    public String dayOfTheWeek(int day, int month, int year) {
      return LocalDate.of(year, month, day).getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.US);
    }
  }
}
