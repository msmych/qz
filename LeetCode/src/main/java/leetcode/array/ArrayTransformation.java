package leetcode.array;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public interface ArrayTransformation {

  List<Integer> transformArray(int[] arr);

  class Solution implements ArrayTransformation {
    public List<Integer> transformArray(int[] arr) {
      while (true) {
        Map<Boolean, List<Integer>> toChange = range(1, arr.length - 1)
          .filter(i -> arr[i] > arr[i - 1] && arr[i] > arr[i + 1] || arr[i] < arr[i - 1] && arr[i] < arr[i + 1])
          .boxed()
          .collect(partitioningBy(i -> arr[i] > arr[i - 1]));
        if (toChange.get(true).isEmpty() && toChange.get(false).isEmpty()) {
          break;
        }
        toChange.get(true).forEach(i -> arr[i]--);
        toChange.get(false).forEach(i -> arr[i]++);
      }
      return Arrays.stream(arr).boxed().collect(toList());
    }

  }
}

