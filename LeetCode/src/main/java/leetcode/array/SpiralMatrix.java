package leetcode.array;

import java.util.ArrayList;
import java.util.List;

public interface SpiralMatrix {

  List<Integer> spiralOrder(int[][] matrix);

  class Solution implements SpiralMatrix {

    private enum Direction {RIGHT, DOWN, LEFT, UP}

    private Direction direction = Direction.RIGHT;
    private int i = 0, j = 0;
    private int leftBorder = 0, rightBorder, upBorder = 1, downBorder;

    @Override
    public List<Integer> spiralOrder(int[][] matrix) {
      List<Integer> result = new ArrayList<>();
      int m = matrix.length;
      if (m == 0) return result;
      int n = matrix[0].length;
      downBorder = m - 1;
      rightBorder = n - 1;
      for (int index = 0; index < m * n; index++) {
        result.add(matrix[i][j]);
        updateIndexes();
      }
      return result;
    }

    private void updateIndexes() {
      switch (direction) {
        case RIGHT:
          updateRight();
          break;
        case DOWN:
          updateDown();
          break;
        case LEFT:
          updateLeft();
          break;
        case UP:
          updateUp();
      }
    }

    private void updateRight() {
      if (j == rightBorder) {
        direction = Direction.DOWN;
        i++;
        rightBorder--;
      } else {
        j++;
      }
    }

    private void updateDown() {
      if (i == downBorder) {
        direction = Direction.LEFT;
        j--;
        downBorder--;
      } else {
        i++;
      }
    }

    private void updateLeft() {
      if (j == leftBorder) {
        direction = Direction.UP;
        i--;
        leftBorder++;
      } else {
        j--;
      }
    }

    private void updateUp() {
      if (i == upBorder) {
        direction = Direction.RIGHT;
        j++;
        upBorder++;
      } else {
        i--;
      }
    }
  }
}
