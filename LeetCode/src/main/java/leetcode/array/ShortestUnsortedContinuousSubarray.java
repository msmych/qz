package leetcode.array;

import java.util.Arrays;

public interface ShortestUnsortedContinuousSubarray {

    int findUnsortedSubarray(int[] nums);

    class Solution implements ShortestUnsortedContinuousSubarray {

        @Override
        public int findUnsortedSubarray(int[] nums) {
            int[] arr = new int[nums.length];
            System.arraycopy(nums, 0, arr, 0, nums.length);
            Arrays.sort(arr);
            int left = -1;
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] != nums[i]) {
                    left = i;
                    break;
                }
            }
            if (left == -1) {
                return 0;
            }
            for (int i = arr.length - 1; i > 0; i--) {
                if (arr[i] != nums[i]) {
                    return i - left + 1;
                }
            }
            throw new IllegalArgumentException();
        }
    }

}
