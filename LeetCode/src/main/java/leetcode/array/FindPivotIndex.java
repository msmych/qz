package leetcode.array;

public interface FindPivotIndex {

    int pivotIndex(int[] nums);

    class Solution implements FindPivotIndex {

        @Override
        public int pivotIndex(int[] nums) {
            int left, right;
            for (int i = 0; i < nums.length; i++) {
                left = 0;
                for (int l = 0; l < i; l++) {
                    left += nums[l];
                }
                right = 0;
                for (int r = i + 1; r < nums.length; r++) {
                    right += nums[r];
                }
                if (left == right)
                    return i;
            }
            return -1;
        }
    }
}
