package leetcode.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface PositionsOfLargeGroups {

    List<List<Integer>> largeGroupPositions(String S);

    class Solution implements PositionsOfLargeGroups {

        @Override
        public List<List<Integer>> largeGroupPositions(String S) {
            char letter = S.charAt(0);
            int count = 1, startIndex = 0, i = 0;
            List<List<Integer>> letterPositions = new ArrayList<>();
            while (++i < S.length()) {
                char c = S.charAt(i);
                if (c == letter)
                    count++;
                else {
                    if (count >= 3)
                        letterPositions.add(Arrays.asList(startIndex, i - 1));
                    count = 1;
                    startIndex = i;
                    letter = c;
                }
            }
            if (count >= 3)
                letterPositions.add(Arrays.asList(startIndex, i - 1));
            return letterPositions;
        }
    }
}
