package leetcode.array;

import java.util.LinkedList;
import java.util.List;

public interface AddToArrayFortOfInteger {

    List<Integer> addToArrayForm(int[] A, int K);

    class Solution implements AddToArrayFortOfInteger {

        @Override
        public List<Integer> addToArrayForm(int[] A, int K) {
            List<Integer> sumList = new LinkedList<>();
            int i = A.length - 1, shift = 0;
            while (K > 0 || i >= 0) {
                int a = i >= 0 ? A[i--] : 0,
                        k = K % 10,
                        sum = a + k + shift;
                shift = sum / 10;
                sumList.add(0, sum % 10);
                K /= 10;
            }
            if (shift > 0)
                sumList.add(0, shift);
            return sumList;
        }
    }
}
