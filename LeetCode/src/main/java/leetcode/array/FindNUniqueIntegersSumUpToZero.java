package leetcode.array;

public interface FindNUniqueIntegersSumUpToZero {

  int[] sumZero(int n);

  class Solution implements FindNUniqueIntegersSumUpToZero {
    @Override
    public int[] sumZero(int n) {
      int[] arr = new int[n];
      for (int i = arr.length / 2; i > 0; i--) {
        arr[arr.length / 2 - i] = -i;
        arr[arr.length / 2 + i - (arr.length % 2 == 0 ? 1 : 0)] = i;
      }
      return arr;
    }
  }
}
