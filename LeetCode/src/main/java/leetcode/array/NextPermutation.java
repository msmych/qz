package leetcode.array;

public interface NextPermutation {

    void nextPermutation(int[] nums);

    class Solution implements NextPermutation {

        @Override
        public void nextPermutation(int[] nums) {
            int sourceIndex = -1, targetIndex = -1;
            for (int i = nums.length - 1; i > 0; i--) {
                int source = nums[i];
                for (int j = i - 1; j >= 0; j--) {
                    int target = nums[j];
                    if (source > target && (targetIndex == -1 || j > targetIndex)) {
                        sourceIndex = i;
                        targetIndex = j;
                    }
                }
            }
            if (sourceIndex != -1) {
                int temp = nums[sourceIndex];
                nums[sourceIndex] = nums[targetIndex];
                nums[targetIndex] = temp;
                sort(nums, targetIndex + 1);
            } else
                sort(nums, 0);
        }

        private void sort(int[] nums, int from) {
            for (int i = from; i < nums.length - 1; i++) {
                int minIndex = i;
                for (int j = i + 1; j < nums.length; j++) {
                    if (nums[j] < nums[minIndex])
                        minIndex = j;
                }
                int temp = nums[i];
                nums[i] = nums[minIndex];
                nums[minIndex] = temp;
            }
        }
    }
}
