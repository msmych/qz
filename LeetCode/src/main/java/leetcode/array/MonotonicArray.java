package leetcode.array;

public interface MonotonicArray {

    boolean isMonotonic(int[] A);

    class Solution implements MonotonicArray {

        private enum Direction { UP, DOWN }

        private Direction direction;

        @Override
        public boolean isMonotonic(int[] A) {
            for (int i = 0; i < A.length - 1; i++) {
                if (direction != null) {
                    if (direction == Direction.UP && A[i + 1] < A[i]
                            || direction == Direction.DOWN && A[i + 1] > A[i])
                        return false;
                } else if (A[i + 1] != A[i])
                    direction = A[i + 1] > A[i] ? Direction.UP : Direction.DOWN;
            }
            return true;
        }
    }
}
