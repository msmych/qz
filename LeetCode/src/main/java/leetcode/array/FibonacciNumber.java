package leetcode.array;

import java.util.HashMap;
import java.util.Map;

public interface FibonacciNumber {

    int fib(int N);

    class Solution implements FibonacciNumber {

        private final Map<Integer, Integer> cache = new HashMap<>();

        @Override
        public int fib(int N) {
            if (N <= 1)
                return N;
            if (cache.containsKey(N))
                return cache.get(N);
            int next = fib(N - 2) + fib(N - 1);
            cache.put(N, next);
            return next;
        }
    }
}
