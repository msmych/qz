package leetcode.array;

public interface ProductOfArrayExceptSelf {

  int[] productExceptSelf(int[] nums);

  class Solution implements ProductOfArrayExceptSelf {
    @Override
    public int[] productExceptSelf(int[] nums) {
      int[] products = new int[nums.length];
      products[0] = 1;
      for (int i = 1; i < products.length; i++) {
        products[i] = products[i - 1] * nums[i - 1];
      }
      int product = 1;
      for (int i = products.length - 2; i >= 0; i--) {
        products[i] *= nums[i + 1] * product;
        product *= nums[i + 1];
      }
      return products;
    }
  }
}
