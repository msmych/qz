package leetcode.array;

import java.util.Arrays;

public interface SumOfEvenNumbersAfterQueries {

    int[] sumEvenAfterQueries(int[] A, int[][] queries);

    class Solution implements SumOfEvenNumbersAfterQueries {

        @Override
        public int[] sumEvenAfterQueries(int[] A, int[][] queries) {
            int[] answer = new int[queries.length];
            int sum = Arrays.stream(A).filter(a -> a % 2 == 0).sum();
            for (int i = 0; i < queries.length; i++) {
                int val = queries[i][0], index = queries[i][1];
                if (Math.abs(val % 2) == 1) {
                    if (A[index] % 2 == 0)
                        sum -= A[index];
                    else
                        sum += A[index] + val;
                } else if (Math.abs(A[index] % 2) == 0)
                    sum += val;
                answer[i] = sum;
                A[index] += val;
            }
            return answer;
        }
    }
}
