package leetcode.array;

import static java.util.stream.IntStream.range;

public interface FindWinnerOnATicTacToeGame {

  String tictactoe(int[][] moves);

  class Solution implements FindWinnerOnATicTacToeGame {

    private final char[][] grid = new char[3][3];
    private char c;

    @Override
    public String tictactoe(int[][] moves) {
      c = moves.length % 2 == 1 ? 'X' : '0';
      for (int i = 0; i < moves.length; i++) {
        grid[moves[i][0]][moves[i][1]] = i % 2 == 0 ? 'X' : '0';
      }
      if (range(0, 3).anyMatch(this::isRow) || range(0, 3).anyMatch(this::isColumn) || isDiagonal()) {
        return c == 'X' ? "A" : "B";
      }
      return moves.length == 9 ? "Draw" : "Pending";
    }

    private boolean isRow(int i) {
      return grid[i][0] == c && grid[i][1] == c && grid[i][2] == c;
    }

    private boolean isColumn(int i) {
      return grid[0][i] == c && grid[1][i] == c && grid[2][i] == c;
    }

    private boolean isDiagonal() {
      return grid[0][0] == c && grid[1][1] == c && grid[2][2] == c ||
        grid[0][2] == c && grid[1][1] == c && grid[2][0] == c;
    }
  }
}
