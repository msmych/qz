package leetcode.array;

public interface DuplicateZeros {

    void duplicateZeros(int[] arr);

    class Solution implements DuplicateZeros {

        @Override
        public void duplicateZeros(int[] arr) {
            for (int i = arr.length - 2; i >= 0; i--) {
                if (arr[i] == 0)
                    if (arr.length - 1 - i >= 0)
                        System.arraycopy(arr, i, arr, i + 1, arr.length - 1 - i);
            }
        }
    }
}
