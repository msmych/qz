package leetcode.array;

public interface FlippingAnImage {

    int[][] flipAndInvertImage(int[][] A);

    class Solution implements FlippingAnImage {

        @Override
        public int[][] flipAndInvertImage(int[][] A) {
            int[][] flipped = new int[A.length][A.length];
            for (int i = 0; i < A.length; i++) {
                for (int j = 0; j < A.length; j++) {
                    if (A[i][A.length - j - 1] == 0)
                        flipped[i][j] = 1;
                }
            }
            return flipped;
        }
    }
}
