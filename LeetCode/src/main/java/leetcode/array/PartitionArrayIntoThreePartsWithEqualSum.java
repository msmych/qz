package leetcode.array;

public interface PartitionArrayIntoThreePartsWithEqualSum {

    boolean canThreePartsEqualSum(int[] A);

    class Solution implements PartitionArrayIntoThreePartsWithEqualSum {

        @Override
        public boolean canThreePartsEqualSum(int[] A) {
            return false;
        }
    }
}
