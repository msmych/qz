package leetcode.array;

import java.util.*;
import java.util.stream.IntStream;

public interface FindCommonCharacters {

    List<String> commonChars(String[] A);

    class Solution implements FindCommonCharacters {

        @Override
        public List<String> commonChars(String[] A) {
            List<Map<Character, Integer>> maps = new ArrayList<>();
            for (String a : A) {
                if (a.isEmpty())
                    return new ArrayList<>();
                Map<Character, Integer> map = new HashMap<>();
                for (char c : a.toCharArray()) {
                    map.put(c, map.getOrDefault(c, 0) + 1);
                }
                maps.add(map);
            }
            List<String> common = new ArrayList<>();
            for (Map.Entry<Character, Integer> entry : maps.stream()
                    .min(Comparator.comparingInt(Map::size))
                    .get()
                    .entrySet()) {
                char c = entry.getKey();
                int count = entry.getValue();
                for (Map<Character, Integer> map : maps) {
                    if (!map.containsKey(c))
                        count = 0;
                    else if (map.get(c) < count)
                        count = map.get(c);
                }
                IntStream.range(0, count)
                        .forEach(n -> common.add(Character.toString(c)));
            }
            return common;
        }
    }
}
