package leetcode.array;

import java.util.Arrays;

public interface MaximumAverageSubarray {

    double findMaxAverage(int[] nums, int k);

    class Solution implements MaximumAverageSubarray {

        @Override
        public double findMaxAverage(int[] nums, int k) {
            double[] sums = new double[nums.length - k + 1];
            for (int i = 0; i < k; i++) {
                sums[0] += nums[i];
            }
            for (int i = 1; i <= nums.length - k; i++) {
                sums[i] = sums[i - 1] - nums[i - 1] + nums[i + k - 1];
            }
            return Arrays.stream(sums).max().orElse(0) / k;
        }
    }
}
