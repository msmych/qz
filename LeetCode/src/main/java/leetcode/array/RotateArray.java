package leetcode.array;

public interface RotateArray {

    void rotate(int[] nums, int k);

    class Solution implements RotateArray {

        @Override
        public void rotate(int[] nums, int k) {
            int[] rotated = new int[nums.length];
            for (int i = 0; i < nums.length; i++) {
                rotated[(i + k) % nums.length] = nums[i];
            }
            System.arraycopy(rotated, 0, nums, 0, nums.length);
        }
    }

    class SinglePassSolution implements RotateArray {

        @Override
        public void rotate(int[] nums, int k) {
            if (k > nums.length)
                k = k % nums.length;
            int start = 0, count = 0;
            do {
                int i = start, temp, nextTemp = 0;
                do {
                    temp = i == start ? 0 : nextTemp;
                    int next = (i + k) % nums.length;
                    nextTemp = nums[next];
                    nums[next] = i == start ? nums[i] : temp;
                    count++;
                    i = next;
                } while (i != start);
                start++;
            } while (count < nums.length);
        }
    }

    class ReverseSolution implements RotateArray {

        @Override
        public void rotate(int[] nums, int k) {
            k = k % nums.length;
            reverse(nums, 0, nums.length - 1);
            reverse(nums, 0, k - 1);
            reverse(nums, k, nums.length - 1);
        }

        private void reverse(int[] nums, int from, int to) {
            while (from < to) {
                int temp = nums[to];
                nums[to] = nums[from];
                nums[from] = temp;
                from++;
                to--;
            }
        }
    }
}
