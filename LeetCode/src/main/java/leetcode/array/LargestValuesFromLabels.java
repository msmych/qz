package leetcode.array;

import java.util.*;

public interface LargestValuesFromLabels {

    int largestValsFromLabels(int[] values, int[] labels, int num_wanted, int use_limit);

    class Solution implements LargestValuesFromLabels {

        @Override
        public int largestValsFromLabels(int[] values, int[] labels, int num_wanted, int use_limit) {
            Queue<int[]> queue = new PriorityQueue<>((a1, a2) -> Integer.compare(a2[0], a1[0]));
            for (int i = 0; i < values.length; i++) {
                queue.offer(new int[]{values[i], labels[i]});
            }
            Map<Integer, Integer> map = new HashMap<>();
            int sum = 0;
            while (num_wanted > 0) {
                int[] arr = queue.poll();
                if (arr == null) break;
                if (!map.containsKey(arr[1]) || map.get(arr[1]) < use_limit) {
                    map.put(arr[1], map.getOrDefault(arr[1], 0) + 1);
                    sum += arr[0];
                    num_wanted--;
                }
            }
            return sum;
        }
    }
}
