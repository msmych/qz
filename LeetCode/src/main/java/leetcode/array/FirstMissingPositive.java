package leetcode.array;

public interface FirstMissingPositive {

  int firstMissingPositive(int[] nums);

  class Solution implements FirstMissingPositive {

    @Override
    public int firstMissingPositive(int[] nums) {
      int m = nums.length + 1;
      for (int i = 0; i < nums.length; i++) {
        if (nums[i] <= 0 || nums[i] > nums.length) {
          nums[i] = 0;
        }
      }
      for (int i = 0; i < nums.length; i++) {
        int previous = nums[i] % m;
        if (previous > 0) {
          nums[previous - 1] = (previous * m) + nums[previous - 1] % m;
        }
      }
      for (int i = 0; i < nums.length; i++) {
        if (nums[i] / m != i + 1) {
          return i + 1;
        }
      }
      return m;
    }
  }
}
