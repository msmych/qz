package leetcode.array;

import java.util.HashMap;
import java.util.Map;

public interface PairsOfSongsWithTotalDurationsDivisibleBy60 {

    int numPairsDivisibleBy60(int[] time);

    class Solution implements PairsOfSongsWithTotalDurationsDivisibleBy60 {

        @Override
        public int numPairsDivisibleBy60(int[] time) {
            Map<Integer, Integer> map = new HashMap<>();
            for (int t : time) {
                map.put(t % 60, map.getOrDefault(t % 60, 0) + 1);
            }
            int pairs = 0;
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                if (entry.getKey() == 0 || entry.getKey() >= 30 || !map.containsKey(60 - entry.getKey()))
                    continue;
                pairs += entry.getValue() * map.get(60 - entry.getKey());
            }
            if (map.containsKey(0))
                pairs += getPairs(map.get(0));
            if (map.containsKey(30))
                pairs += getPairs(map.get(30));
            return pairs;
        }

        private int getPairs(int n) {
            if (n < 2)
                return 0;
            int pairs = 0;
            for (int i = 1; i < n; i++) {
                pairs += i;
            }
            return pairs;
        }
    }
}
