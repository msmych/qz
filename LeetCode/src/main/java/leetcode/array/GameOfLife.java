package leetcode.array;

public interface GameOfLife {

  void gameOfLife(int[][] board);

  class Solution implements GameOfLife {

    private int[][] board;

    @Override
    public void gameOfLife(int[][] board) {
      if (board.length == 0) {
        return;
      }
      this.board = board;
      int[][] matrix = new int[board.length][board[0].length];
      for (int i = 0; i < board.length; i++) {
        for (int j = 0; j < board[0].length; j++) {
          matrix[i][j] = nextState(i, j);
        }
      }
      System.arraycopy(matrix, 0, board, 0, matrix.length);
    }

    private int nextState(int i, int j) {
      int neighbors = neighbors(i, j);
      if (board[i][j] == 1) {
        switch (neighbors) {
          case 2:
          case 3:
            return 1;
          default:
            return 0;
        }
      } else if (neighbors == 3) {
        return 1;
      } else {
        return 0;
      }
    }

    private int neighbors(int i, int j) {
      return isLive(i - 1, j - 1) + isLive(i - 1, j) + isLive(i - 1, j + 1) +
        isLive(i, j - 1) + isLive(i, j + 1) +
        isLive(i + 1, j - 1) + isLive(i + 1, j) + isLive(i + 1, j + 1);
    }

    private int isLive(int i, int j) {
      return i >= 0 && i < board.length &&
        j >= 0 && j < board[0].length &&
        board[i][j] == 1
        ? 1 : 0;
    }
  }
}
