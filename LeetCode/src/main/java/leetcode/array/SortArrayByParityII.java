package leetcode.array;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface SortArrayByParityII {

    int[] sortArrayByParityII(int[] A);

    class Solution implements SortArrayByParityII {

        @Override
        public int[] sortArrayByParityII(int[] A) {
            int[] sorted = new int[A.length];
            Map<Boolean, List<Integer>> split = Arrays.stream(A)
                    .boxed()
                    .collect(Collectors.groupingBy(a -> a % 2 == 0));
            for (int i = 0; i < sorted.length; i += 2) {
                int half = i / 2;
                sorted[i] = split.get(true).get(half);
                sorted[i + 1] = split.get(false).get(half);
            }
            return sorted;
        }
    }
}
