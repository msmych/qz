package leetcode.array;

import java.util.Arrays;

public interface HeightChecker {

    int heightChecker(int[] heights);

    class Solution implements HeightChecker {

        @Override
        public int heightChecker(int[] heights) {
            int wrong = 0;
            int[] sorted = new int[heights.length];
            System.arraycopy(heights, 0, sorted, 0, sorted.length);
            Arrays.sort(sorted);
            for (int i = 0; i < heights.length; i++) {
                if (sorted[i] != heights[i]) wrong++;
            }
            return wrong;
        }
    }
}
