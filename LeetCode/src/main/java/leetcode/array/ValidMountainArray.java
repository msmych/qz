package leetcode.array;

public interface ValidMountainArray {

    boolean validMountainArray(int[] A);

    class Solution implements ValidMountainArray {

        private enum Direction { UP, DOWN }
        private Direction direction = Direction.UP;

        @Override
        public boolean validMountainArray(int[] A) {
            if (A.length < 3)
                return false;
            boolean parted = false;
            for (int i = 0; i < A.length - 1; i++) {
                if (direction == Direction.UP) {
                    if (parted) {
                        if (A[i + 1] < A[i])
                            direction = Direction.DOWN;
                        else if (A[i + 1] == A[i])
                            return false;
                    } else if (A[i + 1] > A[i])
                        parted = true;
                    else
                        return false;
                } else if (A[i + 1] >= A[i])
                    return false;
            }
            return direction == Direction.DOWN;
        }
    }
}
