package leetcode.array;

import java.util.ArrayList;
import java.util.List;

public interface ArrayPartition {

    int arrayPairSum(int[] nums);

    class SolutionI implements ArrayPartition {

        @Override
        public int arrayPairSum(int[] nums) {
            List<Integer> numsList = toList(nums);
            numsList.sort((n1, n2) -> n1 > n2 ? 1 : (n1 == n2 ? 0 : -1));
            return maxMinSum(numsList);
        }

        private List<Integer> toList(int[] nums) {
            List<Integer> numsList = new ArrayList<>();
            for (int num : nums) {
                numsList.add(num);
            }
            return numsList;
        }

        private int maxMinSum(List<Integer> numsList) {
            int sum = 0;
            for (int i = 0; i < numsList.size(); i += 2) {
                int n1 = numsList.get(i);
                int n2 = numsList.get(i + 1);
                sum += Math.min(n1, n2);
            }
            return sum;
        }
    }
}
