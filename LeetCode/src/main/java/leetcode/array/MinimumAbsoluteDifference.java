package leetcode.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface MinimumAbsoluteDifference {

  List<List<Integer>> minimumAbsDifference(int[] arr);

  class Solution implements MinimumAbsoluteDifference {
    @Override
    public List<List<Integer>> minimumAbsDifference(int[] arr) {
      Arrays.sort(arr);
      int minDiff = Integer.MAX_VALUE;
      for (int i = 0; i < arr.length - 1; i++) {
        if (arr[i + 1] - arr[i] < minDiff) {
          minDiff = arr[i + 1] - arr[i];
        }
      }
      List<List<Integer>> diffs = new ArrayList<>();
      for (int i = 0; i < arr.length - 1; i++) {
        for (int j = i + 1; j < arr.length; j++) {
          if (arr[j] - arr[i] >= minDiff) {
            if (arr[j] - arr[i] == minDiff) {
              diffs.add(Arrays.asList(arr[i], arr[j]));
            }
            break;
          }
        }
      }
      return diffs;
    }
  }
}
