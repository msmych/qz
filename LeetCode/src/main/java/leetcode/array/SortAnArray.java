package leetcode.array;

import java.util.Arrays;

public interface SortAnArray {

    int[] sortArray(int[] nums);

    class Solution implements SortAnArray {

        @Override
        public int[] sortArray(int[] nums) {
            if (nums.length <= 1) return nums;
            int pivot = nums.length / 2;
            int[] left = sortArray(Arrays.copyOfRange(nums, 0, pivot));
            int[] right = sortArray(Arrays.copyOfRange(nums, pivot, nums.length));
            return merge(left, right);
        }

        private int[] merge(int[] left, int[] right) {
            int[] sorted = new int[left.length + right.length];
            int leftIndex = 0;
            int rightIndex = 0;
            int index = 0;
            while (leftIndex < left.length && rightIndex < right.length) {
                if (left[leftIndex] < right[rightIndex])
                    sorted[index++] = left[leftIndex++];
                else
                    sorted[index++] = right[rightIndex++];
            }
            while (leftIndex < left.length) {
                sorted[index++] = left[leftIndex++];
            }
            while (rightIndex < right.length) {
                sorted[index++] = right[rightIndex++];
            }
            return sorted;
        }
    }
}
