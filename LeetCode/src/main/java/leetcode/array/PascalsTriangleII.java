package leetcode.array;

import java.util.ArrayList;
import java.util.List;

public interface PascalsTriangleII {

    List<Integer> getRow(int rowIndex);

    class Solution implements PascalsTriangleII {

        @Override
        public List<Integer> getRow(int rowIndex) {
            List<Integer> previous = new ArrayList<>();
            previous.add(1);
            List<Integer> row = previous;
            for (int i = 1; i <= rowIndex; i++) {
                row = new ArrayList<>();
                for (int j = 0; j <= i; j++) {
                    int sum;
                    if (j == 0) {
                        sum = previous.get(0);
                    } else if (j == i) {
                        sum = previous.get(j - 1);
                    } else {
                        sum = previous.get(j - 1) + previous.get(j);
                    }
                    row.add(sum);
                }
                previous = row;
            }
            return row;
        }
    }

    class RecursiveSolution implements PascalsTriangleII {

        @Override
        public List<Integer> getRow(int rowIndex) {
            return nextRow(new ArrayList<>(), rowIndex);
        }

        private List<Integer> nextRow(List<Integer> previous, int rowIndex) {
            if (rowIndex < 0)
                return previous;
            List<Integer> row = new ArrayList<>();
            for (int i = 0; i <= previous.size(); i++) {
                row.add(i == 0 || i == previous.size()
                        ? 1
                        : previous.get(i - 1) + previous.get(i));
            }
            return nextRow(row, rowIndex - 1);
        }
    }
}
