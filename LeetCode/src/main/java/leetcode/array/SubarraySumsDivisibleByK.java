package leetcode.array;

public interface SubarraySumsDivisibleByK {

    int subarraysDivByK(int[] A, int K);

    class Solution implements SubarraySumsDivisibleByK {

        @Override
        public int subarraysDivByK(int[] A, int K) {
            int[] p = new int[A.length + 1];
            for (int i = 0; i < A.length; i++)
                p[i + 1] = p[i] + A[i];
            int[] c = new int[K];
            for (int x : p)
                c[(x % K + K) % K]++;
            int count = 0;
            for (int v : c)
                count += v * (v - 1) / 2;
            return count;
        }
    }
}
