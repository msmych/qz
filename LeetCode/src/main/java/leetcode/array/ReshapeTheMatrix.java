package leetcode.array;

public interface ReshapeTheMatrix {

    int[][] matrixReshape(int[][] nums, int r, int c);

    class Solution implements ReshapeTheMatrix {
        @Override
        public int[][] matrixReshape(int[][] nums, int r, int c) {
            if (nums.length == 0 || r * c != nums.length * nums[0].length) {
                return nums;
            }
            int[][] matrix = new int[r][c];
            int i1 = 0, i2 = 0, j1 = 0, j2 = 0;
            while (true) {
                matrix[i2][j2] = nums[i1][j1];
                if (i2 == r - 1 && j2 == c - 1) {
                    break;
                }
                if (j1 == nums[0].length - 1) {
                    i1++;
                    j1 = 0;
                } else {
                    j1++;
                }
                if (j2 == c - 1) {
                    i2++;
                    j2 = 0;
                } else {
                    j2++;
                }
            }
            return matrix;
        }
    }
}
