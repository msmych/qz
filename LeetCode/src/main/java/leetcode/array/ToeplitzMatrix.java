package leetcode.array;

public interface ToeplitzMatrix {

    boolean isToeplitzMatrix(int[][] matrix);

    class Solution implements ToeplitzMatrix {

        @Override
        public boolean isToeplitzMatrix(int[][] matrix) {
            if (matrix.length == 0 || matrix[0].length == 0)
                return true;
            for (int i = 0; i < matrix.length - 1; i++) {
                int n = matrix[i][0];
                for (int j = 1; i + j < matrix.length && j < matrix[0].length; j++) {
                    if (matrix[i + j][j] != n)
                        return false;
                }
            }
            for (int j = 1; j < matrix[0].length - 1; j++) {
                int n = matrix[0][j];
                for (int i = 1; i < matrix.length && j + i < matrix[0].length; i++) {
                    if (matrix[i][j + i] != n)
                        return false;
                }
            }
            return true;
        }
    }
}
