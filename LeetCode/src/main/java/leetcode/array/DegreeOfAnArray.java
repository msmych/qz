package leetcode.array;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public interface DegreeOfAnArray {

    int findShortestSubArray(int[] nums);

    class Solution implements DegreeOfAnArray {

        private class Interval {
            int n, i, j;

            Interval(int n, int i, int j) {
                this.n = n;
                this.i = i;
                this.j = j;
            }
        }

        @Override
        public int findShortestSubArray(int[] nums) {
            Map<Integer, Interval> map = new HashMap<>();
            for (int i = 0; i < nums.length; i++) {
                if (map.containsKey(nums[i])) {
                    Interval interval = map.get(nums[i]);
                    interval.n++;
                    interval.j = i;
                } else
                    map.put(nums[i], new Interval(1, i, i));
            }
            return map.entrySet().stream()
                    .filter(e -> e.getValue().n == map.entrySet().stream()
                            .max(Comparator.comparingInt(entry -> entry.getValue().n))
                            .map(Map.Entry::getValue)
                            .map(interval -> interval.n)
                            .get())
                    .min(Comparator.comparingInt(entry -> entry.getValue().j - entry.getValue().i))
                    .map(Map.Entry::getValue)
                    .map(interval -> interval.j - interval.i + 1)
                    .get();
        }
    }
}
