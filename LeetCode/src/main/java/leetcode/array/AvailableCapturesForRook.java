package leetcode.array;

public interface AvailableCapturesForRook {

    int numRookCaptures(char[][] board);

    class Solution implements AvailableCapturesForRook {

        @Override
        public int numRookCaptures(char[][] board) {
            int captures = 0;
            int[] rookPosition = getRookPosition(board);
            for (int i = rookPosition[0] - 1; i >= 0; i--) {
                char c = board[i][rookPosition[1]];
                if (c == 'p') {
                    captures++;
                    break;
                }
                else if (c == 'B')
                    break;
            }
            for (int j = rookPosition[1] + 1; j < board[0].length; j++) {
                char c = board[rookPosition[0]][j];
                if (c == 'p') {
                    captures++;
                    break;
                }
                else if (c == 'B')
                    break;
            }
            for (int i = rookPosition[0] + 1; i < board.length; i++) {
                char c = board[i][rookPosition[1]];
                if (c == 'p') {
                    captures++;
                    break;
                }
                else if (c == 'B')
                    break;
            }
            for (int j = rookPosition[1] - 1; j >= 0; j--) {
                char c = board[rookPosition[0]][j];
                if (c == 'p') {
                    captures++;
                    break;
                }
                else if (c == 'B')
                    break;
            }
            return captures;
        }

        private int[] getRookPosition(char[][] board) {
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[0].length; j++) {
                    if (board[i][j] == 'R')
                        return new int[]{i, j};
                }
            }
            throw new IllegalArgumentException();
        }
    }
}
