package leetcode.array;

import java.util.Arrays;

public interface Bit1AndBit2Characters {

    boolean isOneBitCharacter(int[] bits);

    class Solution implements Bit1AndBit2Characters {

        @Override
        public boolean isOneBitCharacter(int[] bits) {
            if (bits.length == 1)
                return true;
            if (bits.length == 2)
                return bits[0] == 0;
            return isOneBitCharacter(Arrays.copyOfRange(bits, bits[0] == 0 ? 1 : 2, bits.length));
        }
    }
}
