package leetcode.array;

public interface MaxConsecutiveOnes {

    int findMaxConsecutiveOnes(int[] nums);

    class Solution implements MaxConsecutiveOnes {

        private int max = 0;
        private int m = 0;

        @Override
        public int findMaxConsecutiveOnes(int[] nums) {
            for (int num : nums) {
                if (num == 1) {
                    m++;
                } else {
                    updateMax();
                    m = 0;
                }
            }
            updateMax();
            return max;
        }

        private void updateMax() {
            if (m > max) {
                max = m;
            }
        }
    }
}
