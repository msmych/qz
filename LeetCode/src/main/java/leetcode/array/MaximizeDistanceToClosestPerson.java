package leetcode.array;

public interface MaximizeDistanceToClosestPerson {

    int maxDistToClosest(int[] seats);

     class Solution implements MaximizeDistanceToClosestPerson {

         @Override
         public int maxDistToClosest(int[] seats) {
             int max = 0, lastOccupiedIndex = -1;
             for (int i = 0; i < seats.length; i++) {
                 if (seats[i] == 1) {
                     int distance = lastOccupiedIndex == -1
                             ? i
                             : (i - lastOccupiedIndex) / 2;
                     if (distance > max)
                         max = distance;
                     lastOccupiedIndex = i;
                 }
             }
             int distance = (seats.length - lastOccupiedIndex - 1);
             if (distance > max)
                 max = distance;
             return max;
         }
     }
}
