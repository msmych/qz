package leetcode.array;

import java.util.HashMap;
import java.util.Map;

public interface ElementAppearingMoreThan25pcInSortedArray {

  int findSpecialInteger(int[] arr);

  class Solution implements ElementAppearingMoreThan25pcInSortedArray {
    @Override
    public int findSpecialInteger(int[] arr) {
      Map<Integer, Integer> occurrences = new HashMap<>();
      for (int n : arr) {
        occurrences.merge(n, 1, Integer::sum);
        if (occurrences.get(n) > arr.length / 4) {
          return n;
        }
      }
      throw new IllegalArgumentException();
    }
  }
}
