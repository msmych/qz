package leetcode.array;

import java.util.ArrayList;
import java.util.List;

public interface BinaryPrefixDivisibleBy5 {

    List<Boolean> prefixesDivBy5(int[] A);

    class Solution implements BinaryPrefixDivisibleBy5 {

        @Override
        public List<Boolean> prefixesDivBy5(int[] A) {
            List<Boolean> divisible = new ArrayList<>();
            int n = 0;
            for (int value : A) {
                n <<= 1;
                if (value == 1)
                    n += 1;
                divisible.add(n % 5 == 0);
                n %= 5;
            }
            return divisible;
        }
    }
}
