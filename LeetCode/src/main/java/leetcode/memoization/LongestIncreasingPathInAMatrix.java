package leetcode.memoization;

public interface LongestIncreasingPathInAMatrix {

  int longestIncreasingPath(int[][] matrix);

  class Solution implements LongestIncreasingPathInAMatrix {

    private int[][] matrix;

    @Override
    public int longestIncreasingPath(int[][] matrix) {
      if (matrix.length == 0) {
        return 0;
      }
      this.matrix = matrix;
      int max = 0;
      int[][] cache = new int[matrix.length][matrix[0].length];
      for (int i = 0; i < matrix.length; i++) {
        for (int j = 0; j < matrix[0].length; j++) {
          int length = goFromCell(i, j, cache, null);
          if (length > max) {
            max = length;
          }
        }
      }
      return max;
    }

    private int goFromCell(int i, int j, int[][] cache, Integer previous) {
      if (i < 0 || i >= matrix.length || j < 0 || j >= matrix[0].length || (previous != null && matrix[i][j] >= previous)) {
        return 0;
      }
      if (cache[i][j] > 0) {
        return cache[i][j];
      }
      int val = matrix[i][j];
      int max = 0;
      int up = goFromCell(i - 1, j, cache, val);
      if (up > max) {
        max = up;
      }
      int down = goFromCell(i + 1, j, cache, val);
      if (down > max) {
        max = down;
      }
      int left = goFromCell(i, j - 1, cache, val);
      if (left > max) {
        max = left;
      }
      int right = goFromCell(i, j + 1, cache, val);
      if (right > max) {
        max = right;
      }
      cache[i][j] = ++max;
      return max;
    }
  }
}
