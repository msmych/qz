package leetcode.topological_sort;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public interface CourseSchedule {

  boolean canFinish(int numCourses, int[][] prerequisites);

  class Solution implements CourseSchedule {

    private final Map<Integer, Set<Integer>> courses = new HashMap<>();

    @Override
    public boolean canFinish(int numCourses, int[][] prerequisites) {
      for (int[] prerequisite : prerequisites) {
        courses.putIfAbsent(prerequisite[0], new HashSet<>());
        courses.putIfAbsent(prerequisite[1], new HashSet<>());
        courses.get(prerequisite[1]).add(prerequisite[0]);
      }
      Set<Integer> visited = new HashSet<>();
      while (!courses.values().stream().allMatch(Set::isEmpty)) {
        if (courses.entrySet().stream()
          .filter(e -> !visited.contains(e.getKey()))
          .map(Map.Entry::getValue)
          .noneMatch(Set::isEmpty))
          return false;
        int prerequisite = courses.entrySet().stream()
          .filter(e -> !visited.contains(e.getKey()))
          .filter(e -> e.getValue().isEmpty())
          .findAny()
          .map(Map.Entry::getKey)
          .get();
        for (int i : courses.entrySet().stream()
          .filter(e -> e.getValue().contains(prerequisite))
          .map(Map.Entry::getKey)
          .collect(Collectors.toList())) {
          courses.get(i).remove(prerequisite);
        }
        visited.add(prerequisite);
      }
      return true;
    }
  }
}
