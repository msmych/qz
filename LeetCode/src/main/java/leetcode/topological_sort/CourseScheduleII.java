package leetcode.topological_sort;

import java.util.*;

public interface CourseScheduleII {

  int[] findOrder(int numCourses, int[][] prerequisites);

  class Solution implements CourseScheduleII {
    @Override
    public int[] findOrder(int numCourses, int[][] prerequisites) {
      Map<Integer, List<Integer>> adjustment = new HashMap<>();
      int[] indegree = new int[numCourses];
      int[] topologicalOrder = new int[numCourses];
      for (int[] prerequisite : prerequisites) {
        int dest = prerequisite[0];
        int source = prerequisite[1];
        List<Integer> list = adjustment.getOrDefault(source, new ArrayList<>());
        list.add(dest);
        adjustment.put(source, list);
        indegree[dest]++;
      }
      Queue<Integer> queue = new LinkedList<>();
      for (int i = 0; i < numCourses; i++) {
        if (indegree[i] == 0) {
          queue.add(i);
        }
      }
      int i = 0;
      while (!queue.isEmpty()) {
        int node = queue.poll();
        topologicalOrder[i++] = node;
        if (adjustment.containsKey(node)) {
          for (int neighbor : adjustment.get(node)) {
            indegree[neighbor]--;
            if (indegree[neighbor] == 0) {
              queue.offer(neighbor);
            }
          }
        }
      }
      return i == numCourses ? topologicalOrder : new int[0];
    }
  }
}
