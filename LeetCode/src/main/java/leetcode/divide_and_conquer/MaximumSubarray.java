package leetcode.divide_and_conquer;

public interface MaximumSubarray {

    int maxSubArray(int[] nums);

    class Solution implements MaximumSubarray {

        @Override
        public int maxSubArray(int[] nums) {
            if (nums.length == 0) return 0;
            int sum = nums[0];
            int max = nums[0];
            for (int i = 1; i < nums.length; i++) {
                sum = Math.max(nums[i], sum + nums[i]);
                if (sum > max)
                    max = sum;
            }
            return max;
        }
    }
}
