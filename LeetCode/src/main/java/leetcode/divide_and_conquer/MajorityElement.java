package leetcode.divide_and_conquer;

import java.util.HashMap;
import java.util.Map;

public interface MajorityElement {

    int majorityElement(int[] nums);

    class Solution implements MajorityElement {

        @Override
        public int majorityElement(int[] nums) {
            Map<Integer, Integer> map = new HashMap<>();
            for (int num : nums) {
                map.put(num, map.getOrDefault(num, 0) + 1);
            }
            return map.entrySet().stream()
                    .filter(e -> e.getValue() > nums.length / 2)
                    .findAny()
                    .map(Map.Entry::getKey)
                    .orElseThrow(IllegalArgumentException::new);
        }
    }
}
