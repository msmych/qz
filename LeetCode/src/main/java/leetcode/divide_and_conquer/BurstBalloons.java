package leetcode.divide_and_conquer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Math.max;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;

public interface BurstBalloons {

  int maxCoins(int[] nums);

  class Solution implements BurstBalloons {

    private final Map<List<Integer>, Integer> cache = new HashMap<>();

    @Override
    public int maxCoins(int[] nums) {
      return nextMax(stream(nums).boxed().collect(toList()));
    }

    private int nextMax(List<Integer> balloons) {
      if (balloons.isEmpty()) {
        return 0;
      } else if (balloons.size() == 1) {
        return balloons.get(0);
      } else if (balloons.size() == 2) {
        return balloons.get(0) * balloons.get(1) + max(balloons.get(0), balloons.get(1));
      }
      if (cache.containsKey(balloons)) {
        return cache.get(balloons);
      }
      int max = 0;
      for (int i = 0; i < balloons.size(); i++) {
        int coins = balloons.get(i);
        if (i > 0) {
          coins *= balloons.get(i - 1);
        }
        if (i < balloons.size() - 1) {
          coins *= balloons.get(i + 1);
        }
        coins += nextMax(concat(
          balloons.subList(0, i).stream(),
          balloons.subList(i + 1, balloons.size()).stream())
          .collect(toList()));
        if (coins > max) {
          max = coins;
        }
      }
      cache.put(balloons, max);
      return max;
    }
  }
}
