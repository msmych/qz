package leetcode.divide_and_conquer;

public interface SearchA2DMatrixII {

  boolean searchMatrix(int[][] matrix, int target);

  class Solution implements SearchA2DMatrixII {

    private int[][] matrix;
    private int target;

    @Override
    public boolean searchMatrix(int[][] matrix, int target) {
      if (matrix.length == 0) return false;
      this.matrix = matrix;
      this.target = target;
      return searchNext(0, 0, matrix.length - 1, matrix[0].length - 1);
    }

    private boolean searchNext(int i1, int j1, int i2, int j2) {
      if (i1 == i2) return searchInRow(i1, j1, j2);
      if (j1 == j2) return searchInColumn(j1, i1, i2);
      int col = (j2 + j1) / 2;
      for (int i = i1; i <= i2; i++) {
        if (matrix[i][col] >= target) {
          if (i == i2 && col == j1) {
            return matrix[i][col] == target ||
              searchNext(i1, col + 1, i2, j2);
          }
          return searchNext(i, j1, i2, col) || searchNext(i1, col, i, j2);
        }
      }
      return searchNext(i1, col + 1, i2, j2);
    }

    private boolean searchInRow(int i, int j1, int j2) {
      while (j1 <= j2) {
        int mid = (j1 + j2) / 2;
        if (matrix[i][mid] == target) return true;
        else if (matrix[i][mid] < target) j1++;
        else j2--;
      }
      return false;
    }

    private boolean searchInColumn(int j, int i1, int i2) {
      while (i1 <= i2) {
        int mid = (i1 + i2) / 2;
        if (matrix[mid][j] == target) return true;
        else if (matrix[mid][j] < target) i1++;
        else i2--;
      }
      return false;
    }
  }
}
