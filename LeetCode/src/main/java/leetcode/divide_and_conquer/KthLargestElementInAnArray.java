package leetcode.divide_and_conquer;

import java.util.Arrays;

public interface KthLargestElementInAnArray {

  int findKthLargest(int[] nums, int k);

  class Solution implements KthLargestElementInAnArray {

    @Override
    public int findKthLargest(int[] nums, int k) {
      Arrays.sort(nums);
      return nums[nums.length - k];
    }
  }
}
