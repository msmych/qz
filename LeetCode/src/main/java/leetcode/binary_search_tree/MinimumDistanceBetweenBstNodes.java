package leetcode.binary_search_tree;

import leetcode.tree.TreeNode;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public interface MinimumDistanceBetweenBstNodes {

    int minDiffInBST(TreeNode root);

    class Solution implements MinimumDistanceBetweenBstNodes {

        private final Set<Integer> vals = new HashSet<>();

        @Override
        public int minDiffInBST(TreeNode root) {
            traverse(root);
            return vals.stream()
                    .map(val -> vals.stream()
                            .filter(v -> v != val)
                            .map(v -> Math.abs(v - val))
                            .min(Comparator.naturalOrder())
                            .get())
                    .min(Comparator.naturalOrder())
                    .get();
        }

        private void traverse(TreeNode node) {
            if (node == null)
                return;
            vals.add(node.val);
            traverse(node.left);
            traverse(node.right);
        }
    }
}
