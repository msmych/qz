package leetcode.binary_search_tree;

import leetcode.tree.TreeNode;

public interface BinarySearchTreeToGreaterSumTree {

    TreeNode bstToGst(TreeNode root);

    class Solution implements BinarySearchTreeToGreaterSumTree {

        private int sum;

        @Override
        public TreeNode bstToGst(TreeNode root) {
            sum = getSum(root);
            toGst(root, 0);
            return root;
        }

        private int getSum(TreeNode node) {
            if (node == null) return 0;
            return node.val + getSum(node.left) + getSum(node.right);
        }

        private int toGst(TreeNode node, int delta) {
            if (node == null) return 0;
            int val = node.val;
            int left = toGst(node.left, delta);
            node.val = sum - delta - left;
            return toGst(node.right, delta + left + val) + left + val;
        }
    }
}
