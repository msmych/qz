package leetcode.sort;

import java.util.stream.IntStream;

public interface WiggleSortII {

  void wiggleSort(int[] nums);

  class Solution implements WiggleSortII {

    private int[] nums;

    @Override
    public void wiggleSort(int[] nums) {
      if (nums.length < 2) {
        return;
      }
      this.nums = nums;
      int mid = IntStream.of(nums).sorted().limit(nums.length / 2 + 1).max().getAsInt();
      for (int i = 0, left = 0, right = nums.length - 1; i <= right;) {
        int index = index(i);
        if (nums[index] > mid) {
          swap(index(left++), index(i++));
        } else if (nums[index] < mid) {
          swap(index, index(right--));
        } else {
          i++;
        }
      }
    }

    private int index(int i) {
      return (2 * i + 1) % (nums.length | 1);
    }

    private void swap(int i, int j) {
      int num = nums[i];
      nums[i] = nums[j];
      nums[j] = num;
    }
  }
}
