package leetcode.sort;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public interface RelativeRanks {

    String[] findRelativeRanks(int[] nums);

    class Solution implements RelativeRanks {
        @Override
        public String[] findRelativeRanks(int[] nums) {
            int[] sorted = new int[nums.length];
            System.arraycopy(nums, 0, sorted, 0, nums.length);
            Arrays.sort(sorted);
            Map<Integer, String> map = new HashMap<>();
            for (int i = sorted.length - 1; i >= 0; i--) {
                String rank;
                switch (sorted.length - i) {
                    case 1:
                        rank = "Gold Medal";
                        break;
                    case 2:
                        rank = "Silver Medal";
                        break;
                    case 3:
                        rank = "Bronze Medal";
                        break;
                    default:
                        rank = String.valueOf(sorted.length - i);
                }
                map.put(sorted[i], rank);
            }
            String[] ranks = new String[nums.length];
            for (int i = 0; i < nums.length; i++) {
                ranks[i] = map.get(nums[i]);
            }
            return ranks;
        }
    }
}
