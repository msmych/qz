package leetcode.sort;

import java.util.ArrayList;
import java.util.List;

public interface CarPooling {

    boolean carPooling(int[][] trips, int capacity);

    class Solution implements CarPooling {

        private class Position {
            int position;
            int passengers;
            boolean isPickUp;

            Position(int position, int passengers, boolean isPickUp) {
                this.position = position;
                this.passengers = passengers;
                this.isPickUp = isPickUp;
            }
        }

        @Override
        public boolean carPooling(int[][] trips, int capacity) {
            List<Position> positions = new ArrayList<>();
            for (int[] trip : trips) {
                positions.add(new Position(trip[1], trip[0], true));
                positions.add(new Position(trip[2], trip[0], false));
            }
            positions.sort((p1, p2) -> {
                if (p1.position != p2.position)
                    return p1.position > p2.position ? 1 : -1;
                if (!p1.isPickUp && p2.isPickUp)
                    return -1;
                if (p1.isPickUp && !p2.isPickUp)
                    return 1;
                return 0;
            });
            int passengers = 0;
            for (Position p : positions) {
                if (p.isPickUp) passengers += p.passengers;
                else passengers -= p.passengers;
                if (passengers > capacity) return false;
            }
            return true;
        }
    }
}
