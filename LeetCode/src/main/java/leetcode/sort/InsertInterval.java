package leetcode.sort;

import leetcode.Interval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public interface InsertInterval {

    List<Interval> insert(List<Interval> intervals, Interval newInterval);

    class Solution implements InsertInterval {

        @Override
        public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
            if (intervals.isEmpty())
                return Collections.singletonList(newInterval);
            int start = -1;
            if (intervals.get(0).start > newInterval.start)
                start = newInterval.start;
            List<Interval> nextIntervals = new ArrayList<>();
            boolean merged = false;
            for (Interval interval : intervals) {
                if (!merged && interval.start > newInterval.end) {
                    if (start == -1)
                        start = newInterval.start;
                    nextIntervals.add(new Interval(start, newInterval.end));
                    nextIntervals.add(interval);
                    merged = true;
                    continue;
                }
                if (interval.end < newInterval.start
                        || interval.start > newInterval.end) {
                    nextIntervals.add(interval);
                    continue;
                }
                if (start == -1)
                    start = interval.start > newInterval.start
                            ? newInterval.start
                            : interval.start;
                if (interval.end >= newInterval.end) {
                    nextIntervals.add(new Interval(start, interval.end));
                    merged = true;
                }
            }
            if (!merged)
                nextIntervals.add(start == -1 ? newInterval : new Interval(start, newInterval.end));
            return nextIntervals;
        }
    }
}
