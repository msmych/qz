package leetcode.sort;

import leetcode.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

import static java.util.Comparator.naturalOrder;

public interface AllElementsInTwoBinarySearchTrees {

  List<Integer> getAllElements(TreeNode root1, TreeNode root2);

  class Solution implements AllElementsInTwoBinarySearchTrees {

    private List<Integer> vals = new ArrayList<>();

    @Override
    public List<Integer> getAllElements(TreeNode root1, TreeNode root2) {
      traverse(root1);
      traverse(root2);
      vals.sort(naturalOrder());
      return vals;
    }

    private void traverse(TreeNode node) {
      if (node == null) {
        return;
      }
      vals.add(node.val);
      traverse(node.left);
      traverse(node.right);
    }
  }
}
