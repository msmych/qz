package leetcode.sort;

import leetcode.linked_list.ListNode;

import java.util.PriorityQueue;
import java.util.Queue;

import static java.util.Comparator.naturalOrder;

public interface SortList {

  ListNode sortList(ListNode head);

  class Solution implements SortList {
    @Override
    public ListNode sortList(ListNode head) {
      Queue<Integer> queue = new PriorityQueue<>(naturalOrder());
      while (head != null) {
        queue.offer(head.val);
        head = head.next;
      }
      ListNode dummy = new ListNode(0);
      ListNode node = dummy;
      while (!queue.isEmpty()) {
        node.next = new ListNode(queue.poll());
        node = node.next;
      }
      return dummy.next;
    }
  }
}
