package leetcode.sort;

import leetcode.Interval;

import java.util.ArrayList;
import java.util.List;

public interface MergeIntervals {

  List<Interval> merge(List<Interval> intervals);

  class Solution implements MergeIntervals {

    @Override
    public List<Interval> merge(List<Interval> intervals) {
      intervals.sort((i1, i2) -> {
        if (i1.start != i2.start) {
          return Integer.compare(i1.start, i2.start);
        }
        return Integer.compare(i1.end, i2.end);
      });
      List<Interval> merged = new ArrayList<>();
      int start = -1, end = -1;
      for (Interval interval : intervals) {
        if (interval.start > end) {
          if (end > -1) {
            merged.add(new Interval(start, end));
          }
          start = interval.start;
          end = interval.end;
        } else if (interval.end > end) {
          end = interval.end;
        }
      }
      if (!merged.isEmpty()) {
        Interval last = merged.get(merged.size() - 1);
        if (last.start != start) {
          merged.add(new Interval(start, end));
        } else if (last.end < end) {
          merged.get(merged.size() - 1).end = end;
        }
      } else if (end > -1) {
        merged.add(new Interval(start, end));
      }
      return merged;
    }
  }
}
