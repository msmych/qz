package leetcode.sort;

import java.util.*;
import java.util.stream.Collectors;

public interface RelativeSortArray {

    int[] relativeSortArray(int[] arr1, int[] arr2);

    class Solution implements RelativeSortArray {

        @Override
        public int[] relativeSortArray(int[] arr1, int[] arr2) {
            int[] sorted = new int[arr1.length];
            Map<Integer, Integer> map = new HashMap<>();
            for (int n : arr1) {
                map.put(n, map.getOrDefault(n, 0) + 1);
            }
            int i = 0;
            for (int n : arr2) {
                for (int j = 0; j < map.get(n); j++, i++) {
                    sorted[i] = n;
                }
            }
            Set<Integer> set2 = Arrays.stream(arr2).boxed().collect(Collectors.toSet());
            List<Integer> remaining = Arrays.stream(arr1)
                    .filter(n -> !set2.contains(n))
                    .sorted()
                    .boxed()
                    .collect(Collectors.toList());
            for (int j = 0; j < remaining.size(); j++) {
                sorted[i + j] = remaining.get(j);
            }
            return sorted;
        }
    }
}
