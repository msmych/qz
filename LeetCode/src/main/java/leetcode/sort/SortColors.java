package leetcode.sort;

public interface SortColors {

  void sortColors(int[] nums);

  class Solution implements SortColors {

    @Override
    public void sortColors(int[] nums) {
      int[] colors = new int[3];
      for (int num : nums) {
        colors[num]++;
      }
      int i = 0;
      for (int color = 0; color < colors.length; color++) {
        for (int j = colors[color]; j > 0; j--) {
          nums[i++] = color;
        }
      }
    }
  }
}
