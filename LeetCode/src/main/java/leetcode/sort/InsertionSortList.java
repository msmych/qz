package leetcode.sort;

import leetcode.linked_list.ListNode;

public interface InsertionSortList {

    ListNode insertionSortList(ListNode head);

    class Solution implements InsertionSortList {

        @Override
        public ListNode insertionSortList(ListNode head) {
            return null;
        }
    }
}
