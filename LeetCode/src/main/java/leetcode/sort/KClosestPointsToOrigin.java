package leetcode.sort;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface KClosestPointsToOrigin {

    int[][] kClosest(int[][] points, int K);

    class Solution implements KClosestPointsToOrigin {

        @Override
        public int[][] kClosest(int[][] points, int K) {
            List<int[]> closestList = Stream.of(points)
                    .sorted((p1, p2) -> (p1[0]*p1[0] + p1[1]*p1[1]) > (p2[0]*p2[0] + p2[1]*p2[1]) ? 1 : -1)
                    .limit(K)
                    .collect(Collectors.toList());
            int[][] closest = new int[K][2];
            for (int i = 0; i < closestList.size(); i++)
                closest[i] = closestList.get(i);
            return closest;
        }
    }
}
