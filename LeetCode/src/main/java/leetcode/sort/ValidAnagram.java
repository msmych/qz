package leetcode.sort;

import java.util.HashMap;
import java.util.Map;

public interface ValidAnagram {

    boolean isAnagram(String s, String t);

    class Solution implements ValidAnagram {

        @Override
        public boolean isAnagram(String s, String t) {
            if (s.length() != t.length())
                return false;
            return getMap(s).equals(getMap(t));
        }

        private Map<Character, Integer> getMap(String s) {
            Map<Character, Integer> map = new HashMap<>();
            for (char c : s.toCharArray()) {
                map.put(c, map.getOrDefault(c, 0) + 1);
            }
            return map;
        }
    }
}
