package leetcode.sort;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public interface MatrixCellsInDistanceOrder {

    int[][] allCellsDistOrder(int R, int C, int r0, int c0);

    class Solution implements MatrixCellsInDistanceOrder {

        @Override
        public int[][] allCellsDistOrder(int R, int C, int r0, int c0) {
            Queue<int[]> queue = new PriorityQueue<>(
                    Comparator.comparingInt(c -> Math.abs(c[0] - r0) + Math.abs(c[1] - c0)));
            for (int i = 0; i < R; i++) {
                for (int j = 0; j < C; j++) {
                    queue.offer(new int[]{i, j});
                }
            }
            return toArray(queue);
        }

        private int[][] toArray(Queue<int[]> queue) {
            int[][] arr = new int[queue.size()][2];
            for (int i = 0; !queue.isEmpty(); i++) {
                arr[i] = queue.poll();
            }
            return arr;
        }
    }
}
