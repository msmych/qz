package leetcode.sort;

public interface SquaresOfASortedArray {

    int[] sortedSquares(int[] A);

    class Solution implements SquaresOfASortedArray {

        @Override
        public int[] sortedSquares(int[] A) {
            int firstPositiveIndex = 0;
            for (int i = 0; i < A.length; i++) {
                if (A[i] >= 0) {
                    firstPositiveIndex = i;
                    break;
                }
            }
            int[] squares = new int[A.length];
            int i = firstPositiveIndex - 1, j = firstPositiveIndex, k = 0;
            while (i >= 0 && j < A.length) {
                if (-A[i] < A[j]) {
                    squares[k] = A[i] * A[i];
                    i--;
                } else {
                    squares[k] = A[j] * A[j];
                    j++;
                }
                k++;
            }
            for (; i >= 0; i--)
                squares[k++] = A[i] * A[i];
            for (; j < A.length; j++)
                squares[k++] = A[j] * A[j];
            return squares;
        }
    }
}
