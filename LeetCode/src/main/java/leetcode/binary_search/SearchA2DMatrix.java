package leetcode.binary_search;

public interface SearchA2DMatrix {

  boolean searchMatrix(int[][] matrix, int target);

  class Solution implements SearchA2DMatrix {

    @Override
    public boolean searchMatrix(int[][] matrix, int target) {
      if (matrix.length == 0) {
        return false;
      }
      int left = 0, right = matrix.length * matrix[0].length - 1;
      while (left <= right) {
        int mid = left + (right - left) / 2;
        int i = mid / matrix[0].length,
          j = mid % matrix[0].length;
        if (matrix[i][j] < target) {
          left = mid + 1;
        } else if (matrix[i][j] > target) {
          right = mid - 1;
        } else {
          return true;
        }
      }
      return false;
    }
  }
}
