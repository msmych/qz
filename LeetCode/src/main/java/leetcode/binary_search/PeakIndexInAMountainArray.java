package leetcode.binary_search;

public interface PeakIndexInAMountainArray {

    int peakIndexInMountainArray(int[] A);

    class Solution implements PeakIndexInAMountainArray {

        @Override
        public int peakIndexInMountainArray(int[] A) {
            int left = 0, right = A.length;
            while (right - left > 1) {
                int mid = left + (right - left) / 2;
                if (mid == 0) {
                    left = mid + 1;
                    continue;
                } else if (mid >= A.length - 1) {
                    right = mid - 1;
                    continue;
                }
                if (A[mid] > A[mid - 1] && A[mid] > A[mid + 1])
                    return mid;
                else if (A[mid] < A[mid - 1])
                    right = mid - 1;
                else
                    left = mid + 1;
            }
            if (left > 0 && A[left] > A[left - 1] && A[left] > A[left + 1])
                return left;
            else if (right < A.length - 1 && A[right] > A[right - 1] && A[right] > A[right + 1])
                return right;
            throw new IllegalArgumentException();
        }
    }
}
