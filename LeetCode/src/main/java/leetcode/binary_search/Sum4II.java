package leetcode.binary_search;

import java.util.HashMap;
import java.util.Map;

public interface Sum4II {

  int fourSumCount(int[] A, int[] B, int[] C, int[] D);

  class Solution implements Sum4II {

    @Override
    public int fourSumCount(int[] A, int[] B, int[] C, int[] D) {
      return zero(sums(A, B), sums(C, D));
    }

    private Map<Integer, Integer> sums(int[] nums1, int[] nums2) {
      Map<Integer, Integer> sums = new HashMap<>();
      for (int num1 : nums1) {
        for (int num2 : nums2) {
          int sum = num1 + num2;
          sums.put(sum, sums.getOrDefault(sum, 0) + 1);
        }
      }
      return sums;
    }

    private int zero(Map<Integer, Integer> map1, Map<Integer, Integer> map2) {
      int zero = 0;
      for (Map.Entry<Integer, Integer> entry1 : map1.entrySet()) {
        int opposite = -entry1.getKey();
        if (map2.containsKey(opposite)) {
          zero += entry1.getValue() * map2.get(opposite);
        }
      }
      return zero;
    }
  }
}
