package leetcode.binary_search;

public abstract class AbstractGuessGame {

    private final int pick;

    public AbstractGuessGame(int pick) {
        this.pick = pick;
    }

    protected int guess(int num) {
        return Integer.compare(pick, num);
    }

    public abstract int guessNumber(int n);
}
