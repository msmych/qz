package leetcode.binary_search;

public class FirstBadVersion extends FirstAbstractBadVersion {

    protected FirstBadVersion(int badVersion) {
        super(badVersion);
    }

    @Override
    public int firstBadVersion(int n) {
        int left = 1;
        int right = n;
        int leftNotBad = -1;
        int rightBad = -1;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (isBadVersion(mid)) {
                if (leftNotBad == mid - 1)
                    return mid;
                else {
                    rightBad = mid;
                    right = mid - 1;
                }
            } else if (rightBad == mid + 1)
                return mid + 1;
            else {
                leftNotBad = mid;
                left = mid + 1;
            }
        }
        if (isBadVersion(left))
            return left;
        else if (left + 1 == rightBad)
            return rightBad;
        else
            return -1;
    }
}
