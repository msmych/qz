package leetcode.binary_search;

import java.util.LinkedList;
import java.util.List;

public interface FindKClosestElements {

    List<Integer> findClosestElements(int[] arr, int k, int x);

    class Solution implements FindKClosestElements {

        private final List<Integer> list = new LinkedList<>();
        private int[] arr;
        private int k;
        private int x;

        @Override
        public List<Integer> findClosestElements(int[] arr, int k, int x) {
            this.arr = arr;
            this.k = k;
            this.x = x;
            if (arr.length == 0)
                return list;
            int left = 0;
            int right = arr.length - 1;
            while (left + 1 < right) {
                int mid = left + (right - left) / 2;
                if (arr[mid] < x)
                    left = mid + 1;
                else if (arr[mid] > x)
                    right = mid - 1;
                else {
                    fillList(mid);
                    return list;
                }
            }
            fillList(Math.abs(arr[right] - x) < Math.abs(arr[left] - x) ? right : left);
            return list;
        }

        private void fillList(int index) {
            int left = 0, right = 1;
            while (left + right <= k) {
                if (index + right < arr.length
                        && (index - left < 0 || Math.abs(arr[index + right] - x) < Math.abs(arr[index - left] - x))) {
                    list.add(arr[index + right]);
                    right++;
                } else {
                    list.add(0, arr[index - left]);
                    left++;
                }
            }
        }
    }
}
