package leetcode.binary_search;

public abstract class FirstAbstractBadVersion {

    private final int badVersion;

    protected FirstAbstractBadVersion(int badVersion) {
        this.badVersion = badVersion;
    }

    protected boolean isBadVersion(int version) {
        return badVersion != -1 && version >= badVersion;
    }

    public abstract int firstBadVersion(int n);
}
