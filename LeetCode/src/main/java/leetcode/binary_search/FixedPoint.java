package leetcode.binary_search;

public interface FixedPoint {

    int fixedPoint(int[] A);

    class Solution implements FixedPoint {

        @Override
        public int fixedPoint(int[] A) {
            for (int i = 0; i < A.length; i++) {
                if (A[i] == i) return i;
            }
            return -1;
        }
    }
}
