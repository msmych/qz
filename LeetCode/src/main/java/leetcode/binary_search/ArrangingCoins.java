package leetcode.binary_search;

public interface ArrangingCoins {

    int arrangeCoins(int n);

    class Solution implements ArrangingCoins {

        @Override
        public int arrangeCoins(int n) {
            if (n == 0) {
                return 0;
            }
            int coins = 1;
            for (int i = 1; n >= i; n -= i++) {
                coins = i;
            }
            return coins;
        }
    }
}
