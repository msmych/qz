package leetcode.binary_search;

public class GuessGame extends AbstractGuessGame {

    public GuessGame(int pick) {
        super(pick);
    }

    @Override
    public int guessNumber(int n) {
        int left = 0;
        int right = n;
        while (left <= right) {
            int mid = (int) (((long) left + (long) right) / 2);
            switch (guess(mid)) {
                case -1:
                    right = mid - 1;
                    break;
                case 0:
                    return mid;
                case 1:
                    left = mid + 1;
            }
        }
        return -1;
    }
}
