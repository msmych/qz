package leetcode.binary_search;

import java.util.Arrays;

public interface LongestIncreasingSubsequence {

  int lengthOfLIS(int[] nums);

  class Solution implements LongestIncreasingSubsequence {

    @Override
    public int lengthOfLIS(int[] nums) {
      int[] dp = new int[nums.length];
      int length = 0;
      for (int num : nums) {
        int index = Arrays.binarySearch(dp, 0, length, num);
        if (index < 0) {
          index = -(index + 1);
        }
        dp[index] = num;
        if (index == length) {
          length++;
        }
      }
      return length;
    }
  }
}
