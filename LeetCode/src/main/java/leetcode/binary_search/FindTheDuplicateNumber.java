package leetcode.binary_search;

public interface FindTheDuplicateNumber {

  int findDuplicate(int[] nums);

  class Solution implements FindTheDuplicateNumber {

    @Override
    public int findDuplicate(int[] nums) {
      int slow = nums[0];
      int fast = nums[0];
      do {
        slow = nums[slow];
        fast = nums[nums[fast]];
      } while (slow != fast);
      int i1 = nums[0];
      int i2 = slow;
      while (i1 != i2) {
        i1 = nums[i1];
        i2 = nums[i2];
      }
      return i2;
    }
  }
}
