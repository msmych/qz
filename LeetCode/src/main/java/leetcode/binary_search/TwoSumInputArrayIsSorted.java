package leetcode.binary_search;

public interface TwoSumInputArrayIsSorted {

    int[] twoSum(int[] numbers, int target);

    class Solution implements TwoSumInputArrayIsSorted {

        @Override
        public int[] twoSum(int[] numbers, int target) {
            int rightBorder = numbers.length - 1;
            for (int i = 0; i < rightBorder; i++) {
                for (int j = rightBorder; j > i; j--) {
                    if (numbers[i] + numbers[j] == target) {
                        return new int[]{i + 1, j + 1};
                    } else if (numbers[j] > target) {
                        rightBorder--;
                    }
                }
            }
            throw new IllegalArgumentException();
        }
    }

    class BinarySearchSolution implements TwoSumInputArrayIsSorted {

        private int[] numbers;
        private int target;

        @Override
        public int[] twoSum(int[] numbers, int target) {
            this.numbers = numbers;
            this.target = target;
            if (numbers.length < 2)
                throw new IllegalArgumentException();
            for (int i = 0; i < numbers.length - 1; i++) {
                int j = findSecondIndex(i);
                if (j != -1)
                    return new int[]{i + 1, j + 1};
            }
            throw new IllegalArgumentException();
        }

        private int findSecondIndex(int i) {
            int left = i + 1;
            int right = numbers.length - 1;
            while (left <= right) {
                int mid = left + (right - left) / 2;
                int sum = numbers[i] + numbers[mid];
                if (sum == target)
                    return mid;
                else if (sum < target)
                    left = mid + 1;
                else
                    right = mid - 1;
            }
            return -1;
        }
    }
}
