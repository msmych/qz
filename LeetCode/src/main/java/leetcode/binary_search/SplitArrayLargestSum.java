package leetcode.binary_search;

public interface SplitArrayLargestSum {

    int splitArray(int[] nums, int m);

    class Solution implements SplitArrayLargestSum {

        @Override
        public int splitArray(int[] nums, int m) {
            return 0;
        }
    }
}
