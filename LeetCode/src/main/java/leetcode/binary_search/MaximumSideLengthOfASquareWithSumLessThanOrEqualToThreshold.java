package leetcode.binary_search;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.min;

public interface MaximumSideLengthOfASquareWithSumLessThanOrEqualToThreshold {

  int maxSideLength(int[][] mat, int threshold);

  class Solution implements MaximumSideLengthOfASquareWithSumLessThanOrEqualToThreshold {

    private final Map<Integer, Boolean> cache = new HashMap<>();

    private int[][] mat;
    private int threshold;

    @Override
    public int maxSideLength(int[][] mat, int threshold) {
      this.mat = mat;
      this.threshold = threshold;
      int max = min(mat.length, mat[0].length);
      int left = 1, right = max;
      while (left < right) {
        int mid = left + (right - left) / 2;
        if (fits(mid)) {
          if (mid == max || !fits(mid + 1)) {
            return mid;
          } else {
            left = mid + 1;
          }
        } else {
          right = mid - 1;
        }
      }
      return fits(right) ? right : 0;
    }

    private boolean fits(int side) {
      if (cache.containsKey(side)) {
        return cache.get(side);
      }
      int sum = sum(side);
      if (sum <= threshold) {
        cache.put(side, true);
        return true;
      }
      for (int i = 0; i < mat.length - side; i++) {
        for (int j = 0; j < mat[i].length - side; j++) {
          sum = nextColumn(i, j, side, sum);
          if (sum <= threshold) {
            cache.put(side, true);
            return true;
          }
        }
        if (i < mat.length - side - 1) {
          sum = nextRow(i, side, sum);
          if (sum <= threshold) {
            cache.put(side, true);
            return true;
          }
        }
      }
      cache.put(side, false);
      return false;
    }

    private int sum(int side) {
      int sum = 0;
      for (int i = 0; i < side; i++) {
        for (int j = 0; j < side; j++) {
          sum += mat[i][j];
        }
      }
      return sum;
    }

    private int nextColumn(int i, int j, int side, int sum) {
      for (int k = i; k < i + side; k++) {
        sum = sum - mat[k][j] + mat[k][j + side];
      }
      return sum;
    }

    private int nextRow(int j, int side, int sum) {
      for (int k = 0; k < side; k++) {
        sum = sum - mat[0][k] + mat[side][k];
      }
      return sum;
    }
  }
}
