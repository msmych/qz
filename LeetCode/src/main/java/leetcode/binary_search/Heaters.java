package leetcode.binary_search;

import java.util.IntSummaryStatistics;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public interface Heaters {

    int findRadius(int[] houses, int[] heaters);

    class Solution implements Heaters {

        @Override
        public int findRadius(int[] houses, int[] heaters) {
            IntSummaryStatistics statistics = Stream.of(houses, heaters)
                    .flatMapToInt(IntStream::of)
                    .summaryStatistics();
            int left = Math.min(statistics.getMin(), 0);
            int right = statistics.getMax();
            while (left <= right) {
                int radius = left + (right - left) / 2;
                if (isEnough(houses, heaters, radius)) {
                    if (isEnough(houses, heaters, radius - 1)) {
                        right = radius - 1;
                    } else {
                        return radius;
                    }
                } else {
                    left = radius + 1;
                }
            }
            throw new IllegalArgumentException();
        }

        private boolean isEnough(int[] houses, int[] heaters, int radius) {
            Set<Integer> houseSet = IntStream.of(houses).boxed().collect(Collectors.toSet());
            for (int heater : heaters) {
                houseSet.removeIf(house -> house >= heater - radius && house <= heater + radius);
            }
            return houseSet.isEmpty();
        }
    }
}
