package leetcode.binary_search;

public interface PowXN {

    double myPow(double x, int n);

    class Solution implements PowXN {

        @Override
        public double myPow(double x, int n) {
            if (n == 0)
                return 1;
            else if (x == 1)
                return 1;
            else if (x == -1)
                return n % 2 == 0 ? 1 : -1;
            else if (n > 0)
                return positivePower(x, n);
            else
                return negativePower(x, n);
        }

        private double positivePower(double x, int n) {
            double p = x;
            for (; n > 1; n--) {
                if (Math.abs(p) < 0.00001)
                    return 0;
                p *= x;
            }
            return p;
        }

        private double negativePower(double x, int n) {
            double p = 1;
            for (; n < 0; n++) {
                if (Math.abs(p) < 0.00001)
                    return 0;
                p /= x;
            }
            return p;
        }
    }
}
