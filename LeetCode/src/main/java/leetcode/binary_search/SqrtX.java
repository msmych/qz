package leetcode.binary_search;

public interface SqrtX {

    int mySqrt(int x);

    class Solution implements SqrtX {

        @Override
        public int mySqrt(int x) {
            int lower = -1;
            int upper = -1;
            int left = 0;
            int right = x/2 + 1;
            int mid;
            while (left <= right) {
                mid = (int) (((long)(right + left)) / 2);
                long square = (long)mid*mid;
                if (square == x)
                    return mid;
                else if (x > square) {
                    lower = mid;
                    left = mid + 1;
                }
                else {
                    upper = mid;
                    right = mid - 1;
                }
            }
            if (lower != -1 && upper != -1)
                return lower;
            return -1;
        }
    }
}
