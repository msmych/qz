package leetcode.binary_search;

import java.util.*;

public interface IntersectionOfTwoArraysII {

    int[] intersect(int[] nums1, int[] nums2);

    class Solution implements IntersectionOfTwoArraysII {

        @Override
        public int[] intersect(int[] nums1, int[] nums2) {
            return getArray(merge(getMap(nums1), getMap(nums2)));
        }

        private Map<Integer, Integer> getMap(int[] nums) {
            Map<Integer, Integer> map = new HashMap<>();
            for (int n : nums) {
                map.put(n, map.getOrDefault(n, 0) + 1);
            }
            return map;
        }

        private Map<Integer, Integer> merge(Map<Integer, Integer> map1, Map<Integer, Integer> map2) {
            Set<Integer> keys = new HashSet<>();
            keys.addAll(map1.keySet());
            keys.addAll(map2.keySet());
            Map<Integer, Integer> intersection = new HashMap<>();
            for (Integer key : keys) {
                int num1 = map1.getOrDefault(key, 0),
                        num2 = map2.getOrDefault(key, 0);
                intersection.put(key, Math.min(num1, num2));
            }
            return intersection;
        }

        private int[] getArray(Map<Integer, Integer> map) {
            List<Integer> list = new ArrayList<>();
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                for (int i = 0; i < entry.getValue(); i++) {
                    list.add(entry.getKey());
                }
            }
            int[] nums = new int[list.size()];
            for (int i = 0; i < list.size(); i++) {
                nums[i] = list.get(i);
            }
            return nums;
        }
    }
}
