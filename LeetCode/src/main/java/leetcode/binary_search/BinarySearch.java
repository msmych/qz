package leetcode.binary_search;

import java.util.Arrays;

public interface BinarySearch {

    int search(int[] nums, int target);

    class Solution implements BinarySearch {

        @Override
        public int search(int[] nums, int target) {
            if (nums.length == 0)
                return -1;
            if (nums.length == 1)
                return nums[0] == target ? 0 : -1;
            int mid = nums.length / 2;
            if (nums[mid] == target)
                return mid;
            else if (target < nums[mid]) {
                return search(Arrays.copyOf(nums, mid), target);
            } else {
                int right = search(Arrays.copyOfRange(nums, mid + 1, nums.length), target);
                return right != -1 ? right + mid + 1 : -1;
            }
        }
    }
}
