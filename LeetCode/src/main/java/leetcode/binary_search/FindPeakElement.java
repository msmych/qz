package leetcode.binary_search;

public interface FindPeakElement {

  int findPeakElement(int[] nums);

  class TemplateIISolution implements FindPeakElement {

    @Override
    public int findPeakElement(int[] nums) {
      if (nums.length == 0)
        return -1;
      int left = 0;
      int right = nums.length;
      while (left < right) {
        int mid = left + (right - left) / 2;
        if (mid == 0 || nums[mid] > nums[mid - 1]) {
          if (mid == nums.length - 1 || nums[mid] > nums[mid + 1]) {
            return mid;
          }
        }
        if (mid == 0) {
          return nums[0] > nums[1] ? 0 : 1;
        } else if (mid == nums.length - 1) {
          return nums[mid] > nums[mid - 1] ? mid : mid - 1;
        } else if (nums[mid - 1] > nums[mid + 1]) {
          right = mid - 1;
        } else {
          left = mid + 1;
        }
      }
      return left;
    }
  }

  class TemplateIIISolution implements FindPeakElement {

    @Override
    public int findPeakElement(int[] nums) {
      if (nums.length == 0) {
        return -1;
      }
      int left = 0;
      int right = nums.length - 1;
      while (left + 1 < right) {
        int mid = left + (right - left) / 2;
        if ((mid == 0 || nums[mid - 1] < nums[mid])
          && (mid == nums.length - 1 || nums[mid + 1] < nums[mid])) {
          return mid;
        }
        if (mid == 0) {
          return nums[0] > nums[1] ? 0 : 1;
        } else if (mid == nums.length - 1) {
          return nums[mid] > nums[mid - 1] ? mid : mid - 1;
        } else if (nums[mid - 1] > nums[mid + 1]) {
          right = mid - 1;
        } else {
          left = mid + 1;
        }
      }
      return nums[right] > nums[left] ? right : left;
    }
  }
}
