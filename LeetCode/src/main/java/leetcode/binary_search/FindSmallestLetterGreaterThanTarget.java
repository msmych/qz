package leetcode.binary_search;

public interface FindSmallestLetterGreaterThanTarget {

    char nextGreatestLetter(char[] letters, char target);

    class Solution implements FindSmallestLetterGreaterThanTarget {

        @Override
        public char nextGreatestLetter(char[] letters, char target) {
            if (target < letters[0])
                return letters[0];
            int left = 0;
            int right = letters.length - 1;
            while (left < right) {
                int mid = left + (right - left) / 2;
                char val = letters[mid];
                if (val == target || val < target) {
                    char next = letters[mid + 1];
                    if (next > target)
                        return next;
                    left = mid + 1;
                } else
                    right = mid - 1;
            }
            return right == letters.length - 1 ? letters[0] : letters[right + 1];
        }
    }
}
