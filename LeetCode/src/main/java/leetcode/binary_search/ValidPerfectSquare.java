package leetcode.binary_search;

public interface ValidPerfectSquare {

    boolean isPerfectSquare(int num);

    class Solution implements ValidPerfectSquare {

        @Override
        public boolean isPerfectSquare(int num) {
            int left = 1;
            int right = num / 2 + 1;
            while (left <= right) {
                long mid = left + (right - left) / 2;
                long square = mid * mid;
                if (square < num)
                    left = (int) (mid + 1);
                else if (square > num)
                    right = (int) (mid - 1);
                else
                    return true;
            }
            return false;
        }
    }
}
