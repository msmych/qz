package leetcode.binary_search;

public interface SearchInsertPosition {

    int searchInsert(int[] nums, int target);

    class Solution implements SearchInsertPosition {

        @Override
        public int searchInsert(int[] nums, int target) {
            int left = 0, right = nums.length - 1;
            while (left <= right) {
                int mid = left + (right - left) / 2;
                if (nums[mid] == target)
                    return mid;
                else if (nums[mid] < target)
                    left = mid + 1;
                else
                    right = mid - 1;
            }
            if (left < 0)
                return 0;
            if (left >= nums.length)
                return nums.length;
            return left;
        }
    }
}
