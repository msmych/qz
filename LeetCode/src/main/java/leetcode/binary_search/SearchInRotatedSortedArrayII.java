package leetcode.binary_search;

import java.util.Arrays;

public interface SearchInRotatedSortedArrayII {

    boolean search(int[] nums, int target);

    class Solution implements SearchInRotatedSortedArrayII {

        @Override
        public boolean search(int[] nums, int target) {
            if (nums.length == 0)
                return false;
            int left = 0, right = nums.length - 1, pivot = -1;
            while (left < right) {
                int mid = left + (right - left) / 2;
                if (nums[mid] == target)
                    return true;
                if (nums[mid] == nums[0] && nums[mid] == nums[nums.length - 1])
                    return Arrays.stream(nums).anyMatch(n -> n == target);
                if (nums[mid] > nums[mid + 1]) {
                    pivot = mid + 1;
                    break;
                }
                if (nums[mid] > nums[0] || nums[mid] > nums[nums.length - 1])
                    left = mid + 1;
                else
                    right = mid - 1;
            }
            if (pivot == -1)
                pivot = right + 1;
            int[] leftNums = Arrays.copyOfRange(nums, pivot, nums.length),
                    rightNums = Arrays.copyOf(nums, pivot);
            System.arraycopy(leftNums, 0, nums, 0, leftNums.length);
            System.arraycopy(rightNums, 0, nums, leftNums.length, rightNums.length);
            left = 0;
            right = nums.length - 1;
            while (left <= right) {
                int mid = left + (right - left) / 2;
                if (nums[mid] == target)
                    return true;
                if (nums[mid] < target)
                    left = mid + 1;
                else
                    right = mid - 1;
            }
            return false;
        }
    }
}
