package leetcode.binary_search;

import java.util.Arrays;

public interface SearchInRotatedSortedArray {

  int search(int[] nums, int target);

  class Solution implements SearchInRotatedSortedArray {

    @Override
    public int search(int[] nums, int target) {
      if (nums.length == 0) {
        return -1;
      }
      if (nums.length == 1) {
        return nums[0] == target ? 0 : -1;
      }
      int[] leftNums = null;
      int[] rightNums = null;
      for (int i = 0; i < nums.length - 1; i++) {
        if (nums[i] > nums[i + 1]) {
          leftNums = Arrays.copyOfRange(nums, i + 1, nums.length);
          rightNums = Arrays.copyOf(nums, i + 1);
          break;
        }
      }
      if (leftNums == null) {
        leftNums = nums;
        rightNums = new int[0];
      }
      int left = 0;
      int right = nums.length - 1;
      while (left <= right) {
        int mid = (left + right) / 2;
        int val = mid < leftNums.length
          ? leftNums[mid]
          : rightNums[mid - leftNums.length];
        if (val == target) {
          return mid < leftNums.length
            ? mid + rightNums.length
            : mid - leftNums.length;
        } else if (val > target) {
          right = mid - 1;
        } else {
          left = mid + 1;
        }
      }
      return -1;
    }
  }
}
