package leetcode.binary_search;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface IntersectionOfTwoArrays {

    int[] intersection(int[] nums1, int[] nums2);

    class Solution implements IntersectionOfTwoArrays {

        @Override
        public int[] intersection(int[] nums1, int[] nums2) {
            Set<Integer> intersection = new HashSet<>();
            List<Integer> list1 = toList(nums1);
            List<Integer> list2 = toList(nums2);
            for (Integer i : list1) {
                if (list2.contains(i)) {
                    intersection.add(i);
                }
            }
            for (Integer i : list2) {
                if (list1.contains(i)) {
                    intersection.add(i);
                }
            }
            return toArray(intersection);
        }

        private List<Integer> toList(int[] nums) {
            List<Integer> list = new ArrayList<>();
            for (int num : nums) {
                list.add(num);
            }
            return list;
        }

        private int[] toArray(Set<Integer> set) {
            Integer[] integers = set.toArray(new Integer[]{});
            int[] ints = new int[integers.length];
            for (int i = 0; i < integers.length; i++) {
                ints[i] = integers[i];
            }
            return ints;
        }
    }
}
