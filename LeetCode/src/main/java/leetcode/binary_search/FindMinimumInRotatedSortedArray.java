package leetcode.binary_search;

public interface FindMinimumInRotatedSortedArray {

    int findMin(int[] nums);

    class Solution implements FindMinimumInRotatedSortedArray {

        @Override
        public int findMin(int[] nums) {
            if (nums.length == 0)
                return -1;
            else if (nums.length == 1)
                return nums[0];
            int leftVal = nums[0];
            int left = 1;
            int right = nums.length - 1;
            while (left < right) {
                int mid = left + (right - left) / 2;
                if (nums[mid] < leftVal) {
                    if (nums[mid] < nums[mid - 1])
                        return nums[mid];
                    else
                        right = mid - 1;
                } else {
                    leftVal = nums[mid];
                    left = mid + 1;
                }
            }
            return Math.min(nums[0], nums[left]);
        }
    }

    class SolutionII implements FindMinimumInRotatedSortedArray {

        private int[] nums;

        @Override
        public int findMin(int[] nums) {
            this.nums = nums;
            return findMin(0, nums.length - 1);
        }

        private int findMin(int left, int right) {
            if (left > right)
                return Integer.MAX_VALUE;
            int mid = left + (right - left) / 2;
            int val = nums[mid];
            int rightMin = findMin(mid + 1, right);
            int leftMin = mid == 0 || val < nums[mid - 1] ? val : findMin(left, mid - 1);
            return Math.min(leftMin, rightMin);
        }
    }
}
