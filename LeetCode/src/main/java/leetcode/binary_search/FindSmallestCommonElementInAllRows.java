package leetcode.binary_search;

import java.util.HashMap;
import java.util.Map;

public interface FindSmallestCommonElementInAllRows {

  int smallestCommonElement(int[][] mat);

  class Solution implements FindSmallestCommonElementInAllRows {
    @Override
    public int smallestCommonElement(int[][] mat) {
      if (mat.length == 0) {
        return -1;
      }
      Map<Integer, Integer> map = new HashMap<>();
      for (int j = 0; j < mat[0].length; j++) {
        for (int i = 0; i < mat.length; i++) {
          map.merge(mat[i][j], 1, Integer::sum);
        }
      }
      return map.entrySet().stream()
        .filter(e -> e.getValue() == mat.length)
        .mapToInt(Map.Entry::getKey)
        .min()
        .orElse(-1);
    }
  }
}
