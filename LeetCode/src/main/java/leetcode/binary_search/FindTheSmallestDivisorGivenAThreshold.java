package leetcode.binary_search;

import java.util.stream.IntStream;

public interface FindTheSmallestDivisorGivenAThreshold {

  int smallestDivisor(int[] nums, int threshold);

  class Solution implements FindTheSmallestDivisorGivenAThreshold {

    private int[] nums;
    private int threshold;

    @Override
    public int smallestDivisor(int[] nums, int threshold) {
      this.nums = nums;
      this.threshold = threshold;
      int left = 1;
      int right = IntStream.of(nums).max().getAsInt() + 1;
      while (left < right) {
        int mid = left + (right - left) / 2;
        if (fits(mid)) {
          if (mid == 1 || fits(mid - 1)) {
            right = mid - 1;
          } else {
            return mid;
          }
        } else {
          left = mid + 1;
        }
      }
      return left;
    }

    private boolean fits(int divisor) {
      return IntStream.of(nums).map(n -> divide(n, divisor)).sum() <= threshold;
    }

    private int divide(int dividend, int divisor) {
      int fraction = dividend / divisor;
      return dividend % divisor == 0 ? fraction : fraction + 1;
    }
  }
}
