package leetcode.binary_search;

import java.util.IntSummaryStatistics;

import static java.lang.Math.abs;
import static java.lang.Math.min;
import static java.util.Arrays.stream;

public interface SumOfMutatedArrayClosestToTarget {

  int findBestValue(int[] arr, int target);

  class Solution implements SumOfMutatedArrayClosestToTarget {
    @Override
    public int findBestValue(int[] arr, int target) {
      IntSummaryStatistics statistics = stream(arr).summaryStatistics();
      if (target >= statistics.getSum()) {
        return statistics.getMax();
      }
      if (target <= statistics.getMin() * arr.length) {
        int left = target / arr.length;
        int right = left + 1;
        return abs(target - left * arr.length) < abs(target - right * arr.length) ? left : right;
      }
      int left = statistics.getMin(), right = statistics.getMax();
      while (left < right - 1) {
        int mid = left + (right - left) / 2;
        int sum = sum(arr, mid), leftSum = sum(arr, mid - 1), rightSum = sum(arr, mid + 1);
        int diff = abs(target - sum), leftDiff = abs(target - leftSum), rightDiff = abs(target - rightSum);
        if (diff < leftDiff && diff < rightDiff) {
          return mid;
        } else if (rightDiff > leftDiff) {
          right = mid - 1;
        } else {
          left = mid + 1;
        }
      }
      return abs(target - sum(arr, left)) < abs(target - sum(arr, right)) ? left : right;
    }

    private int sum(int[] arr, int mid) {
      return stream(arr).map(n -> min(n, mid)).sum();
    }
  }
}
