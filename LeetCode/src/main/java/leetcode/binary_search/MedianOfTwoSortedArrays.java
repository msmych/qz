package leetcode.binary_search;

public interface MedianOfTwoSortedArrays {

  double findMedianSortedArrays(int[] nums1, int[] nums2);

  class Solution implements MedianOfTwoSortedArrays {

    @Override
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
      return nums1.length < nums2.length ? getMedian(nums1, nums2) : getMedian(nums2, nums1);
    }

    private double getMedian(int[] a, int[] b) {
      int left = 0, right = a.length;
      while (left <= right) {
        int leftMid = left + (right - left) / 2,
          rightMid = (a.length + b.length + 1) / 2 - leftMid;
        if ((rightMid == 0 || leftMid == a.length || b[rightMid - 1] <= a[leftMid])
          && (leftMid == 0 || leftMid == b.length || a[leftMid - 1] <= b[rightMid])) {
          int leftMax;
          if (leftMid == 0)
            leftMax = b[rightMid - 1];
          else if (rightMid == 0)
            leftMax = a[leftMid - 1];
          else
            leftMax = Math.max(a[leftMid - 1], b[rightMid - 1]);
          if ((a.length + b.length) % 2 == 1)
            return leftMax;
          int rightMin;
          if (leftMid == a.length)
            rightMin = b[rightMid];
          else if (rightMid == b.length)
            rightMin = a[leftMid];
          else
            rightMin = Math.min(a[leftMid], b[rightMid]);
          return (leftMax + rightMin) / 2.0;
        } else if (leftMid < a.length && b[rightMid - 1] > a[leftMid])
          left = leftMid + 1;
        else
          right = leftMid - 1;
      }
      return 0.0;
    }
  }
}
