package leetcode.binary_search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface FindPositiveIntegerSolutionForAGivenEquation {

  interface CustomFunction {
    int f(int x, int y);
  }

  List<List<Integer>> findSolution(CustomFunction customfunction, int z);

  class Solution implements FindPositiveIntegerSolutionForAGivenEquation {
    @Override
    public List<List<Integer>> findSolution(CustomFunction customfunction, int z) {
      List<List<Integer>> solutions = new ArrayList<>();
      for (int x = 1; x <= 1000; x++) {
        for (int y = 1; y <= 1000; y++) {
          if (customfunction.f(x, y) == z) {
            solutions.add(Arrays.asList(x, y));
          }
        }
      }
      return solutions;
    }
  }
}
