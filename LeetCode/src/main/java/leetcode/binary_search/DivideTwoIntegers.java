package leetcode.binary_search;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Math.abs;

public interface DivideTwoIntegers {

    int divide(int dividend, int divisor);

    class Solution implements DivideTwoIntegers {

        @Override
        public int divide(int dividend, int divisor) {
          boolean positive = dividend < 0 == divisor < 0;
          long absDividend = abs((long) dividend);
          long absDivisor = abs((long) divisor);
          long quotient = 0;
          while (absDivisor <= absDividend) {
            long div = absDivisor;
            long i = 1;
            while (div <= absDividend) {
              div <<= 1;
              i <<= 1;
            }
            quotient += i >> 1;
            absDividend -= div >> 1;
          }
          if (positive && quotient > MAX_VALUE) {
            return MAX_VALUE;
          }
          return (int) (positive ? quotient : ~quotient + 1);
        }
    }
}
