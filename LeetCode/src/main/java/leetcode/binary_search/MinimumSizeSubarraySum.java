package leetcode.binary_search;

public interface MinimumSizeSubarraySum {

    int minSubArrayLen(int s, int[] nums);

    class Solution implements MinimumSizeSubarraySum {

        @Override
        public int minSubArrayLen(int s, int[] nums) {
            int len = 0;
            int sum = 0;
            int left = 0;
            for (int i = 0; i < nums.length; i++) {
                sum += nums[i];
                while (sum >= s) {
                    int l = i - left + 1;
                    if (len == 0 || l < len) {
                        len = l;
                    }
                    sum -= nums[left++];
                }
            }
            return len;
        }
    }
}
