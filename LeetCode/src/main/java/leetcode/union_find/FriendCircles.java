package leetcode.union_find;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

public interface FriendCircles {

  int findCircleNum(int[][] M);

  class Solution implements FriendCircles {
    @Override
    public int findCircleNum(int[][] M) {
      Set<Set<Integer>> circles = new HashSet<>();
      for (int i = 0; i < M.length; i++) {
        for (int j = 0; j < M[i].length; j++) {
          if (M[i][j] == 1) {
            circles.add(Stream.of(i, j).collect(toSet()));
          }
        }
      }
      boolean united;
      do {
        united = false;
        for (int i = 0; i < M.length; i++) {
          int index = i;
          Set<Set<Integer>> iCircles = circles.stream()
            .filter(circle -> circle.contains(index))
            .collect(toSet());
          if (iCircles.size() > 1) {
            circles.removeAll(iCircles);
            circles.add(iCircles.stream().flatMap(Set::stream).collect(toSet()));
            united = true;
          }
        }
      } while (united && circles.size() > 1);
      return circles.size();
    }
  }
}
