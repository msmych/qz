package leetcode.trie;

import java.util.*;

public interface WordSearchII {

  List<String> findWords(char[][] board, String[] words);

  class Solution implements WordSearchII {

    private static class Node {
      final Map<Character, Node> children = new HashMap<>();
      String word = "";
    }

    private static class Point {
      int i;
      int j;

      Point(int i, int j) {
        this.i = i;
        this.j = j;
      }

      @Override
      public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return i == point.i &&
          j == point.j;
      }

      @Override
      public int hashCode() {
        return Objects.hash(i, j);
      }
    }

    private final Node root = new Node();
    private final Set<Point> visited = new HashSet<>();
    private char[][] board;

    @Override
    public List<String> findWords(char[][] board, String[] words) {
      this.board = board;
      List<String> found = new ArrayList<>();
      if (board.length == 0)
        return found;
      Arrays.stream(words).forEach(this::insert);
      Set<String> foundSet = new HashSet<>();
      for (int i = 0; i < board.length; i++) {
        for (int j = 0; j < board[0].length; j++) {
          foundSet.addAll(find(i, j, root));
        }
      }
      found.addAll(foundSet);
      return found;
    }

    private void insert(String word) {
      Node node = root;
      for (char c : word.toCharArray()) {
        node.children.putIfAbsent(c, new Node());
        node = node.children.get(c);
      }
      node.word = word;
    }

    private Set<String> find(int i, int j, Node node) {
      if (node == null)
        return Collections.emptySet();
      Set<String> found = new HashSet<>();
      if (!node.word.isEmpty())
        found.add(node.word);
      Point point = new Point(i, j);
      if (i < 0 || j < 0 || i >= board.length || j >= board[0].length || visited.contains(point))
        return found;
      char c = board[i][j];
      if (!node.children.containsKey(c))
        return found;
      visited.add(point);
      Node next = node.children.get(c);
      found.addAll(find(i + 1, j, next));
      found.addAll(find(i - 1, j, next));
      found.addAll(find(i, j + 1, next));
      found.addAll(find(i, j - 1, next));
      visited.remove(point);
      return found;
    }
  }
}
