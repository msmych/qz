package leetcode.trie;

import java.util.*;
import java.util.stream.Collectors;

public interface PalindromePairs {

    List<List<Integer>> palindromePairs(String[] words);

    class Solution implements PalindromePairs {

        class Node {
            final Map<Character, Node> children = new HashMap<>();
            String word;
            int index;
        }

        private final Node root = new Node();

        @Override
        public List<List<Integer>> palindromePairs(String[] words) {
            for (int i = 0; i < words.length; i++)
                insert(words[i], i);
            List<List<Integer>> pairs = new ArrayList<>();
            for (int i = 0; i < words.length; i++)
                pairs.addAll(find(words[i], 0, i, root));
            return pairs;
        }

        private void insert(String word, int index) {
            Node node = root;
            for (char c : word.toCharArray()) {
                node.children.putIfAbsent(c, new Node());
                node = node.children.get(c);
            }
            node.word = word;
            node.index = index;
        }

        private Set<List<Integer>> find(String word, int to, int j, Node node) {
            Set<List<Integer>> pairs = new HashSet<>();
            String w = word.substring(0, word.length() - to);
            if (node.word != null && j != node.index)
                if (w.isEmpty() || isAnagram(w))
                    pairs.add(Arrays.asList(node.index, j));
            if (!w.isEmpty()) {
                char last = w.charAt(w.length() - 1);
                if (node.children.containsKey(last))
                    pairs.addAll(find(word, to + 1, j, node.children.get(last)));
            } else
                pairs.addAll(palindromes(node, word.length()).entrySet().stream()
                        .filter(p -> p.getValue() != j)
                        .map(p -> Arrays.asList(p.getValue(), j))
                        .collect(Collectors.toSet()));
            return pairs;
        }

        private boolean isAnagram(String word) {
            return new StringBuffer(word).reverse().toString().equals(word);
        }

        private Map<String, Integer> palindromes(Node root, int from) {
            Map<String, Integer> palindromes = new HashMap<>();
            if (root.word != null && isAnagram(root.word.substring(from)))
                palindromes.put(root.word, root.index);
            for (Node child : root.children.values())
                palindromes.putAll(palindromes(child, from));
            return palindromes;
        }
    }
}
