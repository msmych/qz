package leetcode.trie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ReplaceWords {

    String replaceWords(List<String> dict, String sentence);

    class Solution implements ReplaceWords {

        class Node {
            final Map<Character, Node> children = new HashMap<>();
            boolean isRoot = false;
        }

        private final Node root = new Node();

        @Override
        public String replaceWords(List<String> dict, String sentence) {
            insert(dict);
            List<String> words = new ArrayList<>();
            for (String word : sentence.split(" ")) {
                Node node = root;
                StringBuilder sb = new StringBuilder();
                boolean found = false;
                for (int i = 0; i < word.length(); i++) {
                    char c = word.charAt(i);
                    if (node.isRoot) {
                        found = true;
                        words.add(sb.toString());
                        break;
                    }
                    if (node.children.containsKey(c)) {
                        sb.append(c);
                        node = node.children.get(c);
                        if (i == word.length() - 1 && node.isRoot) {
                            found = true;
                            words.add(sb.toString());
                        }
                    } else
                        break;
                }
                if (!found)
                    words.add(word);
            }
            return String.join(" ", words);
        }

        private void insert(List<String> words) {
            for (String word : words) {
                Node node = root;
                for (char c : word.toCharArray()) {
                    node.children.putIfAbsent(c, new Node());
                    node = node.children.get(c);
                }
                node.isRoot = true;
            }
        }
    }
}
