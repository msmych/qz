package leetcode.trie;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public interface IndexPairsOfAString {

    int[][] indexPairs(String text, String[] words);

    class Solution implements IndexPairsOfAString {

        @Override
        public int[][] indexPairs(String text, String[] words) {
            List<int[]> pairs = new ArrayList<>();
            for (String word : words) {
                int base = 0;
                String s = text;
                while (s.contains(word)) {
                    int startIndex = s.indexOf(word);
                    pairs.add(new int[]{base + startIndex, base + startIndex + word.length() - 1});
                    s = s.substring(startIndex + 1);
                    base += startIndex + 1;
                }
            }
            return pairs.stream()
                    .sorted((p1, p2) -> {
                        if (p1[0] != p2[0]) return Integer.compare(p1[0], p2[0]);
                        return Integer.compare(p1[1], p2[1]);
                    }).collect(Collectors.toList())
                    .toArray(new int[][]{});
        }
    }
}
