package leetcode.trie;

import java.util.HashMap;
import java.util.Map;

public class MapSum extends AbstractMapSum {

    class Node {
        final Map<Character, Node> children = new HashMap<>();
        int val = 0;
    }

    private final Node root = new Node();

    @Override
    public void insert(String key, int val) {
        Node node = root;
        for (char c : key.toCharArray()) {
            node.children.putIfAbsent(c, new Node());
            node = node.children.get(c);
        }
        node.val = val;
    }

    @Override
    public int sum(String prefix) {
        Node node = root;
        for (char c : prefix.toCharArray()) {
            if (!node.children.containsKey(c))
                return 0;
            node = node.children.get(c);
        }
        return getValSum(node);
    }

    private int getValSum(Node root) {
        if (root == null)
            return 0;
        return root.val + root.children.values().stream()
                .map(this::getValSum)
                .reduce((v1, v2) -> v1 + v2)
                .orElse(0);
    }
}
