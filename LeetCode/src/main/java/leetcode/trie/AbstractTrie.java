package leetcode.trie;

public abstract class AbstractTrie {

    /** Initialize your data structure here. */
    public AbstractTrie() {}

    /** Inserts a word into the trie. */
    public abstract void insert(String word);

    /** Returns if the word is in the trie. */
    public abstract boolean search(String word);

    /** Returns if there is any word in the trie that starts with the given prefix. */
    public abstract boolean startsWith(String prefix);
}
/**
 * Your Trie object will be instantiated and called as such:
 * Trie obj = new Trie();
 * obj.insert(word);
 * boolean param_2 = obj.search(word);
 * boolean param_3 = obj.startsWith(prefix);
 */