package leetcode.trie;

public abstract class AbstractMapSum {

    /** Initialize your data structure here. */
    public AbstractMapSum() {}

    public abstract void insert(String key, int val);

    public abstract int sum(String prefix);
}

/**
 * Your MapSum object will be instantiated and called as such:
 * MapSum obj = new MapSum();
 * obj.insert(key,val);
 * int param_2 = obj.sum(prefix);
 */
