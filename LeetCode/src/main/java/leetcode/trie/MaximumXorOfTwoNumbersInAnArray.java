package leetcode.trie;

import java.util.HashMap;
import java.util.Map;

public interface MaximumXorOfTwoNumbersInAnArray {

    int findMaximumXOR(int[] nums);

    class Solution implements MaximumXorOfTwoNumbersInAnArray {

        class Node {
            final Map<Boolean, Node> children = new HashMap<>();
        }

        private final Node root = new Node();

        @Override
        public int findMaximumXOR(int[] nums) {
            for (int num : nums)
                insert(num);
            int max = 0;
            for (int num : nums) {
                int maxXor = maxXor(root, num, 1);
                if (maxXor > max)
                    max = maxXor;
            }
            return max;
        }

        private void insert(int val) {
            Node node = root;
            for (long i = 1; i <= val; i *= 2) {
                boolean bit = (val & i) > 0;
                node.children.putIfAbsent(bit, new Node());
                node = node.children.get(bit);
            }
        }

        private int maxXor(Node node, int val, int base) {
            boolean vbit = (val & base) > 0;
            int tmax = 0, fmax = 0;
            if (node.children.containsKey(true)) {
                if (!vbit)
                    tmax = base;
                tmax += maxXor(node.children.get(true), val, 2*base);
            }
            if (node.children.containsKey(false)) {
                if (vbit)
                    fmax = base;
                fmax += maxXor(node.children.get(false), val, 2*base);
            }
            return Math.max(tmax, fmax);
        }
    }
}
