package leetcode.trie;

public abstract class AbstractWordDictionary {

    /** Initialize your data structure here. */
    public AbstractWordDictionary() {}

    /** Adds a word into the data structure. */
    public abstract void addWord(String word);

    /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
    public abstract boolean search(String word);
}

/**
 * Your WordDictionary object will be instantiated and called as such:
 * WordDictionary obj = new WordDictionary();
 * obj.addWord(word);
 * boolean param_2 = obj.search(word);
 */