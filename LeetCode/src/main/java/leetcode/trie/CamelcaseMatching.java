package leetcode.trie;

import java.util.ArrayList;
import java.util.List;

public interface CamelcaseMatching {

    List<Boolean> camelMatch(String[] queries, String pattern);

    class Solution implements CamelcaseMatching {

        @Override
        public List<Boolean> camelMatch(String[] queries, String pattern) {
            List<Boolean> matches = new ArrayList<>();
            List<String> patternWords = getWords(pattern);
            for (String query : queries) {
                List<String> words = getWords(query);
                if (words.size() != patternWords.size() &&
                        (Character.isLowerCase(patternWords.get(0).charAt(0)) ||
                                Character.isUpperCase(words.get(0).charAt(0)))) {
                    matches.add(false);
                    continue;
                }
                boolean match = true;
                if (Character.isUpperCase(patternWords.get(0).charAt(0))
                        && Character.isLowerCase(words.get(0).charAt(0)))
                    words.remove(0);
                for (int i = 0; i < words.size(); i++) {
                    String word = words.get(i), patternWord = patternWords.get(i);
                    if (i == 0 &&
                            Character.isUpperCase(word.charAt(0)) ||
                            Character.isUpperCase(patternWord.charAt(0))) {
                        if (word.charAt(0) != patternWord.charAt(0) || patternWord.length() > word.length()) {
                            match = false;
                            break;
                        }
                    }
                    for (int j = 0; j < word.length(); j++) {
                        if (word.charAt(j) == patternWord.charAt(0)) patternWord = patternWord.substring(1);
                        if (patternWord.isEmpty()) break;
                    }
                    if (!patternWord.isEmpty())
                        match = false;
                }
                matches.add(match);
            }
            return matches;
        }

        private List<String> getWords(String query) {
            List<String> words = new ArrayList<>();
            StringBuilder sb = new StringBuilder();
            for (char c : query.toCharArray()) {
                if (Character.isUpperCase(c)) {
                    if (sb.length() > 0) words.add(sb.toString());
                    sb = new StringBuilder().append(c);
                } else sb.append(c);
            }
            if (sb.length() > 0) words.add(sb.toString());
            return words;
        }
    }
}
