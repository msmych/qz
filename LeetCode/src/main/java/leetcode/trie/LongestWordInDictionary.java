package leetcode.trie;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface LongestWordInDictionary {

    String longestWord(String[] words);

    class Solution implements LongestWordInDictionary {

        @Override
        public String longestWord(String[] words) {
            List<String> longest = new ArrayList<>(Collections.singletonList(""));
            Set<String> wordSet = Stream.of(words).collect(Collectors.toSet());
            while (true) {
                List<String> next = wordSet.stream()
                        .filter(w -> w.length() == longest.iterator().next().length() + 1)
                        .filter(w -> longest.stream().anyMatch(w::startsWith))
                        .collect(Collectors.toList());
                if (next.isEmpty())
                    break;
                longest.clear();
                longest.addAll(next);
            }
            return longest.stream()
                    .min(Comparator.naturalOrder())
                    .orElse("");
        }
    }
}
