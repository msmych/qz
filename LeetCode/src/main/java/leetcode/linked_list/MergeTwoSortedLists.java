package leetcode.linked_list;

public interface MergeTwoSortedLists {

    ListNode mergeTwoLists(ListNode l1, ListNode l2);

    class Solution implements MergeTwoSortedLists {

        @Override
        public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
            if (l1 == null && l2 == null)
                return null;
            ListNode dummy = new ListNode(0);
            if (l1 != null && l2 != null) {
                if (l1.val < l2.val) {
                    dummy.next = l1;
                    dummy.next.next = mergeTwoLists(l1.next, l2);
                } else {
                    dummy.next = l2;
                    dummy.next.next = mergeTwoLists(l1, l2.next);
                }
            } else
                dummy.next = l1 != null ? l1 : l2;
            return dummy.next;
        }
    }
}
