package leetcode.linked_list;

public interface FlattenAMultilevelDoublyLinkedList {

    Node flatten(Node head);

    class Solution implements FlattenAMultilevelDoublyLinkedList {

        @Override
        public Node flatten(Node head) {
            Node dummy = new Node();
            Node node = dummy;
            while (head != null) {
                node.next = new Node();
                node.next.val = head.val;
                node.next.prev = node;
                node = node.next;
                if (head.child != null) {
                    Node n = flatten(head.child);
                    while (n != null) {
                        node.next = new Node();
                        node.next.val = n.val;
                        node.next.prev = node;
                        node = node.next;
                        n = n.next;
                    }
                }
                head = head.next;
            }
            if (dummy.next != null)
                dummy.next.prev = null;
            return dummy.next;
        }
    }
}
