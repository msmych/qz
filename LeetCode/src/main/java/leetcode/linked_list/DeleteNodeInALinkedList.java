package leetcode.linked_list;

public interface DeleteNodeInALinkedList {

    void deleteNode(ListNode node);

    class Solution implements DeleteNodeInALinkedList {

        @Override
        public void deleteNode(ListNode node) {
            node.val = node.next.val;
            node.next = node.next.next;
        }
    }
}
