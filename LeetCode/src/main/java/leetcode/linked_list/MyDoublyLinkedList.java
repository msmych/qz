package leetcode.linked_list;

public class MyDoublyLinkedList extends MyAbstractLinkedList {

    private Entry head;

    class Entry {
        Entry previous;
        Entry next;
        int val;

        Entry (int val) {
            this.val = val;
        }
    }

    @Override
    public int get(int index) {
        if (head == null) return -1;
        Entry entry = head;
        for (int i = 0; i < index; i++) {
            if (entry != null)
                entry = entry.next;
            else return -1;
        }
        return entry != null ? entry.val : -1;
    }

    @Override
    public void addAtHead(int val) {
        Entry entry = new Entry(val);
        entry.next = head;
        if (head != null)
            head.previous = entry;
        head = entry;
    }

    @Override
    public void addAtTail(int val) {
        Entry entry = new Entry(val);
        if (head == null)
            head = entry;
        Entry tail = head;
        while (tail.next != null) {
            tail = tail.next;
        }
        tail.next = entry;
        entry.previous = tail;
    }

    @Override
    public void addAtIndex(int index, int val) {
        Entry entry = new Entry(val);
        if (index == 0) {
            if (head == null) {
                head = entry;
            } else {
                entry.next = head.next;
                head = entry;
            }
            return;
        }
        Entry old = head;
        for (int i = 1; i < index; i++) {
            if (old != null)
                old = old.next;
            else return;
        }
        if (old == null) return;
        Entry next = old.next;
        entry.next = next;
        if (next != null)
            next.previous = entry;
        old.next = entry;
        entry.previous = old;
    }

    @Override
    public void deleteAtIndex(int index) {
        if (head == null) return;
        if (index == 0) {
            head = head.next;
            head.previous = null;
        }
        Entry entry = head;
        for (int i = 0; i < index; i++) {
            if (entry != null)
                entry = entry.next;
        }
        if (entry == null) return;
        Entry previous = entry.previous;
        Entry next = entry.next;
        previous.next = next;
        if (next != null)
            next.previous = previous;
    }
}
