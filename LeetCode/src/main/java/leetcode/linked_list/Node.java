package leetcode.linked_list;

public class Node {
    public int val;
    public Node prev;
    public Node next;
    public Node child;

    public Node() {}

    public Node(int val, Node prev, Node next, Node child) {
        this.val = val;
        this.prev = prev;
        this.next = next;
        this.child = child;
    }

    public static Node fromVals(Object... vals) {
        if (vals.length == 0) return null;
        Node dummy = new Node();
        Node node = dummy;
        for (Object val : vals) {
            if (!(val instanceof Integer) && !(val instanceof Node))
                throw new IllegalArgumentException();
            if (val instanceof Integer) {
                node.next = new Node();
                node.next.val = (int) val;
                node.next.prev = node;
                node = node.next;
            } else {
                node.child = (Node) val;
            }
        }
        return dummy.next;
    }

    public static boolean equalVals(Node head, int... vals) {
        return equalVals(head, 0, vals) == vals.length;
    }

    private static int equalVals(Node head, int i, int[] vals) {
        Node node = head;
        if (node == null)
            return vals.length == 0 ? i : -1;
        while (node != null) {
            if (i == vals.length)
                return -1;
            int val = vals[i];
            if (node.val != val)
                return -1;
            i++;
            if (node.child != null) {
                i = equalVals(node.child, i, vals);
                if (i == -1)
                    return -1;
            }
            node = node.next;
        }
        return i;
    }
}
