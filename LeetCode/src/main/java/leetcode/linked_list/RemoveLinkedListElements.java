package leetcode.linked_list;

import java.util.ArrayList;
import java.util.List;

public interface RemoveLinkedListElements {

    ListNode removeElements(ListNode head, int val);

    class Solution implements RemoveLinkedListElements {

        @Override
        public ListNode removeElements(ListNode head, int val) {
            List<ListNode> nodes = toListExcludingValue(head, val);
            return toHead(nodes);
        }

        private List<ListNode> toListExcludingValue(ListNode node, int val) {
            List<ListNode> nodes = new ArrayList<>();
            while (node != null) {
                if (node.val != val)
                    nodes.add(node);
                node = node.next;
            }
            return nodes;
        }

        private ListNode toHead(List<ListNode> list) {
            if (list.isEmpty()) return null;
            ListNode node = list.get(0);
            for (int i = 0; i < list.size(); i++) {
                ListNode n = node;
                for (int j = 1; j < i; j++) {
                    n = n.next;
                }
                n.next = list.get(i);
                n.next.next = null;
            }
            return node;
        }
    }
}
