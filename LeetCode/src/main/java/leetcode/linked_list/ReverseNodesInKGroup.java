package leetcode.linked_list;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public interface ReverseNodesInKGroup {

    ListNode reverseKGroup(ListNode head, int k);

    class Solution implements ReverseNodesInKGroup {

        @Override
        public ListNode reverseKGroup(ListNode head, int k) {
            Queue<Stack<Integer>> valsQueue = new LinkedList<>();
            ListNode node = head;
            while (hasNextK(node, k)) {
                Stack<Integer> vals = new Stack<>();
                for (int i = 0; i < k; i++) {
                    vals.push(node.val);
                    node = node.next;
                }
                valsQueue.offer(vals);
            }
            Queue<Integer> tail = new LinkedList<>();
            while (node != null) {
                tail.offer(node.val);
                node = node.next;
            }
            ListNode dummy = new ListNode(0);
            ListNode reversed = dummy;
            while (!valsQueue.isEmpty()) {
                Stack<Integer> vals = valsQueue.poll();
                while (!vals.isEmpty()) {
                    reversed.next = new ListNode(vals.peek());
                    vals.pop();
                    reversed = reversed.next;
                }
            }
            while (!tail.isEmpty()) {
                reversed.next = new ListNode(tail.poll());
                reversed = reversed.next;
            }
            return dummy.next;
        }

        private boolean hasNextK(ListNode head, int k) {
            ListNode node = head;
            int count = 0;
            while (node != null) {
                count++;
                if (count >= k)
                    return true;
                node = node.next;
            }
            return false;
        }
    }
}
