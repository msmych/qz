package leetcode.linked_list;

public class ListNode {
    
    public int val;
    public ListNode next;

    public ListNode(int x) {
        val = x;
        next = null;
    }

    public static ListNode fromVals(int... vals) {
        if (vals.length == 0) return null;
        ListNode dummy = new ListNode(0);
        ListNode node = dummy;
        for (int val : vals) {
            node.next = new ListNode(val);
            node = node.next;
        }
        return dummy.next;
    }

    public static boolean equalVals(ListNode head, int... vals) {
        ListNode node = head;
        for (int val : vals) {
            if (node == null || node.val != val)
                return false;
            node = node.next;
        }
        return node == null;
    }
}
