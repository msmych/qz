package leetcode.linked_list;

import java.util.ArrayList;
import java.util.List;

public interface ReverseLinkedListII {

    ListNode reverseBetween(ListNode head, int m, int n);

    class Solution implements ReverseLinkedListII {

        @Override
        public ListNode reverseBetween(ListNode head, int m, int n) {
            List<ListNode> list = new ArrayList<>();
            while (head != null) {
                list.add(head);
                head = head.next;
            }
            for (int i = 0; i <= (n - m) / 2; i++) {
                ListNode node = list.get(m - 1 + i);
                list.set(m - 1 + i, list.get(n - 1 - i));
                list.set(n - 1 - i, node);
            }
            return toListNode(list);
        }

        private ListNode toListNode(List<ListNode> list) {
            ListNode node = list.get(0);
            for (int i = 0; i < list.size(); i++) {
                ListNode n = node;
                for (int j = 1; j < i; j++) {
                    n = n.next;
                }
                n.next = list.get(i);
                n.next.next = null;
            }
            return node;
        }
    }
}
