package leetcode.linked_list;

import java.util.HashSet;
import java.util.Set;

public interface LinkedListCycle {

    boolean hasCycle(ListNode head);

    class Solution implements LinkedListCycle {

        @Override
        public boolean hasCycle(ListNode head) {
            Set<ListNode> nodes = new HashSet<>();
            ListNode node = head;
            while (node != null) {
                if (nodes.contains(node)) {
                    return true;
                }
                nodes.add(node);
                node = node.next;
            }
            return false;
        }
    }

    class TwoPointersSolution implements LinkedListCycle {

        @Override
        public boolean hasCycle(ListNode head) {
            if (head == null || head.next == null) return false;
            ListNode slow = head;
            ListNode fast = head.next;
            while (slow != fast) {
                if (fast == null || fast.next == null) return false;
                slow = slow.next;
                fast = fast.next.next;
            }
            return true;
        }
    }
}
