package leetcode.linked_list;

public interface AddTwoNumbers {

    ListNode addTwoNumbers(ListNode l1, ListNode l2);

    class Solution implements AddTwoNumbers {

        @Override
        public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
            ListNode sum = new ListNode(0);
            ListNode next = sum;
            sum.next = next;
            boolean shift = false;
            while (hasNext(l1, l2, shift)) {
                ListNode listNode1 = l1 != null ? l1 : new ListNode(0);
                ListNode listNode2 = l2 != null ? l2 : new ListNode(0);

                int number = listNode1.val + listNode2.val;
                if (shift) number++;
                shift = number >= 10;

                l1 = listNode1.next;
                l2 = listNode2.next;

                next.val = number % 10;
                next.next = hasNext(l1, l2, shift) ? new ListNode(0) : null;
                next = next.next;
            }
            return sum;
        }

        private boolean hasNext(ListNode l1, ListNode l2, boolean shift) {
            return l1 != null || l2 != null || shift;
        }
    }
}
