package leetcode.linked_list;

public interface RemoveDuplicatesFromSortedList {

    ListNode deleteDuplicates(ListNode head);

    class Solution implements RemoveDuplicatesFromSortedList {

        @Override
        public ListNode deleteDuplicates(ListNode head) {
            if (head == null || head.next == null)
                return head;
            while (head.next != null && head.val == head.next.val)
                head.next = head.next.next;
            head.next = deleteDuplicates(head.next);
            return head;
        }
    }

    class SolutionII implements RemoveDuplicatesFromSortedList {

        @Override
        public ListNode deleteDuplicates(ListNode head) {
            if (head == null || head.next == null)
                return head;
            if (head.val == head.next.val) {
                int val = head.val;
                while (head != null && head.val == val) {
                    head = head.next;
                }
            } else {
                head.next = deleteDuplicates(head.next);
                return head;
            }
            return deleteDuplicates(head);
        }
    }
}
