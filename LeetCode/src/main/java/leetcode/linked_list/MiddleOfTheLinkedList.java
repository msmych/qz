package leetcode.linked_list;

import java.util.ArrayList;
import java.util.List;

public interface MiddleOfTheLinkedList {

    ListNode middleNode(ListNode head);

    class Solution implements MiddleOfTheLinkedList {

        @Override
        public ListNode middleNode(ListNode head) {
            List<ListNode> nodes = new ArrayList<>();
            ListNode node = head;
            while (node != null) {
                nodes.add(node);
                node = node.next;
            }
            return nodes.get(nodes.size() / 2);
        }
    }
}
