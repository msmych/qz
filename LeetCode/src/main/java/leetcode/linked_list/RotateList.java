package leetcode.linked_list;

public interface RotateList {

    ListNode rotateRight(ListNode head, int k);

    class Solution implements RotateList {

        private final ListNode firstDummy = new ListNode(0);
        private final ListNode secondDummy = new ListNode(0);

        private ListNode first = firstDummy;
        private ListNode second = secondDummy;

        @Override
        public ListNode rotateRight(ListNode head, int k) {
            int n = countNodes(head);
            if (n == 0) return null;
            k = k % n;
            head = setSecondGetHead(head, n - k);
            getFirst(head);
            first.next = secondDummy.next;
            return firstDummy.next;
        }

        private int countNodes(ListNode head) {
            ListNode node = head;
            int n = 0;
            for (; node != null; n++) {
                node = node.next;
            }
            return n;
        }

        private ListNode setSecondGetHead(ListNode head, int last) {
            for (int i = 0; i < last; i++) {
                second.next = new ListNode(head.val);
                second = second.next;
                head = head.next;
            }
            return head;
        }

        private void getFirst(ListNode head) {
            while (head != null) {
                first.next = new ListNode(head.val);
                first = first.next;
                head = head.next;
            }
        }
    }
}
