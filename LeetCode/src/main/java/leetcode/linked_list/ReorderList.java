package leetcode.linked_list;

import java.util.ArrayList;
import java.util.List;

public interface ReorderList {

    void reorderList(ListNode head);

    class Solution implements ReorderList {

        @Override
        public void reorderList(ListNode head) {
            if (head == null) return;
            List<ListNode> nodes = new ArrayList<>();
            ListNode dummy = new ListNode(0);
            dummy.next = head;
            ListNode node = dummy.next;
            while (node != null) {
                nodes.add(node);
                node = node.next;
            }
            node = dummy.next;
            for (int i = 1; i < nodes.size(); i++) {
                if ((i & 1) == 0) node.next = nodes.get(i / 2);
                else node.next = nodes.get(nodes.size() - i / 2 - 1);
                node = node.next;
            }
            node.next = null;
        }
    }
}
