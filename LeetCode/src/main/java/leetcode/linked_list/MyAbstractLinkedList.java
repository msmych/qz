package leetcode.linked_list;

public abstract class MyAbstractLinkedList {

    /** Initialize your data structure here. */
    public MyAbstractLinkedList() {}

    /** Get the value of the index-th node in the linked list. If the index is invalid, return -1. */
    public abstract int get(int index);

    /** Add a node of value val before the first element of the linked list.
     * After the insertion, the new node will be the first node of the linked list. */
    public abstract void addAtHead(int val);

    /** Append a node of value val to the last element of the linked list. */
    public abstract void addAtTail(int val);

    /** Add a node of value val before the index-th node in the linked list.
     * If index equals to the length of linked list, the node will be appended to the end of linked list.
     * If index is greater than the length, the node will not be inserted. */
    public abstract void addAtIndex(int index, int val);

    /** Delete the index-th node in the linked list, if the index is valid. */
    public abstract void deleteAtIndex(int index);
}

/**
 * Your MySinglyLinkedList object will be instantiated and called as such:
 * MySinglyLinkedList obj = new MySinglyLinkedList();
 * int param_1 = obj.get(index);
 * obj.addAtHead(val);
 * obj.addAtTail(val);
 * obj.addAtIndex(index,val);
 * obj.deleteAtIndex(index);
 */

