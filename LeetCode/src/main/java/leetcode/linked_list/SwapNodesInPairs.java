package leetcode.linked_list;

public interface SwapNodesInPairs {

    ListNode swapPairs(ListNode head);

    class Solution implements SwapNodesInPairs {

        @Override
        public ListNode swapPairs(ListNode head) {
            if (head == null || head.next == null)
                return head;
            ListNode temp = head, tail = head.next.next;
            head = head.next;
            head.next = temp;
            head.next.next = swapPairs(tail);
            return head;
        }
    }
}
