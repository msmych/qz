package leetcode.linked_list;

class RandomListNode {

    int label;
    RandomListNode next, random;

    RandomListNode(int x) { this.label = x; }
}
