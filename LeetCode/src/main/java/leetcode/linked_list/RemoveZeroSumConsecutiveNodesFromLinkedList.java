package leetcode.linked_list;

public interface RemoveZeroSumConsecutiveNodesFromLinkedList {

    ListNode removeZeroSumSublists(ListNode head);

    class Solution implements RemoveZeroSumConsecutiveNodesFromLinkedList {

        @Override
        public ListNode removeZeroSumSublists(ListNode head) {
            return null;
        }
    }
}
