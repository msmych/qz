package leetcode.linked_list;

public interface ReverseLinkedList {

    ListNode reverseList(ListNode head);

    class IterativeSolution implements ReverseLinkedList {

        @Override
        public ListNode reverseList(ListNode head) {
            ListNode previous = null;
            ListNode node = head;
            while (node != null) {
                ListNode next = node.next;
                node.next = previous;
                previous = node;
                node = next;
            }
            return previous;
        }
    }

    class RecursiveSolution implements ReverseLinkedList {

        @Override
        public ListNode reverseList(ListNode head) {
            return reverseNext(head, null);
        }

        private ListNode reverseNext(ListNode head, ListNode tail) {
            if (head == null)
                return tail;
            ListNode node = new ListNode(head.val);
            node.next = tail;
            tail = node;
            head = head.next;
            return reverseNext(head, tail);
        }
    }
}
