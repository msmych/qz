package leetcode.linked_list;

import static java.lang.Integer.parseInt;

public interface ConvertBinaryNumberInALinkedListToInteger {

  int getDecimalValue(ListNode head);

  class Solution implements ConvertBinaryNumberInALinkedListToInteger {
    @Override
    public int getDecimalValue(ListNode head) {
      StringBuilder sb = new StringBuilder();
      while (head != null) {
        sb.append(head.val);
        head = head.next;
      }
      return parseInt(sb.toString(), 2);
    }
  }
}
