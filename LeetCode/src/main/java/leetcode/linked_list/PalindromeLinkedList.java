package leetcode.linked_list;

import java.util.ArrayList;
import java.util.List;

public interface PalindromeLinkedList {

    boolean isPalindrome(ListNode head);

    class Solution implements PalindromeLinkedList {

        @Override
        public boolean isPalindrome(ListNode head) {
            List<Integer> vals = new ArrayList<>();
            ListNode node = head;
            while (node != null) {
                vals.add(node.val);
                node = node.next;
            }
            int i = 0;
            int j = vals.size() - 1;
            while (i <= j) {
                if (!vals.get(i).equals(vals.get(j))) return false;
                i++;
                j--;
            }
            return true;
        }
    }
}
