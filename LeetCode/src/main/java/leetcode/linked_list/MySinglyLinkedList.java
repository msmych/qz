package leetcode.linked_list;

public class MySinglyLinkedList extends MyAbstractLinkedList {

    class Entry {
        int value;
        Entry next;

        Entry(int value, Entry next) {
            this.value = value;
            this.next = next;
        }
    }

    private Entry head;

    public MySinglyLinkedList() {}

    @Override
    public int get(int index) {
        Entry entry = head;
        for (int i = 0; i < index; i++) {
            if (entry != null) {
                entry = entry.next;
            } else return -1;
        }
        return entry != null ? entry.value : -1;
    }

    @Override
    public void addAtHead(int val) {
        head = new Entry(val, head);
    }

    @Override
    public void addAtTail(int val) {
        Entry entry = head;
        while (entry.next != null) {
            entry = entry.next;
        }
        entry.next = new Entry(val, null);
    }

    @Override
    public void addAtIndex(int index, int val) {
        if (head == null && index == 0) {
            head = new Entry(val, null);
            return;
        }
        Entry entry = head;
        for (int i = 1; i < index; i++) {
            if (entry != null) {
                entry = entry.next;
            } else return;
        }
        if (entry != null)
            entry.next = new Entry(val, entry.next);
    }

    @Override
    public void deleteAtIndex(int index) {
        if (head != null && index == 0) {
            head = head.next;
            return;
        }
        Entry entry = head;
        for (int i = 1; i < index; i++) {
            if (entry != null) {
                entry = entry.next;
            } else return;
        }
        if (entry != null && entry.next != null)
            entry.next = entry.next.next;
    }
}
