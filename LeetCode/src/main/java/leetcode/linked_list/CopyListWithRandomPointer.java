package leetcode.linked_list;

import java.util.HashMap;
import java.util.Map;

public interface CopyListWithRandomPointer {

  RandomListNode copyRandomList(RandomListNode head);

  class Solution implements CopyListWithRandomPointer {

    private final Map<RandomListNode, RandomListNode> visited = new HashMap<>();

    @Override
    public RandomListNode copyRandomList(RandomListNode head) {
      if (head == null) return null;
      if (visited.containsKey(head)) {
        return visited.get(head);
      }
      RandomListNode copy = new RandomListNode(head.label);
      visited.put(head, copy);
      copy.next = copyRandomList(head.next);
      copy.random = copyRandomList(head.random);
      return copy;
    }
  }
}
