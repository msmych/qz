package leetcode.linked_list;

import java.util.HashSet;
import java.util.Set;

public interface LinkedListCycleII {

    ListNode detectCycle(ListNode head);

    class Solution implements LinkedListCycleII {

        @Override
        public ListNode detectCycle(ListNode head) {
            if (head == null) return null;
            ListNode node = head;
            Set<ListNode> nodes = new HashSet<>();
            nodes.add(node);
            node = node.next;
            while (node != null) {
                if (nodes.contains(node)) {
                    return node;
                }
                nodes.add(node);
                node = node.next;
            }
            return null;
        }
    }
}
