package leetcode.graph;

import leetcode.tree.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

public interface MaximumLevelSumOfABinaryTree {

  int maxLevelSum(TreeNode root);

  class Solution implements MaximumLevelSumOfABinaryTree {
    @Override
    public int maxLevelSum(TreeNode root) {
      if (root == null) {
        return 0;
      }
      Queue<TreeNode> queue = new LinkedList<>();
      queue.offer(root);
      int max = Integer.MIN_VALUE;
      int maxLevel = 1;
      int level = 1;
      while (!queue.isEmpty()) {
        int size = queue.size();
        int sum = 0;
        for (; size > 0; size--) {
          TreeNode node = queue.poll();
          sum += node.val;
          if (node.left != null) {
            queue.offer(node.left);
          }
          if (node.right != null) {
            queue.offer(node.right);
          }
        }
        if (sum > max) {
          max = sum;
          maxLevel = level;
        }
        level++;
      }
      return maxLevel;
    }
  }
}
