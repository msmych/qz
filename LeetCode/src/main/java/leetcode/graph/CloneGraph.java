package leetcode.graph;

public interface CloneGraph {

    UndirectedGraphNode cloneGraph(UndirectedGraphNode node);

    class Solution implements CloneGraph {

        @Override
        public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
            return null;
        }
    }
}
