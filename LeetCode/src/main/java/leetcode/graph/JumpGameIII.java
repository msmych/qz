package leetcode.graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toSet;

public interface JumpGameIII {

  boolean canReach(int[] arr, int start);

  class Solution implements JumpGameIII {
    @Override
    public boolean canReach(int[] arr, int start) {
      Queue<Integer> queue = new LinkedList<>();
      queue.offer(start);
      Set<Integer> zeroes = IntStream.range(0, arr.length).filter(i -> arr[i] == 0).boxed().collect(toSet());
      Set<Integer> visited = new HashSet<>();
      visited.add(start);
      while (!queue.isEmpty()) {
        int i = queue.poll();
        visited.add(i);
        if (zeroes.contains(i)) {
          return true;
        }
        if (i + arr[i] < arr.length && !visited.contains(i + arr[i])) {
          queue.offer(i + arr[i]);
        }
        if (i - arr[i] >= 0 && !visited.contains(i - arr[i])) {
          queue.offer(i - arr[i]);
        }
      }
      return false;
    }
  }
}
