package leetcode.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UndirectedGraphNode {
    int label;
    List<UndirectedGraphNode> neighbors;
    UndirectedGraphNode(int x) { label = x; neighbors = new ArrayList<>(); }

    static UndirectedGraphNode fromLabelAndNeighbors(int label, UndirectedGraphNode... neighbors) {
        UndirectedGraphNode node = new UndirectedGraphNode(label);
        node.neighbors.addAll(Arrays.asList(neighbors));
        return node;
    }
}
