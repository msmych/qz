package leetcode.graph;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface FindTheTownJudge {

    int findJudge(int N, int[][] trust);

    class Solution implements FindTheTownJudge {

        @Override
        public int findJudge(int N, int[][] trust) {
            Set<Integer> judges = IntStream.range(1, N + 1)
                    .filter(n -> Arrays.stream(trust)
                            .noneMatch(t -> t[0] == n))
                    .filter(n -> Arrays.stream(trust)
                            .filter(t -> t[1] == n)
                            .mapToInt(t -> t[0])
                            .distinct()
                            .count() == N - 1)
                    .boxed()
                    .collect(Collectors.toSet());
            return judges.size() == 1 ? judges.iterator().next() : -1;
        }
    }
}
