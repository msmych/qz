package leetcode.graph;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public interface FlowerPlantingWithNoAdjacent {

    int[] gardenNoAdj(int N, int[][] paths);

    class Solution implements FlowerPlantingWithNoAdjacent {

        private final Map<Integer, List<Integer>> adjacent = new HashMap<>();
        private final Map<Integer, Integer> flowers = new HashMap<>();

        @Override
        public int[] gardenNoAdj(int N, int[][] paths) {
            IntStream.range(1, N + 1).forEach(n -> adjacent.put(n, new ArrayList<>()));
            Arrays.stream(paths).forEach(p -> {
                adjacent.get(p[0]).add(p[1]);
                adjacent.get(p[1]).add(p[0]);
            });
            IntStream.range(1, N + 1).forEach(this::plant);
            return flowers.entrySet().stream()
                    .sorted(Comparator.comparingInt(Map.Entry::getKey))
                    .mapToInt(Map.Entry::getValue)
                    .toArray();
        }

        private void plant(int i) {
            List<Integer> available = Stream.of(1, 2, 3, 4).collect(Collectors.toList());
            available.removeAll(adjacent.get(i).stream()
                    .filter(flowers::containsKey)
                    .map(flowers::get)
                    .collect(Collectors.toList()));
            flowers.put(i, available.get(0));
        }
    }
}
