package leetcode.graph;

import java.util.HashSet;
import java.util.Set;

public interface CountServersThatCommunicate {

  int countServers(int[][] grid);

  class Solution implements CountServersThatCommunicate {

    private final Set<Integer> visited = new HashSet<>();
    private int[][] grid;
    private int communicating = 0;

    @Override
    public int countServers(int[][] grid) {
      this.grid = grid;
      for (int i = 0; i < grid.length; i++) {
        for (int j = 0; j < grid[i].length; j++) {
          if (grid[i][j] != 0) {
            startFrom(i, j);
          }
        }
      }
      return communicating;
    }

    private void startFrom(int i, int j) {
      int cell = 1000 * i + j;
      if (visited.contains(cell)) {
        return;
      }
      visited.add(cell);
      int connected = 0;
      for (int row = 0; row < grid.length; row++) {
        if (row != i && grid[row][j] == 1) {
          connected += connected(row, j);
        }
      }
      for (int column = 0; column < grid[i].length; column++) {
        if (column != j && grid[i][column] == 1) {
          connected += connected(i, column);
        }
      }
      if (connected > 0) {
        connected++;
      }
      communicating += connected;
    }

    private int connected(int i, int j) {
      int cell = 1000 * i + j;
      if (visited.contains(cell)) {
        return 0;
      }
      visited.add(cell);
      int connected = 1;
      for (int row = 0; row < grid.length; row++) {
        if (row != i && grid[row][j] == 1) {
          connected += connected(row, j);
        }
      }
      for (int column = 0; column < grid[i].length; column++) {
        if (column != j && grid[i][column] == 1) {
          connected += connected(i, column);
        }
      }
      return connected;
    }
  }
}
