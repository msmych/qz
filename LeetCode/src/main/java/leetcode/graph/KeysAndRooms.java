package leetcode.graph;

import java.util.*;

public interface KeysAndRooms {

    boolean canVisitAllRooms(List<List<Integer>> rooms);

    class Solution implements KeysAndRooms {

        private final Queue<Integer> keys = new LinkedList<>();
        private final Set<Integer> visited = new HashSet<>();

        @Override
        public boolean canVisitAllRooms(List<List<Integer>> rooms) {
            keys.offer(0);
            while (!keys.isEmpty()) {
                int key = keys.poll();
                if (visited.contains(key))
                    continue;
                visited.add(key);
                for (int k : rooms.get(key))
                    keys.offer(k);
            }
            return visited.size() == rooms.size();
        }
    }
}
