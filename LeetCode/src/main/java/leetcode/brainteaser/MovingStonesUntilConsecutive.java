package leetcode.brainteaser;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface MovingStonesUntilConsecutive {

    int[] numMovesStones(int a, int b, int c);

    class Solution implements MovingStonesUntilConsecutive {

        @Override
        public int[] numMovesStones(int a, int b, int c) {
            List<Integer> stones = Stream.of(a, b, c).sorted().collect(Collectors.toList());
            int ab = stones.get(1) - stones.get(0);
            int bc = stones.get(2) - stones.get(1);
            if (ab == 1 && bc == 1) return new int[]{0, 0};
            if (ab == 1) return new int[]{1, bc - 1};
            if (bc == 1) return new int[]{1, ab - 1};
            if (ab == 2) return new int[]{1, bc};
            if (bc == 2) return new int[]{1, ab};
            return new int[]{2, (ab - 1) + (bc - 1)};
        }
    }
}
