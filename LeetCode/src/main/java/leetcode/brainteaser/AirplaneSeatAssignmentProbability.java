package leetcode.brainteaser;

public interface AirplaneSeatAssignmentProbability {

  double nthPersonGetsNthSeat(int n);

  class Solution implements AirplaneSeatAssignmentProbability {
    @Override
    public double nthPersonGetsNthSeat(int n) {
      return n == 1 ? 1.0 : 0.5;
    }
  }
}
