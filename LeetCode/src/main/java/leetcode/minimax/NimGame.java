package leetcode.minimax;

public interface NimGame {

    boolean canWinNim(int n);

    class Solution implements NimGame {

        @Override
        public boolean canWinNim(int n) {
            return (n - 4) % 4 != 0;
        }
    }
}
