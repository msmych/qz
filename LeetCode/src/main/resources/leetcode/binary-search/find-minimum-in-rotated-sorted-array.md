# [Find Minimum in Rotated Sorted Array](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/)
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e.,  `[0,1,2,4,5,6,7]` might become  `[4,5,6,7,0,1,2]`).

Find the minimum element.

You may assume no duplicate exists in the array.

**Example 1:**
```
Input: [3,4,5,1,2] 
Output: 1
```
**Example 2:**
```
Input: [4,5,6,7,0,1,2]
Output: 0
```

# [II](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array-ii/)
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e.,  `[0,1,2,4,5,6,7]` might become  `[4,5,6,7,0,1,2]`).

Find the minimum element.

The array may contain duplicates.

**Example 1:**
```
Input: [1,3,5]
Output: 1
```
**Example 2:**
```
Input: [2,2,2,0,1]
Output: 0
```
**Note:**

* Would allow duplicates affect the run-time complexity? How and why?
