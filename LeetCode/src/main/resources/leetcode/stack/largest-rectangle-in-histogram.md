# [Largest Rectangle in Histogram](https://leetcode.com/problems/largest-rectangle-in-histogram/)
Given *n* non-negative integers representing the histogram's bar height where the width of each bar is 1, 
find the area of largest rectangle in the histogram.

```
   6
  5O
  XX
  XX 3
2 XX2O
O1XXOO
OOXXOO
```
Above is a histogram where width of each bar is 1, 
given height = `[2,1,5,6,2,3]`.
The largest rectangle is shown in the shaded area, which has area = `10` unit.

**Example:**
```
Input: [2,1,5,6,2,3]
Output: 10
```
