# [Subsets](https://leetcode.com/problems/subsets/)
Given a set of **distinct** integers, *nums*, 
return all possible subsets (the power set).

**Note:** The solution set must not contain duplicate subsets.

**Example:**
```
Input: nums = [1,2,3]
Output:
[
  [3],
  [1],
  [2],
  [1,2,3],
  [1,3],
  [2,3],
  [1,2],
  []
]
```

# [II](https://leetcode.com/problems/subsets-ii/)
Given a collection of integers that might contain duplicates, **nums**, 
return all possible subsets (the power set).

**Note:** The solution set must not contain duplicate subsets.

**Example:**
```
Input: [1,2,2]
Output:
[
  [2],
  [1],
  [1,2,2],
  [2,2],
  [1,2],
  []
]
```
