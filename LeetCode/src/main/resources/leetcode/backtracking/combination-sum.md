# [Combination Sum](https://leetcode.com/problems/combination-sum/)
Given a **set** of candidate numbers (`candidates`) (**without duplicates**) 
and a target number (`target`), 
find all unique combinations in `candidates` 
where the candidate numbers sums to `target`.

The **same** repeated number may be chosen from `candidates` 
unlimited number of times.

**Note:**

* All numbers (including `target`) will be positive integers.
* The solution set must not contain duplicate combinations.

**Example 1:**
```
Input: candidates = [2,3,6,7], target = 7,
A solution set is:
[
  [7],
  [2,2,3]
]
```
**Example 2:**
```
Input: candidates = [2,3,5], target = 8,
A solution set is:
[
  [2,2,2,2],
  [2,3,3],
  [3,5]
]
```

# [II](https://leetcode.com/problems/combination-sum-ii/)
Given a **set** of candidate numbers (`candidates`) 
and a target number (`target`), 
find all unique combinations in `candidates` 
where the candidate numbers sums to `target`.

Each number in `candidates` may only be used **once** in the combination.

**Note:**

* All numbers (including `target`) will be positive integers.
* The solution set must not contain duplicate combinations.

**Example 1:**
```
Input: candidates = [10,1,2,7,6,1,5], target = 8,
A solution set is:
[
  [1, 7],
  [1, 2, 5],
  [2, 6],
  [1, 1, 6]
]
```
**Example 2:**
```
Input: candidates = [2,5,2,1,2], target = 5,
A solution set is:
[
  [1,2,2],
  [5]
]
```
