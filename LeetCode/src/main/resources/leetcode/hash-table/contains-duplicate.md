# [Contains Duplicate](https://leetcode.com/problems/contains-duplicate/)
Given an array of integers, find if the array contains any duplicates.

Your function should return true if any value appears at least twice in the array, and it should return false if every element is distinct.

**Example 1:**
```
Input: [1,2,3,1]
Output: true
```
**Example 2:**
```
Input: [1,2,3,4]
Output: false
```
**Example 3:**
```
Input: [1,1,1,3,3,4,3,2,4,2]
Output: true
```

# [II](https://leetcode.com/problems/contains-duplicate-ii/)
Given an array of integers and an integer *k*, 
find out whether there are two distinct indices *i* and *j* in the array 
such that **nums[i]** = **nums[j]** and the **absolute** difference between *i* and *j* is at most *k*.

**Example 1:**
```
Input: nums = [1,2,3,1], k = 3
Output: true
```
**Example 2:**
```
Input: nums = [1,0,1,1], k = 1
Output: true
```
**Example 3:**
```
Input: nums = [1,2,3,1,2,3], k = 2
Output: false
```

# [III](https://leetcode.com/problems/contains-duplicate-iii/)
Given an array of integers, 
find out whether there are two distinct indices *i* and *j* in the array 
such that the **absolute** difference between **nums[i]** and **nums[j]** is at most *t* 
and the **absolute** difference between *i* and *j* is at most *k*.

**Example 1:**
```
Input: nums = [1,2,3,1], k = 3, t = 0
Output: true
```
**Example 2:**
```
Input: nums = [1,0,1,1], k = 1, t = 2
Output: true
```
**Example 3:**
```
Input: nums = [1,5,9,1,5,9], k = 2, t = 3
Output: false
```
