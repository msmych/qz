# [Isomorphic String](https://leetcode.com/problems/isomorphic-strings/)
Given two strings _**s**_ and _**t**_, determine if they are isomorphic.

Two strings are isomorphic if the characters in _**s**_ can be replaced to get _**t**_.

All occurrences of a character must be replaced with another character while preserving the order of characters. 
No two characters may map to the same character but a character may map to itself.

**Example 1:**
```
Input: s = "egg", t = "add"
Output: true
```
**Example 2:**
```
Input: s = "foo", t = "bar"
Output: false
```
**Example 3:**
```
Input: s = "paper", t = "title"
Output: true
```
**Note:**

You may assume both _**s**_ and _**t**_ have the same length.
