# [Sort Array By Parity](https://leetcode.com/problems/sort-array-by-parity/)
Given an array `A` of non-negative integers, 
return an array consisting of all the even elements of `A`, 
followed by all the odd elements of `A`.

You may return any answer array that satisfies this condition.

**Example 1:**
```
Input: [3,1,2,4]
Output: [2,4,3,1]
The outputs [4,2,3,1], [2,4,1,3], and [4,2,1,3] would also be accepted.
```

**Note:**

1. `1 <= A.length <= 5000`
2. `0 <= A[i] <= 5000`

# [II](https://leetcode.com/problems/sort-array-by-parity-ii/)
Given an array `A` of non-negative integers, 
half of the integers in A are odd, 
and half of the integers are even.

Sort the array so that whenever `A[i]` is odd, `i` is odd; 
and whenever `A[i]` is even, `i` is even.

You may return any answer array that satisfies this condition.

**Example 1:**
```
Input: [4,2,5,7]
Output: [4,5,2,7]
Explanation: [4,7,2,5], [2,5,4,7], [2,7,4,5] would also have been accepted.
```

**Note:**

1. `2 <= A.length <= 20000`
2. `A.length % 2 == 0`
3. `0 <= A[i] <= 1000`
