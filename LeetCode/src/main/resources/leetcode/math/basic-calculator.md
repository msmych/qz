# [Basic Calculator](https://leetcode.com/problems/basic-calculator/)
Implement a basic calculator to evaluate a simple expression string.

The expression string may contain open `(` and closing parentheses `)`, 
the plus `+` or minus sign `-`, non-negative integers and empty spaces ` `.

**Example 1:**
```
Input: "1 + 1"
Output: 2
```
**Example 2:**
```
Input: " 2-1 + 2 "
Output: 3
```
**Example 3:**
```
Input: "(1+(4+5+2)-3)+(6+8)"
Output: 23
```
**Note:**
* You may assume that the given expression is always valid.
* **Do not** use the eval built-in library function.

# [II](https://leetcode.com/problems/basic-calculator-ii/)
Implement a basic calculator to evaluate a simple expression string.

The expression string contains only **non-negative** integers, 
`+`, `-`, `*`, `/` operators and empty spaces ` `. 
The integer division should truncate toward zero.

**Example 1:**
```
Input: "3+2*2"
Output: 7
```
**Example 2:**
```
Input: " 3/2 "
Output: 1
```
**Example 3:**
```
Input: " 3+5 / 2 "
Output: 5
```
**Note:**

* You may assume that the given expression is always valid.
* **Do not** use the eval built-in library function.
