# [Reverse Words in a String](https://leetcode.com/problems/reverse-words-in-a-string/description/)
Given an input string, reverse the string word by word.

**Example:**  
```
Input: "the sky is blue",
Output: "blue is sky the".
```
**Note:**

* A word is defined as a sequence of non-space characters.
* Input string may contain leading or trailing spaces. However, your reversed string should not contain leading or trailing spaces.
* You need to reduce multiple spaces between two words to a single space in the reversed string.

**Follow up:** For C programmers, try to solve it *in-place* in *O*(1) space.

# [III](https://leetcode.com/problems/reverse-words-in-a-string-iii/description/)
Given a string, you need to reverse the order of characters in each word within a sentence while still preserving whitespace and initial word order.

**Example 1:**
```
Input: "Let's take LeetCode contest"
Output: "s'teL ekat edoCteeL tsetnoc"
```
**Note:** In the string, each word is separated by single space and there will not be any extra space in the string.
