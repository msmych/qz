# [Linked List Cycle](https://leetcode.com/problems/linked-list-cycle/description/)
Given a linked list, determine if it has a cycle in it.

Follow up:

Can you solve it without using extra space?

# [II](https://leetcode.com/problems/linked-list-cycle-ii/description/)
Given a linked list, return the node where the cycle begins. 
If there is no cycle, return `null`.

**Note:** Do not modify the linked list.

**Follow up:**
Can you solve it without using extra space?
