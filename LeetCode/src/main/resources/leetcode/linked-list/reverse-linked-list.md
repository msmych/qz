# [Reverse Linked List](https://leetcode.com/problems/reverse-linked-list/description/)
Reverse a singly linked list.

**Example:**
```
Input: 1->2->3->4->5->NULL
Output: 5->4->3->2->1->NULL
```
**Follow up:**

A linked list can be reversed either iteratively or recursively. 
Could you implement both?

# [II](https://leetcode.com/problems/reverse-linked-list-ii/)
Reverse a linked list from position *m* to *n*. 
Do it in one-pass.

**Note:** 1 ≤ *m* ≤ *n* ≤ length of list.

**Example:**
```
Input: 1->2->3->4->5->NULL, m = 2, n = 4
Output: 1->4->3->2->5->NULL
```
