# [Valid Palindrome](https://leetcode.com/problems/valid-palindrome/description/)
Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.

**Note:** For the purpose of this problem, we define empty string as valid palindrome.

**Example 1:**
```
Input: "A man, a plan, a canal: Panama"
Output: true
```
**Example 2:**
```
Input: "race a car"
Output: false
```

# [II](https://leetcode.com/problems/valid-palindrome-ii/)
Given a non-empty string `s`, you may delete **at most** one character. 
Judge whether you can make it a palindrome.

**Example 1:**
```
Input: "aba"
Output: True
```
**Example 2:**
```
Input: "abca"
Output: True
Explanation: You could delete the character 'c'.
```
**Note:**

1. The string will only contain lowercase characters a-z. 
The maximum length of the string is 50000.
