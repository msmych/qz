# [Reverse String](https://leetcode.com/problems/reverse-string/description/)
Write a function that takes a string as input and returns the string reversed.

**Example 1:**
```
Input: "hello"
Output: "olleh"
```
**Example 2:**
```
Input: "A man, a plan, a canal: Panama"
Output: "amanaP :lanac a ,nalp a ,nam A"
```

# [II](https://leetcode.com/problems/reverse-string-ii/)
Given a string and an integer k, 
you need to reverse the first k characters 
for every 2k characters counting from the start of the string. 
If there are less than k characters left, reverse all of them. 
If there are less than 2k but greater than or equal to k characters, 
then reverse the first k characters and left the other as original.

**Example:**
```
Input: s = "abcdefg", k = 2
Output: "bacdfeg"
```
**Restrictions:**
1. The string consists of lower English letters only.
2. Length of the given string and k will in the range [1, 10000]
