package quicksort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QuicksortTest {

    private Quicksort quicksort = new Quicksort();

    @Test void arr594653() {
        int[] arr = new int[]{5, 9, 4, 6, 5, 3};
        quicksort.quickSort(arr);
        assertArrayEquals(new int[]{3, 4, 5, 5, 6, 9}, arr);
    }
}