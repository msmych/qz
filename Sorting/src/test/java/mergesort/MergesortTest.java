package mergesort;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MergesortTest {

    private Mergesort mergesort = new Mergesort();

    @Test void arr15328764() {
        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8}, mergesort.topDown(new int[]{1, 5, 3, 2, 8, 7, 6, 4}));
    }
}