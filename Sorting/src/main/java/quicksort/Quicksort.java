package quicksort;

public class Quicksort {

    public void quickSort(int[] arr) {
        qSort(arr, 0, arr.length - 1);
    }

    private void qSort(int[] arr, int begin, int end) {
        if (begin >= end) return;
        int partition = partition(arr, begin, end);
        qSort(arr, begin, partition - 1);
        qSort(arr, partition + 1, end);
    }

    private int partition(int[] arr, int begin, int end) {
        int pivot = arr[end];
        int i = begin;
        for (int j = begin; j < end; j++) {
            if (arr[j] >= pivot) continue;
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
            i++;
        }
        int temp = arr[i];
        arr[i] = arr[end];
        arr[end] = temp;
        return i;
    }
}
