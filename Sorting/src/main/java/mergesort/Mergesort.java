package mergesort;

import java.util.Arrays;

public class Mergesort {

    public int[] topDown(int[] arr) {
        if (arr.length <= 1) return arr;
        int pivot = arr.length / 2;
        int[] left = topDown(Arrays.copyOfRange(arr, 0, pivot));
        int[] right = topDown(Arrays.copyOfRange(arr, pivot, arr.length));
        return merge(left, right);
    }

    private int[] merge(int[] left, int[] right) {
        int[] sorted = new int[left.length + right.length];
        int leftIndex = 0;
        int rightIndex = 0;
        int index = 0;
        while (leftIndex < left.length && rightIndex < right.length) {
            if (left[leftIndex] < right[rightIndex])
                sorted[index++] = left[leftIndex++];
            else
                sorted[index++] = right[rightIndex++];
        }
        while (leftIndex < left.length) {
            sorted[index++] = left[leftIndex++];
        }
        while (rightIndex < right.length) {
            sorted[index++] = right[rightIndex++];
        }
        return sorted;
    }
}
