# Possible phrase
Given a list of word sequences, 
return a phrase that respects the partial words order,
or "Impossible" if such phrase not exists

**Examples:**
```
["fire walk", "walk with", "with me"] -> "fire walk with me"
["i", "love", "you"] -> "i love you"
["really hate", "hate programming", "programming really"] -> "Impossible"
```
