# Closest mutual parent
Given two nodes,
return its closest mutual parent,
or `null` if it not exists

**Example 1:**
```
Node 1: { parent: { parent: PARENT } }
Node 2: { parent: PARENT }
Output: PARENT
```

**Example 2:**
```
Node 1: { parent: { parent: ROOT1 } }
Node 2: { parent: { parent: ROOT2 } }
Output: null
```
