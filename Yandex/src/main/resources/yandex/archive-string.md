# Archive string
Given an alphanumerical string,
return a string where every sequence of the same letters is replaced
with the letter and the number of occurrences

**Example:**
```
Input: aabcccbb
Output: a2bc3b2
```
