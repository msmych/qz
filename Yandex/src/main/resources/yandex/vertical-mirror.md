# Vertical mirror
Given a set of points on a plane,
check if there is a vertical line
such that every point would have its "twin"
on the other side of the line

**Example 1:**
```
Input: [{0, 0}, {1, 1}, {3, 1}, {4, 0}]
Output: true
```

**Example 2:**
```
Input: [{-1, -1}, {0, 0}, {1, 1}]
Output: false
```
