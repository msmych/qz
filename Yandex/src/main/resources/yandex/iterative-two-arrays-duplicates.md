# Iterative two arrays duplicates
Given two arrays of numbers,
return an array where each number represents
a number of duplicates in an array 
consisting of two subarrays of initial arrays 
from the beginning to the index

**Example:**
```
Nums 1: [ 1, 3, 5, 6 ]
Nums 2: [ 2, 4, 3, 6 ]
Output: [ 0, 0, 1, 2 ]
```
