package yandex;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoubleIterator<E> implements Iterator<E> {

    private final Iterator<E> iter1;
    private final Iterator<E> iter2;

    DoubleIterator(Iterator<E> iter1, Iterator<E> iter2) {
        this.iter1 = iter1;
        this.iter2 = iter2;
    }

    @Override
    public boolean hasNext() {
        if (iter1.hasNext())
            return true;
        return iter2.hasNext();
    }

    @Override
    public E next() {
        if (iter1.hasNext())
            return iter1.next();
        if (iter2.hasNext())
            return iter2.next();
        throw new NoSuchElementException();
    }
}
