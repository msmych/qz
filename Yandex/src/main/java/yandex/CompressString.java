package yandex;

public interface CompressString {

    String archive(String letters);

    class Solution implements CompressString {

        @Override
        public String archive(String letters) {
            Character last = null;
            int ocurences = 0;
            StringBuilder sb = new StringBuilder();
            for (char letter : letters.toCharArray()) {
                if (last == null || last != letter) {
                    if (ocurences > 1)
                        sb.append(ocurences);
                    sb.append(letter);
                    last = letter;
                    ocurences = 1;
                } else {
                    ocurences++;
                }
            }
            if (ocurences > 0)
                sb.append(ocurences);
            return sb.toString();
        }
    }
}
