package yandex;

import java.util.HashSet;
import java.util.Set;

public interface ClosestMutualParent {

    class Node {

        Node parent;

        Node(Node parent) {
            this.parent = parent;
        }
    }

    Node getClosestMutualParent(Node node1, Node node2);

    class Solution implements ClosestMutualParent {

        @Override
        public Node getClosestMutualParent(Node node1, Node node2) {
            Set<Node> parents = new HashSet<>();
            while (node1 != null) {
                parents.add(node1);
                if (parents.contains(node2))
                    return node2;
                node1 = node1.parent;
            }
            while (node2 != null) {
                if (parents.contains(node2))
                    return node2;
                node2 = node2.parent;
            }
            return null;
        }
    }
}
