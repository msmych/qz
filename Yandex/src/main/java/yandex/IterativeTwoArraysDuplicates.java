package yandex;

import java.util.HashSet;
import java.util.Set;

public interface IterativeTwoArraysDuplicates {

    int[] getDuplicates(int[] nums1, int[] nums2);

    class Solution implements IterativeTwoArraysDuplicates {

        @Override
        public int[] getDuplicates(int[] nums1, int[] nums2) {
            int[] duplicates = new int[nums1.length];
            Set<Integer> nums = new HashSet<>();
            for (int i = 0; i < nums1.length; i++) {
                if (i > 0)
                    duplicates[i] = duplicates[i - 1];
                int num1= nums1[i];
                int num2= nums2[i];
                if (nums.contains(num1))
                    duplicates[i]++;
                else
                    nums.add(num1);
                if (nums.contains(num2))
                    duplicates[i]++;
                else
                    nums.add(num2);
            }
            return duplicates;
        }
    }
}
