package yandex;

import java.util.*;

public interface PossiblePhrase {

    String composePhrase(List<String> lines);

    class Solution implements PossiblePhrase {

        @Override
        public String composePhrase(List<String> lines) {
            Map<String, Set<String>> wordMap = new HashMap<>();
            for (String line : lines) {
                String[] words = line.split(" ");
                for (int i = 0; i < words.length; i++) {
                    String word = words[i];
                    wordMap.putIfAbsent(word, new HashSet<>());
                    wordMap.get(word).addAll(Arrays.asList(Arrays.copyOfRange(words, i + 1, words.length)));
                }
            }
            LinkedList<String> phrase = new LinkedList<>();
            while (!wordMap.isEmpty()) {
                Set<String> words = wordMap.keySet();
                boolean found = false;
                for (String word : words) {
                    if (wordMap.get(word).isEmpty()) {
                        phrase.addFirst(word);
                        wordMap.remove(word);
                        for (Set<String> nextWords : wordMap.values()) {
                            nextWords.remove(word);
                        }
                        found = true;
                        break;
                    }
                }
                if (!found)
                    return "Impossible";
            }
            return String.join(" ", phrase);
        }
    }
}
