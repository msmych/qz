package yandex;

import java.util.ArrayList;
import java.util.List;

public interface VerticalMirror {

    class Point {

        int x;
        int y;

        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    boolean existsMirror(List<Point> points);

    class Solution implements VerticalMirror {

        @Override
        public boolean existsMirror(List<Point> points) {
            points = new ArrayList<>(points);
            points.sort((p1, p2) -> p1.x > p2.x ? 1 : -1);
            Double mirror = null;
            do {
                Point left = points.get(0);
                Point right = points.get(points.size() - 1);
                if (left.y != right.y)
                    return false;
                if (mirror == null)
                    mirror = (right.x - left.x) / 2.0;
                else if (Double.valueOf((right.x - left.x) / 2.0).equals(mirror))
                    return false;
                points.remove(left);
                points.remove(right);
            } while (!points.isEmpty());
            return true;
        }
    }
}
