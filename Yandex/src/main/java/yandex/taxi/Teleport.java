package yandex.taxi;

import java.util.List;

public interface Teleport {

    int[] vehicleId(int n, String vertices, int m, List<String> tracks);

    class Solution implements Teleport {

        @Override
        public int[] vehicleId(int n, String vertices, int m, List<String> tracks) {
            return new int[0];
        }
    }
}
