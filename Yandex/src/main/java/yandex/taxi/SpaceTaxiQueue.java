package yandex.taxi;

import java.util.List;

public interface SpaceTaxiQueue {

    int minimalQueueLength(int n, List<String> points);

    class Solution implements SpaceTaxiQueue {

        @Override
        public int minimalQueueLength(int n, List<String> points) {
            return 0;
        }
    }
}
