package yandex.taxi;

import java.util.List;

public interface HotelTaxi {

    int[] maxGuests(int n, int m, List<String> actions);

    class Solution implements HotelTaxi {

        @Override
        public int[] maxGuests(int n, int m, List<String> actions) {
            return new int[0];
        }
    }
}
