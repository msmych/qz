package yandex.taxi;

import java.util.List;

public interface GroupingPointsOnLine {

    int group(int n, int r, List<Integer> points);

    class Solution implements GroupingPointsOnLine {

        @Override
        public int group(int n, int r, List<Integer> points) {
            return 0;
        }
    }
}
