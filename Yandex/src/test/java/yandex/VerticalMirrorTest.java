package yandex;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static yandex.VerticalMirror.Point;

class VerticalMirrorTest {

    private VerticalMirror verticalMirror = new VerticalMirror.Solution();

    @Test void input_00_11_31_40true() {
        assertTrue(verticalMirror.existsMirror(Arrays.asList(
                new Point(0, 0), new Point(1, 1), new Point(3, 1), new Point(4, 0))));
    }

    @Test void input_N1N1_00_11false() {
        assertFalse(verticalMirror.existsMirror(Arrays.asList(
                new Point(-1, -1), new Point(0, 0), new Point(1, 1))));
    }
}