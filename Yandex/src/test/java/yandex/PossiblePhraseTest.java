package yandex;

import org.junit.jupiter.api.Test;
import yandex.PossiblePhrase;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class PossiblePhraseTest {

    private PossiblePhrase possiblePhrase = new PossiblePhrase.Solution();

    @Test void fire_walk_with_me() {
        assertEquals("fire walk with me", possiblePhrase.composePhrase(
                Arrays.asList("fire walk", "walk with", "with me")));
    }

    @Test void i_love_you() {
        assertTrue(Arrays.asList("i love you", "i you love", "love i you", "love you i", "you i love", "you love i")
                .contains(possiblePhrase.composePhrase(Arrays.asList("i", "love", "you"))));
    }

    @Test void really_hate_programming() {
        assertEquals("Impossible", possiblePhrase.composePhrase(
                Arrays.asList("really hate", "hate programming", "programming really")));
    }
}