package yandex;

import org.junit.jupiter.api.Test;
import yandex.ClosestMutualParent;
import yandex.ClosestMutualParent.Node;

import static org.junit.jupiter.api.Assertions.*;

class ClosestMutualParentTest {

    private ClosestMutualParent closestMutualParent = new ClosestMutualParent.Solution();

    @Test void nodesWithMutualParent() {
        Node parent = new Node(null);
        Node node0 = new Node(parent);
        Node node1 = new Node(node0);
        Node node2 = new Node(parent);
        assertEquals(parent, closestMutualParent.getClosestMutualParent(node1, node2));
    }

    @Test void nodesWithoutMutualParent() {
        Node root1 = new Node(null);
        Node child1 = new Node(root1);
        Node node1 = new Node(child1);
        Node root2 = new Node(null);
        Node child2 = new Node(root2);
        Node node2 = new Node(child2);
        assertNull(closestMutualParent.getClosestMutualParent(node1, node2));
    }
}