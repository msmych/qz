package yandex.taxi;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GroupingPointsOnLineTest {

    private GroupingPointsOnLine groupingPointsOnLine = new GroupingPointsOnLine.Solution();

    @Test void input32_351output1() {
        assertEquals(1, groupingPointsOnLine.group(3, 2, Arrays.asList(3, 5, 1)));
    }

    @Test void input32_471output3() {
        assertEquals(3, groupingPointsOnLine.group(3, 2, Arrays.asList(4, 7, 1)));
    }

    @Test void input61_624351output2() {
        assertEquals(2, groupingPointsOnLine.group(6, 1, Arrays.asList(6, 2, 4, 3, 5, 1)));
    }
}