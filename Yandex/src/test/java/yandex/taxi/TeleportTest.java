package yandex.taxi;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class TeleportTest {

    private Teleport teleport = new Teleport.Solution();

    @Test void n4m4outputN1234N1() {
        assertArrayEquals(new int[]{-1, 2, 3, 4, -1},
                teleport.vehicleId(4, "-1 -1 -1 1 1 1 1 -1", 4, Arrays.asList(
                        "1 1 -2 1",
                        "1 2 0 0",
                        "1 3 0 0",
                        "1 4 1 1",
                        "5 0 2 4 5 6")));
    }

    @Test void n4m4outputN12134() {
        assertArrayEquals(new int[]{-1, 2, 1, 3, 4},
                teleport.vehicleId(4, "-1 -1 -1 1 1 1 1 -1", 4, Arrays.asList(
                        "2 1 -2 1 3 -1 1",
                        "1 2 0 0",
                        "1 3 0 0",
                        "1 4 1 1",
                        "5 0 2 4 5 6")));
    }
}