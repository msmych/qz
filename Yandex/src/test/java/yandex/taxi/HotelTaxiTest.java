package yandex.taxi;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class HotelTaxiTest {

    private HotelTaxi hotelTaxi = new HotelTaxi.Solution();

    @Test void n7m3arrival3_2departure2_5arrival3_2arrival2_2arrival1_10departure3_7output10_7() {
        assertArrayEquals(new int[]{10, 7},
                hotelTaxi.maxGuests(7, 3, Arrays.asList(
                        "arrival 3 2",
                        "departure 2 5",
                        "arrival 3 2",
                        "arrival 2 2",
                        "arrival 1 10",
                        "departure 3 7")));
    }
}