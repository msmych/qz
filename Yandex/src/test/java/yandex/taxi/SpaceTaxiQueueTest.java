package yandex.taxi;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SpaceTaxiQueueTest {

    private SpaceTaxiQueue spaceTaxiQueue = new SpaceTaxiQueue.Solution();

    @Test void input10points_output3() {
        assertEquals(3, spaceTaxiQueue.minimalQueueLength(10, Arrays.asList(
                "1 1 1",
                "2 2 2",
                "3 3 3",
                "4 4 4",
                "5 5 5",
                "6 6 6",
                "7 7 7",
                "3 6 9",
                "2 4 6",
                "1 2 3")));
    }

    @Test void input5points_output4() {
        assertEquals(4, spaceTaxiQueue.minimalQueueLength(5, Arrays.asList(
                "1 1 1",
                "2 2 2",
                "3 3 3",
                "4 1 4",
                "9 9 9")));
    }
}