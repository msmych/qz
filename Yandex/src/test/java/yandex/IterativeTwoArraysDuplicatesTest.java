package yandex;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class IterativeTwoArraysDuplicatesTest {

    private IterativeTwoArraysDuplicates iterativeTwoArraysDuplicates = new IterativeTwoArraysDuplicates.Solution();

    @Test void nums1356nums2436() {
        assertArrayEquals(new int[]{0, 0, 1, 2}, iterativeTwoArraysDuplicates.getDuplicates(new int[]{1, 3, 5, 6}, new int[]{2, 4, 3, 6}));
    }
}