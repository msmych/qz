package yandex;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class DoubleIteratorTest {

    @Test void i123i45() {
        List<Integer> list1 = Arrays.asList(1, 2, 3);
        List<Integer> list2 = Arrays.asList(4, 5);
        DoubleIterator doubleIterator = new DoubleIterator<>(list1.iterator(), list2.iterator());
        for (int i = 1; i <= 5; i++) {
            assertTrue(doubleIterator.hasNext());
            assertEquals(i, doubleIterator.next());
        }
        assertFalse(doubleIterator.hasNext());
        assertThrows(NoSuchElementException.class, doubleIterator::next);
    }
}