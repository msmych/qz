package yandex;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CompressStringTest {

    private CompressString compressString = new CompressString.Solution();

    @Test void input_aabcccbb_output_a2bc3b2() {
        assertEquals("a2bc3b2", compressString.archive("aabcccbb"));
    }
}