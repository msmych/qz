package print_foo_bar_alternately;

public class FooBar {

    private final static int FOO = 0;
    private final static int BAR = 1;

    private int message = FOO;

    private int n;

    public FooBar(int n) {
        this.n = n;
    }

    public void foo(Runnable printFoo) throws InterruptedException {

        for (int i = 0; i < n; i++) {
            synchronized (this) {
                while (true) {
                    if (message == FOO) {
                        printFoo.run();
                        message = BAR;
                        notifyAll();
                        break;
                    } else {
                        wait();
                    }
                }
            }
        }
    }

    public void bar(Runnable printBar) throws InterruptedException {

        for (int i = 0; i < n; i++) {
            synchronized (this) {
                while (true) {
                    if (message == BAR) {
                        printBar.run();
                        message = FOO;
                        notifyAll();
                        break;
                    } else {
                        wait();
                    }
                }
            }
        }
    }
}
