package zero_even_odd;

import java.util.Arrays;
import java.util.Set;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;

public class ZeroEvenOdd {

    private enum Num {
        ZERO(0, 2), EVEN(3), ODD(1);

        final Set<Integer> order;

        Num(int... order) {
            this.order = Arrays.stream(order).boxed().collect(Collectors.toSet());
        }

        static Num fromOrder(int order) {
            return Arrays.stream(values())
                    .filter(n -> n.order.contains(order))
                    .findAny()
                    .orElseThrow(() -> new IllegalArgumentException());
        }
    }

    private int n;

    private int num = 0;

    public ZeroEvenOdd(int n) {
        this.n = n;
    }

    // printNumber.accept(x) outputs "x", where x is an integer.
    public void zero(IntConsumer printNumber) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            synchronized (this) {
                while (true) {
                    if (Num.fromOrder(num) == Num.ZERO) {
                        printNumber.accept(0);
                        num = (num + 1) % 4;
                        notifyAll();
                        break;
                    } else {
                        wait();
                    }
                }
            }
        }
    }

    public void even(IntConsumer printNumber) throws InterruptedException {
        for (int i = 2; i <= n; i += 2) {
            synchronized (this) {
                while (true) {
                    if (Num.fromOrder(num) == Num.EVEN) {
                        printNumber.accept(i);
                        num = (num + 1) % 4;
                        notifyAll();
                        break;
                    } else {
                        wait();
                    }
                }
            }
        }
    }

    public void odd(IntConsumer printNumber) throws InterruptedException {
        for (int i = 1; i <= n; i += 2) {
            synchronized (this) {
                while (true) {
                    if (Num.fromOrder(num) == Num.ODD) {
                        printNumber.accept(i);
                        num = (num + 1) % 4;
                        notifyAll();
                        break;
                    } else {
                        wait();
                    }
                }
            }
        }
    }
}
