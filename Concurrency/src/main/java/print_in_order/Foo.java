package print_in_order;

public class Foo {

    private int step = 1;

    public Foo() {

    }

    public synchronized void first(Runnable printFirst) throws InterruptedException {

        // printFirst.run() outputs "first". Do not change or remove this line.
        printFirst.run();
        step++;
        notifyAll();
    }

    public synchronized void second(Runnable printSecond) throws InterruptedException {
        while (true) {
            if (step == 2) {
                // printSecond.run() outputs "second". Do not change or remove this line.
                printSecond.run();
                step++;
                notifyAll();
                break;
            } else wait();
        }
    }

    public synchronized void third(Runnable printThird) throws InterruptedException {
        while (true) {
            if (step == 3) {
                // printThird.run() outputs "third". Do not change or remove this line.
                printThird.run();
                break;
            } else wait();
        }
    }
}
