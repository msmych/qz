package deadlock;

public class Deadlock {

    private final Object lock1 = new Object();
    private final Object lock2 = new Object();

    private void action1() {
        synchronized (lock1) {
            System.out.println("Locked 1");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (lock2) {
                System.out.println("Locked 2");
            }
        }
    }

    private void action2() {
        synchronized (lock2) {
            System.out.println("Locked 2");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (lock1) {
                System.out.println("Locked 1");
            }
        }
    }

    public static void main(String[] args) {
        Deadlock deadlock = new Deadlock();
        new Thread(deadlock::action1).start();
        new Thread(deadlock::action2).start();
    }
}
