package building_h2o;

public class H2O {

  private enum HO {__, H, O, HO, HH}

  private volatile HO ho = HO.__;

  public H2O() {

  }

  public void hydrogen(Runnable releaseHydrogen) throws InterruptedException {
    synchronized (this) {
      while (true) {
        if (ho != HO.HH) {
          // releaseHydrogen.run() outputs "H". Do not change or remove this line.
          releaseHydrogen.run();
          switch (ho) {
            case __:
              ho = HO.H;
              break;
            case H:
              ho = HO.HH;
              break;
            case O:
              ho = HO.HO;
              break;
            case HO:
              ho = HO.__;
          }
          notifyAll();
          break;
        } else {
          wait();
        }
      }
    }
  }

  public void oxygen(Runnable releaseOxygen) throws InterruptedException {
    synchronized (this) {
      while (true) {
        if (ho != HO.O && ho != HO.HO) {
          // releaseOxygen.run() outputs "O". Do not change or remove this line.
          releaseOxygen.run();
          switch (ho) {
            case __:
              ho = HO.O;
              break;
            case H:
              ho = HO.HO;
              break;
            case HH:
              ho = HO.__;
          }
          notifyAll();
          break;
        } else {
          wait();
        }
      }
    }
  }
}
