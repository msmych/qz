package pingpong;

public class PingPong {

    private String last = "PONG";

    private synchronized void action(String message) {
        while (true) {
            if (last.equals(message))
                wait(this);
            else {
                System.out.print(message);
                last = message;
                sleep(1000);
                notifyAll();
            }
        }
    }

    public static void main(String[] args) {
        PingPong pingPong = new PingPong();
        new Thread(() -> pingPong.action("ping")).start();
        new Thread(() -> pingPong.action("PONG")).start();
    }

    private void wait(Object obj) {
        try {
            obj.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
