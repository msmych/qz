package fizz_buzz_multithreaded;

import org.junit.jupiter.api.Test;

class FizzBuzzTest {

  @Test void fizz_buzz() {
    FizzBuzz fizzBuzz = new FizzBuzz(15);
    new Thread(() -> {
      try {
        fizzBuzz.fizz(() -> System.out.print("fizz"));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }).start();
    new Thread(() -> {
      try {
        fizzBuzz.buzz(() -> System.out.print("buzz"));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }).start();
    new Thread(() -> {
      try {
        fizzBuzz.fizzbuzz(() -> System.out.print("fizzbuzz"));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }).start();
    new Thread(() -> {
      try {
        fizzBuzz.number(System.out::print);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }).start();
  }
}
