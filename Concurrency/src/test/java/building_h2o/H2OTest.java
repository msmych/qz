package building_h2o;

import org.junit.jupiter.api.Test;

class H2OTest {

  @Test void HOH() {
    run("HOH");
  }

  @Test void OOHHHH() {
    run("OOHHHH");
  }

  @Test void OOOOOOHHHHHHHHHH() {
    run("OOOOOOHHHHHHHHHH");
  }

  @Test void HHHHHHHHHHHHOOOOOOO() {
    run("HHHHHHHHHHHHOOOOOOO");
  }

  private void run(String s) {
    H2O h2o = new H2O();
    for (char c : s.toCharArray()) {
      if (c == 'H') {
        H(h2o);
      } else if (c == 'O'){
        O(h2o);
      }
    }
  }

  private void H(H2O h2o) {
    new Thread(() -> {
      try {
        h2o.hydrogen(() -> System.out.print("H"));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }).start();
  }

  private void O(H2O h2O) {
    new Thread(() -> {
      try {
        h2O.oxygen(() -> System.out.print("O"));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }).start();
  }
}
